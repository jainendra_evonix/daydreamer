
<!--Core js-->
<script src="<?php  echo base_url();?>user_assets/js/jquery.js"></script>
<script src="<?php  echo base_url();?>user_assets/js/jquery-1.8.3.min.js"></script>
<script src="<?php  echo base_url();?>user_assets/bs3/js/bootstrap.min.js"></script>
<script src="<?php  echo base_url();?>user_assets/js/jquery-ui-1.9.2.custom.min.js"></script>
<!-- <script src="http://ajax.googleapis.com/ajax/libs/jqueryui/1.9.2/jquery-ui.min.js"></script> -->
<script class="include" type="text/javascript" src="<?php  echo base_url();?>user_assets/js/jquery.dcjqaccordion.2.7.js"></script>
<script src="<?php  echo base_url();?>user_assets/js/jquery.scrollTo.min.js"></script>
<script src="<?php  echo base_url();?>user_assets/js/easypiechart/jquery.easypiechart.js"></script>
<script src="<?php  echo base_url();?>user_assets/js/jQuery-slimScroll-1.3.0/jquery.slimscroll.js"></script>
<script src="<?php  echo base_url();?>user_assets/js/jquery.nicescroll.js"></script>
<script src="<?php  echo base_url();?>user_assets/js/jquery.nicescroll.js"></script>


<!-- <script src="js/bootstrap-switch.js"></script> -->

<script type="text/javascript" src="<?php  echo base_url();?>user_assets/js/fuelux/js/spinner.min.js"></script>
<script type="text/javascript" src="<?php  echo base_url();?>user_assets/js/bootstrap-fileupload/bootstrap-fileupload.js"></script>
<!-- <script type="text/javascript" src="<?php  echo base_url();?>user_assets/js/bootstrap-wysihtml5/wysihtml5-0.3.0.js"></script>
<script type="text/javascript" src="<?php  echo base_url();?>user_assets/js/bootstrap-wysihtml5/bootstrap-wysihtml5.js"></script> -->
<script type="text/javascript" src="<?php  echo base_url();?>user_assets/js/bootstrap-datepicker/js/bootstrap-datepicker.js"></script>
<script type="text/javascript" src="<?php  echo base_url();?>user_assets/js/bootstrap-datetimepicker/js/bootstrap-datetimepicker.js"></script>



<script type="text/javascript" src="<?php  echo base_url();?>user_assets/js/bootstrap-daterangepicker/moment.min.js"></script>
<script type="text/javascript" src="<?php  echo base_url();?>user_assets/js/bootstrap-daterangepicker/daterangepicker.js"></script>
<script type="text/javascript" src="<?php  echo base_url();?>user_assets/js/bootstrap-colorpicker/js/bootstrap-colorpicker.js"></script>
<script type="text/javascript" src="<?php  echo base_url();?>user_assets/js/bootstrap-timepicker/js/bootstrap-timepicker.js"></script>
<script type="text/javascript" src="<?php  echo base_url();?>user_assets/js/jquery-multi-select/js/jquery.multi-select.js"></script>
<script type="text/javascript" src="<?php  echo base_url();?>user_assets/js/jquery-multi-select/js/jquery.quicksearch.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/dropzone/4.2.0/min/dropzone.min.js"></script>

<script type="text/javascript" src="<?php  echo base_url();?>user_assets/js/bootstrap-inputmask/bootstrap-inputmask.min.js"></script>

<script src="<?php  echo base_url();?>user_assets/js/jquery-tags-input/jquery.tagsinput.js"></script>

<script src="<?php  echo base_url();?>user_assets/js/select2/select2.js"></script>
<script src="<?php  echo base_url();?>user_assets/js/select-init.js"></script>


<!--common script init for all pages-->
<script src="<?php  echo base_url();?>user_assets/js/scripts.js"></script>
<script src="//cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.3.0/js/bootstrap-datepicker.min.js"></script>

<!-- <script src="js/toggle-init.js"></script> -->

<script src="<?php  echo base_url();?>user_assets/js/advanced-form.js"></script>
<!--Easy Pie Chart-->
<script src="<?php  echo base_url();?>user_assets/js/easypiechart/jquery.easypiechart.js"></script>
<!--Sparkline Chart-->
<script src="<?php  echo base_url();?>user_assets/js/sparkline/jquery.sparkline.js"></script>

<script src="https://cdnjs.cloudflare.com/ajax/libs/rateYo/2.2.0/jquery.rateyo.min.js"></script>
 <!--jQuery Flot Chart-->
<!-- <script src="<?php  echo base_url();?>user_assets/js/flot-chart/jquery.flot.js"></script>
<script src="<?php  echo base_url();?>user_assets/js/flot-chart/jquery.flot.tooltip.min.js"></script>
<script src="<?php  echo base_url();?>user_assets/js/flot-chart/jquery.flot.resize.js"></script>
<script src="<?php  echo base_url();?>user_assets/js/flot-chart/jquery.flot.pie.resize.js"></script> -->

<!-- <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
 -->

<!-- SmartMenus jQuery plugin -->
<script type="text/javascript" src="<?php echo base_url(); ?>web_assets/js/jquery.smartmenus.js"></script>
<!-- SmartMenus jQuery Bootstrap Addon -->
<script type="text/javascript" src="<?php echo base_url(); ?>web_assets/js/jquery.smartmenus.bootstrap.js"></script>

<script src="<?php echo base_url(); ?>web_assets/js/main.js"></script> <!-- Resource jQuery -->

<script src="<?php echo base_url(); ?>web_assets/owl/owl.carousel.min.js"></script>
<script type="text/javascript">
  Dropzone.options.imageUpload = {
        maxFilesize:1,
        acceptedFiles: ".jpeg,.jpg,.png,.gif"
    };
</script>

<script>
$(document).ready(function(){
	$('.owl-complogo').owlCarousel({
	    loop:true,
	    dots:false,
	    margin:50,
	    autoplay:true,
		autoplayTimeout:5000,
		autoplayHoverPause:false,
		nav:true,
	    navText:[
	        "<i class='fa fa-angle-left fa-2x'></i>",
	        "<i class='fa fa-angle-right fa-2x'></i>"
	    ],
	    responsiveClass:true,
	    responsive:{
	        0:{
	            items:3,
	            nav:true
	        },
	        600:{
	            items:4,
	            nav:true
	        },
	        1000:{
	            items:6,
	            nav:true
	        }
	    }
	});


  // Add smooth scrolling to all links in navbar + footer link
  $("a[href='#myPage']").on('click', function(event) {

   // Make sure this.hash has a value before overriding default behavior
  if (this.hash !== "") {

    // Prevent default anchor click behavior
    event.preventDefault();

    // Store hash
    var hash = this.hash;

    // Using jQuery's animate() method to add smooth page scroll
    // The optional number (900) specifies the number of milliseconds it takes to scroll to the specified area
    $('html, body').animate({
      scrollTop: $(hash).offset().top
    }, 900, function(){

      // Add hash (#) to URL when done scrolling (default click behavior)
      window.location.hash = hash;
      });
    } // End if
  });

})

</script>