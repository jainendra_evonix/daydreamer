        <?php // echo "<pre>"; print_r($userEducationDetails); exit;
           // echo "<pre>"; print_r($years); exit; ?>
           <style>
             .modal-backdrop{
                 position: absolute !important;
                 height: 0px !important;
             } 
         </style>


         <!--main content start-->
         <section id="" class="container">
            <section class="wrapper">
                <!-- page start-->
                <div class="row">
                    <div class="col-md-8 col-md-offset-2">
                        <h3 class="form-heading">Education Information</h3>

                        <div class="row">
                            <div class="col-md-12">
                                <!--collapse start-->

                                <div class="panel-group m-bot20" id="accordionedu">
                                    <div class="panel">
                                        <div class="panel-heading heading-invert">
                                            <h4 class="panel-title">
                                                <div class="row">
                                                    <div class="col-md-3">
                                                        <strong>Institute Name</strong>
                                                    </div>
                                                    <div class="col-md-2">
                                                        <strong>Duration</strong>
                                                    </div>
                                                    <div class="col-md-3">
                                                        <strong>Course of Study</strong>
                                                    </div>
                                                    <div class="col-md-3">
                                                        <strong>Qualification type</strong>
                                                    </div>
                                                    <div class="col-md-1">
                                                        <strong>Grade</strong>
                                                    </div>
                                                </div>
                                            </h4>
                                        </div>
                                    </div>

                                    <?php if($userEducationDetails){ 
                                      error_reporting(0);
                                      foreach ($userEducationDetails as  $value) {


                                          ?>




                                          <div class="panel">
                                            <div class="panel-heading">
                                                <h4 class="panel-title">
                                                    <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordionedu" href="<?php echo '#'.$value->id; ?>">
                                                        <div class="row">
                                                            <div class="col-md-3">
                                                                <?php echo $value->institute_name; ?>
                                                            </div>
                                                            <div class="col-md-2">
                                                                <?php if($value->to_year==0){ echo "studying";}else{    echo $value->from_year."-".$value->to_year; } ?>
                                                            </div>
                                                            <div class="col-md-3">
                                                                <?php echo $value->course; ?>
                                                            </div>
                                                            <div class="col-md-3">
                                                                <?php echo $value->qualification; ?>
                                                            </div>
                                                            <div class="col-md-1">
                                                                <?php echo $value->grade; ?>
                                                            </div>
                                                        </div>
                                                    </a>
                                                </h4>
                                            </div>



                                            <div id="<?php echo $value->id; ?>" class="panel-collapse collapse">
                                                <div class="panel-body">
                                                    <div class="weather-category twt-category">
                                                        <ul>
                                                            <li>
                                                                Course of Study:
                                                                <h5><?php echo $value->course; ?></h5>
                                                            </li>
                                                            <li>
                                                                Qualification type:
                                                                <h5><?php echo $value->qualification; ?></h5>
                                                            </li>
                                                            <li>
                                                                Grade
                                                                <h5><?php echo $value->grade; ?></h5>
                                                            </li>
                                                            <li>
                                                                Date From:
                                                                <h5><?php echo $value->fromMonth1." , ".$value->from_year;?></h5>
                                                            </li>
                                                            <li>
                                                                Date To:
                                                                <h5><?php if($value->to_year==0){echo "studying";} else{ echo $value->toMonth1." , ".$value->to_year; }?></h5>
                                                            </li>
                                                            <li>
                                                                Predicted/Achieved:
                                                                <h5><?php echo $value->achvd; ?></h5>
                                                            </li>
                                                        </ul>
                                                    </div>
                                                    <span class="divider"></span>
                                                    <footer class="twt-footer">
                                                           <!-- <strong>Add Media: </strong> 
                                                            <a  href="#"><span class="label label-default"><i class="fa fa-file-text-o"></i>Document</span></a>
                                                            <a href="#"><span class="label label-default"><i class="fa fa-picture-o"></i>Photo</span></a>
                                                            <a href="#"><span class="label label-default"><i class="fa fa-link"></i>Link</span></a> -->
                                                            <span class="pull-right">
                                                                <a  data ="<?php echo $value->id; ?>" data-toggle="modal" data-target="#myModal" href="javascript:void(0);" ng-click="editEducation(<?php echo $value->id; ?>);"><i class="fa fa-pencil"></i>Edit</a>&nbsp;

                                                                <a data="<?php echo $value->id; ?>" data-toggle="modal" data-target="" href="javascript:void(0);" ng-click="deleteEducation(<?php echo $value->id; ?>);"><i class="fa fa-trash-o" aria-hidden="true"> Delete</i></a>

                                                            </span>
                                                        </footer>
                                                    </div>
                                                </div>
                                            </div>
                                            <?php    } } ?>

                                        </div>
                                        <!--collapse end-->
                                    </div>
                                  </div>



                                    <!--  modal open-->

                                    <form class="form-horizontal bucket-form" name="editUserEducation" ng-submit="submitEditForm()" novalidate >
                                        <div class="modal fade" id="myModal" role="dialog" style="background-color: rgba(0, 0, 0, 0.5);">
                                            <div class="modal-dialog modal-lg">

                                              <!-- Modal content-->
                                              <div class="modal-content">
                                                <div class="modal-header">
                                                  <button type="button" class="close" data-dismiss="modal">&times;</button>
                                                  <h4 class="modal-title">Edit Education Info</h4>
                                              </div>
                                              <div class="modal-body">
                                                <h4>Academic Institution</h4>

                                                <div class="row">
                                                   <div class="col-md-3">
                                                       Name of Place of Study<span class="req">*</span>

                                                   </div>

                                                   <div class="col-md-9">
                                                    <input type="text"    ng-model="editUser.einstName" name="einstName" class="form-control" placeholder="Enter name of institution" required>
                                                    <input type="hidden"  ng-model="editUser.hiddenId" name="hiddenId" class="form-control" placeholder="Enter name of institution" required>
                                                    <span ng-show="submittedd && editUserEducation.einstName.$invalid" class="help-block has-error ng-hide warnig">Please enter name of institution</span>
                                                    <span class="help-block has-error ng-hide warnig" style="color: red;" ng-show="einstNameError">{{einstNameError}}</span>
                                                </div>
                                            </div><br>

                                            <div class="row">
                                               <div class="col-md-3">
                                                   Date From<span class="req">*</span>

                                               </div>
                                               <div class="col-md-3">
                                                 <select class="form-control m-bot15" ng-model="editUser.efromMonth" name="efromMonth" id="efromMonth" onchange="return checkFromMonthE();" required>
                                                    <option value="" selected="selected">Month</option>
                                                    <?php if($months)
                                                    {
                                                     foreach ($months as $value) { ?>

                                                     <option value="<?php echo $value->id; ?>" ><?php echo $value->month_name; ?></option>

                                                     <?php    } } ?>
                                                 </select>

                                                 <span ng-show="submittedd && editUserEducation.efromMonth.$invalid" class="help-block has-error ng-hide warnig">Please select month</span>
                                                 <span class="help-block has-error ng-hide warnig" style="color: red;" ng-show="efromMonthError">{{efromMonthError}}</span>
                                             </div>
                                             <div class="col-md-3">



                                               <select class="form-control m-bot15" ng-model="editUser.efromYear" name="efromYear" value="" id="efromYear" onchange="return checkFromDateE();" required>
                                                <option value="" selected="selected">Year</option>
                                                <?php if($years)
                                                {
                                                 foreach ($years as $value) { ?>

                                                 <option value="<?php echo $value->year; ?>"  ><?php echo $value->year; ?></option>

                                                 <?php    } } ?>


                                             </select>
                                             <span ng-show="submittedd && editUserEducation.efromYear.$invalid" class="help-block has-error ng-hide warnig">Please select year</span>
                                             <span class="help-block has-error ng-hide warnig" style="color: red;" ng-show="efromYearError">{{efromYearError}}</span>


                                         </div>

                                         <div class="col-sm-3">
                                             <div class="checkbox">
                                                <label>
                                                            
                                                            <input type="checkbox"  ng-model="editUser.ecurrStatus" name="ecurrStatus" value="yes" > <strong>Current</strong>
                                                            <p class="help-text">Currently studied<span class="req">*</span></p>
                                                            <span ng-show="submitted && editUserEducation.ecurrStatus.$invalid" class="help-block has-error ng-hide warnig">Please select status</span>
                                                            <span class="help-block has-error ng-hide warnig" style="color: red;" ng-show="ecurrStatusError">{{ecurrStatusError}}</span>
                                                            
                                                        </label>
                                                    </div>
                                                </div>

                                            </div>
                                            <br>
                                            <div class="row">
                                               <div class="col-md-3">
                                                   Date To<span class="req">*</span>

                                               </div>
                                               <div ng-show="!editUser.ecurrStatus==1">
                                               <div class="col-md-3">
                                                <select class="form-control m-bot15" ng-model="editUser.etoMonth" name="etoMonth" id="etoMonth" onchange="return checkToMonthE();" ng-required="!editUser.ecurrStatus==1">
                                                    <option value="" selected="selected">Month</option>
                                                    <?php if($months)
                                                    {
                                                     foreach ($months as $value) { ?>

                                                     <option value="<?php echo $value->id; ?>" ><?php echo $value->month_name; ?></option>

                                                     <?php    } } ?>
                                                 </select>

                                                 <span ng-show="submittedd && editUserEducation.efromMonth.$invalid" class="help-block has-error ng-hide warnig">Please select month</span>
                                                 <span class="help-block has-error ng-hide warnig" style="color: red;" ng-show="efromMonthError">{{efromMonthError}}</span>
                                             </div>



                                             <div class="col-md-3">

                                              <select class="form-control m-bot15" ng-model="editUser.etoYear" name="etoYear" value="" id="etoYear" onchange="return checkToDateE();" ng-required="!editUser.ecurrStatus==1">
                                                <option value="" selected="selected">Year</option>
                                                <?php if($years)
                                                {
                                                 foreach ($years as $value) { ?>

                                                 <option value="<?php echo $value->year; ?>"  ><?php echo $value->year; ?></option>

                                                 <?php    } } ?>


                                             </select>
                                             <span ng-show="submittedd && editUserEducation.efromYear.$invalid" class="help-block has-error ng-hide warnig">Please select year</span>
                                             <span class="help-block has-error ng-hide warnig" style="color: red;" ng-show="efromYearError">{{efromYearError}}</span>
                                         </div>
                                     </div>
                 <!-- <div class="col-md-3">
                 <label>
               <input type="radio" value="yes" ng-model="editUser.estudying" name="estudying"  required > Yes
                                                            <input type="radio"  value="no" ng-model="editUser.estudying" name="estudying" required >No
                                                            <span ng-show="submittedd && editUserEducation.estudying.$invalid" class="help-block has-error ng-hide warnig">Please select status</span>
                                                    <span class="help-block has-error ng-hide warnig" style="color: red;" ng-show="estudyingError">{{estudyingError}}</span>
                                                    <p class="help-text">Select this if currently studied<span class="req">*</span></p></label>
                                                </div> -->

                                            </div>
                                            <hr>
                                            <!-- <h4>Course of study</h4> -->
                                            <div class="row">
                                               <div class="col-md-3">
                                                Course of study<span class="req">*</span>

                                            </div>
                                            <div class="col-md-6">
                                                <input type="text" class="form-control" placeholder="Specify course of study" ng-model="editUser.ecourseStudy" name="ecourseStudy" required>
                                                <span ng-show="submittedd && editUserEducation.ecourseStudy.$invalid" class="help-block has-error ng-hide warnig">Please enter course of study</span>
                                                <span class="help-block has-error ng-hide warnig" style="color: red;" ng-show="ecourseStudyError">{{ecourseStudyError}}</span>
                                            </div>
                                            <div class="col-md-3">
                                                <!-- <p>display additional instructions</p> -->
                                            </div>
                                        </div>
                                        <br>
                                        <div class="row">
                                           <div class="col-md-3">
                                              Qualification Type<span class="req">*</span>

                                          </div>
                                          <div class="col-md-6">
                                           <select class="form-control m-bot15" ng-model="editUser.equalificationType" name="equalificationType" required="">
                                            <option value="">Select qualification type</option>
                                            <?php if($qualification) {
                                              foreach($qualification as $options){ ?>

                                              <option value="<?php echo $options->id; ?>"><?php echo $options->qualification; ?></option>

                                              <?php  } }?>

                                          </select>

                                          <span ng-show="submittedd && editUserEducation.equalificationType.$invalid" class="help-block has-error ng-hide warnig">Please enter name of institution</span>
                                          <span class="help-block has-error ng-hide warnig" style="color: red;" ng-show="equalificationTypeError">{{equalificationTypeError}}</span>
                                      </div>
                                      <div class="col-md-3">
                                          <!-- <p>display additional instructions</p> -->
                                      </div>

                                  </div>
                                  <div class="row">
                                   <div class="col-md-3">
                                    Select Grade<span class="req">*</span>

                                </div>
                                <div class="col-md-6" style="padding-block-end:13px;">
                                   <!-- <select class="form-control m-bot15" ng-model="editUser.egrade" name="egrade" required="">
                                    <option value=""  selected="selected">Select grade</option>
                                    <?php if($grade ){

                                       foreach ($grade as $key ) { ?>

                                       <option value="<?php echo $key->id; ?>" ><?php echo $key->grade; ?></option>

                                       <?php    } } ?>


                                   </select> -->
                                   <input type="text" class="form-control" placeholder="Enter grade" ng-model="editUser.egrade" name="egrade" require ng-pattern="/.*[a-zA-Z]+.*/" required>

                                   <span ng-show="submittedd && editUserEducation.egrade.$invalid" class="help-block has-error ng-hide warnig">Please enter grade</span>
                                   <span class="help-block has-error ng-hide warnig" style="color: red;" ng-show="egradeError">{{egradeError}}</span>
                               </div>



                           </div>
                           <div class="row">
                               <div class="col-md-3">
                                 Predicted/Achieved<span class="req">*</span>

                             </div>
                             <div class="col-md-6">
                              <select class="form-control m-bot15" ng-model="editUser.eachieved" name="eachieved" required> 
                                <option value="" >Select predicted/achieved</option>
                                <?php if($achieved){ 
                                    foreach($achieved as $value){?>
                                    <option value="<?php echo $value->id; ?>"><?php echo $value->achieved; ?></option>

                                    <?php  } }?>

                                </select>
                                <span ng-show="submittedd && editUserEducation.eachieved.$invalid" class="help-block has-error ng-hide warnig">Please enter name of institution</span>
                                <span class="help-block has-error ng-hide warnig" style="color: red;" ng-show="eachievedError">{{eachievedError}}</span>
                            </div>



                        </div>
                        <div class="row">
                           <div class="col-md-12">
                              <button type="submit" ng-click="submittedd = true" class="btn btn-info pull-right btn-sm"><strong> Update <i class="fa fa-arrow-right"></i></strong></button>
                          </div>
                      </div>
                  </div>
                  <div class="modal-footer">
                      <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                  </div>
              </div>
              
          </div>
      </div>
    </form>
    <!--  modal close-->


    

    <?php if($this->session->flashdata('succmsg')){  ?>

    <div class="alert alert-success">
       <strong>Success!</strong> <?php echo $this->session->flashdata('succmsg'); ?>
       <!-- <button type="button" class="close" data-dismiss="modal">&times;</button> -->
       <a type="button" class="close" data-dismiss="alert" aria-hidden="true">×</a>
    </div>

    <?php    } ?>
    <hr>
    <h3 class="form-heading">Add Education </h3>
    <section class="panel">
        <div class="panel-body">
            <form class="form-horizontal bucket-form" name="userEducation" ng-submit="submitForm()" novalidate>
                <h4>Academic Institution</h4>
                <div class="form-group">
                    <label class="col-sm-3 control-label">Name of Place of Study<span class="req">*</span></label>
                    <div class="col-sm-6">
                        <input type="text"  ng-model="user.instName" name="instName" class="form-control" placeholder="Enter name of institution" require ng-pattern="/.*[a-zA-Z]+.*/" required>

                        <span ng-show="submitted && userEducation.instName.$invalid" class="help-block has-error ng-hide warnig">Please enter name of institution</span>
                        <span class="help-block has-error ng-hide warnig" style="color: red;" ng-show="instNameError">{{instNameError}}</span>
                    </div>
                </div>


                <div class="form-group">
                    <label class="control-label col-md-3">Date From<span class="req">*</span></label>
                    <div class="col-sm-2 padingr">
                        <select class="form-control m-bot15" ng-model="user.fromMonth" name="fromMonth" id="fromMonth"  onchange="checkFromMonth();" required>
                            <option value="" selected="selected">Month</option>
                            <?php if($months)
                            {
                             foreach ($months as $value) { ?>

                             <option value="<?php echo $value->id; ?>" ><?php echo $value->month_name; ?></option>

                             <?php    } } ?>
                         </select>
                         <span ng-show="submitted && userEducation.fromMonth.$invalid" class="help-block has-error ng-hide warnig">Please select month</span>
                         <span class="help-block has-error ng-hide warnig" style="color: red;" ng-show="fromMonthError">{{fromMonthError}}</span>
                     </div>
                     <div class="col-sm-2 padingr">



                        <select class="form-control m-bot15" ng-model="user.fromYear" name="fromYear" id="fromYear" onchange="return checkFromDate();" value="" required>
                            <option value="" selected="selected">Year</option>
                            <?php if($years)
                            {
                             foreach ($years as $value) { ?>

                             <option value="<?php echo $value->year; ?>"  ><?php echo $value->year; ?></option>

                             <?php    } } ?>


                         </select>
                         <span ng-show="submitted && userEducation.fromYear.$invalid" class="help-block has-error ng-hide warnig">Please select year</span>
                         <span class="help-block has-error ng-hide warnig" style="color: red;" ng-show="fromYearError">{{fromYearError}}</span>

                     </div>

                     <div class="col-sm-5">
                        <div class="checkbox">
                            <label>
                            <input type="checkbox"  ng-model="user.currStatus" name="currStatus" value="yes"> <strong>Current</strong>
                                                            <p class="help-text">Select this if currently studied<span class="req">*</span></p>
                                                            <span ng-show="submitted && userEducation.studying.$invalid" class="help-block has-error ng-hide warnig">Please select status</span>
                                                            <span class="help-block has-error ng-hide warnig" style="color: red;" ng-show="studyingError">{{studyingError}}</span>
                                                            
                                                        </label>
                                                    </div>
                                                </div>
                                            </div>



                                            <div class="form-group" ng-show="!user.currStatus==1">
                                                <label class="control-label col-md-3">Date To<span class="req">*</span></label>
                                                <div class="col-sm-2 padingr">

                                                   <select class="form-control m-bot15" ng-model="user.toMonth" name="toMonth" id="toMonth" onchange="checkToMonth();" ng-required="!user.currStatus==1">
                                                    <option value="" selected="selected">Month</option>
                                                    <?php if($months)
                                                    {
                                                     foreach ($months as $value) { ?>

                                                     <option value="<?php echo $value->id; ?>" ><?php echo $value->month_name; ?></option>

                                                     <?php    } } ?>
                                                 </select>

                                                 <span ng-show="submitted && userEducation.toMonth.$invalid" class="help-block has-error ng-hide warnig">Please select month</span>
                                                 <span class="help-block has-error ng-hide warnig" style="color: red;" ng-show="toMonthError">{{toMonthError}}</span>
                                             </div>
                                             <div class="col-sm-2 padingr">



                                               <select class="form-control m-bot15" ng-model="user.toYear" name="toYear" id="toYear" onchange="checkToDate();" ng-required="!user.currStatus">
                                                <option value="" selected="selected">Year</option>
                                                <?php if($years)
                                                {
                                                 foreach ($years as $value) { ?>

                                                 <option value="<?php echo $value->year; ?>" ><?php echo $value->year; ?></option>

                                                 <?php    } } ?>
                                             </select>
                                             <span ng-show="submitted && userEducation.toYear.$invalid" class="help-block has-error ng-hide warnig">Please select year</span>
                                             <span class="help-block has-error ng-hide warnig" style="color: red;" ng-show="toYearError">{{toYearError}}</span>
                                         </div>

                                     </div>


                                     <hr>

                                     <h4>Course of study</h4>
                                     <div class="form-group">
                                        <label class="col-sm-3 control-label">Course of Study<span class="req">*</span></label>
                                        <div class="col-sm-6">
                                            <input type="text" class="form-control" placeholder="Specify course of study" ng-model="user.courseStudy" name="courseStudy" require ng-pattern="/.*[a-zA-Z]+.*/" required>
                                            <span ng-show="submitted && userEducation.courseStudy.$invalid" class="help-block has-error ng-hide warnig">Please enter course of study</span>
                                            <span class="help-block has-error ng-hide warnig" style="color: red;" ng-show="courseStudyError">{{courseStudyError}}</span>
                                        </div>
                                        <div class="col-md-3">
                                            <span class="help-block">Display additional instructions here.</span>
                                        </div>


                                    </div>
                                    <div class="form-group">
                                        <label class="col-sm-3 control-label">Qualification type<span class="req">*</span></label>
                                        <div class="col-sm-6">
                                            <!-- <input type="text" class="form-control" placeholder="Specify qualification type"> -->
                                            <select class="form-control m-bot15" ng-model="user.qualificationType" name="qualificationType" required="">
                                                <option value="">Select qualification type</option>
                                                <?php if($qualification) {
                                                  foreach($qualification as $options){ ?>

                                                  <option value="<?php echo $options->id; ?>"><?php echo $options->qualification; ?></option>

                                                  <?php  } }?>

                                              </select>
                                              <span ng-show="submitted && userEducation.qualificationType.$invalid" class="help-block has-error ng-hide warnig">Please select qualification type</span>
                                              <span class="help-block has-error ng-hide warnig" style="color: red;" ng-show="courseStudyError">{{qualificationTypeError}}</span>
                                          </div>
                                          <div class="col-md-3">
                                            <span class="help-block">Display additional instructions here.</span>
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label class="col-sm-3 control-label">Grade<span class="req">*</span></label>
                                        <div class="col-sm-6">
                                            <!-- <select class="form-control m-bot15" ng-model="user.grade" name="grade" required="">
                                                <option value=""  selected="selected">Select grade</option>
                                                <?php if($grade ){

                                                   foreach ($grade as $key ) { ?>

                                                   <option value="<?php echo $key->id; ?>" ><?php echo $key->grade; ?></option>

                                                   <?php    } } ?>


                                               </select> -->


                                                   <input type="text" class="form-control" placeholder="Enter grade" ng-model="user.grade" name="grade" require ng-pattern="/.*[a-zA-Z]+.*/" required>

                                               <span ng-show="submitted && userEducation.grade.$invalid" class="help-block has-error ng-hide warnig">Please enter grade</span>
                                               <span class="help-block has-error ng-hide warnig" style="color: red;" ng-show="gradeError">{{gradeError}}</span>
                                           </div>
                                       </div>

                                       <div class="form-group">
                                        <label class="col-sm-3 control-label">Predicted/Achieved<span class="req">*</span></label>
                                        <div class="col-sm-6">
                                            <select class="form-control m-bot15" ng-model="user.achieved" name="achieved" required> 
                                                <option value="" >Select predicted/achieved</option>
                                                <?php if($achieved){ 
                                                    foreach($achieved as $value){?>
                                                    <option value="<?php echo $value->id; ?>"><?php echo $value->achieved; ?></option>

                                                    <?php  } }?>

                                                </select>

                                                <span ng-show="submitted && userEducation.achieved.$invalid" class="help-block has-error ng-hide warnig">Please predicted/achieved</span>
                                                <span class="help-block has-error ng-hide warnig" style="color: red;" ng-show="achievedError">{{achievedError}}</span>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <div class="col-md-9">
                                                <button class="btn btn-success btn-sm pull-right" ng-click="submitted = true" ><strong>Add <i class="fa fa-plus"></i></strong></button>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <div class="col-md-3">
                                                <a href="<?php echo base_url(); ?>user/profile" type="button" class="btn btn-danger btn-sm"><strong><i class="fa fa-arrow-left"></i> Back</strong></a>
                                                <span class="help-block small text-muted">(Personal Information)</span>
                                            </div>
                                            <?php if(!empty($userEducationDetails))  {?>
                                            <div class="col-md-9">
                                             <a href="<?php echo base_url(); ?>user/preEmployment"> <button type="button"  class="btn btn-info pull-right btn-sm"><strong>Save &amp; Continue <i class="fa fa-arrow-right"></i></strong></button></a>
                                             <span class="clearfix"></span>
                                             <span class="help-block pull-right small text-muted">(Pre-Employment)</span>
                                         </div>

                                         <?php }?>

                                     </div>
                                 </form>
                             </div>
                         </section>
                     </div>
                 </div>
                 <!-- page end-->

             </section>
         </section>
         <!--main content end-->






         <!--right sidebar start-->
         <div class="right-sidebar">
            <div class="search-row">
                <input type="text" placeholder="Search" class="form-control">
            </div>
            <div class="right-stat-bar">
                <ul class="right-side-accordion">
                    <li class="widget-collapsible">
                        <a href="#" class="head widget-head red-bg active clearfix">
                            <span class="pull-left">work progress (5)</span>
                            <span class="pull-right widget-collapse"><i class="ico-minus"></i></span>
                        </a>
                        <ul class="widget-container">
                            <li>
                                <div class="prog-row side-mini-stat clearfix">
                                    <div class="side-graph-info">
                                        <h4>Target sell</h4>
                                        <p>
                                            25%, Deadline 12 june 13
                                        </p>
                                    </div>
                                    <div class="side-mini-graph">
                                        <div class="target-sell">
                                        </div>
                                    </div>
                                </div>
                                <div class="prog-row side-mini-stat">
                                    <div class="side-graph-info">
                                        <h4>product delivery</h4>
                                        <p>
                                            55%, Deadline 12 june 13
                                        </p>
                                    </div>
                                    <div class="side-mini-graph">
                                        <div class="p-delivery">
                                            <div class="sparkline" data-type="bar" data-resize="true" data-height="30" data-width="90%" data-bar-color="#39b7ab" data-bar-width="5" data-data="[200,135,667,333,526,996,564,123,890,564,455]">
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="prog-row side-mini-stat">
                                    <div class="side-graph-info payment-info">
                                        <h4>payment collection</h4>
                                        <p>
                                            25%, Deadline 12 june 13
                                        </p>
                                    </div>
                                    <div class="side-mini-graph">
                                        <div class="p-collection">
                                          <span class="pc-epie-chart" data-percent="45">
                                              <span class="percent"></span>
                                          </span>
                                      </div>
                                  </div>
                              </div>
                              <div class="prog-row side-mini-stat">
                                <div class="side-graph-info">
                                    <h4>delivery pending</h4>
                                    <p>
                                        44%, Deadline 12 june 13
                                    </p>
                                </div>
                                <div class="side-mini-graph">
                                    <div class="d-pending">
                                    </div>
                                </div>
                            </div>
                            <div class="prog-row side-mini-stat">
                                <div class="col-md-12">
                                    <h4>total progress</h4>
                                    <p>
                                        50%, Deadline 12 june 13
                                    </p>
                                    <div class="progress progress-xs mtop10">
                                        <div style="width: 50%" aria-valuemax="100" aria-valuemin="0" aria-valuenow="20" role="progressbar" class="progress-bar progress-bar-info">
                                            <span class="sr-only">50% Complete</span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </li>
                    </ul>
                </li>
                <li class="widget-collapsible">
                    <a href="#" class="head widget-head terques-bg active clearfix">
                        <span class="pull-left">contact online (5)</span>
                        <span class="pull-right widget-collapse"><i class="ico-minus"></i></span>
                    </a>
                    <ul class="widget-container">
                        <li>
                            <div class="prog-row">
                                <div class="user-thumb">
                                    <a href="#"><img src="<?php echo base_url(); ?>user_assets/images/avatar1_small.jpg" alt=""></a>
                                </div>
                                <div class="user-details">
                                    <h4><a href="#">Jonathan Smith</a></h4>
                                    <p>
                                        Work for fun
                                    </p>
                                </div>
                                <div class="user-status text-danger">
                                    <i class="fa fa-comments-o"></i>
                                </div>
                            </div>
                            <div class="prog-row">
                                <div class="user-thumb">
                                    <a href="#"><img src="<?php echo base_url(); ?>user_assets/images/avatar1.jpg" alt=""></a>
                                </div>
                                <div class="user-details">
                                    <h4><a href="#">Anjelina Joe</a></h4>
                                    <p>
                                        Available
                                    </p>
                                </div>
                                <div class="user-status text-success">
                                    <i class="fa fa-comments-o"></i>
                                </div>
                            </div>
                            <div class="prog-row">
                                <div class="user-thumb">
                                    <a href="#"><img src="<?php echo base_url(); ?>user_assets/images/chat-avatar2.jpg" alt=""></a>
                                </div>
                                <div class="user-details">
                                    <h4><a href="#">John Doe</a></h4>
                                    <p>
                                        Away from Desk
                                    </p>
                                </div>
                                <div class="user-status text-warning">
                                    <i class="fa fa-comments-o"></i>
                                </div>
                            </div>
                            <div class="prog-row">
                                <div class="user-thumb">
                                    <a href="#"><img src="<?php echo base_url(); ?>user_assets/images/avatar1_small.jpg" alt=""></a>
                                </div>
                                <div class="user-details">
                                    <h4><a href="#">Mark Henry</a></h4>
                                    <p>
                                        working
                                    </p>
                                </div>
                                <div class="user-status text-info">
                                    <i class="fa fa-comments-o"></i>
                                </div>
                            </div>
                            <div class="prog-row">
                                <div class="user-thumb">
                                    <a href="#"><img src="<?php echo base_url(); ?>user_assets/images/avatar1.jpg" alt=""></a>
                                </div>
                                <div class="user-details">
                                    <h4><a href="#">Shila Jones</a></h4>
                                    <p>
                                        Work for fun
                                    </p>
                                </div>
                                <div class="user-status text-danger">
                                    <i class="fa fa-comments-o"></i>
                                </div>
                            </div>
                            <p class="text-center">
                                <a href="#" class="view-btn">View all Contacts</a>
                            </p>
                        </li>
                    </ul>
                </li>
                <li class="widget-collapsible">
                    <a href="#" class="head widget-head purple-bg active">
                        <span class="pull-left"> recent activity (3)</span>
                        <span class="pull-right widget-collapse"><i class="ico-minus"></i></span>
                    </a>
                    <ul class="widget-container">
                        <li>
                            <div class="prog-row">
                                <div class="user-thumb rsn-activity">
                                    <i class="fa fa-clock-o"></i>
                                </div>
                                <div class="rsn-details ">
                                    <p class="text-muted">
                                        just now
                                    </p>
                                    <p>
                                        <a href="#">Jim Doe </a>Purchased new equipments for zonal office setup
                                    </p>
                                </div>
                            </div>
                            <div class="prog-row">
                                <div class="user-thumb rsn-activity">
                                    <i class="fa fa-clock-o"></i>
                                </div>
                                <div class="rsn-details ">
                                    <p class="text-muted">
                                        2 min ago
                                    </p>
                                    <p>
                                        <a href="#">Jane Doe </a>Purchased new equipments for zonal office setup
                                    </p>
                                </div>
                            </div>
                            <div class="prog-row">
                                <div class="user-thumb rsn-activity">
                                    <i class="fa fa-clock-o"></i>
                                </div>
                                <div class="rsn-details ">
                                    <p class="text-muted">
                                        1 day ago
                                    </p>
                                    <p>
                                        <a href="#">Jim Doe </a>Purchased new equipments for zonal office setup
                                    </p>
                                </div>
                            </div>
                        </li>
                    </ul>
                </li>
                <li class="widget-collapsible">
                    <a href="#" class="head widget-head yellow-bg active">
                        <span class="pull-left"> shipment status</span>
                        <span class="pull-right widget-collapse"><i class="ico-minus"></i></span>
                    </a>
                    <ul class="widget-container">
                        <li>
                            <div class="col-md-12">
                                <div class="prog-row">
                                    <p>
                                        Full sleeve baby wear (SL: 17665)
                                    </p>
                                    <div class="progress progress-xs mtop10">
                                        <div class="progress-bar progress-bar-success" role="progressbar" aria-valuenow="20" aria-valuemin="0" aria-valuemax="100" style="width: 40%">
                                            <span class="sr-only">40% Complete</span>
                                        </div>
                                    </div>
                                </div>
                                <div class="prog-row">
                                    <p>
                                        Full sleeve baby wear (SL: 17665)
                                    </p>
                                    <div class="progress progress-xs mtop10">
                                        <div class="progress-bar progress-bar-info" role="progressbar" aria-valuenow="20" aria-valuemin="0" aria-valuemax="100" style="width: 70%">
                                            <span class="sr-only">70% Completed</span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </li>
                    </ul>
                </li>
            </ul>
        </div>
    </div>

