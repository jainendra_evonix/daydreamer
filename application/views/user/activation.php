<div class="section">
<div class="container">
<div class="well">

    <p>
        Thanks for creating an account with Day Dreamer.
    </p>
    <p>
        We sent an email to your email account, please login there and click on link to activate your account.
    </p>
    <p>
        If you have not got any email then please check your junk emails and if still not there then please wait for 4 to 5 minute. 
    </p>
    <p>
        If still you have not got any email then please write us at <a href="mailto:support@evonixtech.com">support@evonixtech.com</a>
    </p>
    <br>
    <p>
        Thanks<br>
        Day Dreamer Team
    </p>

</div>

</div>
</div>