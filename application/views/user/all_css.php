<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">

    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="shortcut icon" href="<?php  echo base_url();?>user_assets/images/fevicon.jpg">

   <title><?php echo isset($title) ? $title : 'Day Dreamer'; ?></title>
    <!--Core CSS -->
     <link rel="stylesheet" href="//maxcdn.bootstrapcdn.com/font-awesome/4.2.0/css/font-awesome.min.css" />
    <link href="<?php  echo base_url();?>user_assets/bs3/css/bootstrap.min.css" rel="stylesheet">
    <link href="<?php  echo base_url();?>user_assets/css/bootstrap-reset.css" rel="stylesheet">
    <link href="<?php  echo base_url();?>user_assets/font-awesome/css/font-awesome.css" rel="stylesheet" />
  
    <link rel="stylesheet" href="//maxcdn.bootstrapcdn.com/bootstrap/3.3.1/css/bootstrap.min.css" />
    <link rel="stylesheet" href="<?php echo base_url(); ?>user_assets/css/bootstrap-switch.css" />
    
    <!-- start front end css -->

    <link href="https://fonts.googleapis.com/css?family=David+Libre|Hind:300,400,600,700" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Lato" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Sanchez" rel="stylesheet">
    <link href="http://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css" rel="stylesheet">
    <link rel="stylesheet" href="<?php echo base_url();?>web_assets/css/jquery.smartmenus.bootstrap.css">
    <!-- <link rel="stylesheet" href="css/reset.css"> -->
    <link rel="stylesheet" href="<?php echo base_url();?>web_assets/owl/owl.carousel.css">
    <link rel="stylesheet" href="<?php echo base_url();?>web_assets/css/style.css">
    <!-- end front end css -->
    <!--  <link rel="stylesheet" href="<?php echo base_url(); ?>user_assets/css/bootstrap-datepicker.css" /> -->
    <link rel="stylesheet" type="text/css" href="<?php  echo base_url();?>user_assets/js/bootstrap-fileupload/bootstrap-fileupload.css" />
    <link href="http://ajax.googleapis.com/ajax/libs/jqueryui/1.9.2/themes/smoothness/jquery-ui.css" rel="stylesheet" type="text/css" />
    <link rel="stylesheet" type="text/css" href="<?php  echo base_url();?>user_assets/js/bootstrap-wysihtml5/bootstrap-wysihtml5.css" />
    <link rel="stylesheet" type="text/css" href="<?php  echo base_url();?>user_assets/js/bootstrap-datepicker/css/datepicker.css" />
     <link rel="stylesheet" type="text/css" href="<?php  echo base_url();?>user_assets/js/bootstrap-colorpicker/css/colorpicker.css" />
    <link rel="stylesheet" type="text/css" href="<?php  echo base_url();?>user_assets/js/bootstrap-daterangepicker/daterangepicker-bs3.css" />
    <link rel="stylesheet" type="text/css" href="<?php  echo base_url();?>user_assets/js/bootstrap-datetimepicker/css/datetimepicker.css" />
    <link rel="stylesheet" type="text/css" href="<?php  echo base_url();?>user_assets/js/jquery-multi-select/css/multi-select.css" />
    <link rel="stylesheet" type="text/css" href="<?php  echo base_url();?>user_assets/js/jquery-tags-input/jquery.tagsinput.css" />
     <link href="https://cdnjs.cloudflare.com/ajax/libs/dropzone/4.0.1/min/dropzone.min.css" rel="stylesheet">
    <link rel="stylesheet" type="text/css" href="<?php  echo base_url();?>user_assets/js/select2/select2.css" />
   <link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.3.0/css/datepicker.min.css" />
   <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/rateYo/2.2.0/jquery.rateyo.min.css">

    <!-- Custom styles for this template -->
    <link href="<?php  echo base_url();?>user_assets/css/style.css" rel="stylesheet">
    <link href="<?php  echo base_url();?>user_assets/css/style-responsive.css" rel="stylesheet" />
    <!-- Css for smartWizard  -->
    
    <!-- JS file include -->
   
<!-- 
    <script type="text/javascript" src="<?php  echo base_url();?>user_assets/js/moment.js"></script> -->
   <script src="https://ajax.googleapis.com/ajax/libs/angularjs/1.5.6/angular.min.js"></script>
   <script src="https://cdnjs.cloudflare.com/ajax/libs/angular-ui-bootstrap/1.2.0/ui-bootstrap-tpls.js"></script>
  <script src="<?php echo base_url();?>user_assets/js/datetime-picker.js"></script>
 


    <!-- Just for debugging purposes. Don't actually copy this line! -->
    <!--[if lt IE 9]>
    <script src="js/ie8-responsive-file-warning.js"></script><![endif]-->

    <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
    <script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
    <![endif]-->


 
<!-- <script src="http://ajax.googleapis.com/ajax/libs/angularjs/1.0.5/angular.min.js"></script>
 -->
</head>