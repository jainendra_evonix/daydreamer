<script>


			var postApp = angular.module('postApp', []);

			postApp.controller('postController', function ($scope, $http) {


			 $scope.user = {};
              
             


			 $scope.submitForm = function () {

                if ($scope.userEducation.$valid) {
               
                 
                            $http({
                                method: 'POST',
                                        dataType: 'json',
                                        url: '<?php echo base_url() ?>user/educationInfo',
                                        data: $scope.user, //forms user object
                                        headers: {'Content-Type': 'application/json'}
                                }).success(function (data) {
                                	
                              
                                if (data.status == 1) {
                        	     	$('.help-block').css('display','none');
                        	     	
                                	window.location = '<?php echo base_url();?>user/education';
                        } else if (data.status == 2)
                        {
                        	     window.location = '<?php echo base_url();?>user/education';
                                
                        } else if (data.status == 3)
                        {
                        	     window.location = '<?php echo base_url();?>user/education';
                                
                         }
                        else {
                        	//console.log('dataaa')
                        	//console.log(data.error.competitionname.error)
                                $scope.instNameError = data.error.instName.error;
                                $scope.fromYearError = data.error.fromYear.error;
                                $scope.fromMonthError = data.error.fromMonth.error;
                                $scope.toYearError = data.error.toYear.error;
                                $scope.toMonthError = data.error.toMonth.error;
                                $scope.studyingError = data.error.studying.error;
                                $scope.courseStudyError = data.error.courseStudy.error;
                                $scope.qualificationTypeError = data.error.qualificationType.error;
                                $scope.gradeError = data.error.grade.error;
                                $scope.achievedError = data.error.achieved.error;
                               

                        } 	
                        
                        });

                   
                }
			     };

    







     $scope.editUser = {};


              /*$scope.editUser.einstName='<?php echo !empty($userEducationDetails) ? $userEducationDetails->institute_name :'' ?>';
              $scope.editUser.efromYear='<?php echo !empty($userEducationDetails) ? $userEducationDetails->from_year :'' ?>';
              $scope.editUser.efromMonth='<?php echo !empty($userEducationDetails) ? $userEducationDetails->from_month :'' ?>';
              $scope.editUser.etoYear='<?php echo !empty($userEducationDetails) ? $userEducationDetails->to_year :'' ?>';
              $scope.editUser.etoMonth='<?php echo !empty($userEducationDetails) ? $userEducationDetails->to_month :'' ?>';
              $scope.editUser.ecourseStudy='<?php echo !empty($userEducationDetails) ? $userEducationDetails->course :'' ?>'; 
              $scope.editUser.equalificationType='<?php echo !empty($userEducationDetails) ? $userEducationDetails->qualification_type :'' ?>';  
              $scope.editUser.egrade='<?php echo !empty($userEducationDetails) ? $userEducationDetails->grade :'' ?>';
              $scope.editUser.eachieved='<?php echo !empty($userEducationDetails) ? $userEducationDetails->achieved :'' ?>';*/
              

  $scope.submitEditForm = function () {

        if ($scope.editUserEducation.$valid) {

            

                     $http({
                                method: 'POST',
                                        dataType: 'json',
                                        url: '<?php echo base_url() ?>user/updateEducationInfo',
                                        data: $scope.editUser, //forms user object
                                        headers: {'Content-Type': 'application/json'}
                                }).success(function (data) {
                                	
                              
                                if (data.status == 1) {
                        	     	$('.help-block').css('display','none');
                        	     	
                                	window.location = '<?php echo base_url();?>user/education';
                        } else if (data.status == 2)
                        {
                        	     window.location = '<?php echo base_url();?>user/education';
                                
                        } else if (data.status == 3)
                        {
                        	     window.location = '<?php echo base_url();?>user/education';
                                
                         }
                        else {
                        	//console.log('dataaa')
                        	//console.log(data.error.competitionname.error)
                                $scope.einstNameError = data.error.einstName.error;
                                $scope.efromYearError = data.error.efromYear.error;
                                $scope.efromMonthError = data.error.efromMonth.error;
                                $scope.etoYearError = data.error.etoYear.error;
                                $scope.etoMonthError = data.error.etoMonth.error;
                                $scope.estudyingError = data.error.estudying.error;
                                $scope.ecourseStudyError = data.error.ecourseStudy.error;
                                $scope.equalificationTypeError = data.error.equalificationType.error;
                                $scope.egradeError = data.error.egrade.error;
                                $scope.eachievedError = data.error.eachieved.error;
                               

                        } 	
                        
                        });



        }

   };



 $scope.editEducation=function(id){

      //alert(id);


                            $http({
                                method: 'POST',
                                        dataType: 'json',
                                        url: '<?php echo base_url() ?>user/getUserEducationInfo',
                                        data: {id:id}, //forms user object
                                        headers: {'Content-Type': 'application/json'}
                                }).success(function (data) {
                                    
                               //alert(data.userEductionDetail.institute_name);
                               $scope.editUser = {

                               
                               'einstName':data.userEducationDetail.institute_name,
                               'efromYear':data.userEducationDetail.from_year,
                               'efromMonth':data.userEducationDetail.from_month,
                               'etoYear':data.userEducationDetail.to_year,
                               'etoMonth':data.userEducationDetail.to_month,
                               'ecurrStatus':data.userEducationDetail.studying,
                               'ecourseStudy':data.userEducationDetail.course,
                               'equalificationType':data.userEducationDetail.qualification_type,
                               'egrade':data.userEducationDetail.grade,
                               'eachieved':data.userEducationDetail.achieved,
                               'hiddenId':data.userEducationDetail.id,
                             

                               }
                          
             

                          });

     



 }

 /*$scope.deleteEducation=function(id)
		 {

         
             
             $http({
                                method: 'POST',
                                        dataType: 'json',
                                        url: '<?php echo base_url() ?>user/deleteUserEducationInfo',
                                        data: {id:id}, //forms user object
                                        headers: {'Content-Type': 'application/json'}
                                }).success(function (data) {
                                    
                               //alert(data.userEductionDetail.institute_name);
                              window.location = '<?php echo base_url();?>user/education';
                          
             

                          });

		 }

*/


    $scope.deleteEducation= function (id){
     if (confirm("Are you sure?")) {


             $http({
                                method: 'POST',
                                        dataType: 'json',
                                        url: '<?php echo base_url() ?>user/deleteUserEducationInfo',
                                        data: {id:id}, //forms user object
                                        headers: {'Content-Type': 'application/json'}
                                }).success(function (data) {
                                    //alert("deleted");
                               //alert(data.userEductionDetail.institute_name);
                              window.location = '<?php echo base_url();?>user/education';
                          
             

                          });


        
    }
    
     }
      
/*$scope.checkEducation=function()
{
  alert(234)
 var $scope.edu = "<?php echo  $userEducation->id; ?>";
   
   alert(edu);

   if(edu!=null){

   	  alert('notEmpty')

   } else{

   	alert('empty');
   }
   

}*/

});


</script>
<script>
  


function checkFromDate()
{
 
 var fromMon   =  $('#fromMonth').val();
 var fromYear= $('#fromYear').val();
 var toMon = $('#toMonth').val();
 var toYear= $('#toYear').val();
 

 
  if (fromYear > toYear) {
                alert("To date should be greater then from date");
                $('#toYear').val(" ");
                return;
            }
            else if (fromYear == toYear) {
                if (fromMon > toMon) {
                    alert("To date should be greater then from date");
                   $('#toMon').val(" ");
                    return;
                }
            }
 



}


function checkToDate()
{


 var fromMon   =  $('#fromMonth').val();
 var fromYear= $('#fromYear').val();
 var toMon = $('#toMonth').val();
 var toYear= $('#toYear').val();
 
 //alert(fromMon+" "+fromYear+" "+toMon+ " "+toYear);

 
 
  if (fromYear > toYear) {
                alert("To date should be greater then from date");
                $('#toYear').val(" ");
                return;
            }
            else if (fromYear == toYear) {
                if (fromMon > toMon) {
                    alert("To date should be greater then from date");
                    $('#toYear').val(" ");
                    return;
                }
            }
}


function checkFromMonth()
{


 var fromMon   =  $('#fromMonth').val();
 var fromYear= $('#fromYear').val();
 var toMon = $('#toMonth').val();
 var toYear= $('#toYear').val();
// alert(fromMon+" "+fromYear+" "+toMon+ " "+toYear);

 
 
  if (fromYear > toYear) {
                alert("To date should be greater then from date");
               // $('#fromMonth').val("");
                return;
            }
            else if (fromYear == toYear) {
                if (fromMon > toMon) {
                    alert("To date should be greater then from date");
                  //  $('#fromMonth').val(" ");
                    return;
                }
            }
}

function checkToMonth()
{


 var fromMon   =  $('#fromMonth').val();
 var fromYear= $('#fromYear').val();
 var toMon = $('#toMonth').val();
 var toYear= $('#toYear').val();
 
  // alert(fromMon+" "+fromYear+" "+toMon+ " "+toYear);

 
 
  if (fromYear > toYear) {
                alert("To date should be greater then from date");

              //  $('#toMonth').val('');
                return;
            }
            else if (fromYear == toYear) {
                if (fromMon > toMon) {
                    alert("To date should be greater then from date");
                    $('#toMonth').val(' ');
                    return;
                }
            }
}





function checkFromDateE()
{
 
 var fromMon   =  $('#efromMonth').val();
 var fromYear= $('#efromYear').val();
 var toMon = $('#etoMonth').val();
 var toYear= $('#etoYear').val();
 

 
  if (fromYear > toYear) {
                alert("To date should be greater then from date");
                $('#etoYear').val(" ");
                return;
            }
            else if (fromYear == toYear) {
                if (fromMon > toMon) {
                    alert("To date should be greater then from date");
                   $('#etoMonth').val(" ");
                    return;
                }
            }
 



}



function checkToDateE()
{


 var fromMon   =  $('#efromMonth').val();
 var fromYear= $('#efromYear').val();
 var toMon = $('#etoMonth').val();
 var toYear= $('#etoYear').val();
 
 //alert(fromMon+" "+fromYear+" "+toMon+ " "+toYear);

 
 
  if (fromYear > toYear) {
                alert("To date should be greater then from date");
                $('#etoYear').val(" ");
                return;
            }
            else if (fromYear == toYear) {
                if (fromMon > toMon) {
                    alert("To date should be greater then from date");
                    $('#etoYear').val(" ");
                    return;
                }
            }
}




function checkFromMonthE()
{

//alert();

 var fromMon   =  $('#efromMonth').val();
 var fromYear= $('#efromYear').val();
 var toMon = $('#etoMonth').val();
 var toYear= $('#etoYear').val();
// alert(fromMon+" "+fromYear+" "+toMon+ " "+toYear);

 
 
  if (fromYear > toYear) {
                alert("To date should be greater then from date");
               // $('#fromMonth').val("");
                return;
            }
            else if (fromYear == toYear) {
                if (fromMon > toMon) {
                    alert("To date should be greater then from date");
                    $('#efromMonth').val(" ");
                    return;
                }
            }
}




function checkToMonthE()
{


 var fromMon   =  $('#efromMonth').val();
 var fromYear= $('#efromYear').val();
 var toMon = $('#etoMonth').val();
 var toYear= $('#etoYear').val();
 
  // alert(fromMon+" "+fromYear+" "+toMon+ " "+toYear);

 
 
  if (fromYear > toYear) {
                alert("To date should be greater then from date");

              //  $('#toMonth').val('');
                return;
            }
            else if (fromYear == toYear) {
                if (fromMon > toMon) {
                    alert("To date should be greater then from date");
                    $('#etoMonth').val(' ');
                    return;
                }
            }
}



</script>
