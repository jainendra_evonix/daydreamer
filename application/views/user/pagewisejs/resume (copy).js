<script src="<?php echo base_url();?>user_assets/js/angular-file-upload.min.js"></script>
<script>
	

   var postApp = angular.module('postApp',['angularFileUpload']);

   postApp.controller('postController', ['$scope', 'FileUploader', '$http', function ($scope, FileUploader, $http) {

    



     $scope.uResume = {};

     $scope.submitUserResume = function(){
      //alert(12)

      /* console.log(scope.userEmployment);
        console.log(scope.userPreEmployment);*/
          $scope.uResume.covrLetter = $('#covrLetter').val();
          $scope.uResume.resumeUser = $('#resumeUser').val();
     
        if($scope.userResume.$valid){
             
		           //  alert('valid');


		               $http({
                                method: 'POST',
                                        dataType: 'json',
                                        url: '<?php echo base_url() ?>user/userResumeInfo',
                                        data: $scope.uResume, //forms user object
                                        headers: {'Content-Type': 'application/json'}
                                }).success(function (data) {
                                	
                              
                                if (data.status == 1) {
                        	     	$('.help-block').css('display','none');
                        	     	
                                	window.location = '<?php echo base_url();?>user/reference';
                        } else if (data.status == 2)
                        {
                        	     window.location = '<?php echo base_url();?>user/resume';
                                
                        } else{

                $scope.covrLetterError = data.error.covrLetter.error;
                $scope.resumeTitleError = data.error.resumeTitle.error;
                $scope.resumeUserError = data.error.resumeUser.error;
                
                
             }
            });

                




        }
       
       // alert('not valid')

     };


     $scope.uploadFile = function(event){
  var filename = event.target.files[0].name;
        //alert('file was selected: ' + filename);
        var t = filename.substr(0, filename.lastIndexOf("."));
//alert(t);
        var replaced = t.replace(/\./g,'_');
        //alert(replaced);
  $("#covrLetter").val(replaced+'.pdf');
       
    };
    

     $scope.euploadFile = function(event){
  var filename = event.target.files[0].name;
        //alert('file was selected: ' + filename);
        var t = filename.substr(0, filename.lastIndexOf("."));
//alert(t);
                file_name = fileItem.file.name;
                file_ext = file_name.split('.').pop();
        var replaced = t.replace(/\./g,'_');
        //alert(replaced);
  $("#resumeUser").val(replaced+'.'+file_ext);
       
    };



    
     var sub = $scope.uploader2 = new FileUploader({
      //alert();
            url: '<?php echo base_url();?>user/uploadCoverLetter'
        });

        // FILTERS

        sub.filters.push({
            name: 'customFilter',
            fn: function(item /*{File|FileLikeObject}*/, options) {
                return this.queue.length < 10;
            }
        });
        
        sub.onSuccessItem = function (fileItem, response, status, headers) {

                var t = fileItem.file.name.substr(0, fileItem.file.name.lastIndexOf("."));
                
                file_name = fileItem.file.name;
                file_ext = file_name.split('.').pop();
              // alert(file_ext);

        var replaced = t.replace(/\./g,'_');
        
                var filesize = fileItem.file.size;

                //alert(filesize);

               
                if(filesize<=500*1000)
               {

                $('#succ').html('File uploaded');
                $("#covrLetter").val(replaced+'.'+file_ext);
               }
               else
               {
                $("#covrLetter").val('');
               }
                
            };


            // userResume upload 

            var sub = $scope.euploader2 = new FileUploader({
      //alert();
            url: '<?php echo base_url();?>user/uploadResume'
        });

        // FILTERS

        sub.filters.push({
            name: 'customFilter',
            fn: function(item /*{File|FileLikeObject}*/, options) {
                return this.queue.length < 10;
            }
        });
        
        sub.onSuccessItem = function (fileItem, response, status, headers) {

                var t = fileItem.file.name.substr(0, fileItem.file.name.lastIndexOf("."));
             
             file_name = fileItem.file.name;
                file_ext = file_name.split('.').pop();
        var replaced = t.replace(/\./g,'_');
        
                var filesize = fileItem.file.size;

                //alert(filesize);
               
                if(filesize<=500*1000)
               {

                $('#succ2').html('File uploaded');
                $("#resumeUser").val(replaced+'.'+file_ext);
               }
               else
               {
                $("#resumeUser").val('');
               }
                
            };


/* start code for add new profile (resume)*/


$scope.newUploadFile = function(event){
  var filename = event.target.files[0].name;
        alert('file was selected: ' + filename);
        var t = filename.substr(0, filename.lastIndexOf("."));
//alert(t);
        var replaced = t.replace(/\./g,'_');
        //alert(replaced);
  $("#neWcovrLetter").val(replaced+'.pdf');
       
    };
    

     $scope.newEuploadFile = function(event){
  var filename = event.target.files[0].name;
        alert('file was selected: ' + filename);
        var t = filename.substr(0, filename.lastIndexOf("."));
//alert(t);
                file_name = fileItem.file.name;
                file_ext = file_name.split('.').pop();
        var replaced = t.replace(/\./g,'_');
        //alert(replaced); NewUesumeUser
  $("#newResumeUser").val(replaced+'.'+file_ext);
       
    };



 var sub = $scope.newuploader2 = new FileUploader({
      //alert();
            url: '<?php echo base_url();?>user/newuploadCoverLetter'
        });

        // FILTERS

        sub.filters.push({
            name: 'customFilter',
            fn: function(item /*{File|FileLikeObject}*/, options) {
                return this.queue.length < 10;
            }
        });
        
        sub.onSuccessItem = function (fileItem, response, status, headers) {

                var t = fileItem.file.name.substr(0, fileItem.file.name.lastIndexOf("."));
                
                file_name = fileItem.file.name;
                file_ext = file_name.split('.').pop();
              // alert(file_ext);

        var replaced = t.replace(/\./g,'_');
        
                var filesize = fileItem.file.size;

                //alert(filesize);

               
                if(filesize<=500*1000)
               {

                $('#succc').html('File uploaded');
                $("#NewcovrLetter").val(replaced+'.'+file_ext);
               }
               else
               {
                $("#NewcovrLetter").val('');
               }
                
            };


            // userResume upload 

            var sub = $scope.neweuploader2 = new FileUploader({
         // alert();
            url: '<?php echo base_url();?>user/newuploadResume'
        });

        // FILTERS

        sub.filters.push({
            name: 'customFilter',
            fn: function(item /*{File|FileLikeObject}*/, options) {
                return this.queue.length < 10;
            }
        });
        
        sub.onSuccessItem = function (fileItem, response, status, headers) {

                var t = fileItem.file.name.substr(0, fileItem.file.name.lastIndexOf("."));
             
             file_name = fileItem.file.name;
                file_ext = file_name.split('.').pop();
        var replaced = t.replace(/\./g,'_');
        
                var filesize = fileItem.file.size;

                //alert(filesize);
               
                if(filesize<=500*1000)
               {

                $('#succc2').html('File uploaded');
                $("#resumeUser").val(replaced+'.'+file_ext);
               }
               else
               {
                $("#newResumeUser").val('');
               }
                
            };













/*end code add new  profile (resume)*/





 $scope.deleteResume= function (id){
     if (confirm("Are you sure?")) {


             $http({
                                method: 'POST',
                                        dataType: 'json',
                                        url: '<?php echo base_url() ?>user/deleteUserResume',
                                        data: {id:id}, //forms user object
                                        headers: {'Content-Type': 'application/json'}
                                }).success(function (data) {
                                    //alert("deleted");
                               //alert(data.userEductionDetail.institute_name);
                              window.location = '<?php echo base_url();?>user/resume';
                          
             

                          });


        
    }
    
     }
 



   	 }]);

   	 </script>