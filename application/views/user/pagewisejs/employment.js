    <script>
	


		var postApp = angular.module('postApp',[]);



		postApp.controller('postController',function($scope,$http){

          $scope.employmentDetail = {};


            $scope.employmentDetail.position='<?php echo !empty($userEmploymentDetail) ? $userEmploymentDetail->position_held :'' ?>';
            $scope.employmentDetail.cmpnyName='<?php echo !empty($userEmploymentDetail) ? $userEmploymentDetail->company_name :'' ?>';
            $scope.employmentDetail.yearFrom='<?php echo !empty($userEmploymentDetail) ? $userEmploymentDetail->from_year :'' ?>';
            $scope.employmentDetail.monthFrom='<?php echo !empty($userEmploymentDetail) ? $userEmploymentDetail->from_month :'' ?>';
            $scope.employmentDetail.dateTo='<?php echo !empty($userEmploymentDetail) ? $userEmploymentDetail->to_year :'' ?>';
            $scope.employmentDetail.toMonth='<?php echo !empty($userEmploymentDetail) ? $userEmploymentDetail->to_month :'' ?>';
            $scope.employmentDetail.userCity='<?php echo !empty($userEmploymentDetail) ? $userEmploymentDetail->location_city :'' ?>';
            $scope.employmentDetail.userCountry='<?php echo !empty($userEmploymentDetail) ? $userEmploymentDetail->location_country :'' ?>';
            $scope.employmentDetail.role='<?php echo !empty($userEmploymentDetail) ? $userEmploymentDetail->purpose_of_role :'' ?>';
            $scope.employmentDetail.respn=`<?php echo !empty($userEmploymentDetail) ? $userEmploymentDetail->rspnsibilities_achvmnts :'' ?>`;
            $scope.employmentDetail.training='<?php echo !empty($userEmploymentDetail) ? $userEmploymentDetail->internal_training :'' ?>';




           $scope.submitForm = function(){

                      

                      if($scope.userEmployment.$valid){
             
		             
		               $http({
                                method: 'POST',
                                        dataType: 'json',
                                        url: '<?php echo base_url() ?>user/employmentInfo',
                                        data: $scope.employmentDetail, //forms user object
                                        headers: {'Content-Type': 'application/json'}
                                }).success(function (data) {
                                	
                              
                                if (data.status == 1) {
                        	     	$('.help-block').css('display','none');
                        	     	
                                	window.location = '<?php echo base_url();?>user/resume';
                        } else if (data.status == 2)
                        {
                        	     window.location = '<?php echo base_url();?>user/resume';
                                
                        } else if (data.status == 3)
                        {
                        	     window.location = '<?php echo base_url();?>user/employment';


                    }else{

                $scope.cEmpError = data.error.cEmp.error;
                $scope.salaryError = data.error.salary.error;
                $scope.dIncomeError = data.error.dIncome.error;
                $scope.crentactError = data.error.crentact.error;
                $scope.addiInstrctnError = data.error.addiInstrctn.error;
                $scope.charitywrkError = data.error.charitywrk.error;
                $scope.assCharityError = data.error.assCharity.error;
                $scope.organisationsError = data.error.organisations.error;
                
             }
            });

                




        }


           };



		});


</script>

<script>

function checkFromMonth()
{


 var fromMon   =  $('#fromMonth').val();
 var fromYear= $('#fromYear').val();
 var toMon = $('#toMonth').val();
 var toYear= $('#toYear').val();
// alert(fromMon+" "+fromYear+" "+toMon+ " "+toYear);

 
 
  if (toYear > fromYear) {

              // alert("To date should be greater then from date");
                $('#toYear').val(" ");
                return;
            }
            else if (fromYear == toYear) {
                if (fromMon > toMon) {
                     alert("To date should be greater than from date");
                    $('#toMonth').val(" ");
                    return;
                }
            }
}


function checkFromDate()
{
 
 var fromMon   =  $('#fromMonth').val();
 var fromYear= $('#fromYear').val();
 var toMon = $('#toMonth').val();
 var toYear= $('#toYear').val();
 

 
  if (fromYear > toYear ) {

               // alert("Please select to month");
                $('#toYear').val(" ");
                return;
            }
            else if (fromYear == toYear) {
                if (fromMon > toMon) {
                 //   alert("Please select greater to month from");
                   $('#toMon').val(" ");
                    return;
                }
            }
 



}


function checkToDate()
{


 var fromMon   =  $('#fromMonth').val();
 var fromYear= $('#fromYear').val();
 var toMon = $('#toMonth').val();
 var toYear= $('#toYear').val();
 
 //alert(fromMon+" "+fromYear+" "+toMon+ " "+toYear);

 
 
  if (toYear > fromYear ) {
                alert("To date should be greater than from date");
                $('#toYear').val(" ");
                return;
            }
            else if (fromYear == toYear) {
                if (fromMon > toMon) {
                    alert("To date should be greater than from date");
                    $('#toYear').val(" ");
                    return;
                }
            }
}


function checkToMonth()
{


 var fromMon   =  $('#fromMonth').val();
 var fromYear= $('#fromYear').val();
 var toMon = $('#toMonth').val();
 var toYear= $('#toYear').val();
 
  // alert(fromMon+" "+fromYear+" "+toMon+ " "+toYear);

 
 
  if ( toYear > fromYear ) {
               // alert("To date should be greater then from date");

                $('#toMonth').val(' ');
                return;
            }
            else if (fromYear == toYear) {
                if (fromMon > toMon) {
                  //  alert("To date should be greater then from date");
                    $('#toMonth').val(' ');
                    return;
                }
            }
}



</script>