<script>

  
     var postApp = angular.module('postApp', []);


     postApp.controller('postController', function ($scope, $http) {


           $scope.userReferenceDetail = {};

           $scope.userReferenceDetail.userRefence='<?php echo !empty($savedUserReferences) ? $savedUserReferences->type_of_reference :'' ?>';
           $scope.userReferenceDetail.bDetail='<?php echo !empty($savedUserReferences) ? $savedUserReferences->business_details_main_contact :'' ?>';
           $scope.userReferenceDetail.addLine1='<?php echo !empty($savedUserReferences) ? $savedUserReferences->address1 :'' ?>';
           $scope.userReferenceDetail.addLine2='<?php echo !empty($savedUserReferences) ? $savedUserReferences->address2 :'' ?>';
           $scope.userReferenceDetail.addLine3='<?php echo !empty($savedUserReferences) ? $savedUserReferences->address3 :'' ?>';
           $scope.userReferenceDetail.city='<?php echo !empty($savedUserReferences) ? $savedUserReferences->city :'' ?>';
           $scope.userReferenceDetail.country='<?php echo !empty($savedUserReferences) ? $savedUserReferences->country :'' ?>';
           $scope.userReferenceDetail.postcode='<?php echo !empty($savedUserReferences) ? $savedUserReferences->post_code :'' ?>';
           
           $scope.userReferenceDetail.email='<?php echo !empty($savedUserReferences) ? $savedUserReferences->email :'' ?>';
           $scope.userReferenceDetail.telephone='<?php echo !empty($savedUserReferences) ? $savedUserReferences->telephone :'' ?>';
           $scope.userReferenceDetail.fax='<?php echo !empty($savedUserReferences) ? $savedUserReferences->fax :'' ?>';
          


        
           $scope.submitForm=function(){
           
            if($scope.userReference.$valid){

              // alert('valid')

               

                            $http({
                                method: 'POST',
                                        dataType: 'json',
                                        url: '<?php echo base_url() ?>user/referenceInfo',
                                        data: $scope.userReferenceDetail, //forms user object
                                        headers: {'Content-Type': 'application/json'}
                                }).success(function (data) {
                                	
                              
                                if (data.status == 1) {
                        	     	$('.help-block').css('display','none');
                        	     	
                                	window.location = '<?php echo base_url();?>user/additionalInfo';
                        } else if (data.status == 2)
                        {
                        	     window.location = '<?php echo base_url();?>user/reference';
                                
                        } else if (data.status == 3)
                        {
                        	     window.location = '<?php echo base_url();?>user/reference';
                                
                         }
                        else {
                        	//console.log('dataaa')
                        	//console.log(data.error.competitionname.error)
                                $scope.userRefenceError = data.error.userRefence.error;
                                $scope.bDetailError = data.error.bDetail.error;
                                $scope.addLine1Error = data.error.addLine1.error;
                                $scope.addLine2Error = data.error.addLine2.error;
                                $scope.addLine3Error = data.error.addLine3.error;
                                $scope.cityError = data.error.city.error;
                                $scope.countryError = data.error.country.error;
                                $scope.postcodeError = data.error.postcode.error;
                                $scope.emailError = data.error.email.error;
                                $scope.telephoneError = data.error.telephone.error;
                                $scope.faxError = data.error.fax.error;
                               

                        } 	
                        
                        });



            }


           };



     }) ;



</script>
