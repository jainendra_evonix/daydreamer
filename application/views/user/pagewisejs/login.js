    <!--for login-->
    <script>
                // Defining angularjs application.
                var postApp = angular.module('postApp', []);
                // Controller function and passing $http service and $scope var.
                postApp.controller('postController', function ($scope, $http) {

                // create a blank object to handle form data.
                $scope.user = {};
                        // calling our submit function.
                        $scope.submitForm = function () { 
    
                        // Posting data to php file
                        if ($scope.userLogin.$valid) {
                   //alert('login');
                                $http({
                                method: 'POST',
                                        dataType: 'json',
                                        url: '<?php echo base_url() ?>user/checklogin',
                                        data: $scope.user, //forms user object
                                        headers: {'Content-Type': 'application/json'}
                                }).success(function (data) {
                                  //alert(data.status);
                                  //alert(data.message);
                        //console.log(data);
                                // console.log(data.error.password.error);
                                if (data.status == 1) {
                       
                        window.location = '<?php echo base_url();?>user/profile';
                        } else if (data.status == 0)
                        {
                               $('#message').html('');
                               $('#message').append(data.message);
                               $scope.emailaddressError='';
                                /*$('#message').append('<div class="alert alert-danger"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button><b>Invalid credentials!</b> Wrong email id or password !.</div>');*/
                                
                        } else if (data.status == 3)
                        {
                               $('#message').html('');
                                $('#message').append('<div class="alert alert-danger"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>Email id and password are required !.</div>');
                                
                         } else if (data.status == 4)
                        {

                               window.location = '<?php echo base_url();?>user/dashboard';
                              // $('#message').html('');
                              // $('#message').append(data.message);
                                /*$('#message').append('<div class="alert alert-danger"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button><b>Invalid credentials!</b> Wrong email id or password !.</div>');*/
                                
                        }
                        else {
                              //$('#message').html('');
                                $scope.emailaddressError = data.error.emailaddress.error;
                                $scope.passwordError = data.error.password.error;
                                //alert($scope.emailaddressError);
                                /*if ($scope.emailaddressError) {
                                $scope.emailaddressError='';
                             }*/
                             $("#emailaddress").keyup(function(){
                        //alert($scope.emailaddressError);
                        $scope.emailaddressError='';
                      });
                      
                      $("#password").keyup(function(){
                        $scope.passwordError='';
                      });
                        }
                        });
                        } 
                        };
              

          <!--for forgotpassword-->

                $scope.forgotpassword = {};
                        $scope.submitForgotPasswordForm = function () {
    
                        if ($scope.userForgotPassword.$valid) {
                  // alert('fp');
                                $http({
                                method: 'POST',
                                        dataType: 'json',
                                        url: '<?php echo base_url() ?>user/check_forgot_password',
                                        data: $scope.forgotpassword,
                                        headers: {'Content-Type': 'application/json'}
                                }).success(function (data) {
                                  //alert(data.status);
                                if (data.status == 1) {
                        //alert('ok');
                        $('#message2').html('');
                               $('#message2').append(data.message);
                               $('#email').val("");
                               /*setTimeout(function(){
                                window.location = '<?php echo base_url();?>';
                                }, 7000);*/
                                $scope.forgotpassword.email='';
                                window.location = '<?php echo base_url();?>user/index';
                                $('.help-block').css('display','none');
                        } else if (data.status == 0)
                        {
                               $('#message2').html('');
                               $('#message2').append(data.message);
                                
                        } else if (data.status == 3)
                        {
                               $('#message2').html('');
                                $('#message2').append('<div class="alert alert-danger"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>Email id is required !.</div>');
                                
                        } else if (data.status == 4)
                        {
                               $('#message2').html('');
                               $('#message2').append(data.message);
                                
                        }
                        else {
                                $scope.emailError = data.error.email.error;
                                $("#email").keyup(function(){
                        //alert($scope.emailError);
                        $scope.emailError='';
                      });
                        }
                        });
                        }
                        };
             });

    </script>