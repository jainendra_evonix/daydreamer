<script>
	
	var postApp = angular.module('postApp',[]);

	postApp.controller('postController',function($scope,$http){

      
    
     $scope.userBenefits={};

      $scope.submitForm = function()
      {

	         
	        if($scope.userRecieptBenefits.$valid)
	        {

               $http({
                                method: 'POST',
                                        dataType: 'json',
                                        url: '<?php echo base_url() ?>user/userBenefitsInfo',
                                        data: $scope.userBenefits, //forms user object
                                        headers: {'Content-Type': 'application/json'}
                                }).success(function (data) {
                                	
                              
                                if (data.status == 1) {
                        	     	$('.help-block').css('display','none');
                        	     	
                               	window.location = '<?php echo base_url();?>user/resume';
                        } else if (data.status == 2)
                        {
                        	    window.location = '<?php echo base_url();?>user/recieptBenefits';
                                
                        } else if (data.status == 3)
                        {
                        	    window.location = '<?php echo base_url();?>user/recieptBenefits';


                    }else{

				                $scope.benefitsrError = data.error.benefitsr.error;
				                $scope.benefitsTypeError = data.error.benefitsType.error;
				                $scope.otherbenefitsError = data.error.otherbenefits.error;
				                $scope.benefitsFreqError = data.error.benefitsFreq.error;
				                $scope.amountError = data.error.amount.error;
				                $scope.currencyError = data.error.currency.error;
				                $scope.fdayError = data.error.fday.error;
				                $scope.fmonthError = data.error.fmonth.error;
				                $scope.fyearError = data.error.fyear.error;
			                    $scope.tdayError = data.error.tday.error;
			                    $scope.tmonthError = data.error.tmonth.error;
			                    $scope.tyearError = data.error.tyear.error;
			                    
                
             }
            });


	        }


      };



 //  

     $scope.editUserBenefit=function(id)
     {

        // alert(id);

        $http({
              
              method:'POST',
              dataType:'json',
              url:'<?php echo base_url(); ?>user/getUserBenefits',
              data:{id:id},
              headers:{'Content-Type':'application/json'}
             }).success(function(data){
             // alert(data.userSavedBenefitsInfo)
             	$scope.userEditBenefits = {

                      'eid':data.benefitsInfo.id,
                      'ebenefitsType':data.benefitsInfo.benefit_type,
                      'eotherbenefits':data.benefitsInfo.other_benefit,
                      'ebenefitsFreq':data.benefitsInfo.benefit_frequency,
                      'eamount':data.benefitsInfo.amount,
                      'ecurrency':data.benefitsInfo.currency,
                      'efday':data.benefitsInfo.reciept_f_day,
                      'efmonth':data.benefitsInfo.reciept_f_mnth,
                      'efyear':data.benefitsInfo.reciept_f_yr,
                      'etday':data.benefitsInfo.reciept_t_day,
                      'etmonth':data.benefitsInfo.reciept_t_mnth,
                      'etyear':data.benefitsInfo.reciept_t_yr
                        


             	}

             	 if(data.benefitsInfo.receiving==1) {
                         //alert('here');
                          $('#ereceiving').prop('checked', true);
                          $('#showdiv').hide();
                           }
                          else{ 
                          //alert('there');
                           $('#ereceiving').prop('checked', false); 
                           $('#showdiv').show();
                       }

                       if(data.benefitsInfo.benefit_type==7){

                       	 $('#otherBene').show();


                       }else{

                       	 $('#otherBene').hide();
                       }

             }); 


     }



 
  // function delete user benefit

  $scope.delUserBenefits=function(id)
  {
     
    // alert('as'+' '+id);


        if (confirm("Are you sure?")) {


             $http({
                                method: 'POST',
                                        dataType: 'json',
                                        url: '<?php echo base_url() ?>user/deleteUserBenefit',
                                        data: {id:id}, //forms user object
                                        headers: {'Content-Type': 'application/json'}
                                }).success(function (data) {
                                    //alert("deleted");
                               //alert(data.userEductionDetail.institute_name);
                              window.location = '<?php echo base_url();?>user/recieptBenefits';
                          
             

                          });


        
    }

  }




 $scope.editUserBenefits={};


 $scope.submitEditBenefits=function()
 {
    

      if($scope.editUserBenefits.$valid)
	        {

               $http({
                                method: 'POST',
                                        dataType: 'json',
                                        url: '<?php echo base_url() ?>user/replaceUserBenefitsInfo',
                                        data: $scope.userEditBenefits, //forms user object
                                        headers: {'Content-Type': 'application/json'}
                                }).success(function (data) {
                                	
                              
                                if (data.status == 1) {
                        	     	$('.help-block').css('display','none');
                        	     	
                               	window.location = '<?php echo base_url();?>user/recieptBenefits';
                        } else if (data.status == 2)
                        {
                        	    window.location = '<?php echo base_url();?>user/recieptBenefits';
                                
                        } else if (data.status == 3)
                        {
                        	    window.location = '<?php echo base_url();?>user/recieptBenefits';


                    }else{

				                $scope.eidError = data.error.eid.error;
				                $scope.ebenefitsTypeError = data.error.ebenefitsType.error;
				                $scope.eotherbenefitsError = data.error.eotherbenefits.error;
				                $scope.ebenefitsFreqError = data.error.ebenefitsFreq.error;
				                $scope.eamountError = data.error.eamount.error;
				                $scope.ecurrencyError = data.error.ecurrency.error;
				                $scope.efdayError = data.error.efday.error;
				                $scope.efmonthError = data.error.efmonth.error;
				                $scope.efyearError = data.error.efyear.error;
			                    $scope.etdayError = data.error.etday.error;
			                    $scope.etmonthError = data.error.etmonth.error;
			                    $scope.etyearError = data.error.etyear.error;
			                    
               
               }

            });


	        }

 };



	});

</script>


 <script>
 	
    $(document).ready(function(){
     
      $("#otherBenefits").hide();
      $("#otherBene").hide();

    });

 
 function removeReq()
 {
   
   alert('ghj')
   $(':input').removeAttr('required');

 }


 </script>

<script>  
  
    
 jQuery("#benefits").change(function(){



   var selectValue = jQuery("#benefits").val();

        //  alert(selectValue);

         if(selectValue == "7")
         {

           $("#otherBenefits").show();

         }else{

          $("#otherBenefits").hide();
        }
      });


jQuery('#ebenefits').change(function()
{
   // alert()

   var selectValue = jQuery("#ebenefits").val();
   if(selectValue == "7")
         {

           $("#otherBene").show();

         }else{

          $("#otherBene").hide();
        }
});




</script>