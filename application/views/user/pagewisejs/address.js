<script>
	
   var postApp = angular.module('postApp',[]);

   postApp.controller('postController',function($scope,$http){



   	 $scope.userAddressModel = {};


   	  $scope.submitForm = function()
   	  {

   	  	if($scope.userAddress.$valid)
   	  	{

            $http({
                
                method:'POST',
                dataType:'json',
                url:'<?php echo base_url() ?>user/addressInfo',
                data:$scope.userAddressModel,
                headers:{'Content-Type': 'application/json'}
               }).success(function(data){

               	  if (data.status == 1) {
                        	     	$('.help-block').css('display','none');
                        	     	
                                	window.location = '<?php echo base_url();?>user/education';
                        } else if (data.status == 2)
                        {
                        	     window.location = '<?php echo base_url();?>user/address';
                                
                        } else if (data.status == 3)
                        {
                        	     window.location = '<?php echo base_url();?>user/preEmployment';
                                
                         }
                        else {
                        	//console.log('dataaa')
                        	//console.log(data.error.competitionname.error)
                                $scope.addLine1Error = data.error.addLine1.error;
                                $scope.addLine2Error = data.error.addLine2.error;
                                $scope.addLine3Error = data.error.addLine3.error;
                                $scope.countryError = data.error.country.error;
                                $scope.cityError = data.error.city.error;
                                $scope.dayError = data.error.day.error;
                                $scope.monthError = data.error.month.error;
                                $scope.yearError = data.error.year.error;
                                $scope.statusError = data.error.status.error;
                                $scope.achievedError = data.error.achieved.error;
                               

                        } 	

               });


   	  	}

   	  };


   
   $scope.editAddress=function(id)
   { 

   	// alert(id)



        $http({
              
              method:'POST',
              dataType:'json',
              url:'<?php echo base_url(); ?>user/getUserAddresses',
              data:{id:id},
              headers:{'Content-Type':'application/json'}
             }).success(function(data){
             // alert(data.userSavedBenefitsInfo)
             	$scope.euserAddressModel = {

                      'hiddenId':data.userAddresses.id,
                      'eaddLine1':data.userAddresses.add_l_1,
                      'eaddLine2':data.userAddresses.add_l_2,
                      'eaddLine3':data.userAddresses.add_l_3,
                      'ecountry':data.userAddresses.country,
                      'ecity':data.userAddresses.city,
                      'eday':data.userAddresses.resident_f_d,
                      'emonth':data.userAddresses.resident_f_m,
                      'eyear':data.userAddresses.resident_f_y,
                      'estatus':data.userAddresses.status,
                      'eactAdd':data.userAddresses.addAct,
                      
                        


             	}

             	 if(data.benefitsInfo.receiving==1) {
                         //alert('here');
                          $('#ereceiving').prop('checked', true);
                          $('#showdiv').hide();
                           }
                          else{ 
                          //alert('there');
                           $('#ereceiving').prop('checked', false); 
                           $('#showdiv').show();
                       }

                       if(data.benefitsInfo.benefit_type==7){

                       	 $('#otherBene').show();


                       }else{

                       	 $('#otherBene').hide();
                       }

             }); 





   };



     $scope.euserAddressModel={};


      $scope.submitEditForm=function()
      {

          if ($scope.editUserAddress.$valid) {

            

                     $http({
                                method: 'POST',
                                        dataType: 'json',
                                        url: '<?php echo base_url() ?>user/updateAddressInfo',
                                        data: $scope.euserAddressModel, //forms user object
                                        headers: {'Content-Type': 'application/json'}
                                }).success(function (data) {
                                	
                              
                                if (data.status == 1) {
                        	     	$('.help-block').css('display','none');
                        	     	
                                	window.location = '<?php echo base_url();?>user/address';
                        } else if (data.status == 2)
                        {
                        	     window.location = '<?php echo base_url();?>user/education';
                                
                        } else if (data.status == 3)
                        {
                        	     window.location = '<?php echo base_url();?>user/education';
                                
                         }
                        else {
                        	//console.log('dataaa')
                        	//console.log(data.error.competitionname.error)
                                $scope.eaddLine1Error = data.error.eaddLine1.error;
                                $scope.eaddLine2Error = data.error.eaddLine2.error;
                                $scope.eaddLine3Error = data.error.eaddLine3.error;
                                $scope.ecountryError = data.error.ecountry.error;
                                $scope.ecityError = data.error.ecity.error;
                                $scope.edayError = data.error.eday.error;
                                $scope.emonthError = data.error.emonth.error;
                                $scope.eyearError = data.error.eyear.error;
                                $scope.estatusError = data.error.estatus.error;
                                $scope.eactAddError = data.error.eactAdd.error;
                               

                        } 	
                        
                        });



        }

          

      };
  


  $scope.deleteAddress=function(id)
  {

    if (confirm("Are you sure?")) {


             $http({
                                method: 'POST',
                                        dataType: 'json',
                                        url: '<?php echo base_url() ?>user/deleteUserAddressInfo',
                                        data: {id:id}, //forms user object
                                        headers: {'Content-Type': 'application/json'}
                                }).success(function (data) {
                                    //alert("deleted");
                               //alert(data.userEductionDetail.institute_name);
                              window.location = '<?php echo base_url();?>user/address';
                          
             

                          });


        
    }
    


  }




   });
  


</script>

<script>
	


	function call()
      {
      
      //alert('wert');
      var    kcyear = document.getElementsByName("year")[0],
          kcmonth = document.getElementsByName("month")[0],
          kcday = document.getElementsByName("day")[0];
           
     var d = new Date();
     var n = d.getFullYear();
     for (var i = n; i >= 1950; i--) {
      var opt = new Option();
      opt.value = opt.text = i;
      kcyear.add(opt);
        }
     kcyear.addEventListener("change", validate_date);
     kcmonth.addEventListener("change", validate_date);

     function validate_date() {
     var y = +kcyear.value, m = kcmonth.value, d = kcday.value;
     if (m === "2")
         var mlength = 28 + (!(y & 3) && ((y % 100) !== 0 || !(y & 15)));
     else var mlength = [31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31][m - 1];
     kcday.length = 0;
     for (var i = 1; i <= mlength; i++) {
         var opt = new Option();
         opt.value = opt.text = i;
         if (i == d) opt.selected = true;
         kcday.add(opt);
     }
         }
        validate_date();
      }

  $(document).on('change','#my_multi_select3',function(){
    console.log('On change')
    var that = $(this);
    parent = that.parent('.ms-elem-selectable');
    console.log(parent)
    disparent = that.parent('.ms-elem-selection');
    console.log(disparent)
    console.log($(this).val())
  })
</script>