<script src="<?php echo base_url();?>user_assets/js/angular-file-upload.min.js"></script>
<script>
	

   var postApp = angular.module('postApp',['angularFileUpload']);


/*
postApp.directive('fileModel', function() {
  return {
    restrict: 'A',
    link: function (scope, element, attrs) {
      var onChangeFunc = scope.$eval(attrs.fileModel);
      element.bind('change', onChangeFunc);
    }
  };
});*/

 

 postApp.directive("selectJob", function ($timeout, $parse) {
            return {
            restrict: 'AC',
            link: function (scope, element, attrs) {
            var selectedVal = [];
            var options = [],

            el = $(element),
            angularTriggeredChange = false,
            selectOptions = attrs["selectOptions"];
            // Watch for changes to the select UI
            el.select2().on("change", function (e) {
            // if the user triggered the change, let angular know
            //console.log(e.target.value)
               // scope.tesmp = 
               selectedVal.push(e.target.value);
               console.log(scope)
              // console.log(scope.user)
            //console.log(selectedVal);
            scope.userPreEmployment.organisations = selectedVal;
            scope.userEmployment.organisations = selectedVal;
            // if angular triggered the change, then nothing to update
            angularTriggeredChange = false;
            });
            }
            };
            });

   


   postApp.directive("selectOrganisation", function ($timeout, $parse) {
            return {
            restrict: 'AC',
            link: function (scope, element, attrs) {
            var selectedVal = [];
            var options = [],

            el = $(element),
            angularTriggeredChange = false,
            selectOptions = attrs["selectOptions"];
            // Watch for changes to the select UI
            el.select2().on("change", function (e) {
            // if the user triggered the change, let angular know
            //console.log(e.target.value)
               // scope.tesmp = 
               selectedVal.push(e.target.value);
             console.log(e.val)
               //console.log(scope.eUserEmployment)
             // console.log(selectedVal);
            scope.editUserPreEmployment.eorganisations = e.val;
            scope.eUserEmployment.eorganisations = e.val;
            // if angular triggered the change, then nothing to update
            angularTriggeredChange = false;
            });
            }
            };
            });   

   postApp.controller('postController', ['$scope', 'FileUploader', '$http', function ($scope, FileUploader, $http) {

 

   
    $scope.userEmployment = {};

       
      /* $scope.userEmployment.cEmp='<?php echo !empty($PreEmploymentInfo) ? $PreEmploymentInfo->current_emplymnt_status :'' ?>';
       $scope.userEmployment.salary='<?php echo !empty($PreEmploymentInfo) ? $PreEmploymentInfo->present_income :'' ?>';
       $scope.userEmployment.dIncome='<?php echo !empty($PreEmploymentInfo) ? $PreEmploymentInfo->desired_income :'' ?>';
       $scope.userEmployment.crentact='<?php echo !empty($PreEmploymentInfo) ? $PreEmploymentInfo->currently_actively_applying_status :'' ?>';
       $scope.userEmployment.addiInstrctn='<?php echo !empty($PreEmploymentInfo) ? $PreEmploymentInfo->currently_actively_applying_desc :'' ?>';
       $scope.userEmployment.charitywrk='<?php echo !empty($PreEmploymentInfo) ? $PreEmploymentInfo->undertaken_voluntary_charity_work :'' ?>';
       $scope.userEmployment.assCharity='<?php echo !empty($PreEmploymentInfo) ? $PreEmploymentInfo->interested_in_volunteering_assisting_charities_status :'' ?>';
       $scope.userEmployment.id='<?php echo !empty($PreEmploymentInfo) ? $PreEmploymentInfo->id :'' ?>';*/




     $scope.submitForm = function(){

      /* console.log(scope.userEmployment);
        console.log(scope.userPreEmployment);*/
      //   $scope.userEmployment.pdf = $('#pdf').val();
     
        if($scope.userPreEmployment.$valid){
             
		           //  alert('valid');


		               $http({
                                method: 'POST',
                                        dataType: 'json',
                                        url: '<?php echo base_url() ?>user/preEmploymentInfo',
                                        data: $scope.userEmployment, //forms user object
                                        headers: {'Content-Type': 'application/json'}
                                }).success(function (data) {
                                	
                              
                                if (data.status == 1) {
                        	     	$('.help-block').css('display','none');
                        	     	
                               	window.location = '<?php echo base_url();?>user/recieptBenefits';
                        } else if (data.status == 2)
                        {
                        	    window.location = '<?php echo base_url();?>user/preEmployment';
                                
                        } else if (data.status == 3)
                        {
                        	    window.location = '<?php echo base_url();?>user/preEmployment';


                    }else{

                $scope.cEmpError = data.error.cEmp.error;
                $scope.salaryError = data.error.salary.error;
                $scope.dIncomeError = data.error.dIncome.error;
                $scope.crentactError = data.error.crentact.error;
                $scope.addiInstrctnError = data.error.addiInstrctn.error;
                $scope.charitywrkError = data.error.charitywrk.error;
                $scope.assCharityError = data.error.assCharity.error;
                $scope.organisationsError = data.error.organisations.error;
                $scope.pdfError = data.error.pdf.error;
                
             }
            });

                




        }


     };


                      
           
 
  $scope.uploadFile = function(event){
  var filename = event.target.files[0].name;
        //alert('file was selected: ' + filename);
        var t = filename.substr(0, filename.lastIndexOf("."));
//alert(t);
        var replaced = t.replace(/\./g,'_');
        //alert(replaced);
  $("#pdf").val(replaced+'.pdf');
       
    };
    


 $scope.euploadFile = function(event){
  var filename = event.target.files[0].name;
        //alert('file was selected: ' + filename);
        var t = filename.substr(0, filename.lastIndexOf("."));
//alert(t);
        var replaced = t.replace(/\./g,'_');
        //alert(replaced);
  $("#epdf").val(replaced+'.pdf');
       
    };


    var sub = $scope.uploader2 = new FileUploader({
      //alert();
            url: '<?php echo base_url();?>user/uploadResume'
        });

        // FILTERS

        sub.filters.push({
            name: 'customFilter',
            fn: function(item /*{File|FileLikeObject}*/, options) {
                return this.queue.length < 10;
            }
        });
        
        sub.onSuccessItem = function (fileItem, response, status, headers) {

                var t = fileItem.file.name.substr(0, fileItem.file.name.lastIndexOf("."));

        var replaced = t.replace(/\./g,'_');
        
                var filesize = fileItem.file.size;

                //alert(filesize);
               
                if(filesize<=500*1000)
               {

                $('#succ').html('File uploaded');
                $("#pdf").val(replaced+'.pdf');
               }
               else
               {
                $("#pdf").val('');
               }
                
            };





   $scope.eUserEmployment = {};

  
  // new upload cv


    $scope.euploadFile = function(event){
  var filename = event.target.files[0].name;
        alert('file was selected: ' + filename);
        var t = filename.substr(0, filename.lastIndexOf("."));
alert(t);
        var replaced = t.replace(/\./g,'_');
        //alert(replaced);
  $("#epdf").val(replaced+'.pdf');
       
    };
    
    var sub = $scope.euploader2 = new FileUploader({
      //alert();
            url: '<?php echo base_url();?>user/uploadResume'
        });

        // FILTERS

        sub.filters.push({
            name: 'customFilter',
            fn: function(item /*{File|FileLikeObject}*/, options) {
                return this.queue.length < 10;
            }
        });
        
        sub.onSuccessItem = function (fileItem, response, status, headers) {

                var t = fileItem.file.name.substr(0, fileItem.file.name.lastIndexOf("."));

        var replaced = t.replace(/\./g,'_');
        
                var filesize = fileItem.file.size;

                //alert(filesize);
               
                if(filesize<=500*1000)
               {

                $('#esucc').html('File uploaded');
                $("#epdf").val(replaced+'.pdf');
               }
               else
               {
                $("#epdf").val('');
               }
                
            };




  $scope.submitEditPreEmp = function() {

     

    $scope.eUserEmployment.epdf = $('#epdf').val();

     
   //  alert(2345)
            // $scope.eUserEmployment.eorganisations = editUserPreEmployment.eorganisations;


           if($scope.editUserPreEmployment.$valid){
                  
                  // alert('valid')

                  $http({
                         
                         method:'POST',
                         dataType:'json',
                         url:'<?php echo base_url(); ?>user/replaceUserPreEmploymentInfo',
                         data:$scope.eUserEmployment,
                         headers: {'Content-Type': 'application/json'}



                  }).success(function(data){


                  
                                if (data.status == 1) {
                                $('.help-block').css('display','none');
                                
                                  window.location = '<?php echo base_url();?>user/preEmployment';
                        } else if (data.status == 2)
                        {
                               window.location = '<?php echo base_url();?>user/preEmployment';
                                
                        } else if (data.status == 3)
                        {
                               window.location = '<?php echo base_url();?>user/preEmployment';
                                
                         }
                        else {
                          //console.log('dataaa')
                          //console.log(data.error.competitionname.error)
                                $scope.eemplrNmeError = data.error.eemplrNme.error;
                                $scope.efromMonthError = data.error.efromMonth.error;
                                $scope.efromYearError = data.error.efromYear.error;
                                $scope.etoYearError = data.error.etoYear.error;
                                $scope.etoMonthError = data.error.etoMonth.error;
                                $scope.edesignationError = data.error.edesignation.error;
                                $scope.ejobprofileError = data.error.ejobprofile.error;
                                $scope.eintTrainingError  = data.error.eintTraining.error;
                                $scope.ecEmpError = data.error.ecEmp.error;
                                $scope.eempreferenceError = data.error.eempreference.error;
                                $scope.esalaryError = data.error.esalary.error;
                                $scope.eachievedError = data.error.eachieved.error;
                                $scope.edIncomeError = data.error.edIncome.error;
                                $scope.einsuranceNoError = data.error.einsuranceNo.error;
                                $scope.echaritywrkError = data.error.echaritywrk.error;
                                $scope.eassCharityError = data.error.eassCharity.error;
                                $scope.epdfError = data.error.epdf.error;

                               

                        }   



                  });






            }
     /*       
     else{
       alert('notValid')
     }*/


  };







  $scope.editPreEmployment=function(id){
  
  // alert(id);

              $http({

                 method: 'POST',
                 dataType:'json',
                 url:'<?php echo base_url(); ?>user/getUserPreEmployment',
                 data:{id:id},
                 headers:{'Content-Type':'application/json'}

              }).success(function(data){
                 
                 console.log(data);
           
                // alert(data.userPreEmployementInfo);
                var $e1 = $("#e9").select2();
               // console.log($e1);
                 $e1.val(data.userOrganisation).trigger("change");
                  console.log($e1);
                 // alert(data)
                $scope.eUserEmployment = {
                                
                               
                               
                               'eid':data.userPreEmployementInfo.id,
                               'eemplrNme':data.userPreEmployementInfo.employer_name,
                               'efromMonth':data.userPreEmployementInfo.from_month,
                               'efromYear':data.userPreEmployementInfo.from_year,
                               'etoMonth':data.userPreEmployementInfo.to_month,
                               'etoYear':data.userPreEmployementInfo.to_year,
                               'edesignation':data.userPreEmployementInfo.designation,
                               'ejobprofile':data.userPreEmployementInfo.job_profile,
                               'eintTraining':data.userPreEmployementInfo.internal_training,

                               'ecEmp':data.userPreEmployementInfo.current_emplymnt_status,
                               'esalary':data.userPreEmployementInfo.present_income,
                               'epfrequency':data.userPreEmployementInfo.prsnt_income_freq, 
                               'edIncome':data.userPreEmployementInfo.desired_income,
                               'einsuranceNo':data.userPreEmployementInfo.national_insurance_no,
                               'edfrequency':data.userPreEmployementInfo.de_income_freq,
                               'erecieptOf':data.userPreEmployementInfo.reciept_benefit,
                               'ecrentact':data.userPreEmployementInfo.currently_actively_applying_status,
                               'emytext':data.userPreEmployementInfo.currently_actively_applying_desc,
                               'echaritywrk':data.userPreEmployementInfo.undertaken_voluntary_charity_work,
                               'eassCharity':data.userPreEmployementInfo.interested_in_volunteering_assisting_charities_status,
                               
                             

                               }



              });

      








  }


  
  $scope.delPreEmployment=function(id){
    
     if (confirm("Are you sure?")) {


             $http({
                                method: 'POST',
                                        dataType: 'json',
                                        url: '<?php echo base_url() ?>user/deleteUserPreEmployment',
                                        data: {id:id}, //forms user object
                                        headers: {'Content-Type': 'application/json'}
                                }).success(function (data) {
                                    //alert("deleted");
                               //alert(data.userEductionDetail.institute_name);
                              window.location = '<?php echo base_url();?>user/preEmployment';
                          
             

                          });


        
    }



  }

   

   }]);

/*var myfile="";

$('#resume_link').click(function( e ) {
    e.preventDefault();
    $('#resume').trigger('click');
});

$('#resume').on( 'change', function() {
   myfile= $( this ).val();
   var ext = myfile.split('.').pop();
   if(ext=="pdf" || ext=="docx" || ext=="doc"){
       alert(ext);
   } else{
       alert(ext);
   }
});*/


// $("#e9").removeAttr("multiple");

/* var fday   = $('#fday').val('');
       var fmonth = $('#fmonth').val('');
       var fyear  = $('#fyear').val('');
       var tday   = $('#tday').val('');
       var tmonth = $('#tmonth').val('');
       var rtoyear= $('#rtoyear').val('');*/


  function validateRDate()
  {

  var recieptFromDate = $('#fday').val()+$('#fmonth').val()+$('#fyear').val();
  var recieptToDate   = $('#tday').val()+$('#tmonth').val()+$('#rtoyear').val();

    //alert(recieptFromDate+'  '+recieptToDate);

   if(recieptFromDate > recieptToDate  || recieptFromDate == recieptToDate )
   {

    
           alert('Please select valid date...!!!');

       $('#fday').val('');
       $('#fmonth').val('');
       $('#fyear').val('');
       $('#tday').val('');
       $('#tmonth').val('');
       $('#rtoyear').val('');



    }



}
  



$(document).ready(function(){
        
       // alert('hide');

        $('#otherBenefits').hide();
    });












function validateDate() {
          


           var fromMon   =  $('#fromMonth').val();
           var fromYear= $('#fromYear').val();
           var toMon = $('#toMonth').val();
           var toYear= $('#toYear').val();


            if (fromYear > toYear) {
              //  alert("From year can't be > to year....");
                return;
            }
            else if (fromYear == toYear) {
                if (fromMon > toMon) {
                   // alert("From month can't be > to month....");
                    return;
                }
            }
        }



function checkFromMonth()
{


 var fromMon   =  $('#fromMonth').val();
 var fromYear= $('#fromYear').val();
 var toMon = $('#toMonth').val();
 var toYear= $('#toYear').val();
// alert(fromMon+" "+fromYear+" "+toMon+ " "+toYear);

 
 
  if (fromYear > toYear) {

              // alert("To date should be greater then from date");
                $('#toYear').val(" ");
                return;
            }
            else if (fromYear == toYear) {
                if (fromMon > toMon) {
                  //   alert("To date should be greater than from date");
                    $('#toMonth').val(" ");
                    return;
                }
            }
}


function checkFromDate()
{
 
 var fromMon   =  $('#fromMonth').val();
 var fromYear= $('#fromYear').val();
 var toMon = $('#toMonth').val();
 var toYear= $('#toYear').val();
 

 
  if (fromYear > toYear) {

               // alert("Please select to month");
                $('#toYear').val(" ");
                return;
            }
            else if (fromYear == toYear) {
                if (fromMon > toMon) {
                 //   alert("Please select greater to month from");
                   $('#toMon').val(" ");
                    return;
                }
            }
 



}


function checkToDate()
{


 var fromMon   =  $('#fromMonth').val();
 var fromYear= $('#fromYear').val();
 var toMon = $('#toMonth').val();
 var toYear= $('#toYear').val();
 
 //alert(fromMon+" "+fromYear+" "+toMon+ " "+toYear);

 
 
  if (fromYear > toYear) {
               // alert("To date should be greater than from date");
                $('#toYear').val(" ");
                return;
            }
            else if (fromYear == toYear) {
                if (fromMon > toMon) {
                //    alert("To date should be greater than from date");
                    $('#toYear').val(" ");
                    return;
                }
            }
}




function checkToMonth()
{


 var fromMon   =  $('#fromMonth').val();
 var fromYear= $('#fromYear').val();
 var toMon = $('#toMonth').val();
 var toYear= $('#toYear').val();
 
  // alert(fromMon+" "+fromYear+" "+toMon+ " "+toYear);

 
 
  if (fromYear > toYear) {
               // alert("To date should be greater then from date");

                $('#toMonth').val(' ');
                return;
            }
            else if (fromYear == toYear) {
                if (fromMon > toMon) {
                  //  alert("To date should be greater then from date");
                    $('#toMonth').val(' ');
                    return;
                }
            }
}


function checkFromDateE()
{
 
 var fromMon   =  $('#efromMonth').val();
 var fromYear= $('#efromYear').val();
 var toMon = $('#etoMonth').val();
 var toYear= $('#etoYear').val();
 

 
  if (fromYear > toYear) {
               // alert("To date should be greater than from date");
                $('#etoYear').val(" ");
                return;
            }
            else if (fromYear == toYear) {
                if (fromMon > toMon) {
                  //  alert("To date should be greater than from date");
                   $('#etoMonth').val(" ");
                    return;
                }
            }
 



}



function checkToDateE()
{


 var fromMon   =  $('#efromMonth').val();
 var fromYear= $('#efromYear').val();
 var toMon = $('#etoMonth').val();
 var toYear= $('#etoYear').val();
 
 //alert(fromMon+" "+fromYear+" "+toMon+ " "+toYear);

 
 
  if (fromYear > toYear) {
              //  alert("To date should be greater then from date");
                $('#etoYear').val(" ");
                return;
            }
            else if (fromYear == toYear) {
                if (fromMon > toMon) {
                  //  alert("To date should be greater then from date");
                    $('#etoYear').val(" ");
                    return;
                }
            }
}




function checkFromMonthE()
{

//alert();

 var fromMon   =  $('#efromMonth').val();
 var fromYear= $('#efromYear').val();
 var toMon = $('#etoMonth').val();
 var toYear= $('#etoYear').val();
// alert(fromMon+" "+fromYear+" "+toMon+ " "+toYear);

 
 
  if (fromYear > toYear) {
               // alert("To date should be greater then from date");
               // $('#fromMonth').val("");
                return;
            }
            else if (fromYear == toYear) {
                if (fromMon > toMon) {
                  //  alert("To date should be greater then from date");
                    $('#efromMonth').val(" ");
                    return;
                }
            }
}




function checkToMonthE()
{


 var fromMon   =  $('#efromMonth').val();
 var fromYear= $('#efromYear').val();
 var toMon = $('#etoMonth').val();
 var toYear= $('#etoYear').val();
 
  // alert(fromMon+" "+fromYear+" "+toMon+ " "+toYear);

 
 
  if (fromYear > toYear) {
               // alert("To date should be greater then from date");

              //  $('#toMonth').val('');
                return;
            }
            else if (fromYear == toYear) {
                if (fromMon > toMon) {
                   // alert("To date should be greater then from date");
                    $('#etoMonth').val(' ');
                    return;
                }
            }
}


</script>


<script type="text/javascript">
$(document).ready(function() {
  var showChar = 50;
  var ellipsestext = "...";
  var moretext = "less";
  var lesstext = "more";
  $('.more').each(function() {
    var content = $(this).html();

    if(content.length > showChar) {

      var c = content.substr(0, showChar);
      var h = content.substr(showChar-1, content.length - showChar);

      var html = c + '<span class="moreelipses">'+ellipsestext+'</span>&nbsp;<span class="morecontent"><span>' + h + '</span>&nbsp;&nbsp;<a href="" class="morelink">'+moretext+'</a></span>';

      $(this).html(html);
    }

  });

  $(".morelink").click(function(){
    if($(this).hasClass("less")) {
      $(this).removeClass("less");
      $(this).html(moretext);
    } else {
      $(this).addClass("less");
      $(this).html(lesstext);
    }
    $(this).parent().prev().toggle();
    $(this).prev().toggle();
    return false;
  });
});
</script>

<script>
  
  
    $(document).ready(function() {
        var max_fields      = 6; //maximum input boxes allowed
        var wrapper         = $(".input_fields_wrap"); //Fields wrapper
        var add_button      = $(".add_field_button"); //Add button ID
       
        var x = 1; //initlal text box count
        $('#add').click(function(e){ //on add input button click
          //  alert()
            e.preventDefault();
            if(x < max_fields){ //max input box allowed
                x++; //text box increment
                $(wrapper).append('<div><input type="text" ng-model="userEmployment.mytext" class="input form-control" name="mytext" required/><a href="#" class="remove_field btn btn-danger"><i class="fa fa-minus"></i></a></div>'); //add input box
            }
        });
       
        $(wrapper).on("click",".remove_field", function(e){ //user click on remove text
            e.preventDefault(); $(this).parent('div').remove(); x--;
        })
    });

</script>


<script>  
  
    
 jQuery("#benefits").change(function(){



   var selectValue = jQuery("#benefits").val();

        //  alert(selectValue);

         if(selectValue == "7")
         {

           $("#otherBenefits").show();

         }else{

          $("#otherBenefits").hide();
        }
      });


</script>

<script type="text/javascript">
  
  $(document).ready(function() {
     // /   alert(122)
        var max_fields      = 10; //maximum input boxes allowed
        var wrapper         = $(".input_fields_wrap"); //Fields wrapper
        var add_button      = $(".add_field_button"); //Add button ID
       
        var x = 1; //initlal text box count
        $('#add').click(function(e){ //on add input button click
           // alert()
            e.preventDefault();
            if(x < max_fields){ //max input box allowed
                x++; //text box increment
                $(wrapper).append('<div><input type="text" ng-model="userEmployment.mytext" class="input form-control" name="mytext" required/><a href="#" class="remove_field btn btn-danger"><i class="fa fa-minus"></i></a></div>'); //add input box
            }
        });
       
        $(wrapper).on("click",".remove_field", function(e){ //user click on remove text
            e.preventDefault(); $(this).parent('div').remove(); x--;
        })
    });


</script>