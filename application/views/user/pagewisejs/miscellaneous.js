<script>
        
        var postApp = angular.module('postApp',[]);


        postApp.controller('postController',function ($scope,$http){

          $scope.userMiscellaneousDetail = {};

          
          $scope.userMiscellaneousDetail.lifeStyle=`<?php echo !empty($userMiscellaneousInfo) ? $userMiscellaneousInfo->life_style :'' ?>`;
          $scope.userMiscellaneousDetail.health=`<?php echo !empty($userMiscellaneousInfo) ? $userMiscellaneousInfo->health :'' ?>`;
          $scope.userMiscellaneousDetail.fitness=`<?php echo !empty($userMiscellaneousInfo) ? $userMiscellaneousInfo->fitness :'' ?>`;

          $scope.userMiscellaneousDetail.optradio=`<?php echo !empty($userMiscellaneousInfo) ? $userMiscellaneousInfo->lyf_style_rating :'' ?>`;
          $scope.userMiscellaneousDetail.optradio1=`<?php echo !empty($userMiscellaneousInfo) ? $userMiscellaneousInfo->health_rating :'' ?>`;
          $scope.userMiscellaneousDetail.optradio2=`<?php echo !empty($userMiscellaneousInfo) ? $userMiscellaneousInfo->fitness_rating :'' ?>`;




          $scope.submitForm=function(){

              if($scope.userMiscellaneous.$valid)
              {


                   $http({
                                method: 'POST',
                                        dataType: 'json',
                                        url: '<?php echo base_url() ?>user/miscellaneousInfo',
                                        data: $scope.userMiscellaneousDetail, //forms user object
                                        headers: {'Content-Type': 'application/json'}
                                }).success(function (data) {
                                	
                              
                                if (data.status == 1) {
                        	     	$('.help-block').css('display','none');
                        	     	
                                	window.location = '<?php echo base_url();?>user/socialMedia';
                        } else if (data.status == 2)
                        {
                        	     window.location = '<?php echo base_url();?>user/miscellaneous';
                                
                        } else if (data.status == 3)
                        {
                        	     window.location = '<?php echo base_url();?>user/miscellaneous';
                                
                         }
                        else {
                        	//console.log('dataaa')
                        	//console.log(data.error.competitionname.error)
                                $scope.lifeStyleError = data.error.lifeStyle.error;
                                $scope.healthError = data.error.health.error;
                                $scope.fitnessError = data.error.fitness.error;
                                $scope.optradioError = data.error.optradio.error;
                                $scope.optradio1Error = data.error.optradio1.error;
                                $scope.optradio2Error = data.error.optradio2.error;
                                
                               

                        } 	
                        
                        });



              }


          };



        });





</script>