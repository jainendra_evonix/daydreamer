
<script type="text/javascript">
var postApp = angular.module('postApp', []);
postApp.controller('postController', function ($scope, $http) {
$scope.user = {};
$scope.submitForm = function () {

if ($scope.userLogin.$valid) {
  
$http({

  method: 'POST',
  dataType: 'json',
  url: '<?php echo base_url();?>user/check_login', 
  data: $scope.user, //forms client object
      headers: {'Content-Type': 'application/json'}

    }).success(function(data){

    if(data.status==1){

     window.location = '<?php echo base_url('user') ?>';
     

    }

     else if(data.status==0)
     {
      
      //window.location='<?php echo base_url('user')?>';
      $('#message').html('');
      $('#message').append(data.message);
      $scope.emailaddressError='';
     }


    });
      
       

}
};
});

</script>