
<?php // echo print_r($userMiscellaneousInfo); exit; ?>

        
        <div class="row">
            <div class="col-md-3">
            </div>
                <div class="col-md-6">
        <?php if($this->session->flashdata('succmsg')){  ?>
                
                                        <div class="alert alert-success">
                                         <strong>Success!</strong> <?php echo $this->session->flashdata('succmsg'); ?>
                                         
                                         <a type="button" class="close" data-dismiss="alert" aria-hidden="true">×</a>
                                        </div>

                                        <?php    } ?>
                                        </div>
                                <div class="col-md-3">
                                    
                                </div>
                         </div>
        <!--main content start-->
        <section id="" class="container">
            <section class="wrapper">
                <!-- page start-->
                <div class="row">
                    <div class="col-md-8 col-md-offset-2">
                        <h3 class="form-heading">Miscellaneous Information</h3>
                       <!--  <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit.</p>
 -->
                        <section class="panel">
                            <div class="panel-body">
                                <form class="form-horizontal bucket-form" name="userMiscellaneous" ng-submit="submitForm()" novalidate>
                                    <h4>Health and Lifestyle</h4>
                                    <div class="form-group">
                                        <label class="col-sm-3 control-label">How healthy is your life-style?<span class="req">*</span></label>
                                        <!-- <div class="row">
                                            <div class="col-sm-10">
                                              <div class="col-sm-1">
                                                <input tabindex="3" type="radio" ng-model="userEmployment.cEmp" value="yes"  name="cEmp" required>
                                                  
                                              </div> 
                                              <div class="col-sm-1">
                                                <input tabindex="3" type="radio" ng-model="userEmployment.cEmp" value="yes"  name="cEmp" required>
                                                  
                                              </div>
                                              <div class="col-sm-1">
                                                <input tabindex="3" type="radio" ng-model="userEmployment.cEmp" value="yes"  name="cEmp" required>
                                                  
                                              </div>
                                              <div class="col-sm-1">
                                                <input tabindex="3" type="radio" ng-model="userEmployment.cEmp" value="yes"  name="cEmp" required>
                                                  
                                              </div>
                                              <div class="col-sm-1">
                                                <input tabindex="3" type="radio" ng-model="userEmployment.cEmp" value="yes"  name="cEmp" required>
                                                  
                                              </div>
                                             
                                              
                                              
                                            </div>

                                        </div> -->


                                        <div class="col-sm-9">

                                              
                                              <ul class="list-inline list-unstyled">
                                                <li class="radio">
                                                <label><input type="radio" ng-model="userMiscellaneousDetail.optradio" name="optradio" value="1" required><br> <span class="healthrating" >1</span></label>
                                                </li>
                                                <li class="radio">
                                                  <label><input type="radio" ng-model="userMiscellaneousDetail.optradio" name="optradio" value="2" required><br> <span class="healthrating" >2</span></label>
                                                </li>
                                                <li class="radio">
                                                  <label><input type="radio" ng-model="userMiscellaneousDetail.optradio" name="optradio" value="3" required><br> <span class="healthrating" >3</span></label>
                                                </li>
                                                <li class="radio">
                                                  <label><input type="radio" ng-model="userMiscellaneousDetail.optradio" name="optradio" value="4" required ><br> <span class="healthrating" >4</span></label>
                                                </li>
                                                <li class="radio">
                                                  <label><input type="radio" ng-model="userMiscellaneousDetail.optradio" name="optradio" value="5" required><br> <span class="healthrating" >5</span></label>
                                                </li>
                                                <li class="radio">
                                                  <label><input type="radio" name="optradio" value="6" required><br> <span class="healthrating" >6</span></label>
                                                </li>
                                                <li class="radio">
                                                  <label><input type="radio" ng-model="userMiscellaneousDetail.optradio" name="optradio" value="7" required><br> <span class="healthrating" >7</span></label>
                                                </li>
                                                <li class="radio">
                                                  <label><input type="radio" ng-model="userMiscellaneousDetail.optradio" name="optradio" value="8" required><br> <span class="healthrating" >8</span></label>
                                                </li>
                                                <li class="radio">
                                                  <label><input type="radio" ng-model="userMiscellaneousDetail.optradio" name="optradio" value="9" required ><br> <span class="healthrating">9</span></label>
                                                </li>
                                                <li class="radio">
                                                  <label><input type="radio" ng-model="userMiscellaneousDetail.optradio" name="optradio" value="10" required><br> <span class="healthrating">10</span></label>
                                                </li>
                                                <span ng-show="submittedd && userMiscellaneous.optradio.$invalid" class="help-block has-error ng-hide warnig">Please rate yourself</span>
             <span class="help-block has-error ng-hide warnig" style="color: red;" ng-show="optradioError">{{optradioError}}</span>
                                            </ul>

                                            <textarea class="form-control"  ng-model="userMiscellaneousDetail.lifeStyle" name="lifeStyle" require ng-pattern="/.*[a-zA-Z]+.*/" rows="2" required></textarea>

                                            <span ng-show="submittedd && userMiscellaneous.lifeStyle.$invalid" class="help-block has-error ng-hide warnig">Please enter about your life-style</span>
             <span class="help-block has-error ng-hide warnig" style="color: red;" ng-show="lifeStyleError">{{lifeStyleError}}</span>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-sm-3 control-label">How important to you is your health?<span class="req">*</span></label>
                                        <div class="col-sm-9">

                                             <ul class="list-inline list-unstyled">
                                                <li class="radio">
                                                  <label><input type="radio" ng-model="userMiscellaneousDetail.optradio1" name="optradio1" value="1" required><br> <span class="healthrating" >1</span></label>
                                                </li>
                                                <li class="radio">
                                                  <label><input type="radio" ng-model="userMiscellaneousDetail.optradio1" name="optradio1" value="2" required><br> <span class="healthrating" >2</span></label>
                                                </li>
                                                <li class="radio">
                                                  <label><input type="radio" ng-model="userMiscellaneousDetail.optradio1" name="optradio1" value="3" required><br> <span class="healthrating" >3</span></label>
                                                </li>
                                                <li class="radio">
                                                  <label><input type="radio" ng-model="userMiscellaneousDetail.optradio1" name="optradio1" value="4" required><br> <span class="healthrating" >4</span></label>
                                                </li>
                                                <li class="radio">
                                                  <label><input type="radio" ng-model="userMiscellaneousDetail.optradio1" name="optradio1" value="5" required><br> <span class="healthrating" >5</span></label>
                                                </li>
                                                <li class="radio">
                                                  <label><input type="radio" ng-model="userMiscellaneousDetail.optradio1" name="optradio1" value="6" required><br> <span class="healthrating" >6</span></label>
                                                </li>
                                                <li class="radio">
                                                  <label><input type="radio" ng-model="userMiscellaneousDetail.optradio1" name="optradio1" value="7" required><br> <span class="healthrating" >7</span></label>
                                                </li>
                                                <li class="radio">
                                                  <label><input type="radio" ng-model="userMiscellaneousDetail.optradio1" name="optradio1" value="8" required><br> <span class="healthrating" >8</span></label>
                                                </li>
                                                <li class="radio">
                                                  <label><input type="radio" ng-model="userMiscellaneousDetail.optradio1" name="optradio1"  value="9" required><br> <span class="healthrating">9</span></label>
                                                </li>
                                                <li class="radio">
                                                  <label><input type="radio" ng-model="userMiscellaneousDetail.optradio1" name="optradio1" value="10" required><br> <span class="healthrating" >10</span></label>
                                                </li>

                                                 <span ng-show="submittedd && userMiscellaneous.optradio1.$invalid" class="help-block has-error ng-hide warnig">Please rate yourself</span>
             <span class="help-block has-error ng-hide warnig" style="color: red;" ng-show="optradio1Error">{{optradio1Error}}</span>
                                            </ul>




                                            <textarea class="form-control" ng-model="userMiscellaneousDetail.health" name="health" require ng-pattern="/.*[a-zA-Z]+.*/" rows="2" required></textarea>
                                            <span ng-show="submittedd && userMiscellaneous.health.$invalid" class="help-block has-error ng-hide warnig">Please enter about your health</span>
             <span class="help-block has-error ng-hide warnig" style="color: red;" ng-show="healthError">{{healthError}}</span>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-sm-3 control-label">How important to you is your fitness?<span class="req">*</span></label>
                                        <div class="col-sm-9">

                                           <ul class="list-inline list-unstyled">
                                                <li class="radio">
                                                  <label><input type="radio" ng-model="userMiscellaneousDetail.optradio2" name="optradio2" value="1" required><br> <span class="healthrating" >1</span></label>
                                                </li>
                                                <li class="radio">
                                                  <label><input type="radio" ng-model="userMiscellaneousDetail.optradio2" name="optradio2" value="2" required ><br> <span class="healthrating" >2</span></label>
                                                </li>
                                                <li class="radio">
                                                  <label><input type="radio" ng-model="userMiscellaneousDetail.optradio2" name="optradio2" value="3" required><br> <span class="healthrating">3</span></label>
                                                </li>
                                                <li class="radio">
                                                  <label><input type="radio" ng-model="userMiscellaneousDetail.optradio2" name="optradio2" value="4" required ><br> <span class="healthrating" >4</span></label>
                                                </li>
                                                <li class="radio">
                                                  <label><input type="radio" ng-model="userMiscellaneousDetail.optradio2" name="optradio2" value="5" required ><br> <span class="healthrating" >5</span></label>
                                                </li>
                                                <li class="radio">
                                                  <label><input type="radio" ng-model="userMiscellaneousDetail.optradio2" name="optradio2" value="6" required ><br> <span class="healthrating" >6</span></label>
                                                </li>
                                                <li class="radio">
                                                  <label><input type="radio" ng-model="userMiscellaneousDetail.optradio2" name="optradio2" value="7" required><br> <span class="healthrating" >7</span></label>
                                                </li>
                                                <li class="radio">
                                                  <label><input type="radio" ng-model="userMiscellaneousDetail.optradio2" name="optradio2" value="8" required ><br> <span class="healthrating" >8</span></label>
                                                </li>
                                                <li class="radio">
                                                  <label>
                                                  <input type="radio" ng-model="userMiscellaneousDetail.optradio2" name="optradio2" value="9" required><br>
                                                   <span class="healthrating">9</span></label>
                                                </li>
                                                <li class="radio">
                                                  <label>
                                                  <input type="radio" value="10" ng-model="userMiscellaneousDetail.optradio2" name="optradio2"  required>
                                                  <br> 
                                                  <span class="healthrating">10</span></label>
                                                </li>


                                             <span ng-show="submittedd && userMiscellaneous.optradio2.$invalid" class="help-block has-error ng-hide warnig">Please rate yourself</span>
             <span class="help-block has-error ng-hide warnig" style="color: red;" ng-show="optradio2Error">{{optradio2Error}}</span>


                                            </ul>

                                            <textarea class="form-control" ng-model="userMiscellaneousDetail.fitness"  name="fitness" require ng-pattern="/.*[a-zA-Z]+.*/" rows="2" required></textarea>
                                       <span ng-show="submittedd && userMiscellaneous.fitness.$invalid" class="help-block has-error ng-hide warnig">Please enter about fitness</span>
             <span class="help-block has-error ng-hide warnig" style="color: red;" ng-show="fitnessError">{{fitnessError}}</span>

                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <div class="col-md-3">
                                            <a href="<?php echo base_url(); ?>user/additionalInfo" type="button" class="btn btn-danger btn-sm"><strong><i class="fa fa-arrow-left"></i> Back</strong></a>
                                            <span class="help-block small text-muted">(Additional Information)</span>
                                        </div>
                                        <div class="col-md-9">
                                            <button type="submit" ng-click="submittedd = true" class="btn btn-info pull-right btn-sm"><strong>Save &amp; Continue <i class="fa fa-arrow-right"></i></strong></button>
                                            <span class="clearfix"></span>
                                            <span class="help-block pull-right small text-muted">(Social Media)</span>
                                        </div>
                                    </div>
                                    
                                </form>
                            </div>
                        </section>
                    </div>
                </div>

                <!-- page end-->

            </section>
        </section>
        <!--main content end-->





    <!--right sidebar start-->
    <div class="right-sidebar">
        <div class="search-row">
            <input type="text" placeholder="Search" class="form-control">
        </div>
        <div class="right-stat-bar">
            <ul class="right-side-accordion">
                <li class="widget-collapsible">
                    <a href="#" class="head widget-head red-bg active clearfix">
                        <span class="pull-left">work progress (5)</span>
                        <span class="pull-right widget-collapse"><i class="ico-minus"></i></span>
                    </a>
                    <ul class="widget-container">
                        <li>
                            <div class="prog-row side-mini-stat clearfix">
                                <div class="side-graph-info">
                                    <h4>Target sell</h4>
                                    <p>
                                        25%, Deadline 12 june 13
                                    </p>
                                </div>
                                <div class="side-mini-graph">
                                    <div class="target-sell">
                                    </div>
                                </div>
                            </div>
                            <div class="prog-row side-mini-stat">
                                <div class="side-graph-info">
                                    <h4>product delivery</h4>
                                    <p>
                                        55%, Deadline 12 june 13
                                    </p>
                                </div>
                                <div class="side-mini-graph">
                                    <div class="p-delivery">
                                        <div class="sparkline" data-type="bar" data-resize="true" data-height="30" data-width="90%" data-bar-color="#39b7ab" data-bar-width="5" data-data="[200,135,667,333,526,996,564,123,890,564,455]">
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="prog-row side-mini-stat">
                                <div class="side-graph-info payment-info">
                                    <h4>payment collection</h4>
                                    <p>
                                        25%, Deadline 12 june 13
                                    </p>
                                </div>
                                <div class="side-mini-graph">
                                    <div class="p-collection">
                                      <span class="pc-epie-chart" data-percent="45">
                                          <span class="percent"></span>
                                      </span>
                                  </div>
                              </div>
                          </div>
                          <div class="prog-row side-mini-stat">
                            <div class="side-graph-info">
                                <h4>delivery pending</h4>
                                <p>
                                    44%, Deadline 12 june 13
                                </p>
                            </div>
                            <div class="side-mini-graph">
                                <div class="d-pending">
                                </div>
                            </div>
                        </div>
                        <div class="prog-row side-mini-stat">
                            <div class="col-md-12">
                                <h4>total progress</h4>
                                <p>
                                    50%, Deadline 12 june 13
                                </p>
                                <div class="progress progress-xs mtop10">
                                    <div style="width: 50%" aria-valuemax="100" aria-valuemin="0" aria-valuenow="20" role="progressbar" class="progress-bar progress-bar-info">
                                        <span class="sr-only">50% Complete</span>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </li>
                </ul>
            </li>
            <li class="widget-collapsible">
                <a href="#" class="head widget-head terques-bg active clearfix">
                    <span class="pull-left">contact online (5)</span>
                    <span class="pull-right widget-collapse"><i class="ico-minus"></i></span>
                </a>
                <ul class="widget-container">
                    <li>
                        <div class="prog-row">
                            <div class="user-thumb">
                                <a href="#"><img src="<?php echo base_url(); ?>user_assets/images/avatar1_small.jpg" alt=""></a>
                            </div>
                            <div class="user-details">
                                <h4><a href="#">Jonathan Smith</a></h4>
                                <p>
                                    Work for fun
                                </p>
                            </div>
                            <div class="user-status text-danger">
                                <i class="fa fa-comments-o"></i>
                            </div>
                        </div>
                        <div class="prog-row">
                            <div class="user-thumb">
                                <a href="#"><img src="<?php echo base_url(); ?>user_assets/images/avatar1.jpg" alt=""></a>
                            </div>
                            <div class="user-details">
                                <h4><a href="#">Anjelina Joe</a></h4>
                                <p>
                                    Available
                                </p>
                            </div>
                            <div class="user-status text-success">
                                <i class="fa fa-comments-o"></i>
                            </div>
                        </div>
                        <div class="prog-row">
                            <div class="user-thumb">
                                <a href="#"><img src="<?php echo base_url(); ?>user_assets/images/chat-avatar2.jpg" alt=""></a>
                            </div>
                            <div class="user-details">
                                <h4><a href="#">John Doe</a></h4>
                                <p>
                                    Away from Desk
                                </p>
                            </div>
                            <div class="user-status text-warning">
                                <i class="fa fa-comments-o"></i>
                            </div>
                        </div>
                        <div class="prog-row">
                            <div class="user-thumb">
                                <a href="#"><img src="<?php echo base_url(); ?>user_assets/images/avatar1_small.jpg" alt=""></a>
                            </div>
                            <div class="user-details">
                                <h4><a href="#">Mark Henry</a></h4>
                                <p>
                                    working
                                </p>
                            </div>
                            <div class="user-status text-info">
                                <i class="fa fa-comments-o"></i>
                            </div>
                        </div>
                        <div class="prog-row">
                            <div class="user-thumb">
                                <a href="#"><img src="<?php echo base_url(); ?>user_assets/images/avatar1.jpg" alt=""></a>
                            </div>
                            <div class="user-details">
                                <h4><a href="#">Shila Jones</a></h4>
                                <p>
                                    Work for fun
                                </p>
                            </div>
                            <div class="user-status text-danger">
                                <i class="fa fa-comments-o"></i>
                            </div>
                        </div>
                        <p class="text-center">
                            <a href="#" class="view-btn">View all Contacts</a>
                        </p>
                    </li>
                </ul>
            </li>
            <li class="widget-collapsible">
                <a href="#" class="head widget-head purple-bg active">
                    <span class="pull-left"> recent activity (3)</span>
                    <span class="pull-right widget-collapse"><i class="ico-minus"></i></span>
                </a>
                <ul class="widget-container">
                    <li>
                        <div class="prog-row">
                            <div class="user-thumb rsn-activity">
                                <i class="fa fa-clock-o"></i>
                            </div>
                            <div class="rsn-details ">
                                <p class="text-muted">
                                    just now
                                </p>
                                <p>
                                    <a href="#">Jim Doe </a>Purchased new equipments for zonal office setup
                                </p>
                            </div>
                        </div>
                        <div class="prog-row">
                            <div class="user-thumb rsn-activity">
                                <i class="fa fa-clock-o"></i>
                            </div>
                            <div class="rsn-details ">
                                <p class="text-muted">
                                    2 min ago
                                </p>
                                <p>
                                    <a href="#">Jane Doe </a>Purchased new equipments for zonal office setup
                                </p>
                            </div>
                        </div>
                        <div class="prog-row">
                            <div class="user-thumb rsn-activity">
                                <i class="fa fa-clock-o"></i>
                            </div>
                            <div class="rsn-details ">
                                <p class="text-muted">
                                    1 day ago
                                </p>
                                <p>
                                    <a href="#">Jim Doe </a>Purchased new equipments for zonal office setup
                                </p>
                            </div>
                        </div>
                    </li>
                </ul>
            </li>
            <li class="widget-collapsible">
                <a href="#" class="head widget-head yellow-bg active">
                    <span class="pull-left"> shipment status</span>
                    <span class="pull-right widget-collapse"><i class="ico-minus"></i></span>
                </a>
                <ul class="widget-container">
                    <li>
                        <div class="col-md-12">
                            <div class="prog-row">
                                <p>
                                    Full sleeve baby wear (SL: 17665)
                                </p>
                                <div class="progress progress-xs mtop10">
                                    <div class="progress-bar progress-bar-success" role="progressbar" aria-valuenow="20" aria-valuemin="0" aria-valuemax="100" style="width: 40%">
                                        <span class="sr-only">40% Complete</span>
                                    </div>
                                </div>
                            </div>
                            <div class="prog-row">
                                <p>
                                    Full sleeve baby wear (SL: 17665)
                                </p>
                                <div class="progress progress-xs mtop10">
                                    <div class="progress-bar progress-bar-info" role="progressbar" aria-valuenow="20" aria-valuemin="0" aria-valuemax="100" style="width: 70%">
                                        <span class="sr-only">70% Completed</span>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </li>
                </ul>
            </li>
        </ul>
    </div>
</div>

