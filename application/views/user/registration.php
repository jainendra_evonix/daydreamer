  <!-- <body class="login-body" ng-app="postApp" ng-controller="postController"> -->

    <div class="container">

      <form class="form-signin" name="userLogin" ng-submit="submitForm()" novalidate>
        <h2 class="form-signin-heading"><strong>Sign Up</strong></h2>
         <div id="message"></div>
        <div class="login-wrap">
                     <?php echo $this->session->flashdata('successmsg');  ?>
                      <?php echo $this->session->flashdata('errormsg');  ?>
            <p>Enter your personal details below</p>
              
             <!--  <input type="text" class="form-control" placeholder="Name" id="name" ng-model="user.name" name="name" required>                   
            <span ng-show="submitted && userLogin.name.$invalid" class="help-block has-error">Name is required.</span>
            <span class="help-block has-error" ng-show="nameError">{{nameError}}</span>
                            <br> -->

            <input type="text" class="form-control" placeholder="First Name" ng-model="user.fname" name="fname"  required>
               
             <span ng-show="submitted && userLogin.fname.$invalid" class="help-block has-error ng-hide warnig">Please Enter First Name</span> 
             <span class="help-block has-error ng-hide warnig" ng-show="fnameError">{{fnameError}}</span> 
              <br>
            <input type="text" class="form-control" name="lname" ng-model="user.lname"  placeholder="Last Name" required autofocus>

            <span ng-show="submitted && userLogin.lname.$invalid" class="help-block has-error ng-hide warnig">Please Enter Last Name</span> 
            <span class="help-block has-error ng-hide warnig" ng-show="lnameError">{{lnameError}}</span> 

            <input type="text" class="form-control" name="city" ng-model="user.city"  placeholder="City/Town" required autofocus>

            <span ng-show="submitted && userLogin.city.$invalid" class="help-block has-error ng-hide warnig">Please Enter City Name</span> 
            <span class="help-block has-error ng-hide warnig" ng-show="cityError">{{cityError}}</span> 
            

            <p> Enter your account details below</p>

            <input type="email" class="form-control" name="emailid" id="emailid" ng-model="user.emailid" placeholder="Email" required autocomplete="off">
             <span id="email_status" style="color:red;"></span>
            <span ng-show="submitted && userLogin.emailid.$invalid" class="help-block has-error ng-hide warnig">Please Enter Email Id Name</span>
            <span class="help-block has-error ng-hide warnig" ng-show="emailidError">{{emailidError}}</span> 
      <br>

            <input type="password" class="form-control" name="password" ng-model="user.password" placeholder="Password" ng-minlength="6" ng-maxlength="20" required>
             
            <span ng-show="submitted && userLogin.password.$invalid" class="help-block has-error ng-hide warnig">Please Enter Password (Min six charecters)</span>
            <span class="help-block has-error ng-hide warnig" ng-show="passwordError">{{passwordError}}</span>
                   
            <input type="password" class="form-control" name="cpassword" ng-model="user.cpassword" placeholder="Re-type Password" ng-minlength="6" ng-maxlength="20" required>

            <span ng-show="submitted && userLogin.cpassword.$invalid" class="help-block has-error ng-hide warnig">Please Enter Password Again</span>
            <span class="help-block has-error ng-hide warnig" ng-show="cpasswordError">{{cpasswordError}}</span> 

            <label class="checkbox">
                <input type="checkbox" value="agree this condition" ng-required="true" name="agree" ng-model="user.agree"> I agree to the <a href="#">Terms of Service</a> and <a href="#">Privacy Policy</a>
                 <span ng-show="submitted && userLogin.agree.$invalid" class="help-block has-error ng-hide warnig">Please accept terms & conditions</span>
            <span class="help-block has-error ng-hide warnig" ng-show="agreeError">{{agreeError}}</span>
            </label>
            <button class="btn btn-lg btn-login btn-block" type="submit" ng-click="submitted = true">Submit</button>
            <span class="help-block has-error warnig" ng-show="IsMatch">Password does not match Confirm Password.</span>
            <span ng-show="userLogin.password !== userLogin.cpassword" class="help-block has-error ng-hide">Passwords have to match!</span>
            <div class="registration">
                Already Registered.
                <a class="" href="<?php echo base_url(); ?>user/login">
                    Login
                </a>
            </div>

        </div>

      </form>

    </div>


