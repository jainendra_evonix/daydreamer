 <?php //echo "<pre>"; print_r($userProfileData); exit; 

                 //echo $userProfileData->country_name; exit;
          ?>
 <!-- <body class="login-body" ng-app="postApp" ng-controller="postController" ng-cloak>  -->
     
      <!--main content start-->
                <section id="" class="container">
                    <section class="wrapper">
                        <!-- page start-->
                        <div class="row">
                            <div class="col-md-8 col-md-offset-2">
                                <h3 class="form-heading">Personal Information</h3>
                               <!--  <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit.</p> -->
                                <section class="panel">
                                    <div class="panel-body">
                                        <form class="form-horizontal bucket-form" name="userProfile" ng-submit="submitForm()" novalidate >
                                            <div class="form-group">
                                                <label class="col-sm-3 control-label">Gender<span class="req">*</span></label>
                                                <div class="col-sm-6 icheck">
                                                    <div class="square single-row">
                                                        <div class="">
                                                            <!-- <input tabindex="3" type="radio"  ng-model="user.gender" name="gender"  ng-required="!gender"> -->
                                                             <input name="gender" type="radio" ng-model="user.gender" value="male" <?php echo  set_radio('gender', 'male', TRUE); ?> required>Male<br/>
                                                           <!--  <label>Male </label> -->
                                                            
                                                             
                                                            <!-- <span class="help-block has-error ng-hide" ng-show="genderError">{{genderError}}</span> --> 
                                                        </div>
                                                        <div class="">
                                                            <!-- <input tabindex="3" type="radio"  ng-model="user.gender" name="gender" required="!gender"> -->
                                                            <input name="gender" type="radio" ng-model="user.gender" value="female" <?php echo  set_radio('gender', 'female'); ?> required>Female<br/>
                                                            <!-- <label>Female </label> -->
                                                         
                                                        </div>
                                                        <span ng-show="submitted && userProfile.gender.$invalid" class="help-block has-error ng-hide warnig">Please Select Your Gender</span>
                                                        <span class="help-block has-error ng-hide warnig" style="color: red;" ng-show="genderError">{{genderError}}</span>
                                                    </div>
                                                </div>
                                            </div>


                                            <div class="form-group">
                                                <label class="col-sm-3 control-label">Relationship Status<span class="req">*</span></label>
                                                <div class="col-sm-6">
                                                    <!-- <input type="text"  class="form-control" placeholder="Enter your nationality"> -->
                                                    <select class="form-control m-bot15" ng-model="user.rstatus" name="rstatus" required>
                                                    <option disabled="disabled" value="" selected="selected">Select your status</option>
                                                    <?php  if($relationshipStatus ){
                                                         
                                                     foreach ($relationshipStatus as $key) { ?>
                                                     <option value="<?php echo $key->id; ?>" ><?php echo $key->rel_status; ?></option>

                                                  <?php  }  }?>

                                                     
                                                    </select>
                                                    <span ng-show="submitted && userProfile.rstatus.$invalid" class="help-block has-error ng-hide warnig">Please Enter Your Relationship Status (E.g. Single/Married) </span> 
                                                    <span class="help-block has-error ng-hide" ng-show="rstatusError">{{rstatusError}}</span>
                                                </div>
                                                <div class="col-md-3">
                                                   <!--  <span class="help-block">Display additional instructions here.</span> -->
                                                </div>
                                            </div>

      <div class="form-group" >
        <label class="control-label col-md-3">Date Of Birth<span class="req">*</span></label>
          <div class="col-md-6">
             <div class="row">
               
                     <div class="col-md-4">
                     <select class="form-control m-bot15" ng-model="user.month"  id="month" name="month" onChange="call();" required>
                                                            
                                                              <option value="">Month</option>
                                                               <option value="1">Jan</option>
                                                               <option value="2">Feb</option>
                                                               <option value="3">Mar</option>
                                                               <option value="4">Apr</option>
                                                               <option value="5">May</option>
                                                               <option value="6">Jun</option>
                                                               <option value="7">Jul</option>
                                                               <option value="8">Aug</option>
                                                               <option value="9">Sep</option>
                                                               <option value="10">Oct</option>
                                                               <option value="11">Nov</option>
                                                               <option value="12">Dec</option>
                                                             </select>

                 <span ng-show="submitted && userProfile.month.$invalid" class="help-block has-error ng-hide warnig">Please select month</span>
                 <span class="help-block has-error ng-hide warnig" style="color: red;" ng-show="monthError">{{monthError}}</span>
                     </div>

                     <div class="col-md-4">
                               <select class="form-control m-bot15" ng-model="user.day" name="day" id="day" value="" required>
                                                              <option value="">Day</option>
                                                              <?php foreach ($days as $key ) { ?>

                                                                 <option value="<?php echo $key->day; ?>" ><?php echo $key->day; ?></option>
                                                           <?php    } ?>

                                                          <!--  <option value="">Day</option>
                                                           <option value="29">29</option> -->
                                                      </select>

                            <span ng-show="submitted && userProfile.day.$invalid" class="help-block has-error ng-hide warnig">Please select day</span>
                 <span class="help-block has-error ng-hide warnig" style="color: red;" ng-show="dayError">{{dayError}}</span>                          
                                           </div>

                      <div class="col-md-4">
                          <select class="form-control m-bot15" ng-model="user.year" name="year" id="year" value="" onchange="call();" required>
                                                               
                                                                <option value="">Year</option>
                                                                   <?php foreach ($years as $value) { ?>
                                                                       <option value="<?php echo $value->year ?>"><?php echo $value->year ?></option>
                                                                  <?php  } ?>

                                                       </select>
                          <span ng-show="submitted && userProfile.year.$invalid" class="help-block has-error ng-hide warnig">Please select year</span>
                     <span class="help-block has-error ng-hide warnig" style="color: red;" ng-show="yearError">{{yearError}}</span>

                       </div>
                       <span id="errMsg" class="warnig"></span>

                 </div>
                 </div>

             </div>

                                     
                                            <div class="form-group">
                                                <label class="col-sm-3 control-label">Nationality<span class="req">*</span></label>
                                                <div class="col-sm-6">
                                                    <!-- <input type="text"  class="form-control" placeholder="Enter your nationality"> -->
                                                    <select class="form-control m-bot15" ng-model="user.nationality" name="nationality" required>
                                                    <option disabled="disabled" value="" selected="selected">Select your nationality</option>
                                                    <?php  if($countries ){
                                                         
                                                     foreach ($countries as $country) { ?>
                                                     <option value="<?php echo $country->country_code; ?>"><?php echo $country->country_name; ?></option>

                                                  <?php  }  }?>

                                                     
                                                    </select>
                                                    <span ng-show="submitted && userProfile.nationality.$invalid" class="help-block has-error ng-hide warnig">Please Select Your Country </span> 
                     <span class="help-block has-error ng-hide"  ng-show="nationalitydateError">{{nationalitydateError}}</span>
                                                </div>
                                                <div class="col-md-3">
                                                    <span class="help-block">Display additional instructions here.</span>
                                                </div>
                                            </div>



                                            <div class="form-group">
                                                <label class="col-sm-3 control-label">Ethnic Origin<span class="req">*</span></label>
                                                <div class="col-sm-6">
                                                    <!-- <input type="text"  class="form-control" placeholder="Enter your ethnic origin"> -->
                                    <select class="form-control m-bot15"  ng-model="user.ethnicorigin" name="ethnicorigin" value=""  required >
                                      <option disabled="disabled" value="" selected="selected">Select your ethnic origin</option>
                                                     <?php   if($ethnicOrigin ){

                                                        foreach($ethnicOrigin as $origin){ ?>
                                                            
                                                            <option value="<?php echo $origin->id; ?>"><?php echo $origin->ethnic_origin; ?></option>

                                                     <?php    } }?>
                                                        
                                                        
                                                    </select>
                                                    <span ng-show="submitted && userProfile.ethnicorigin.$invalid" class="help-block has-error ng-hide warnig">Please Select Your Ethnic Origin </span> 
                     <span class="help-block has-error ng-hide"  ng-show="ethnicorigindateError">{{ethnicorigindateError}}</span>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label class="col-sm-3 control-label">Disabilities<span class="req">*</span></label>
                                                <div class="col-sm-6 icheck">
                                                    <div class="square single-row">
                                                        <div class="">
                                                            <input tabindex="3" type="radio" name="disability" ng-model="user.disability" value="yes"  ng-click="showme=true" <?php echo  set_radio('disability', 'yes', TRUE); ?> required>
                                                            <label>Yes </label>
                                                        </div>
                                                        <div class="">
                                                            <input tabindex="3" type="radio" name="disability"  ng-model="user.disability" value="no"  ng-click="showme=false" <?php echo  set_radio('disability', 'no'); ?> required>
                                                            <label>No </label>
                                                            
                                                        </div>
                                                        <span ng-show="submitted && userProfile.disability.$invalid" class="help-block has-error ng-hide warnig">Please Select Your Disabilities Status</span>
                                                          <span class="help-block has-error ng-hide" ng-show="disabilityError">{{disabilityError}}</span>
                                                    </div>
                                                </div>
                                                 </div>
                                             
                                             
                                                    
                                                <div class="form-group" ng-show="showme==1">
                                                <label class="col-sm-3 control-label">If yes, please provide info</label>
                                                <div class="col-sm-6">
                                                    <textarea class="form-control" rows="3" ng-model="user.disabilityDesc" name="disabilityDesc" require ng-pattern="/.*[a-zA-Z]+.*/" ng-required="showme==1" ></textarea>


                                                    <span ng-show="submitted && userProfile.disabilityDesc.$invalid" class="help-block has-error ng-hide warnig">Please Describe Disabilities </span>
                                                          <span class="help-block has-error ng-hide" ng-show="disabilityDescError">{{disabilityDescError}}</span>
                                                </div>
                                                <div class="col-md-3">
                                                    <span class="help-block">Display additional instructions here.</span>
                                                </div>
                                            </div>

                                          
                                              <div class="form-group">
                                                <label class="col-sm-3 control-label">Desired Job Type<span class="req">*</span></label>
                                                <div class="col-sm-6 icheck">
                                                    <div class="square single-row">
                                                        <div class="">
                                                            <input tabindex="3" type="radio"  name="desiredJobtype" ng-model="user.desiredJobtype" value="Full Time"  <?php echo  set_radio('desiredJobtype', 'Full Time', TRUE); ?> required>
                                                            <label>Full time </label>
                                                        </div>
                                                        <div class="">
                                                            <input tabindex="3" type="radio"  name="desiredJobtype" ng-model="user.desiredJobtype" value="Part Time" <?php echo  set_radio('desiredJobtype', 'Part Time'); ?>  required>
                                                            <label>Part Time </label>
                                                            
                                                        </div>
                                                        <span ng-show="submitted && userProfile.permittedtowork.$invalid" class="help-block has-error ng-hide warnig">Please Select Permitted to work in UK and Ireland</span>
                                                          <span class="help-block has-error ng-hide" ng-show="permittedtoworkError">{{permittedtoworkError}}</span>
                                                    </div>
                                                </div>
                                            </div>
                                            

                                            <div class="form-group">
                                                <label class="col-sm-3 control-label">Permitted to work in UK and Ireland<span class="req">*</span></label>
                                                <div class="col-sm-6 icheck">
                                                    <div class="square single-row">
                                                        <div class="">
                                                            <input tabindex="3" type="radio"  name="permittedtowork" ng-model="user.permittedtowork" value="yes"  <?php echo  set_radio('permittedtowork', 'yes', TRUE); ?> required>
                                                            <label>Yes </label>
                                                        </div>
                                                        <div class="">
                                                            <input tabindex="3" type="radio"  name="permittedtowork" ng-model="user.permittedtowork" value="no" <?php echo  set_radio('permittedtowork', 'no'); ?>  required>
                                                            <label>No </label>
                                                            
                                                        </div>
                                                        <span ng-show="submitted && userProfile.permittedtowork.$invalid" class="help-block has-error ng-hide warnig">Please Select Permitted to work in UK and Ireland</span>
                                                          <span class="help-block has-error ng-hide" ng-show="permittedtoworkError">{{permittedtoworkError}}</span>
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="form-group last">
                                                <label class="control-label col-md-3">Job Type<span class="req">*</span></label>
                                                <div class="col-md-6">
                                                   <select multiple   id="e9" style="width:100%;" ng-model="user.jobtype"  name="jobtype[]" value="" class="populate m-bot15"   data-select-organisation required="">
                                                    <optgroup label="select organisations">

                                                    <?php if($job_type){

                                                          foreach ($job_type as $value) {
                                                              
                                                          ?>

                                                       
                                                         <option value="<?php echo $value->id; ?>"><?php echo $value->job_type; ?></option>
                                                        <?php } } ?>
                                                    </optgroup>
                                                </select>   

                                                      <span ng-show="submitted && userProfile.jobtype.$invalid" class="help-block has-error ng-hide warnig">Please Select jobtype</span>
                                                        <span class="help-block has-error ng-hide warnig" ng-show="jobtypeError">{{jobtypeError}}</span>
                                                       
                                                </div>
                                                
                                            </div>

                                            <div class="form-group">
                                                <div class="col-md-3">
                                                    <!-- <a href="" type="button" class="btn btn-danger btn-sm"><strong><i class="fa fa-arrow-left"></i> Back</strong></a>
                                                    <span class="help-block small text-muted">(Personal Information)</span> -->
                                                </div>
                                                <div class="col-md-9">
                                                    <button type="submit" class="btn btn-info pull-right btn-sm"  ng-click="submitted = true"><strong>Save &amp; Continue <i class="fa fa-arrow-right"></i></strong></button>
                                                    <span class="clearfix"></span>
                                                    <span class="help-block pull-right small text-muted">(Education)</span>
                                                </div>
                                            </div>
                                        </form>
                                    </div>
                                </section>
                            </div>
                        </div>
                        <!-- page end-->

                    </section>
                </section>
                <!--main content end-->





            <!--right sidebar start-->
            <div class="right-sidebar">
                <div class="search-row">
                    <input type="text" placeholder="Search" class="form-control">
                </div>
                <div class="right-stat-bar">
                    <ul class="right-side-accordion">
                        <li class="widget-collapsible">
                            <a href="#" class="head widget-head red-bg active clearfix">
                                <span class="pull-left">work progress (5)</span>
                                <span class="pull-right widget-collapse"><i class="ico-minus"></i></span>
                            </a>
                            <ul class="widget-container">
                                <li>
                                    <div class="prog-row side-mini-stat clearfix">
                                        <div class="side-graph-info">
                                            <h4>Target sell</h4>
                                            <p>
                                                25%, Deadline 12 june 13
                                            </p>
                                        </div>
                                        <div class="side-mini-graph">
                                            <div class="target-sell">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="prog-row side-mini-stat">
                                        <div class="side-graph-info">
                                            <h4>product delivery</h4>
                                            <p>
                                                55%, Deadline 12 june 13
                                            </p>
                                        </div>
                                        <div class="side-mini-graph">
                                            <div class="p-delivery">
                                                <div class="sparkline" data-type="bar" data-resize="true" data-height="30" data-width="90%" data-bar-color="#39b7ab" data-bar-width="5" data-data="[200,135,667,333,526,996,564,123,890,564,455]">
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="prog-row side-mini-stat">
                                        <div class="side-graph-info payment-info">
                                            <h4>payment collection</h4>
                                            <p>
                                                25%, Deadline 12 june 13
                                            </p>
                                        </div>
                                        <div class="side-mini-graph">
                                            <div class="p-collection">
                                              <span class="pc-epie-chart" data-percent="45">
                                                  <span class="percent"></span>
                                              </span>
                                          </div>
                                      </div>
                                  </div>
                                  <div class="prog-row side-mini-stat">
                                    <div class="side-graph-info">
                                        <h4>delivery pending</h4>
                                        <p>
                                            44%, Deadline 12 june 13
                                        </p>
                                    </div>
                                    <div class="side-mini-graph">
                                        <div class="d-pending">
                                        </div>
                                    </div>
                                </div>
                                <div class="prog-row side-mini-stat">
                                    <div class="col-md-12">
                                        <h4>total progress</h4>
                                        <p>
                                            50%, Deadline 12 june 13
                                        </p>
                                        <div class="progress progress-xs mtop10">
                                            <div style="width: 50%" aria-valuemax="100" aria-valuemin="0" aria-valuenow="20" role="progressbar" class="progress-bar progress-bar-info">
                                                <span class="sr-only">50% Complete</span>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </li>
                        </ul>
                    </li>
                    <li class="widget-collapsible">
                        <a href="#" class="head widget-head terques-bg active clearfix">
                            <span class="pull-left">contact online (5)</span>
                            <span class="pull-right widget-collapse"><i class="ico-minus"></i></span>
                        </a>
                        <ul class="widget-container">
                            <li>
                                <div class="prog-row">
                                    <div class="user-thumb">
                                        <a href="#"><img src="<?php echo base_url(); ?>user_assets/images/avatar1_small.jpg" alt=""></a>
                                    </div>
                                    <div class="user-details">
                                        <h4><a href="#">Jonathan Smith</a></h4>
                                        <p>
                                            Work for fun
                                        </p>
                                    </div>
                                    <div class="user-status text-danger">
                                        <i class="fa fa-comments-o"></i>
                                    </div>
                                </div>
                                <div class="prog-row">
                                    <div class="user-thumb">
                                        <a href="#"><img src="<?php echo base_url(); ?>user_assets/images/avatar1.jpg" alt=""></a>
                                    </div>
                                    <div class="user-details">
                                        <h4><a href="#">Anjelina Joe</a></h4>
                                        <p>
                                            Available
                                        </p>
                                    </div>
                                    <div class="user-status text-success">
                                        <i class="fa fa-comments-o"></i>
                                    </div>
                                </div>
                                <div class="prog-row">
                                    <div class="user-thumb">
                                        <a href="#"><img src="<?php echo base_url(); ?>user_assets/images/chat-avatar2.jpg" alt=""></a>
                                    </div>
                                    <div class="user-details">
                                        <h4><a href="#">John Doe</a></h4>
                                        <p>
                                            Away from Desk
                                        </p>
                                    </div>
                                    <div class="user-status text-warning">
                                        <i class="fa fa-comments-o"></i>
                                    </div>
                                </div>
                                <div class="prog-row">
                                    <div class="user-thumb">
                                        <a href="#"><img src="<?php echo base_url(); ?>user_assets/images/avatar1_small.jpg" alt=""></a>
                                    </div>
                                    <div class="user-details">
                                        <h4><a href="#">Mark Henry</a></h4>
                                        <p>
                                            working
                                        </p>
                                    </div>
                                    <div class="user-status text-info">
                                        <i class="fa fa-comments-o"></i>
                                    </div>
                                </div>
                                <div class="prog-row">
                                    <div class="user-thumb">
                                        <a href="#"><img src="<?php echo base_url(); ?>user_assets/images/avatar1.jpg" alt=""></a>
                                    </div>
                                    <div class="user-details">
                                        <h4><a href="#">Shila Jones</a></h4>
                                        <p>
                                            Work for fun
                                        </p>
                                    </div>
                                    <div class="user-status text-danger">
                                        <i class="fa fa-comments-o"></i>
                                    </div>
                                </div>
                                <p class="text-center">
                                    <a href="#" class="view-btn">View all Contacts</a>
                                </p>
                            </li>
                        </ul>
                    </li>
                    <li class="widget-collapsible">
                        <a href="#" class="head widget-head purple-bg active">
                            <span class="pull-left"> recent activity (3)</span>
                            <span class="pull-right widget-collapse"><i class="ico-minus"></i></span>
                        </a>
                        <ul class="widget-container">
                            <li>
                                <div class="prog-row">
                                    <div class="user-thumb rsn-activity">
                                        <i class="fa fa-clock-o"></i>
                                    </div>
                                    <div class="rsn-details ">
                                        <p class="text-muted">
                                            just now
                                        </p>
                                        <p>
                                            <a href="#">Jim Doe </a>Purchased new equipments for zonal office setup
                                        </p>
                                    </div>
                                </div>
                                <div class="prog-row">
                                    <div class="user-thumb rsn-activity">
                                        <i class="fa fa-clock-o"></i>
                                    </div>
                                    <div class="rsn-details ">
                                        <p class="text-muted">
                                            2 min ago
                                        </p>
                                        <p>
                                            <a href="#">Jane Doe </a>Purchased new equipments for zonal office setup
                                        </p>
                                    </div>
                                </div>
                                <div class="prog-row">
                                    <div class="user-thumb rsn-activity">
                                        <i class="fa fa-clock-o"></i>
                                    </div>
                                    <div class="rsn-details ">
                                        <p class="text-muted">
                                            1 day ago
                                        </p>
                                        <p>
                                            <a href="#">Jim Doe </a>Purchased new equipments for zonal office setup
                                        </p>
                                    </div>
                                </div>
                            </li>
                        </ul>
                    </li>
                    <li class="widget-collapsible">
                        <a href="#" class="head widget-head yellow-bg active">
                            <span class="pull-left"> shipment status</span>
                            <span class="pull-right widget-collapse"><i class="ico-minus"></i></span>
                        </a>
                        <ul class="widget-container">
                            <li>
                                <div class="col-md-12">
                                    <div class="prog-row">
                                        <p>
                                            Full sleeve baby wear (SL: 17665)
                                        </p>
                                        <div class="progress progress-xs mtop10">
                                            <div class="progress-bar progress-bar-success" role="progressbar" aria-valuenow="20" aria-valuemin="0" aria-valuemax="100" style="width: 40%">
                                                <span class="sr-only">40% Complete</span>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="prog-row">
                                        <p>
                                            Full sleeve baby wear (SL: 17665)
                                        </p>
                                        <div class="progress progress-xs mtop10">
                                            <div class="progress-bar progress-bar-info" role="progressbar" aria-valuenow="20" aria-valuemin="0" aria-valuemax="100" style="width: 70%">
                                                <span class="sr-only">70% Completed</span>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </li>
                        </ul>
                    </li>
                </ul>
            </div>
         </div>



