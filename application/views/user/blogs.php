
<style type="text/css">
  .freelancer-wrap.jobdesc .btn {
    position: initial;
  }
  .freelancer-wrap.jobdesc .btn::before {
    border: none;
  }
</style>

<div class="cd-hero-inner">
  <div class="container">
    <div class="row">
      <div class="col-md-6 col-sm-6">
        <!-- <h1>Welcome Jenny</h1> -->
      </div>
      <div class="col-md-6 col-sm-6">
        <div class="breadcmb"><a href="<?php echo base_url(); ?>user">Home</a> / <a href="<?php echo base_url(); ?>user/blog"><span>Blogs</span> </a></div>
      </div>
    </div>
  </div>
</div>

<div class="listpgWraper">
  <div class="container">
    
    <!-- Search Result and sidebar start -->
    <div class="row">
      <div class="col-md-12">
        <h3>Blogs</h3>
      </div>
      <div class="col-md-12 col-sm-12">

            <div class="row">
                <div class="col-md-4 col-sm-6 col-xs-12">
                        <img class="img-responsive" src="<?php echo base_url() ?>blogs_img/Blog2.jpg" alt="">
                    <div class="freelancer-wrap jobdesc row-fluid clearfix">
                        <div class="">
                            <h4 style="min-height: 50px;"><a href="<?php echo base_url(); ?>user/viewBlog">Web Developer / PHP &amp; Drupal Ninja!</a></h4>
                            <p class="skillsreq small">
                              <a href="#" class="small"><i class="fa fa-calendar"></i> 3-May-2017 2:34 PM</a>
                               &nbsp; &nbsp; <a href="#" class="small"><i class="fa fa-user"></i> Admin</a>
                            </p>
                            <hr style="margin: 5px 0;">
                            <p class="text-justify">Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam...</p>
                            <a href="#" class="btn btn-primary btn-sm btn-top pull-right">Read more <i class="fa fa-arrow-circle-right"></i></a>
                        </div><!-- end col -->
                    </div><!-- end freelancer-wrap -->
                </div><!-- end col -->

                <div class="col-md-4 col-sm-6 col-xs-12">
                        <img class="img-responsive" src="<?php echo base_url() ?>blogs_img/Blog2.jpg" alt="">
                    <div class="freelancer-wrap jobdesc row-fluid clearfix">
                        <div class="">
                            <h4 style="min-height: 50px;"><a href="<?php echo base_url(); ?>user/viewBlog">Web Developer / PHP &amp; Drupal Ninja!</a></h4>
                            <p class="skillsreq small">
                              <a href="#" class="small"><i class="fa fa-calendar"></i> 3-May-2017 2:34 PM</a>
                               &nbsp; &nbsp; <a href="#" class="small"><i class="fa fa-user"></i> Admin</a>
                            </p>
                            <hr style="margin: 5px 0;">
                            <p class="text-justify">Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam...</p>
                            <a href="#" class="btn btn-primary btn-sm btn-top pull-right">Read more <i class="fa fa-arrow-circle-right"></i></a>
                        </div><!-- end col -->
                    </div><!-- end freelancer-wrap -->
                </div><!-- end col -->

                <div class="col-md-4 col-sm-6 col-xs-12">
                        <img class="img-responsive" src="<?php echo base_url() ?>blogs_img/Blog2.jpg" alt="">
                    <div class="freelancer-wrap jobdesc row-fluid clearfix">
                        <div class="">
                            <h4 style="min-height: 50px;"><a href="#">Web Developer / PHP &amp; Drupal Ninja!</a></h4>
                            <p class="skillsreq small">
                              <a href="#" class="small"><i class="fa fa-calendar"></i> 3-May-2017 2:34 PM</a>
                               &nbsp; &nbsp; <a href="#" class="small"><i class="fa fa-user"></i> Admin</a>
                            </p>
                            <hr style="margin: 5px 0;">
                            <p class="text-justify">Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam...</p>
                            <a href="#" class="btn btn-primary btn-sm btn-top pull-right">Read more <i class="fa fa-arrow-circle-right"></i></a>
                        </div><!-- end col -->
                    </div><!-- end freelancer-wrap -->
                </div><!-- end col -->

                <div class="col-md-4 col-sm-6 col-xs-12">
                        <img class="img-responsive" src="<?php echo base_url() ?>blogs_img/Blog2.jpg" alt="">
                    <div class="freelancer-wrap jobdesc row-fluid clearfix">
                        <div class="">
                            <h4 style="min-height: 50px;"><a href="#">Web Developer / PHP &amp; Drupal Ninja!</a></h4>
                            <p class="skillsreq small">
                              <a href="#" class="small"><i class="fa fa-calendar"></i> 3-May-2017 2:34 PM</a>
                               &nbsp; &nbsp; <a href="#" class="small"><i class="fa fa-user"></i> Admin</a>
                            </p>
                            <hr style="margin: 5px 0;">
                            <p class="text-justify">Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam...</p>
                            <a href="#" class="btn btn-primary btn-sm btn-top pull-right">Read more <i class="fa fa-arrow-circle-right"></i></a>
                        </div><!-- end col -->
                    </div><!-- end freelancer-wrap -->
                </div><!-- end col -->

                <div class="col-md-4 col-sm-6 col-xs-12">
                        <img class="img-responsive" src="<?php echo base_url() ?>blogs_img/Blog2.jpg" alt="">
                    <div class="freelancer-wrap jobdesc row-fluid clearfix">
                        <div class="">
                            <h4 style="min-height: 50px;"><a href="#">Web Developer / PHP &amp; Drupal Ninja!</a></h4>
                            <p class="skillsreq small">
                              <a href="#" class="small"><i class="fa fa-calendar"></i> 3-May-2017 2:34 PM</a>
                               &nbsp; &nbsp; <a href="#" class="small"><i class="fa fa-user"></i> Admin</a>
                            </p>
                            <hr style="margin: 5px 0;">
                            <p class="text-justify">Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam...</p>
                            <a href="#" class="btn btn-primary btn-sm btn-top pull-right">Read more <i class="fa fa-arrow-circle-right"></i></a>
                        </div><!-- end col -->
                    </div><!-- end freelancer-wrap -->
                </div><!-- end col -->

                <div class="col-md-4 col-sm-6 col-xs-12">
                        <img class="img-responsive" src="<?php echo base_url() ?>blogs_img/Blog2.jpg" alt="">
                    <div class="freelancer-wrap jobdesc row-fluid clearfix">
                        <div class="">
                            <h4 style="min-height: 50px;"><a href="#">Web Developer / PHP &amp; Drupal Ninja!</a></h4>
                            <p class="skillsreq small">
                              <a href="#" class="small"><i class="fa fa-calendar"></i> 3-May-2017 2:34 PM</a>
                               &nbsp; &nbsp; <a href="#" class="small"><i class="fa fa-user"></i> Admin</a>
                            </p>
                            <hr style="margin: 5px 0;">
                            <p class="text-justify">Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam...</p>
                            <a href="#" class="btn btn-primary btn-sm btn-top pull-right">Read more <i class="fa fa-arrow-circle-right"></i></a>
                        </div><!-- end col -->
                    </div><!-- end freelancer-wrap -->
                </div><!-- end col -->
            </div>

            <div class="pagiWrap">
              <div class="row">
                <div class="col-md-2 col-sm-3 col-md-offset-6">
                  <div class="showreslt text-right">Showing 1-10</div>
                </div>
                <div class="col-md-4 col-sm-4 text-right">
                  <ul class="pagination">
                    <li><a href="#."><i class="fa fa-angle-left"></i></a></li>
                    <li class="active"><a href="#.">1</a></li>
                    <li><a href="#.">2</a></li>
                    <li><a href="#.">3</a></li>
                    <li><a href="#.">4</a></li>
                    <li><a href="#.">5</a></li>
                    <li><a href="#.">6</a></li>
                    <li><a href="#.">7</a></li>
                    <li><a href="#.">8</a></li>
                    <li><a href="#."><i class="fa fa-angle-right"></i></a></li>
                  </ul>
                </div>
              </div>
            </div>


      </div>
    </div>
  </div>
</div>

