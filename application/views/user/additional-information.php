<?php //echo "<pre>"; print_r($savedUserAdditionalInfo); exit; ?>
       <!--  <section class="container-fluid">
            <div class="row" id="topbanner">
                <div class="col-md-6 col-md-offset-3">
                    <h2 class="text-center">Customer Online Form</h2>
                    <p class="text-center">Lorem ipsum dolor sit amet, consectetur adipisicing elit.</p>
                </div>
            </div>

            <div class="row" id="formsteps">
                <div class="col-md-11 col-md-offset-1">
                    <ul class="breadcrumbs-alt">
                        <li>
                            <a href="<?php echo base_url(); ?>user/profile">Personal Information</a>
                        </li>
                        <li>
                            <a href="<?php echo base_url(); ?>user/education">Education</a>
                        </li>
                        <li>
                            <a href="<?php echo base_url(); ?>user/preEmployment">Pre-Employment</a>
                        </li>
                        <li>
                            <a href="<?php echo base_url(); ?>user/employment">Employment</a>
                        </li>
                        <li>
                            <a href="<?php echo base_url(); ?>user/reference">References</a>
                        </li>
                        <li>
                            <a class="current" href="<?php echo base_url(); ?>user/additionalInfo">Additional Information</a>
                        </li>
                        <li>
                            <a href="miscellaneous.php">Miscellaneous</a>
                        </li>
                        <li>
                            <a href="social-media.php">Social Media</a>
                        </li>
                    </ul>
                </div>
            </div>
        </section> -->

        <!--main content start-->
        
        <section id="" class="container">
            <section class="wrapper">
                <!-- page start-->
                <div class="row">
                    <div class="col-md-8 col-md-offset-2">
                        <h3 class="form-heading">Additional Information</h3>
                       
                        <section class="panel">
                            <div class="panel-body">
                                <form class="form-horizontal bucket-form" name="userAdditional" ng-submit="submitForm()" novalidate >
                                    <div class="form-group">
                                        <label class="col-sm-3 control-label">Favourite Subject at School?<span class="req">*</span></label>
                                        <div class="col-sm-9">
                                            <input type="text"  class="form-control" ng-model="userAdditionalInfo.favSub" name="favSub" require ng-pattern="/.*[a-zA-Z]+.*/" placeholder="Subject Name" required>
             <span ng-show="submittedd && userAdditional.favSub.$invalid" class="help-block has-error ng-hide warnig">Please enter your favourite subject name</span>
             <span class="help-block has-error ng-hide warnig" style="color: red;" ng-show="favSubError">{{favSubError}}</span>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-sm-3 control-label">After stating, explain why?<span class="req">*</span></label>
                                        <div class="col-sm-9">
                                            <textarea class="form-control" rows="2" ng-model="userAdditionalInfo.exp" name="exp" require ng-pattern="/.*[a-zA-Z]+.*/" required  ></textarea>
                                           
             <span ng-show="submittedd && userAdditional.exp.$invalid" class="help-block has-error ng-hide warnig">Please enter explanation</span>
             <span class="help-block has-error ng-hide warnig" style="color: red;" ng-show="expError">{{expError}}</span>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-sm-3 control-label">If you could learn a new subject, what and why?<span class="req">*</span></label>
                                        <div class="col-sm-9">
                                            <textarea class="form-control" ng-model="userAdditionalInfo.newSub" name="newSub" require ng-pattern="/.*[a-zA-Z]+.*/" required rows="2"></textarea>
                                <span ng-show="submittedd && userAdditional.newSub.$invalid" class="help-block has-error ng-hide warnig">Please enter your description</span>
             <span class="help-block has-error ng-hide warnig" style="color: red;" ng-show="newSubError">{{newSubError}}</span>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-sm-3 control-label">Dream job when younger and why?<span class="req">*</span></label>
                                        <div class="col-sm-9">
                                            <textarea class="form-control" ng-model="userAdditionalInfo.dreamJob" name="dreamJob" require ng-pattern="/.*[a-zA-Z]+.*/"  rows="2" required></textarea>


                                             <span ng-show="submittedd && userAdditional.dreamJob.$invalid" class="help-block has-error ng-hide warnig">Please enter your description</span>
             <span class="help-block has-error ng-hide warnig" style="color: red;" ng-show="dreamJobError">{{dreamJobError}}</span>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-sm-3 control-label">Significant long-term aspirations in life?<span class="req">*</span></label>
                                        <div class="col-sm-9">
                                            <textarea class="form-control" ng-model="userAdditionalInfo.significant" name="significant" require ng-pattern="/.*[a-zA-Z]+.*/" rows="2" required ></textarea>

                              <span ng-show="submittedd && userAdditional.significant.$invalid" class="help-block has-error ng-hide warnig">Please enter your description</span>
             <span class="help-block has-error ng-hide warnig" style="color: red;" ng-show="significantError">{{significantError}}</span>


                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-sm-3 control-label">Steps taken to achieve these<span class="req">*</span></label>
                                        <div class="col-sm-9">
                                            <textarea class="form-control" ng-model="userAdditionalInfo.achieve" name="achieve" require ng-pattern="/.*[a-zA-Z]+.*/"  rows="2" required ></textarea>

                                            <span ng-show="submittedd && userAdditional.achieve.$invalid" class="help-block has-error ng-hide warnig">Please enter your description</span>
             <span class="help-block has-error ng-hide warnig" style="color: red;" ng-show="achieveError">{{achieveError}}</span>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-sm-3 control-label">Biggest regret in life<span class="req">*</span></label>
                                        <div class="col-sm-9">
                                            <textarea class="form-control" ng-model="userAdditionalInfo.regret" name="regret" require ng-pattern="/.*[a-zA-Z]+.*/" rows="2" required></textarea>
                            <span ng-show="submittedd && userAdditional.regret.$invalid" class="help-block has-error ng-hide warnig">Please enter your description</span>
             <span class="help-block has-error ng-hide warnig" style="color: red;" ng-show="regretError">{{regretError}}</span>

                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-sm-3 control-label">If you had 24 hours to live, how would you spend it and what regrets would you have?<span class="req">*</span></label>
                                        <div class="col-sm-9">
                                            <textarea class="form-control" ng-model="userAdditionalInfo.live" name="live" rows="4" require ng-pattern="/.*[a-zA-Z]+.*/" required ></textarea>
                                <span ng-show="submittedd && userAdditional.live.$invalid" class="help-block has-error ng-hide warnig">Please enter your description</span>
             <span class="help-block has-error ng-hide warnig" style="color: red;" ng-show="liveError">{{liveError}}</span>
 

                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <div class="col-md-3">
                                            <a href="<?php echo base_url(); ?>user/reference" type="button" class="btn btn-danger btn-sm"><strong><i class="fa fa-arrow-left"></i> Back</strong></a>
                                            <span class="help-block small text-muted">(References)</span>
                                        </div>
                                        <div class="col-md-9">
                                            <button type="submit" class="btn btn-info pull-right btn-sm" ng-click="submittedd = true" ><strong>Save &amp; Continue <i class="fa fa-arrow-right"></i></strong></button>
                                            <span class="clearfix"></span>
                                            <span class="help-block pull-right small text-muted">(Miscellaneous)</span>
                                        </div>
                                    </div>
                                    
                                </form>
                            </div>
                        </section>
                    </div>
                </div>
                <!-- page end-->

            </section>
        </section>
        <!--main content end-->





    <!--right sidebar start-->
    <div class="right-sidebar">
        <div class="search-row">
            <input type="text" placeholder="Search" class="form-control">
        </div>
        <div class="right-stat-bar">
            <ul class="right-side-accordion">
                <li class="widget-collapsible">
                    <a href="#" class="head widget-head red-bg active clearfix">
                        <span class="pull-left">work progress (5)</span>
                        <span class="pull-right widget-collapse"><i class="ico-minus"></i></span>
                    </a>
                    <ul class="widget-container">
                        <li>
                            <div class="prog-row side-mini-stat clearfix">
                                <div class="side-graph-info">
                                    <h4>Target sell</h4>
                                    <p>
                                        25%, Deadline 12 june 13
                                    </p>
                                </div>
                                <div class="side-mini-graph">
                                    <div class="target-sell">
                                    </div>
                                </div>
                            </div>
                            <div class="prog-row side-mini-stat">
                                <div class="side-graph-info">
                                    <h4>product delivery</h4>
                                    <p>
                                        55%, Deadline 12 june 13
                                    </p>
                                </div>
                                <div class="side-mini-graph">
                                    <div class="p-delivery">
                                        <div class="sparkline" data-type="bar" data-resize="true" data-height="30" data-width="90%" data-bar-color="#39b7ab" data-bar-width="5" data-data="[200,135,667,333,526,996,564,123,890,564,455]">
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="prog-row side-mini-stat">
                                <div class="side-graph-info payment-info">
                                    <h4>payment collection</h4>
                                    <p>
                                        25%, Deadline 12 june 13
                                    </p>
                                </div>
                                <div class="side-mini-graph">
                                    <div class="p-collection">
                                      <span class="pc-epie-chart" data-percent="45">
                                          <span class="percent"></span>
                                      </span>
                                  </div>
                              </div>
                          </div>
                          <div class="prog-row side-mini-stat">
                            <div class="side-graph-info">
                                <h4>delivery pending</h4>
                                <p>
                                    44%, Deadline 12 june 13
                                </p>
                            </div>
                            <div class="side-mini-graph">
                                <div class="d-pending">
                                </div>
                            </div>
                        </div>
                        <div class="prog-row side-mini-stat">
                            <div class="col-md-12">
                                <h4>total progress</h4>
                                <p>
                                    50%, Deadline 12 june 13
                                </p>
                                <div class="progress progress-xs mtop10">
                                    <div style="width: 50%" aria-valuemax="100" aria-valuemin="0" aria-valuenow="20" role="progressbar" class="progress-bar progress-bar-info">
                                        <span class="sr-only">50% Complete</span>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </li>
                </ul>
            </li>
            <li class="widget-collapsible">
                <a href="#" class="head widget-head terques-bg active clearfix">
                    <span class="pull-left">contact online (5)</span>
                    <span class="pull-right widget-collapse"><i class="ico-minus"></i></span>
                </a>
                <ul class="widget-container">
                    <li>
                        <div class="prog-row">
                            <div class="user-thumb">
                                <a href="#"><img src="<?php echo base_url(); ?>user_assets/images/avatar1_small.jpg" alt=""></a>
                            </div>
                            <div class="user-details">
                                <h4><a href="#">Jonathan Smith</a></h4>
                                <p>
                                    Work for fun
                                </p>
                            </div>
                            <div class="user-status text-danger">
                                <i class="fa fa-comments-o"></i>
                            </div>
                        </div>
                        <div class="prog-row">
                            <div class="user-thumb">
                                <a href="#"><img src="<?php echo base_url(); ?>user_assets/images/avatar1.jpg" alt=""></a>
                            </div>
                            <div class="user-details">
                                <h4><a href="#">Anjelina Joe</a></h4>
                                <p>
                                    Available
                                </p>
                            </div>
                            <div class="user-status text-success">
                                <i class="fa fa-comments-o"></i>
                            </div>
                        </div>
                        <div class="prog-row">
                            <div class="user-thumb">
                                <a href="#"><img src="<?php echo base_url(); ?>user_assets/images/chat-avatar2.jpg" alt=""></a>
                            </div>
                            <div class="user-details">
                                <h4><a href="#">John Doe</a></h4>
                                <p>
                                    Away from Desk
                                </p>
                            </div>
                            <div class="user-status text-warning">
                                <i class="fa fa-comments-o"></i>
                            </div>
                        </div>
                        <div class="prog-row">
                            <div class="user-thumb">
                                <a href="#"><img src="<?php echo base_url(); ?>user_assets/images/avatar1_small.jpg" alt=""></a>
                            </div>
                            <div class="user-details">
                                <h4><a href="#">Mark Henry</a></h4>
                                <p>
                                    working
                                </p>
                            </div>
                            <div class="user-status text-info">
                                <i class="fa fa-comments-o"></i>
                            </div>
                        </div>
                        <div class="prog-row">
                            <div class="user-thumb">
                                <a href="#"><img src="<?php echo base_url(); ?>user_assets/images/avatar1.jpg" alt=""></a>
                            </div>
                            <div class="user-details">
                                <h4><a href="#">Shila Jones</a></h4>
                                <p>
                                    Work for fun
                                </p>
                            </div>
                            <div class="user-status text-danger">
                                <i class="fa fa-comments-o"></i>
                            </div>
                        </div>
                        <p class="text-center">
                            <a href="#" class="view-btn">View all Contacts</a>
                        </p>
                    </li>
                </ul>
            </li>
            <li class="widget-collapsible">
                <a href="#" class="head widget-head purple-bg active">
                    <span class="pull-left"> recent activity (3)</span>
                    <span class="pull-right widget-collapse"><i class="ico-minus"></i></span>
                </a>
                <ul class="widget-container">
                    <li>
                        <div class="prog-row">
                            <div class="user-thumb rsn-activity">
                                <i class="fa fa-clock-o"></i>
                            </div>
                            <div class="rsn-details ">
                                <p class="text-muted">
                                    just now
                                </p>
                                <p>
                                    <a href="#">Jim Doe </a>Purchased new equipments for zonal office setup
                                </p>
                            </div>
                        </div>
                        <div class="prog-row">
                            <div class="user-thumb rsn-activity">
                                <i class="fa fa-clock-o"></i>
                            </div>
                            <div class="rsn-details ">
                                <p class="text-muted">
                                    2 min ago
                                </p>
                                <p>
                                    <a href="#">Jane Doe </a>Purchased new equipments for zonal office setup
                                </p>
                            </div>
                        </div>
                        <div class="prog-row">
                            <div class="user-thumb rsn-activity">
                                <i class="fa fa-clock-o"></i>
                            </div>
                            <div class="rsn-details ">
                                <p class="text-muted">
                                    1 day ago
                                </p>
                                <p>
                                    <a href="#">Jim Doe </a>Purchased new equipments for zonal office setup
                                </p>
                            </div>
                        </div>
                    </li>
                </ul>
            </li>
            <li class="widget-collapsible">
                <a href="#" class="head widget-head yellow-bg active">
                    <span class="pull-left"> shipment status</span>
                    <span class="pull-right widget-collapse"><i class="ico-minus"></i></span>
                </a>
                <ul class="widget-container">
                    <li>
                        <div class="col-md-12">
                            <div class="prog-row">
                                <p>
                                    Full sleeve baby wear (SL: 17665)
                                </p>
                                <div class="progress progress-xs mtop10">
                                    <div class="progress-bar progress-bar-success" role="progressbar" aria-valuenow="20" aria-valuemin="0" aria-valuemax="100" style="width: 40%">
                                        <span class="sr-only">40% Complete</span>
                                    </div>
                                </div>
                            </div>
                            <div class="prog-row">
                                <p>
                                    Full sleeve baby wear (SL: 17665)
                                </p>
                                <div class="progress progress-xs mtop10">
                                    <div class="progress-bar progress-bar-info" role="progressbar" aria-valuenow="20" aria-valuemin="0" aria-valuemax="100" style="width: 70%">
                                        <span class="sr-only">70% Completed</span>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </li>
                </ul>
            </li>
        </ul>
    </div>
</div>

