<style type="text/css">
.help-block{
  color:red;

}
.profileimg {
  width:250px;
  height:250px;
  position: relative;
  display: block;
  margin: auto;
}
.editprofileimg {
  position: absolute;
  bottom: 46px;
  right: 46px;
  width: 40px;
  height: 40px;
  background-color: rgba(0, 0, 0, 0.7);
  color: #ccc;
  padding: 7px;
  font-size: 18px;
  text-align: center;
  border-radius: 4px;
}
.editprofileimg:hover {
  color: #fff;
  background-color: rgba(0, 0, 0, 1);
}

.cropit-preview 

{ 
  background-color: #f8f8f8; 
  background-size: cover;
  border: 1px solid #ccc;
  border-radius: 3px; 
  margin-top: 7px;
  width: 202px;
  height: 202px;

}
.cropit-preview-image-container 
{ 
  cursor: move; 
} 
.image-size-label 
{ 
  margin-top: 10px; 
} 
input,.export
{
 display: block; 
} 
button 
{ 
  margin-top: 10px; 
} 
.req{ 
  color:red; 
} 


.socialfgroup {
  position: relative;
}
.socialerror {
  bottom: -30px;
  position: absolute;
}
body {
  background: #fafafa;
}
.form-control {
  border: 1px solid #ddd;
  box-shadow: none;
}
.form-group.uploadblk {
    display: block;
    height: 45px;
    left: 15px;
    margin-top: 0;
    position: absolute;
    width: 100%;
}
.form-group.submitblk {
    margin-top: 50px;
}
</style>

<br><br><br><br>
<!--main content start-->
<section id="adminsection" class="container">
  <section class="wrapper">
    <!-- page start-->
    <div class="row">
      <div class="col-md-8 col-md-offset-2">
        <h3 class="form-heading"><strong>Homeless Refferal Form</strong></h3>
        
        <?php echo $this->session->flashdata('successmsg');?>
        <?php echo $this->session->flashdata('errormsg');?>
        <!-- <p>Create and add a new office. <a href="#" class="pull-right">Help <i class="fa fa-question-circle"></i></a></p> -->
        
        <section class="">
          <div class="">
            <div class="row">
              <section class="panel">
                <section class="panel-body">
                <form class="form-horizontal bucket-form" ng-submit="submitRefferalForm()" name="refferalForm" novalidate>
               <div class="col-md-12">
                 <h4>Referee Info</h4>
                 <hr>
               </div>
               

                   <div class="col-md-12">
                     <div class="form-group">
                       <label class="col-sm-3 col-sm-offset-1 control-label">First Name:</label>
                       <div class="col-sm-7">
                          <input type="text"  class="form-control" name="rfirst_name" ng-model="refferal.rfirst_name" placeholder="Please enter Firstname" required> 
                          <span ng-show="submitted && refferalForm.rfirst_name.$error.required"  class="help-block has-error ng-hide">First Name is required.</span>
                          <span ng-show="errorFirstName" class="help-block has-error ng-hide">{{errorFirstName}}</span>
                        </div>
                     </div>  

                      <div class="form-group">
                       <label class="col-sm-3 col-sm-offset-1 control-label">Last Name:</label>
                       <div class="col-sm-7">
                          <input type="text"  class="form-control" name="rlast_name" ng-model="refferal.rlast_name" placeholder="Please enter Lastname" required> 
                          <span ng-show="submitted && refferalForm.rlast_name.$error.required"  class="help-block has-error ng-hide">Last Name is required.</span>
                          <span ng-show="errorFirstName" class="help-block has-error ng-hide">{{errorLastName}}</span>
                        </div>
                     </div> 

                     <div class="form-group">
                       <label class="col-sm-3 col-sm-offset-1 control-label">Mobile No.:</label>
                       <div class="col-sm-7">
                          <input type="text"  class="form-control" name="rmobile" ng-model="refferal.rmobile" placeholder="Please enter Mobile No." required ng-pattern="/^\+?\d{2}[- ]?\d{3}[- ]?\d{5}$/"/ autofocus > 
                          <span ng-show="submitted && refferalForm.rmobile.$error.required"  class="help-block has-error ng-hide">Mobile No. is required.</span>
                          <span ng-show="errorFirstName" class="help-block has-error ng-hide">{{errorMobileNo}}</span>
                        </div>
                     </div> 
                       <div class="form-group">
                       <label class="col-sm-3 col-sm-offset-1 control-label">Can we contact you regarding below referral:</label>
                       <div class="col-sm-7">
                           <div class="col-sm-6 icheck">
                                                    <div class="square single-row">
                                                        <div class="">
                                                           
                                                             <input name="contact" type="radio" ng-model="refferal.contact" value="yes" <?php echo  set_radio('contact', 'yes', TRUE); ?> required>Yes<br/>
                                                           
                                                        </div>
                                                        <div class="">
                                                           
                                                            <input name="contact" type="radio" ng-model="refferal.contact" value="no" <?php echo  set_radio('contact', 'no'); ?> required>No<br/>
                                                           
                                                         
                                                        </div>
                                                        <span ng-show="submitted && refferalForm.contact.$invalid" class="help-block has-error ng-hide warnig">Field is required</span>
                                                        <span class="help-block has-error ng-hide warnig" style="color: red;" ng-show="genderError">{{genderError}}</span>
                                                    </div>
                                                </div>
                        </div>
                     </div>

                    <hr>
                   </div>
                <hr>

                  <div class="col-md-12">

                    <h4>Homeless Information</h4>
                    <hr>
                  </div>

                 <!--  <form class="form-horizontal bucket-form" ng-submit="submitRefferalForm()" name="refferalForm" novalidate> -->

                    <div class="col-md-12">
                      <div class="form-group">
                        <label class="col-sm-3 col-sm-offset-1 control-label">First Name:</label>
                        <div class="col-sm-7">
                          <input type="text"  class="form-control" name="first_name" ng-model="refferal.first_name" placeholder="Please enter Firstname" required> 
                          <span ng-show="submitted && refferalForm.first_name.$error.required"  class="help-block has-error ng-hide">First Name is required.</span>
                          <span ng-show="errorFirstName" class="help-block has-error ng-hide">{{errorFirstName}}</span>
                        </div>
                      </div>
                      <div class="form-group">
                        <label class="col-sm-3 col-sm-offset-1 control-label">Last Name:</label>
                        <div class="col-sm-7">
                          <input type="text"  class="form-control" name="last_name" ng-model="refferal.last_name" placeholder="Please enter Lastname" required>
                          <span ng-show="submitted && refferalForm.last_name.$error.required"  class="help-block has-error ng-hide">Last Name is required.</span>
                          <span ng-show="errorLastName" class="help-block has-error ng-hide">{{errorLastName}}</span>
                        </div>
                      </div>

                      <div class="form-group" >
                        <label class="control-label col-md-4">Date Of Birth<span class="req"></span></label>
                        <div class="col-md-6">
                         <div class="row">



                           <div class="col-md-4">
                            <select class="form-control m-bot15" ng-model="refferal.day" name="day" id="day" value="" required>
                              <option value="">Day</option>
                              <?php foreach ($days as $key ) { ?>

                              <option value="<?php echo $key->day; ?>" ><?php echo $key->day; ?></option>
                              <?php    } ?>

                                                        
                          </select>

                          <span ng-show="submitted && refferalForm.day.$invalid" class="help-block has-error ng-hide warnig">Please select day</span>
                          <span class="help-block has-error ng-hide warnig" style="color: red;" ng-show="dayError">{{dayError}}</span>                          
                        </div>

                        <div class="col-md-4">
                         <select class="form-control m-bot15" ng-model="refferal.month"  id="month" name="month" onChange="call();" required>

                          <option value="">Month</option>
                          <option value="1">Jan</option>
                          <option value="2">Feb</option>
                          <option value="3">Mar</option>
                          <option value="4">Apr</option>
                          <option value="5">May</option>
                          <option value="6">Jun</option>
                          <option value="7">Jul</option>
                          <option value="8">Aug</option>
                          <option value="9">Sep</option>
                          <option value="10">Oct</option>
                          <option value="11">Nov</option>
                          <option value="12">Dec</option>
                        </select>

                      <span ng-show="submitted && refferalForm.month.$invalid" class="help-block has-error ng-hide warnig">Please select month</span>
                      <span class="help-block has-error ng-hide warnig" style="color: red;" ng-show="monthError">{{monthError}}</span>
                    </div>

                    <div class="col-md-4">
                      <select class="form-control m-bot15" ng-model="refferal.year" name="year" id="year" value="" onchange="call();" required>

                        <option value="">Year</option>
                        <?php foreach ($years as $value) { ?>
                        <option value="<?php echo $value->year ?>"><?php echo $value->year ?></option>
                        <?php  } ?>

                      </select>
                      <span ng-show="submitted && refferalForm.year.$invalid" class="help-block has-error ng-hide warnig">Please select year</span>
                      <span class="help-block has-error ng-hide warnig" style="color: red;" ng-show="yearError">{{yearError}}</span>

                    </div>
                    <span id="errMsg" class="warnig"></span>

                  </div>
                </div>

              </div>

                                              <div class="form-group">
                                                <label class="col-sm-3 col-sm-offset-1 control-label">Telephone:</label>
                                                <div class="col-sm-7">
                                                  <input type="text"  class="form-control" name="telephone" ng-model="refferal.telephone" placeholder="Please enter Telephone" required>
                                                  <span ng-show="submitted && refferalForm.telephone.$error.required"  class="help-block has-error ng-hide">Telephone is required.</span>
                                                  <span ng-show="errorTelephone" class="help-block has-error ng-hide">{{errorTelephone}}</span>
                                                </div>
                                              </div>

                                              <div class="form-group">
                                                <label class="col-sm-3 col-sm-offset-1 control-label">Email:</label>
                                                <div class="col-sm-7">
                                                  <input type="text"  class="form-control" name="email" ng-model="refferal.email" placeholder="Please enter Email" required>
                                                  <span ng-show="submitted && refferalForm.email.$error.required"  class="help-block has-error ng-hide">Email is required.</span>
                                                  <span ng-show="errorEmail" class="help-block has-error ng-hide">{{errorEmail}}</span>
                                                </div>
                                              </div>
                                              <div class="form-group">
                                                <label class="col-sm-3 col-sm-offset-1 control-label">Country:</label>
                                                <div class="col-sm-7">
                                                  <select  class="form-control" name="country" ng-model="refferal.country" onchange="getState(this.value);" required>
                                                    <option selected="true" value=""></option>
                                                    <?php
                                                    foreach($allCountries as $key){

                                                      ?>
                                                      <option value="<?php echo $key->id;?>"><?php echo $key->name;?></option>
                                                      <?php }?>
                                                    </select>
                                                    <span ng-show="submitted && refferalForm.country.$error.required"  class="help-block has-error ng-hide">Country is required.</span>
                                                    <span ng-show="errorCountry" class="help-block has-error ng-hide">{{errorCountry}}</span>
                                                  </div>
                                                </div>

                                                <div class="form-group">
                                                  <label class="col-sm-3 col-sm-offset-1 control-label">State:</label>
                                                  <div class="col-sm-7">
                                                    <select  class="form-control" name="state" ng-model="refferal.state" id="state-list" onchange="getCity(this.value);" required>
                                                      <option selected="true" value=""></option>

                                                    </select>
                                                    <span ng-show="submitted && refferalForm.country.$error.required"  class="help-block has-error ng-hide">Country is required.</span>
                                                    <span ng-show="errorCountry" class="help-block has-error ng-hide">{{errorCountry}}</span>
                                                  </div>
                                                </div>
                                                <div class="form-group">
                                                  <label class="col-sm-3 col-sm-offset-1 control-label">City:</label>
                                                  <div class="col-sm-7">
                                                   <select  class="form-control" name="city" ng-model="refferal.city" id="city-list" required>
                                                    <option selected="true" value=""></option>

                                                  </select>
                                                  <span ng-show="submitted && refferalForm.city.$error.required"  class="help-block has-error ng-hide">City is required.</span>
                                                  <span ng-show="errorCity" class="help-block has-error ng-hide">{{errorCity}}</span>
                                                </div>
                                              </div>

                                              <div class="form-group">
                                                <label class="col-sm-3 col-sm-offset-1 control-label">Location:</label>
                                                <div class="col-sm-7">
                                                  <input type="text"  class="form-control" name="location" ng-model="refferal.location" placeholder="Please enter Location" required>
                                                  <span ng-show="submitted && refferalForm.location.$error.required"  class="help-block has-error ng-hide">Location is required.</span>
                                                  <span ng-show="errorLocation" class="help-block has-error ng-hide">{{errorLocation}}</span>
                                                </div>
                                              </div>

                                              <div class="form-group">
                                                <label class="col-sm-3 col-sm-offset-1 control-label">Notes:</label>
                                                <div class="col-sm-7">
                                                  <textarea rows="4" cols="4"  class="form-control" name="notes" ng-model="refferal.notes" placeholder="Please enter Notes" required style="resize:none;"></textarea>
                                                  <span ng-show="submitted && refferalForm.notes.$error.required"  class="help-block has-error ng-hide">Notes is required.</span>
                                                  <span ng-show="errorNotes" class="help-block has-error ng-hide">{{errorNotes}}</span>
                                                </div>
                                              </div>

                                     


                                              <div class="col-md-11">
                                                <div class="form-group submitblk">
                                                  <div class="">
                                                    <button type="submit" class="btn btn-info pull-right btn-sm" ng-click="submitted = true"><strong>Submit</strong></button><span class="pull-right"> &nbsp; &nbsp; </span>
                                                    <a href="<?php echo base_url();?>admin/viewProfile"><button type="button" class="btn btn-danger pull-right btn-sm"><strong><i class="fa fa-times"></i> Cancel</strong></button></a>
                                                  </div>
                                                </div>
                                              </div>
                                            </form>

                                                <!-- <div class="form-group uploadblk">
                                                <label class="control-label col-sm-3 col-sm-offset-1 text-right">Upload Image:</label>
                                                <div class="controls col-sm-7">

                                                <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#myModal">Upload <i class="fa fa-upload"></i></button> -->
                                                <!-- Modal -->
                                                <!-- <div id="myModal" class="modal fade" role="dialog">
                                                  <div class="modal-dialog"> -->

                                                    <!-- Modal content-->
                                                    <!-- <div class="modal-content">
                                                      <div class="modal-header">
                                                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                                                        <h4 class="modal-title">Upload Images</h4>
                                                      </div>
                                                      <div class="modal-body">
                                                        <div class="row">
                                                          <div class="col-md-12">
                                                            <h2></h2>
                                                            <form action="<?php echo base_url()?>user/upload_refferal_image" enctype="multipart/form-data" class="dropzone" id="image-upload">
                                                              <div>
                                                                <h3></h3>
                                                              </div>
                                                            </form>
                                                          </div>
                                                        </div>
                                                      </div>
                                                      <div class="modal-footer">
                                                        <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
                                                      </div>
                                                    </div>

                                                  </div>
                                                </div>

                                                </div> -->

                                              </div>

                                          </section>
                                        </section>
                                      </div>
                                      <br>
                                    </div>
                                  </section>
                                </div>
                              </div>
                              <!-- page end-->

                            </section>
                          </section>
                          <!--main content end-->










