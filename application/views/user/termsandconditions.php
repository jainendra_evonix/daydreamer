
<div class="cd-hero-inner">
  <div class="container">
    <div class="row">
      <div class="col-md-6 col-sm-6">
        <!-- <h1>Terms And Conditions</h1> -->
      </div>
      <div class="col-md-6 col-sm-6">
        <div class="breadcmb"><a href="index.php">Home</a> / <span>Terms And Conditions</span></div>
      </div>
    </div>
  </div>
</div>

<div class="listpgWraper">
  <div class="container"> 
    
    
    <!-- Search Result and sidebar start -->
    <div class="row">
      <div class="col-md-12 col-sm-12"> 
          <div class="freelancer-wrap row-fluid clearfix">
              <div class="col-md-12">
                  <h2 class="text-info"><strong>Terms And Conditions</strong></h2>
                  <h3 class="lead">This is the daydreamerproject.com website. Your use of this website is subject to the following terms and conditions; which you are deemed to accept each time you use this website.</h3>

                  <h3 class="lead"><i class="fa fa-angle-double-right"></i> <strong>Terms</strong></h3>
                  <h3 class="lead">“Daydreamer Project” refers to the project as a subsidiary of Daydreamer Ltd as its holding company (“holding company” and “subsidiary” having the meaning as defined by Section 736 of the Companies Act as amended by the Companies Act 1989)</h3>
                  <h3 class="lead">“You” means the person, firm, company or organisation browsing and/or using the daydreamerproject.com website,</h3>
                  <h3 class="lead">“Our”, “us” and similar variations refer to Daydreamer Ltd as a controlling commercial entity to this website.</h3>

                  <h3 class="lead"><i class="fa fa-angle-double-right"></i> <strong>Information on this Site</strong></h3>
                  <h3 class="lead">Whilst we make every effort to ensure that the information provided on this site is accurate and complete, some of the information supplied to us by 3rd parties by not have been fact-checked by our team, as we will have deemed the source trustworthy.  We accept no liability</h3>

                  <h3 class="lead"><i class="fa fa-angle-double-right"></i> <strong>Information You Provide</strong></h3>
                  <h3 class="lead">You are solely responsible for any information and the accuracy of such information that you provide us. Providing us with incorrect information may delay your applications & limit the service we can offer you. If information you provide is falsified, this may result in you being barred from using our services.</h3>
                  <h3 class="lead">The information you provide would be used for our client matching services, work placement, recruitment or administrative purposes. We may store sensitive personal data you provide digitally or in file form for reference purposes, and will always adhere to data protection at all times.</h3>
                  <h3 class="lead">All our members of staff sign our data protection policy and are fully aware of the consequences of a breach of data protection. We are also continually improving our data storing methods, to ensure the data you provide us with is more secure and readily accessible for your benefit.</h3>

                  <h3 class="lead"><i class="fa fa-angle-double-right"></i> <strong>Lawful Use</strong></h3>
                  <h3 class="lead">You may only use this Site for lawful purposes when seeking employment or for reference purposes to recommend information to fellow peers for positions of interest, you may not use this site for any purposes which may be deemed in any capacity as unlawful. Any such activity shall be reported to the relevant authorities and action taken.</h3>

                  <h3 class="lead"><i class="fa fa-angle-double-right"></i> <strong>Links</strong></h3>
                  <h3 class="lead">You may on this site (daydreamerproject.com) offer links to sites outside of our own, which may be of interest to you. We accept no responsibility/liability for any content on those sites and do not endorse or approve of the contents on any such sites. Clicking on links which take you to external sites is entirely at your own risk.</h3>
                  <h3 class="lead">We take great care to ensure all links to external sites on our own are accurate, however in some cases links may be broken; if this is the case please contact us on <a href="mailto:info@daydreamerproject.com">info@daydreamerproject.com</a> to advise.</h3>

                  <h3 class="lead"><i class="fa fa-angle-double-right"></i> <strong>Copyright</strong></h3>
                  <h3 class="lead">The rights on this site are protected by international copyright, software and trademark laws. By using this site, you agree to adhere to such rights and not to use this site in any way which would infringe these rights.</h3>
                  <h3 class="lead">You may download or print extracts of this site for your personal use online. Unless otherwise stated, the copyright and similar rights in this website are in all the material contained on this website belong to Daydreamer Ltd. You may not use any of this material for commercial or public purposes.</h3>
                  <h3 class="lead">You may not under any circumstances, whether directly or indirectly including through any programme copy/remanufacture/reproduce any of the content either in part of in full from this site. You may not transfer any of the content on this site to any transferable medium.</h3>
                  <h3 class="lead">In any such case, if you require any of the information from this site to be reproduced, contacts us on <a href="mailto:info@daydreamerproject.com">info@daydreamerproject.com</a>.</h3>

                  <h3 class="lead"><i class="fa fa-angle-double-right"></i> <strong>Liability</strong></h3>
                  <h3 class="lead">Daydreamer Ltd does not guarantee that the site will be error-free, uninterrupted, nor that it will provide specific results from the use of the site or any content, search or link on it.</h3>

                  <h3 class="lead"><i class="fa fa-angle-double-right"></i> <strong>Equal Opportunities Policy</strong></h3>
                  <h3 class="lead">We look to employ or assist all individuals find employment, regardless of gender, race, disability, age or sexual orientation. We do not discriminate against individuals or groups, support the Equality Act 2010 &amp; believe opportunities should be available to all without any exceptions.</h3>

                  <h3 class="lead"><i class="fa fa-angle-double-right"></i> Changes to Terms, Conditions &amp; Invalidity</h3>
                  <h3 class="lead">Daydreamer Project as an extension of Daydreamer Ltd reserves the right to change these Terms and Conditions at any time without prior notice. You will be deemed to accept the terms and conditions as amended following your future use of the site.</h3>
                  <br>
                  <blockquote>
                    <h3 class="lead"><em>Homelessness, seeing those in need and overall poverty ****ed me off a little, so I wanted to do a little something to help. One thing led to another and the Daydreamer Project was born. Our core values are still the same, but in order to be sustainable, we’ve had to change the business model slightly</em></h3>
                    <footer>
                      <span class="lead"><strong>Richard Narayan</strong></span>
                    </footer>
                  </blockquote>
              </div><!-- end col -->
          </div><!-- end freelancer-wrap -->
      </div>
    </div>
  </div>
</div>
