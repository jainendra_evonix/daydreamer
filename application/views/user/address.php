            <?php // echo "<pre>"; print_r($userSavedAddress); exit;
               // echo "<pre>"; print_r($years); exit; ?>
               <style>
                 .modal-backdrop{
                     position: absolute !important;
                     height: 0px !important;
                 } 
             </style>


             <!--main content start-->
             <section id="" class="container">
                <section class="wrapper">
                    <!-- page start-->
                    <div class="row">
                        <div class="col-md-8 col-md-offset-2">
                            <h3 class="form-heading">Address Information</h3>

                            <div class="row">
                                <div class="col-md-12">
                                    <!--collapse start-->

                                    <div class="panel-group m-bot20" id="accordionedu">
                                        <div class="panel">
                                            <div class="panel-heading heading-invert">
                                                <h4 class="panel-title">
                                                    <div class="row">
                                                        <div class="col-md-2">
                                                            <strong>Address </strong>
                                                        </div>
                                                        <div class="col-md-2">
                                                            <strong>City</strong>
                                                        </div>
                                                        <div class="col-md-3">
                                                            <strong>Country</strong>
                                                        </div>
                                                        <div class="col-md-3">
                                                            <strong>Status</strong>
                                                        </div>
                                                        <div class="col-md-2">
                                                            <strong>Active</strong>
                                                        </div>
                                                    </div>
                                                </h4>
                                            </div>
                                        </div>

                                        <?php if($userSavedAddress){ 
                                          error_reporting(0);
                                          foreach ($userSavedAddress as  $value) {


                                              ?>




                                              <div class="panel">
                                                <div class="panel-heading">
                                                    <h4 class="panel-title">
                                                        <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordionedu" href="<?php echo '#'.$value->id; ?>">
                                                            <div class="row">
                                                                <div class="col-md-2">
                                                                    <?php echo $value->add_l_1.'...'; ?>
                                                                </div>
                                                                <div class="col-md-2">
                                                                    <?php  echo $value->city;  ?>
                                                                </div>
                                                                <div class="col-md-3">
                                                                    <?php echo $value->country_name; ?>
                                                                </div>
                                                                <div class="col-md-3">
                                                                    <?php echo $value->status; ?>
                                                                </div>
                                                                <div class="col-md-2">
                                                                    <?php echo $value->addAct; ?>
                                                                </div>
                                                            </div>
                                                        </a>
                                                    </h4>
                                                </div>



                                                <div id="<?php echo $value->id; ?>" class="panel-collapse collapse">
                                                    <div class="panel-body">
                                                        <div class="weather-category twt-category">
                                                            <ul>
                                                                <li>
                                                                    Address line 1:
                                                                    <h5><?php echo $value->add_l_1; ?></h5>
                                                                </li>
                                                                <li>
                                                                    Address line 2:
                                                                    <h5><?php echo $value->add_l_2; ?></h5>
                                                                </li>
                                                                <li>
                                                                    Address line 3:
                                                                    <h5><?php echo $value->add_l_3; ?></h5>
                                                                </li>
                                                                <li>
                                                                    City:
                                                                    <h5><?php echo $value->city;?></h5>
                                                                </li>
                                                                <li>
                                                                   Country:
                                                                    <h5><?php echo $value->country_name; ?></h5>
                                                                </li>
                                                                <li>
                                                                    Address Type:
                                                                    <h5><?php echo $value->status; ?></h5>
                                                                </li>
                                                            </ul>
                                                        </div>
                                                        <span class="divider"></span>
                                                        <footer class="twt-footer">
                                                              
                                                                <span class="pull-right">
                                                                    <a  data ="<?php echo $value->id; ?>" data-toggle="modal" data-target="#myModal" href="javascript:void(0);" ng-click="editAddress(<?php echo $value->id; ?>);"><i class="fa fa-pencil"></i>Edit</a>&nbsp;

                                                                    <a data="<?php echo $value->id; ?>" data-toggle="modal" data-target="" href="javascript:void(0);" ng-click="deleteAddress(<?php echo $value->id; ?>);"><i class="fa fa-trash-o" aria-hidden="true"> Delete</i></a>

                                                                </span>
                                                            </footer>
                                                        </div>
                                                    </div>
                                                </div>
                                                <?php    } } ?>

                                            </div>
                                            <!--collapse end-->
                                        </div>
                                      </div>



                                        <!--  modal open-->

                                <form class="form-horizontal bucket-form" name="editUserAddress" ng-submit="submitEditForm();" novalidate >
                                            <div class="modal fade" id="myModal" role="dialog" style="background-color: rgba(0, 0, 0, 0.5);">
                                                <div class="modal-dialog modal-lg">

                                                  <!-- Modal content-->
                                                  <div class="modal-content">
                                                    <div class="modal-header">
                                                      <button type="button" class="close" data-dismiss="modal">&times;</button>
                                                      <h4 class="modal-title">Edit Address Info</h4>
                                                  </div>
                                                  <div class="modal-body">
                                                    <h4>Address</h4>

                                                    <div class="row">
                                                       <div class="col-md-3">
                                                           Address Line 1<span class="req">*</span>

                                                       </div>

                                                       <div class="col-md-9">
                                                        <input type="text"  class="form-control" ng-model="euserAddressModel.eaddLine1" name="eaddLine1" require ng-pattern="/.*[a-zA-Z]+.*/" placeholder="Address line 1" required>

                                               <input type="hidden"  ng-model="euserAddressModel.hiddenId" name="hiddenId" class="form-control">

                                                        
                                                       <span ng-show="submitted && editUserAddress.eaddLine1.$invalid" class="help-block has-error ng-hide warnig">Please enter your address line</span>
                                            <span class="help-block has-error ng-hide warnig" style="color: red;" ng-show="addLine1Error">
                               {{addLine1Error}}</span>
                                                    </div>
                                                </div>
                                                <br>

                                                <div class="row">
                                                   <div class="col-md-3">
                                                       Address Line 2<span class="req">*</span>
                                                     </div>
                                                     <div class="col-md-9">
                                                     <input type="text"  class="form-control" ng-model="euserAddressModel.eaddLine2" name="eaddLine2"  require ng-pattern="/.*[a-zA-Z]+.*/" placeholder=" Address line 2" required>
                                               <span ng-show="submitted && editUserAddress.eaddLine2.$invalid" class="help-block has-error ng-hide warnig">Please enter your address</span>
                     <span class="help-block has-error ng-hide warnig" style="color: red;" ng-show="eaddLine2Error">
                               {{eaddLine2Error}}</span>

                                                     </div>
                                                  
                                                </div>

                                                <br>
                                                  
                                                  <div class="row">
                                                   <div class="col-md-3">
                                                       Address Line 3<span class="req">*</span>
                                                     </div>
                                                     <div class="col-md-9">
                                                     <input type="text"  class="form-control" ng-model="euserAddressModel.eaddLine3" name="eaddLine3"  require ng-pattern="/.*[a-zA-Z]+.*/" placeholder=" Address line 3" required>
                                               <span ng-show="submitted && editUserAddress.eaddLine3.$invalid" class="help-block has-error ng-hide warnig">Please enter your address</span>
                     <span class="help-block has-error ng-hide warnig" style="color: red;" ng-show="eaddLine3Error">
                               {{eaddLine3Error}}</span>

                                                     </div>
                                                  
                                                </div>
                                                <br>

                                                <div class="row">
                                                   <div class="col-md-3">
                                                       Country<span class="req">*</span>

                                                   </div>
                                                  
                                                  <div class="col-md-9">
                                                    <select class="form-control m-bot15" ng-model="euserAddressModel.ecountry" name="ecountry" required>
                                                    <!-- <input type="text"  class="form-control" ng-model="userReferenceDetail.city" name="city"  placeholder="City"> -->
                                               <option value="" selected="selected">Select country</option>
                                                       <?php foreach ($countries as $key) {   ?>
                                                           
                                                     
                                                        <option value="<?php echo $key->id; ?>"><?php echo $key->country_name; ?></option>

                                                        <?php } ?>

                                                    </select>
                                                    <span ng-show="submitted && editUserAddress.ecountry.$invalid" class="help-block has-error ng-hide warnig">Please select your country name</span>
                                                  <span class="help-block has-error ng-hide warnig" style="color: red;" ng-show="ecountryError">
                                                     {{ecountryError}}</span>

                                                     </div>

                                                </div>

                                               
                                                <div class="row">
                                                   <div class="col-md-3">
                                                    City / County<span class="req">*</span>

                                                </div>
                                                <div class="col-md-9">
                                                     <input type="text"  class="form-control" ng-model="euserAddressModel.ecity" name="ecity"   require ng-pattern="/.*[a-zA-Z]+.*/" placeholder="City" required>
                                                     <span ng-show="submitted && editUserAddress.ecity.$invalid" class="help-block has-error ng-hide warnig">Please enter your city name</span>
                                         <span class="help-block has-error ng-hide warnig" style="color: red;" ng-show="ecityError">
                                                   {{ecityError}}</span>
                                                </div>
                                               
                                            </div>

                                            <br>
                                            <div class="row">
                                               <div class="col-md-3">
                                                  Resident Since<span class="req">*</span>

                                              </div>
                                              <div class="col-md-3">
                                               <select class="form-control m-bot15" ng-model="euserAddressModel.eday" name="eday" id="day" value="" required>
                                                                  <option value="">Day</option>
                                                                  <?php foreach ($days as $key ) { ?>

                                                                     <option value="<?php echo $key->day; ?>" ><?php echo $key->day; ?></option>
                                                               <?php    } ?>

                                                             
                                                          </select>

                                <span ng-show="submitted && editUserAddress.eday.$invalid" class="help-block has-error ng-hide warnig">Please select day</span>
                      <span class="help-block has-error ng-hide warnig" style="color: red;" ng-show="edayError">{{edayError}}</span>
                                          </div>
                                          <div class="col-md-3">
                                              
                                             <select class="form-control m-bot15" ng-model="euserAddressModel.emonth"  id="month" name="emonth" onChange="call();" required>
                                                                
                                                                  <option value="">Month</option>
                                                                   <option value="1">Jan</option>
                                                                   <option value="2">Feb</option>
                                                                   <option value="3">Mar</option>
                                                                   <option value="4">Apr</option>
                                                                   <option value="5">May</option>
                                                                   <option value="6">Jun</option>
                                                                   <option value="7">Jul</option>
                                                                   <option value="8">Aug</option>
                                                                   <option value="9">Sep</option>
                                                                   <option value="10">Oct</option>
                                                                   <option value="11">Nov</option>
                                                                   <option value="12">Dec</option>
                                                                 </select>

                     <span ng-show="submitted && editUserAddress.emonth.$invalid" class="help-block has-error ng-hide warnig">Please select month</span>
                     <span class="help-block has-error ng-hide warnig" style="color: red;" ng-show="emonthError">{{emonthError}}</span>

                                          </div>
                                           <div class="col-md-3">
                                               <select class="form-control m-bot15" ng-model="euserAddressModel.eyear" name="eyear" id="year" value="" onchange="call();" required>
                                                                   
                                                                    <option value="">Year</option>
                                                                       <?php foreach ($years as $value) { ?>
                                                                           <option value="<?php echo $value->year ?>"><?php echo $value->year ?></option>
                                                                      <?php  } ?>

                                                           </select>
                              <span ng-show="submitted && editUserAddress.eyear.$invalid" class="help-block has-error ng-hide warnig">Please select year</span>
                         <span class="help-block has-error ng-hide warnig" style="color: red;" ng-show="eyearError">{{eyearError}}</span>
                                          </div>


                                      </div>
                                      <br>
                                       <div class="row">
                                                   <div class="col-md-3">
                                                    Status?<span class="req">*</span>

                                                </div>
                                                <div class="col-md-9">
                                                    <div class="square single-row">
                                                            <div class="col-md-2">
                                                                
                                                                 <input name="estatus" type="radio" ng-model="euserAddressModel.estatus" value="Owned" <?php echo  set_radio('estatus', 'Owned'); ?> required>Owned<br/>
                                                               
                                                            </div>
                                                            <div class="col-md-2">
                                                                
                                                                <input name="estatus" type="radio" ng-model="euserAddressModel.estatus" value="Rented" <?php echo  set_radio('estatus', 'Rented'); ?> required>Rented<br/>
                                                              </div>
                                                             <div class="col-md-4">
                                                                
                                                                <input name="estatus" type="radio" ng-model="euserAddressModel.estatus" value="Living With Parents" <?php echo  set_radio('status', 'Living With Parents'); ?> required>Living With Parents<br/>
                                                               </div>
                                                            
                                                        </div>
                                                        <span ng-show="submitted && editUserAddress.estatus.$invalid" class="help-block has-error ng-hide warnig">Please Select Your Status</span>
                                                            <span class="help-block has-error ng-hide warnig" style="color: red;" ng-show="estatusError">{{estatusError}}</span>
                                                </div>
                                               
                                            </div>
                   <br>
                                             <div class="row">
                                                   <div class="col-md-3">
                                                    Active Address?<span class="req">*</span>

                                                </div>
                                                <div class="col-md-9">
                                                    <div class="square single-row">
                                                            <div class="col-md-2">
                                                                
                                                                 <input name="eactAdd" type="radio" ng-model="euserAddressModel.eactAdd" value="Yes" <?php echo  set_radio('eactAdd', 'Yes'); ?> required>Yes
                                                               
                                                            </div>
                                                            <div class="col-md-2">
                                                                
                                                                <input name="eactAdd" type="radio" ng-model="euserAddressModel.eactAdd" value="No" <?php echo  set_radio('eactAdd', 'No'); ?> required>No
                                                              </div>
                                                             
                                                            
                                                        </div>
                                                        <span ng-show="submitted && editUserAddress.eactAdd.$invalid" class="help-block has-error ng-hide warnig">Please Select Your Status</span>
                                                            <span class="help-block has-error ng-hide warnig" style="color: red;" ng-show="eactAddError">{{eactAddError}}</span>
                                                </div>
                                               
                                            </div>
                                      
                               
                            <div class="row">
                               <div class="col-md-12">
                                  <button type="submit" ng-click="submitted = true" class="btn btn-info pull-right btn-sm"><strong> Update <i class="fa fa-arrow-right"></i></strong></button>
                              </div>
                          </div>
                      </div>
                      <div class="modal-footer">
                          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                      </div>
                  </div>
                  
              </div>
          </div>
        </form>
        <!--  modal close-->


        

        <?php if($this->session->flashdata('succmsg')){  ?>

        <div class="alert alert-success">
           <strong>Success!</strong> <?php echo $this->session->flashdata('succmsg'); ?>
           <!-- <button type="button" class="close" data-dismiss="modal">&times;</button> -->
           <a type="button" class="close" data-dismiss="alert" aria-hidden="true">×</a>
        </div>

        <?php    } ?>
        <hr>

        <h3 class="form-heading">Add Address </h3>
        <section class="panel">
            <div class="panel-body">
                <form class="form-horizontal bucket-form" name="userAddress" ng-submit="submitForm()" novalidate  >
                                            <!-- <h4>For reference purposes</h4> -->
                                           
                                           
                                            <div class="form-group">
                                                <label class="col-sm-3 control-label">Address Line 1<span class="req">*</span></label>
                                                <div class="col-sm-6">
                                                    <input type="text"  class="form-control" ng-model="userAddressModel.addLine1" name="addLine1" require ng-pattern="/.*[a-zA-Z]+.*/" placeholder="Address line 1" required >

                                               <span ng-show="submittedd && userAddress.addLine1.$invalid" class="help-block has-error ng-hide warnig">Please enter your business detail</span>
                     <span class="help-block has-error ng-hide warnig" style="color: red;" ng-show="addLine1Error">
                               {{addLine1Error}}</span>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label class="col-sm-3 control-label">Address Line 2<span class="req">*</span></label>
                                                <div class="col-sm-6">
                                                 <input type="text"  class="form-control" ng-model="userAddressModel.addLine2" name="addLine2"  require ng-pattern="/.*[a-zA-Z]+.*/" placeholder=" Address line 2" required>
                                               <span ng-show="submittedd && userAddress.addLine2.$invalid" class="help-block has-error ng-hide warnig">Please enter your address</span>
                     <span class="help-block has-error ng-hide warnig" style="color: red;" ng-show="addLine2Error">
                               {{addLine2Error}}</span>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label class="col-sm-3 control-label">Address Line 3<span class="req">*</span></label>
                                                <div class="col-sm-6">
                                                    <input type="text"  class="form-control" ng-model="userAddressModel.addLine3" name="addLine3"  require ng-pattern="/.*[a-zA-Z]+.*/" placeholder="Address line 3" required>
                                                    <span ng-show="submittedd && userAddress.addLine3.$invalid" class="help-block has-error ng-hide warnig">Please enter your address</span>
                     <span class="help-block has-error ng-hide warnig" style="color: red;" ng-show="addLine3Error">
                               {{addLine3Error}}</span>
                                                </div>
                                            </div>

                                               <div class="form-group">
                                                <label class="col-sm-3 control-label">Country<span class="req">*</span></label>
                                                <div class="col-sm-6">
                                                    <!-- <input type="text"  class="form-control" placeholder="Country"> -->
                                                     <select class="form-control m-bot15" ng-model="userAddressModel.country" name="country" required>
                                                    <!-- <input type="text"  class="form-control" ng-model="userReferenceDetail.city" name="city"  placeholder="City"> -->
                                               <option value="" selected="selected">Select country</option>
                                                       <?php foreach ($countries as $key) {   ?>
                                                           
                                                     
                                                        <option value="<?php echo $key->id; ?>"><?php echo $key->country_name; ?></option>

                                                        <?php } ?>

                                                    </select>
                                                    <span ng-show="submittedd && userAddress.country.$invalid" class="help-block has-error ng-hide warnig">Please select your country name</span>
                                                  <span class="help-block has-error ng-hide warnig" style="color: red;" ng-show="countryError">
                                                     {{countryError}}</span>

                                                     
                                                </div>
                                            </div>

                                            <div class="form-group">
                                                <label class="col-sm-3 control-label">City / County<span class="req">*</span></label>
                                                <div class="col-sm-6">
                                                 <!-- <select class="form-control m-bot15" ng-model="userReferenceDetail.city" name="city" required> -->
                                                    <input type="text"  class="form-control" ng-model="userAddressModel.city" name="city"   require ng-pattern="/.*[a-zA-Z]+.*/" placeholder="City" required>
                                 <span ng-show="submittedd && userAddress.city.$invalid" class="help-block has-error ng-hide warnig">Please enter your city name</span>
                     <span class="help-block has-error ng-hide warnig" style="color: red;" ng-show="cityError">
                               {{cityError}}</span>
                                                </div>
                                            </div>



                                            <div class="form-group" >
            <label class="control-label col-md-3">Resident Since<span class="req">*</span></label>
              <div class="col-md-6">
                 <div class="row">
                   
                        

                         <div class="col-md-4">
                                   <select class="form-control m-bot15" ng-model="userAddressModel.day" name="day" id="day" value="" required>
                                                                  <option value="">Day</option>
                                                                  <?php foreach ($days as $key ) { ?>

                                                                     <option value="<?php echo $key->day; ?>" ><?php echo $key->day; ?></option>
                                                               <?php    } ?>

                                                              <!--  <option value="">Day</option>
                                                               <option value="29">29</option> -->
                                                          </select>

                                <span ng-show="submittedd && userAddress.day.$invalid" class="help-block has-error ng-hide warnig">Please select day</span>
                      <span class="help-block has-error ng-hide warnig" style="color: red;" ng-show="dayError">{{dayError}}</span>                          
                        </div>

                         <div class="col-md-4">
                         <select class="form-control m-bot15" ng-model="userAddressModel.month"  id="month" name="month" onChange="call();" required>
                                                                
                                                                  <option value="">Month</option>
                                                                   <option value="1">Jan</option>
                                                                   <option value="2">Feb</option>
                                                                   <option value="3">Mar</option>
                                                                   <option value="4">Apr</option>
                                                                   <option value="5">May</option>
                                                                   <option value="6">Jun</option>
                                                                   <option value="7">Jul</option>
                                                                   <option value="8">Aug</option>
                                                                   <option value="9">Sep</option>
                                                                   <option value="10">Oct</option>
                                                                   <option value="11">Nov</option>
                                                                   <option value="12">Dec</option>
                                                                 </select>

                     <span ng-show="submittedd && userAddress.month.$invalid" class="help-block has-error ng-hide warnig">Please select month</span>
                     <span class="help-block has-error ng-hide warnig" style="color: red;" ng-show="monthError">{{monthError}}</span>
                         </div>

                          <div class="col-md-4">
                              <select class="form-control m-bot15" ng-model="userAddressModel.year" name="year" id="year" value="" onchange="call();" required>
                                                                   
                                                                    <option value="">Year</option>
                                                                       <?php foreach ($years as $value) { ?>
                                                                           <option value="<?php echo $value->year ?>"><?php echo $value->year ?></option>
                                                                      <?php  } ?>

                                                           </select>
                              <span ng-show="submittedd && userAddress.year.$invalid" class="help-block has-error ng-hide warnig">Please select year</span>
                         <span class="help-block has-error ng-hide warnig" style="color: red;" ng-show="yearError">{{yearError}}</span>

                           </div>
                           <span id="errMsg" class="warnig"></span>

                     </div>
                     </div>

                 </div>

                                            <div class="form-group">
                                                    <label class="col-sm-3 control-label">Status?<span class="req">*</span></label>
                                                    <div class="col-sm-6 icheck">
                                                        <div class="square single-row">
                                                            <div class="">
                                                                
                                                                 <input name="status" type="radio" ng-model="userAddressModel.status" value="Owned" <?php echo  set_radio('status', 'Owned'); ?> required>Owned<br/>
                                                               
                                                            </div>
                                                            <div class="">
                                                                
                                                                <input name="status" type="radio" ng-model="userAddressModel.status" value="Rented" <?php echo  set_radio('status', 'Rented'); ?> required>Rented<br/>
                                                              </div>
                                                             <div class="">
                                                                
                                                                <input name="status" type="radio" ng-model="userAddressModel.status" value="Living With Parents" <?php echo  set_radio('status', 'Living With Parents'); ?> required>Living With Parents<br/>
                                                               </div>
                                                            
                                                        </div>
                                                        <span ng-show="submittedd && userAddress.status.$invalid" class="help-block has-error ng-hide warnig">Please Select Your Status</span>
                                                            <span class="help-block has-error ng-hide warnig" style="color: red;" ng-show="statusError">{{statusError}}</span>
                                                    </div>
                                                </div>
                                            
                                           <div class="form-group">
                                                    <label class="col-sm-3 control-label">Active Address<span class="req">*</span></label>
                                                    <div class="col-sm-6 icheck">
                                                        <div class="square single-row">
                                                            <div class="">
                                                                
                                                                 <input name="actAdd" type="radio" ng-model="userAddressModel.actAdd" value="Yes" <?php echo  set_radio('actAdd', 'Yes'); ?> required>Yes<br/>
                                                               
                                                            </div>
                                                            <div class="">
                                                                
                                                                <input name="actAdd" type="radio" ng-model="userAddressModel.actAdd" value="No" <?php echo  set_radio('actAdd', 'No'); ?> required>No<br/>
                                                              
                                                             
                                                            </div>
                                                             
                                                            
                                                        </div>
                                                        <span ng-show="submittedd && userAddress.actAdd.$invalid" class="help-block has-error ng-hide warnig">Please select your status </span>
                                                            <span class="help-block has-error ng-hide warnig" style="color: red;" ng-show="actAddError">{{actAddError}}</span>
                                                    </div>
                                                </div>

                                            
                                            <div class="form-group">
                                                <div class="col-md-9">
                                                    <!-- <button type="submit" ng-click="submittedd=true" class="btn btn-success btn-sm pull-right"><strong>Add More <i class="fa fa-plus"></i></strong></button> -->
                                                    <button class="btn btn-success btn-sm pull-right" ng-click="submittedd = true" ><strong>Add <i class="fa fa-plus"></i></strong></button>
                                                </div>
                                            </div>
                                            
                                            <div class="form-group">
                                                <div class="col-md-3">
                                                    <a href="<?php echo base_url(); ?>user/employment" type="button" class="btn btn-danger btn-sm"><strong><i class="fa fa-arrow-left"></i> Back</strong></a>
                                                    <span class="help-block small text-muted">(Personal Info)</span>
                                                </div>
                                                <div class="col-md-9">
                                                   <a href="<?php echo base_url(); ?>user/education"> <button type="submit" class="btn btn-info pull-right btn-sm"><strong>Save &amp; Continue <i class="fa fa-arrow-right"></i></strong></button></a>
                                                    <span class="clearfix"></span>
                                                    <span class="help-block pull-right small text-muted">(Education)</span>
                                                </div>
                                            </div>
                                            
                                        </form>
                                 </div>
                             </section>
                         </div>
                     </div>
                     <!-- page end-->

                 </section>
             </section>
             <!--main content end-->






             <!--right sidebar start-->
             <div class="right-sidebar">
                <div class="search-row">
                    <input type="text" placeholder="Search" class="form-control">
                </div>
                <div class="right-stat-bar">
                    <ul class="right-side-accordion">
                        <li class="widget-collapsible">
                            <a href="#" class="head widget-head red-bg active clearfix">
                                <span class="pull-left">work progress (5)</span>
                                <span class="pull-right widget-collapse"><i class="ico-minus"></i></span>
                            </a>
                            <ul class="widget-container">
                                <li>
                                    <div class="prog-row side-mini-stat clearfix">
                                        <div class="side-graph-info">
                                            <h4>Target sell</h4>
                                            <p>
                                                25%, Deadline 12 june 13
                                            </p>
                                        </div>
                                        <div class="side-mini-graph">
                                            <div class="target-sell">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="prog-row side-mini-stat">
                                        <div class="side-graph-info">
                                            <h4>product delivery</h4>
                                            <p>
                                                55%, Deadline 12 june 13
                                            </p>
                                        </div>
                                        <div class="side-mini-graph">
                                            <div class="p-delivery">
                                                <div class="sparkline" data-type="bar" data-resize="true" data-height="30" data-width="90%" data-bar-color="#39b7ab" data-bar-width="5" data-data="[200,135,667,333,526,996,564,123,890,564,455]">
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="prog-row side-mini-stat">
                                        <div class="side-graph-info payment-info">
                                            <h4>payment collection</h4>
                                            <p>
                                                25%, Deadline 12 june 13
                                            </p>
                                        </div>
                                        <div class="side-mini-graph">
                                            <div class="p-collection">
                                              <span class="pc-epie-chart" data-percent="45">
                                                  <span class="percent"></span>
                                              </span>
                                          </div>
                                      </div>
                                  </div>
                                  <div class="prog-row side-mini-stat">
                                    <div class="side-graph-info">
                                        <h4>delivery pending</h4>
                                        <p>
                                            44%, Deadline 12 june 13
                                        </p>
                                    </div>
                                    <div class="side-mini-graph">
                                        <div class="d-pending">
                                        </div>
                                    </div>
                                </div>
                                <div class="prog-row side-mini-stat">
                                    <div class="col-md-12">
                                        <h4>total progress</h4>
                                        <p>
                                            50%, Deadline 12 june 13
                                        </p>
                                        <div class="progress progress-xs mtop10">
                                            <div style="width: 50%" aria-valuemax="100" aria-valuemin="0" aria-valuenow="20" role="progressbar" class="progress-bar progress-bar-info">
                                                <span class="sr-only">50% Complete</span>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </li>
                        </ul>
                    </li>
                    <li class="widget-collapsible">
                        <a href="#" class="head widget-head terques-bg active clearfix">
                            <span class="pull-left">contact online (5)</span>
                            <span class="pull-right widget-collapse"><i class="ico-minus"></i></span>
                        </a>
                        <ul class="widget-container">
                            <li>
                                <div class="prog-row">
                                    <div class="user-thumb">
                                        <a href="#"><img src="<?php echo base_url(); ?>user_assets/images/avatar1_small.jpg" alt=""></a>
                                    </div>
                                    <div class="user-details">
                                        <h4><a href="#">Jonathan Smith</a></h4>
                                        <p>
                                            Work for fun
                                        </p>
                                    </div>
                                    <div class="user-status text-danger">
                                        <i class="fa fa-comments-o"></i>
                                    </div>
                                </div>
                                <div class="prog-row">
                                    <div class="user-thumb">
                                        <a href="#"><img src="<?php echo base_url(); ?>user_assets/images/avatar1.jpg" alt=""></a>
                                    </div>
                                    <div class="user-details">
                                        <h4><a href="#">Anjelina Joe</a></h4>
                                        <p>
                                            Available
                                        </p>
                                    </div>
                                    <div class="user-status text-success">
                                        <i class="fa fa-comments-o"></i>
                                    </div>
                                </div>
                                <div class="prog-row">
                                    <div class="user-thumb">
                                        <a href="#"><img src="<?php echo base_url(); ?>user_assets/images/chat-avatar2.jpg" alt=""></a>
                                    </div>
                                    <div class="user-details">
                                        <h4><a href="#">John Doe</a></h4>
                                        <p>
                                            Away from Desk
                                        </p>
                                    </div>
                                    <div class="user-status text-warning">
                                        <i class="fa fa-comments-o"></i>
                                    </div>
                                </div>
                                <div class="prog-row">
                                    <div class="user-thumb">
                                        <a href="#"><img src="<?php echo base_url(); ?>user_assets/images/avatar1_small.jpg" alt=""></a>
                                    </div>
                                    <div class="user-details">
                                        <h4><a href="#">Mark Henry</a></h4>
                                        <p>
                                            working
                                        </p>
                                    </div>
                                    <div class="user-status text-info">
                                        <i class="fa fa-comments-o"></i>
                                    </div>
                                </div>
                                <div class="prog-row">
                                    <div class="user-thumb">
                                        <a href="#"><img src="<?php echo base_url(); ?>user_assets/images/avatar1.jpg" alt=""></a>
                                    </div>
                                    <div class="user-details">
                                        <h4><a href="#">Shila Jones</a></h4>
                                        <p>
                                            Work for fun
                                        </p>
                                    </div>
                                    <div class="user-status text-danger">
                                        <i class="fa fa-comments-o"></i>
                                    </div>
                                </div>
                                <p class="text-center">
                                    <a href="#" class="view-btn">View all Contacts</a>
                                </p>
                            </li>
                        </ul>
                    </li>
                    <li class="widget-collapsible">
                        <a href="#" class="head widget-head purple-bg active">
                            <span class="pull-left"> recent activity (3)</span>
                            <span class="pull-right widget-collapse"><i class="ico-minus"></i></span>
                        </a>
                        <ul class="widget-container">
                            <li>
                                <div class="prog-row">
                                    <div class="user-thumb rsn-activity">
                                        <i class="fa fa-clock-o"></i>
                                    </div>
                                    <div class="rsn-details ">
                                        <p class="text-muted">
                                            just now
                                        </p>
                                        <p>
                                            <a href="#">Jim Doe </a>Purchased new equipments for zonal office setup
                                        </p>
                                    </div>
                                </div>
                                <div class="prog-row">
                                    <div class="user-thumb rsn-activity">
                                        <i class="fa fa-clock-o"></i>
                                    </div>
                                    <div class="rsn-details ">
                                        <p class="text-muted">
                                            2 min ago
                                        </p>
                                        <p>
                                            <a href="#">Jane Doe </a>Purchased new equipments for zonal office setup
                                        </p>
                                    </div>
                                </div>
                                <div class="prog-row">
                                    <div class="user-thumb rsn-activity">
                                        <i class="fa fa-clock-o"></i>
                                    </div>
                                    <div class="rsn-details ">
                                        <p class="text-muted">
                                            1 day ago
                                        </p>
                                        <p>
                                            <a href="#">Jim Doe </a>Purchased new equipments for zonal office setup
                                        </p>
                                    </div>
                                </div>
                            </li>
                        </ul>
                    </li>
                    <li class="widget-collapsible">
                        <a href="#" class="head widget-head yellow-bg active">
                            <span class="pull-left"> shipment status</span>
                            <span class="pull-right widget-collapse"><i class="ico-minus"></i></span>
                        </a>
                        <ul class="widget-container">
                            <li>
                                <div class="col-md-12">
                                    <div class="prog-row">
                                        <p>
                                            Full sleeve baby wear (SL: 17665)
                                        </p>
                                        <div class="progress progress-xs mtop10">
                                            <div class="progress-bar progress-bar-success" role="progressbar" aria-valuenow="20" aria-valuemin="0" aria-valuemax="100" style="width: 40%">
                                                <span class="sr-only">40% Complete</span>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="prog-row">
                                        <p>
                                            Full sleeve baby wear (SL: 17665)
                                        </p>
                                        <div class="progress progress-xs mtop10">
                                            <div class="progress-bar progress-bar-info" role="progressbar" aria-valuenow="20" aria-valuemin="0" aria-valuemax="100" style="width: 70%">
                                                <span class="sr-only">70% Completed</span>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </li>
                        </ul>
                    </li>
                </ul>
            </div>
        </div>

