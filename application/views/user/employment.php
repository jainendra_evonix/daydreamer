
        <!--main content start-->
        <section id="" class="container">
            <section class="wrapper">
                <!-- page start-->
                <div class="row">
                    <div class="col-md-8 col-md-offset-2">
                        <h3 class="form-heading">Employment Details</h3>
                        
                        <section class="panel">
                            <div class="panel-body">
                                <form class="form-horizontal bucket-form" name="userEmployment" ng-submit="submitForm()" novalidate >
                                    <div class="form-group">
                                        <label class="col-sm-3 control-label">Position Held<span class="req">*</span></label>
                                        <div class="col-sm-6">
                                            <input type="text"  class="form-control" value="" ng-model="employmentDetail.position" name="position" placeholder="Specify the position held in previous company" require ng-pattern="/.*[a-zA-Z]+.*/" required>

                                            <span ng-show="submitted && userEmployment.position.$invalid" class="help-block has-error ng-hide warnig">Please enter your position</span>
                                                    <span class="help-block has-error ng-hide warnig" style="color: red;" ng-show="positionError">{{positionError}}</span>
                                        </div>
                                        <div class="col-md-3">
                                            <span class="help-block">Display additional instructions here.</span>
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label class="col-sm-3 control-label">Company Name<span class="req">*</span></label>
                                        <div class="col-sm-6">
                                            <input type="text"  class="form-control" ng-model="employmentDetail.cmpnyName" name="cmpnyName" placeholder="current company name" value="" require ng-pattern="/.*[a-zA-Z]+.*/" required>
                                            <span ng-show="submitted && userEmployment.cmpnyName.$invalid" class="help-block has-error ng-hide warnig">Please enter company name</span>
                                            <span class="help-block has-error ng-hide warnig" style="color: red;" ng-show="cmpnyNameError">{{cmpnyNameError}}</span>

                                        </div>
                                        <div class="col-md-3">
                                            <span class="help-block">Display additional instructions here.</span>
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label class="control-label col-md-3">Date From<span class="req">*</span></label>
                                        
                                        <div class="col-sm-2 padingr">
                                            <select class="form-control m-bot15" value="" ng-model="employmentDetail.monthFrom"  name="monthFrom"  id="fromMonth" onchange="return checkFromMonth();" required>
                                                <option value="" selected="selected">Month</option>
                                              
                                                <?php foreach ($months as  $value) { ?>
                                                <option value="<?php echo $value->id; ?>"><?php echo $value->month_name; ?></option>
                                                    
                                           <?php    } ?>
                                            </select>
                                            <span ng-show="submitted && userEmployment.monthFrom.$invalid" class="help-block has-error ng-hide warnig">Please select month</span>
                                            <span class="help-block has-error ng-hide warnig" style="color: red;" ng-show="monthFromError">{{monthFromError}}</span>
                                        </div>
                                        
                                        <div class="col-sm-2 padingr">
                                            <select class="form-control m-bot15" value="" ng-model="employmentDetail.yearFrom" name="yearFrom" value="" id="fromYear" onchange="return checkFromDate();" required>
                                                <option value="" selected="selected">Year</option>
                                               <?php foreach ($years as  $value) { ?>
                                                <option value="<?php echo $value->id; ?>"><?php echo $value->year; ?></option>
                                                    
                                           <?php    } ?>
                                                
                                            </select>
                                            <span ng-show="submitted && userEmployment.yearFrom.$invalid" class="help-block has-error ng-hide warnig">Please select year</span>
                                            <span class="help-block has-error ng-hide warnig" style="color: red;" ng-show="yearFromError">{{yearFromError}}</span>
                                        </div>

                                    </div>
                                    <div class="form-group">
                                        <label class="control-label col-md-3">Date To<span class="req">*</span></label>
                                        
                                        <div class="col-sm-2 padingr">
                                            <select class="form-control m-bot15" ng-model="employmentDetail.toMonth" name="toMonth" required="" value="" id="toMonth" onchange="return checkToMonth();">
                                                <option value="" selected="selected">Month</option>
                                                <?php foreach ($months as  $value) { ?>
                                                <option value="<?php echo $value->id; ?>"><?php echo $value->month_name; ?></option>
                                                    
                                           <?php    } ?>
                                                
                                            </select>

                                             <span ng-show="submitted && userEmployment.toMonth.$invalid" class="help-block has-error ng-hide warnig">Please select month</span>
                                            <span class="help-block has-error ng-hide warnig" style="color: red;" ng-show="toMonthError">{{toMonthError}}</span>

                                        </div>

                                         <div class="col-sm-2 padingr">
                                            <select class="form-control m-bot15" value="" ng-model="employmentDetail.dateTo" name="dateTo" required="" onchange="return checkToDate();" id="toYear">
                                                <option value="" selected="selected">Year</option>
                                                <?php foreach ($years as  $value) { ?>
                                                <option value="<?php echo $value->id; ?>"><?php echo $value->year; ?></option>
                                                    
                                           <?php    } ?>
                                                
                                                
                                            </select>
                                            <span ng-show="submitted && userEmployment.dateTo.$invalid" class="help-block has-error ng-hide warnig">Please select year</span>
                                            <span class="help-block has-error ng-hide warnig" style="color: red;" ng-show="dateToError">{{dateToError}}</span>
                                        </div>
                                             
                                             <div class="col-md-3 padingr">
                                            <span class="help-block">Note:- To year select first</span>
                                        </div>

                                    </div>
                                    <div class="form-group">
                                        <label class="col-sm-3 control-label">Location, City<span class="req">*</span></label>
                                        <div class="col-sm-6">
                                            <input type="text"  class="form-control" ng-model="employmentDetail.userCity" name="userCity" placeholder="Enter city" require ng-pattern="/.*[a-zA-Z]+.*/" required="">
                                            <span ng-show="submitted && userEmployment.userCity.$invalid" class="help-block has-error ng-hide warnig">Please enter your city</span>
                                            <span class="help-block has-error ng-hide warnig" style="color: red;" ng-show="userCityError">{{userCityError}}</span>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-sm-3 control-label">Location, Country<span class="req">*</span></label>
                                        <div class="col-sm-6">
                                            <input type="text"  class="form-control" ng-model="employmentDetail.userCountry" name="userCountry" placeholder="Enter country" require ng-pattern="/.*[a-zA-Z]+.*/"  required="">

                                            <span ng-show="submitted && userEmployment.userCountry.$invalid" class="help-block has-error ng-hide warnig">Please enter country</span>
                                            <span class="help-block has-error ng-hide warnig" style="color: red;" ng-show="userCountryError">{{userCountryError}}</span>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-sm-3 control-label">Purpose of Role<span class="req">*</span></label>
                                        <div class="col-sm-6">
                                            <input type="text"  class="form-control" ng-model="employmentDetail.role" name="role" placeholder="Purpose of role" require ng-pattern="/.*[a-zA-Z]+.*/"  required="">
                                            <span ng-show="submitted && userEmployment.role.$invalid" class="help-block has-error ng-hide warnig">Please enter purpose of role</span>
                                            <span class="help-block has-error ng-hide warnig" style="color: red;" ng-show="roleError">{{roleError}}</span>

                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-sm-3 control-label">Key Responsibilities / Achievements<span class="req">*</span></label>
                                        <div class="col-sm-6">
                                            <textarea class="form-control" rows="3" ng-model="employmentDetail.respn" name="respn"  require ng-pattern="/.*[a-zA-Z]+.*/" required=""></textarea>

                                             <span ng-show="submitted && userEmployment.respn.$invalid" class="help-block has-error ng-hide warnig">Please enter Key responsibilities / achievements</span>
                                            <span class="help-block has-error ng-hide warnig" style="color: red;" ng-show="respnError">{{respnError}}</span>
                                        </div>
                                        <div class="col-md-3">
                                            <span class="help-block">Display additional instructions here.</span>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-sm-3 control-label">Internal Training<span class="req">*</span></label>
                                        <div class="col-sm-6">
                                            <!-- <input type="text"  class="form-control" placeholder="Internal training done"> -->
                                            <select class="form-control m-bot15" value="" ng-model="employmentDetail.training" name="training" required="">
                                                <option value="" selected="selected">Select training type</option>
                                                <?php foreach ($training as $value) { ?>

                                                     <option value="<?php echo $value->id; ?>"><?php echo $value->trng_name; ?></option>
                                           <?php      } ?>
                                                
                                                
                                                
                                            </select>
                                          <span ng-show="submitted && userEmployment.training.$invalid" class="help-block has-error ng-hide warnig">Please select internal training</span>
                                            <span class="help-block has-error ng-hide warnig" style="color: red;" ng-show="respnError">{{respnError}}</span>

                                        </div>
                                    </div>
                                   <!--  <div class="form-group">
                                        <div class="col-md-9">
                                            <button type="submit" class="btn btn-success btn-sm pull-right"><strong>Add More <i class="fa fa-plus"></i></strong></button>
                                        </div>
                                    </div> -->
                                    <div class="form-group">
                                        <div class="col-md-3">
                                            <a href="<?php echo base_url(); ?>user/preEmployment" type="button" class="btn btn-danger btn-sm"><strong><i class="fa fa-arrow-left"></i> Back</strong></a>
                                            <span class="help-block small text-muted">(Pre-Employment)</span>
                                        </div>
                                        <div class="col-md-9">
                                            <button type="submit" ng-click="submitted=true" class="btn btn-info pull-right btn-sm"><strong>Save &amp; Continue <i class="fa fa-arrow-right"></i></strong></button>
                                            <span class="clearfix"></span>
                                            <span class="help-block pull-right small text-muted">(References)</span>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </section>
                    </div>
                </div>
                <!-- page end-->

            </section>
        </section>
        <!--main content end-->





    <!--right sidebar start-->
    <div class="right-sidebar">
        <div class="search-row">
            <input type="text" placeholder="Search" class="form-control">
        </div>
        <div class="right-stat-bar">
            <ul class="right-side-accordion">
                <li class="widget-collapsible">
                    <a href="#" class="head widget-head red-bg active clearfix">
                        <span class="pull-left">work progress (5)</span>
                        <span class="pull-right widget-collapse"><i class="ico-minus"></i></span>
                    </a>
                    <ul class="widget-container">
                        <li>
                            <div class="prog-row side-mini-stat clearfix">
                                <div class="side-graph-info">
                                    <h4>Target sell</h4>
                                    <p>
                                        25%, Deadline 12 june 13
                                    </p>
                                </div>
                                <div class="side-mini-graph">
                                    <div class="target-sell">
                                    </div>
                                </div>
                            </div>
                            <div class="prog-row side-mini-stat">
                                <div class="side-graph-info">
                                    <h4>product delivery</h4>
                                    <p>
                                        55%, Deadline 12 june 13
                                    </p>
                                </div>
                                <div class="side-mini-graph">
                                    <div class="p-delivery">
                                        <div class="sparkline" data-type="bar" data-resize="true" data-height="30" data-width="90%" data-bar-color="#39b7ab" data-bar-width="5" data-data="[200,135,667,333,526,996,564,123,890,564,455]">
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="prog-row side-mini-stat">
                                <div class="side-graph-info payment-info">
                                    <h4>payment collection</h4>
                                    <p>
                                        25%, Deadline 12 june 13
                                    </p>
                                </div>
                                <div class="side-mini-graph">
                                    <div class="p-collection">
                                      <span class="pc-epie-chart" data-percent="45">
                                          <span class="percent"></span>
                                      </span>
                                  </div>
                              </div>
                          </div>
                          <div class="prog-row side-mini-stat">
                            <div class="side-graph-info">
                                <h4>delivery pending</h4>
                                <p>
                                    44%, Deadline 12 june 13
                                </p>
                            </div>
                            <div class="side-mini-graph">
                                <div class="d-pending">
                                </div>
                            </div>
                        </div>
                        <div class="prog-row side-mini-stat">
                            <div class="col-md-12">
                                <h4>total progress</h4>
                                <p>
                                    50%, Deadline 12 june 13
                                </p>
                                <div class="progress progress-xs mtop10">
                                    <div style="width: 50%" aria-valuemax="100" aria-valuemin="0" aria-valuenow="20" role="progressbar" class="progress-bar progress-bar-info">
                                        <span class="sr-only">50% Complete</span>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </li>
                </ul>
            </li>
            <li class="widget-collapsible">
                <a href="#" class="head widget-head terques-bg active clearfix">
                    <span class="pull-left">contact online (5)</span>
                    <span class="pull-right widget-collapse"><i class="ico-minus"></i></span>
                </a>
                <ul class="widget-container">
                    <li>
                        <div class="prog-row">
                            <div class="user-thumb">
                                <a href="#"><img src="<?php echo base_url(); ?>user_assets/images/avatar1_small.jpg" alt=""></a>
                            </div>
                            <div class="user-details">
                                <h4><a href="#">Jonathan Smith</a></h4>
                                <p>
                                    Work for fun
                                </p>
                            </div>
                            <div class="user-status text-danger">
                                <i class="fa fa-comments-o"></i>
                            </div>
                        </div>
                        <div class="prog-row">
                            <div class="user-thumb">
                                <a href="#"><img src="<?php echo base_url(); ?>user_assets/images/avatar1.jpg" alt=""></a>
                            </div>
                            <div class="user-details">
                                <h4><a href="#">Anjelina Joe</a></h4>
                                <p>
                                    Available
                                </p>
                            </div>
                            <div class="user-status text-success">
                                <i class="fa fa-comments-o"></i>
                            </div>
                        </div>
                        <div class="prog-row">
                            <div class="user-thumb">
                                <a href="#"><img src="<?php echo base_url(); ?>user_assets/images/chat-avatar2.jpg" alt=""></a>
                            </div>
                            <div class="user-details">
                                <h4><a href="#">John Doe</a></h4>
                                <p>
                                    Away from Desk
                                </p>
                            </div>
                            <div class="user-status text-warning">
                                <i class="fa fa-comments-o"></i>
                            </div>
                        </div>
                        <div class="prog-row">
                            <div class="user-thumb">
                                <a href="#"><img src="<?php echo base_url(); ?>user_assets/images/avatar1_small.jpg" alt=""></a>
                            </div>
                            <div class="user-details">
                                <h4><a href="#">Mark Henry</a></h4>
                                <p>
                                    working
                                </p>
                            </div>
                            <div class="user-status text-info">
                                <i class="fa fa-comments-o"></i>
                            </div>
                        </div>
                        <div class="prog-row">
                            <div class="user-thumb">
                                <a href="#"><img src="<?php echo base_url(); ?>user_assets/images/avatar1.jpg" alt=""></a>
                            </div>
                            <div class="user-details">
                                <h4><a href="#">Shila Jones</a></h4>
                                <p>
                                    Work for fun
                                </p>
                            </div>
                            <div class="user-status text-danger">
                                <i class="fa fa-comments-o"></i>
                            </div>
                        </div>
                        <p class="text-center">
                            <a href="#" class="view-btn">View all Contacts</a>
                        </p>
                    </li>
                </ul>
            </li>
            <li class="widget-collapsible">
                <a href="#" class="head widget-head purple-bg active">
                    <span class="pull-left"> recent activity (3)</span>
                    <span class="pull-right widget-collapse"><i class="ico-minus"></i></span>
                </a>
                <ul class="widget-container">
                    <li>
                        <div class="prog-row">
                            <div class="user-thumb rsn-activity">
                                <i class="fa fa-clock-o"></i>
                            </div>
                            <div class="rsn-details ">
                                <p class="text-muted">
                                    just now
                                </p>
                                <p>
                                    <a href="#">Jim Doe </a>Purchased new equipments for zonal office setup
                                </p>
                            </div>
                        </div>
                        <div class="prog-row">
                            <div class="user-thumb rsn-activity">
                                <i class="fa fa-clock-o"></i>
                            </div>
                            <div class="rsn-details ">
                                <p class="text-muted">
                                    2 min ago
                                </p>
                                <p>
                                    <a href="#">Jane Doe </a>Purchased new equipments for zonal office setup
                                </p>
                            </div>
                        </div>
                        <div class="prog-row">
                            <div class="user-thumb rsn-activity">
                                <i class="fa fa-clock-o"></i>
                            </div>
                            <div class="rsn-details ">
                                <p class="text-muted">
                                    1 day ago
                                </p>
                                <p>
                                    <a href="#">Jim Doe </a>Purchased new equipments for zonal office setup
                                </p>
                            </div>
                        </div>
                    </li>
                </ul>
            </li>
            <li class="widget-collapsible">
                <a href="#" class="head widget-head yellow-bg active">
                    <span class="pull-left"> shipment status</span>
                    <span class="pull-right widget-collapse"><i class="ico-minus"></i></span>
                </a>
                <ul class="widget-container">
                    <li>
                        <div class="col-md-12">
                            <div class="prog-row">
                                <p>
                                    Full sleeve baby wear (SL: 17665)
                                </p>
                                <div class="progress progress-xs mtop10">
                                    <div class="progress-bar progress-bar-success" role="progressbar" aria-valuenow="20" aria-valuemin="0" aria-valuemax="100" style="width: 40%">
                                        <span class="sr-only">40% Complete</span>
                                    </div>
                                </div>
                            </div>
                            <div class="prog-row">
                                <p>
                                    Full sleeve baby wear (SL: 17665)
                                </p>
                                <div class="progress progress-xs mtop10">
                                    <div class="progress-bar progress-bar-info" role="progressbar" aria-valuenow="20" aria-valuemin="0" aria-valuemax="100" style="width: 70%">
                                        <span class="sr-only">70% Completed</span>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </li>
                </ul>
            </li>
        </ul>
    </div>
</div>

