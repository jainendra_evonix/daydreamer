<?php //echo "<pre>"; print_r($userbenefits); exit; ?>

<style type="text/css">
    /*donesteps*/
    .breadcrumbs-alt a.donesteps {
        background: #66BB6A;
        color: #fff;
    }
    .breadcrumbs-alt a.donesteps:before {
        border-color: #66BB6A #66BB6A #66BB6A rgba(0,0,0,0);
        border-style: solid;
        border-width: 1.5em 0 1.7em 1em;
        content: "";
        left: -1em;
        margin-top: -1.6em;
        position: absolute;
        top: 50%;
        transition: all 0.3s ease-in-out 0s;
    }
    .breadcrumbs-alt a.donesteps:after {
        border-bottom: 1.5em solid rgba(0,0,0,0);
        border-left: 1em solid #66BB6A;
        border-top: 1.5em solid rgba(0,0,0,0);
        content: "";
        margin-top: -1.5em;
        position: absolute;
        right: -1em;
        top: 50%;
        transition: all 0.3s ease-in-out 0s;
    }


    /*pending*/
    .breadcrumbs-alt a.pending {
        background: #F44336;
        color: #fff;
    }
    .breadcrumbs-alt a.pending:before {
        border-color: #F44336 #F44336 #F44336 rgba(0,0,0,0);
        border-style: solid;
        border-width: 1.5em 0 1.7em 1em;
        content: "";
        left: -1em;
        margin-top: -1.6em;
        position: absolute;
        top: 50%;
        transition: all 0.3s ease-in-out 0s;
    }
    .breadcrumbs-alt a.pending:after {
        border-bottom: 1.5em solid rgba(0,0,0,0);
        border-left: 1em solid #F44336;
        border-top: 1.5em solid rgba(0,0,0,0);
        content: "";
        margin-top: -1.5em;
        position: absolute;
        right: -1em;
        top: 50%;
        transition: all 0.3s ease-in-out 0s;
    }

</style>

                <!-- <div class="row" id="topbanner">
                    <div class="col-md-6 col-md-offset-3">
                        <h2 class="text-center">Customer Online Form</h2>
                        <p class="text-center">Lorem ipsum dolor sit amet, consectetur adipisicing elit.</p>
                    </div>
                </div> -->
<div class="cd-hero-inner">
  <div class="container">
    <div class="row">
      <div class="col-md-6 col-sm-6">
       <h2 style="color:white;">Welcome,&nbsp;<?php echo $this->session->userdata('f_name'); ?></h2>
      </div>
      <div class="col-md-6 col-sm-6">
        <div class="breadcmb"><a href="<?php echo base_url(); ?>user/index">Home</a> / <span>Job Search</span></div>
      </div>
    </div>
  </div>
</div>
<section class="container-fluid">

                <div class="row" id="formsteps">
                    <div class="col-md-12">
                  <!--  style="padding-left: 4em;" -->
                        <ul class="breadcrumbs-alt" style="padding-left: 4em;" >
                            <li>
                                <a class="<?php if(@($current==1)){echo 'current';} else { if($profile==1){ echo "donesteps"; } } ?>" href="<?php if(@($profile==1)){echo base_url().'user/profile';}else{echo '#'; } ?>">Personal Information</a>
                            </li>
                            <li>
                            <a href="<?php if(@($additional_info==1)){echo base_url().'user/address' ;} else {echo '#'; } ?>" class="<?php if(@($current==10 )){echo 'current';} else { if($additional_info==1){ echo "donesteps"; }}  ?>">Address</a></li>
                            <!-- <li>
                                <a class="donesteps" href="<?php if(@($profile==1)){echo base_url().'user/profile';}else{echo '#'; } ?>">Personal</a>
                            </li> -->
                            <!-- <li>
                                <a class="pending" href="<?php if(@($profile==1)){echo base_url().'user/profile';}else{echo '#'; } ?>">Personal</a>
                            </li> -->
                            <li>
                                <a href="<?php if(@($education == 1)){ echo base_url().'user/education'; } else { echo '#'; }?>" class="<?php if(@($current==2)){echo 'current';} else { if($education==1){ echo "donesteps"; }} ?>" >Education</a>
                            </li>
                            <li>
                                <a href="<?php if(@($pre_employment==1)){ echo base_url().'user/preEmployment';}else{ echo '#' ; } ?>" class="<?php if(@($current==3 )){echo 'current';} else if($pre_employment==1){ { echo "donesteps"; }} ?>">Employment</a>
                            </li>

                             <?php if(!empty($userbenefits) ){ ?>

                               <li>
                                <a href="<?php  echo base_url().'user/recieptBenefits'; ?>" class="<?php if(@($current==4 )){echo 'current';} else {  echo "donesteps"; }  ?>">Reciept Benefits</a>
                            </li>
                            <?php  }?>
                            <!-- <li>
                                <a href="<?php if(@($employment==1)){ echo base_url().'user/employment'; } else{ echo '#'; } ?>" class="<?php if(@($current==4 )){echo 'current';} else {if($employment==1) {echo "donesteps";}  } ?>">Employment</a>
                            </li> -->
                              <li>
                                <a href="<?php if(@($resume==1)){echo base_url().'user/resume' ;}else{echo '#'; }  ?>" class="<?php if(@($current==5 )){echo 'current';} else { if($resume==1){ echo "donesteps"; }}  ?>">Resume</a>
                            </li>

                            <li>
                                <a href="<?php if(@($references==1)){echo base_url().'user/reference' ; } else { echo '#' ;} ?>" class="<?php if(@($current==6 )){echo 'current';} else { if($references==1){ echo "donesteps"; }} ?>">References</a>
                            </li>
                           <li>
                                <a href="<?php if(@($additional_info==1)){echo base_url().'user/additionalInfo' ;} else {echo '#'; } ?>" class="<?php if(@($current==7 )){echo 'current';} else { if($additional_info==1){ echo "donesteps"; }}  ?>">Additional Information</a>
                            </li>
                           <!--  <li>
                                <a href="<?php if(@($miscellaneous==1)){ echo base_url().'user/miscellaneous' ; } else{echo '#'; } ?>" class="<?php if(@($current==8 )){echo 'current';} else { if($miscellaneous==1){ echo "donesteps"; }} ?>">Miscellaneous</a>
                            </li> -->
                            <li>
                                <a href="<?php if(@($social_media==1)){echo base_url().'user/socialMedia' ;}else{echo '#'; }  ?>" class="<?php if(@($current==9 )){echo 'current';} else { if($social_media==1){ echo "donesteps"; }}  ?>">Social Media</a>
                            </li>
                           
                        </ul>
                    </div>
                </div>
            </section>