

       

        <!--main content start-->
        <section id="" class="container">
            <section class="wrapper">
                <!-- page start-->
                <div class="row">
                    <div class="col-md-8 col-md-offset-2">
                        <h3 class="form-heading">Social Media Information</h3>
                       <!--  <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit.</p> -->

                        <section class="panel">
                            <div class="panel-body">
                                <form class="form-horizontal bucket-form" name="userSocial" ng-submit="submitForm()" novalidate>
                                   
                                    <h4>Social Media Account Links</h4>
                                    <div class="form-group">
                                        <label class="control-label col-md-3">Facebook</label>
                                        <div class="col-md-9">
                                            <div class="input-group m-bot15">
                                                <span class="input-group-addon"><i class="fa fa-facebook"></i></span>
                                                <input type="url" class="form-control" ng-model="userMediaInfo.fb" name="fb"  placeholder="Facebook"  value="" >

                                                 <span ng-show="submittedd && userSocial.fb.$invalid" class="help-block has-error ng-hide warnig">Please enter your facebook profile link</span>
             <span class="help-block has-error ng-hide warnig" style="color: red;" ng-show="fbError">{{fbError}}</span>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="control-label col-md-3">Youtube</label>
                                        <div class="col-md-9">
                                            <div class="input-group m-bot15">
                                                <span class="input-group-addon"><i class="fa fa-youtube"></i></span>
                                                <input type="url" class="form-control" ng-model="userMediaInfo.youtube" name="youtube" placeholder="Youtube"  value="" >
                                                <span ng-show="submittedd && userSocial.youtube.$invalid" class="help-block has-error ng-hide warnig">Please enter your youtube profile link</span>
             <span class="help-block has-error ng-hide warnig" style="color: red;" ng-show="youtubeError">{{youtubeError}}</span>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="control-label col-md-3">Twitter</label>
                                        <div class="col-md-9">
                                            <div class="input-group m-bot15">
                                                <span class="input-group-addon"><i class="fa fa-twitter"></i></span>
                                                <input type="url" class="form-control" ng-model="userMediaInfo.twitter" name="twitter" value="" placeholder="Twitter" > 
                                        <span ng-show="submittedd && userSocial.twitter.$invalid" class="help-block has-error ng-hide warnig">Please enter your twitter profile link</span>
             <span class="help-block has-error ng-hide warnig" style="color: red;" ng-show="twitterError">{{twitterError}}</span>

                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="control-label col-md-3">LinkedIn</label>
                                        <div class="col-md-9">
                                            <div class="input-group m-bot15">
                                                <span class="input-group-addon"><i class="fa fa-linkedin"></i></span>
                                                <input type="url" class="form-control" ng-model="userMediaInfo.linkedIn" name="linkedIn" value=""  placeholder="LinkedIn" >
                                                 <span ng-show="submittedd && userSocial.linkedIn.$invalid" class="help-block has-error ng-hide warnig">Please enter your linkedIn profile link</span>
             <span class="help-block has-error ng-hide warnig" style="color: red;" ng-show="linkedInError">{{linkedInError}}</span>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="control-label col-md-3">Instagram</label>
                                        <div class="col-md-9">
                                            <div class="input-group m-bot15">
                                                <span class="input-group-addon"><i class="fa fa-instagram"></i></span>
                                                <input type="url" class="form-control" ng-model="userMediaInfo.instagram" name="instagram" value=""  placeholder="Instagram" >
                                    <span ng-show="submittedd && userSocial.instagram.$invalid" class="help-block has-error ng-hide warnig">Please enter your instagram profile link</span>
             <span class="help-block has-error ng-hide warnig" style="color: red;" ng-show="instagramError">{{instagramError}}</span>


                                            </div>
                                        </div>
                                    </div>

                                    <div class="form-group last">
                                        <label class="control-label col-md-3"><span class="label label-danger">NOTE!</span></label>
                                        <div class="col-md-9">
                                            
                                            <div class="help-block">"please manage your social media profiles to appear professional as employers often search the internet for candidates and may not be entirely happy with what they come across"</div>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <div class="col-md-3">
                                            <a href="<?php echo base_url(); ?>user/miscellaneous" type="button" class="btn btn-danger btn-sm"><strong><i class="fa fa-arrow-left"></i> Back</strong></a>
                                            <span class="help-block small text-muted">(Miscellaneous)</span>
                                        </div>
                                        <div class="col-md-9">
                                            <button type="submit" ng-click="submittedd=true" class="btn btn-info pull-right btn-sm"><strong>Save &amp; Finish <i class="fa fa-save"></i></strong></button>
                                            <span class="clearfix"></span>
                                            <!-- <span class="help-block pull-right small text-muted">(Social Media)</span> -->
                                        </div>
                                    </div>
                                    
                                </form>
                            </div>
                        </section>
                    </div>
                </div>
                <!-- page end-->

            </section>
        </section>
        <!--main content end-->





    <!--right sidebar start-->
    <div class="right-sidebar">
        <div class="search-row">
            <input type="text" placeholder="Search" class="form-control">
        </div>
        <div class="right-stat-bar">
            <ul class="right-side-accordion">
                <li class="widget-collapsible">
                    <a href="#" class="head widget-head red-bg active clearfix">
                        <span class="pull-left">work progress (5)</span>
                        <span class="pull-right widget-collapse"><i class="ico-minus"></i></span>
                    </a>
                    <ul class="widget-container">
                        <li>
                            <div class="prog-row side-mini-stat clearfix">
                                <div class="side-graph-info">
                                    <h4>Target sell</h4>
                                    <p>
                                        25%, Deadline 12 june 13
                                    </p>
                                </div>
                                <div class="side-mini-graph">
                                    <div class="target-sell">
                                    </div>
                                </div>
                            </div>
                            <div class="prog-row side-mini-stat">
                                <div class="side-graph-info">
                                    <h4>product delivery</h4>
                                    <p>
                                        55%, Deadline 12 june 13
                                    </p>
                                </div>
                                <div class="side-mini-graph">
                                    <div class="p-delivery">
                                        <div class="sparkline" data-type="bar" data-resize="true" data-height="30" data-width="90%" data-bar-color="#39b7ab" data-bar-width="5" data-data="[200,135,667,333,526,996,564,123,890,564,455]">
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="prog-row side-mini-stat">
                                <div class="side-graph-info payment-info">
                                    <h4>payment collection</h4>
                                    <p>
                                        25%, Deadline 12 june 13
                                    </p>
                                </div>
                                <div class="side-mini-graph">
                                    <div class="p-collection">
                                      <span class="pc-epie-chart" data-percent="45">
                                          <span class="percent"></span>
                                      </span>
                                  </div>
                              </div>
                          </div>
                          <div class="prog-row side-mini-stat">
                            <div class="side-graph-info">
                                <h4>delivery pending</h4>
                                <p>
                                    44%, Deadline 12 june 13
                                </p>
                            </div>
                            <div class="side-mini-graph">
                                <div class="d-pending">
                                </div>
                            </div>
                        </div>
                        <div class="prog-row side-mini-stat">
                            <div class="col-md-12">
                                <h4>total progress</h4>
                                <p>
                                    50%, Deadline 12 june 13
                                </p>
                                <div class="progress progress-xs mtop10">
                                    <div style="width: 50%" aria-valuemax="100" aria-valuemin="0" aria-valuenow="20" role="progressbar" class="progress-bar progress-bar-info">
                                        <span class="sr-only">50% Complete</span>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </li>
                </ul>
            </li>
            <li class="widget-collapsible">
                <a href="#" class="head widget-head terques-bg active clearfix">
                    <span class="pull-left">contact online (5)</span>
                    <span class="pull-right widget-collapse"><i class="ico-minus"></i></span>
                </a>
                <ul class="widget-container">
                    <li>
                        <div class="prog-row">
                            <div class="user-thumb">
                                <a href="#"><img src="<?php echo base_url(); ?>user_assets/images/avatar1_small.jpg" alt=""></a>
                            </div>
                            <div class="user-details">
                                <h4><a href="#">Jonathan Smith</a></h4>
                                <p>
                                    Work for fun
                                </p>
                            </div>
                            <div class="user-status text-danger">
                                <i class="fa fa-comments-o"></i>
                            </div>
                        </div>
                        <div class="prog-row">
                            <div class="user-thumb">
                                <a href="#"><img src="<?php echo base_url(); ?>user_assets/images/avatar1.jpg" alt=""></a>
                            </div>
                            <div class="user-details">
                                <h4><a href="#">Anjelina Joe</a></h4>
                                <p>
                                    Available
                                </p>
                            </div>
                            <div class="user-status text-success">
                                <i class="fa fa-comments-o"></i>
                            </div>
                        </div>
                        <div class="prog-row">
                            <div class="user-thumb">
                                <a href="#"><img src="<?php echo base_url(); ?>user_assets/images/chat-avatar2.jpg" alt=""></a>
                            </div>
                            <div class="user-details">
                                <h4><a href="#">John Doe</a></h4>
                                <p>
                                    Away from Desk
                                </p>
                            </div>
                            <div class="user-status text-warning">
                                <i class="fa fa-comments-o"></i>
                            </div>
                        </div>
                        <div class="prog-row">
                            <div class="user-thumb">
                                <a href="#"><img src="<?php echo base_url(); ?>user_assets/images/avatar1_small.jpg" alt=""></a>
                            </div>
                            <div class="user-details">
                                <h4><a href="#">Mark Henry</a></h4>
                                <p>
                                    working
                                </p>
                            </div>
                            <div class="user-status text-info">
                                <i class="fa fa-comments-o"></i>
                            </div>
                        </div>
                        <div class="prog-row">
                            <div class="user-thumb">
                                <a href="#"><img src="<?php echo base_url(); ?>user_assets/images/avatar1.jpg" alt=""></a>
                            </div>
                            <div class="user-details">
                                <h4><a href="#">Shila Jones</a></h4>
                                <p>
                                    Work for fun
                                </p>
                            </div>
                            <div class="user-status text-danger">
                                <i class="fa fa-comments-o"></i>
                            </div>
                        </div>
                        <p class="text-center">
                            <a href="#" class="view-btn">View all Contacts</a>
                        </p>
                    </li>
                </ul>
            </li>
            <li class="widget-collapsible">
                <a href="#" class="head widget-head purple-bg active">
                    <span class="pull-left"> recent activity (3)</span>
                    <span class="pull-right widget-collapse"><i class="ico-minus"></i></span>
                </a>
                <ul class="widget-container">
                    <li>
                        <div class="prog-row">
                            <div class="user-thumb rsn-activity">
                                <i class="fa fa-clock-o"></i>
                            </div>
                            <div class="rsn-details ">
                                <p class="text-muted">
                                    just now
                                </p>
                                <p>
                                    <a href="#">Jim Doe </a>Purchased new equipments for zonal office setup
                                </p>
                            </div>
                        </div>
                        <div class="prog-row">
                            <div class="user-thumb rsn-activity">
                                <i class="fa fa-clock-o"></i>
                            </div>
                            <div class="rsn-details ">
                                <p class="text-muted">
                                    2 min ago
                                </p>
                                <p>
                                    <a href="#">Jane Doe </a>Purchased new equipments for zonal office setup
                                </p>
                            </div>
                        </div>
                        <div class="prog-row">
                            <div class="user-thumb rsn-activity">
                                <i class="fa fa-clock-o"></i>
                            </div>
                            <div class="rsn-details ">
                                <p class="text-muted">
                                    1 day ago
                                </p>
                                <p>
                                    <a href="#">Jim Doe </a>Purchased new equipments for zonal office setup
                                </p>
                            </div>
                        </div>
                    </li>
                </ul>
            </li>
            <li class="widget-collapsible">
                <a href="#" class="head widget-head yellow-bg active">
                    <span class="pull-left"> shipment status</span>
                    <span class="pull-right widget-collapse"><i class="ico-minus"></i></span>
                </a>
                <ul class="widget-container">
                    <li>
                        <div class="col-md-12">
                            <div class="prog-row">
                                <p>
                                    Full sleeve baby wear (SL: 17665)
                                </p>
                                <div class="progress progress-xs mtop10">
                                    <div class="progress-bar progress-bar-success" role="progressbar" aria-valuenow="20" aria-valuemin="0" aria-valuemax="100" style="width: 40%">
                                        <span class="sr-only">40% Complete</span>
                                    </div>
                                </div>
                            </div>
                            <div class="prog-row">
                                <p>
                                    Full sleeve baby wear (SL: 17665)
                                </p>
                                <div class="progress progress-xs mtop10">
                                    <div class="progress-bar progress-bar-info" role="progressbar" aria-valuenow="20" aria-valuemin="0" aria-valuemax="100" style="width: 70%">
                                        <span class="sr-only">70% Completed</span>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </li>
                </ul>
            </li>
        </ul>
    </div>
</div>

