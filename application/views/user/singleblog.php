
<div class="cd-hero-inner">
  <div class="container">
    <div class="row">
      <div class="col-md-6 col-sm-6">
        <h1>PHP &amp; Drupal Ninja!</h1>
      </div>
      <div class="col-md-6 col-sm-6">
        <div class="breadcmb"><a href="<?php echo base_url();?>user">Home</a>/<a href="<?php echo base_url(); ?>user/blog"><span>Blogs</span></div>
      </div>
    </div>
  </div>
</div>

<div class="listpgWraper">
  <div class="container"> 
    
    
    <!-- Search Result and sidebar start -->
    <div class="row">
      <div class="col-md-3 col-sm-6 pull-right"> 
        <!-- Social Icons -->
        <form class="searchform">
          <div class="form-group input-group" style="z-index: 0;">
              <input type="text" class="form-control" placeholder="Blog, Topic, Keyword">
              <span class="input-group-btn">
                  <button class="btn btn-default" type="button" style="border-color: #3E3947;"><i class="fa fa-search"></i>
                  </button>
              </span>
          </div>
        </form>

        <div class="sidebar">
          <h4 class="widget-title">POPULAR CATEGORIES</h4>
    			<ul class="list-unstyled">
    				  <li><a href="#">Jpb Trends</a></li>
              <hr style="margin: 5px 0;">
    			    <li><a href="#">Companies</a></li>
              <hr style="margin: 5px 0;">
    			    <li><a href="#">IT Industry </a></li>
              <hr style="margin: 5px 0;">
    			    <li><a href="#">Technology</a></li>
              <hr style="margin: 5px 0;">
    			    <li><a href="#">Market Research</a></li>
              <hr style="margin: 5px 0;">
              <li><a href="#">Finance</a></li>
    			</ul>
        </div>
        
        <!-- Sponsord By -->
        <div class="sidebar">
          <h4 class="widget-title">POPULAR POST</h4>
		      <ul class="list-unstyled">
            	<li>
                <div class="row">
                  <div class="col-xs-4 col-sm-4 col-md-4 col-lg-4">
                    <img class="img-responsive" src="<?php echo base_url() ?>blogs_img/Blog2.jpg" alt="" style="margin-top: 5px;">
                  </div>
                  <div class="col-xs-8 col-sm-8 col-md-8 col-lg-8">
                    <a href="#" style="margin-top: 0;">Accounts &amp; Finance</a>
                    <p class="small">By: Admin</p>
                  </div>
                </div>
              </li>
              <hr style="margin: 5px 0 10px;">
              <li>
                <div class="row">
                  <div class="col-xs-4 col-sm-4 col-md-4 col-lg-4">
                    <img class="img-responsive" src="<?php echo base_url() ?>blogs_img/Blog2.jpg" alt="" style="margin-top: 5px;">
                  </div>
                  <div class="col-xs-8 col-sm-8 col-md-8 col-lg-8">
                    <a href="#" style="margin-top: 0;">PHP &amp; Drupal Ninja!</a>
                    <p class="small">By: Admin</p>
                  </div>
                </div>
              </li>
              <hr style="margin: 5px 0 10px;">
              <li>
                <div class="row">
                  <div class="col-xs-4 col-sm-4 col-md-4 col-lg-4">
                    <img class="img-responsive" src="<?php echo base_url() ?>blogs_img/Blog2.jpg" alt="" style="margin-top: 5px;">
                  </div>
                  <div class="col-xs-8 col-sm-8 col-md-8 col-lg-8">
                    <a href="#" style="margin-top: 0;">Web Developer</a>
                    <p class="small">By: Admin</p>
                  </div>
                </div>
              </li>
              <hr style="margin: 5px 0 10px;">
              <li>
                <div class="row">
                  <div class="col-xs-4 col-sm-4 col-md-4 col-lg-4">
                    <img class="img-responsive" src="<?php echo base_url() ?>blogs_img/Blog2.jpg" alt="" style="margin-top: 5px;">
                  </div>
                  <div class="col-xs-8 col-sm-8 col-md-8 col-lg-8">
                    <a href="#" style="margin-top: 0;">Advertise with us</a>
                    <p class="small">By: Admin</p>
                  </div>
                </div>
              </li>
              <hr style="margin: 5px 0 10px;">
              <li>
                <div class="row">
                  <div class="col-xs-4 col-sm-4 col-md-4 col-lg-4">
                    <img class="img-responsive" src="<?php echo base_url() ?>blogs_img/Blog2.jpg" alt="" style="margin-top: 5px;">
                  </div>
                  <div class="col-xs-8 col-sm-8 col-md-8 col-lg-8">
                    <a href="#" style="margin-top: 0;">Accounts &amp; Finance</a>
                    <p class="small">By: Admin</p>
                  </div>
                </div>
              </li>
          </ul>
        </div>

        <div class="sidebar">
          <h4 class="widget-title">ARCHIVES</h4>
          <ul class="list-unstyled">
              <li><a href="#">January 2017</a></li>
              <hr style="margin: 5px 0;">
              <li><a href="#">February 2017</a></li>
              <hr style="margin: 5px 0;">
              <li><a href="#">March 2017</a></li>
          </ul>
        </div>
      </div>
      <div class="col-md-9 col-sm-12"> 
        <!-- Search List -->
        <ul class="searchList list-unstyled">

          <!-- job start -->
          <li>
            <div class="row">
              <div class="col-md-12 col-sm-6 col-xs-12">
                    <img class="img-responsive" src="<?php echo base_url() ?>blogs_img/Blog2.jpg" alt="">
                    <div class="freelancer-wrap row-fluid clearfix">
                        <div class="col-md-12">
                            <h4><a href="#">Web Developer / PHP &amp; Drupal Ninja!</a></h4>
                            <p class="skillsreq small">
                              <a href="#" class="small"><i class="fa fa-calendar"></i> 3-May-2017 2:34 PM</a>
                               &nbsp; &nbsp; <a href="#" class="small"><i class="fa fa-user"></i> Admin</a>
                            </p>
                            <hr>
                            <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
                            tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,
                            quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo
                            consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse
                            cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non
                            proident, sunt in culpa qui officia deserunt mollit anim id est laborum. Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
                            tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,
                            quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo
                            consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse
                            cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non
                            proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>
                            <br>
                            <blockquote>
                              <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
                              tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,
                              quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo
                              consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse
                              cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non
                              proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>
                            </blockquote>

                            <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
                            tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,
                            quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo
                            consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse
                            cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non
                            proident, sunt in culpa qui officia deserunt mollit anim id est laborum. Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
                            tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,
                            quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo
                            consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse
                            cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non
                            proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>
                        </div><!-- end col -->
                    </div><!-- end freelancer-wrap -->
                </div><!-- end col -->
            </div>
          </li>
          <!-- job end -->
          
        </ul>
        
      </div>
    </div>
  </div>
</div>
