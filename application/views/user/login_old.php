
 <!--  <body class="login-body" ng-app="postApp" ng-controller="postController"> -->

    <div class="container">

      <form class="form-signin" name="userLogin"  ng-submit="submitForm()" novalidate>
        <h2 class="form-signin-heading"><strong>sign in now</strong></h2>
        <div id="message"></div>
        <div class="login-wrap">
            <div class="user-login-info">
                <input type="email" class="form-control" placeholder="Email ID"  ng-model="user.emailaddress" name="emailaddress" required autofocus autocomplete="off">
                <span ng-show="userLogin.emailaddress.$error.required" class="help-block has-error ng-hide">Valid Email ID is required.</span>
          <span class="help-block has-error ng-hide" ng-show="emailidError">{{emailidError}}</span>

      <br>

                <input type="password" class="form-control" placeholder="Password" name="password" ng-model="user.password"  required autocomplete="off" >
                <span ng-show="userLogin.password.$error.required" class="help-block has-error ng-hide">Valid Password is required.</span>
          <span class="help-block has-error ng-hide" ng-show="passwordError">{{passwordError}}</span>

            </div>
            <label class="checkbox">
                <input type="checkbox" value="remember-me"> Remember me
                <span class="pull-right">
                    <a data-toggle="modal" href="#myModal"> Forgot Password?</a>

                </span>
            </label>
            <button class="btn btn-lg btn-login btn-block" type="submit">Sign in</button>

            <div class="registration">
                Don't have an account yet?
                <a class="" href="<?php echo base_url(); ?>user/registration">
                    Create an account
                </a>
            </div>

        </div>

          <!-- Modal -->
          <div aria-hidden="true" aria-labelledby="myModalLabel" role="dialog" tabindex="-1" id="myModal" class="modal fade">
              <div class="modal-dialog">
                  <div class="modal-content">
                      <div class="modal-header">
                          <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                          <h4 class="modal-title">Forgot Password ?</h4>
                      </div>
                      <div class="modal-body">
                          <p>Enter your e-mail address below to reset your password.</p>
                          <input type="text" name="email" placeholder="Email" autocomplete="off" class="form-control placeholder-no-fix">

                      </div>
                      <div class="modal-footer">
                          <button data-dismiss="modal" class="btn btn-default" type="button">Cancel</button>
                          <button class="btn btn-success" type="submit" ng-click="submitted = true">Submit</button>
                      </div>
                  </div>
              </div>
          </div>
          <!-- modal -->

      </form>

    </div>


