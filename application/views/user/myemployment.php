
           <div class="row">
             <div class="col-md-8 col-md-offset-2">
                 <section class="panel">
                      <div class="panel-body">
                          <form class="form-horizontal bucket-form" name="userPreEmployment" ng-submit="submitForm()" novalidate >
                                     <div class="form-group">
                                                <label class="col-sm-3 control-label">Current Working<span class="req">*</span></label>
                                                <div class="col-sm-6 icheck">
                                                    <div class="square single-row">
                                                        <div class="">
                                                            
                                                             <input name="working" type="radio" ng-model="user.working" value="Yes" ng-click="showme=true" <?php echo  set_radio('working', 'Yes', TRUE); ?> required>Yes<br/>
                                                          
                                                        </div>
                                                        <div class="">
                                                            
                                                            <input name="working" type="radio" ng-model="user.working" value="No" ng-click="showme=false" <?php echo  set_radio('working', 'No'); ?> required>No<br/>
                                                           
                                                         
                                                        </div>
                                                        <span ng-show="submitted && userProfile.gender.$invalid" class="help-block has-error ng-hide warnig">Please Select Your Working Status</span>
                                                        <span class="help-block has-error ng-hide warnig" style="color: red;" ng-show="workingError">{{workingError}}</span>
                                                    </div>
                                                </div>
                                            </div>


                       <div  ng-show="showme==1">
                           <h3 class="form-heading">Employment Details</h3>
                                
                           
                                    <div class="form-group">
                                        <label class="col-sm-3 control-label">Position Held<span class="req">*</span></label>
                                        <div class="col-sm-6">
                                            <input type="text"  class="form-control" value="" ng-model="employmentDetail.position" name="position" placeholder="Specify the position held in previous company" require ng-pattern="/.*[a-zA-Z]+.*/" required>

                                            <span ng-show="submitted && userEmployment.position.$invalid" class="help-block has-error ng-hide warnig">Please enter your position</span>
                                                    <span class="help-block has-error ng-hide warnig" style="color: red;" ng-show="positionError">{{positionError}}</span>
                                        </div>
                                        <div class="col-md-3">
                                            <span class="help-block">Display additional instructions here.</span>
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label class="col-sm-3 control-label">Company Name<span class="req">*</span></label>
                                        <div class="col-sm-6">
                                            <input type="text"  class="form-control" ng-model="employmentDetail.cmpnyName" name="cmpnyName" placeholder="current company name" value="" require ng-pattern="/.*[a-zA-Z]+.*/" required>
                                            <span ng-show="submitted && userEmployment.cmpnyName.$invalid" class="help-block has-error ng-hide warnig">Please enter company name</span>
                                            <span class="help-block has-error ng-hide warnig" style="color: red;" ng-show="cmpnyNameError">{{cmpnyNameError}}</span>

                                        </div>
                                        <div class="col-md-3">
                                            <span class="help-block">Display additional instructions here.</span>
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label class="control-label col-md-3">Date From<span class="req">*</span></label>
                                        
                                        <div class="col-sm-2 padingr">
                                            <select class="form-control m-bot15" value="" ng-model="employmentDetail.monthFrom"  name="monthFrom"  id="fromMonth" onchange="return checkFromMonth();" required>
                                                <option value="" selected="selected">Month</option>
                                              
                                                <?php foreach ($months as  $value) { ?>
                                                <option value="<?php echo $value->id; ?>"><?php echo $value->month_name; ?></option>
                                                    
                                           <?php    } ?>
                                            </select>
                                            <span ng-show="submitted && userEmployment.monthFrom.$invalid" class="help-block has-error ng-hide warnig">Please select month</span>
                                            <span class="help-block has-error ng-hide warnig" style="color: red;" ng-show="monthFromError">{{monthFromError}}</span>
                                        </div>
                                        
                                        <div class="col-sm-2 padingr">
                                            <select class="form-control m-bot15" value="" ng-model="employmentDetail.yearFrom" name="yearFrom" value="" id="fromYear" onchange="return checkFromDate();" required>
                                                <option value="" selected="selected">Year</option>
                                               <?php foreach ($years as  $value) { ?>
                                                <option value="<?php echo $value->id; ?>"><?php echo $value->year; ?></option>
                                                    
                                           <?php    } ?>
                                                
                                            </select>
                                            <span ng-show="submitted && userEmployment.yearFrom.$invalid" class="help-block has-error ng-hide warnig">Please select year</span>
                                            <span class="help-block has-error ng-hide warnig" style="color: red;" ng-show="yearFromError">{{yearFromError}}</span>
                                        </div>

                                    </div>
                                    <div class="form-group">
                                        <label class="control-label col-md-3">Date To<span class="req">*</span></label>
                                        
                                        <div class="col-sm-2 padingr">
                                            <select class="form-control m-bot15" ng-model="employmentDetail.toMonth" name="toMonth" required="" value="" id="toMonth" onchange="return checkToMonth();">
                                                <option value="" selected="selected">Month</option>
                                                <?php foreach ($months as  $value) { ?>
                                                <option value="<?php echo $value->id; ?>"><?php echo $value->month_name; ?></option>
                                                    
                                           <?php    } ?>
                                                
                                            </select>

                                             <span ng-show="submitted && userEmployment.toMonth.$invalid" class="help-block has-error ng-hide warnig">Please select month</span>
                                            <span class="help-block has-error ng-hide warnig" style="color: red;" ng-show="toMonthError">{{toMonthError}}</span>

                                        </div>

                                         <div class="col-sm-2 padingr">
                                            <select class="form-control m-bot15" value="" ng-model="employmentDetail.dateTo" name="dateTo" required="" onchange="return checkToDate();" id="toYear">
                                                <option value="" selected="selected">Year</option>
                                                <?php foreach ($years as  $value) { ?>
                                                <option value="<?php echo $value->id; ?>"><?php echo $value->year; ?></option>
                                                    
                                           <?php    } ?>
                                                
                                                
                                            </select>
                                            <span ng-show="submitted && userEmployment.dateTo.$invalid" class="help-block has-error ng-hide warnig">Please select year</span>
                                            <span class="help-block has-error ng-hide warnig" style="color: red;" ng-show="dateToError">{{dateToError}}</span>
                                        </div>
                                             
                                             <div class="col-md-3 padingr">
                                            <span class="help-block">Note:- To year select first</span>
                                        </div>

                                    </div>
                                    <div class="form-group">
                                        <label class="col-sm-3 control-label">Location, City<span class="req">*</span></label>
                                        <div class="col-sm-6">
                                            <input type="text"  class="form-control" ng-model="employmentDetail.userCity" name="userCity" placeholder="Enter city" require ng-pattern="/.*[a-zA-Z]+.*/" required="">
                                            <span ng-show="submitted && userEmployment.userCity.$invalid" class="help-block has-error ng-hide warnig">Please enter your city</span>
                                            <span class="help-block has-error ng-hide warnig" style="color: red;" ng-show="userCityError">{{userCityError}}</span>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-sm-3 control-label">Location, Country<span class="req">*</span></label>
                                        <div class="col-sm-6">
                                            <input type="text"  class="form-control" ng-model="employmentDetail.userCountry" name="userCountry" placeholder="Enter country" require ng-pattern="/.*[a-zA-Z]+.*/"  required="">

                                            <span ng-show="submitted && userEmployment.userCountry.$invalid" class="help-block has-error ng-hide warnig">Please enter country</span>
                                            <span class="help-block has-error ng-hide warnig" style="color: red;" ng-show="userCountryError">{{userCountryError}}</span>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-sm-3 control-label">Purpose of Role<span class="req">*</span></label>
                                        <div class="col-sm-6">
                                            <input type="text"  class="form-control" ng-model="employmentDetail.role" name="role" placeholder="Purpose of role" require ng-pattern="/.*[a-zA-Z]+.*/"  required="">
                                            <span ng-show="submitted && userEmployment.role.$invalid" class="help-block has-error ng-hide warnig">Please enter purpose of role</span>
                                            <span class="help-block has-error ng-hide warnig" style="color: red;" ng-show="roleError">{{roleError}}</span>

                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-sm-3 control-label">Key Responsibilities / Achievements<span class="req">*</span></label>
                                        <div class="col-sm-6">
                                            <textarea class="form-control" rows="3" ng-model="employmentDetail.respn" name="respn"  require ng-pattern="/.*[a-zA-Z]+.*/" required=""></textarea>

                                             <span ng-show="submitted && userEmployment.respn.$invalid" class="help-block has-error ng-hide warnig">Please enter Key responsibilities / achievements</span>
                                            <span class="help-block has-error ng-hide warnig" style="color: red;" ng-show="respnError">{{respnError}}</span>
                                        </div>
                                        <div class="col-md-3">
                                            <span class="help-block">Display additional instructions here.</span>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-sm-3 control-label">Internal Training<span class="req">*</span></label>
                                        <div class="col-sm-6">
                                            <!-- <input type="text"  class="form-control" placeholder="Internal training done"> -->
                                            <select class="form-control m-bot15" value="" ng-model="employmentDetail.training" name="training" required="">
                                                <option value="" selected="selected">Select training type</option>
                                                <?php foreach ($training as $value) { ?>

                                                     <option value="<?php echo $value->id; ?>"><?php echo $value->trng_name; ?></option>
                                           <?php      } ?>
                                                
                                                
                                                
                                            </select>
                                          <span ng-show="submitted && userEmployment.training.$invalid" class="help-block has-error ng-hide warnig">Please select internal training</span>
                                            <span class="help-block has-error ng-hide warnig" style="color: red;" ng-show="respnError">{{respnError}}</span>

                                        </div>
                                    </div>
                                    
                                            <div class="form-group">

                                                <div class="col-md-3">
                                                    <a href="<?php echo base_url(); ?>user/education" type="button" class="btn btn-danger btn-sm"><strong><i class="fa fa-arrow-left"></i> Back</strong></a>
                                                    <span class="help-block small text-muted">(Education)</span>
                                                </div>
                                                <div class="col-md-9">

                                                    <a href="<?php echo base_url(); ?>user/employment"> <button type="button"  class="btn btn-info pull-right btn-sm"><strong>Save &amp; Continue <i class="fa fa-arrow-right"></i></strong></button></a>

                                                    <span class="clearfix"></span>
                                                    <span class="help-block pull-right small text-muted">(Employment)</span>
                                                </div>
                                            </div>

                                        </form>







                                            </div>

                                            <div class="form-group">

                                                <div class="col-md-3">
                                                    <a href="<?php echo base_url(); ?>user/education" type="button" class="btn btn-danger btn-sm"><strong><i class="fa fa-arrow-left"></i> Back</strong></a>
                                                    <span class="help-block small text-muted">(Education)</span>
                                                </div>
                                                <div class="col-md-9">

                                                    <a href="<?php echo base_url(); ?>user/employment"> <button type="button"  class="btn btn-info pull-right btn-sm"><strong>Save &amp; Continue <i class="fa fa-arrow-right"></i></strong></button></a>

                                                    <span class="clearfix"></span>
                                                    <span class="help-block pull-right small text-muted">(Employment)</span>
                                                </div>
                                            </div>



                                         </div>
                                   </section>
                               </div>
                            </div>