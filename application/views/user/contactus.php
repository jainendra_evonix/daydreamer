
<style type="text/css">
  .btn-submit {
      position: initial !important;
      font-size: 18px !important;
  }
  .btn-submit::before {
    display: none;
  }
</style>


<div class="cd-hero-inner">
  <div class="container">
    <div class="row">
      <div class="col-md-6 col-sm-6">
        <!-- <h1>Contact Us</h1> -->
      </div>
      <div class="col-md-6 col-sm-6">
        <div class="breadcmb"><a href="index.php">Home</a> / <span>Contact Us</span></div>
      </div>
    </div>
  </div>
</div>

<div class="listpgWraper">
  <div class="container">
    
    
    <!-- Search Result and sidebar start -->
    <div class="row">
      <div class="col-md-12 col-sm-12"> 
          <div class="freelancer-wrap row-fluid clearfix">
              <div class="col-md-12">
                  <h2 class="text-info"><strong>Contact Us</strong></h2>
                  <h3 class="lead">We would love to hear from you regarding any queries you may have. We will always endeavour to contact you by either phone or email, as soon as we possibly can.</h3> 
                  <h3 class="lead">Please enter as much detail in the box provided, as this will allow us to give a complete response and prove to be more time efficient for you.”</h3>
                  <div class="row">
                    <div class="col-md-8 col-sm-8">
                      <div class="searchform well">
                        <div class="row">
                          <div class="col-md-6 col-sm-6">
                            <input type="text" class="form-control" placeholder="Full Name">
                          </div>
                          <div class="col-md-6 col-sm-6">
                            <input type="text" class="form-control" placeholder="E-Mail ID">
                          </div>
                          <div class="clearfix"></div>
                          <br>
                          <div class="col-md-6 col-sm-6">
                            <input type="text" class="form-control" placeholder="Phone Number">
                          </div>
                          <div class="col-md-6 col-sm-6">
                            <input type="text" class="form-control" placeholder="Subject">
                          </div>
                          <div class="clearfix"></div>
                          <br>
                          <div class="col-md-12 col-sm-12">
                            <textarea class="form-control" placeholder="Your Message" rows="4"></textarea>
                          </div>
                          <div class="col-md-12 col-sm-12">
                            <br>
                            <button class="btn btn-submit"> Send <i class="fa fa-arrow-right"></i> </button>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                  <br>
                  <h3 class="lead">“We will endeavour to contact you by either phone or email, as soon as we possibly can. If you do not hear from us within 14 days, it does not mean we have forgotten about you, it simply means that we have been inundated with messages &amp; are unable to contact you immediately, however will be in touch.”</h3>
              </div><!-- end col -->
          </div><!-- end freelancer-wrap -->
      </div>
    </div>
  </div>
</div>
