

<style type="text/css">
  .freelancer-wrap.jobdesc .btn {
    position: initial;
  }
  .freelancer-wrap.jobdesc .btn::before {
    border: none;
  }
  .dashblock:hover, .dashblock:visited, .dashblock:active, .dashblock:focus {
    text-decoration: none;
  }
  .freelancer-wrap {
    padding: 20px;
  }
  .dashblock i {
    font-size: 34px;
    line-height: 60px;
    color: #333;
  }
  .dashblock h3 {
    margin-top: 5px;
    color: #333;
    font-weight: 600;
  }
  .divider {
    margin-top: 0;
    margin-bottom: 30px;
  }
  .dashblock {
    margin-bottom: 30px;
    border: 1px solid #eaeaea;
  }
  .jobdate, .jobdesc {
    position: relative;
    padding: 15px 30px;
    background-color: #ffffff;
    /*border: 1px solid #eaeaea;*/
    min-height: 100px;
  }
  .jobdate {
    border-right: 2px solid #464861;
  }
  .jobdate h6 {
    margin: 24px 0;
    font-weight: 700;
    font-size: 18px;
    color: #464861;
    text-align: center;
  }
  .jobaction li a {
    font-weight: 600;
    text-decoration: none;
  }
  .jobaction li a i {
    font-size: 24px;
    color: #999;
  }
  .jobaction li a:hover i {
    color: #555;
  }
</style>

<?php // echo "<pre>"; print_r($myAppliedJobs); exit; ?>


<div class="cd-hero-inner">
  <div class="container">
    <div class="row">
      <div class="col-md-6 col-sm-6">
      </div>
      <div class="col-md-6 col-sm-6">
        <div class="breadcmb"><a href="<?php echo base_url(); ?>user">Home</a> / <span>Saved Jobs</span></div>
      </div>
    </div>
  </div>
</div>

<div class="listpgWraper">
  <div class="container">
    
    <!-- Search Result and sidebar start -->
    <div class="row">
     <div class="col-md-11 col-sm-11">
              <a href="<?php echo base_url(); ?>user/dashboard"><h3><strong></strong>Back to Daydreamer</h3></a>
              <!-- <br> -->
            </div>
    <?php if(!empty($myAppliedJobs)){ ?>
      <div class="col-md-12 col-sm-12">
        <h3><strong>Applied Jobs</strong></h3>
       <!--  <br>
        <h4><strong>Don't forget to apply,</strong> these jobs will expire soon.</h4> -->
        <br>
      </div>
      <?php } ?>
      <div class="col-md-12 col-sm-12">

            <div class="row">


                <?php if(!empty($myAppliedJobs)) {

                  foreach ($myAppliedJobs as $key) {  ?>
                 
               
                <div class="col-md-12 col-sm-12 col-xs-12">
                    <div class="dashblock">
                      <div class="row-fluid clearfix">
                        
                        <div class="col-md-12 col-sm-12 col-xs-12 jobdesc">
                        <table class="table table-bordered">
                          <tbody>
                            <tr class="table_header">
                              <th class="application" width="35%">Application</th>
                              <th class="company" width="24%">Company</th>
                              <th class="date_applied" width="14%">Applied</th>
                              <th class="date_applied" width="08%">Notes</th>
                              <th class="" width="16%">Rate</th>
                            </tr>
                            
                            
                            <tr>
                              <td class="application">
                                <p class="job_title">
                                  <a href="<?php echo base_url().'user/applyJob/'.$key->j_id; ?>"><strong><?php echo  $key->job_title; ?></strong></a>
                                  <!-- <span class="jobref">Job ref: 246314-J39738</span> -->
                                  
                                </p>
                                
                                              
                              </td>
                              <td class="company">
                                <p class="agency_name grey-text"><a href="<?php echo 'http://'.$key->web_address; ?>"><?php echo $key->company_name." ".$key->city; ?></a></p>
                              </td>
                              <td class="date_applied">
                              <?php echo   date('jS M Y', strtotime($key->aplliedDate)); ?>
                              
                                <p class="status_message grey-text small-text">Sending Shortly</p>
                                          
                              </td>

                              <td class="notes">
                                <p class="add_note"><a href="javascript:void();" title="Add note"><?php echo $key->job_desc;  ?></a></p>
                              </td>
                              
                              <td class="">
                                                
                                  <p class="feedback_info grey-text">Not eligible</p>               
                                
                                              
                              </td>
                            </tr>
                            

                          </tbody>
                        </table>
                        </div>
                      </div>
                    </div>
                </div>

</div>

    </div>

                           
 




                <?php  } }else{ ?>
                         
                          
                            
              <!-- <div class="col-md-11 col-sm-11">
                  <a href="<?php echo base_url(); ?>user/dashboard"><h3><strong></strong>Back to Daydreamer</h3></a>
                  <br>
              </div> -->
      


                    <a href="#" class="dashblock">
                      <div class="freelancer-wrap jobdesc row-fluid clearfix">
                        <i class="ion-ios-email-outline"></i>
                        <h2>No applied jobs</h2>
                        <span class="text-info"><strong>You have no one appllied job</strong></span>
                      </div>
                    </a>
               

                <?php  } ?>

                <!-- <div class="col-md-12 hidden-xs"><hr class="divider"></div> -->

                <!-- <div class="col-md-12 col-sm-12 col-xs-12">
                  <h4><strong>Don't forget to apply,</strong> these jobs will expire soon.</h4>
                </div> -->

               <!--  <div class="col-md-12 col-sm-12 col-xs-12">
                    <div class="dashblock">
                      <div class="row-fluid clearfix">
                        <div class="col-md-2 col-sm-2 col-xs-12 jobdate">
                          <h6>5 days</h6>
                        </div>
                        <div class="col-md-10 col-sm-10 col-xs-12 jobdesc">
                        <div class="row">
                            <div class="col-md-1 col-sm-2">
                              <a href="#"><i class="fa fa-star"></i></a>
                            </div>
                            <div class="col-md-9">
                              <h3>Java Developer - SQL Agile Java Spring</h3>
                              <span class="text-muted">£35,000 to £45,000 + bonus ... | West London | Permanent</span>
                            </div>
                            <div class="col-md-2">
                              <ul class="list-inline list-unstyled jobaction">
                                <li><a href="#">View job</a></li>
                                <li><a href="#"><i class="fa fa-trash-o"></i></a></li>
                              </ul>
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                </div> -->

               <!--  <div class="col-md-12 col-sm-12 col-xs-12">
                    <div class="dashblock">
                      <div class="row-fluid clearfix">
                        <div class="col-md-2 col-sm-2 col-xs-12 jobdate">
                          <h6>5 days</h6>
                        </div>
                        <div class="col-md-10 col-sm-10 col-xs-12 jobdesc">
                        <div class="row">
                            <div class="col-md-1 col-sm-2">
                              <a href="#"><i class="fa fa-star"></i></a>
                            </div>
                            <div class="col-md-9">
                              <h3>Java Developer - SQL Agile Java Spring</h3>
                              <span class="text-muted">£35,000 to £45,000 + bonus ... | West London | Permanent</span>
                            </div>
                            <div class="col-md-2">
                              <ul class="list-inline list-unstyled jobaction">
                                <li><a href="#">View job</a></li>
                                <li><a href="#"><i class="fa fa-trash-o"></i></a></li>
                              </ul>
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                </div> -->

            </div>

      </div>
    </div>
  </div>
</div>

<?php ?>