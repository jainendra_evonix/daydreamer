

  <body class="login-body" ng-app="postApp" ng-controller="postController">

    <div class="container">

      <form class="form-signin" name="userLogin" ng-submit="submitForm()" novalidate>
        <h2 class="form-signin-heading"><strong>Sign Up</strong></h2>
        <div class="login-wrap">
            <p>Enter your personal details below</p>
            <input type="text" class="form-control" name="fname" ng-model="user.fname" placeholder="First Name" required autofocus>
               
             <span ng-show="userLogin.fname.$error.required" class="help-block has-error ng-hide">Please Enter First Name</span> 
             <span class="help-block has-error ng-hide" ng-show="fnameError">{{fnameError}}</span> 

            <input type="text" class="form-control" name="lname" ng-model="user.lname"  placeholder="Last Name" required autofocus>

            <span ng-show="userLogin.lname.$error.required" class="help-block has-error ng-hide">Please Enter Last Name</span> 
            <span class="help-block has-error ng-hide" ng-show="lnameError">{{lnameError}}</span> 

            <input type="text" class="form-control" name="city" ng-model="user.city"  placeholder="City/Town" required autofocus>

            <span ng-show="userLogin.city.$error.required" class="help-block has-error ng-hide">Please Enter City Name</span> 
            <span class="help-block has-error ng-hide" ng-show="cityError">{{cityError}}</span> 
            <!-- <div class="radios">
                <label class="label_radio col-lg-6 col-sm-6" for="radio-01">
                    <input name="sample-radio" id="radio-01" value="1" type="radio" checked /> Male
                </label>
                <label class="label_radio col-lg-6 col-sm-6" for="radio-02">
                    <input name="sample-radio" id="radio-02" value="1" type="radio" /> Female
                </label>
            </div> -->

            <p> Enter your account details below</p>
            <input type="email" class="form-control" name="emailid" ng-model="registration.emailid" placeholder="Email" required autofocus>

            <span ng-show="userLogin.emailid.$error.required" class="help-block has-error ng-hide">Please Enter Email Id Name</span>
            <span class="help-block has-error ng-hide" ng-show="cityError">{{emailidError}}</span> 


            <input type="password" class="form-control" name="password" ng-model="registration.password" placeholder="Password" required>
             <span class="help-block has-error ng-hide" ng-show="cityError">{{passwordError}}</span>
            <span ng-show="userLogin.password.$error.required" class="help-block has-error ng-hide">Please Enter Password</span>

            <input type="password" class="form-control" name="cnfrmpassword" ng-model="registration.cnfrmpassword" placeholder="Re-type Password" required>

            <span ng-show="userLogin.cnfrmpassword.$error.required" class="help-block has-error ng-hide">Please Enter Password Again</span>
            <span class="help-block has-error ng-hide" ng-show="cityError">{{cnfrmpasswordError}}</span> 
            <label class="checkbox">
                <input type="checkbox" value="agree this condition" required> I agree to the <a href="#">Terms of Service</a> and <a href="#">Privacy Policy</a>
            </label>
            <button class="btn btn-lg btn-login btn-block" type="submit" ng-click="submitted = true">Submit</button>

            <div class="registration">
                Already Registered.
                <a class="" href="<?php echo base_url(); ?>welcome">
                    Login
                </a>
            </div>

        </div>

      </form>

    </div>


