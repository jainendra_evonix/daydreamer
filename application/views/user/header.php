<!-- SITE HEADER -->

<style type="text/css">
.profilemenu {
width: 265px !important;
margin-left: -172px !important;
}
.profilemenu h4 {
    margin-top: 0;
    margin-bottom: 5px;
}
.profilemenu p {
margin-bottom: 5px;
}
.profilemenu .col-xs-4 {
padding-right: 0px;
}
.profilemenu li a {
	position: relative;
}
.profilemenuicons {
	position: absolute;
	right: 15px;
	top: 28%;
}
</style>

<body id="myPage" ng-app="postApp" ng-controller="postController as ctrl">
<header class="cd-auto-hide-header">

	<!-- Navbar -->
     <div class="main-nav-container">
    
	    <div class="top-sub-nav">
	    <?php if($this->session->userdata('user_id')){ ?>
	    	<!-- <a href="<?php echo base_url();?>client">Are you an employer?</a> -->
	    <!-- <a href="<?php echo base_url(); ?>user/logout">	<span class="pull-right"> <i class="fa fa-power-off" aria-hidden="true"></i>&nbsp; Log Out </span></a> -->
            <span class="pull-right">Recruiting? Call us on <strong>01772 639042</strong></span>
	    	<?php } ?>
	    </div>
	    
	    <div class="container-fluid navbar navbar-default main-nav" role="navigation">
	      <div class="navbar-header">
	        <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
	          <span class="sr-only">Toggle navigation</span>
	          <span class="icon-bar"></span>
	          <span class="icon-bar"></span>
	          <span class="icon-bar"></span>
	        </button>
	        <a class="navbar-brand" href="<?php echo base_url(); ?>user/index">
	          <!-- <img src="images/logo.png" class="logotop"> -->
	          <p class="logo-text">DAYDREAMER</span></p>
	        </a>
	      </div>

	      <div class="navbar-collapse collapse">
	       

	        <!-- Right nav -->
	        <ul class="nav navbar-nav navbar-right">
	          <li class="active"><a href="<?php echo base_url(); ?>user/searchedJob">View All Jobs</a></li>
	          <li><a href="<?php echo base_url() ?>user/blog">Blog</a></li>
	          <li><a href="#">How to apply</a></li>
	          <li><a href="<?php if(@($this->session->userdata('user_id'))){ echo base_url().'user/resume'; } else{ echo base_url().'user/registration'; } ?>">Post CV</a></li>
	          <li><a href="#">Company Profiles <span class="caret"></span></a>
	            <ul class="dropdown-menu">
	              <li><a href="#">Action</a></li>
	              <li><a href="#">Another action</a></li>
	              <li><a href="#">Something else here</a></li>
	              <li class="divider"></li>
	              <li class="dropdown-header">Nav header</li>
	              <li><a href="#">A sub menu <span class="caret"></span></a>
	                <ul class="dropdown-menu">
	                  <li><a href="#">Action</a></li>
	                  <li><a href="#">Another action</a></li>
	                  <li><a href="#">Something else here</a></li>
	                  <li class="disabled"><a class="disabled" href="#">Disabled item</a></li>
	                  <li><a href="#">One more link</a></li>
	                </ul>
	              </li>
	              <li><a href="#">A separated link</a></li>
	            </ul>
	          </li>
	          
	          <?php if(!$this->session->userdata('user_id')){ ?>
	          <li><a href="<?php echo base_url(); ?>user/login"><i class="fa fa-sign-in"></i> Login</a></li>
	          <li><a href="<?php echo base_url(); ?>user/registration"><i class="fa fa-user-plus"></i> Register</a></li>
           <?php }?>

           <?php if($this->session->userdata('user_id')){ ?>

                <li><a href="#">Account<span class="caret"></span></a>

                <ul class="dropdown-menu profilemenu">
              <li>
              <div class="clearfix">
              <div class="col-xs-4">
              <img src="<?php error_reporting(0); echo base_url(); ?>uploads/profilePic/<?php if($userProfileData->user_profilePic){echo $userProfileData->user_profilePic;}else{ echo 'default.png'; }?>" class="img-responsive">
              </div>
              <div class="col-xs-8">
              <h4><strong><?php echo $this->session->userdata('fname')." ".$this->session->userdata('lname'); ?></strong></h4>
              <p><?php  echo $this->session->userdata('user_email'); ?></p>
              <a href="<?php echo base_url(); ?>user/dashboard" class="btn btn-primary btn-block btn-xs"><strong>Dashboard</strong></a>
              </div>
              </div>
              </li>
              <li class="divider"></li>
              <li><a href="#">Account Settings <span class="glyphicon profilemenuicons glyphicon-cog pull-right"></span></a></li>
              <li class="divider"></li>
              <li><a href="#">User Stats <span class="glyphicon profilemenuicons glyphicon-stats pull-right"></span></a></li>
              <li class="divider"></li>
              <li><a href="<?php echo base_url(); ?>user/resume">Resume <span class="glyphicon profilemenuicons glyphicon-stats pull-right"></span></a></li>
              <li class="divider"></li>
              <li><a href="#">Job Alert <span class="glyphicon profilemenuicons glyphicon-stats pull-right"></span></a></li>
              <li class="divider"></li>
              <!-- <li><a href="#">Messages <span class="badge profilemenuicons pull-right"> 42 </span></a></li> -->
              <li class="divider"></li>
              <li><a href="#">Favourites Snippets <span class="glyphicon profilemenuicons glyphicon-heart pull-right"></span></a></li>
              <li class="divider"></li>
              <li><a href="<?php echo base_url(); ?>user/logout">Sign Out <span class="glyphicon profilemenuicons glyphicon-log-out pull-right"></span></a></li>
            </ul>






              	</li>

          <!--  <li><a href="<?php echo base_url(); ?>user/dashboard">Dashboard</a></li>
	          <li><a href="<?php echo base_url(); ?>user/logout"><i class="fa fa-power-off" aria-hidden="true"></i>&nbsp;
 Log Out</a></li> -->
	          <?php } ?>
	        </ul>

	      </div><!--/.nav-collapse -->
	    </div>
    </div>

	<!-- <div class="logo"><a href="#0"><img src="img/cd-logo.svg" alt="Logo"></a></div>

	<nav class="cd-primary-nav">
		<a href="#cd-navigation" class="nav-trigger">
			<span>
				<em aria-hidden="true"></em>
				Menu
			</span>
		</a>

		<ul id="cd-navigation">
			<li><a href="#0">The team</a></li>
			<li><a href="#0">Our Products</a></li>
			<li><a href="#0">Our Services</a></li>
			<li><a href="#0">Shopping tools</a></li>
			<li><a href="#0">Contact Us</a></li>
		</ul>
	</nav> -->
</header> <!-- .cd-auto-hide-header -->