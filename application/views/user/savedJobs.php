

<style type="text/css">
  .freelancer-wrap.jobdesc .btn {
    position: initial;
  }
  .freelancer-wrap.jobdesc .btn::before {
    border: none;
  }
  .dashblock:hover, .dashblock:visited, .dashblock:active, .dashblock:focus {
    text-decoration: none;
  }
  .freelancer-wrap {
    padding: 20px;
  }
  .dashblock i {
    font-size: 34px;
    line-height: 60px;
    color: #333;
  }
  .dashblock h3 {
    margin-top: 5px;
    color: #333;
    font-weight: 600;
  }
  .divider {
    margin-top: 0;
    margin-bottom: 30px;
  }
  .dashblock {
    margin-bottom: 30px;
    border: 1px solid #eaeaea;
  }
  .jobdate, .jobdesc {
    position: relative;
    padding: 15px 30px;
    background-color: #ffffff;
    /*border: 1px solid #eaeaea;*/
    min-height: 100px;
  }
  .jobdate {
    border-right: 2px solid #464861;
  }
  .jobdate h6 {
    margin: 24px 0;
    font-weight: 700;
    font-size: 18px;
    color: #464861;
    text-align: center;
  }
  .jobaction li a {
    font-weight: 600;
    text-decoration: none;
  }
  .jobaction li a i {
    font-size: 24px;
    color: #999;
  }
  .jobaction li a:hover i {
    color: #555;
  }
</style>

<?php // echo "<pre>"; print_r($mySavedJobs); exit; ?>


<div class="cd-hero-inner">
  <div class="container">
    <div class="row">
      <div class="col-md-6 col-sm-6">
      </div>
      <div class="col-md-6 col-sm-6">
        <div class="breadcmb"><a href="<?php echo base_url(); ?>user">Home</a> / <span>Saved Jobs</span></div>
      </div>
    </div>
  </div>
</div>

<div class="listpgWraper">
  <div class="container">
    
    <!-- Search Result and sidebar start -->
    <div class="row">
      <div class="col-md-11 col-sm-11">
          <a href="<?php echo base_url(); ?>user/dashboard"><h3><strong></strong>Back to Daydreamer</h3></a>
          <!-- <br> -->
        </div>
    <?php if(!empty($mySavedJobs)){ ?>
      <div class="col-md-12 col-sm-12">
        <h3><strong>Saved Jobs</strong></h3>
        <br>
        <h4><strong>Don't forget to apply,</strong> these jobs will expire soon.</h4>
      </div>
      <?php } ?>
      <div class="col-md-12 col-sm-12">

            <div class="row">
                <?php if(!empty($mySavedJobs)) {

                  foreach ($mySavedJobs as $key) {  ?>
                 
               
                <div class="col-md-12 col-sm-12 col-xs-12">
                    <div class="dashblock">
                      <div class="row-fluid clearfix">
                        <div class="col-md-2 col-sm-2 col-xs-12 jobdate">
                          <h6>Today</h6>
                        </div>
                        <div class="col-md-10 col-sm-10 col-xs-12 jobdesc">
                        <div class="row">
                            <div class="col-md-1 col-sm-2">
                              <a href="#"><i class="fa fa-star"></i></a>
                            </div>
                            <div class="col-md-9">
                              <h3><?php echo $key->job_title; ?></h3>
                              <span class="text-muted">£<?php echo $key->salary; ?> to £45,000 + bonus ... | <?php echo $key->city; ?> | <?php echo $key->job_type; ?></span>
                            </div>
                            <div class="col-md-2">
                              <ul class="list-inline list-unstyled jobaction">
                                <li><a href="<?php echo base_url().'user/applyJob/'.$key->j_id; ?>">View job</a></li>
                                <li><a href="javascript:void(0);" onclick="delSaveUserJob(<?php echo $key->j_id; ?>);"><i class="fa fa-trash-o"></i></a></li>
                              </ul>
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                </div>

                <?php  } }else{ ?>
                         
                          
                            
              <div class="col-md-11 col-sm-11">
        <a href="<?php echo base_url(); ?>user/dashboard"><h3><strong></strong>Back to Daydreamer</h3></a>
        <br>
      </div>
      <div class="col-md-1 col-sm-1">
      <h4><a href="#" class="pull-right" title="Settings"><i class="fa fa-cog"></i></a><br></h4>
      </div>


                    <a href="#" class="dashblock">
                      <div class="freelancer-wrap jobdesc row-fluid clearfix">
                        <i class="ion-ios-email-outline"></i>
                        <h2>No saved jobs</h2>
                        <span class="text-info"><strong>You have no one saved job</strong></span>
                      </div>
                    </a>
               

                <?php  } ?>

                <!-- <div class="col-md-12 hidden-xs"><hr class="divider"></div> -->

                <!-- <div class="col-md-12 col-sm-12 col-xs-12">
                  <h4><strong>Don't forget to apply,</strong> these jobs will expire soon.</h4>
                </div> -->

               <!--  <div class="col-md-12 col-sm-12 col-xs-12">
                    <div class="dashblock">
                      <div class="row-fluid clearfix">
                        <div class="col-md-2 col-sm-2 col-xs-12 jobdate">
                          <h6>5 days</h6>
                        </div>
                        <div class="col-md-10 col-sm-10 col-xs-12 jobdesc">
                        <div class="row">
                            <div class="col-md-1 col-sm-2">
                              <a href="#"><i class="fa fa-star"></i></a>
                            </div>
                            <div class="col-md-9">
                              <h3>Java Developer - SQL Agile Java Spring</h3>
                              <span class="text-muted">£35,000 to £45,000 + bonus ... | West London | Permanent</span>
                            </div>
                            <div class="col-md-2">
                              <ul class="list-inline list-unstyled jobaction">
                                <li><a href="#">View job</a></li>
                                <li><a href="#"><i class="fa fa-trash-o"></i></a></li>
                              </ul>
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                </div> -->

               <!--  <div class="col-md-12 col-sm-12 col-xs-12">
                    <div class="dashblock">
                      <div class="row-fluid clearfix">
                        <div class="col-md-2 col-sm-2 col-xs-12 jobdate">
                          <h6>5 days</h6>
                        </div>
                        <div class="col-md-10 col-sm-10 col-xs-12 jobdesc">
                        <div class="row">
                            <div class="col-md-1 col-sm-2">
                              <a href="#"><i class="fa fa-star"></i></a>
                            </div>
                            <div class="col-md-9">
                              <h3>Java Developer - SQL Agile Java Spring</h3>
                              <span class="text-muted">£35,000 to £45,000 + bonus ... | West London | Permanent</span>
                            </div>
                            <div class="col-md-2">
                              <ul class="list-inline list-unstyled jobaction">
                                <li><a href="#">View job</a></li>
                                <li><a href="#"><i class="fa fa-trash-o"></i></a></li>
                              </ul>
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                </div> -->

            </div>

      </div>
    </div>
  </div>
</div>

<?php ?>