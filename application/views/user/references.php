<?php // echo "<pre>"; print_r($savedUserAdditionalInfo); exit; ?>
        <!-- <section class="container-fluid">
            <div class="row" id="topbanner">
                <div class="col-md-6 col-md-offset-3">
                    <h2 class="text-center">Customer Online Form</h2>
                    <p class="text-center">Lorem ipsum dolor sit amet, consectetur adipisicing elit.</p>
                </div>
            </div>

            <div class="row" id="formsteps">
                <div class="col-md-11 col-md-offset-1">
                    <ul class="breadcrumbs-alt">
                        <li>
                            <a href="<?php echo base_url();?>user/profile">Personal Information</a>
                        </li>
                        <li>
                            <a href="<?php echo base_url();?>user/education">Education</a>
                        </li>
                        <li>
                            <a href="<?php echo base_url();?>user/preEducation">Pre-Employment</a>
                        </li>
                        <li>
                            <a href="<?php echo base_url();?>user/employment">Employment</a>
                        </li>
                        <li>
                            <a class="current" href="<?php echo base_url();?>user/reference">References</a>
                        </li>
                        <li>
                            <a href="<?php echo base_url(); ?>user/additionalInfos">Additional Information</a>
                        </li>
                        <li>
                            <a href="miscellaneous.php">Miscellaneous</a>
                        </li>
                        <li>
                            <a href="social-media.php">Social Media</a>
                        </li>
                    </ul>
                </div>
            </div>
        </section> -->
                                
                <div class="row">
        <div class="col-md-3">
        </div>
        <div class="col-md-6">
<?php if($this->session->flashdata('succmsg')){  ?>
        
                                <div class="alert alert-success">
                                 <strong>Success!</strong> <?php echo $this->session->flashdata('succmsg'); ?>
                                 <!-- <button type="button" class="close" data-dismiss="modal">&times;</button> -->
                                 <a type="button" class="close" data-dismiss="alert" aria-hidden="true">×</a>
                                </div>

                                <?php    } ?>
                                </div>
                                <div class="col-md-3">
                                    
                                </div>
                                </div>

        <!--main content start-->
        <section id="" class="container">
            <section class="wrapper">
                <!-- page start-->
                <div class="row">
                    <div class="col-md-8 col-md-offset-2">
                        <h3 class="form-heading">References</h3>
                      

                        <section class="panel">
                            <div class="panel-body">
                        <form class="form-horizontal bucket-form" name="userReference" ng-submit="submitForm()" novalidate  >
                                    <!-- <h4>For reference purposes</h4> -->
                                    <div class="form-group">
                                        <label class="col-sm-3 control-label">Type Of Reference<span class="req">*</span></label>
                                        <div class="col-sm-6">
                                            <select class="form-control m-bot15" ng-model="userReferenceDetail.userRefence" name="userRefence" required>
                                        <option value="" selected="selected">Select reference type</option>
                                               <?php foreach ($userRefences as $key) {   ?>
                                                   
                                             
                                                <option value="<?php echo $key->id; ?>"><?php echo $key->reference; ?></option>

                                                <?php } ?>
                                            </select>

             <span ng-show="submittedd && userReference.userRefence.$invalid" class="help-block has-error ng-hide warnig">Please select your refrence type</span>
             <span class="help-block has-error ng-hide warnig" style="color: red;" ng-show="userRefenceError">{{userRefenceError}}</span>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-sm-3 control-label">Business Details / Main Contact<span class="req">*</span></label>
                                        <div class="col-sm-6">
                                            <input type="text"  class="form-control" ng-model="userReferenceDetail.bDetail" name="bDetail"  require ng-pattern="/.*[a-zA-Z]+.*/" placeholder="Contact person" required>
                                <span ng-show="submittedd && userReference.bDetail.$invalid" class="help-block has-error ng-hide warnig">Please enter your business detail</span>
             <span class="help-block has-error ng-hide warnig" style="color: red;" ng-show="bDetailError">{{bDetailError}}</span>

                                        </div>
                                        <div class="col-md-3">
                                            <span class="help-block">Display additional instructions here.</span>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-sm-3 control-label">Address Line 1<span class="req">*</span></label>
                                        <div class="col-sm-6">
                                            <input type="text"  class="form-control" ng-model="userReferenceDetail.addLine1" name="addLine1" require ng-pattern="/.*[a-zA-Z]+.*/" placeholder="Address line 1" required >

                                       <span ng-show="submittedd && userReference.addLine1.$invalid" class="help-block has-error ng-hide warnig">Please enter your business detail</span>
             <span class="help-block has-error ng-hide warnig" style="color: red;" ng-show="addLine1Error">
                       {{addLine1Error}}</span>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-sm-3 control-label">Address Line 2<span class="req">*</span></label>
                                        <div class="col-sm-6">
                                         <input type="text"  class="form-control" ng-model="userReferenceDetail.addLine2" name="addLine2"  require ng-pattern="/.*[a-zA-Z]+.*/" placeholder=" Address line 2" required>
                                       <span ng-show="submittedd && userReference.addLine2.$invalid" class="help-block has-error ng-hide warnig">Please enter your address</span>
             <span class="help-block has-error ng-hide warnig" style="color: red;" ng-show="addLine2Error">
                       {{addLine2Error}}</span>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-sm-3 control-label">Address Line 3<span class="req">*</span></label>
                                        <div class="col-sm-6">
                                            <input type="text"  class="form-control" ng-model="userReferenceDetail.addLine3" name="addLine3"  require ng-pattern="/.*[a-zA-Z]+.*/" placeholder="Address line 3" required>
                                            <span ng-show="submittedd && userReference.addLine3.$invalid" class="help-block has-error ng-hide warnig">Please enter your address</span>
             <span class="help-block has-error ng-hide warnig" style="color: red;" ng-show="addLine3Error">
                       {{addLine3Error}}</span>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-sm-3 control-label">City / County<span class="req">*</span></label>
                                        <div class="col-sm-6">
                                         <!-- <select class="form-control m-bot15" ng-model="userReferenceDetail.city" name="city" required> -->
                                            <input type="text"  class="form-control" ng-model="userReferenceDetail.city" name="city"   require ng-pattern="/.*[a-zA-Z]+.*/" placeholder="City" required>
                         <span ng-show="submittedd && userReference.city.$invalid" class="help-block has-error ng-hide warnig">Please enter your address</span>
             <span class="help-block has-error ng-hide warnig" style="color: red;" ng-show="cityError">
                       {{cityError}}</span>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-sm-3 control-label">Country<span class="req">*</span></label>
                                        <div class="col-sm-6">
                                            <!-- <input type="text"  class="form-control" placeholder="Country"> -->
                                             <select class="form-control m-bot15" ng-model="userReferenceDetail.country" name="country" required>
                                            <!-- <input type="text"  class="form-control" ng-model="userReferenceDetail.city" name="city"  placeholder="City"> -->
                                    <option value="" selected="selected">Select country</option>
                                               <?php foreach ($countries as $key) {   ?>
                                                   
                                             
                                                <option value="<?php echo $key->id; ?>"><?php echo $key->country_name; ?></option>

                                                <?php } ?>

                                            </select>
                                            <span ng-show="submittedd && userReference.country.$invalid" class="help-block has-error ng-hide warnig">Please select your country name</span>
             <span class="help-block has-error ng-hide warnig" style="color: red;" ng-show="countryError">
                       {{countryError}}</span>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-sm-3 control-label">Postcode<span class="req">*</span></label>
                                        <div class="col-sm-6">
                                            <input type="text"  class="form-control" ng-model="userReferenceDetail.postcode" name="postcode" placeholder="Postcode" require ng-pattern="/^[0-9A-Za-z ]{4,10}$/" required>
                                            <span ng-show="submittedd && userReference.postcode.$invalid" class="help-block has-error ng-hide warnig">Please enter your postcode</span>
             <span class="help-block has-error ng-hide warnig" style="color: red;" ng-show="postcodeError">
                       {{postcodeError}}</span>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-sm-3 control-label">Email<span class="req">*</span></label>
                                        <div class="col-sm-6">
                                            <input type="email"  class="form-control" ng-model="userReferenceDetail.email" name="email" placeholder="Email" required>
                                             <span ng-show="submittedd && userReference.email.$invalid" class="help-block has-error ng-hide warnig">Please enter reference email</span>
             <span class="help-block has-error ng-hide warnig" style="color: red;" ng-show="emailError">
                       {{emailError}}</span>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-sm-3 control-label">Telephone<span class="req">*</span></label>
                                        <div class="col-sm-6">
                                            <input type="text"  class="form-control" ng-model="userReferenceDetail.telephone" name="telephone"  require ng-pattern="/^(0|[1-9][0-9]*)$/" placeholder="Telephone" required>
                  <span ng-show="submittedd && userReference.telephone.$invalid" class="help-block has-error ng-hide warnig">Please enter your telephone number</span>
             <span class="help-block has-error ng-hide warnig" style="color: red;" ng-show="telephoneError">
                       {{telephoneError}}</span>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-sm-3 control-label">FAX</label>
                                        <div class="col-sm-6">
                                            <input type="text" ng-model="userReferenceDetail.fax" name="fax"  class="form-control" require ng-pattern="/^(0|[1-9][0-9]*)$/" placeholder="Fax" >
                                            <span ng-show="submittedd && userReference.fax.$invalid" class="help-block has-error ng-hide warnig">Please enter your fax number</span>
             <span class="help-block has-error ng-hide warnig" style="color: red;" ng-show="faxError">
                       {{faxError}}</span>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <div class="col-md-9">
                                            <!-- <button type="submit" ng-click="submittedd=true" class="btn btn-success btn-sm pull-right"><strong>Add More <i class="fa fa-plus"></i></strong></button> -->
                                            <button class="btn btn-success btn-sm pull-right" ng-click="submittedd = true" ><strong>Add <i class="fa fa-plus"></i></strong></button>
                                        </div>
                                    </div>
                                    
                                    <div class="form-group">
                                        <div class="col-md-3">
                                            <a href="<?php echo base_url(); ?>user/employment" type="button" class="btn btn-danger btn-sm"><strong><i class="fa fa-arrow-left"></i> Back</strong></a>
                                            <span class="help-block small text-muted">(Employment)</span>
                                        </div>
                                        <div class="col-md-9">
                                           <a href="<?php echo base_url(); ?>user/additionalInfo"> <button type="submit" class="btn btn-info pull-right btn-sm"><strong>Save &amp; Continue <i class="fa fa-arrow-right"></i></strong></button></a>
                                            <span class="clearfix"></span>
                                            <span class="help-block pull-right small text-muted">(Additional Information)</span>
                                        </div>
                                    </div>
                                    
                                </form>
                            </div>
                        </section>
                    </div>
                </div>
                <!-- page end-->

            </section>
        </section>
        <!--main content end-->





    <!--right sidebar start-->
    <div class="right-sidebar">
        <div class="search-row">
            <input type="text" placeholder="Search" class="form-control">
        </div>
        <div class="right-stat-bar">
            <ul class="right-side-accordion">
                <li class="widget-collapsible">
                    <a href="#" class="head widget-head red-bg active clearfix">
                        <span class="pull-left">work progress (5)</span>
                        <span class="pull-right widget-collapse"><i class="ico-minus"></i></span>
                    </a>
                    <ul class="widget-container">
                        <li>
                            <div class="prog-row side-mini-stat clearfix">
                                <div class="side-graph-info">
                                    <h4>Target sell</h4>
                                    <p>
                                        25%, Deadline 12 june 13
                                    </p>
                                </div>
                                <div class="side-mini-graph">
                                    <div class="target-sell">
                                    </div>
                                </div>
                            </div>
                            <div class="prog-row side-mini-stat">
                                <div class="side-graph-info">
                                    <h4>product delivery</h4>
                                    <p>
                                        55%, Deadline 12 june 13
                                    </p>
                                </div>
                                <div class="side-mini-graph">
                                    <div class="p-delivery">
                                        <div class="sparkline" data-type="bar" data-resize="true" data-height="30" data-width="90%" data-bar-color="#39b7ab" data-bar-width="5" data-data="[200,135,667,333,526,996,564,123,890,564,455]">
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="prog-row side-mini-stat">
                                <div class="side-graph-info payment-info">
                                    <h4>payment collection</h4>
                                    <p>
                                        25%, Deadline 12 june 13
                                    </p>
                                </div>
                                <div class="side-mini-graph">
                                    <div class="p-collection">
                                      <span class="pc-epie-chart" data-percent="45">
                                          <span class="percent"></span>
                                      </span>
                                  </div>
                              </div>
                          </div>
                          <div class="prog-row side-mini-stat">
                            <div class="side-graph-info">
                                <h4>delivery pending</h4>
                                <p>
                                    44%, Deadline 12 june 13
                                </p>
                            </div>
                            <div class="side-mini-graph">
                                <div class="d-pending">
                                </div>
                            </div>
                        </div>
                        <div class="prog-row side-mini-stat">
                            <div class="col-md-12">
                                <h4>total progress</h4>
                                <p>
                                    50%, Deadline 12 june 13
                                </p>
                                <div class="progress progress-xs mtop10">
                                    <div style="width: 50%" aria-valuemax="100" aria-valuemin="0" aria-valuenow="20" role="progressbar" class="progress-bar progress-bar-info">
                                        <span class="sr-only">50% Complete</span>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </li>
                </ul>
            </li>
            <li class="widget-collapsible">
                <a href="#" class="head widget-head terques-bg active clearfix">
                    <span class="pull-left">contact online (5)</span>
                    <span class="pull-right widget-collapse"><i class="ico-minus"></i></span>
                </a>
                <ul class="widget-container">
                    <li>
                        <div class="prog-row">
                            <div class="user-thumb">
                                <a href="#"><img src="<?php echo base_url();?>user_assets/images/avatar1_small.jpg" alt=""></a>
                            </div>
                            <div class="user-details">
                                <h4><a href="#">Jonathan Smith</a></h4>
                                <p>
                                    Work for fun
                                </p>
                            </div>
                            <div class="user-status text-danger">
                                <i class="fa fa-comments-o"></i>
                            </div>
                        </div>
                        <div class="prog-row">
                            <div class="user-thumb">
                                <a href="#"><img src="<?php echo base_url();?>user_assets/images/avatar1.jpg" alt=""></a>
                            </div>
                            <div class="user-details">
                                <h4><a href="#">Anjelina Joe</a></h4>
                                <p>
                                    Available
                                </p>
                            </div>
                            <div class="user-status text-success">
                                <i class="fa fa-comments-o"></i>
                            </div>
                        </div>
                        <div class="prog-row">
                            <div class="user-thumb">
                                <a href="#"><img src="<?php echo base_url();?>user_assets/images/chat-avatar2.jpg" alt=""></a>
                            </div>
                            <div class="user-details">
                                <h4><a href="#">John Doe</a></h4>
                                <p>
                                    Away from Desk
                                </p>
                            </div>
                            <div class="user-status text-warning">
                                <i class="fa fa-comments-o"></i>
                            </div>
                        </div>
                        <div class="prog-row">
                            <div class="user-thumb">
                                <a href="#"><img src="<?php echo base_url();?>user_assets/images/avatar1_small.jpg" alt=""></a>
                            </div>
                            <div class="user-details">
                                <h4><a href="#">Mark Henry</a></h4>
                                <p>
                                    working
                                </p>
                            </div>
                            <div class="user-status text-info">
                                <i class="fa fa-comments-o"></i>
                            </div>
                        </div>
                        <div class="prog-row">
                            <div class="user-thumb">
                                <a href="#"><img src="<?php echo base_url();?>user_assets/images/avatar1.jpg" alt=""></a>
                            </div>
                            <div class="user-details">
                                <h4><a href="#">Shila Jones</a></h4>
                                <p>
                                    Work for fun
                                </p>
                            </div>
                            <div class="user-status text-danger">
                                <i class="fa fa-comments-o"></i>
                            </div>
                        </div>
                        <p class="text-center">
                            <a href="#" class="view-btn">View all Contacts</a>
                        </p>
                    </li>
                </ul>
            </li>
            <li class="widget-collapsible">
                <a href="#" class="head widget-head purple-bg active">
                    <span class="pull-left"> recent activity (3)</span>
                    <span class="pull-right widget-collapse"><i class="ico-minus"></i></span>
                </a>
                <ul class="widget-container">
                    <li>
                        <div class="prog-row">
                            <div class="user-thumb rsn-activity">
                                <i class="fa fa-clock-o"></i>
                            </div>
                            <div class="rsn-details ">
                                <p class="text-muted">
                                    just now
                                </p>
                                <p>
                                    <a href="#">Jim Doe </a>Purchased new equipments for zonal office setup
                                </p>
                            </div>
                        </div>
                        <div class="prog-row">
                            <div class="user-thumb rsn-activity">
                                <i class="fa fa-clock-o"></i>
                            </div>
                            <div class="rsn-details ">
                                <p class="text-muted">
                                    2 min ago
                                </p>
                                <p>
                                    <a href="#">Jane Doe </a>Purchased new equipments for zonal office setup
                                </p>
                            </div>
                        </div>
                        <div class="prog-row">
                            <div class="user-thumb rsn-activity">
                                <i class="fa fa-clock-o"></i>
                            </div>
                            <div class="rsn-details ">
                                <p class="text-muted">
                                    1 day ago
                                </p>
                                <p>
                                    <a href="#">Jim Doe </a>Purchased new equipments for zonal office setup
                                </p>
                            </div>
                        </div>
                    </li>
                </ul>
            </li>
            <li class="widget-collapsible">
                <a href="#" class="head widget-head yellow-bg active">
                    <span class="pull-left"> shipment status</span>
                    <span class="pull-right widget-collapse"><i class="ico-minus"></i></span>
                </a>
                <ul class="widget-container">
                    <li>
                        <div class="col-md-12">
                            <div class="prog-row">
                                <p>
                                    Full sleeve baby wear (SL: 17665)
                                </p>
                                <div class="progress progress-xs mtop10">
                                    <div class="progress-bar progress-bar-success" role="progressbar" aria-valuenow="20" aria-valuemin="0" aria-valuemax="100" style="width: 40%">
                                        <span class="sr-only">40% Complete</span>
                                    </div>
                                </div>
                            </div>
                            <div class="prog-row">
                                <p>
                                    Full sleeve baby wear (SL: 17665)
                                </p>
                                <div class="progress progress-xs mtop10">
                                    <div class="progress-bar progress-bar-info" role="progressbar" aria-valuenow="20" aria-valuemin="0" aria-valuemax="100" style="width: 70%">
                                        <span class="sr-only">70% Completed</span>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </li>
                </ul>
            </li>
        </ul>
    </div>
</div>

