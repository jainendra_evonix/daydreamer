

<div class="cd-hero-inner">
  <div class="container">
    <div class="row">
      <div class="col-md-6 col-sm-6">
        <!-- <h1>About Us</h1> -->
      </div>
      <div class="col-md-6 col-sm-6">
        <div class="breadcmb"><a href="<?php echo base_url(); ?>user">Home</a> / <span>About Us</span></div>
      </div>
    </div>
  </div>
</div>

<div class="listpgWraper">
  <div class="container"> 
    
    
    <!-- Search Result and sidebar start -->
    <div class="row">
      <div class="col-md-12 col-sm-12"> 
          <div class="freelancer-wrap row-fluid clearfix">
              <div class="col-md-12">
                  <h2 class="text-info"><strong>About Us</strong></h2>
                  <h3 class="lead">Not A Recruitment Company!</h3>
                  <h3 class="lead">What was first naively envisaged as an enterprise set out to assist the homeless getting back into work, we found that there were numerous barriers to entry for those we wished to help. We had little success in our efforts and we were small in what we could achieve, but we had a dream.</h3>
                  <h3 class="lead"><strong>We had an idea based on an equation that grew like wildfire:</strong></h3>
                  <h2 class="lead"><strong>Step one:</strong> You take a homeless person
                  <br><strong>Step two:</strong> You get said homeless person a job
                  <br><strong>Step three:</strong> Homeless person has money for rent
                  <br><strong>Step four:</strong> Not homeless anymore</h2>

                  <img class="img-responsive" src="img/home_corporation_sectionbg4.jpg" alt="">
                  <br>
                  <h3 class="lead">We slightly changed our way of thinking, taking from the analogue age and going digital in our fight to end homelessness as it is not so simple as we found. There are numerous hurdles to cross, we had limited resources, at times we didn’t know what we were doing and at times we still don’t, but we will try. </h3>
                  <h3 class="lead">We are here for those who are in need of work; or have a dream job they aspire to achieve. We will work with our partner organisations to see what we can do and try and help along the way. If you still have a dream, we still have a dream.</h3>
                  <br>
                 <!--  <blockquote>
                    <h3 class="lead"><em>Homelessness, seeing those in need and overall poverty ****ed me off a little, so I wanted to do a little something to help. One thing led to another and the Daydreamer Project was born. Our core values are still the same, but in order to be sustainable, we’ve had to change the business model slightly</em></h3>
                    <footer>
                      <span class="lead"><strong>Richard Narayan</strong></span>
                    </footer>
                  </blockquote> -->
              </div><!-- end col -->
          </div><!-- end freelancer-wrap -->
      </div>
    </div>
  </div>
</div>

