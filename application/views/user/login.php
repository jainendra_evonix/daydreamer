<!-- <body ng-app="postApp" ng-controller="postController"> -->
<style type="text/css">
  
.modal-backdrop.in {
    opacity: 0.0;
    display: none;
}
.modal.in .modal-dialog {
  margin-top: 98px;
}
</style>
      <div class="container">
          
          <!--for login-->
          
           <div class="row">
        <div class="col-md-3">
        </div>
        <div class="col-md-6">
<?php if($this->session->flashdata('successmsg')){  ?>
        
                                <div class="alert alert-success">
                                 <strong>Success!</strong> <?php echo $this->session->flashdata('succmsg'); ?>
                                 <!-- <button type="button" class="close" data-dismiss="modal">&times;</button> -->
                                 <a type="button" class="close" data-dismiss="alert" aria-hidden="true">×</a>
                                </div>

                                <?php    } ?>
                                </div>
                                <div class="col-md-3">
                                    
                                </div>
                                </div>

          <form class="form-signin" name="userLogin" ng-submit="submitForm()" novalidate>  
          <?php echo validation_errors(); ?>  
         <?php echo form_open('form'); ?> 
            <h2 class="form-signin-heading">sign in now</h2>
            <div id="message"></div>
            <div class="login-wrap">
           
          <input type="email" class="form-control" placeholder="Email ID" id="emailaddress" ng-model="user.emailaddress" name="emailaddress" required>                
               
          <span ng-show="submitted && userLogin.emailaddress.$invalid" class="help-block has-error warnig ng-hide">Valid Email ID is required.</span>
          <span class="help-block has-error ng-hide warnig" ng-show="emailaddressError">{{emailaddressError}}</span>
                <br>
                <input type="password" class="form-control" placeholder="Password" id="password" ng-model="user.password" name="password" required>
                <span ng-show="submitted && userLogin.password.$error.required"  class="help-block has-error warnig ng-hide">Password is required.</span>
               <span class="help-block has-error ng-hide warnig" ng-show="passwordError">{{passwordError}}</span>
        
                <label class="checkbox">
                <input type="checkbox"  ng-model="user.remember" name="remember" value="yes"> Remember me
                 <!-- <input type="checkbox" value="rememberme" name="user.rememberMe" name="rememberMe"> -->
                    <span class="pull-right">
                  <a data-toggle="modal" href="login.html#myModal"> Forgot Password?</a>
    
                    </span>
                </label>
                <button class="btn btn-lg btn-login btn-block" type="submit" ng-click="submitted = true"><i class="fa fa-lock"></i> Sign in</button>
           <div class="registration">
                Don't have an account yet?
                <a class="" href="<?php echo base_url(); ?>user/registration">
                    Create an account
                </a>
            </div>
            </div>
            
        </form>
            
            <!--for forgot password-->
              <!-- Modal -->
          <form class="form-signin" name="userForgotPassword" ng-submit="submitForgotPasswordForm()" novalidate>
              <div aria-hidden="true" aria-labelledby="myModalLabel" role="dialog" tabindex="-1" id="myModal" class="modal fade">
                  <div class="modal-dialog" style="z-index:800;">
                      <div class="modal-content">
                          <div class="modal-header">
                              <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                              <h4 class="modal-title">Forgot Password ?</h4>
                          </div>
                          <div class="modal-body">
                          <div id="message2"></div>
                              <p>Enter your e-mail address below to reset your password.</p>
                              <input type="email" id="email" name="email" placeholder="Email ID" class="form-control placeholder-no-fix" ng-model="forgotpassword.email" required>
                              <span ng-show="forgotpasswordsubmitted && userForgotPassword.email.$invalid" class="help-block has-error warnig">Valid Email ID is required.</span>
          <span class="help-block has-error ng-hide warnig" ng-show="emailError">{{emailError}}</span>
    
                          </div>
                          <div class="modal-footer">
                              <button data-dismiss="modal" class="btn btn-default" type="button">Cancel</button>
                              <button class="btn btn-theme" type="submit" ng-click="forgotpasswordsubmitted = true">Submit</button>
                          </div>
                      </div>
                  </div>
              </div>
    
          </form>
      
      </div>