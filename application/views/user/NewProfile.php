<?php //echo "<pre>"; print_r($resumesInfo); exit; 
   ?>
<style>
   .coverLetter{
   min-width: 899px;
   min-height: 150px;
   }


   
</style>
<br>
<div class="container-fluid">
   <div class="row">
      <!-- <div class="col-md-2"></div> -->
      <div class="col-md-10 col-md-offset-1">
         <div class="freelancer-wrap resumeblock row-fluid clearfix">
            <?php if($this->session->flashdata('succmsg')){  ?>
            <div class="alert alert-success">
               <strong>Success!</strong> <?php echo $this->session->flashdata('succmsg'); ?>
               <!-- <button type="button" class="close" data-dismiss="modal">&times;</button> -->
               <a type="button" class="close" data-dismiss="alert" aria-hidden="true">×</a>
            </div>
            <?php    } ?>
            
            
            <h4>Add New Profile</h4>
            <form class="form-inline" name="userResume" ng-submit="submitUserResume()" novalidate>
               <br>
               <div class="form-group resumetitle">
                  <input type="text" class="form-control"  name="profileName" ng-model="uResume.profileName" require ng-pattern="/.*[a-zA-Z]+.*/" placeholder="Profile Name" required="">
                  <span id="uplErr" class="warning" style="color: red;"></span>
                  <span ng-show="submitted && userResume.profileName.$invalid" class="help-block has-error ng-hide warnig">Please enter profile name</span>
                  <span class="help-block has-error ng-hide warnig" style="color: red;" ng-show="profileNameError">{{profileNameError}}</span>
               </div>
               <!-- <textarea class="form-control coverLetter" ng-model="uResume.resumeCover" name="resumeCover" require ng-pattern="/.*[a-zA-Z]+.*/" rows="4" required></textarea> -->
               <!-- <h4>Upload New Cover Letter</h4> -->
               <strong>
                  <p>Upload New Cover Letter</p>
               </strong>
               <div class="form-group">
                  <input type="file" accept="application/pdf,application/msword,application/vnd.openxmlformats-officedocument.wordprocessingml.document" file-model="uploadFile" nv-file-select="" uploader="uploader2"/>
               </div>
               <button type="submit"  class="btn btn-primary pull-right btn-sm" style="width:200px;" ng-click="uploader2.uploadAll()"><i class="fa fa-upload"></i> Upload Cover Letter</button>
               <span id="succ" style="color: green;" ></span>
               <input type="hidden" class="form-control" id="covrLetter" name="covrLetter" ng-model="uResume.covrLetter" required/>
               <span id="uplErr" class="warning" style="color: red;"></span>
               <span ng-show="submitted && userResume.covrLetter.$invalid" class="help-block has-error ng-hide warnig">Please upload cover letter</span>
               <span class="help-block has-error ng-hide warnig" style="color: red;" ng-show="covrLetterError">{{covrLetterError}}</span>
               <br></br>
               <!-- <h4>Upload New Resume</h4> -->
               <strong>
                  <p>Upload New Resume</p>
               </strong>
               <p class="text-muted">Recruiters are looking for updated resumes.</p>
               <div class="form-group resumetitle">
                  <input type="text" class="form-control" id="" ng-model="uResume.resumeTitle" name="resumeTitle" require ng-pattern="/.*[a-zA-Z]+.*/" placeholder="Resume Title" required="">
                  <span id="uplErr" class="warning" style="color: red;"></span>
                  <span ng-show="submitted && userResume.resumeTitle.$invalid" class="help-block has-error ng-hide warnig">Please resume title</span>
                  <span class="help-block has-error ng-hide warnig" style="color: red;" ng-show="resumeTitleError">{{resumeTitleError}}</span>
               </div>
               <div class="form-group">
                  <input type="file" accept="application/pdf,application/msword,application/vnd.openxmlformats-officedocument.wordprocessingml.document" file-model="euploadFile" nv-file-select="" uploader="euploader2"/>
               </div>
               <button type="submit"   class="btn btn-primary pull-right btn-sm"  style="width:200px;" ng-click="euploader2.uploadAll()"><i class="fa fa-upload"></i> Upload Resume</button>
               <span id="succ2" style="color: green;" ></span>
               <input type="hidden" class="form-control" id="resumeUser" name="resumeUser" ng-model="uResume.resumeUser" required/>
               <span ng-show="submitted && userResume.resumeUser.$invalid" class="help-block has-error ng-hide warnig">Please upload resume</span>
               <span class="help-block has-error ng-hide warnig" style="color: red;" ng-show="resumeTitleError">{{resumeUserError}}</span>
         </div>
         <div class="form-group">
         <div class="col-md-3">
         <a href="<?php echo base_url(); ?>user/employment" type="button" class="btn btn-danger btn-sm"><strong><i class="fa fa-arrow-left"></i> Back</strong></a>
         <span class="help-block small text-muted">(Employment)</span>
         </div>
         <div class="col-md-9">
         <!-- <a href="<?php echo base_url(); ?>user/reference">  <button type="button"  class="btn btn-info pull-right btn-sm"><strong>Save &amp; Continue <i class="fa fa-arrow-right"></i></strong></button></a> -->
         <button type="submit" ng-click="submitted = true" class="btn btn-info pull-right btn-sm"><strong>Add <i class="fa fa-arrow-right"></i></strong></button>
         </form>
         <span class="clearfix"></span>
         <span class="help-block pull-right small text-muted">(References)</span>
         </div>
         </div>
      </div>
   </div>
</div>
<br>