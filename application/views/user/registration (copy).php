<style type="text/css">
    .hb-info p:before {
        content: '';
        width: 100%;
        height: 1px;
        background: #eee;
        display: block;
        margin-bottom: 10px;
    }
    .hb-info p {
        line-height: 20px;
        text-align: justify;
        color: #666;
    }
</style>

<div class="page_header">
    <div class="page_header_parallax2">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <h3><span>SIGNUP</span></h3>
                    <h3>STATUS<small>UIO</small> <br>Sign Up Here</h3>
                </div>
            </div>
        </div>
    </div>
    <div class="bcrumb-wrap">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <ul class="bcrumbs">
                        <li><a href="<?php echo base_url(); ?>index"><i class="fa fa-home"></i> Home</a></li>
                        <li>Sign Up</li>
                    </ul>
                    <div class="clearfix"></div>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="inner-content" ng-app="postApp" ng-controller="postController">

    <div class="container">
        <div class="row">
            <!-- <div class="col-md-6">
                
            </div> -->
            <div class="col-lg-3">
            </div>
            <div class="col-lg-6">
                  <div class="form-panel">
                      <?php echo $this->session->flashdata('successmsg');  ?>
                      <?php echo $this->session->flashdata('errormsg');  ?>
                      <h4 class="mb"><i class="fa fa-angle-right"></i> Sign Up</h4>
                      <form class="form-login" name="userLogin" ng-submit="submitForm()" novalidate>
                        <div id="message"></div>
                        <div class="login-wrap">
                            <input type="text" class="form-control" placeholder="Name" id="name" ng-model="user.name" name="name" required>                 
                            <span ng-show="submitted && userLogin.name.$invalid" class="help-block has-error">Name is required.</span>
                            <span class="help-block has-error" ng-show="nameError">{{nameError}}</span>
                        <br>
                        <input type="text" class="form-control" placeholder="Contact No." id="contactno" ng-model="user.contactno" name="contactno" required>                   
                            <span ng-show="submitted && userLogin.contactno.$invalid" class="help-block has-error">Contact no. is required.</span>
                            <span class="help-block has-error" ng-show="contactnoError">{{contactnoError}}</span>
                        <br>
                            <input type="email" class="form-control" placeholder="Email ID" id="emailaddress" ng-model="user.emailaddress" name="emailaddress" required>                    
                            <span ng-show="submitted && userLogin.emailaddress.$invalid" class="help-block has-error">Valid Email ID is required.</span>
                            <span class="help-block has-error" ng-show="emailaddressError">{{emailaddressError}}</span>
                        <br>
                            <input type="password" class="form-control" placeholder="Password" id="password" ng-model="user.password" name="password" required>
                            <span ng-show="submitted && userLogin.password.$error.required"  class="help-block has-error">Password is required.</span>
                            <span class="help-block has-error" ng-show="passwordError">{{passwordError}}</span>
                            <br>
                    <input type="password" class="form-control" placeholder="Confirm Password" id="cpassword" ng-model="user.cpassword" name="cpassword" required>
                    <span ng-show="submitted && userLogin.cpassword.$error.required"  class="help-block has-error">Confirm Password is required.</span>
                                    <span class="help-block has-error" ng-show="cpasswordError">{{cpasswordError}}</span>
 
                            <!-- <label class="checkbox">
                            <span class="pull-right">
                            <a data-toggle="modal" href="login.html#myModal"> Forgot Password?</a>
                            </span>
                            </label> -->
                            <br>
                            <button class="btn btn-info" type="submit" ng-click="submitted = true"><i class="fa fa-lock"></i> SUBMIT</button>
                            <span class="help-block has-error" ng-show="IsMatch">Password does not match Confirm Password.</span>
                        </div>
                       </form>
                  </div>
                </div>
                <div class="col-lg-3">
            </div>
        </div>
    </div>
</div>