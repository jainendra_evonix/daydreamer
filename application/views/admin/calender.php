<style type="text/css">
  .fc-event-skin.fc-event-skin2 {
    background-color: #1FB521 !important;
    border-color: #5DB51F !important;
    color: #FFFFFF !important;
  }
</style>


 <!--main content start-->
    <section id="" class="container">
        <section class="wrapper">
        <!-- page start-->
        <div class="row">
            
            <div class="col-md-12">
                <h4 class="form-heading"><strong>Calendar</strong></h4>
                <div class="row">
                  <form method="post" class="form-inline" action="<?php echo base_url();?>admin/viewCalender">
                    <div class="col-md-2">
                      <p><strong><?php echo date('jS M Y');?></strong></p>
                    </div>
                    <div class="col-md-6 col-md-push-1" style="background: #eee none repeat scroll 0 0; padding: 13px 18px;">
                      <!-- <form class="form-inline" method="post" action="<?php echo base_url();?>admin/viewCalender"> -->
                        <div class="form-group">
                          <label for="email" class="">Interview:</label>
                          <input type="checkbox" class="form-control" name="type" id="interview" value="interview" style="margin-right: 20px; margin-top: 0;">
                        </div>
                        <div class="form-group">
                          <label for="pwd">Jobs:</label>
                          <input type="checkbox" class="form-control" name="type" id="jobs" value="jobs" style="margin-right: 20px; margin-top: 0;">
                        </div>

                         <div class="form-group">
                          <label for="pwd">Training:</label>
                          <input type="checkbox" class="form-control" name="type"  id="training" value="training"  style="margin-right: 20px; margin-top: 0;">
                        </div>
                         <div class="form-group">
                          <label for="pwd">Events:</label>
                          <input type="checkbox" class="form-control" name="type" id="events" value="events" style="margin-right: 20px; margin-top: 0;">
                        </div>

                        <div class="form-group">
                          <label for="pwd">All:</label>
                          <input type="checkbox" class="form-control" name="type" id="all" value="all" style="margin-right: 20px; margin-top: 0;">
                        </div>
                        <input type="submit" class="btn btn-success" name="submit" value="submit">
                      <br>
                    </div>
                   </form>
                </div>
                

                <br>
                <section class="panel">
                    <div class="panel-body">
                        <!-- page start-->
                        <div class="row">
                            <aside class="col-sm-12">
                                  <div id="calendar" class="has-toolbar"></div>
                            </aside>
                            <aside class="col-sm-12">
                               
                            </aside>
                        </div>
                        <!-- page end-->
                    </div>
                </section>
                </div>
            </div>
        <!-- page end-->
        </section>
    </section>
    <!--main content end-->