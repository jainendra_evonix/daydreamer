
<style>
#msg_ifr {

  height:245px !important;
}
.help-block{

    color:red;
    position: absolute;
    left: 4px;
    bottom: -27px;
}

.compose-mail .form-group {
    position: relative;
    margin-bottom: 11px;
}

.error {
    color: red;
    /*position: absolute;*/
    top: 23px;
    left: 100px;
    width: 80% !important;
}
</style>
<!--main content start-->
<section id="main-content">
    <section class="wrapper">

        <div class="row">
            <div class="col-md-12">
                <ul class="breadcrumbs-alt">
                   
                    <!-- <li>
                        <a class="current" href="#">Email Template</a>
                    </li> -->
                </ul>
            </div>
        </div>

        <div class="row">
            <div class="col-sm-12" id="incident_mail">
                <section class="panel">
                    <?php echo $this->session->flashdata('message');?>
                    <header class="panel-heading wht-bg">
                     <h4 class="gen-case"> Email Template
                         <form action="#" class="pull-right mail-src-position">
                            <div class="input-append">
                               
                            </div>
                        </form>
                    </h4>
                </header>
                <div class="panel-body">
                   
                    <div class="clearfix"></div>
                    <div class="row">
                     
                        </div>
                       
                        <div class="compose-mail">
                            <form role="form-horizontal" method="post"  name="emailForm" id="email_format" action="<?php echo base_url();?>admin/insertEmailFormat">
                                <div class="form-group">
                                 <label class=" control-label">Template:</label>
                                
                                  <input type="text"  class="form-control" name="template_name" ng-model="email.template_name"> 
                                 <!--  <span ng-show="submitted && emailForm.template_name.$error.required"  class="help-block has-error ng-hide">Template Name is required.</span>
                                  <span ng-show="errorTemplateName" class="help-block has-error ng-hide">{{errorTemplateName}}</span> -->
                                
                              </div>
                                
                           
                                <div class="form-group">
                                    <label for="subject" class="">Subject:</label>
                                    <input type="text" name="subject" ng-model="email.subject" tabindex="1" id="subject" class="form-control">
                                    <!-- <span ng-show="submitted && emailForm.subject.$error.required"  class="help-block has-error ng-hide">Subject is required.</span>
                                     <span ng-show="errorSubject" class="help-block has-error ng-hide">{{errorSubject}}</span> -->
                                </div>

                                <div class="compose-editor form-group" id="emailtempbtn">
                                  
                                    <textarea id="msg" class="wysihtml5 form-control" name="message" ng-model="email.message" rows="9"></textarea>
                                    <!-- <span ng-show="submitted && emailForm.message.$error.required"  class="help-block has-error ng-hide">Message is required.</span>
                                    <span ng-show="errorMessage" class="help-block has-error ng-hide">{{errorMessage}}</span> -->
                                    <!-- <input type="file" class="default"> -->
                                </div>
                               <!--  <div class="compose-btn">
                                    <button class="btn btn-primary btn-sm"><i class="fa fa-check"></i> Send</button>
                                    <button class="btn btn-sm"><i class="fa fa-times"></i> Discard</button>
                                    <button class="btn btn-sm">Draft</button>
                                </div> -->
                                  <div class="" style="text-align:right;">
                                <button  class="btn btn-sm"><i class="fa fa-times"></i>Cancel</button>
                                <button type="submit"  class="btn btn-primary btn-sm"><i class="fa fa-floppy-o"></i> Save</button>
                                 </div>
                            </form>
                        </div>
                    </div>
                </section>
            </div>
        </div>

        <!-- page end-->
    </section>
</section>
<!--main content end-->
