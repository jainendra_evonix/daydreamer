


        <link rel="stylesheet" href="js/data-tables/DT_bootstrap.css" />

        <!--main content start-->

        <section id="adminsection" class="container">
            <section class="wrapper">
                <!-- page start-->
                <div class="row">
                    <div class="col-md-12">
                        <h4 class="form-heading"><strong>All Clients</strong></h4>

                        <?php echo $this->session->userdata('successmsg');?>
                        <?php echo $this->session->userdata('errormsg');?>
                        <a href="<?php echo base_url();?>admin/createOffice" class="btn btn-success pull-right" style="margin-left:900px;position:absolute;top:8px;">Create Client <i class="fa fa-plus"></i></a>

                        <br>
                        <?php
                        if(!empty($allOffices))
                        {

                        ?>
                        <section class="panel">
                            <div class="panel-body">
                                <div class="row">
                                    <div class="col-md-12">
                                       <!--  <button type="submit" class="btn btn-info pull-right btn-sm" style="margin-bottom:18px;"><strong><i class="fa fa-plus"></i> Create New Office</strong></button><span class="pull-right"> &nbsp; &nbsp; </span> -->

                                        <!-- <button type="button" class="btn btn-info pull-right btn-sm"><strong><i class="fa fa-times"></i> Cancel</strong></button> -->
                                        <!-- <br> -->
                                        <!-- <hr> -->
                                    </div>
                                    <div class="col-md-12">
                                        <section class="">
                                            <div class="table-responsive" style="border: 1px solid #ccc;padding: 6px;">
                                            <table  id="employee-grid"  cellpadding="0" cellspacing="0" border="0" class="display" width="100%">
                                                <thead>
                                                    <tr>
                                                        <th style="width: 35px;">Sr.No</th>
                                                        <th>Client Name</th>
                                                        <th style="width: 250px;">Contact Person</th>
                                                        <th style="width: 180px;">Action</th>
                                                        <!-- <th>Country</th>
                                                        <th>Contact No.</th> -->
                                                    </tr>
                                                </thead>
                                         
                                            </table>
                                            </div>
                                        </section>
                                    </div>
                                </div>
                            </div>
                        </section>
                        <?php }else{?>

                        <section class="panel" style="display:none;">
                            <div class="panel-body">
                                <div class="row">
                                    <div class="col-md-12">
                                       <!--  <button type="submit" class="btn btn-info pull-right btn-sm" style="margin-bottom:18px;"><strong><i class="fa fa-plus"></i> Create New Office</strong></button><span class="pull-right"> &nbsp; &nbsp; </span> -->

                                        <!-- <button type="button" class="btn btn-info pull-right btn-sm"><strong><i class="fa fa-times"></i> Cancel</strong></button> -->
                                        <!-- <br> -->
                                        <!-- <hr> -->
                                    </div>
                                    <div class="col-md-12">
                                        <section class="">
                                            <div class="table-responsive" style="border: 1px solid #ccc;padding: 6px;">
                                            <table  id="employee-grid"  cellpadding="0" cellspacing="0" border="0" class="display" width="100%">
                                                <thead>
                                                    <tr>
                                                        <th style="width: 35px;">Sr.No</th>
                                                        <th>Client Name</th>
                                                        <th style="width: 250px;">Contact Person</th>
                                                        <th style="width: 150px;">Action</th>
                                                        <!-- <th>Country</th>
                                                        <th>Contact No.</th> -->
                                                    </tr>
                                                </thead>
                                         
                                            </table>
                                            </div>
                                        </section>
                                    </div>
                                </div>
                            </div>
                        </section>
                        <?php }?>
                    </div>
                </div>
                <!-- page end-->

            </section>
        </section>
        <!--main content end-->





    
</div>




<!--dynamic table initialization -->
