<style type="text/css">
table {
  border: 1px solid #c0c0c0;
  page-break-inside: avoid;
}
.table-inverse {
    background-color: #373a3c;
    color: #eceeef;
}
.container { margin:150px auto;}
td, th {
  border: 1px solid #c0c0c0;
  padding: .4em;
}

tr.row th { text-align: left; }

td.sum, td.total { text-align: right; }

tfoot tr th { text-align: left; }

table { border-collapse: separate; }

.ts-time {
  text-align: right;
  width: 6em;
}

.ts-time.ts-start { color: rgb(238, 130, 14); }

.ts-time.ts-end { color: rgb(8, 151, 59); }

.ts-delta { color: rgb(18, 151, 214); }
</style>

<div class="container">
	<div class="row">
     <div class="col-sm-12">
   <h3>View Timesheet</h3>
<!-- <table class="ts-grid table table-inverse">
      <thead>
    <tr>
          <th style="visibility: hidden;">Days</th>
          <th>Morning</th>
          <th>Afternoon</th>
          <th>Duration</th>
        </tr>
  </thead>
      <tbody>
    <tr class="ts-row">
          <th>Monday</th>
          <td class="ts-range"><input class="ts-time ts-start" type="time" value=""/>
        to
        <input class="ts-time ts-end" type="time" value=""/></td>
          <td class="ts-range"><input class="ts-time ts-start" type="time" value="14:00"/>
        to
        <input class="ts-time ts-end" type="time" value="17:45"/></td>
          <td class="ts-sum"></td>
        </tr>
    <tr class="ts-row">
          <th>Tuesday</th>
          <td class="ts-range"><input class="ts-time ts-start" type="time" value="08:30"/>
        to
        <input class="ts-time ts-end" type="time" value="12:30"/></td>
          <td class="ts-range"><input class="ts-time ts-start" type="time" value="14:00"/>
        to
        <input class="ts-time ts-end" type="time" value="17:45"/></td>
          <td class="ts-sum"></td>
        </tr>
    <tr class="ts-row">
          <th>Wednesday</th>
          <td class="ts-range"><input class="ts-time ts-start" type="time" value="08:30"/>
        to
        <input class="ts-time ts-end" type="time" value="12:30"/></td>
          <td class="ts-range"><input class="ts-time ts-start" type="time" value="14:00"/>
        to
        <input class="ts-time ts-end" type="time" value="17:45"/></td>
          <td class="ts-sum"></td>
        </tr>
    <tr class="ts-row">
          <th>Thursday</th>
          <td class="ts-range"><input class="ts-time ts-start" type="time" value="08:30"/>
        to
        <input class="ts-time ts-end" type="time" value="12:30"/></td>
          <td class="ts-range"><input class="ts-time ts-start" type="time" value="14:00"/>
        to
        <input class="ts-time ts-end" type="time" value="17:45"/></td>
          <td class="ts-sum"></td>
        </tr>
    <tr class="ts-row">
          <th>Friday</th>
          <td class="ts-range"><input class="ts-time ts-start" type="time" value="08:30"/>
        to
        <input class="ts-time ts-end" type="time" value="12:30"/></td>
          <td class="ts-range"><input class="ts-time ts-start" type="time" value="14:00"/>
        to
        <input class="ts-time ts-end" type="time" value="17:30"/></td>
          <td class="ts-sum"></td>
        </tr>
    <tr class="ts-row">
          <th>Saturday</th>
          <td class="ts-range"><input class="ts-time ts-start" type="time" value="08:30"/>
        to
        <input class="ts-time ts-end" type="time" value="12:30"/></td>
          <td class="ts-range"><input class="ts-time ts-start" type="time" value=""/>
        to
        <input class="ts-time ts-end" type="time" value=""/></td>
          <td class="ts-sum"></td>
        </tr>
    <tr class="ts-row">
          <th>Sunday</th>
          <td class="ts-range"><input class="ts-time ts-start" type="time" value=""/>
        to
        <input class="ts-time ts-end" type="time" value=""/></td>
          <td class="ts-range"><input class="ts-time ts-start" type="time" value=""/>
        to
        <input class="ts-time ts-end" type="time" value=""/></td>
          <td class="ts-sum"></td>
        </tr>
  </tbody>
      <tfoot>
    <tr>
          <th colspan="3">Total worked hours of the week</th>
          <td class="ts-total"></td>
        </tr>
  </tfoot>
    </table>
        -->

    
<br>
</div>

<div class="col-md-12">
<table>
       <thead>
         <tr>
           <th>Date</th>
           <th>Day</th>
           <th>Start Time</th>
           <th>Finish Time</th>
           <th>Unpaid Break Duration</th>
           <th>Pay Rate</th>
           <th>Pay Type</th>
         </tr>
       </thead>
       <tbody>
         <tr>
          <td>2017-01-25</td>
          <td>Monday</td>
          <td>9:30 AM</td>
          <td>6:30 PM</td>
          <td>7</td>
          <td>20</td>
          <td>demo</td>

         </tr>
         <tr>
          <td>2017-01-25</td>
          <td>Monday</td>
          <td>9:30 AM</td>
          <td>6:30 PM</td>
          <td>7</td>
          <td>20</td>
          <td>demo</td>

         </tr>
         <tr>
          <td>2017-01-25</td>
          <td>Monday</td>
          <td>9:30 AM</td>
          <td>6:30 PM</td>
          <td>7</td>
          <td>20</td>
          <td>demo</td>

         </tr>
         <tr>
          <td>2017-01-25</td>
          <td>Monday</td>
          <td>9:30 AM</td>
          <td>6:30 PM</td>
          <td>7</td>
          <td>20</td>
          <td>demo</td>

         </tr>
 
       </tbody>
    </table>

</div>

</div>
	</div>