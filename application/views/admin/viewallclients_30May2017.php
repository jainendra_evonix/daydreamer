


        <link rel="stylesheet" href="js/data-tables/DT_bootstrap.css" />

        <!--main content start-->
        <section id="adminsection" class="container">
            <section class="wrapper">
                <!-- page start-->
                <div class="row">
                    <div class="col-md-12">
                        <h4 class="form-heading"><strong>All Offices</strong></h4>

                        <?php echo $this->session->userdata('successmsg');?>
                        <?php echo $this->session->userdata('errormsg');?>
                        <p>All Offices information list. <a href="#" class="pull-right">Help <i class="fa fa-question-circle"></i></a></p>

                        <br>
                        <section class="panel">
                            <div class="panel-body">
                                <div class="row">
                                    <div class="col-md-12">
                                       <!--  <button type="submit" class="btn btn-info pull-right btn-sm" style="margin-bottom:18px;"><strong><i class="fa fa-plus"></i> Create New Office</strong></button><span class="pull-right"> &nbsp; &nbsp; </span> -->

                                        <!-- <button type="button" class="btn btn-info pull-right btn-sm"><strong><i class="fa fa-times"></i> Cancel</strong></button> -->
                                        <!-- <br> -->
                                        <!-- <hr> -->
                                    </div>
                                    <div class="col-md-12">
                                        <section class="">
                                            <div class="adv-table">
                                            <table  class="display table table-bordered table-striped" id="dynamic-table">
                                            <thead>
                                            <tr>
                                                <th>Sr.No</th>
                                                <th>Client Name</th>
                                                <th>Action</th>
                                                <!--<th>Country</th>
                                                <th>Contact No.</th>
                                                 -->
                                            </tr>
                                            </thead>
                                            <tbody>
                                                <?php

                                               $count=1;
                                               foreach($allOffices as $key){
                                                ?>
                                            <tr class="gradeX">
                                                
                                                <td><?php echo $count++;?></td>
                                                <td><span data-toggle="collapse" onclick="return myFunction(<?php echo $key->id;?>);" data-target="#<?php echo $key->id;?>" style="cursor: pointer;"><?php echo $key->clnt_first_name.' '.$key->clnt_last_name?></span>
                                                    <div id="<?php echo $key->id;?>" class="collapse">    
                                                        <h5><strong>Sub clients:</strong></h5>
                                                        <ul class="list-inline list-unstyled" id="showSubOffices">
                                                          
                                                         
                                                           
                                                           
                                                        </ul>


                                                    </div>

                                                   <!--  <div id="demo"></div> -->
                                                
                                                </td>
                                                 <td>
                                                   <ul class="list-unstyled list-inline">
                                                    <?php
                                                     if($key->deactivate =='0'){

                                                    ?>
                                                     <li onclick="return deactivateClient();"><a href="<?php echo base_url();?>admin/disableClient?id=<?php echo $key->id;?>" title="activate client"><i class="fa fa-check-square-o" aria-hidden="true"></i></a></li>
                                                     <?php } else {?>
                                                     <li onclick="return activateClient();"><a href="<?php echo base_url();?>admin/enableClient?id=<?php echo $key->id;?>" title="deactivate client"><i class="fa fa-minus-circle" aria-hidden="true"></i></a></li>
                                                        <?php }?>

                                                        <li><a href="<?php echo base_url();?>admin/editClientDetails?id=<?php echo $key->id;?>" title="Update Information"><i class="fa fa-pencil" aria-hidden="true"></i></a>
                                                        </li>
                                                   </ul>


                                                 </td>
                                                <!--<td><?php //echo $key->clnt_country;?></td>
                                                <td><?php //echo $key->clnt_telephone;?></td> -->
                                                
                                            </tr>
                                            <?php }?>
                                            </tbody>
                                            </table>
                                            </div>
                                        </section>
                                    </div>
                                </div>
                            </div>
                        </section>
                    </div>
                </div>
                <!-- page end-->

            </section>
        </section>
        <!--main content end-->





    
</div>




<!--dynamic table initialization -->
