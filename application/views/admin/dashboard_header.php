

<body ng-app="postApp" ng-controller="postController as ctrl">

<section id="container">
      <header class="header clearfix" id="adminheader">
          <!--logo start-->
          <div class="brand">

              <a href="index.html" class="logo">
                  <!-- <img src="images/logo.png" alt=""> -->
                  DAY DREAMER
              </a>
          </div>
          <!--logo end-->

           <!-- <div class="horizontal-menu">
            <form role="form">
              <div class="form-group" style="position: relative; margin-bottom: 0;">
                <input type="text" class="form-control search" placeholder=" Search" style="width: 300px; border: 1px solid #ccc; border-radius: 25px; color: #333; font-weight: 400;">
                <button class="btn btn-default" type="button" style="position: absolute; top: 0; right: 0; border-radius: 0 25px 25px 0;"><strong>Search</strong></button>
              </div>
            </form>
          </div> -->


          <div class="horizontal-menu navbar-collapse collapse ">
           <!--  <ul class="nav navbar-nav">
                <!-- <li><a href="#">Home</a></li> -->
                <!-- <li class="active"><a href="#">Jobs</a></li> -->
                <!-- <li class="dropdown">
                    <a data-toggle="dropdown" data-hover="dropdown" class="dropdown-toggle" href="#">Recruiters <b class=" fa fa-angle-down"></b></a>
                    <ul class="dropdown-menu">
                        <li><a href="#">Browse All Recruiters</a></li>
                        <li><a href="#">Add Recruiters</a></li>
                        <li><a href="#">Search Recruiters</a></li>
                    </ul>
                </li>
                <li><a href="#">Services</a></li>
                <li class="dropdown">
                    <a data-toggle="dropdown" data-hover="dropdown" class="dropdown-toggle" href="#">Companies <b class=" fa fa-angle-down"></b></a>
                    <ul class="dropdown-menu">
                        <li><a href="#">All Companies</a></li>
                        <li><a href="#">Add Companies</a></li>
                        <li><a href="#">Search Companies</a></li>
                    </ul>
                </li> -->
            <!--</ul> -->

          </div>

          <div class="top-nav clearfix">
              <!--search & user info start-->
              <ul class="nav pull-right top-menu">
                  <li>
                      <input type="text" class="form-control search" placeholder=" Search" style="background-image: url('<?php echo base_url();?>client_assets/images/search-icon.png');">
                  </li>
                  <!-- user login dropdown start-->
                  <?php
                  $admin_session_data=$this->session->userdata(base_url().'login_session');
                  $adminFname=$admin_session_data['first_name'];
                  $adminLname=$admin_session_data['last_name'];
                  $adminName=$adminFname.' '.$adminLname;
                  
                   $adminProfilePic=$admin_session_data['profile_picture'];
                  // //echo $adminProfilePic;
                  $profilepic=base_url().'client_uploads/profile_pic/'.$adminProfilePic;
                 // echo $adminProfilePic;exit;
                  ?>
	                 <?php if(!empty($admin_session_data)){
                      
                  
                    ?>
                  <li class="dropdown">
                      <a data-toggle="dropdown" class="dropdown-toggle" href="#">
                          <img alt="" src="<?php echo $profilepic;?>">
                          <span class="username"><?php echo $adminName;?></span>
                          <b class="caret"></b>
                      </a>
                      <ul class="dropdown-menu extended logout">
                          <li><a href="<?php echo base_url();?>admin/viewProfile"><i class=" fa fa-suitcase"></i>Profile</a></li>
                          <li><a href="<?php echo base_url();?>admin/changePassword"><i class="fa fa-cog"></i> Settings</a></li>
                          <!-- <li><a href="<?php //echo base_url();?>admin/changeProfilePicture"><i class="fa fa-cog"></i> Change Profile Picture</a></li> -->
                          <li><a href="<?php echo base_url();?>admin/logout"><i class="fa fa-key"></i> Log Out</a></li>
                      </ul>
                  </li>
                  <?php }?>
                 
                  <!-- user login dropdown end -->
              </ul>
              <!--search & user info end-->
          </div>
      </header>
</section>

<section id="adminheadermain">
    <!--navigation start-->
    <nav class="navbar navbar-inverse" role="navigation">
        <!-- Brand and toggle get grouped for better mobile display -->
        <div class="navbar-header">
            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-ex1-collapse">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <!-- <a class="navbar-brand" href="#">Daydreamer</a> -->
        </div>

        <!-- Collect the nav links, forms, and other content for toggling -->
        <div class="collapse navbar-collapse navbar-ex1-collapse">
            <ul class="nav navbar-nav">
                <li id="home"><a href="<?php echo base_url();?>admin/viewDashboard">Home</a></li>
                <li id="organization"><a href="<?php echo base_url();?>admin/vieworganization">Organization</a></li>
                <li id="contacts"><a href="<?php echo base_url();?>admin/viewContacts">Contacts</a></li>
                <li class="dropdown" id="job">
                    <a href="javascript:;" class="dropdown-toggle" data-toggle="dropdown">Clients <b class="caret"></b></a>
                    <ul class="dropdown-menu">
                        <li><a href="<?php echo base_url();?>admin/createOffice">Create New client</a></li>
                        <li><a href="<?php echo base_url();?>admin/viewAllOffices">All Clients</a></li>
                        <li><a href="<?php echo base_url();?>admin/viewClientCSVForm">Upload CSV</a></li>
                    </ul>
                </li>
                <!-- <li class="dropdown">
                    <a href="javascript:;" class="dropdown-toggle" data-toggle="dropdown">Candidates <b class="caret"></b></a>
                    <ul class="dropdown-menu">
                        <li><a href="#">Create New Candidate</a></li>
                        <li><a href="#">All Candidates</a></li>
                    </ul>
                </li> -->
               <!--  <li class="dropdown">
                    <a href="javascript:;" class="dropdown-toggle" data-toggle="dropdown">Interviews <b class="caret"></b></a>
                    <ul class="dropdown-menu">
                        <li><a href="#">Create Interview</a></li>
                        <li><a href="#">All Interviews</a></li>
                    </ul>
                </li> -->
               
                  <!-- <li class="dropdown"  id="client">
                    <a href="javascript:;" class="dropdown-toggle" data-toggle="dropdown">Candidates <b class="caret"></b></a>
                    <ul class="dropdown-menu"> -->
                      
                        <!-- <li><a href="#">Create New Candidate</a></li> -->
                       
                        
                       
                        
                  <!--   </ul>
                </li> -->
                  
                <li class="dropdown">
                    <a href="javascript:;" class="dropdown-toggle" data-toggle="dropdown">Candidates <b class="caret"></b></a>
                    <ul class="dropdown-menu">
                       <li ><a href="<?php echo base_url();?>admin/viewAllCandidates">All Candidates</a></li>
                        <li><a href="<?php echo base_url();?>admin/viewCSVForm">Upload CSV</a></li>
                    </ul>
                </li>
                
                <li id="package"><a href="<?php echo base_url();?>admin/createPackage">Packages</a></li>
               
                 <li class="dropdown" id="jobs">
                    <a href="javascript:;" class="dropdown-toggle" data-toggle="dropdown">Jobs <b class="caret"></b></a>
                    <ul class="dropdown-menu">
                        <li id="jobs"><a href="<?php echo base_url();?>admin/viewJobs">Jobs</a></li>
                        <li><a href="<?php echo base_url();?>admin/viewJobCalender">ViewCalender</a></li>
                    </ul>
                </li>
                <li class="dropdown" id="jobs">
                    <a href="javascript:;" class="dropdown-toggle" data-toggle="dropdown">Homeless <b class="caret"></b></a>
                    <ul class="dropdown-menu">
                        <li id="homeless"><a href="<?php echo base_url();?>admin/viewHomeless">View Homeless</a></li>
                        <li><a href="<?php echo base_url();?>admin/viewSubstance">View Substance</a></li>
                    </ul>
                </li>
                <!--  <li id="analytics"><a href="<?php //echo base_url();?>admin/viewAnalytics">Analytics</a></li> -->

                  <li class="dropdown" id="blogs">
                    <a href="javascript:;" class="dropdown-toggle" data-toggle="dropdown">Blog <b class="caret"></b></a>
                 <ul class="dropdown-menu">
                        <li><a href="<?php echo base_url();?>admin/viewBlogForm">Create New Blog</a></li>
                        <li><a href="<?php echo base_url();?>admin/viewAllBlogs">View All Blogs</a></li>
                    </ul>
                  </li>

                  <li id="package"><a href="<?php echo base_url();?>admin/emailTemplates">Templates</a></li>
                  <!-- <li id="interviews"><a href="<?php echo base_url();?>admin/viewInterviewForm">Interviews</a></li> -->
                  <li class="dropdown" id="interview">
                    <a href="javascript:;" class="dropdown-toggle" data-toggle="dropdown">Interview <b class="caret"></b></a>
                 <ul class="dropdown-menu">
                        <!-- <li><a href="<?php echo base_url();?>admin/viewInterviewForm">Create New Interview</a></li> -->
                        <li><a href="<?php echo base_url();?>admin/viewAllInterviews">View All Interviews</a></li>
                        <li><a href="<?php echo base_url();?>admin/viewInterviewCalender">View Calender</a></li>
                        <li><a href="<?php echo base_url();?>admin/createGroupInterview">Group Interview</a></li>
                        <!-- <li></li> -->
                    </ul>
                  </li>
                  <li id="training"><a href="<?php echo base_url();?>admin/viewTrainingForm">Training</a></li>
                  <li id="calender"><a href="<?php echo base_url();?>admin/viewCalender">Calender</a></li>
                 <li id="timesheets"><a href="<?php echo base_url();?>admin/manageTimesheets">Timesheet</a></li>
                   <!-- <li class="dropdown">
                    <a href="javascript:;" class="dropdown-toggle" data-toggle="dropdown">Timesheet<b class="caret"></b></a>
                    <ul class="dropdown-menu">
                         <li id="timesheets"><a href="<?php //echo base_url();?>admin/createTimesheets">Create Timesheet</a></li> -->
                         
                        <!--  <li id="timesheets"><a href="<?php //echo base_url();?>admin/viewTimesheets">view Timesheet</a></li> -->
                  <!--   </ul>
                </li> -->
                  
                  <li class="dropdown">
                    <a href="javascript:;" class="dropdown-toggle" data-toggle="dropdown">Event<b class="caret"></b></a>
                    <ul class="dropdown-menu">
                        <li id="events"><a href="<?php echo base_url();?>admin/viewEventForm">Add Event</a></li>
                        <li id="allevents"><a href="<?php echo base_url();?>admin/viewEvents">All Events</a></li>
                        <li id="allevents"><a href="<?php echo base_url();?>admin/viewEventCalender">View Calender</a></li>
                    </ul>
                </li>
                  <li id="data"><a href="<?php echo base_url();?>admin/viewDataForm">Data</a></li>

                  <li class="dropdown">
                    <a href="javascript:;" class="dropdown-toggle" data-toggle="dropdown">Referrals<b class="caret"></b></a>
                    <ul class="dropdown-menu">
                        <li id="events"><a href="<?php echo base_url();?>admin/viewCandidateReferralForm">Candidate referral form</a></li>
                        <li id="allevents"><a href="<?php echo base_url();?>admin/viewBussinessReferalForm">Business referral form</a></li>
                        <li id="allevents"><a href="<?php echo base_url();?>admin/viewHomelessForm">Homeless person input form</a></li>
                    </ul>
                </li>

                  
               <!--  <li><a href="#">Reports</a></li> -->
               
               <!-- <li id="viewCredentials"><a href="<?php //echo base_url();?>client/viewAllCredentials">View Credentials</a></li>
                
                <li id="feedback"><a href="<?php //echo base_url();?>client/viewFeedbackForm">Feedback</a></li>

                <li id="feedback"><a href="<?php //echo base_url();?>admin/createPackage">Packages</a></li> -->
                
            </ul>

            <!-- <ul class="nav navbar-nav navbar-right">
                <li><a href="javascript:;">Link</a></li>
                <li class="dropdown">
                    <a href="javascript:;" class="dropdown-toggle" data-toggle="dropdown">Dropdown <b class="caret"></b></a>
                    <ul class="dropdown-menu">
                        <li><a href="#">Action</a></li>
                        <li><a href="#">Another action</a></li>
                        <li><a href="#">Something else here</a></li>
                        <li><a href="#">Separated link</a></li>
                    </ul>
                </li>
            </ul> -->
        </div><!-- /.navbar-collapse -->
    </nav>
    <!--navigation end-->
</section>