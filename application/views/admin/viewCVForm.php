                                           <style type="text/css">
                                           .help-block{

                                            color:red;
                                        }

                                        </style>
                                        <section class="panel container">
                                            <div class="panel-body">
                                             <h4>View and Save CV's</h4>
                                               <?php echo $this->session->flashdata('successmsg');?>
                                               <?php echo $this->session->flashdata('errormsg');?>
                                             <hr>

                                             <form class="form-horizontal bucket-form" ng-submit="submitCVForm()" name="CVForm" novalidate>
                                                <input type="hidden" name="package_id" ng-model="cv.package_id">
                                            <!-- <div class="form-group" >
                                                <label class="col-sm-3 control-label">Job Ads:</label>
                                                <div class="col-sm-9 icheck">
                                                    <div class="square single-row">
                                                        <div class="">
                                                            <input tabindex="3" type="radio" name="jobadds" value="yes" ng-model="jobadds.jobadds" onclick="showJobDiv();">

                                                            <label>Yes </label>
                                                        </div>
                                                        <div class="">
                                                            <input tabindex="3" type="radio" name="jobadds" value="no" ng-model="jobadds.jobadds" onclick="hideJobDiv();">
                                                            <label>No </label>
                                                        </div>
                                                    </div>
                                                    <span ng-show="submitted && jobAddsForm.jobadds.$error.required"  class="help-block has-error ng-hide">Job Ads is required</span>
                                                </div>
                                            </div> -->
                                            <br><br>
                                            <div class="form-group" id="job_adds_div">
                                                <div class="col-sm-3">
                                                    <label for="">Number of CV's:</label>
                                                    <input type="text" class="form-control" id="job_adds" name="no_of_cv" ng-model="cv.no_of_cv" placeholder="" required>
                                                    <span ng-show="submitted && submitCVForm.no_of_cv.$error.required"  class="help-block has-error ng-hide">Number of CV is required</span>
                                                </div>
                                                <div class="col-sm-3">
                                                    <label for="">Number of View's:</label>
                                                    <input type="text" class="form-control" id="job_adds" name="no_of_views" ng-model="cv.no_of_views" placeholder="" required>
                                                    <span ng-show="submitted && submitCVForm.no_of_views.$error.required"  class="help-block has-error ng-hide">Number of Views is required</span>
                                                   
                                                    
                                                </div>
                                                <div class="col-sm-3">
                                                    <label for="">Number of Days:</label>
                                                    <select class="form-control m-bot15" id="duration" name="no_of_days" ng-model="cv.no_of_days" required>
                                                        <option disabled="disabled" selected="selected">Select duration</option>
                                                        <option>1</option>
                                                        <option>2</option>
                                                        <option>3</option>
                                                        <option>4</option>
                                                        <option>5</option>
                                                        <option>6</option>
                                                        <option>7</option>
                                                        <option>8</option>
                                                        <option>9</option>
                                                        <option>10</option>
                                                        <option>11</option>
                                                        <option>12</option>
                                                        <option>13</option>
                                                        <option>14</option>
                                                        <option>15</option>
                                                        <option>16</option>
                                                        <option>18</option>
                                                        <option>19</option>
                                                        <option>20</option>
                                                        <option>21</option>
                                                        <option>22</option>
                                                        <option>23</option>
                                                        <option>24</option>
                                                        <option>25</option>
                                                        <option>26</option>
                                                        <option>28</option>
                                                        <option>29</option>
                                                        <option>30</option>
                                                        <option>31</option>
                                                        
                                                    </select>
                                                     <span ng-show="submitted && submitCVForm.no_of_days.$error.required"  class="help-block has-error ng-hide">Number of Days is required</span>
                                                    
                                                </div>
                                                


                                                
                                                <div class="col-sm-9">
                                                    <hr>
                                                    <button type="submit" class="btn btn-info btn-sm pull-right" ng-click="submitted = true" id="btn"><strong><i class="fa fa-plus"></i> Save <i class="fa fa-"></i></i></strong></button><span class="pull-right"> &nbsp; &nbsp; </span>
                                                    <br>
                                                </div>
                                            </form>
                                        </div>

                                        <div class="form-group">
                                            <div class="col-sm-9">
                                                <br>
                                                <table class="table table-bordered table-condensed">
                                                    <thead style="background-color: #474752; color: #fff;">
                                                      <tr>
                                                        <th>Number of CV's</th>
                                                        <th>Number of View's</th>
                                                        <th>Number of Days</th>
                                                        <th>Action</th>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                   <?php 
                                                      foreach($cvInfo as $key){
                                                       // echo '<pre>';print_r($key);
                                                   ?>
                                                  <tr>
                                                    <td><?php echo $key->no_of_cv;?></td>
                                                    <td><?php echo $key->no_of_views;?></td>
                                                    <td><?php echo $key->no_of_days;?></td>
                                                    <td><a href="<?php echo base_url();?>admin/deleteViewCV?id=<?php echo $key->id;?>&&package_id=<?php echo $package_id;?>" onclick="return deleteCVConfirmMsg();"><i class="fa fa-trash"></i> Delete</a></td>
                                                </tr>
                                                <?php }?>



                                            </tbody>
                                        </table><br>
                                        <a href="<?php echo base_url();?>admin/createPackage"  class="btn btn-danger btn-sm pull-right" ng-click="submitted = true" id="btn"><strong><i class="fa fa-arrow-left"></i> Complete <i class="fa fa-"></i></i></strong></a>
                                    </div>
                                </div>
                            </div>
                        </section>