<style type="text/css">
.sorting{

  width:64px;

}
.autocomplete-suggestions {
  background-color: #fff;
  border: 1px solid #ddd;
  box-shadow: 0 3px 10px rgba(0, 0, 0, 0.2);
}
.autocomplete-suggestion {
  padding: 5px 12px;
}
.autocomplete-suggestion:hover {
  background-color: #333;
  color: #fff;
}
</style>
<!--main content start-->
<section class="panel container">
<section class="panel-body">
  <div class="row">
    <div class="col-md-12">
      <form class="form-inline" method="post" action="<?php echo base_url();?>admin/viewJobs">
  <div class="form-group">
    <label for="email" style="width: 100px;">Job Title:</label>
    <input type="text" name="job_title" class="form-control" id="job_title" style="width: 200px; margin-right: 25px;">
  </div>
  <div class="form-group">
    <label for="pwd" style="width: 100px;">Min Salary:</label>
    <input type="text" name="min_salary" class="form-control" id="salary" style="width: 200px; margin-right: 25px;">
  </div>

   <div class="form-group">
    <label for="pwd" style="width: 100px;">Max Salary:</label>
    <input type="text" name="max_salary" class="form-control" id="salary" style="width: 200px; margin-right: 25px;">
  </div>
  <div class="clearfix"><br></div>
  <div class="form-group">
    <label for="pwd" style="width: 100px;">Job Type:</label>
    <select name="job_type" class="form-control" style="width: 200px; margin-right: 25px;">
     <option value="" selected="true">(Select a Job Type)</option>
      <?php foreach($jobType as $key){?>
      <option value="<?php echo $key->id;?>"><?php echo $key->job_type?></option>
       <?php }?>
    </select>
  </div>
   <div class="form-group">
    <label for="pwd" style="width: 100px;">Client Name:</label>
    <input type="text"  class="form-control" id="selctionajax" name="client_name" style="width: 200px; margin-right: 25px;">
    <input type="hidden" name="client_name" ng-model="interview.client_name" id="organizationname">
    <br>
  </div>
  <button type="submit" class="btn btn-info">Submit</button>
</form> 

    </div>

  </div>
</section>
</section>
<section id="adminsection" class="container">
    <section class="wrapper">
        <!-- page start-->
        <div class="row">
            <div class="col-md-12">
              <?php //echo $id;exit;?>
                <?php echo $this->session->flashdata('successmsg');  ?>
                <?php echo $this->session->flashdata('errormsg');  ?>
                <h4 class="form-heading"><strong>All Clients Jobs</strong></h4>
                <!-- <p>Create and showcase all Job Openings. <a href="#" class="pull-right">Help <i class="fa fa-question-circle"></i></a></p> -->

                <br>
                <section class="panel">
                    <div class="panel-body">
                        <div class="row">
                          <!-- <form>
                            <input type="hidden" name="client_id" id="client_id" value="<?php //echo $id;?>">
                            <input type="hidden" name="client_id" id="subclient_id" value="<?php //echo $subClient_id;?>">
                          </form> -->
                         <!--  <form method="post" action="<?php //echo base_url();?>admin/mainJobsData">
                            <div class="col-md-9 pageSearch">
                                <div class="searchform">
                                  <div class="row">
                                    <div class="col-md-3 col-sm-3">
                                      <input type="text" name="job_title" class="form-control" placeholder="Job Title">
                                  </div>
                                  <div class="col-md-3 col-sm-2">
                                      <!-- <select class="form-control">
                                        <option>Industry</option>
                                    </select> -->
                                  <!--   <input  type="text" id="closeDate" name="closing_date"  class="form-control" placeholder="Closing Date">
                                </div> -->
                                <!--  <div class="col-md-4 col-sm-3">
                                      <input type="text" class="form-control" placeholder="Closing Date">
                                  </div> -->
                               <!--  <div class="col-md-3 col-sm-2">
                                  <select class="form-control" name="industry">
                                    <option disabled="disabled" selected="selected" value="">Select your Industry</option>
                                    <?php
                                     //foreach($allIndustries as $key){

                                    ?>

                                    <option value="<?php //echo $key->ind_id?>"><?php //echo $key->industry_name;?></option>
                                    <?php //} ?>
                                </select>
                            </div> -->
                           <!--  <div class="col-md-2 col-sm-3">
                              
                            <input type="text" name="salary" class="form-control" placeholder="Salary">
                        </div>
                        <div class="col-md-1 col-sm-2 nopadding">
                          <button type="submit" class="btn"><i class="fa fa-search" aria-hidden="true"></i></button>
                      </div>
                  </div>
              </div>
          </div>
          </form> -->
          <div class="col-md-8">
            <!-- <button type="submit" class="btn btn-info pull-right btn-sm"><strong><i class="fa fa-plus"></i> New Job Opening</strong></button><span class="pull-right"> &nbsp; &nbsp; </span> -->

            <!-- <button type="button" class="btn btn-info pull-right btn-sm"><strong><i class="fa fa-times"></i> Cancel</strong></button> -->
            <!-- <br> -->
            <!-- <hr> -->
        </div>
        <div class="col-md-12">
            <section class="">
                <div class="table-responsive" style="border: 1px solid #ccc;padding: 6px;">

                    <table  id="employee-grid"  cellpadding="0" cellspacing="0" border="0" class="display" width="100%">
                        <thead>
                            <tr>
                             
                             <th>Sr.No</th>
                             <th>Job Title</th>
                             <th>Recruiter</th>
                            <!--  <th style="width: 15%;">Manager</th>
                             <th style="width: 15%;" >Positions</th>
                             <th style="width: 15%x">City</th> -->
                             <th>Job Status</th>
                             <th >Salary</th>
                             <th >Currency</th>
                             <th >Client Name</th>
                             <th >Created Date</th>
                             <th >Closing Date</th>
                             <th>Job Type</th>
                             <th >Action</th>
                         </tr>
                     </thead>
                    <!--  -->
                </table>
            </div>
        </section>
    </div>
</div>
</div>
</section>
</div>
</div>
<!-- page end-->

</section>
</section>
<!--main content end-->






</div>



<!--dynamic table initialization -->
