
<style type="text/css">
.help-block{
  color:red;

}

.form-control.common{

  background-color: #fff;
}
#accordionedu .panel .panel-heading.heading-invert {
  background: #474752 none repeat scroll 0 0;
  border-color: #474752;
  border-radius: 0;
}
.common{

  color: white;
}
.help-block{

  color: #737373;
}

</style>


<!--main content start-->
<section id="adminsection" class="container">
  <section class="wrapper">
    <!-- page start-->
    <div class="row">
      <div class="col-md-12">
        <h4 class="form-heading col-sm-5"><strong>Candidate Profile</strong></h4>
        <?php echo $this->session->flashdata('successmsg');?>
        <?php echo $this->session->flashdata('errormsg');?>
        <?php
        $path=base_url().'uploads/profilePic/'.$userProfilePic->user_profilePic;
        
        ?>
        <img src="<?php echo $path; ?>" style="width:200px;height:200px;" class="img-thumbnail">
        <!--  <p>Create and add a new office. <a href="#" class="pull-right">Help <i class="fa fa-question-circle"></i></a></p> -->
        <br><br>
        <!--Section for Education details-->
        <section class="panel">
          <div class="panel-body">
            <div class="row">

              <div class="col-md-12">
                <h4>Education Information <!-- <a href="#" class="pull-right" onclick="removeReadOnly();"> <i class="fa fa-pencil-square-o"></i></a> --></h4>
                <hr>
              </div>

              <div class="col-md-12">
                <!--collapse start-->

                <div class="panel-group m-bot20" id="accordionedu">
                  <div class="panel">
                    <div class="panel-heading heading-invert">
                      <h4 class="panel-title">
                        <div class="row">
                          <div class="col-md-3">
                            <strong class="common">Institute Name</strong>
                          </div>
                          <div class="col-md-2">
                            <strong class="common">Duration</strong>
                          </div>
                          <div class="col-md-3">
                            <strong class="common">Course of Study</strong>
                          </div>
                          <div class="col-md-3">
                            <strong class="common">Qualification type</strong>
                          </div>
                          <div class="col-md-1">
                            <strong class="common">Grade</strong>
                          </div>
                        </div>
                      </h4>
                    </div>
                  </div>

                  <?php if($userEducationDetails){ 
                    error_reporting(0);
                    foreach ($userEducationDetails as  $value) {


                      ?>




                      <div class="panel">
                        <div class="panel-heading">
                          <h4 class="panel-title">
                            <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordionedu" href="<?php echo '#'.$value->id; ?>">
                              <div class="row">
                                <div class="col-md-3">
                                  <?php echo $value->institute_name; ?>
                                </div>
                                <div class="col-md-2">
                                  <?php if($value->to_year==0){ echo "studying";}else{    echo $value->from_year."-".$value->to_year; } ?>
                                </div>
                                <div class="col-md-3">
                                  <?php echo $value->course; ?>
                                </div>
                                <div class="col-md-3">
                                  <?php echo $value->qualification; ?>
                                </div>
                                <div class="col-md-1">
                                  <?php echo $value->grade; ?>
                                </div>
                              </div>
                            </a>
                          </h4>
                        </div>



                        <div id="<?php echo $value->id; ?>" class="panel-collapse collapse">
                          <div class="panel-body">
                            <div class="weather-category twt-category">
                              <ul>
                                <li>
                                  Course of Study:
                                  <h5><?php echo $value->course; ?></h5>
                                </li>
                                <li>
                                  Qualification type:
                                  <h5><?php echo $value->qualification; ?></h5>
                                </li>
                                <li>
                                  Grade
                                  <h5><?php echo $value->grade; ?></h5>
                                </li>
                                <li>
                                  Date From:
                                  <h5><?php echo $value->fromMonth1." , ".$value->from_year;?></h5>
                                </li>
                                <li>
                                  Date To:
                                  <h5><?php if($value->to_year==0){echo "studying";} else{ echo $value->toMonth1." , ".$value->to_year; }?></h5>
                                </li>
                                <li>
                                  Predicted/Achieved:
                                  <h5><?php echo $value->achvd; ?></h5>
                                </li>
                              </ul>
                            </div>
                            <span class="divider"></span>
                            <footer class="twt-footer">
                                                           
                                                            <span class="pull-right">
                                                              <a  data ="<?php echo $value->id; ?>" data-toggle="modal" data-target="#myModal" href="javascript:void(0);" ng-click="editEducation(<?php echo $value->id; ?>);"><i class="fa fa-pencil"></i>Edit</a>&nbsp;

                                                              <a data="<?php echo $value->id; ?>" data-toggle="modal" data-target="" href="javascript:void(0);" ng-click="deleteEducation(<?php echo $value->id; ?>);"><i class="fa fa-trash-o" aria-hidden="true"> Delete</i></a>

                                                            </span>
                                                          </footer>
                                                        </div>
                                                      </div>
                                                    </div>
                                                    <?php    } } ?>

                                                  </div>
                                                  <!--collapse end-->
                                                </div>

                                                
                                         </div>
                                       </div>
                                     </section>


                                     <!--Section for Personal details-->
                                     <section class="panel">

                                       <div class="panel-body">

                                         <div class="row">
                                           <div class="col-md-12">
                                            <h4>Personal Information <!-- <a href="#" class="pull-right" onclick="removeReadOnly();"> <i class="fa fa-pencil-square-o"></i></a> --></h4>
                                            <hr>
                                          </div>
                                          <form class="form-horizontal bucket-form" name="userPersonal" ng-submit="submitPersonalForm()" novalidate >
                                            <div class="form-group">
                                              <label class="col-sm-3 control-label">Gender<span class="req">*</span></label>
                                              <div class="col-sm-6 icheck">
                                                <div class="square single-row">
                                                  <div class="">
                                                    <!-- <input tabindex="3" type="radio"  ng-model="user.gender" name="gender"  ng-required="!gender"> -->
                                                    <input name="gender" type="radio" ng-model="personal.gender" value="male" <?php echo  set_radio('gender', 'male', TRUE); ?> required>Male<br/>
                                                    <!--  <label>Male </label> -->


                                                    <!-- <span class="help-block has-error ng-hide" ng-show="genderError">{{genderError}}</span> --> 
                                                  </div>
                                                  <div class="">
                                                    <!-- <input tabindex="3" type="radio"  ng-model="user.gender" name="gender" required="!gender"> -->
                                                    <input name="gender" type="radio" ng-model="personal.gender" value="female" <?php echo  set_radio('gender', 'female'); ?> required>Female<br/>
                                                    <!-- <label>Female </label> -->

                                                  </div>
                                                  <span ng-show="submitted && userPersonal.gender.$invalid" class="help-block has-error ng-hide warnig">Please Select Your Gender</span>
                                                  <span class="help-block has-error ng-hide warnig" style="color: red;" ng-show="genderError">{{genderError}}</span>
                                                </div>
                                              </div>
                                            </div>



                                            <div class="form-group">
                                              <label class="col-sm-3 control-label">Relationship Status<span class="req">*</span></label>
                                              <div class="col-sm-6">
                                                <!-- <input type="text"  class="form-control" placeholder="Enter your nationality"> -->
                                                <select class="form-control m-bot15" ng-model="personal.rstatus" name="rstatus" required>
                                                  <option disabled="disabled" value="" selected="selected">Select your status</option>
                                                  <?php  if($relationshipStatus ){

                                                   foreach ($relationshipStatus as $key) { ?>
                                                   <option value="<?php echo $key->id; ?>" ><?php echo $key->rel_status; ?></option>

                                                   <?php  }  }?>


                                                 </select>
                                                 <span ng-show="submitted && userPersonal.rstatus.$invalid" class="help-block has-error ng-hide warnig">Please Enter Your Relationship Status (E.g. Single/Married) </span> 
                                                 <span class="help-block has-error ng-hide" ng-show="rstatusError">{{rstatusError}}</span>
                                               </div>
                                               <div class="col-md-3">
                                                 <!--  <span class="help-block">Display additional instructions here.</span> -->
                                               </div>
                                             </div>

                                             <div class="form-group" >
                                              <label class="control-label col-md-3">Date Of Birth<span class="req">*</span></label>
                                              <div class="col-md-6">
                                               <div class="row">



                                                 <div class="col-md-4">
                                                   <select class="form-control m-bot15" ng-model="personal.day" name="day" id="day" value="" required>
                                                    <option value="">Day</option>
                                                    <?php foreach ($days as $key ) { ?>

                                                    <option value="<?php echo $key->day; ?>" ><?php echo $key->day; ?></option>
                                                    <?php    } ?>

                                                          <!--  <option value="">Day</option>
                                                          <option value="29">29</option> -->
                                                        </select>

                                                        <span ng-show="submitted && userPersonal.day.$invalid" class="help-block has-error ng-hide warnig">Please select day</span>
                                                        <span class="help-block has-error ng-hide warnig" style="color: red;" ng-show="dayError">{{dayError}}</span>                          
                                                      </div>

                                                      <div class="col-md-4">
                                                       <select class="form-control m-bot15" ng-model="personal.month"  id="month" name="month" onChange="call();" required>

                                                        <option value="">Month</option>
                                                        <option value="1">Jan</option>
                                                        <option value="2">Feb</option>
                                                        <option value="3">Mar</option>
                                                        <option value="4">Apr</option>
                                                        <option value="5">May</option>
                                                        <option value="6">Jun</option>
                                                        <option value="7">Jul</option>
                                                        <option value="8">Aug</option>
                                                        <option value="9">Sep</option>
                                                        <option value="10">Oct</option>
                                                        <option value="11">Nov</option>
                                                        <option value="12">Dec</option>
                                                      </select>

                                                      <span ng-show="submitted && userPersonal.month.$invalid" class="help-block has-error ng-hide warnig">Please select month</span>
                                                      <span class="help-block has-error ng-hide warnig" style="color: red;" ng-show="monthError">{{monthError}}</span>
                                                    </div>

                                                    <div class="col-md-4">
                                                      <select class="form-control m-bot15" ng-model="personal.year" name="year" id="year" value="" onchange="call();" required>

                                                        <option value="">Year</option>
                                                        <?php foreach ($years as $value) { ?>
                                                        <option value="<?php echo $value->year ?>"><?php echo $value->year ?></option>
                                                        <?php  } ?>

                                                      </select>
                                                      <span ng-show="submitted && userPersonal.year.$invalid" class="help-block has-error ng-hide warnig">Please select year</span>
                                                      <span class="help-block has-error ng-hide warnig" style="color: red;" ng-show="yearError">{{yearError}}</span>

                                                    </div>
                                                    <span id="errMsg" class="warnig"></span>

                                                  </div>
                                                </div>

                                              </div>


                                              <div class="form-group">
                                                <label class="col-sm-3 control-label">Nationality<span class="req">*</span></label>
                                                <div class="col-sm-6">
                                                  <!-- <input type="text"  class="form-control" placeholder="Enter your nationality"> -->
                                                  <select class="form-control m-bot15" ng-model="personal.nationality" name="nationality" required>
                                                    <option disabled="disabled" value="" selected="selected">Select your nationality</option>
                                                    <?php  if($allCountries ){

                                                     foreach ($allCountries as $country) { ?>
                                                     <option value="<?php echo $country->country_code; ?>"><?php echo $country->country_name; ?></option>

                                                     <?php  }  }?>

                                                     
                                                   </select>
                                                   <span ng-show="submitted && userPersonal.nationality.$invalid" class="help-block has-error ng-hide warnig">Please Select Your Country </span> 
                                                   <span class="help-block has-error ng-hide"  ng-show="nationalitydateError">{{nationalitydateError}}</span>
                                                 </div>
                                                 <div class="col-md-3">
                                                  <span class="help-block">Display additional instructions here.</span>
                                                </div>
                                              </div>



                                              <div class="form-group">
                                                <label class="col-sm-3 control-label">Ethnic Origin<span class="req">*</span></label>
                                                <div class="col-sm-6">
                                                  <!-- <input type="text"  class="form-control" placeholder="Enter your ethnic origin"> -->
                                                  <select class="form-control m-bot15"  ng-model="personal.ethnicorigin" name="ethnicorigin" value=""  required >
                                                    <option disabled="disabled" value="" selected="selected">Select your ethnic origin</option>
                                                    <?php   if($ethnic_origin ){

                                                      foreach($ethnic_origin as $origin){ ?>

                                                      <option value="<?php echo $origin->id; ?>"><?php echo $origin->ethnic_origin; ?></option>

                                                      <?php    } }?>


                                                    </select>
                                                    <span ng-show="submitted && userPersonal.ethnicorigin.$invalid" class="help-block has-error ng-hide warnig">Please Select Your Ethnic Origin </span> 
                                                    <span class="help-block has-error ng-hide"  ng-show="ethnicorigindateError">{{ethnicorigindateError}}</span>
                                                  </div>
                                                </div>
                                                <div class="form-group">
                                                  <label class="col-sm-3 control-label">Disabilities<span class="req">*</span></label>
                                                  <div class="col-sm-6 icheck">
                                                    <div class="square single-row">
                                                      <div class="">
                                                        <input tabindex="3" type="radio" name="disability" ng-model="personal.disability" value="Yes"  ng-click="showme=true" <?php echo  set_radio('disability', 'Yes', TRUE); ?> required>
                                                        <label>Yes </label>
                                                      </div>
                                                      <div class="">
                                                        <input tabindex="3" type="radio" name="disability"  ng-model="personal.disability" value="no"  ng-click="showme=false" <?php echo  set_radio('disability', 'no'); ?> required>
                                                        <label>No </label>

                                                      </div>
                                                      <span ng-show="submitted && userPersonal.disability.$invalid" class="help-block has-error ng-hide warnig">Please Select Your Disabilities Status</span>
                                                      <span class="help-block has-error ng-hide" ng-show="disabilityError">{{disabilityError}}</span>
                                                    </div>
                                                  </div>
                                                </div>

                                                <?php if($userProfileData->user_disabilities_type!=''){  ?>

                                                <?php   }?>

                                                <div class="form-group" ng-show="showme==1">
                                                  <label class="col-sm-3 control-label">If yes, please provide info</label>
                                                  <div class="col-sm-6">
                                                    <textarea class="form-control" rows="3" ng-model="personal.disabilityDesc" name="disabilityDesc" require ng-pattern="/.*[a-zA-Z]+.*/" ng-required="showme==1" ></textarea>


                                                    <span ng-show="submitted && userPersonal.disabilityDesc.$invalid" class="help-block has-error ng-hide warnig">Please Describe Disabilities </span>
                                                    <span class="help-block has-error ng-hide" ng-show="disabilityDescError">{{disabilityDescError}}</span>
                                                  </div>
                                                  <div class="col-md-3">
                                                    <span class="help-block">Display additional instructions here.</span>
                                                  </div>
                                                </div>


                                                <div class="form-group">
                                                  <label class="col-sm-3 control-label">Desired Job Type<span class="req">*</span></label>
                                                  <div class="col-sm-6 icheck">
                                                    <div class="square single-row">
                                                      <div class="">
                                                        <input tabindex="3" type="radio"  name="desiredJobtype" ng-model="personal.desiredJobtype" value="Full Time"  <?php echo  set_radio('desiredJobtype', 'Full Time', TRUE); ?> required>
                                                        <label>Full time </label>
                                                      </div>
                                                      <div class="">
                                                        <input tabindex="3" type="radio"  name="desiredJobtype" ng-model="personal.desiredJobtype" value="Part Time" <?php echo  set_radio('desiredJobtype', 'Part Time'); ?>  required>
                                                        <label>Part Time </label>

                                                      </div>
                                                      <span ng-show="submitted && userPersonal.permittedtowork.$invalid" class="help-block has-error ng-hide warnig">Please Select Permitted to work in UK and Ireland</span>
                                                      <span class="help-block has-error ng-hide" ng-show="permittedtoworkError">{{permittedtoworkError}}</span>
                                                    </div>
                                                  </div>
                                                </div>


                                                <div class="form-group">
                                                  <label class="col-sm-3 control-label">Permitted to work in UK and Ireland<span class="req">*</span></label>
                                                  <div class="col-sm-6 icheck">
                                                    <div class="square single-row">
                                                      <div class="">
                                                        <input tabindex="3" type="radio"  name="permittedtowork" ng-model="personal.permittedtowork" value="Yes"  <?php echo  set_radio('permittedtowork', 'yes', TRUE); ?> required>
                                                        <label>Yes </label>
                                                      </div>
                                                      <div class="">
                                                        <input tabindex="3" type="radio"  name="permittedtowork" ng-model="personal.permittedtowork" value="no" <?php echo  set_radio('permittedtowork', 'no'); ?>  required>
                                                        <label>No </label>

                                                      </div>
                                                      <span ng-show="submitted && userPersonal.permittedtowork.$invalid" class="help-block has-error ng-hide warnig">Please Select Permitted to work in UK and Ireland</span>
                                                      <span class="help-block has-error ng-hide" ng-show="permittedtoworkError">{{permittedtoworkError}}</span>
                                                    </div>
                                                  </div>
                                                </div>

                                           <!--  <div class="form-group last">
                                                <label class="control-label col-md-3">Job Type<span class="req">*</span></label>
                                                <div class="col-md-6">
                                                 <!--      <!-- <select  class="multi-select jobType" multiple="" ng-dropdown-multiselect="" ng-model="user.jobtype"  id="my_multi_select3" value="" name="jobtype[]" data-select-job  required=""> -->
                                              <!--  <select  class="multi-select" ng-dropdown-multiselect=""  ng-model="user.jobtype"  id="demo" value="" name="jobtype[]" multiple='multiple'>
                                                      <?php  
//echo "<pre>";print_r();exit;
                                                        
                                                     // foreach ($job_type as  $value) { 
                                                        $selected = '';
                                                        //if(isset($user_jobType) && in_array($value->id, $user_jobType)){
                                                          //$selected = "selected='selected'";
                                                        //}
                                                        ?>

                                                       <option value="<?php //echo $value->id; ?>" <?php //echo $selected; ?>><?php //echo $value->job_type; ?></option>
                                                        
                                                        <?php    //}  ?>
                                                      </select> -->


                                                      <!-- <span ng-show="submitted && userProfile.jobtype.$invalid" class="help-block has-error ng-hide warnig">Please Select jobtype</span>
                                                      <span class="help-block has-error ng-hide warnig" ng-show="jobtypeError">{{jobtypeError}}</span> -->



                                                      <!-- <div class="help-block"><span class="label label-danger">NOTE!</span> Please select minimum of 1 and maximum of 4</div> -->
                                                <!-- /div>
                                                <div class="col-md-3">
                                                    <span class="help-block">Display additional instructions here.</span>
                                                </div>
                                              </div>  -->
                                              <div class="form-group">
                                                <div class="col-md-3">
                                                    <!-- <a href="" type="button" class="btn btn-danger btn-sm"><strong><i class="fa fa-arrow-left"></i> Back</strong></a>
                                                    <span class="help-block small text-muted">(Personal Information)</span> -->
                                                  </div>
                                                  <div class="col-md-9">
                                                    <!-- <button type="submit" class="btn btn-info pull-right btn-sm"  ng-click="submitted = true"><strong>Save &amp; Continue <i class="fa fa-arrow-right"></i></strong></button> -->
                                                    <span class="clearfix"></span>
                                                    <span class="help-block pull-right small text-muted">(Education)</span>
                                                  </div>
                                                </div>
                                              </form>

                                            </div>

                                          </div>

                                        </section>

                                        <!--Section for Employment details-->

                                        <section class="panel">

                                          <div class="panel-body">
                                            <div class="row">
                                             <div class="col-md-12">
                                              <h4>Employment Information <!-- <a href="#" class="pull-right" onclick="removeReadOnly();"> <i class="fa fa-pencil-square-o"></i></a> --></h4>
                                              <hr>
                                            </div>

                                            <div class="col-md-8 col-md-offset-2">
                                <!-- <h3 class="form-heading">Pre-Employment Details</h3>
                                <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit.</p> -->
                                <!--collapse start-->
                                <div class="panel-group m-bot20" id="accordionedu">
                                  <div class="panel">
                                    <div class="panel-heading heading-invert">
                                      <h4 class="panel-title">
                                        <div class="row">
                                          <div class="col-md-4">
                                            <strong class="common">Employer name</strong>
                                          </div>
                                          <div class="col-md-3">
                                            <strong class="common">Designation</strong>
                                          </div>
                                          <div class="col-md-3">
                                            <strong class="common">Duration</strong>
                                          </div>
                                                   <!--  <div class="col-md-3">
                                                        <strong>Employer Reference</strong>
                                                      </div> -->
                                                      <div class="col-md-1">
                                                        <strong class="common">Currently Employed </strong>
                                                      </div>
                                                    </div>
                                                  </h4>
                                                </div>
                                              </div>
                                              <?php error_reporting(0);
                                              foreach ($PreEmploymentInfo as $key ) {


                                               ?>
                                               <div class="panel">
                                                <div class="panel-heading">
                                                  <h4 class="panel-title">
                                                    <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordionedu" href="<?php echo '#'.$key->id;?>">
                                                      <div class="row">
                                                        <div class="col-md-4">
                                                          <?php echo $key->employer_name; ?>
                                                        </div>
                                                        <div class="col-md-3">
                                                          <?php echo $key->designation; ?>
                                                        </div>
                                                        <div class="col-md-3">
                                                          <?php echo $key->fromMonth.' '.$key->from_year.' - '.$key->toMonth.' '.$key->to_year; ?>
                                                        </div>
                                                       <!--  <div class="col-md-3">
                                                            <?php echo $key->employer_reference; ?>
                                                          </div> -->
                                                          <div class="col-md-1">
                                                           <?php echo $key->current_emplymnt_status; ?>
                                                         </div>
                                                       </div>
                                                     </a>
                                                   </h4>
                                                 </div>
                                                 <div id="<?php echo $key->id; ?>" class="panel-collapse collapse">
                                                  <div class="panel-body">
                                                    <div class="weather-category twt-category">
                                                      <ul>
                                                        <li>
                                                          Currently applying:
                                                          <h5><?php echo $key->undertaken_voluntary_charity_work; ?></h5>
                                                        </li>
                                                        <li>
                                                         Charity work:
                                                         <h5><?php echo $key->interested_in_volunteering_assisting_charities_status; ?></h5>
                                                       </li>
                                                       <li>
                                                         Job Profile
                                                         <!-- <h5><?php echo $key->job_profile; ?></h5> -->
                                                         <!--  <textarea class="form-control" readonly="true" rows="2"><?php echo $key->job_profile; ?></textarea> -->
                                                         <div class="comment more"><?php echo $key->job_profile; ?></div>
                                                       </li>
                                                       <li>
                                                        Present Income:   Frequency
                                                        <h5><?php echo $key->present_income.'    '.$key->prsnt_income_freq;; ?></h5>
                                                      </li>
                                                      <li>
                                                        Desired Income:   Frequency 
                                                        <h5><?php echo $key->desired_income.'    '.$key->de_income_freq; ?></h5>
                                                      </li>

                                                    </ul>
                                                  </div>
                                                  <span class="divider"></span>
                                                  <footer class="twt-footer">
                                                        <!-- <strong>Add Media: </strong>
                                                        <a href="#"><span class="label label-default"><i class="fa fa-file-text-o"></i>Document</span></a>
                                                        <a href="#"><span class="label label-default"><i class="fa fa-picture-o"></i>Photo</span></a>
                                                        <a href="#"><span class="label label-default"><i class="fa fa-link"></i>Link</span></a> -->
                                                        <span class="pull-right">

                                                         <a  data ="<?php echo $key->id; ?>" data-toggle="modal" data-target="#myModal" href="javascript:void(0);" ng-click="editPreEmployment(<?php echo $key->id; ?>);"><i class="fa fa-pencil"></i>Edit</a>&nbsp;


                                                         <a data="<?php echo $key->id; ?>" data-toggle="modal" data-target="" href="javascript:void(0);" ng-click="delPreEmployment(<?php echo $key->id; ?>);"><i class="fa fa-trash-o" aria-hidden="true"> Delete</i></a>
                                                       </span>
                                                     </footer>
                                                   </div>
                                                 </div>
                                               </div>
                                               <?php } ?>

                                             </div>
                                           </div>

                                           <form class="form-horizontal bucket-form" name="userPreEmployment" ng-submit="submitPreEmploymentEmpForm()" novalidate >



                                            <div class="form-group">
                                              <label class="col-sm-5 control-label">Do you have any previous work experience?<span class="req">*</span></label>
                                              <div class="col-sm-6 icheck">
                                                <div class="square single-row">
                                                  <div class="">

                                                   <input name="wrkexp" type="radio" ng-model="userEmployment.wrkexp" value="Yes" ng-click="showme=true" <?php echo  set_radio('recieptOf', 'Yes', TRUE); ?>  required>Yes<br/>

                                                 </div>
                                                 <div class="">

                                                  <input name="wrkexp" type="radio" ng-model="userEmployment.wrkexp" value="No" ng-click="showme=false" <?php echo  set_radio('recieptOf', 'No'); ?> ng-checked="true" required>No<br/>


                                                </div>
                                                <span ng-show="submitted && userPreEmployment.wrkexp.$invalid" class="help-block has-error ng-hide warnig">Please select your status</span>
                                                <span class="help-block has-error ng-hide warnig" style="color: red;" ng-show="wrkexpError">{{wrkexpError}}</span>
                                              </div>
                                            </div>
                                          </div>

                                          <div  ng-show="showme==1">                      

                                            <div class="form-group">
                                              <label class="col-sm-3 control-label">Employer name<span class="req">*</span></label>
                                              <div class="col-sm-6">
                                                <input type="text"  ng-model="userEmployment.emplrNme" name="emplrNme" class="form-control" placeholder="Enter name of institution" require ng-pattern="/.*[a-zA-Z]+.*/" ng-required="showme==1">

                                                <span ng-show="submitted && userPreEmployment.emplrNme.$invalid" class="help-block has-error ng-hide warnig">Please enter name of employer</span>
                                                <span class="help-block has-error ng-hide warnig" style="color: red;" ng-show="emplrNmeError">{{emplrNmeError}}</span>
                                              </div>
                                            </div>

                                            <div class="form-group">
                                              <label class="control-label col-md-3">Date from<span class="req">*</span></label>
                                              <div class="col-sm-2 padingr">
                                                <select class="form-control" ng-model="userEmployment.fromMonth" name="fromMonth" id="fromMonth" onchange="return checkFromMonth();" ng-required="showme==1">
                                                  <option value="" selected="selected">Month</option>
                                                  <?php if($months)
                                                  {
                                                   foreach ($months as $value) { ?>

                                                   <option value="<?php echo $value->id; ?>" ><?php echo $value->month_name; ?></option>

                                                   <?php    } } ?>
                                                 </select>
                                                 <span ng-show="submitted && userPreEmployment.fromMonth.$invalid" class="help-block has-error ng-hide warnig">Please select month</span>
                                                 <span class="help-block has-error ng-hide warnig" style="color: red;" ng-show="fromMonthError">{{fromMonthError}}</span>
                                               </div>
                                               <div class="col-sm-2 padingr">



                                                <select class="form-control" ng-model="userEmployment.fromYear" name="fromYear" value="" id="fromYear" onchange="return checkFromDate();"  ng-required="showme==1">
                                                  <option value="" selected="selected">Year</option>
                                                  <?php if($years)
                                                  {
                                                   foreach ($years as $value) { ?>

                                                   <option value="<?php echo $value->year; ?>"  ><?php echo $value->year; ?></option>

                                                   <?php    } } ?>


                                                 </select>
                                                 <span ng-show="submitted && userPreEmployment.fromYear.$invalid" class="help-block has-error ng-hide warnig">Please select year</span>
                                                 <span class="help-block has-error ng-hide warnig" style="color: red;" ng-show="fromYearError">{{fromYearError}}</span>

                                               </div>
                                             </div>


                                             <div class="form-group">
                                              <label class="control-label col-md-3">Date to<span class="req">*</span></label>
                                              <div class="col-sm-2 padingr">

                                               <select class="form-control" ng-model="userEmployment.toMonth" name="toMonth" id="toMonth" onchange="return checkToMonth();" ng-required="showme==1">
                                                <option value="" selected="selected">Month</option>
                                                <?php if($months)
                                                {
                                                 foreach ($months as $value) { ?>

                                                 <option value="<?php echo $value->id; ?>" ><?php echo $value->month_name; ?></option>

                                                 <?php    } } ?>
                                               </select>

                                               <span ng-show="submitted && userPreEmployment.toMonth.$invalid" class="help-block has-error ng-hide warnig">Please select month</span>
                                               <span class="help-block has-error ng-hide warnig" style="color: red;" ng-show="toMonthError">{{toMonthError}}</span>
                                             </div>
                                             <div class="col-sm-2 padingr">



                                               <select class="form-control" ng-model="userEmployment.toYear" name="toYear" onchange="return checkToDate();" id="toYear" ng-required="showme==1">
                                                <option value="" selected="selected">Year</option>
                                                <?php if($years)
                                                {
                                                 foreach ($years as $value) { ?>

                                                 <option value="<?php echo $value->year; ?>" ><?php echo $value->year; ?></option>

                                                 <?php    } } ?>
                                               </select>
                                               <span ng-show="submitted && userPreEmployment.toYear.$invalid" class="help-block has-error ng-hide warnig">Please select year</span>
                                               <span class="help-block has-error ng-hide warnig" style="color: red;" ng-show="toYearError">{{toYearError}}</span>
                                             </div>
                                             <div class="col-md-3 padingr">
                                              <span class="help-block">Note:- To year select first</span>
                                            </div>
                                          </div>

                                          <div class="form-group">
                                            <label class="col-sm-3 control-label">Job type<span class="req">*</span></label>
                                            <div class="col-sm-6">
                                              <input type="text"  ng-model="userEmployment.designation" name="designation" class="form-control" placeholder="Enter job type" require ng-pattern="/.*[a-zA-Z]+.*/" ng-required="showme==1">

                                              <span ng-show="submitted && userPreEmployment.designation.$invalid" class="help-block has-error ng-hide warnig">Please enter job type</span>
                                              <span class="help-block has-error ng-hide warnig" style="color: red;" ng-show="designationError">{{designationError}}</span>
                                            </div>
                                          </div>



                                          <div class="form-group">
                                            <label class="col-sm-3 control-label">Job profile<span class="req">*</span></label>
                                            <div class="col-sm-6">
                                              <textarea class="form-control" rows="3" ng-model="userEmployment.jobprofile" name="jobprofile" require ng-pattern="/.*[a-zA-Z]+.*/" ng-required="showme==1" style="resize:none" ></textarea>


                                              <span ng-show="submitted && userPreEmployment.jobprofile.$invalid" class="help-block has-error ng-hide warnig">Please describe job profile </span>
                                              <span class="help-block has-error ng-hide" ng-show="jobprofileError">{{jobprofileError}}</span>
                                            </div>
                                          </div>

                                          <div class="form-group">
                                            <label class="col-sm-3 control-label">Internal training<span class="req">*</span></label>
                                            <div class="col-sm-6">
                                              <input type="text"  ng-model="userEmployment.intTraining" name="intTraining" class="form-control" placeholder="Enter internal training" require ng-pattern="/.*[a-zA-Z]+.*/" ng-required="showme==1">

                                              <span ng-show="submitted && userPreEmployment.intTraining.$invalid" class="help-block has-error ng-hide warnig">Please enter internal training</span>
                                              <span class="help-block has-error ng-hide warnig" style="color: red;" ng-show="intTrainingError">{{intTrainingError}}</span>
                                            </div>
                                          </div>

                                          <div class="form-group">
                                            <label class="col-sm-3 control-label">Currently Employed<span class="req">*</span></label>
                                            <div class="col-sm-6 icheck">
                                              <div class="square single-row">
                                                <div class="">
                                                  <input tabindex="3" type="radio" ng-model="userEmployment.cEmp" value="yes"  name="cEmp" ng-required="showme==1">
                                                  <input type="hidden" name="id" ng-model="userEmployment.id">
                                                  <label>Yes </label>
                                                </div>
                                                <div class="">
                                                  <input tabindex="3" type="radio" ng-model="userEmployment.cEmp" value="no" name="cEmp" ng-required="showme==1">
                                                  <label>No </label>
                                                </div>

                                              </div>
                                              <span ng-show="submitted && userPreEmployment.cEmp.$invalid" class="help-block has-error ng-hide warnig">Please select your status</span>
                                              <span class="help-block has-error ng-hide warnig" style="color: red;" ng-show="cEmpError">{{cEmpError}}</span>
                                            </div>

                                          </div>

                                          <div class="form-group">
                                            <label class="col-sm-3 control-label">Present Income<span class="req">*</span></label>
                                            <div class="col-sm-3">
                                              <div class="input-group m-bot15">
                                                <span class="input-group-addon"><i class="fa fa-gbp"></i></span>
                                                <input type="text" class="form-control"  value="" ng-model="userEmployment.salary" name="salary" placeholder="00.00 "  require ng-pattern="/^\d{0,9}(\.\d{1,9})?$/" ng-required="showme==1">


                                              </div>
                                              <span ng-show="submitted && userPreEmployment.salary.$invalid" class="help-block has-error ng-hide warnig">Please enter your income</span>
                                              <span class="help-block has-error ng-hide warnig" style="color: red;" ng-show="salaryError">{{salaryError}}</span>
                                            </div>

                                            <div class="col-sm-3">
                                              <div class="input-group">
                                               <!--  <span class="input-group-addon"><i class="fa fa-gbp"></i></span> -->

                                               <select class="form-control" ng-model="userEmployment.pfrequency" name="pfrequency" value="" id="fromYear" onchange="return checkFromDate();"  ng-required="showme==1">
                                                <option value="" selected="selected">Frequency</option>
                                                <?php if($incomefrequency)
                                                {
                                                 foreach ($incomefrequency as $value) { ?>

                                                 <option value="<?php echo $value->income_frequency; ?>"  ><?php echo $value->income_frequency; ?></option>

                                                 <?php    } } ?>


                                               </select>

                                             </div>
                                             <span ng-show="submitted && userPreEmployment.pfrequency.$invalid" class="help-block has-error ng-hide warnig">Please select frequency</span>
                                             <span class="help-block has-error ng-hide warnig" style="color: red;" ng-show="pfrequencyError">{{pfrequencyError}}</span>
                                           </div>

                                         </div>
                                         <div class="form-group">
                                          <label class="col-sm-3 control-label">Desired Income<span class="req">*</span></label>
                                          <div class="col-sm-3">
                                            <div class="input-group">
                                              <span class="input-group-addon"><i class="fa fa-gbp"></i></span>
                                              <input type="text" class="form-control" value="" ng-model="userEmployment.dIncome" name="dIncome" placeholder="00.00 " require ng-pattern="/^\d{0,9}(\.\d{1,9})?$/" ng-required="showme==1">


                                            </div>
                                            <span ng-show="submitted && userPreEmployment.dIncome.$invalid" class="help-block has-error ng-hide warnig">Please enter your desired income</span>
                                            <span class="help-block has-error ng-hide warnig" style="color: red;" ng-show="dIncomeError">{{dIncomeError}}</span>
                                          </div>

                                          <div class="col-sm-3">
                                            <div class="input-group m-bot15">
                                             <!--  <span class="input-group-addon"><i class="fa fa-gbp"></i></span> -->

                                             <select class="form-control" ng-model="userEmployment.dfrequency" name="dfrequency" value="" id=""  ng-required="showme==1">
                                              <option value="" selected="selected">Frequency</option>
                                              <?php if($incomefrequency)
                                              {
                                               foreach ($incomefrequency as $value) { ?>

                                               <option value="<?php echo $value->income_frequency; ?>"  ><?php echo $value->income_frequency; ?></option>

                                               <?php    } } ?>


                                             </select>

                                           </div>
                                           <span ng-show="submitted && userPreEmployment.dfrequency.$invalid" class="help-block has-error ng-hide warnig">Please select frequency</span>
                                           <span class="help-block has-error ng-hide warnig" style="color: red;" ng-show="dfrequencyError">{{dfrequencyError}}</span>
                                         </div>

                                       </div>



                                       <div class="form-group">
                                        <label class="col-sm-3 control-label">National Insurance Number<span class="req">*</span></label>
                                        <div class="col-sm-6">
                                          <input type="text"  ng-model="userEmployment.insuranceNo" name="insuranceNo" class="form-control" placeholder="Enter national insurance number" require ng-pattern="/.*[a-zA-Z]+.*/" >

                                          <span ng-show="submitted && userPreEmployment.insuranceNo.$invalid" class="help-block has-error ng-hide warnig">Please enter name of employer</span>
                                          <span class="help-block has-error ng-hide warnig" style="color: red;" ng-show="insuranceNoError">{{insuranceNoError}}</span>
                                        </div>
                                      </div>


                                      <hr>

                                      <div class="form-group">
                                        <label class="col-sm-3 control-label">Currently Actively applying<span class="req">*</span></label>
                                        <div class="col-sm-6 icheck">
                                          <div class="square single-row">
                                            <div class="">
                                              <input tabindex="3" type="radio" value="yes" ng-model="userEmployment.crentact" name="crentact" ng-click="show=true" ng-required="showme==1">
                                              <label>Yes </label>
                                            </div>
                                            <div class="">
                                              <input tabindex="3" type="radio" value="no" ng-model="userEmployment.crentact" name="crentact" ng-click="show=false" ng-required="showme==1">
                                              <label>No </label>
                                            </div>

                                          </div>
                                          <span ng-show="submitted && userPreEmployment.crentact.$invalid" class="help-block has-error ng-hide warnig">Please select your status</span>
                                          <span class="help-block has-error ng-hide warnig" style="color: red;" ng-show="crentactError">{{crentactError}}</span>

                                        </div>
                                        <div class="col-md-3">
                                          <span class="help-block">Display additional instructions here.</span>
                                        </div>
                                      </div>


                                      <div class="form-group" id="myfields" ng-show="show==1">
                                        <label class="col-sm-3 control-label">If yes, where?<span class="req">*</span></label>

                                        <div class="col-sm-6">
                                          <div class="input_fields_wraps input-group">
                                            <input type="text" ng-model="userEmployment.mytext" name="mytext" class="input form-control" ng-required="show==1">
                                            <button class="add_field_button btn add-more btn-info" id=""><i class="fa fa-plus"></i> Add</button>


                                          </div>
                                          <span ng-show="submitted && userPreEmployment.mytext.$invalid" class="help-block has-error ng-hide warnig">Please select your status</span>
                                          <span class="help-block has-error ng-hide warnig" style="color: red;" ng-show="mytextError">{{mytextError}}</span>
                                        </div>
                                      </div>




                                      <hr>
                                      <div class="form-group">
                                        <label class="col-sm-3 control-label">Undertaken Voluntary / Charity work<span class="req">*</span></label>
                                        <div class="col-sm-6 icheck">
                                          <div class="square single-row">
                                            <div class="">
                                              <input tabindex="3" type="radio"  ng-model="userEmployment.charitywrk" value="yes"  name="charitywrk" ng-required="showme==1">
                                              <label>Yes </label>
                                            </div>
                                            <div class="">
                                              <input tabindex="3" type="radio" ng-model="userEmployment.charitywrk" value="no"  name="charitywrk" ng-required="showme==1">
                                              <label>No </label>
                                            </div>

                                          </div>

                                          <span ng-show="submitted && userPreEmployment.charitywrk.$invalid" class="help-block has-error ng-hide warnig">Please select your status</span>
                                          <span class="help-block has-error ng-hide warnig" style="color: red;" ng-show="charitywrkError">{{charitywrkError}}</span>

                                        </div>
                                        <div class="col-md-3">
                                          <span class="help-block">Display additional instructions here.</span>
                                        </div>
                                      </div>

                                      <div class="form-group">
                                        <label class="col-sm-3 control-label">Interested in Volunteering / Assisting charities<span class="req">*</span></label>
                                        <div class="col-sm-6 icheck">
                                          <div class="square single-row">
                                            <div class="">
                                              <input tabindex="3" type="radio"  value="yes" ng-model="userEmployment.assCharity" name="assCharity" ng-required="showme==1">
                                              <label>Yes </label>
                                            </div>
                                            <div class="">
                                              <input tabindex="3" type="radio"  value="no"  ng-model="userEmployment.assCharity" name="assCharity" ng-required="showme==1">
                                              <label>No </label>
                                            </div>

                                          </div>
                                          <span ng-show="submitted && userPreEmployment.assCharity.$invalid" class="help-block has-error ng-hide warnig">Please select your status</span>
                                          <span class="help-block has-error ng-hide warnig" style="color: red;" ng-show="assCharityError">{{assCharityError}}</span>

                                        </div>
                                        <div class="col-md-3">
                                          <span class="help-block">Display additional instructions here.</span>
                                        </div>
                                      </div>

                                            <!-- <div class="form-group" >
                                                <label class="col-sm-3 control-label">If yes, what sort of organisations?<span class="req">*</span></label>
                                                <div class="col-sm-6">
                                                    <select multiple   id="e9" style="width:100%;" ng-model="userEmployment.organisations"  name="organisations[]" value="" class="populate m-bot15"   data-select-job ng-required="showme==1">
                                                        <optgroup label="select organisations">

                                                            <?php if($organisation){

                                                              foreach ($organisation as $value) {

                                                                  ?>


                                                                  <option value="<?php echo $value->id; ?>"  ><?php echo $value->organisation; ?></option>
                                                                  <?php } } ?>
                                                              </optgroup>
                                                          </select>

                                                          <span ng-show="submitted && userPreEmployment.organisations.$invalid" class="help-block has-error ng-hide warnig">Please select your status</span>
                                                          <span class="help-block has-error ng-hide warnig" style="color: red;" ng-show="organisationsError">{{organisationsError}}</span>

                                                      </div>
                                                    </div> -->
                                                  </div>
                                                  <!-- <div class="form-group">
                                                    <div class="col-md-9">
                                                        <button class="btn btn-success btn-sm pull-right" ng-click="submitted=true" ><strong>Add <i class="fa fa-plus"></i></strong></button>
                                                    </div>
                                                  </div> -->

                                                  <div class="form-group">

                                                    <div class="col-md-3">
                                                      <!-- <a href="<?php echo base_url(); ?>user/education" type="button" class="btn btn-danger btn-sm"><strong><i class="fa fa-arrow-left"></i> Back</strong></a> -->
                                                      <span class="help-block small text-muted">(Education)</span>
                                                    </div>
                                                    <!-- <div class="col-md-9">

                                                        <a href="<?php echo base_url(); ?>user/recieptBenefits"> <button type="button"  class="btn btn-info pull-right btn-sm"><strong>Save &amp; Continue <i class="fa fa-arrow-right"></i></strong></button></a>

                                                        <span class="clearfix"></span>
                                                        <span class="help-block pull-right small text-muted">(Benefits)</span>
                                                      </div> -->
                                                      <div class="col-md-9">

                                                       <!-- <button  class="btn btn-info pull-right btn-sm" ng-click="submitted=true"><strong>Save &amp; Continue <i class="fa fa-arrow-right"></i></strong></button> -->

                                                       <span class="clearfix"></span>
                                                       <span class="help-block pull-right small text-muted">(Benefits)</span>
                                                     </div>

                                                   </div>

                                                 </form>

                                               </div>         

                                             </div>


                                           </section>

                                           <section class="panel">
                                             <div class="panel-body">
                                               <div class="row">
                                                <div class="col-md-12">
                                                  <h4>Profiles <!-- <a href="#" class="pull-right"> <i class="fa fa-pencil-square-o"></i></a> --></h4>
                                                  <hr>
                                                </div>
                                                <div class="col-md-10 col-md-offset-1">
                                                 <div class="freelancer-wrap resumeblock row-fluid clearfix">
                                                  <?php if($this->session->flashdata('succmsg')){  ?>
                                                  <div class="alert alert-success">
                                                   <strong>Success!</strong> <?php echo $this->session->flashdata('succmsg'); ?>
                                                   <!-- <button type="button" class="close" data-dismiss="modal">&times;</button> -->
                                                   <a type="button" class="close" data-dismiss="alert" aria-hidden="true">×</a>
                                                 </div>
                                                 <?php    } ?>
                                                 <ul class="nav nav-tabs">
                                                   <?php foreach ($resumesInfo as $key => $value) { //echo $key;  ?>
                                                   <li class="<?php if($key == 1){echo 'active'; } ?>"><a data-toggle="tab" href="#r<?php echo $key; ?>"><?php echo $value[0]->profilename; ?></a></li>
                                                   <?php }//exit;?>
                                                 </ul>
                                                 <div class="tab-content">
                                                   <?php foreach ($resumesInfo as $key =>$val ) {  ?>
                                                   <div id="r<?php echo $key; ?>" class="tab-pane fade in <?php if($key == 1){echo 'active'; } ?>">
                                                    <div class="freelancer-wrap resumeblock row-fluid clearfix">
                                                     <h4>Resumes 
                                                       <a data="target-blank" href="<?php echo base_url(); ?>user/addProfile" class="pull-right">
                                                        <button type="button" class="btn  btn-primary addNewProfile">Add New Profile</button>
                                                      </a>
                                                    </h4>
                                                    <table class="table table-condensed">
                                                      <thead>
                                                       <tr>
                                                        <th  style="width: 25px;">Resume Title</th>
                                                        <th  style="width: 25px;">Cover Letter</th>
                                                        <th  style="width: 25px;">Resume File</th>
                                                        <th  style="width: 25px;">Action </th>
                                                      </tr>
                                                    </thead>
                                                    <?php    foreach ($val as $v ) { ?>
                                                    <tr>
                                                     <td><?php echo $v->resume_title; ?></td>
                                                     <td><a href="<?php echo base_url().'uploads/coverLetters/'.$v->cover_letter; ?>" target="_blank"> Click here to view</a></td>
                                                     <td><a href="<?php echo base_url().'uploads/userResumes/'.$v->resume; ?>" target="_blank"> Click here to view</a></td>
                                                     <td>
                                                      <ul class="list-unstyled list-inline">
                                                       <li><a data="<?php echo $v->id; ?>" data-toggle="modal" data-target="" href="javascript:void(0);" ng-click="deleteResume(<?php echo $v->id; ?>);"><i class="fa fa-trash-o" aria-hidden="true"> Delete</i></a></li>
                                                     </ul>
                                                   </td>
                                                 </tr>
                                                 <?php  }  ?>
                                               </tbody>  
                                             </table>
                                           </div>
                                         </div>
                                         <?php     }?>
                                       </div>

                                       <?php if(empty($resumesInfo)){ ?>  
                                       <h4>Add New Profile</h4>
                                       <form class="form-inline" name="userResume" ng-submit="submitUserResume()" novalidate>
                                         <br>
                                         <div class="form-group resumetitle">
                                          <input type="text" class="form-control"  name="profileName" ng-model="uResume.profileName" require ng-pattern="/.*[a-zA-Z]+.*/" placeholder="Profile Name" required="">
                                          <span id="uplErr" class="warning" style="color: red;"></span>
                                          <span ng-show="submitted && userResume.profileName.$invalid" class="help-block has-error ng-hide warnig">Please enter profile name</span>
                                          <span class="help-block has-error ng-hide warnig" style="color: red;" ng-show="profileNameError">{{profileNameError}}</span>
                                        </div>
                                        <!-- <textarea class="form-control coverLetter" ng-model="uResume.resumeCover" name="resumeCover" require ng-pattern="/.*[a-zA-Z]+.*/" rows="4" required></textarea> -->
                                        <!-- <h4>Upload New Cover Letter</h4> -->
                                        <strong>
                                          <p>Upload New Cover Letter</p>
                                        </strong>
                                        <div class="form-group">
                                          <input type="file" accept="application/pdf,application/msword,application/vnd.openxmlformats-officedocument.wordprocessingml.document" file-model="uploadFile" nv-file-select="" uploader="uploader2"/>
                                        </div>
                                        <button type="submit"  class="btn btn-primary pull-right btn-sm" style="width:200px;" ng-click="uploader2.uploadAll()"><i class="fa fa-upload"></i> Upload Cover Letter</button>
                                        <span id="succ" style="color: green;" ></span>
                                        <input type="hidden" class="form-control" id="covrLetter" name="covrLetter" ng-model="uResume.covrLetter" required/>
                                        <span id="uplErr" class="warning" style="color: red;"></span>
                                        <span ng-show="submitted && userResume.covrLetter.$invalid" class="help-block has-error ng-hide warnig">Please upload cover letter</span>
                                        <span class="help-block has-error ng-hide warnig" style="color: red;" ng-show="covrLetterError">{{covrLetterError}}</span>
                                        <br></br>
                                        <!-- <h4>Upload New Resume</h4> -->
                                        <strong>
                                          <p>Upload New Resume</p>
                                        </strong>
                                        <p class="text-muted">Recruiters are looking for updated resumes.</p>
                                        <div class="form-group resumetitle">
                                          <input type="text" class="form-control" id="" ng-model="uResume.resumeTitle" name="resumeTitle" require ng-pattern="/.*[a-zA-Z]+.*/" placeholder="Resume Title" required="">
                                          <span id="uplErr" class="warning" style="color: red;"></span>
                                          <span ng-show="submitted && userResume.resumeTitle.$invalid" class="help-block has-error ng-hide warnig">Please resume title</span>
                                          <span class="help-block has-error ng-hide warnig" style="color: red;" ng-show="resumeTitleError">{{resumeTitleError}}</span>
                                        </div>
                                        <div class="form-group">
                                          <input type="file" accept="application/pdf,application/msword,application/vnd.openxmlformats-officedocument.wordprocessingml.document" file-model="euploadFile" nv-file-select="" uploader="euploader2"/>
                                        </div>
                                        <button type="submit"   class="btn btn-primary pull-right btn-sm"  style="width:200px;" ng-click="euploader2.uploadAll()"><i class="fa fa-upload"></i> Upload Resume</button>
                                        <span id="succ2" style="color: green;" ></span>
                                        <input type="hidden" class="form-control" id="resumeUser" name="resumeUser" ng-model="uResume.resumeUser" required/>
                                        <span ng-show="submitted && userResume.resumeUser.$invalid" class="help-block has-error ng-hide warnig">Please upload resume</span>
                                        <span class="help-block has-error ng-hide warnig" style="color: red;" ng-show="resumeTitleError">{{resumeUserError}}</span>

                                        <div class="form-group">
                                         <div class="col-md-3">
                                           <!-- <a href="<?php echo base_url(); ?>user/employment" type="button" class="btn btn-danger btn-sm"><strong><i class="fa fa-arrow-left"></i> Back</strong></a> -->
                                           <span class="help-block small text-muted">(Employment)</span>
                                         </div>
                                         <div class="col-md-9">
                                           <!-- <a href="<?php echo base_url(); ?>user/reference">  <button type="button"  class="btn btn-info pull-right btn-sm"><strong>Save &amp; Continue <i class="fa fa-arrow-right"></i></strong></button></a> -->
                                           <!-- <button type="submit" ng-click="submitted = true" class="btn btn-info pull-right btn-sm"><strong>Save <i class="fa fa-arrow-right"></i></strong></button> -->
                                         </form>
                                         <span class="clearfix"></span>
                                         <span class="help-block pull-right small text-muted">(References)</span>
                                       </div>
                                     </div>
                                     <?php  } else{?>
                                   </div>         
                                   <div class="form-group">
                                     <div class="col-md-3">
                                      <!--  <a href="<?php echo base_url(); ?>user/employment" type="button" class="btn btn-danger btn-sm"><strong><i class="fa fa-arrow-left"></i>Back</strong></a> -->
                                       <span class="help-block small text-muted">(Employment)</span>
                                     </div>
                                     <div class="col-md-9">
                                       <!-- <a href="<?php echo base_url(); ?>user/reference">  <button type="button"  class="btn btn-info pull-right btn-sm"><strong>Save &amp; Continue <i class="fa fa-arrow-right"></i></strong></button></a> -->
                                       <a href="<?php echo base_url(); ?>user/reference"><button type="button"  class="btn btn-info pull-right btn-sm"><strong>Next <i class="fa fa-arrow-right"></i></strong></button></a>

                                       <span class="clearfix"></span>
                                       <span class="help-block pull-right small text-muted">(References)</span>
                                     </div>
                                   </div>

                                   <?php  } ?>
                                 </div>
                               </div>



                             </div>

                           </div>


                         </section>

                         <section class="panel">
                         <div class="panel-body">
                         <div class="row">
                          <div class="col-md-12">
                            <h4>Social Media Information <!-- <a href="#" class="pull-right"> <i class="fa fa-pencil-square-o"></i></a> --></h4>
                            <hr>
                          </div>
                           <div class="col-md-12">
                             <form class="form-horizontal bucket-form" name="userSocial" ng-submit="submitSocialMediaForm()" novalidate>
                                   
                                    <h4>Social Media Account Links</h4>
                                    <div class="form-group">
                                        <label class="control-label col-md-3">Facebook</label>
                                        <div class="col-md-9">
                                            <div class="input-group m-bot15">
                                                <span class="input-group-addon"><i class="fa fa-facebook"></i></span>
                                                <input type="url" class="form-control" ng-model="userMediaInfo.fb" name="fb"  placeholder="Facebook"  value="" >

                                                 <span ng-show="submittedd && userSocial.fb.$invalid" class="help-block has-error ng-hide warnig">Please enter your facebook profile link</span>
             <span class="help-block has-error ng-hide warnig" style="color: red;" ng-show="fbError">{{fbError}}</span>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="control-label col-md-3">Youtube</label>
                                        <div class="col-md-9">
                                            <div class="input-group m-bot15">
                                                <span class="input-group-addon"><i class="fa fa-youtube"></i></span>
                                                <input type="url" class="form-control" ng-model="userMediaInfo.youtube" name="youtube" placeholder="Youtube"  value="" >
                                                <span ng-show="submittedd && userSocial.youtube.$invalid" class="help-block has-error ng-hide warnig">Please enter your youtube profile link</span>
             <span class="help-block has-error ng-hide warnig" style="color: red;" ng-show="youtubeError">{{youtubeError}}</span>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="control-label col-md-3">Twitter</label>
                                        <div class="col-md-9">
                                            <div class="input-group m-bot15">
                                                <span class="input-group-addon"><i class="fa fa-twitter"></i></span>
                                                <input type="url" class="form-control" ng-model="userMediaInfo.twitter" name="twitter" value="" placeholder="Twitter" > 
                                        <span ng-show="submittedd && userSocial.twitter.$invalid" class="help-block has-error ng-hide warnig">Please enter your twitter profile link</span>
             <span class="help-block has-error ng-hide warnig" style="color: red;" ng-show="twitterError">{{twitterError}}</span>

                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="control-label col-md-3">LinkedIn</label>
                                        <div class="col-md-9">
                                            <div class="input-group m-bot15">
                                                <span class="input-group-addon"><i class="fa fa-linkedin"></i></span>
                                                <input type="url" class="form-control" ng-model="userMediaInfo.linkedIn" name="linkedIn" value=""  placeholder="LinkedIn" >
                                                 <span ng-show="submittedd && userSocial.linkedIn.$invalid" class="help-block has-error ng-hide warnig">Please enter your linkedIn profile link</span>
             <span class="help-block has-error ng-hide warnig" style="color: red;" ng-show="linkedInError">{{linkedInError}}</span>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="control-label col-md-3">Instagram</label>
                                        <div class="col-md-9">
                                            <div class="input-group m-bot15">
                                                <span class="input-group-addon"><i class="fa fa-instagram"></i></span>
                                                <input type="url" class="form-control" ng-model="userMediaInfo.instagram" name="instagram" value=""  placeholder="Instagram" >
                                    <span ng-show="submittedd && userSocial.instagram.$invalid" class="help-block has-error ng-hide warnig">Please enter your instagram profile link</span>
             <span class="help-block has-error ng-hide warnig" style="color: red;" ng-show="instagramError">{{instagramError}}</span>


                                            </div>
                                        </div>
                                    </div>

                                    <div class="form-group last">
                                        <label class="control-label col-md-3"><span class="label label-danger">NOTE!</span></label>
                                        <div class="col-md-9">
                                            
                                            <div class="help-block">"please manage your social media profiles to appear professional as employers often search the internet for candidates and may not be entirely happy with what they come across"</div>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <div class="col-md-3">
                                           <!--  <a href="<?php //echo base_url(); ?>user/miscellaneous" type="button" class="btn btn-danger btn-sm"><strong><i class="fa fa-arrow-left"></i> Back</strong></a> -->
                                            <!-- <span class="help-block small text-muted">(Miscellaneous)</span> -->
                                        </div>
                                        <div class="col-md-9">
                                           <!--  <button type="submit" ng-click="submittedd=true" class="btn btn-info pull-right btn-sm"><strong>Save &amp; Finish <i class="fa fa-save"></i></strong></button> -->
                                            <span class="clearfix"></span>
                                            <!-- <span class="help-block pull-right small text-muted">(Social Media)</span> -->
                                        </div>
                                    </div>
                                    
                                </form>
                 
                           </div>

                           </div>
                         </div>
                         </section>


                     
                     <!-- page end-->

                   </section>
                 </section>
                 <!--main content end-->


               </div>


<!-- 
<script type="text/javascript" src="js/ckeditor/ckeditor.js"></script> -->