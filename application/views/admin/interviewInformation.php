
<style type="text/css">
  .freelancer-wrap.jobdesc .btn {
    position: initial;
  }
  .freelancer-wrap.jobdesc .btn::before {
    border: none;
  }
  .thumbnail{

    height:127px;
  }
</style>

<!-- <div class="cd-hero-inner" style="margin-top:-21px;">
  <div class="container">
    <div class="row">
      
      <div class="col-md-12 col-sm-6">
        <div class="text-center" style="color:#fff;"><a href="index.php"></a><span style="font-size:22px;">Event Description</span></div>
      </div>
    </div>
  </div>
</div> -->

<div class="listpgWraper">
  <div class="container">
    
    <!-- Search Result and sidebar start -->
    <div class="row">
      <div class="col-md-10 col-md-offset-1">
        <!-- <h3>Jobs Description</h3> -->
      </div>
      <div class="col-md-12 col-sm-12">
        <!-- Search List -->
        <ul class="searchList list-unstyled">
          <!-- job start -->
          <li>
            <div class="row">
                <a href="<?php echo base_url();?>admin/viewCalender" class="btn btn-info" style="margin-top:-40px;"><i class="fa fa-arrow-left" aria-hidden="true"></i>
Back</a>
              <div class="col-md-10 col-md-offset-1 col-sm-6 col-xs-12">
                    <div class="freelancer-wrap jobdesc row-fluid clearfix">
                       <!--  <div class="col-md-3 text-center">
                            <div class="post-media thumbnail">
                                <?php
                               // $path=base_url().'client_uploads/client_logo/'.$job_description->company_logo;

                                ?>
                                <br/>
                                <img class="img-responsive" src="<?php //echo $path;?>" alt="">
                            </div>
                        </div> --><!-- end col -->

                        <div class="col-md-12">
                          
                            <p style="font-size:22px;"><strong><?php echo $interviewInfo->first_name.' '.$interviewInfo->last_name;?></strong> <span class="pull-right label label-default label-sm"></span></p><br/>
                           <!--  <h4><a href="#"></a><i class="fa fa-briefcase"></i> <?php //echo $eventInfo->clnt_first_name.' '.$eventInfo->clnt_last_name;?></h4><br/> -->
                            
                           <!--  <p class="skillsreq">PHP, MySQL, Linux, Wordpress</p> -->
                            <ul class="list-inline" style="font-size:18px;">
                                
                            </ul>
                           <!--  <p>Lorem ipsum dolor sit amet, non odio tincidunt ut ante, lorem a euismod suspendisse vel...</p> -->
                            <br>
                            <table class="table table-bordered clientinfo">
                              <tbody>

                                <?php
                                  // if($eventInfo->end_date == '1970-01-01 00:00:00'){

                                  //   $end_date='0000-00-00';

                                  // }else{

                                  //   $end_date = date('jS M Y',strtotime($eventInfo->end_date));
                                  // }

                                ?>
                                <tr>
                                  <td class="text-right"><strong>Candidate Name:</strong></td>
                                  <td><?php echo $interviewInfo->first_name.' '.$interviewInfo->last_name;?></td>
                                  <td class="text-right"><strong>Location:</strong></td>
                                  <td><?php echo $interviewInfo->location;?></td>
                                </tr>
                                <tr>
                                  <td class="text-right"><strong>Interview Date:</strong></td>
                                  <td><?php echo date('jS M Y',strtotime($interviewInfo->interview_date));?></td>
                                  <td class="text-right"><strong>Main Contact:</strong></td>
                                  <td><?php echo $interviewInfo->main_contact;?></td>
                                </tr>
                                <tr>
                                  <td class="text-right"><strong>Breakdown of Interview Details:</strong></td>
                                  <td><?php echo $interviewInfo->breakdown_interview;?></td>
                                  <td class="text-right"><strong>Client Name:</strong></td>
                                  <td><?php echo $interviewInfo->clnt_first_name.' '.$interviewInfo->clnt_last_name;?></td>
                                </tr>
                               <!--  <tr>
                                  <td class="text-right"><strong>Event Time:</strong></td>
                                  <td><?php //echo $eventInfo->hours.': '.$eventInfo->minutes.' '.$eventInfo->meridien;?></td>
                                  <td class="text-right"><strong>Event Url:</strong></td>
                                  <td><?php //echo $eventInfo->event_url;?></td>
                                </tr> -->
                                <tr>
                                  <td class="text-right"><strong>Contact Details:</strong></td>
                                  <td><?php echo $interviewInfo->contact_details;?></td>
                                  <td class="text-right"><strong>Site Contact:</strong></td>
                                  <td><?php echo $interviewInfo->site_contact;?></td>
                                </tr>
                              </tbody>
                            </table>
                            <!-- <a href="#" class="btn btn-primary btn-top">APPLY</a> -->
                        </div><!-- end col -->

                        <div class="col-md-12">
                          <hr>
                          <!-- <h4><u><strong>Job Details</strong></u></h4><br/> -->

                          <p><strong>Interview Address:</strong><?php echo $interviewInfo->interview_address;?></p><br>

                         

                        </div>


                         
                         
                       
                    </div><!-- end freelancer-wrap -->
                </div><!-- end col -->
            </div>
          </li>
          <!-- job end -->
        </ul>
        
      </div>
    </div>
  </div>
</div>
