<style type="text/css">
.help-block{

    color:red;
}
</style>

    <div id="resetpassword-page">
        <div class="container">

          <!--for login-->
          <?php
          if($data) {
            //print_r($data);exit;
             date_default_timezone_set("Asia/Kolkata");
	  				//echo $data->updatedtimestamp;
	  				//echo "<br>";
	  				//echo $date2=date("Y-m-d H:i:s");
	  				//echo "<br>";
             $new_date = date("Y-m-d H:i:s", strtotime($data->updatedtimestamp . " +12 hours"));
//echo $new_date;
             if(date("Y-m-d H:i:s")<$new_date && $data->isused!=1) {
              ?>
              <form class="form-signin" name="clientResetPassword" ng-submit="submitForm()" novalidate>	
                  <h2 class="form-signin-heading">Reset your password</h2>
                  <div id="message"></div>
                  <div class="login-wrap">
                      <input type="password" class="form-control" placeholder="New Password" id="npassword" ng-model="reset.npassword" name="npassword" required>		            

                      <span ng-show="submitted && clientResetPassword.npassword.$invalid" class="help-block has-error ng-hide">New Password is required.</span>
                      <span class="help-block has-error ng-hide" ng-show="npasswordError">{{npasswordError}}</span>
                      <br>
                      <input type="password" class="form-control" placeholder="Confirm Password" id="cpassword" ng-model="reset.cpassword" name="cpassword" required>
                      <span ng-show="submitted && clientResetPassword.cpassword.$error.required"  class="help-block has-error ng-hide">Confirm Password is required.</span>
                      <span class="help-block has-error ng-hide" ng-show="cpasswordError">{{cpasswordError}}</span>
                      <br>       
                      <button class="btn btn-lg btn-login btn-block" type="submit" ng-click="submitted = true"> SUBMIT</button>
                      <span class="help-block has-error ng-hide" ng-show="IsMatch">New Password does not match Confirm Password.</span>
                      <input type="hidden" class="form-control" id="emailaddress" ng-model="reset.emailaddress" name="emailaddress">

                  </div>
                  </form><?php
              }
              else {
               ?>
               <form class="form-login" novalidate>	
                  <h2 class="form-login-heading">reset your password</h2>
                  <div class="login-wrap">
                      <div class="login-social-link centered">
                          <p>Link is invalid or has expired.</p>
                      </div>
                  </div>
                  </form><?php
              }
          }?>

      </div>
  </div>

  <!--for login-->
  <script>
                // Defining angularjs application.
                var postApp = angular.module('postApp', []);
                // Controller function and passing $http service and $scope var.
                postApp.controller('postController', function ($scope, $http) {

                // create a blank object to handle form data.
                $scope.user = {};
                $scope.user.emailaddress='<?php echo $data->username;?>';
                        // calling our submit function.
                        $scope.submitForm = function () {
                        	
                        	if ($scope.user.npassword != $scope.user.cpassword) {
                                $scope.IsMatch=true;
                                return false;
                            }else{
  // Posting data to php file
  if ($scope.userResetPassword.$valid) {
                   //alert('login');
                   $http({
                    method: 'POST',
                    dataType: 'json',
                    url: '<?php echo base_url() ?>client/check_reset_password',
                                        data: $scope.user, //forms user object
                                        headers: {'Content-Type': 'application/json'}
                                    }).success(function (data) {
                                        if (data.status == 1) {
                                            $('#message').html('');
                                            $('#message').append(data.message);
                                            $('#npassword').val("");
                                            $('#cpassword').val("");
                                            $scope.user.npassword='';
                                            $scope.user.cpassword='';
                                        } else if (data.status == 0)
                                        {
                                          $('#message').html('');
                                          $('#message').append(data.message);

                                      } else if (data.status == 3)
                                      {
                                          $('#message').html('');
                                          $('#message').append('<div class="alert alert-danger"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>Password are required !.</div>');

                                      }
                                      else {
                        		  //$('#message').html('');
                                  $scope.npasswordError = data.error.npassword.error;
                                  $scope.cpasswordError = data.error.cpassword.error;
                              }
                          });
}
}
//  $scope.IsMatch=false;


};
});

</script>