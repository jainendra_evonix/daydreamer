<style type="text/css">
  .freelancer-wrap.jobdesc .btn {
    position: initial;
  }
  .freelancer-wrap.jobdesc .btn::before {
    border: none;
  }
  .thumbnail{

    height:181px;
  }
</style>

<div class="cd-hero-inner" style="margin-top:-21px;">
  <div class="container">
    <div class="row">
      
      <div class="col-md-12 col-sm-6">
        <div class="text-center" style="color:#fff;"><a href="index.php"></a><span style="font-size:22px;">Job Description</span></div>
      </div>
    </div>
  </div>
</div>

<div class="listpgWraper">
  <div class="container">
    
    <!-- Search Result and sidebar start -->
    <div class="row">
      <div class="col-md-10 col-md-offset-1">
        <!-- <h3>Jobs Description</h3> -->
      </div>
      <div class="col-md-12 col-sm-12">
        <!-- Search List -->
        <ul class="searchList list-unstyled">
          <!-- job start -->
          <li>
            <div class="row">
              <div class="col-md-10 col-md-offset-1 col-sm-6 col-xs-12">
                    <div class="freelancer-wrap jobdesc row-fluid clearfix">
                        <div class="col-md-3 text-center">
                            <div class="post-media thumbnail">
                                <?php
                                $path=base_url().'client_uploads/client_logo/'.$job_description->company_logo;

                                ?>
                                
                                <img class="img-responsive" src="<?php echo $path;?>" alt="">
                            </div>
                        </div><!-- end col -->

                        <div class="col-md-9">
                           <!--  <div class="rating">
                                <i class="fa fa-star"></i>
                                <i class="fa fa-star"></i>
                                <i class="fa fa-star"></i>
                                <i class="fa fa-star"></i>
                                <i class="fa fa-star"></i>
                            </div> -->
                            <p style="font-size:22px;"><strong><?php echo $job_description->job_title;?></strong></p><br/>
                            <h4><a href="#"></a><?php echo $client_name;?></h4><br/>
                            
                           <!--  <p class="skillsreq">PHP, MySQL, Linux, Wordpress</p> -->
                            <ul class="list-inline" style="font-size:18px;">
                                <li><small><i class="fa fa-dollar"></i> <?php echo $job_description->min_salary.'-'.$job_description->max_salary;?></small></li>
                                <li><small><i class="fa fa-briefcase"></i> <?php echo $job_description->experience;?></small></li>
                                <li><small><i class="fa fa-map-marker"></i> <?php echo $job_description->city;?></small></li>
                            </ul>
                           <!--  <p>Lorem ipsum dolor sit amet, non odio tincidunt ut ante, lorem a euismod suspendisse vel...</p> -->
                            <br>
                            <!-- <a href="#" class="btn btn-primary btn-top">APPLY</a> -->
                        </div><!-- end col -->

                        <div class="col-md-12">
                          <hr>
                          <!-- <h4><u><strong>Job Details</strong></u></h4><br/> -->
                        </div>
                          <div class="col-md-6 col-sm-6">
                            
                               <p><strong>Title:</strong> <?php echo $job_description->job_title;?></p><br/>
                               <p><strong>Salary:</strong> $<?php echo $job_description->min_salary.'-'.$job_description->max_salary;?></p><br/>
                              </tr>
                             
                                <p><strong>Industry:</strong> <?php echo $job_description->job_type;?></p><br/>
                             
                              <!-- <tr>
                                <td class="text-right"><p><strong>Functional&nbsp;Area:</strong></p></td>
                                <td class="text-left"><p>IT Software - Application Programming , Maintenance</p></td>
                              </tr> -->
                            <!--   <tr>
                                <td class="text-right"><p><strong>Role&nbsp;Category:</strong></p></td>
                                <td class="text-left"><p>Programming &amp; Design</p></td>
                              </tr> -->
                            
                          </div>
                          <div class="col-md-6 col-sm-6">
                            
                             
                                
                             
                            
                                <p><strong>Qualifications:</strong> <?php echo $job_description->qualifications;?></p><br/>
                             
                              <!-- <tr>
                                <td class="text-right"><p><strong>Job Nature:</strong></p></td>
                                <td class="text-left"><p>Permanent Full Time</p></td>
                              </tr> -->
                              
                                <!-- <p><strong>Gender:</strong> <?php echo $job_description->gender;?></p><br/> -->
                            
                           
                          </div>
                          <div class="col-md-12 col-sm-12">
                          <!--   <h5><strong>Key Skills</strong></h5>
                            <a href="#." class="btn btn-default" style="border: 1px solid #eee;">PHP</a>
                            <a href="#." class="btn btn-default" style="border: 1px solid #eee;">Ajax</a>
                            <a href="#." class="btn btn-default" style="border: 1px solid #eee;">Angular JS</a>
                            <a href="#." class="btn btn-default" style="border: 1px solid #eee;">Javascript</a>
                            <a href="#." class="btn btn-default" style="border: 1px solid #eee;">HTML5</a>
                            <a href="#." class="btn btn-default" style="border: 1px solid #eee;">CSS3</a>
                            <a href="#." class="btn btn-default" style="border: 1px solid #eee;">MySQL</a>
                            <a href="#." class="btn btn-default" style="border: 1px solid #eee;">Java</a>
                            <a href="#." class="btn btn-default" style="border: 1px solid #eee;">Ajax</a>
                            <a href="#." class="btn btn-default" style="border: 1px solid #eee;">Angular JS</a> -->

                            <div class="clearfix"></div>

                            <br><h5><strong>Job Description</strong></h5>
                            <p><?php echo $job_description->job_desc;?></p><br/>

                           <!--  <br><h5><strong>Skills</strong></h5>
                            <p>Must have<br>
                               - Advanced Java AJAX AngularJS, Javascript, HTML5, CSS3, Bootstrap J2EE (JSP, Servlet, EJB, Struts, JMS) Javascript, HTML, CSS, Django, MySQL, Redis, PostgreSQL, Nginx, Flask, Middleware, Pyhton, Socket Programming REST-JSON Web-services
                            </p> -->

                            <br><h5><strong>Qualifications</strong></h5>
                            <p><?php echo $job_description->qualifications;?></p><br/>

                            <br><h5><strong>Working Days</strong></h5>
                            <p><?php echo $job_description->working_days;?></p><br/>

                           <!--  <br><h5><strong>Job Nature</strong></h5>
                            <p>- Full Time</p> -->

                           <!--  <br><h5><strong>Gender</strong></h5>
                            <p><?php echo $job_description->gender;?></p><br/> -->

                            <!-- <br><h5><strong>Education</strong></h5>
                            <p>- B.Tech/B.E. - Any Specialization</p> -->

                            <br><h5><strong>Company Profile</strong></h5>
                            <p><?php echo $job_description->company_profile;?></p>

                            <hr>
                          </div>
                        <!--   <div class="col-md-12 col-sm-12">
                            <a href="#" class="btn btn-primary btn-top">APPLY</a>
                          </div> -->
                    </div><!-- end freelancer-wrap -->
                </div><!-- end col -->
            </div>
          </li>
          <!-- job end -->
        </ul>
        
      </div>
    </div>
  </div>
</div>
