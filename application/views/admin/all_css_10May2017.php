<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">

    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="shortcut icon" href="<?php echo base_url();?>client_assets/images/favicon.png">

    <title>Day Dreamer</title>

    <!--Core CSS -->
    <link href="<?php echo base_url();?>client_assets/bs3/css/bootstrap.min.css" rel="stylesheet">
    <link href="<?php echo base_url();?>client_assets/css/client_css/bootstrap-reset.css" rel="stylesheet">
    <link href="<?php echo base_url();?>client_assets/font-awesome/css/font-awesome.css" rel="stylesheet" />
    <!-- <link rel="stylesheet" type="text/css" href="http://ajax.googleapis.com/ajax/libs/angular_material/1.1.0/angular-material.min.css"> -->
    <!-- <link rel="stylesheet" href="css/bootstrap-switch.css" /> -->
    <!-- <link rel="stylesheet" type="text/css" href="<?php //echo base_url();?>assets/css/client_css/bootstrap-fileupload/bootstrap-fileupload.css" />
    <link rel="stylesheet" type="text/css" href="<?php //echo base_url();?>assets/css/client_css/bootstrap-wysihtml5/bootstrap-wysihtml5.css" />
    <link rel="stylesheet" type="text/css" href="<?php //echo base_url();?>assets/css/client_css/bootstrap-datepicker/css/datepicker.css" />
    <link rel="stylesheet" type="text/css" href="<?php //echo base_url();?>assets/css/client_css/bootstrap-timepicker/compiled/timepicker.css" />
    <link rel="stylesheet" type="text/css" href="<?php //echo base_url();?>assets/css/client_css/bootstrap-colorpicker/css/colorpicker.css" />
    <link rel="stylesheet" type="text/css" href="<?php //echo base_url();?>assets/css/client_css/bootstrap-daterangepicker/daterangepicker-bs3.css" />
    <link rel="stylesheet" type="text/css" href="<?php //echo base_url();?>assets/css/client_css/bootstrap-datetimepicker/css/datetimepicker.css" />
    <link rel="stylesheet" type="text/css" href="<?php //echo base_url();?>assets/css/client_css/jquery-multi-select/css/multi-select.css" />
    <link rel="stylesheet" type="text/css" href="<?php //echo base_url();?>assets/css/client_css/jquery-tags-input/jquery.tagsinput.css" />
-->
<!-- <link rel="stylesheet" type="text/css" href="<?php //echo base_url();?>assets/css/client_css/select2/select2.css" /> -->

<!-- Custom styles for this template -->
<link href="<?php echo base_url();?>web_assets/css/style.css" rel="stylesheet">
<link href="<?php echo base_url();?>client_assets/css/client_css/style.css" rel="stylesheet">
 <link rel="stylesheet" href="<?php echo base_url();?>client_assets/js/client_js/data-tables/DT_bootstrap.css" />
<link href="<?php echo base_url();?>client_assets/css/client_css/style-responsive.css" rel="stylesheet" />
<link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.3.0/css/datepicker.min.css" />

<!-- Angular js..-->
<script src="https://ajax.googleapis.com/ajax/libs/angularjs/1.5.5/angular.min.js"></script>

<!--<script src="https://ajax.googleapis.com/ajax/libs/angularjs/1.5.5/angular-animate.min.js"></script>
<script src="https://ajax.googleapis.com/ajax/libs/angularjs/1.5.5/angular-route.min.js"></script>
<script src="https://ajax.googleapis.com/ajax/libs/angularjs/1.5.5/angular-aria.min.js"></script>
<script src="https://ajax.googleapis.com/ajax/libs/angularjs/1.5.5/angular-messages.min.js"></script>
<script src="<?php //echo base_url();?>client_assets/js/client_js/angular_material_min.js"></script>-->
<!-- Just for debugging purposes. Don't actually copy this line! -->
    <!--[if lt IE 9]>
    <script src="js/ie8-responsive-file-warning.js"></script><![endif]-->

    <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
    <script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
    <![endif]-->
</head>