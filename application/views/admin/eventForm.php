  <style type="text/css">
 .help-block{
  
  color:red;
}

 </style>
 <div class="container">
    <div class="row">
        <div class="col-sm-6 col-sm-offset-3">

 <div class="row well" style="padding: 10px 0;">
                                    <div class="col-sm-12">
                                        <h4><strong>Add Event</strong></h4>
                                        <?php echo $this->session->flashdata('successmsg');?>
                                        <?php echo $this->session->flashdata('errormsg');?>
                                        
                                    </div>
                                    <form class="" ng-submit="submitEventForm()" name="eventForm" novalidate>
                                      <input type="hidden" name="event_id" ng-model="event.event_id">  
                                    <div class="form-group">
                                        <label class="col-sm-12 control-label">Title:</label>
                                        <div class="col-sm-12">
                                            <input type="text" name="event_title" ng-model="event.event_title" class="form-control" required>
                                            <span ng-show="submitted && eventForm.event_title.$error.required"  class="help-block has-error ng-hide">Event is required.</span>
                                            <span ng-show="errorEventTitle" class="help-block has-error ng-hide">{{errorEventTitle}}</span>
                                            <br>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-sm-12 control-label">Client Name:</label>
                                        <div class="col-sm-12">
                                            <select class="form-control" name="client_name" ng-model="event.client_name" required>
                                                <option value="" selected="true">(Select a Client)</option>
                                                <?php
                                                 foreach($allClients as $key){

                                                ?>
                                                  <option value='<?php echo $key->id?>'><?php echo $key->clnt_first_name.' '.$key->clnt_last_name;?></option>
                                                <?php } ?>
                                            </select>
                                            <span ng-show="submitted && eventForm.client_name.$error.required"  class="help-block has-error ng-hide">Client Name is required.</span>
                                            <span ng-show="errorClientName" class="help-block has-error ng-hide">{{errorClientName}}</span>
                                            <br>
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label class="col-sm-12 control-label">Candidate Name:</label>
                                        <div class="col-sm-12">
                                            <select class="form-control" name="candidate_name" ng-model="event.candidate_name" required>
                                                <option value="" selected="true">(Select a Candidate)</option>
                                                <?php
                                                  foreach($allCandidates as $key)
                                                  {
                                                ?>
                                                  <option value="<?php echo $key->id;?>"><?php echo $key->first_name.' '.$key->last_name;?></option>
                                                <?php } ?>
                                            </select>
                                             <span ng-show="submitted && eventForm.candidate_name.$error.required"  class="help-block has-error ng-hide">Candidate Name is required.</span>
                                             <span ng-show="errorCandidateName" class="help-block has-error ng-hide">{{errorCandidateName}}</span>
                                            <br>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-sm-12 control-label">Type:</label>
                                        <div class="col-sm-12">
                                            <select class="form-control" name="type" ng-model="event.type" required>
                                                <option value="" selected="true">(Select a Type)</option>
                                                <option value="Call">Call</option>
                                                <option value="E-mail">E-mail</option>
                                                <option value="Meeting">Meeting</option>
                                                <option value="Interview">Interview</option>
                                                <option value="Personal">Personal</option>
                                                <option value="Other">Other</option>
                                            </select>
                                            <span ng-show="submitted && eventForm.type.$error.required"  class="help-block has-error ng-hide">Type is required.</span>
                                            <span ng-show="errorType" class="help-block has-error ng-hide">{{errorType}}</span>
                                            <br>
                                            <br>
                                        </div>
                                    </div>
                                     
                                    <div class="form-group">
                                       <label class="col-sm-3  control-label">Start Date:</label>
                                       <div class="col-sm-9">
                                        <div class="input-group date" id="demo-date" style="width:100%">
                                          <div class="input-group-content">
                                            <input data-calenderopen type="text" class="form-control" autocomplete="off"  ng-model="event.date" name="date" placeholder="Please enter Date"  id="open_date"   style="margin-bottom:0px;">
                                            <span id="msg1" ng-show="submitted && eventForm.open_date.$invalid" class="help-block has-error">Date is required.</span>
                                            <!-- <span ng-show="errorValidity" class="help-block has-error ng-hide">{{errorValidity}}</span> -->
                                            <span ng-show="errorDate" class="help-block has-error ng-hide">{{errorDate}}</span>
                                          </div>
                                          <!-- <a href="javascript:void(0);" class="input-group-addon date"><i class="fa fa-calendar"></i></a> -->
                                        </div>
                                        <br>
                                      </div>
                                    </div>

                                    <div class="form-group">
                                       <label class="col-sm-3  control-label">End Date:</label>
                                       <div class="col-sm-9">
                                        <div class="input-group date" id="demo-date" style="width:100%">
                                          <div class="input-group-content">
                                            <input data-calenderend type="text" class="form-control" autocomplete="off"  ng-model="event.end_date" name="end_date" placeholder="Please enter end Date"  id="end_date"   style="margin-bottom:0px;">
                                            <span id="msg1" ng-show="submitted && eventForm.end_date.$invalid" class="help-block has-error">End Date is required.</span>
                                            <!-- <span ng-show="errorValidity" class="help-block has-error ng-hide">{{errorValidity}}</span> -->
                                            <span ng-show="errorEndDate" class="help-block has-error ng-hide">{{errorEndDate}}</span>
                                          </div>
                                          <!-- <a href="javascript:void(0);" class="input-group-addon date"><i class="fa fa-calendar"></i></a> -->
                                        </div>
                                        <br>
                                      </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-sm-12 control-label">Time:</label>
                                        <div class="col-xs-3">
                                            <input type="radio" name="time" ng-model="event.time" value="1">
                                        </div>
                                        <div class="col-xs-3" style="padding-left: 0;">
                                            <select class="form-control" style="padding: 6px 0px;" name="hours" ng-model="event.hours">
                                                <option disabled="disabled" selected="selected" value="">Select hours</option>
                                                <option value="1">1</option>
                                                <option value="2">2</option>
                                                <option value="3">3</option>
                                                <option value="4">4</option>
                                                <option value="5">5</option>
                                                <option value="6">6</option>
                                                <option value="7">7</option>
                                                <option value="8">8</option>
                                                <option value="9">9</option>
                                                <option value="10">10</option>
                                                <option value="11">11</option>
                                                <option value="12">12</option>
                                            </select>
                                            <!-- <span ng-show="submitted && eventForm.hours.$error.required"  class="help-block has-error ng-hide">Hours is required.</span>
                                            <span ng-show="errorEventTime" class="help-block has-error ng-hide">{{errorEventTime}}</span> -->
                                        </div>
                                        <div class="col-xs-3" style="padding-left: 0;">
                                            <select class="form-control" style="padding: 6px 0px;" name="minutes" ng-model="event.minutes">
                                                <option disabled="disabled" selected="selected" value="">Select minutes</option>
                                                <option value="00">00</option>
                                                <option value="15">15</option>
                                                <option value="30">30</option>
                                                <option value="45">45</option>
                                            </select>
                                            <!-- <span ng-show="submitted && eventForm.minutes.$error.required"  class="help-block has-error ng-hide">Minutes is required.</span>
                                            <span ng-show="errorMinutes" class="help-block has-error ng-hide">{{errorMinutes}}</span> -->
                                        </div>
                                        <div class="col-xs-3" style="padding-left: 0;" >
                                            <select class="form-control" style="padding: 6px 0px;" name="meridium" ng-model="event.meridium">
                                                                                                <option disabled="disabled" selected="selected" value="">Select minutes</option>
                                             <option disabled="disabled" selected="selected" value="">Select seconds</option>
                                                <option value="AM" selected="true">AM</option>
                                                <option value="PM">PM</option>
                                            </select>
                                            <!-- <span ng-show="submitted && eventForm.meridium.$error.required"  class="help-block has-error ng-hide">Merīdiem is required.</span>
                                            <span ng-show="errorMeridium" class="help-block has-error ng-hide">{{errorMeridium}}</span> -->
                                            <br>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-sm-12 control-label">Event organiser:</label>
                                        <div class="col-sm-12">
                                            <input class="form-control" type="text" name="organiser" ng-model="event.organiser" required>
                                            <span ng-show="submitted && eventForm.organiser.$error.required"  class="help-block has-error ng-hide">Event Organiser is required.</span>
                                            <span ng-show="errorOrganiser" class="help-block has-error ng-hide">{{errorOrganiser}}</span>
                                            <br><br>
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label class="col-sm-12 control-label">Event Contact:</label>
                                        <div class="col-sm-12">
                                            <input class="form-control" type="text" name="contact" ng-model="event.contact" required>
                                            <span ng-show="submitted && eventForm.contact.$error.required"  class="help-block has-error ng-hide">Event Contact is required.</span>
                                            <span ng-show="errorContact" class="help-block has-error ng-hide">{{errorContact}}</span>
                                            <br><br>
                                        </div>
                                    </div>
                                     <div class="form-group">
                                        <label class="col-sm-12 control-label">Contact Details:</label>
                                        <div class="col-sm-12">
                                            <textarea class="form-control" rows="4" cols="4" name="contact_details" ng-model="event.contact_details" style="resize:none;" required></textarea>
                                            <span ng-show="submitted && eventForm.contact_details.$error.required"  class="help-block has-error ng-hide">Contact Details is required.</span>
                                            <span ng-show="errorContactDetails" class="help-block has-error ng-hide">{{errorContactDetails}}</span>
                                            <br><br>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-sm-12 control-label">Event Details:</label>
                                        <div class="col-sm-12">
                                            <textarea class="form-control" rows="4" cols="4" name="event_details" ng-model="event.event_details" style="resize:none;" required></textarea>
                                            <span ng-show="submitted && eventForm.event_details.$error.required"  class="help-block has-error ng-hide">Event Details is required.</span>
                                            <span ng-show="errorEventDetails" class="help-block has-error ng-hide">{{errorEventDetails}}</span>
                                            <br><br>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-sm-12 control-label">Event URL:</label>
                                        <div class="col-sm-12">
                                            <input class="form-control" type="text" name="event_url" ng-model="event.event_url" required>
                                            <span ng-show="submitted && eventForm.event_url.$error.required"  class="help-block has-error ng-hide">Event URL is required.</span>
                                            <span ng-show="errorEventURL" class="help-block has-error ng-hide">{{errorEventURL}}</span>
                                            <br><br>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-sm-12 control-label">State:</label>
                                        <div class="col-sm-12">
                                            <select class="form-control" name="state" ng-model="event.state" required>
                                                <option disabled="disabled" selected="selected" value="">Select your State</option>
                                                <option value="15 minutes">15 minutes</option>
                                                <option value="30 minutes">30 minutes</option>
                                                <option value="45 minutes">45 minutes</option>
                                                <option value="1 hour">1 hour</option>
                                                <option value="1.5 hours">1.5 hours</option>
                                                <option value="2 hours">2 hours</option>
                                                <option value="3 hours">3 hours</option>
                                                <option value="4 hours">4 hours</option>
                                                <option value="more than 4 hours">more than 4 hours</option>
                                            </select>
                                            <span ng-show="submitted && eventForm.state.$error.required"  class="help-block has-error ng-hide">State is required.</span>
                                            <span ng-show="errorState" class="help-block has-error ng-hide">{{errorState}}</span>
                                            <br>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-sm-12 control-label">Desc:</label>
                                        <div class="col-sm-12">
                                            <textarea class="form-control"  rows="3" name="desc" ng-model="event.desc" style="resize:none;" required></textarea>
                                            <span ng-show="submitted && eventForm.desc.$error.required"  class="help-block has-error ng-hide">Description is required.</span>
                                            <span ng-show="errordesc" class="help-block has-error ng-hide">{{errordesc}}</span>
                                            <br>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <div class="col-sm-12">
                                            <button type="submit" class="btn btn-info pull-right btn-sm" ng-click="submitted = true"><strong><i class="fa fa-plus"></i> Add Event</strong></button><span class="pull-right"> &nbsp; &nbsp; </span>
                                        </div>
                                    </div>
                                </form>
                                </div>

                                </div>


        </div>
    </div>