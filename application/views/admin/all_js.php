<!--Core js-->
<script src="<?php echo base_url();?>client_assets/js/client_js/jquery.js"></script>
<script src="<?php echo base_url();?>client_assets/js/client_js/jquery-1.8.3.min.js"></script>
<script src="http://code.jquery.com/jquery-latest.js"></script>
<script src="<?php echo base_url();?>client_assets/bs3/js/bootstrap.min.js"></script>
<script src="<?php echo base_url();?>client_assets/js/client_js/jquery-ui-1.9.2.custom.min.js"></script>

<script src="<?php echo base_url();?>client_assets/js/client_js/ui-bootstrap-tpls-0.14.3.min.js"></script>
<script src="//cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.3.0/js/bootstrap-datepicker.min.js"></script>
<!-- <script src="js/bootstrap-switch.js"><<?php echo base_url();?>assets//script> -->

<script src="//cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.3.0/js/bootstrap-datepicker.min.js"></script>


<!--common script init for all pages-->
<script src="<?php echo base_url();?>client_assets/js/client_js/scripts.js"></script>
<script type="text/javascript" src="<?php echo base_url();?>client_assets/js/client_js/data-tables/jquery.dataTables.js"></script>
<script type="text/javascript" src="<?php echo base_url();?>client_assets/js/client_js/data-tables/DT_bootstrap.js"></script>
<script type="text/javascript" src="<?php echo base_url();?>client_assets/js/client_js/dynamic_table_init.js"></script>
<script type="text/javascript" src="<?php echo base_url();?>client_assets/js/client_js/canvasjs.min.js"></script>
<script type="text/javascript" src="<?php echo base_url();?>client_assets/js/client_js/jquery.cropit.js"></script>
<script type="text/javascript" src="<?php echo base_url();?>client_assets/js/client_js/fullcalendar/fullcalendar.js"></script>
<script type="text/javascript" src="<?php echo base_url();?>client_assets/js/client_js/external-dragging-calendar.js"></script>
<script type="text/javascript" src="<?php echo base_url();?>client_assets/js/client_js/jquery.worked-hours-grid.js"></script>
<script src="//cdn.ckeditor.com/4.5.6/standard/ckeditor.js"></script> 
<script src="https://cdn.datatables.net/1.10.15/js/jquery.dataTables.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/rateYo/2.2.0/jquery.rateyo.min.js"></script>
<!-- <script src="js/toggle-init.js"></script> -->
<script type="text/javascript">
$(document).ready(function(){

	$('#date_of_birth2').datepicker({

            format: "yyyy/mm/dd",
            autoclose:true
        });

	$('#date_of_birth1').datepicker({

            format: "yyyy/mm/dd",
            autoclose:true
        });
});

function confirmDelete(){
var result = confirm("Do you really want to delete?");
if(result){

	return true;
}else{

	return false;
}
}

//function for dispproving job
function confirmJobDisapprove(){
var result = confirm("Do you really want to disapprove a job?");
if(result){

    return true;
}else{

    return false;
}


}

//function for approving job
function confirmApprove(){
var result = confirm("Do you really want to approve a job?");
if(result){

    return true;
}else{

    return false;
}


}

//function for confirming deleting a group
function deleteGroup(){
var result = confirm("Do you really want to delete a Group?");

if(result){

    return true;
}else{

    return false;
}


}
//funtion for deleting homeless
function deleteHomeless(){
var result = confirm("Do you really want to delete?");

if(result){

    return true;
}else{

    return false;
}
    
}
//function for confirming deleting a substance
function deleteSubstance(){
var result = confirm("Do you really want to delete?");

if(result){

    return true;
}else{

    return false;
}


}

//function for deactivating user
function deactivateUser(){
var result = confirm("Do you really want to deactivate a user?");
if(result){

    return true;
}else{

    return false;
}



}

//function for activating user
function activateUser(){
var result = confirm("Do you really want to activate a user?");
if(result){

    return true;
}else{

    return false;
}


}

//function for deactivating office
function deactivateClient(){

var result = confirm("Do you really want to deactivate a office?");
if(result){

    return true;
}else{

    return false;
}

}

//function for activating client
function activateClient(){
var result = confirm("Do you really want to activate a office?");
if(result){

    return true;
}else{

    return false;
}


}

//function for confirming deleting of a blog
function confirmMsg(){
var result = confirm("Do you really want to delete a blog?");

if(result){

    return true;
}else{

    return false;
}
}

//function for confirming deleting of a blog
function deleteCVConfirmMsg(){
var result = confirm("Do you really want to delete?");

if(result){

    return true;
}else{

    return false;
}
}


//function for confirming deleting of a blog
function deleteJobAdv(){
var result = confirm("Do you really want to delete?");

if(result){

    return true;
}else{

    return false;
}
}

function myFunction(id){
//alert(id);
$.ajax({
url:'<?php echo base_url();?>admin/getSubOffices',
type:'POST',
data:{id:id},
success:function(data){
$('#'+id).find('#showSubOffices').html(data);
// $("ul#showSubOffices").html('');
// $("ul#showSubOffices").html(data);

//alert($("#showSubOffices").html(data));
}

});

}


//function for calling a package modal
function editModal(){
$('#myModal-2').modal('show');
}
</script>

<script>

var Script = function () {


    /* initialize the external events
     -----------------------------------------------------------------*/

    $('#external-events div.external-event').each(function() {

        // create an Event Object (http://arshaw.com/fullcalendar/docs/event_data/Event_Object/)
        // it doesn't need to have a start or end
        var eventObject = {
            title: $.trim($(this).text()) // use the element's text as the event title
        };

        // store the Event Object in the DOM element so we can get to it later
        $(this).data('eventObject', eventObject);

        // make the event draggable using jQuery UI
        $(this).draggable({
            zIndex: 999,
            revert: true,      // will cause the event to go back to its
            revertDuration: 0  //  original position after the drag
        });

    });



    
    /* initialize the calendar
     -----------------------------------------------------------------*/

    var date = new Date();
    var d = date.getDate();
    var m = date.getMonth();
    var y = date.getFullYear();
    var a='<?php echo json_encode($events);?>';
    var a=JSON.parse(a);
    console.log(a);

    $('#calendar').fullCalendar({

        header: {
            left: 'prev,next today',
            center: 'title',
            right: 'month,basicWeek,basicDay'
        },
        editable: true,
        droppable: true, // this allows things to be dropped onto the calendar !!!
        drop: function(date, allDay) { // this function is called when something is dropped
               
            // retrieve the dropped element's stored Event Object
            var originalEventObject = $(this).data('eventObject');

            // we need to copy it, so that multiple events don't have a reference to the same object
            var copiedEventObject = $.extend({}, originalEventObject);

            // assign it the date that was reported
            copiedEventObject.start = date;
            copiedEventObject.allDay = allDay;

            // render the event on the calendar
            // the last `true` argument determines if the event "sticks" (http://arshaw.com/fullcalendar/docs/event_rendering/renderEvent/)
            $('#calendar').fullCalendar('renderEvent', copiedEventObject, true);

            // is the "remove after drop" checkbox checked?
            if ($('#drop-remove').is(':checked')) {
                // if so, remove the element from the "Draggable Events" list
                $(this).remove();
            }

        },
       
        events:a
        // events: [
        //     {
        //         title: 'All Day Event',
        //         start: new Date(y, m, 1)
        //     },
        //     {
        //         title: 'Long Event',
        //         start: new Date(y, m, d-5),
        //         end: new Date(y, m, d-2)
        //     },
        //     {
        //         id: 999,
        //         title: 'Repeating Event',
        //         start: new Date(y, m, d-3, 16, 0),
        //         allDay: false
        //     },
        //     {
        //         id: 999,
        //         title: 'Repeating Event',
        //         start: new Date(y, m, d+4, 16, 0),
        //         allDay: false
        //     },
        //     {
        //         title: 'Meeting',
        //         start: new Date(y, m, d, 10, 30),
        //         allDay: false
        //     },
        //     {
        //         title: 'Lunch',
        //         start: new Date(y, m, d, 12, 0),
        //         end: new Date(y, m, d, 14, 0),
        //         allDay: false
        //     },
        //     {
        //         title: 'Birthday Party',
        //         start: new Date(y, m, d+1, 19, 0),
        //         end: new Date(y, m, d+1, 22, 30),
        //         allDay: false
        //     },
        //     {
        //         title: 'Click for Google',
        //         start: new Date(y, m, 28),
        //         end: new Date(y, m, 29),
        //         url: 'http://google.com/'
        //     }
        // ]
    });


}();

</script>

<script>

var Script = function () {


    /* initialize the external events
     -----------------------------------------------------------------*/

    $('#external-events div.external-event').each(function() {

        // create an Event Object (http://arshaw.com/fullcalendar/docs/event_data/Event_Object/)
        // it doesn't need to have a start or end
        var eventObject = {
            title: $.trim($(this).text()) // use the element's text as the event title
        };

        // store the Event Object in the DOM element so we can get to it later
        $(this).data('eventObject', eventObject);

        // make the event draggable using jQuery UI
        $(this).draggable({
            zIndex: 999,
            revert: true,      // will cause the event to go back to its
            revertDuration: 0  //  original position after the drag
        });

    });



    
    /* initialize the calendar
     -----------------------------------------------------------------*/

    var date = new Date();
    var d = date.getDate();
    var m = date.getMonth();
    var y = date.getFullYear();
    var interview='<?php echo json_encode($interviews);?>';
    var interview=JSON.parse(interview);
    console.log(interview);
   
    $('#interview_calendar').fullCalendar({

        header: {
            left: 'prev,next today',
            center: 'title',
            right: 'month,basicWeek,basicDay'
        },
        editable: true,
        droppable: true, // this allows things to be dropped onto the calendar !!!
        drop: function(date, allDay) { // this function is called when something is dropped
               
            // retrieve the dropped element's stored Event Object
            var originalEventObject = $(this).data('eventObject');

            // we need to copy it, so that multiple events don't have a reference to the same object
            var copiedEventObject = $.extend({}, originalEventObject);

            // assign it the date that was reported
            copiedEventObject.start = date;
            copiedEventObject.allDay = allDay;

            // render the event on the calendar
            // the last `true` argument determines if the event "sticks" (http://arshaw.com/fullcalendar/docs/event_rendering/renderEvent/)
            $('#interview_calendar').fullCalendar('renderEvent', copiedEventObject, true);

            // is the "remove after drop" checkbox checked?
            if ($('#drop-remove').is(':checked')) {
                // if so, remove the element from the "Draggable Events" list
                $(this).remove();
            }

        },
       
        events:interview
        // events: [
        //     {
        //         title: 'All Day Event',
        //         start: new Date(y, m, 1)
        //     },
        //     {
        //         title: 'Long Event',
        //         start: new Date(y, m, d-5),
        //         end: new Date(y, m, d-2)
        //     },
        //     {
        //         id: 999,
        //         title: 'Repeating Event',
        //         start: new Date(y, m, d-3, 16, 0),
        //         allDay: false
        //     },
        //     {
        //         id: 999,
        //         title: 'Repeating Event',
        //         start: new Date(y, m, d+4, 16, 0),
        //         allDay: false
        //     },
        //     {
        //         title: 'Meeting',
        //         start: new Date(y, m, d, 10, 30),
        //         allDay: false
        //     },
        //     {
        //         title: 'Lunch',
        //         start: new Date(y, m, d, 12, 0),
        //         end: new Date(y, m, d, 14, 0),
        //         allDay: false
        //     },
        //     {
        //         title: 'Birthday Party',
        //         start: new Date(y, m, d+1, 19, 0),
        //         end: new Date(y, m, d+1, 22, 30),
        //         allDay: false
        //     },
        //     {
        //         title: 'Click for Google',
        //         start: new Date(y, m, 28),
        //         end: new Date(y, m, 29),
        //         url: 'http://google.com/'
        //     }
        // ]
    });


}();

</script>
<script type='text/javascript'>
    "use strict";
    $(function() {
      $(".ts-grid").workedHoursGrid({
        selectors: {
                row: ".ts-row",
                range: ".ts-range",
                start: ".ts-start",
                end: ".ts-end",
                sum: ".ts-sum",
                total: ".ts-total"
            }
      });
    });
</script>
</body>
</html>