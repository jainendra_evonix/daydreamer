
<style type="text/css">
.help-block{
color:red;

}

</style>
        

        <!--main content start-->
        <section id="adminsection" class="container">
            <section class="wrapper">
                <!-- page start-->
                <div class="row">
                    <div class="col-md-12">
                        <h4 class="form-heading"><strong>Create New Office1</strong></h4>
                        <?php echo $this->session->flashdata('successmsg');?>
                        <?php echo $this->session->flashdata('errormsg');?>
                        <p>Create and add a new office. <a href="#" class="pull-right">Help <i class="fa fa-question-circle"></i></a></p>
                        <br>
                        <section class="panel">
                            <div class="panel-body">
                                <div class="row">
                                    <div class="col-md-12">
                                        <h4>Client Information</h4>
                                        <hr>
                                    </div>
                                    <form class="form-horizontal bucket-form" ng-submit="submitclientForm()" name="subClientForm" novalidate>
                                       <input type="hidden" name="client_id" ng-model="office.client_id"> 
                                    <div class="col-md-6">
                                            <div class="form-group">
                                                <label class="col-sm-3 col-sm-offset-1 control-label">First Name:</label>
                                                <div class="col-sm-7">
                                                    <input type="text"  class="form-control" name="first_name" ng-model="office.first_name" required> 
                                                    <span ng-show="submitted && subClientForm.first_name.$error.required"  class="help-block has-error ng-hide">First Name is required.</span>
                                                     <span ng-show="errorFirstName" class="help-block has-error ng-hide">{{errorFirstName}}</span>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label class="col-sm-3 col-sm-offset-1 control-label">Last Name:</label>
                                                <div class="col-sm-7">
                                                    <input type="text"  class="form-control" name="last_name" ng-model="office.last_name" required>
                                                    <span ng-show="submitted && subClientForm.last_name.$error.required"  class="help-block has-error ng-hide">Last Name is required.</span>
                                                     <span ng-show="errorLastName" class="help-block has-error ng-hide">{{errorLastName}}</span>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label class="col-sm-3 col-sm-offset-1 control-label">Telephone:</label>
                                                <div class="col-sm-7">
                                                    <input type="text"  class="form-control" name="telephone" ng-model="office.telephone" required>
                                                    <span ng-show="submitted && subClientForm.telephone.$error.required"  class="help-block has-error ng-hide">Telephone is required.</span>
                                                    <span ng-show="errorTelephone" class="help-block has-error ng-hide">{{errorTelephone}}</span>
                                                </div>
                                            </div>
                                           <!--  <div class="form-group">
                                                <label class="col-sm-3 col-sm-offset-1 control-label">Fax:</label>
                                                <div class="col-sm-7">
                                                    <input type="text"  class="form-control">
                                                </div>
                                            </div> -->
                                    </div>
                                    <div class="col-md-6">
                                            <div class="form-group">
                                                <label class="col-sm-3 col-sm-offset-1 control-label">Email:</label>
                                                <div class="col-sm-7">
                                                     <input type="text"  class="form-control" name="email" ng-model="office.email" required>
                                                    <span ng-show="submitted && subClientForm.email.$error.required"  class="help-block has-error ng-hide">Email is required.</span>
                                                     <span ng-show="errorEmail" class="help-block has-error ng-hide">{{errorEmail}}</span>
                                                </div>
                                            </div>
                                           <!--  <div class="form-group">
                                                <label class="col-sm-3 col-sm-offset-1 control-label">Parent Client:</label>
                                                <div class="col-sm-7">
                                                    <input type="text"  class="form-control" name="parent_client" ng-model="office.parent_client" required>
                                                    <span ng-show="submitted && subClientForm.parent_client.$error.required"  class="help-block has-error ng-hide">Main Office is required.</span>
                                                </div>
                                            </div> -->
                                           <!--  <div class="form-group">
                                                <label class="col-sm-3 col-sm-offset-1 control-label">Account Manager:</label>
                                                <div class="col-sm-7">
                                                    <input type="text"  class="form-control" >
                                                </div>
                                            </div> -->
                                            <div class="form-group">
                                                <label class="col-sm-3 col-sm-offset-1 control-label">Password:</label>
                                                <div class="col-sm-7">
                                                    <input type="password"  class="form-control" name="password" ng-model="office.password" required>
                                                     <span ng-show="submitted && subClientForm.password.$error.required"  class="help-block has-error ng-hide">Password is required.</span>
                                                     <span ng-show="errorPassword" class="help-block has-error ng-hide">{{errorPassword}}</span>
                                                </div>
                                            </div>
                                    </div>
                                    <div class="col-md-12">
                                        <br>
                                        <h4>Address Information</h4>
                                        <hr>
                                    </div>
                   
                                    <div class="col-md-6">
                                            <div class="form-group">
                                                <label class="col-sm-3 col-sm-offset-1 control-label">City:</label>
                                                <div class="col-sm-7">
                                                    <input type="text"  class="form-control" name="city" ng-model="office.city" required>
                                                    <span ng-show="submitted && subClientForm.city.$error.required"  class="help-block has-error ng-hide">City is required.</span> 
                                                    <span ng-show="errorCity" class="help-block has-error ng-hide">{{errorCity}}</span>
                                                </div>
                                            </div>
                                            <!-- <div class="form-group">
                                                <label class="col-sm-3 col-sm-offset-1 control-label">Pincode:</label>
                                                <div class="col-sm-7">
                                                    <input type="text"  class="form-control" name="pincode" ng-model="office.pincode" required>
                                                    <span ng-show="submitted && subClientForm.pincode.$error.required"  class="help-block has-error ng-hide">Pincode is required.</span> 
                                                </div>
                                            </div> -->
                                            <div class="form-group">
                                                <label class="col-sm-3 col-sm-offset-1 control-label">Country:</label>
                                                <div class="col-sm-7">
                                                    <!-- <input type="text"  class="form-control" placeholder="Enter your nationality"> -->
                                                    <select class="form-control" name="country" ng-model="office.country" required>

                                                     <option value="">-None-</option>
                                                       <?php
                                                         foreach($allCountries as $key)
                                                         {
                                                       ?>
                                                       <option value="<?php echo $key->id;?>"><?php echo $key->country_name;?></option>
                                                         <?php } ?>
                                                    </select>
                                                    <span ng-show="submitted && subClientForm.country.$error.required"  class="help-block has-error ng-hide">Country is required.</span> 
                                                     <span ng-show="errorCountry" class="help-block has-error ng-hide">{{errorCountry}}</span>
                                                </div>
                                            </div>
                                            <br>
                                    </div>
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <div class="col-sm-12">
                                                <button type="submit" class="btn btn-info pull-right btn-sm" ng-click="submitted = true"><strong><i class="fa fa-plus"></i> Create Client</strong></button><span class="pull-right"> &nbsp; &nbsp; </span>

                                                <button type="button" class="btn btn-danger pull-right btn-sm"><strong><i class="fa fa-times"></i> Cancel</strong></button>
                                            </div>
                                        </div>
                                    </div>
                                    </form>
                                </div>
                            </div>
                        </section>
                    </div>
                </div>
                <!-- page end-->

            </section>
        </section>
        <!--main content end-->





    
</div>


<!-- 
<script type="text/javascript" src="js/ckeditor/ckeditor.js"></script> -->