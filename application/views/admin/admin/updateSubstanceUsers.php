<style type="text/css">
.radiotop {
    color:#aa00ff;
    display: block;
    font-size: 9px;
    left: -32px;
    line-height: 10px;
    position: absolute;
    text-align: center;
    top: -25px;
    vertical-align: bottom;
    width: 80px;
  }
  .gradelist .radio {
    margin-right: 72px;
    position: relative;
  }
  .gradelist .radio input[type="radio"] {
    margin-left: -5px;
  }
</style>
<div class="row" id="substance_div">
  <div class="col-md-8 col-md-offset-2">
    <section class="panel">
     <section class="panel-body">
       <form class="form-horizontal bucket-form" name="substanceForm" ng-submit="submitSubstanceForm()" novalidate >
        <input type="hidden" name="substance_id" ng-model="substance.substance_id">
         <h4><strong>Substance Use Form</strong></h4><br/>
         <div class="form-group">
            
              <div class="form-group">
                <label class="col-sm-3 control-label">Substance name:</label>
                <div class="col-sm-9">
                  <input class="form-control" type="text" name="drug_name" ng-model="substance.drug_name" required>
                  <span ng-show="submitted && substanceForm.drug_name.$error.required"  class="help-block has-error ng-hide">Drug Name is required.</span>
                  <span ng-show="errorDrugName" class="help-block has-error ng-hide">{{errorDrugName}}</span>

                </div>
              </div>


              <div class="form-group">
               <label class="col-sm-3  control-label">Used from:</label>
               <div class="col-sm-9">
                <div class="input-group date" id="demo-date" style="width:100%">
                  <div class="input-group-content">
                    <input data-calenderopen type="text" class="form-control" autocomplete="off"  ng-model="substance.start_date" name="start_date" placeholder="Please enter From Date"  id="start_date"   style="margin-bottom:0px;">
                    <!-- <span id="msg1" ng-show="submitted && eventForm.open_date.$invalid" class="help-block has-error">Date is required.</span> -->
                    <!-- <span ng-show="errorValidity" class="help-block has-error ng-hide">{{errorValidity}}</span> -->
                    <span ng-show="errorDate" class="help-block has-error ng-hide">{{errorDate}}</span>
                  </div>
                  <!-- <a href="javascript:void(0);" class="input-group-addon date"><i class="fa fa-calendar"></i></a> -->
                </div>

              </div>
            </div>


            <div class="form-group">
             <label class="col-sm-3  control-label">Used until:</label>
             <div class="col-sm-9">
              <div class="input-group date" id="demo-date" style="width:100%">
                <div class="input-group-content">
                  <input data-calenderend type="text" class="form-control" autocomplete="off"  ng-model="substance.end_date" name="end_date" placeholder="Please enter until Date"  id="until_date"   style="margin-bottom:0px;">
                  <!-- <span id="msg1" ng-show="submitted && eventForm.end_date.$invalid" class="help-block has-error">End Date is required.</span> -->
                  <!-- <span ng-show="errorValidity" class="help-block has-error ng-hide">{{errorValidity}}</span> -->
                  <span ng-show="errorEndDate" class="help-block has-error ng-hide">{{errorEndDate}}</span>
                </div>
                <!-- <a href="javascript:void(0);" class="input-group-addon date"><i class="fa fa-calendar"></i></a> -->
              </div>

            </div><br>
          </div>

          <div class="form-group">
               <label class="col-sm-3 control-label">Frequency of use (at peak use):</label>
               <div class="col-sm-9">


                <ul class="list-inline list-unstyled gradelist">
                  <li class="radio">
                    <label><span class="radiotop">Very Frequently (more than 2-3 times a day)</span><input type="radio" ng-model="substance.optradio" name="optradio" value="Very Frequently (more than 2-3 times a day)" required><br> <span class="healthrating" ><strong>1</strong></span></label>
                  </li>
                  <li class="radio">
                    <label><span class="radiotop">Frequently (1-2 times a day)</span><input type="radio" ng-model="substance.optradio" name="optradio" value="Frequently (1-2 times a day)" required><br> <span class="healthrating" ><strong>2</strong></span></label>
                  </li>
                  <li class="radio">
                    <label><span class="radiotop">Occasionally (2-3 times a week)</span><input type="radio" ng-model="substance.optradio" name="optradio" value="Occasionally (2-3 times a week)" required><br> <span class="healthrating" ><strong>3</strong></span></label>
                  </li>
                  <li class="radio">
                    <label><span class="radiotop">Rarely (2-3 times a month)</span><input type="radio" ng-model="substance.optradio" name="optradio" value="Rarely (2-3 times a month)" required ><br> <span class="healthrating" ><strong>4</strong></span></label>
                  </li>
                  <li class="radio">
                    <label><span class="radiotop">Very Rarely (Once a month or less)</span><input type="radio" ng-model="substance.optradio" name="optradio" value="Very Rarely (Once a month or less)" required><br> <span class="healthrating" ><strong>5</strong></span></label>
                  </li>
                  <li class="radio">
                    <label><span class="radiotop">Do not use/Do not have</span><input type="radio" name="optradio" ng-model="substance.optradio" value="Do not use/Do not have" required><br> <span class="healthrating" ><strong>6</span></strong></label>
                  </li>
                  <span ng-show="submittedd && substanceForm.optradio.$invalid" class="help-block has-error ng-hide warnig">Please select Frequency of use</span>
                  <span class="help-block has-error ng-hide warnig" style="color: red;" ng-show="optradioError">{{optradioError}}</span>
                </ul>

                
                </div>
              </div>

         
              <div class="form-group">
               <label class="col-sm-3 control-label">Frequency of use now:</label>
               <div class="col-sm-9">


                <ul class="list-inline list-unstyled gradelist">
                  <li class="radio">
                    <label><span class="radiotop">Very Frequently (more than 2-3 times a day)</span><input type="radio" ng-model="substance.optradio1" name="optradio1" value="Very Frequently (more than 2-3 times a day)" required><br> <span class="healthrating" ><strong>1</strong></span></label>
                  </li>
                  <li class="radio">
                    <label><span class="radiotop">Frequently (1-2 times a day)</span><input type="radio" ng-model="substance.optradio1" name="optradio1" value="Frequently (1-2 times a day)" required><br> <span class="healthrating" ><strong>2</strong></span></label>
                  </li>
                  <li class="radio">
                    <label><span class="radiotop">Occasionally (2-3 times a week)</span><input type="radio" ng-model="substance.optradio1" name="optradio1" value="Occasionally (2-3 times a week)" required><br> <span class="healthrating" ><strong>3</strong></span></label>
                  </li>
                  <li class="radio">
                    <label><span class="radiotop">Rarely (2-3 times a month)</span><input type="radio" ng-model="substance.optradio1" name="optradio1" value="Rarely (2-3 times a month)" required ><br> <span class="healthrating" ><strong>4</strong></span></label>
                  </li>
                  <li class="radio">
                    <label><span class="radiotop">Very Rarely (Once a month or less)</span><input type="radio" ng-model="substance.optradio1" name="optradio1" value="Very Rarely (Once a month or less)" required><br> <span class="healthrating" ><strong>5</strong></span></label>
                  </li>
                  <li class="radio">
                    <label><span class="radiotop">Do not use/Do not have</span><input type="radio" name="optradio1" ng-model="substance.optradio1" value="Do not use/Do not have" required><br> <span class="healthrating" ><strong>6</span></strong></label>
                  </li>
                  <span ng-show="submittedd && substanceForm.optradio.$invalid" class="help-block has-error ng-hide warnig">Please select Frequency of use now</span>
                  <span class="help-block has-error ng-hide warnig" style="color: red;" ng-show="optradioError">{{optradioError}}</span>
                </ul>

                
                                        </div>
                                      </div>

                                      <div class="form-group">
                                        <label class="col-sm-3 control-label">Reasons for use:</label>
                                        <div class="col-sm-9">
                                          <textarea class="form-control" rows="4" cols="4" type="text" name="reasons" ng-model="substance.reasons" required></textarea>
                                          <span ng-show="submitted && substanceForm.reasons.$error.required"  class="help-block has-error ng-hide">Reasons for use is required.</span>
                                          <span ng-show="errorReasons" class="help-block has-error ng-hide">{{errorReasons}}</span>

                                        </div>
                                      </div>

                                      <div class="form-group">
                                        <label class="col-sm-3 control-label">Any other significant information:</label>
                                        <div class="col-sm-9">
                                          <textarea class="form-control" rows="4" cols="4" type="text" name="significant_info" ng-model="substance.significant_info" required></textarea>
                                          <span ng-show="submitted && substanceForm.significant_info.$error.required"  class="help-block has-error ng-hide">Any other significant information is required.</span>
                                          <span ng-show="errorInfo" class="help-block has-error ng-hide">{{errorInfo}}</span>

                                        </div>
                                      </div>

                                    </div>
                                    <div class="form-group">
                                      <div class="col-md-12">
                                        <button type="submit" ng-click="submitted = true" class="btn btn-info btn-sm pull-right"><strong>Submit</strong></button>
                                      </div>
                                    </div>
                                  </form>

                                </section>
                              </section>

                            </div>

                          </div>
                          <!-- page end-->

                        </div>