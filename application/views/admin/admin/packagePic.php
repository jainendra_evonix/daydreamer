<style type="text/css">
.help-block{
  color:red;

}
.profileimg {
  width:250px;
  height:250px;
  position: relative;
  display: block;
  margin: auto;
  background-color: transparent;
  padding: 0 !important;
}
.profileimg .editprofileimg {
    bottom: 5px;
    color: #fff;
    opacity: 0.5;
    position: absolute;
    right: 5px;
    top: auto;
}
.editprofileimg {
  position: absolute;
  bottom: 46px;
  right: 6px;
  top: 205px;
  width: 40px;
  height: 40px;
  background-color: rgba(0, 0, 0, 0.7);
  color: #ccc;
  padding: 7px;
  font-size: 18px;
  text-align: center;
  border-radius: 4px;
}
.editprofileimg:hover {
  color: #fff;
  background-color: rgba(0, 0, 0, 1);
}

.cropit-preview 

{ 
  background-color: #f8f8f8; 
  background-size: cover;
  border: 1px solid #ccc;
  border-radius: 3px; 
  margin-top: 7px;
  width: 202px;
  height: 202px;

}
.cropit-preview-image-container 
{ 
  cursor: move; 
} 
.image-size-label 
{ 
  margin-top: 10px; 
} 
input,.export
{
 display: block; 
} 
button 
{ 
  margin-top: 10px; 
} 
.req{ 
  color:red; 
} 


.socialfgroup {
  position: relative;
}
.socialerror {
    bottom: -30px;
    position: absolute;
}
</style>
<div class="conatiner">
	<div class="row">
		<div class="col-md-6 col-md-offset-3">
			
        <section class="panel">
          <section class="panel-body">
             <?php
             error_reporting(0);
            $path=base_url().'package_images/'.$fetchPackagePic->package_image;
            ?>
		    <div class="profileimg"> 
		      
		      <img src="<?php echo $path;?>" class="img-responsive img-thumbnail img-thumbnail" style="width: 250px; height: 250px;">
		      <a class="editprofileimg" data ="" data-toggle="modal" data-target="#myModal" href="javascript:void(0);"><i class="fa fa-edit"></i></a>
		    </div>
        <a href="<?php echo base_url()?>admin/createPackage" class="btn btn-info pull-right"><i class="fa fa-arrow-left"></i>Complete</a>

		    <br><br>
        </section>
        </section>
		</div>
	</div>
 </div>

 <!--  modal open for change profile picture -->
<form class="form-horizontal bucket-form" id="cropimage"  name="profilePic" ng-submit="submitform()" novalidate>
<input type="hidden" name="package_id" ng-model="brand.package_id">

  <div class="modal fade" id="myModal" role="dialog" style="background-color: rgba(0, 0, 0, 0.5);">
    <div class="modal-dialog">

      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">Change Package Picture</h4>
        </div>
        <div class="modal-body">
         <div class="form-group">

           <div class="col-sm-10">
            <div class="image-editor">
              <input type="file" accept="image*/" ng-model="brand.userfile" id="FileUpload1" class="cropit-image-input"><br>
              <span style="color:#a94442;">Package picture must be minimum width 200px and minimum height 200px.</span>


              <div class="cropit-preview"></div>
              <div class="image-size-label">
                Resize image
              </div>
              <input type="range" class="cropit-image-zoom-input">


              <button class="export">Upload</button>
              <span id="succ" style="color: green;" ></span>
            </div>
            <input type="hidden" class="form-control" id="doc_pic" name="doc_pic" ng-model="brand.doc_pic" required/>


            <span ng-show="submitted && profilePic.doc_pic.$invalid" class="help-block has-error ng-hide warnig">Please upload Package picture</span>
            <span class="help-block has-error ng-hide warnig" style="color: red;" ng-show="doc_picError">{{doc_picError}}</span>
          </div>
        </div>

        <div class="row">
         <div class="col-md-12">
          <button type="submit" ng-click="submitted = true" class="btn btn-info pull-right btn-sm"><strong> Update <i class="fa fa-arrow-right"></i></strong></button>
        </div>
      </div>
    </div>
    <div class="modal-footer">
      <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
    </div>
  </div>

</div>
</div>
</form>

