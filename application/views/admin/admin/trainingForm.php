
<style type="text/css">
.help-block{
color:red;

}

</style>
        

        <!--main content start-->
        <section id="adminsection" class="container">
            <section class="wrapper">
                <!-- page start-->
                <div class="row">
                    <div class="col-md-12">
                        <h4 class="form-heading"><strong>Create New Training</strong></h4>
                        <?php echo $this->session->flashdata('successmsg');?>
                        <?php echo $this->session->flashdata('errormsg');?>
                        <p>Create and add a new Training. <a href="#" class="pull-right">Help <i class="fa fa-question-circle"></i></a></p>
                        <br>
                        <section class="panel">
                            <div class="panel-body">
                                <div class="row">
                                    <div class="col-md-12">
                                        <h4>Create Training</h4>
                                        <hr>
                                    </div>
                                    <form class="form-horizontal bucket-form" ng-submit="submitTrainingForm()" name="trainingForm" novalidate>
                                       <input type="hidden" name="client_id" ng-model="office.client_id"> 
                                    <div class="col-md-6">
                                            <div class="form-group">
                                                <label class="col-sm-3 col-sm-offset-1 control-label">Training provider (Name):</label>
                                                <div class="col-sm-7">
                                                    <input type="text"  class="form-control" name="training_provider" ng-model="training.training_provider" required> 
                                                    <span ng-show="submitted && trainingForm.training_provider.$error.required"  class="help-block has-error ng-hide">Training Provider is required.</span>
                                                     <span ng-show="errorTrainingProvider" class="help-block has-error ng-hide">{{errorTrainingProvider}}</span>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label class="col-sm-3 col-sm-offset-1 control-label">Address:</label>
                                                <div class="col-sm-7">
                                                    <textarea rows="4" cols="4"  class="form-control" name="address" ng-model="training.address" style="resize:none;" required></textarea>
                                                    <span ng-show="submitted && trainingForm.address.$error.required"  class="help-block has-error ng-hide">Address is required.</span>
                                                     <span ng-show="errorAddress" class="help-block has-error ng-hide">{{errorAddress}}</span>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label class="col-sm-3 col-sm-offset-1 control-label">Telephone:</label>
                                                <div class="col-sm-7">
                                                    <input type="text"  class="form-control" name="telephone" ng-model="training.telephone" required>
                                                    <span ng-show="submitted && trainingForm.telephone.$error.required"  class="help-block has-error ng-hide">Telephone is required.</span>
                                                    <span ng-show="errorTelephone" class="help-block has-error ng-hide">{{errorTelephone}}</span>
                                                </div>
                                            </div>


                                            <div class="form-group">
                                                <label class="col-sm-3 col-sm-offset-1 control-label">Email:</label>
                                                <div class="col-sm-7">
                                                    <input type="text"  class="form-control" name="email" ng-model="training.email" required>
                                                    <span ng-show="submitted && trainingForm.email.$error.required"  class="help-block has-error ng-hide">Email is required.</span>
                                                    <span ng-show="errorEmail" class="help-block has-error ng-hide">{{errorEmail}}</span>
                                                </div>
                                            </div>

                                            <div class="form-group">
                                                <label class="col-sm-3 col-sm-offset-1 control-label">FAX:</label>
                                                <div class="col-sm-7">
                                                    <input type="text"  class="form-control" name="fax" ng-model="training.fax" required>
                                                    <span ng-show="submitted && trainingForm.fax.$error.required"  class="help-block has-error ng-hide">FAX is required.</span>
                                                    <span ng-show="errorFax" class="help-block has-error ng-hide">{{errorFax}}</span>
                                                </div>
                                            </div>

                                            
                                        
                                    </div>
                                    <div class="col-md-6">
                                            <div class="form-group">
                                                <label class="col-sm-3 col-sm-offset-1 control-label">Telephone:</label>
                                                <div class="col-sm-7">
                                                     <input type="text"  class="form-control" name="telephone" ng-model="training.telephone" required>
                                                    <span ng-show="submitted && trainingForm.telephone.$error.required"  class="help-block has-error ng-hide">Telephone is required.</span>
                                                     <span ng-show="errorTelephone" class="help-block has-error ng-hide">{{errorTelephone}}</span>
                                                </div>
                                            </div>
                                         
                                            <div class="form-group">
                                                <label class="col-sm-3 col-sm-offset-1 control-label">Client Email:</label>
                                                <div class="col-sm-7">
                                                    <input type="text"  class="form-control" name="client_email" ng-model="training.client_email" required>
                                                     <span ng-show="submitted && trainingForm.client_email.$error.required"  class="help-block has-error ng-hide">Client Email is required.</span>
                                                     <span ng-show="errorClientEmail" class="help-block has-error ng-hide">{{errorClientEmail}}</span>
                                                </div>
                                            </div>


                                            <div class="form-group">
                                                <label class="col-sm-3 col-sm-offset-1 control-label">Client FAX:</label>
                                                <div class="col-sm-7">
                                                    <input type="text"  class="form-control" name="client_fax" ng-model="training.client_fax" required>
                                                     <span ng-show="submitted && trainingForm.client_fax.$error.required"  class="help-block has-error ng-hide">Client FAX is required.</span>
                                                     <span ng-show="errorClientFAX" class="help-block has-error ng-hide">{{errorClientFAX}}</span>
                                                </div>
                                            </div>

                                           
                                             <div class="form-group">
                                                <label class="col-sm-3 col-sm-offset-1 control-label">Mobile:</label>
                                                <div class="col-sm-7">
                                                    <input type="text"  class="form-control" name="client_mobile" ng-model="training.client_mobile" required>
                                                     <span ng-show="submitted && trainingForm.client_mobile.$error.required"  class="help-block has-error ng-hide">Mobile is required.</span>
                                                     <span ng-show="errorMobile" class="help-block has-error ng-hide">{{errorMobile}}</span>
                                                </div>
                                            </div>


                                            <div class="form-group">
                                                <label class="col-sm-3 col-sm-offset-1 control-label">Main Contact:</label>
                                                <div class="col-sm-7">
                                                    <input type="text"  class="form-control" name="main_contact" ng-model="training.main_contact" required>
                                                    <span ng-show="submitted && trainingForm.main_contact.$error.required"  class="help-block has-error ng-hide">Main Contact is required.</span>
                                                    <span ng-show="errorMainContact" class="help-block has-error ng-hide">{{errorMainContact}}</span>
                                                </div>
                                            </div>


                                            <div class="form-group">
                                                <label class="col-sm-3 col-sm-offset-1 control-label">Address:</label>
                                                <div class="col-sm-7">
                                                    <textarea rows="4" cols="4"  class="form-control" name="address" ng-model="training.address" required></textarea>
                                                    <span ng-show="submitted && trainingForm.address.$error.required"  class="help-block has-error ng-hide">Address is required.</span>
                                                    <span ng-show="errorAddress" class="help-block has-error ng-hide">{{errorAddress}}</span>
                                                </div>
                                            </div>
                                            
                                             
                                    </div>

                                     <div class="col-md-12">
                                        <h4>Course Information</h4>
                                        <hr>
                                    </div>

                                    <div class="col-md-6">

                                      <div class="form-group">
                                                <label class="col-sm-3 col-sm-offset-1 control-label">Course Title:</label>
                                                <div class="col-sm-7">
                                                    <input type="text"  class="form-control" name="course_title" ng-model="training.course_title" required>
                                                     <span ng-show="submitted && trainingForm.course_title.$error.required"  class="help-block has-error ng-hide">Course Title is required.</span>
                                                     <span ng-show="errorCourseTitle" class="help-block has-error ng-hide">{{errorCourseTitle}}</span>
                                                </div>
                                            </div>


                                      <div class="form-group">
                                                <label class="col-sm-3 col-sm-offset-1 control-label">Course external reference:</label>
                                                <div class="col-sm-7">
                                                    <input type="text"  class="form-control" name="course_external_reference" ng-model="training.course_external_reference" required>
                                                     <span ng-show="submitted && trainingForm.course_external_reference.$error.required"  class="help-block has-error ng-hide">Course external reference is required.</span>
                                                     <span ng-show="errorCourseExtReference" class="help-block has-error ng-hide">{{errorCourseExtReference}}</span>
                                                </div>
                                            </div>  


                                      <div class="form-group">
                                                <label class="col-sm-3 col-sm-offset-1 control-label">Course internal reference:</label>
                                                <div class="col-sm-7">
                                                    <input type="text"  class="form-control" name="course_internal_reference" ng-model="training.course_internal_reference" required>
                                                     <span ng-show="submitted && trainingForm.course_internal_reference.$error.required"  class="help-block has-error ng-hide">Course internal reference is required.</span>
                                                     <span ng-show="errorCourseIntReference" class="help-block has-error ng-hide">{{errorCourseIntReference}}</span>
                                                </div>
                                            </div>  


                                      <div class="form-group">
                                                <label class="col-sm-3 col-sm-offset-1 control-label">Course Image:</label>
                                                <div class="col-sm-7">
                                                    <input type="text"  class="form-control" name="course_image" ng-model="training.course_image" required>
                                                     <span ng-show="submitted && trainingForm.course_image.$error.required"  class="help-block has-error ng-hide">Course Image is required.</span>
                                                     <span ng-show="errorCourseImage" class="help-block has-error ng-hide">{{errorCourseImage}}</span>
                                                </div>
                                            </div> 


                                    <div class="form-group">
                                                <label class="col-sm-3 col-sm-offset-1 control-label">Online Cost:</label>
                                                <div class="col-sm-7">
                                                    <input type="text"  class="form-control" name="online_cost" ng-model="training.online_cost" required>
                                                     <span ng-show="submitted && trainingForm.online_cost.$error.required"  class="help-block has-error ng-hide">Online Cost is required.</span>
                                                     <span ng-show="errorOnlineCost" class="help-block has-error ng-hide">{{errorOnlineCost}}</span>
                                                </div>
                                            </div> 


                                    <div class="form-group">
                                                <label class="col-sm-3 col-sm-offset-1 control-label">Offline Cost:</label>
                                                <div class="col-sm-7">
                                                    <input type="text"  class="form-control" name="offline_cost" ng-model="training.offline_cost" required>
                                                     <span ng-show="submitted && trainingForm.offline_cost.$error.required"  class="help-block has-error ng-hide">Offline Cost is required.</span>
                                                     <span ng-show="errorOfflineCost" class="help-block has-error ng-hide">{{errorOfflineCost}}</span>
                                                </div>
                                            </div>                                 


                                    </div>

                                  <div class="col-md-6">

                                  <div class="form-group">
                                                <label class="col-sm-3 col-sm-offset-1 control-label">Course Level:</label>
                                                <div class="col-sm-7">
                                                    <input type="text"  class="form-control" name="course_level" ng-model="training.course_level" required>
                                                     <span ng-show="submitted && trainingForm.course_level.$error.required"  class="help-block has-error ng-hide">Course Level is required.</span>
                                                     <span ng-show="errorCourseLevel" class="help-block has-error ng-hide">{{errorCourseLevel}}</span>
                                                </div>
                                            </div> 



                                 <div class="form-group">
                                                <label class="col-sm-3 col-sm-offset-1 control-label">Frequency:</label>
                                                <div class="col-sm-7">
                                                    <input type="text"  class="form-control" name="frequency" ng-model="training.frequency" required>
                                                     <span ng-show="submitted && trainingForm.frequency.$error.required"  class="help-block has-error ng-hide">Frequency is required.</span>
                                                     <span ng-show="errorFrequency" class="help-block has-error ng-hide">{{errorFrequency}}</span>
                                                </div>
                                            </div> 


                                <div class="form-group">
                                                <label class="col-sm-3 col-sm-offset-1 control-label">Duration:</label>
                                                <div class="col-sm-7">
                                                    <input type="text"  class="form-control" name="duration" ng-model="training.duration" required>
                                                     <span ng-show="submitted && trainingForm.duration.$error.required"  class="help-block has-error ng-hide">Duration is required.</span>
                                                     <span ng-show="errorDuration" class="help-block has-error ng-hide">{{errorDuration}}</span>
                                                </div>
                                            </div> 


                               <div class="form-group">
                                                <label class="col-sm-3 col-sm-offset-1 control-label">Entry Requirement:</label>
                                                <div class="col-sm-7">
                                                    <input type="text"  class="form-control" name="entity_req" ng-model="training.entity_req" required>
                                                     <span ng-show="submitted && trainingForm.entity_req.$error.required"  class="help-block has-error ng-hide">Entry Requirement is required.</span>
                                                     <span ng-show="errorEntityReq" class="help-block has-error ng-hide">{{errorEntityReq}}</span>
                                                </div>
                                            </div>                                          





                                  </div>

                                   
                   
                                   
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <div class="col-sm-12">
                                                <button type="submit" class="btn btn-info pull-right btn-sm" ng-click="submitted = true"><strong><i class="fa fa-plus"></i> Create Training</strong></button><span class="pull-right"> &nbsp; &nbsp; </span>

                                                <button type="button" class="btn btn-danger pull-right btn-sm"><strong><i class="fa fa-times"></i> Cancel</strong></button>
                                            </div>
                                        </div>
                                    </div>
                                    </form>
                                </div>
                            </div>
                        </section>
                    </div>
                </div>
                <!-- page end-->

            </section>
        </section>
        <!--main content end-->





    
</div>


<!-- 
<script type="text/javascript" src="js/ckeditor/ckeditor.js"></script> -->