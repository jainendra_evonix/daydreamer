<title>Import bulk data insert in database using php codeigniter </title>
<style>
table {
    width:100%;
}
table, th, td {
    border: 1px solid black;
    border-collapse: collapse;
}
th, td {
    padding: 5px;
    text-align: left;
}
table#t01 tr:nth-child(even) {
    background-color: #eee;
}
table#t01 tr:nth-child(odd) {
   background-color:#fff;
}
table#t01 th {
    background-color: black;
    color: white;
}
.error{

  color: red;
}
</style>
<?php if($this->session->flashdata('message')){?>
          <div align="center" class="alert alert-success">      
            <?php echo $this->session->flashdata('message')?>
          </div>
        <?php } ?>

<br><br>

<div>
  <!-- <h2>Upload CSV</h2> -->
   <div class="row">
      <div class="col-md-4 col-md-offset-4 col-sm-6 col-sm-offset-3">
          <section class="panel">
            <section class="panel-body">
                  <h3><strong>Upload CSV</strong></h3>
                  <hr>
                  <form id="candidateCsvForm" action="<?php echo base_url(); ?>admin/importCSV" class="" method="post" name="upload_excel" enctype="multipart/form-data">
                  <input  type="file" name="file" id="file">
                  <br><br>
                  <button type="submit" id="submit" name="import" class="btn btn-primary button-loading"><i class="fa fa-upload"></i> Import</button>
                  <br><br>
                  </form>
            </section>
          </section>
      <br><br>
      </div>
</div>
</div>
