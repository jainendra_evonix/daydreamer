
        <!--main content start-->
        <section id="adminsection" class="container">
            <section class="wrapper">
                <!-- page start-->
                <div class="row">
                   
                    <div class="col-md-12">
                        
                        <h4 class="form-heading"><strong>All Group Interviews</strong></h4>
                        <?php echo $this->session->flashdata('successmsg');?>
                        <?php echo $this->session->flashdata('errormsg');?>
                        <a href="<?php echo base_url();?>admin/createGroupInterview" class="btn btn-info pull-right" style="margin-top:-36px;"><i class="fa fa-arrow-left"></i>Create Group</a>

                        <br>
                        <section class="panel">
                            <div class="panel-body">
                                <div class="row">
                                    <!-- <div class="col-md-4">
                                        <select class="form-control">
                                            <option value="In-progress" selected="true">All Job Openings</option>
                                            <option value="Waiting for approval">Closing Next Month</option>
                                            <option value="On-Hold">Closing This Month</option>
                                            <option value="Filled">Hot Job Openings</option>
                                            <option value="Cancelled">My Job Openings</option>
                                            <option value="Declined">New Last Week</option>
                                            <option value="Inactive">New This Week</option>
                                        </select>
                                    </div> -->
                                    <div class="col-md-8">
                                        <!-- <button type="submit" class="btn btn-info pull-right btn-sm"><strong><i class="fa fa-plus"></i> New Job Opening</strong></button><span class="pull-right"> &nbsp; &nbsp; </span> -->

                                        <!-- <button type="button" class="btn btn-info pull-right btn-sm"><strong><i class="fa fa-times"></i> Cancel</strong></button> -->
                                        <!-- <br> -->
                                        <!-- <hr> -->
                                    </div>
                                    <div class="col-md-12">
                                        <section class="">
                                      
                                <div class="table-responsive" style="border: 1px solid #ccc;padding: 6px;">
                                
                                <table id="employee-grid"  cellpadding="0" cellspacing="0" border="0" class="display" width="100%">
                                    <thead>
                                        <tr>
                                            <th>Sr.No</th>
                                            <th>Group Name</th>
                                            <th>Job Title</th> 
                                            <th>Client Name</th>
                                            <th>Created Date</th>
                                            <th>Action</th>
                                        </tr>
                                    </thead>
                                </table>
                            </div><!--end .table-responsive -->
                                        </section>
                                    </div>
                                </div>
                            </div>
                        </section>
                    </div>
                </div>
                <!-- page end-->

            </section>
        </section>
        <!--main content end-->






    
</div>