<!--main content start-->
    <section id="" class="container">
        <section class="wrapper">
        <!-- page start-->
        <div class="row">
            <div class="col-md-12">
                <h4 class="form-heading"><strong>Calendar</strong></h4>
                
                <p><strong>July 3, 2017</strong> <a href="#" class="pull-right">Help <i class="fa fa-question-circle"></i></a></p>

                <br>
                <section class="panel">
                    <div class="panel-body">
                        <!-- page start-->
                        <div class="row">
                            <aside class="col-sm-12">
                                  <div id="interview_calendar" class="has-toolbar"></div>
                            </aside>
                            <aside class="col-sm-12">
                               
                            </aside>
                        </div>
                        <!-- page end-->
                    </div>
                </section>
                </div>
            </div>
        <!-- page end-->
        </section>
    </section>
    <!--main content end-->