
<style type="text/css">
    .mylabel.label-success {
        background-color: #5cb85c !important;
    }
</style>

<link rel="stylesheet" href="js/data-tables/DT_bootstrap.css" />

<!--main content start-->
<section class="panel container">
<section class="panel-body">
  <div class="row">
    <div class="col-md-12">
      <form class="form-inline" method="post" action="<?php echo base_url();?>admin/viewAllCandidates">
  <div class="form-group">
    <label for="email">Candidate First Name:</label>
    <input type="text" name="candidate_first_name" class="form-control" id="candidate_first_name" style="width: 155px; margin-right: 5px;">
  </div>
  <div class="form-group">
    <label for="pwd">Candidate Last Name:</label>
    <input type="text" name="candidate_last_name" class="form-control" id="country" style="width: 155px; margin-right: 5px;">
  </div>

  <div class="form-group">
    <label for="pwd">City:</label>
    <input type="text" name="city" class="form-control" id="city" style="width: 155px; margin-right: 5px;">
  </div>
   <div class="form-group">
    <label for="pwd">Email:</label>
    <input type="text" name="email" class="form-control" id="email" style="width: 155px; margin-right: 5px;">
    <br>
  </div>
  <button type="submit" class="btn btn-info">Submit</button>
</form> 

    </div>

  </div>
</section>
</section>
<section id="adminsection" class="container">
    <section class="wrapper">
        <!-- page start-->
        <div class="row">
            <div class="col-md-12">
                <h4 class="form-heading"><strong>All Candidates</strong></h4>
                <?php echo $this->session->userdata('successmsg');?>
                 <?php echo $this->session->userdata('errormsg');?>
                <p>All Candidates information list. <a href="#" class="pull-right">Help <i class="fa fa-question-circle"></i></a></p>

                <br>
                <section class="panel">
                    <div class="panel-body">
                        <div class="row">
                            <div class="col-md-12">
                                <!-- <a><button type="submit" class="btn btn-info pull-right btn-sm" style="margin-bottom:18px;"><strong><i class="fa fa-plus"></i> Create New Office</strong></button><span class="pull-right"> &nbsp; &nbsp; </span></a> -->

                                <!-- <button type="button" class="btn btn-info pull-right btn-sm"><strong><i class="fa fa-times"></i> Cancel</strong></button> -->
                                <!-- <br> -->
                                <!-- <hr> -->
                            </div>
                            <div class="col-md-12">
                                <section class="">
                                    <div class="table-responsive" style="border: 1px solid #ccc;padding: 6px;">
                                        <table  id="employee-grid"  cellpadding="0" cellspacing="0" border="0" class="display" width="100%">
                                            <thead>
                                                <tr>
                                                    <th>Sr.No</th>
                                                    <th>Candidate Name</th>
                                                    <th>City</th>
                                                    <th>Email</th>
                                                    <th>Profile Status</th>
                                                    <!-- <th>Job Applied</th> -->
                                                    <th>Action</th>

                                                </tr>
                                            </thead>
                                           
                                            </table>
                                        </div>
                                    </section>
                                </div>
                            </div>
                        </div>
                    </section>
                </div>
            </div>
            <!-- page end-->

        </section>
    </section>
    <!--main content end-->





    
</div>




<!--dynamic table initialization -->
