
<style type="text/css">
.help-block{
  color:red;

}
.profileimg {
  width:250px;
  height:250px;
  position: relative;
  display: block;
  margin: auto;
}
.editprofileimg {
  position: absolute;
  bottom: 46px;
  right: 46px;
  width: 40px;
  height: 40px;
  background-color: rgba(0, 0, 0, 0.7);
  color: #ccc;
  padding: 7px;
  font-size: 18px;
  text-align: center;
  border-radius: 4px;
}
.editprofileimg:hover {
  color: #fff;
  background-color: rgba(0, 0, 0, 1);
}

.cropit-preview 

{ 
  background-color: #f8f8f8; 
  background-size: cover;
  border: 1px solid #ccc;
  border-radius: 3px; 
  margin-top: 7px;
  width: 202px;
  height: 202px;

}
.cropit-preview-image-container 
{ 
  cursor: move; 
} 
.image-size-label 
{ 
  margin-top: 10px; 
} 
input,.export
{
 display: block; 
} 
button 
{ 
  margin-top: 10px; 
} 
.req{ 
  color:red; 
} 


.socialfgroup {
  position: relative;
}
.socialerror {
    bottom: -30px;
    position: absolute;
}
</style>

        <!--main content start-->
        <section id="adminsection" class="container">
            <section class="wrapper">
                <!-- page start-->
                <div class="row">
                    <div class="col-md-12">
                        <h4 class="form-heading"><strong>Create New Package</strong></h4>
<a href="<?php echo base_url();?>admin/createPackage" class="btn btn-success" style="float: right; margin-left: 17px; margin-top: -6px;"><i class="fa fa-arrow-left" aria-hidden="true"></i> 
Back</a>
                        <?php echo $this->session->flashdata('successmsg');?>
                        <?php echo $this->session->flashdata('errormsg');?>
                        <p>Create and add a new Package. <a href="#" class="pull-right">Help <i class="fa fa-question-circle"></i></a></p>
                        <br>
                        <section class="panel">
                            <div class="panel-body">
                                <div class="row">
                                    <form class="form-horizontal bucket-form" ng-submit="submitJobAdvForm()" name="jobAdvForm" novalidate>
                                       <input type="hidden" id="package_id" name="package_id" ng-model="adds.package_id" value="<?php echo $package_id;?>"> 
                                    <div class="col-md-6">
                                            <h4>Package Information</h4>
                                            <hr>
                                            <div class="form-group">
                                                <label class="col-sm-4 control-label">Package Title:</label>
                                                <div class="col-sm-7">
                                                    <input type="text"  class="form-control" name="package_title" ng-model="adds.package_title" required> 
                                                    <span ng-show="submitted && jobAdvForm.package_title.$error.required"  class="help-block has-error ng-hide">Package Title is required.</span>
                                                     <span ng-show="errorPackageTitle" class="help-block has-error ng-hide">{{errorPackageTitle}}</span>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label class="col-sm-4 control-label">Custom Reference Number:</label>
                                                <div class="col-sm-7">
                                                    <input type="text"  class="form-control" name="reference_number" ng-model="adds.reference_number" required>
                                                    <span ng-show="submitted && jobAdvForm.reference_number.$error.required"  class="help-block has-error ng-hide">Enter a reference number</span>
                                                     <span ng-show="errorReferenceNumber" class="help-block has-error ng-hide">{{errorReferenceNumber}}</span>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label class="col-sm-4 control-label">Description:</label>
                                                <div class="col-sm-7">
                                                    <textarea class="form-control" rows="3" name="description" ng-model="adds.description" required></textarea>
                                                    <span ng-show="submitted && jobAdvForm.description.$error.required"  class="help-block has-error ng-hide">Enter package description</span>
                                                    <span ng-show="errorDescription" class="help-block has-error ng-hide">{{errorDescription}}</span>
                                                </div>
                                            </div>
                                      <!-- <div class="form-group">
                                        <label class="control-label col-sm-3 col-sm-offset-1 text-right">Upload Image:</label>
                                        <div class="controls col-sm-7">
                                           

                                           <input id="uploadFile" class="default ng-pristine ng-valid ng-empty ng-touched" file-model="myFile" ng-model="adds.attached_file" name="attached_file" ng-files="getTheFiles($files)" type="file">
                                           <span ng-show="submitted && jobAdvForm.attached_file.$error.required"  class="help-block has-error ng-hide">Image is required.</span>
                                          <span ng-show="errorFile" class="help-block has-error ng-hide">{{errorFile}}</span><br/>
                                          <span ng-bind="adds.attached_file"></span>
                                       </div> -->
                                       <!--  <div class="profileimg">
                                        <img src="" class="img-responsive img-thumbnail img-thumbnail">
                                        <a class="editprofileimg" data ="" data-toggle="modal" data-target="#myModal" href="javascript:void(0);"><i class="fa fa-edit"></i></a>


                                         </div> -->
                                  <!--  </div> -->
                                            
                                           <!--  <h4>Additional Services</h4>
                                            <hr> -->
                                            <?php

                                               //foreach($fetchService as $key){

                                            ?>
                                           <!--  <div class="form-group" style="padding-bottom: 0;">
                                                <label class="col-sm-4 text-right"><?php //echo $key->service_name;?>:</label>
                                                <div class="col-sm-5 icheck">
                                                    <div class="square single-row"> -->
                                                        <!-- <div class="">
                                                            <input tabindex="3" type="radio" name="<n?php  //echo 'name'.$key->id; ?>" ng-model="<?php //echo 'adds'.'.'.'name'.$key->id; ?>"  value=<?php //echo $key->service_name;?>>
                                                            <label>Yes </label>
                                                        </div>  -->
                                                         <!-- <div class="">
                                                            <input tabindex="3" type="radio" name="<?php  //echo 'name'.$key->id; ?>">
                                                            <label>No </label>
                                                        </div> -->

                                                       <!--  <input type="checkbox" class="custom-control-input messageCheckbox" name="service" ng-model="adds.service"> -->
                                                        <!-- <span class="custom-control-indicator"></span> -->
                                            <!-- <input type="checkbox" id="service" name="<?php //echo 'service'.$key->id;?>"  value=<?php echo $key->id;?> <?php echo isset($key->id) && in_array($key->id, $service) ? 'checked="checked"':''; ?>>
                                                    </div>
                                                </div>
                                                <div class="col-sm-3">
                                                    <a href="<?php echo base_url();?>admin/deleteService?id=<?php //echo $key->id;?>&&package_id=<?php echo $package_id;?>" onclick="return confirmMsg();"><i class="fa fa-trash"></i> Delete</a>
                                                </div>
                                            </div> -->
                                            <?php// }?>
                                           <!--  <div class="form-group" style="padding-bottom: 0;">
                                                <label class="col-sm-4 text-right">E-mail Marketing:</label>
                                                <div class="col-sm-5 icheck">
                                                    <div class="square single-row">
                                                        <div class="">
                                                            <input tabindex="3" type="radio" name="demo-radio2">
                                                            <label>Yes </label>
                                                        </div>
                                                        <div class="">
                                                            <input tabindex="3" type="radio" name="demo-radio2">
                                                            <label>No </label>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-sm-3">
                                                    <a href="" class=""><i class="fa fa-trash"></i> Delete</a>
                                                </div>
                                            </div> -->
                                           <!--  <div class="form-group" style="padding-bottom: 0;">
                                                <label class="col-sm-4 text-right">View/Save CV's:</label>
                                                <div class="col-sm-5 icheck">
                                                    <div class="square single-row">
                                                        <div class="">
                                                            <input tabindex="3" type="radio" name="demo-radio3">
                                                            <label>Yes </label>
                                                        </div>
                                                        <div class="">
                                                            <input tabindex="3" type="radio" name="demo-radio3">
                                                            <label>No </label>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-sm-3">
                                                    <a href="" class=""><i class="fa fa-trash"></i> Delete</a>
                                                </div>
                                            </div> -->
                                         <!--    <div class="form-group" style="padding-bottom: 0;">
                                                <label class="col-sm-4 text-right">Candidate Match:</label>
                                                <div class="col-sm-5 icheck">
                                                    <div class="square single-row">
                                                        <div class="">
                                                            <input tabindex="3" type="radio" name="demo-radio4">
                                                            <label>Yes </label>
                                                        </div>
                                                        <div class="">
                                                            <input tabindex="3" type="radio" name="demo-radio4">
                                                            <label>No </label>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-sm-3">
                                                    <a href="" class=""><i class="fa fa-trash"></i> Delete</a>
                                                </div>
                                            </div> -->
                                           <!--  <div class="form-group">
                                                <div class="col-sm-8 col-sm-offset-1">
                                                    <input type="text" class="form-control"  id="service_name"  placeholder="Enter service name" onkeyup="hideMsg();">
                                                    <span id="msg" style="color:red;"></span>
                                                </div>
                                                <div class="col-sm-2">
                                                    <button class="btn btn-success btn-sm pull-right" onclick="saveService();"><strong>Add <i class="fa fa-plus"></i></strong></button>
                                                </div>
                                            </div> -->
                                            
                                            
                                            
                                            <hr>
                                            
                                            <div class="form-group">
                                                <label class="col-sm-3 control-label">Price:</label>
                                                <div class="col-sm-3">
                                                <label class="control-label">Currency:</label>
                                                    <input type="text" name="currency" ng-model="adds.currency" class="form-control" id="" placeholder="" required>
                                                    <span ng-show="submitted && jobAdvForm.currency.$error.required"  class="help-block has-error ng-hide">Please enter Currency</span>
                                                </div>
                                                <div class="col-sm-3">
                                                <label class="control-label">Amount:</label>
                                                    <input type="text" name="amount" ng-model="adds.amount" class="form-control" id="" placeholder="" required>
                                                    <span ng-show="submitted && jobAdvForm.amount.$error.required"  class="help-block has-error ng-hide">Please enter Amount</span>
                                                </div>
                                               <!--  <div class="col-sm-2">
                                                <label class="control-label">Strikeout:</label>
                                                    <input type="checkbox">
                                                </div> -->
                                            </div>
                                            <div class="form-group">
                                                <label class="col-sm-3 control-label">Offer Price:</label>
                                                <div class="col-sm-3">
                                                <label class="control-label">Currency:</label>
                                                    <input type="text" name="offer_currency" ng-model="adds.offer_currency" class="form-control" id="" placeholder=""required>
                                                     <span ng-show="submitted && jobAdvForm.offer_currency.$error.required"  class="help-block has-error ng-hide">Please enter Offer Currency</span>
                                                </div>
                                                <div class="col-sm-3">
                                                <label class="control-label">Amount:</label>
                                                    <input type="text" name="offer_amount" ng-model="adds.offer_amount" class="form-control" id="" placeholder="" required>
                                                    <span ng-show="submitted && jobAdvForm.offer_amount.$error.required"  class="help-block has-error ng-hide">Please enter Offer Amount</span>
                                                </div>
                                                <!-- <div class="col-sm-2">
                                                <label class="control-label">Strikeout:</label>
                                                    <input type="checkbox">
                                                </div> -->
                                            </div>
                                            <div class="form-group" style="padding-bottom: 0;">
                                                <label class="col-sm-3 text-right">Publish:</label>
                                                <div class="col-sm-5 icheck">
                                                    <div class="square single-row">
                                                        <div class="">
                                                            <input tabindex="3" type="radio" name="radpub" value="yes" ng-model="adds.radpub" required>

                                                            <label>Yes </label>
                                                        </div>
                                                        <div class="">
                                                            <input tabindex="3" type="radio" name="radpub" value="no" ng-model="adds.radpub" required>
                                                            <label>No </label>
                                                        </div>
                                                    </div>
                                                    <span ng-show="submitted && jobAdvForm.radpub.$error.required"  class="help-block has-error ng-hide">Publish is required</span>
                                                </div>
                                            </div>
                                    </div>
                                    
                                    <div class="col-md-6">
                                            
                                 
                                             <h4></h4>
                                             <br>
                                            <hr>
                                            
                                            <div class="form-group">
                                                <label class="col-sm-3 control-label">Package End Date:</label>
                                                <div class="col-sm-8 icheck">
                                                    <div class="square single-row">
                                                        <div class="">
                                                    <input tabindex="3" type="radio"  name="radio" value="yes" ng-model="adds.radio" onclick="showDiv();" required>

                                                    <label>Yes </label>
                                                        </div>
                                                        <div class="">
                                                             <input tabindex="3" type="radio"  name="radio" value="no" ng-model="adds.radio" onclick="hideDiv();" required>
                                                    <label>No </label>
                                                        </div>
                                                    </div>
                                                     <span ng-show="submitted && jobAdvForm.radio.$error.required"  class="help-block has-error ng-hide">End Date is required</span>
                                                </div>
                                            </div>
                                            <!-- <div class="form-group" id="date-div"> -->
                                                <!-- <div class="col-sm-2 col-sm-offset-3 padingr">
                                                    <select class="form-control m-bot15" name="days" ng-model="adds.days" required>
                                                       <option disabled="disabled" selected="selected" value="">Day</option>
                                                        <?php
                                                           foreach($fetchDays as $key){
                                                        ?>
                                                        <option value="<?php echo $key->id;?>"><?php echo $key->day;?></option>

                                                        <?php }?>
                                                    </select>
                                                  <span ng-show="submitted && jobAdvForm.days.$error.required"  class="help-block has-error ng-hide">Day is required</span>

                                                </div> -->
                                                <!-- <div class="col-sm-3 padingr" >
                                                    <select class="form-control m-bot15" name="months" ng-model="adds.months" required>
                                                       <option disabled="disabled" selected="selected" value="">Month</option>
                                                        <option value="January">January</option>
                                                        <option value="February">February</option>
                                                        <option value="March">March</option>
                                                        <option value="April">April</option>
                                                        <option value="May">May</option>
                                                        <option value="June">June</option>
                                                        <option value="July">July</option>
                                                        <option value="August">August</option>
                                                        <option value="September">September</option>
                                                        <option value="October">October</option>
                                                        <option value="November">November</option>
                                                        <option value="December">December</option>
                                                    </select>
                                                     <span ng-show="submitted && jobAdvForm.months.$error.required"  class="help-block has-error ng-hide">Month is required</span>
                                                </div> -->
                                               <!--  <div class="col-sm-3 padingr">
                                                    <select class="form-control m-bot15" style="padding-right: 0;" name="years" ng-model="adds.years" required>
                                                        <option disabled="disabled" selected="selected" value="">Year</option>
                                                        <?php
                                                         foreach($fetchYears as $key){

                                                        ?>
                                                        <option value="<?php echo $key->id;?>"><?php echo $key->year;?></option>
                                                        <?php }?>
                                                    </select>
                                                    <span ng-show="submitted && jobAdvForm.years.$error.required"  class="help-block has-error ng-hide">Year is required</span>
                                                </div> -->

                                            <!-- </div> -->

                                        <div class="form-group">
                                       <label class="col-sm-3  control-label">Validity Date:</label>
                                       <div class="col-sm-7">
                                        <div class="input-group date" id="demo-date" style="width:100%">
                                          <div class="input-group-content">
                                            <input data-calenderopen type="text" class="form-control" autocomplete="off"  ng-model="adds.validity_date" name="validity_date" placeholder="Please enter Opened Date"  id="open_date"   style="margin-bottom:0px;" onchange="hideopen();">
                                            <span id="msg1" ng-show="submitted && jobAdvForm.validity_date.$invalid" class="help-block has-error">Validity Date is required.</span>
                                            <!-- <span ng-show="errorValidity" class="help-block has-error ng-hide">{{errorValidity}}</span> -->
                                            <span ng-show="errorValidity" class="help-block has-error ng-hide">{{errorValidity}}</span>
                                          </div>
                                          <!-- <a href="javascript:void(0);" class="input-group-addon date"><i class="fa fa-calendar"></i></a> -->
                                        </div>
                                      </div>
                                    </div>
                                        
                                        <h4>Social Media</h4>
                                            <hr>
                                            
                                            <div class="form-group" style="padding-bottom: 0;">
                                                <label class="col-sm-3 text-right">Facebook:</label>
                                                <div class="col-sm-5 icheck">
                                                    <div class="square single-row">
                                                        <div class="">
                                                            <input tabindex="3" type="radio"  name="radio1" value="yes" ng-model="adds.radio1">
                                                    <label>Yes </label>
                                                        </div>
                                                        <div class="">
                                                            <input tabindex="3" type="radio"  name="radio1" value="no" ng-model="adds.radio1">
                                                            <label>No </label>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="form-group" style="padding-bottom: 0;">
                                                <label class="col-sm-3 text-right">Twitter:</label>
                                                <div class="col-sm-5 icheck">
                                                    <div class="square single-row">
                                                        <div class="">
                                                            <input tabindex="3" type="radio" name="radio2" value="yes" ng-model="adds.radio2">
                                                            <label>Yes </label>
                                                        </div>
                                                        <div class="">
                                                            <input tabindex="3" type="radio" name="radio2" value="no" ng-model="adds.radio2">
                                                            <label>No </label>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="form-group" style="padding-bottom: 0;">
                                                <label class="col-sm-3 text-right">Linkedin:</label>
                                                <div class="col-sm-5 icheck">
                                                    <div class="square single-row">
                                                        <div class="">
                                                            <input tabindex="3" type="radio" name="radio3" value="yes" ng-model="adds.radio3">
                                                            <label>Yes </label>
                                                        </div>
                                                        <div class="">
                                                            <input tabindex="3" type="radio" name="radio3" value="no" ng-model="adds.radio3">
                                                            <label>No </label>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                    </div>

                                    <!-- <div class="col-md-6">

                                       <div class="profileimg">
                                       
                                          <img src="" class="img-responsive img-thumbnail img-thumbnail">
                                          <a class="editprofileimg" data ="" data-toggle="modal" data-target="#myModal" href="javascript:void(0);"><i class="fa fa-edit"></i></a>
                                        </div>  
                                      
                                    </div> -->
                                    <div class="clearfix"></div>
                                    <br><br>
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <div class="col-sm-7 col-sm-offset-4">
                                                <button type="submit" class="btn btn-info btn-sm pull-right" ng-click="submitted = true" id="btn"><strong><i class="fa fa-plus"></i> Save &amp; Continue <i class="fa fa-"></i></i></strong></button><span class="pull-right"> &nbsp; &nbsp; </span>
                                                <button type="button" class="btn btn-danger btn-sm pull-right"><strong><i class="fa fa-times"></i> Cancel</strong></button>
                                            </div>
                                        </div>
                                    </div>
                                    </form>
                                </div>
                            </div>
                        </section>
                    </div>
                </div>
                <!-- page end-->

            </section>
        </section>
        <!--main content end-->





    
</div>

<!--  modal open for change profile picture -->
<form class="form-horizontal bucket-form" id="cropimage"  name="profilePic" ng-submit="submitform()" novalidate>


  <div class="modal fade" id="myModal" role="dialog" style="background-color: rgba(0, 0, 0, 0.5);">
    <div class="modal-dialog">

      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">Change Package Image</h4>
        </div>
        <div class="modal-body">
         <div class="form-group">

           <div class="col-sm-10">
            <div class="image-editor">
              <input type="file" accept="image*/" ng-model="brand.userfile" id="FileUpload1" class="cropit-image-input"><br>
              <span style="color:#a94442;">Package picture must be minimum width 200px and minimum height 200px.</span>


              <div class="cropit-preview"></div>
              <div class="image-size-label">
                Resize image
              </div>
              <input type="range" class="cropit-image-zoom-input">


              <button class="export">Upload</button>
              <span id="succ" style="color: green;" ></span>
            </div>
            <input type="hidden" class="form-control" id="doc_pic" name="doc_pic" ng-model="brand.doc_pic" required/>


            <span ng-show="submitted && profilePic.doc_pic.$invalid" class="help-block has-error ng-hide warnig">Please upload profile picture</span>
            <span class="help-block has-error ng-hide warnig" style="color: red;" ng-show="doc_picError">{{doc_picError}}</span>
          </div>
        </div>

        <div class="row">
         <div class="col-md-12">
          <button type="submit" ng-click="submitted = true" class="btn btn-info pull-right btn-sm"><strong> Update <i class="fa fa-arrow-right"></i></strong></button>
        </div>
      </div>
    </div>
    <div class="modal-footer">
      <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
    </div>
  </div>

</div>
</div>
</form>


<!-- 
<script type="text/javascript" src="js/ckeditor/ckeditor.js"></script> -->