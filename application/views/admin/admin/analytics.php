
<?php
//print_r($month_array);exit;
$temp=array();
if(!empty($month_array) && !empty($user_array)){

	foreach ($month_array as $key => $value) {
		if(!empty($user_array[$key])){
			$temp[]=array(
				'label'=>$value,
				'y'=>(int)$user_array[$key]->userid

				);

		}else{
			$temp[]=array(
				'label'=>$value,
				'y'=>(int)''

				);

		}
	}

}
$temp=json_encode($temp);




$temp1=array();
if(!empty($month_array) && !empty($client_array)){

	foreach ($month_array as $key => $value) {
		if(!empty($client_array[$key])){
			$temp1[]=array(
				'label'=>$value,
				'y'=>(int)$client_array[$key]->clientid

				);

		}else{
			$temp1[]=array(
				'label'=>$value,
				'y'=>(int)''

				);

		}
	}

}
$temp1=json_encode($temp1);







?>
<script type="text/javascript">
window.onload = function () {

	var dps = <?php echo $temp ?>;
	var chart = new CanvasJS.Chart("chartContainer", {

    theme: "theme2",//theme1
    title:{
    	text: "Number of Users"              
    },
    animationEnabled: true,   // change to true
    data: [              
    {
      // Change type to "bar", "area", "spline", "pie",etc.
      type: "column",
      dataPoints: dps
  }
  ]

});
	chart.render();

	var dps1 = <?php echo $temp1 ?>;

	var chart1 = new CanvasJS.Chart("chartContainer1", {

    theme: "theme2",//theme1
    title:{
    	text: "Number of Clients"              
    },
    animationEnabled: true,   // change to true
    data: [              
    {
      // Change type to "bar", "area", "spline", "pie",etc.
      type: "column",
      dataPoints: dps1
  }
  ]

});
	chart1.render();

	var chart2 = new CanvasJS.Chart("chartContainer2",
	{
		title:{
			text: "Analytics"
		},
		legend: {
			maxWidth: 350,
			itemWidth: 120
		},
		data: [
		{
			type: "pie",
			showInLegend: true,
			legendText: "{indexLabel}",
			dataPoints: <?php echo json_encode($pie_data);?>
		}
		]
	});
	chart2.render();


}
</script>


<div class="container">
	<section id="adminsection" class="container">
	<section class="wrapper">
		<!-- <h2>Analytics</h2> -->
		<h4 class="form-heading">Welcome, <?php echo $firstname.' '.$lastname?></h4></br>
	<div class="row">

		<div class="col-md-3">

			<section class="panel panelhover" style="box-shadow:4px 4px 15px rgba(136, 136, 136, 0.2);">
				<div class="panel-body">
					<span class="mini-stat-icon orange"><i class="fa fa-group"></i></span>
					<div class="clearfix"></div>
					<div class="top-stats-panel">
						<div class="gauge-canvas">
							<h4 class="widget-h"><a href="<?php echo base_url();?>admin/viewAllOffices">Total Clients</a></h4>
							<!-- <p>You can create new job opening by clicking the create icon.</p> -->
						</div>
						<ul class="gauge-meta clearfix">
							<li><strong class="text-xl" style="font-size:25px;padding-left: 100px;"><?php echo $countOffice;?></strong></li>
						</ul>
					</div>
				</div>
			</section>
		</div><!--end .col -->

		<div class="col-md-3">

			<section class="panel panelhover" style="box-shadow:4px 4px 15px rgba(136, 136, 136, 0.2);">
				<div class="panel-body">
					<span class="mini-stat-icon orange"><i class="fa fa-briefcase"></i></span>
					<div class="clearfix"></div>
					<div class="top-stats-panel">
						<div class="gauge-canvas">
							<h4 class="widget-h"><a href="<?php echo base_url();?>admin/createPackage">Total Packages</a></h4>
							<!-- <p>You can create new job opening by clicking the create icon.</p> -->
						</div>
						<ul class="gauge-meta clearfix">
							<li><strong class="text-xl" style="font-size:25px;padding-left: 100px;"><?php echo $countPackage;?></strong></li>
						</ul>
					</div>
				</div>
			</section>
		</div><!--end .col -->

		<div class="col-md-3">

			<section class="panel panelhover" style="box-shadow:4px 4px 15px rgba(136, 136, 136, 0.2);">
				<div class="panel-body">
					<span class="mini-stat-icon orange"><i class="fa fa-group"></i></span>
					<div class="clearfix"></div>
					<div class="top-stats-panel">
						<div class="gauge-canvas">
							<h4 class="widget-h"><a href="<?php echo base_url();?>admin/viewAllCandidates">Total Candidates</a></h4>
							<!-- <p>You can create new job opening by clicking the create icon.</p> -->
						</div>
						<ul class="gauge-meta clearfix">
							<li><strong class="text-xl" style="font-size:25px;padding-left: 100px;"><?php echo $countCandidates;?></strong></li>
						</ul>
					</div>
				</div>
			</section>
		</div><!--end .col -->

		<div class="col-md-3">

			<section class="panel panelhover" style="box-shadow:4px 4px 15px rgba(136, 136, 136, 0.2);">
				<div class="panel-body">
					<span class="mini-stat-icon orange"><i class="fa fa-briefcase"></i></span>
					<div class="clearfix"></div>
					<div class="top-stats-panel">
						<div class="gauge-canvas">
							<h4 class="widget-h"><a href="<?php echo base_url();?>admin/viewJobs">Total Jobs</a></h4>
							<!-- <p>You can create new job opening by clicking the create icon.</p> -->
						</div>
						<ul class="gauge-meta clearfix">
							<li><strong class="text-xl" style="font-size:25px;padding-left: 100px;"><?php echo $countJobs;?></strong></li>
						</ul>
					</div>
				</div>
			</section>
		</div><!--end .col -->

	</div>
	</section>
	</section>
	<div class="row">
		<div class="" style="margin-bottom:51px;">
			
			<form method="post" id="eventForm" method="post" action="<?php echo base_url();?>admin/viewDashboard">
				<div class="panel panel-default">
					<div class="panel-body row">
						<div class="col-md-3 col-md-offset-2 col-sm-6">
							<label>From:</label>
							<div class="input-group date" id="date_of_birth1">
								<div class="input-group-content">

									<input type="text" datepicker="" id="start_date" class="form-control" name="start_date"  autocomplete="off" />

								</div>
								<a href="javascript:void(0);" class="input-group-addon date"><i class="fa fa-calendar"></i></a>
							</div> 
						</div>

						<div class="col-md-3 col-sm-6">
							<label>To:</label>
							<div class="input-group date" id="date_of_birth2">
								<div class="input-group-content">

									<input type="text" datepicker="" id="end_date" class="form-control" name="end_date"  autocomplete="off"/>

								</div>
								<a href="javascript:void(0);" class="input-group-addon date"><i class="fa fa-calendar"></i></a>
							</div>
						</div>

						<div class="col-md-3 col-sm-6">
							<input type="submit" name="name" value="submit" class="btn btn-success lgnsnup-btn" style="margin-left: 27px;
							margin-top: 22px;">
						</div>

					</div>
				</div>
			</form>
		</div>
		<div class="row">
			<div class="col-md-6" >
				<div class="panel panel-default">
					<div class="panel-body">
						<div id="chartContainer" style="height: 300px; width: 100%;"></div>
					</div>
				</div>
				<br>
			</div>

			<div class="col-md-6">
				<div class="panel panel-default">
					<div class="panel-body">
						<div id="chartContainer1" style="height: 300px; width: 100%;"></div>
					</div>
				</div>
				<br>
			</div>
		</div>


		<div class="row">
			<div class="col-md-6">
				<div class="panel panel-default">
					<div class="panel-body">
						<div id="chartContainer2" style="height: 300px; width: 100%;"></div>
					</div>
				</div>
				<br>


			</div>


		</div>

	</div>
</div>