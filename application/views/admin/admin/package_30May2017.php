 <style type="text/css">
 #packagesblk .panel-heading {
  background-color: #474752;
  border-color: #474752;
  color: #fff;
  font-weight: 600;
  min-height: 135px;
  font-size: 16px;
}
#packagesblk .panel-heading .text-xxxl {
  font-size: 46px;
  font-weight: 600;
}
#packagesblk .panel-body .editbtn {
  background-color: transparent;
  border: 1px solid #aaa;
  width: 30px;
  height: 30px;
  border-radius: 50%;
  color: #777;
  transition: all .3s ease;
  font-size: 15px;
}
#packagesblk .panel-body button:hover {
  background-color: #777;
  color: #fff;
  border: 1px solid #777;
}
#packagesblk .panel {
  margin-bottom: 30px;
  box-shadow: 0 2px 15px rgba(0,0,0,0.1);
}

.help-block{

  color:red;
}

.help-block.has-error{

  display: block;
  position: absolute;
  top: 36px;
}
.input-group {
  width: 100%;
}
.form-signin input {
  margin-bottom: 30px !important;
}

.input-group-addon.date{

  position: absolute;
  right: 0;
  padding: 11px;
  width: 40px;
  height: 38px;
}
.comment {
  width: 210px;
  background-color: #fff;
  margin: 10px;
  min-height:54px;
}
a.morelink {
  text-decoration:none;
  outline: none;
}
.morecontent span {
  display: none;

}

.datepicker.datepicker-dropdown{

  z-index:9999 !important;

}

</style>


<!--main content start-->
<section id="packagesblk" class="container">
  <section class="wrapper">
    <!-- page start-->
    <div class="row">
      <div class="col-md-9">
        <br>

        <?php echo $this->session->flashdata('successmsg');?>
        <?php echo $this->session->flashdata('errormsg');?>

        <br>
      </div>
      <div class="col-md-3">
        <br>
        <button href="#myModal-1" data-toggle="modal" type="button" class="btn btn-success btn-sm pull-right"><i class="fa fa-plus"></i> <strong>Add Package</strong></button>

        <!-- Modal -->
        <form class="form-signin" name="packageForm" ng-submit="submitPackageForm()" novalidate>
          <div aria-hidden="true" aria-labelledby="myModalLabel" role="dialog" tabindex="-1" id="myModal-1" class="modal fade">
            <div class="modal-dialog">
              <div class="modal-content">
                <div class="modal-header">
                  <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                  <h4 class="modal-title">Add Package</h4>
                </div>

                <div class="modal-body">
                  <div id="message2"></div>
                  <div class="input-group date">
                    <input type="text" id="email" name="package_title"  placeholder="Package Title" autocomplete="off" class="form-control placeholder-no-fix" ng-model="addPackage.package_title" required>
                    <span ng-show="submitted && packageForm.package_title.$invalid" class="help-block has-error">Package Title is required.</span>
                    <span ng-show="errorTitle" class="help-block has-error ng-hide">{{errorTitle}}</span>
                  </div>
                  <div class="input-group date">
                    <textarea rows="8" cols="4" class="form-control placeholder-no-fix" placeholder="Package Description" autocomplete="off" name="package_description" ng-model="addPackage.package_description" required style="height:85px;width:559px;"></textarea>
                    <span style="padding-top:42px;" ng-show="submitted && packageForm.package_description.$invalid" class="help-block has-error">Package Decription is required.</span>
                    <span ng-show="errorDescription" class="help-block has-error ng-hide">{{errorDescription}}</span>
                  </div></br>
                  <div class="input-group date">
                    <input type="text" class="form-control placeholder-no-fix" placeholder="Amount" autocomplete="off" name="amount" ng-model="addPackage.amount" required>
                    <span ng-show="submitted && packageForm.amount.$invalid" class="help-block has-error">Amount is required.</span>
                    <span ng-show="errorAmount" class="help-block has-error ng-hide">{{errorAmount}}</span>
                  </div>

                           <!-- <md-input-container> 
                           <md-datepicker ng-model="myDate" name ="myDate" md-placeholder="Enter DOB" md-min-date="minDate" md-max-date="maxDate" value="" ng-change="license.expirationdate = myDate.toISOString()" ng-required="true" ></md-datepicker> 
                           <div ng-message="required">This dob is required!</div>
                         </md-input-container> -->
                         <div class="input-group date" id="demo-date2">
                          <div class="input-group-content">
                            <input type="text" class="form-control" autocomplete="off"  ng-model="addPackage.validity" name="validity" placeholder="Validity Date"  id="validity" onchange="hideMsg();" required style="margin-bottom:0px;">
                            <span id="msg" ng-show="submitted && packageForm.validity.$invalid" class="help-block has-error">Validity Date is required.</span>
                            <span ng-show="errorValidity" class="help-block has-error ng-hide">{{errorValidity}}</span>
                          </div>
                          <a href="javascript:void(0);" class="input-group-addon date"><i class="fa fa-calendar"></i></a>
                        </div>

                      </div>
                      <div class="modal-footer">
                        <button data-dismiss="modal" class="btn btn-default" type="button">Cancel</button>
                        <button class="btn btn-success" ng-click="submitted = true" type="submit">Submit</button>
                      </div>

                    </div>
                  </div>
                </div>
              </form>



              <form class="form-signin" name="editpackageForm" ng-submit="submitEditPackageForm()" novalidate>
                <div aria-hidden="true" aria-labelledby="myModalLabel" role="dialog" tabindex="-1" id="myModal-2" class="modal fade">
                  <div class="modal-dialog">
                    <div class="modal-content">
                      <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                        <h4 class="modal-title">Edit Package</h4>
                      </div>
                      
                      <div class="modal-body">
                        <div id="message2"></div>
                        <input type="hidden" class="form-control" value="" name="edit_package_id"  id="edit_package_id">

                        <input type="text" id="email" name="edittitle"  placeholder="Package Title" autocomplete="off" class="form-control placeholder-no-fix" ng-model="package.package_title" required>
                        <span ng-show="editpackagesubmitted && editPackageForm.package_title.$invalid" class="help-block has-error">Package Title is required.</span>
                        <span ng-show="errorEditTitle" class="help-block has-error ng-hide">{{errorEditTitle}}</span>


                        <textarea rows="8" cols="4" class="form-control placeholder-no-fix" placeholder="Package Description" autocomplete="off" name="package_description" ng-model="package.package_description" required style="height:85px;width:559px;"></textarea>
                        <span ng-show="editpackagesubmitted && editPackageForm.package_description.$invalid" class="help-block has-error">Package Decription is required.</span>
                        <span ng-show="errorEditDescription" class="help-block has-error ng-hide">{{errorEditDescription}}</span></br>


                        <input type="text" class="form-control placeholder-no-fix" placeholder="Amount" autocomplete="off" name="amount" ng-model="package.amount" required>
                        <span ng-show="editpackagesubmitted && editPackageForm.amount.$invalid" class="help-block has-error">Amount is required.</span>
                        <span ng-show="errorEditAmount" class="help-block has-error ng-hide">{{errorEditAmount}}</span>




                        <div class="input-group date" id="demo-date1">
                          <div class="input-group-content">
                            <input type="text" class="form-control" autocomplete="off"   name="validity" ng-model="package.validity" placeholder="Validity Date"  id="package_validity" required style="margin-bottom:0px;">
                            <span ng-show="editpackagesubmitted && editPackageForm.validity.$invalid" class="help-block has-error">Validity Date is required.</span>
                            <span ng-show="errorEditValidity" class="help-block has-error ng-hide">{{errorEditValidity}}</span>
                          </div>
                          <a href="javascript:void(0);" class="input-group-addon date"><i class="fa fa-calendar"></i></a>


                        </div>

                      </div>
                      <div class="modal-footer">

                        <button data-dismiss="modal" class="btn btn-default" type="button">Cancel</button>
                        <button class="btn btn-success" ng-click="editpackagesubmitted = true" type="submit">Submit</button>
                      </div>

                    </div>
                  </div>
                </div>
              </form>









              <!-- modal -->
            </div>
            <div class="clearfix"></div>
            <?php
            if(!empty($package_data)){
              foreach($package_data as $key){

                ?>
                <div class="col-md-3">
                  <section class="panel text-center">
                    <div class="panel-heading"><?php echo $key->package_title;?>
                      <div class="price">
                        <h2><span class="text-lg">£</span> <span class="text-xxxl"><?php echo $key->amount;?></span></h2>
                      </div>
                    </div>
                    <div class="panel-body">
                      <br>
                      <h4>Validity date: <?php echo date('jS M Y',strtotime($key->validity_date));?></h4>

                      <hr>
                      <div class="comment more">
                        <?php echo $key->package_description;?>
                      </div>
                      <a  href="<?php echo base_url();?>admin/deletePackage?id=<?php echo $key->id;?>" class="btn btn-default btn-xs pull-left editbtn" onclick="return confirmDelete();"><i class="fa fa-trash-o"></i></a>
                      <a  id="edit" data="<?php echo $key->id; ?>" href="#myModal-2" data-toggle="modal" data-dismiss="modal" ng-click="edit_package(<?php echo $key->id ?>)" class="btn btn-default btn-xs pull-right editbtn" ><i class="fa fa-pencil-square-o"></i></a>
                    </div>
                  </section>
                </div>
                <?php }}else{

                 echo "No packages found"; 

               }?>
                    <!-- <div class="col-md-3">
                        <section class="panel text-center">
                            <div class="panel-heading">Package Heading
                            <div class="price">
                                <h2><span class="text-lg">£</span> <span class="text-xxxl">5000</span></h2>
                            </div>
                            </div>
                            <div class="panel-body">
                                <br>
                                <h4>Validity date: 23-4-2017</h4>
                                <hr>
                                <button type="button" class="btn btn-default btn-xs pull-left"><i class="fa fa-trash-o"></i></button>
                                <button type="button" class="btn btn-default btn-xs pull-right"><i class="fa fa-pencil-square-o"></i></button>
                            </div>
                        </section>
                      </div> -->
                   <!--  <div class="col-md-3">
                        <section class="panel text-center">
                            <div class="panel-heading">Package Heading
                            <div class="price">
                                <h2><span class="text-lg">£</span> <span class="text-xxxl">5000</span></h2>
                            </div>
                            </div>
                            <div class="panel-body">
                                <br>
                                <h4>Validity date: 23-4-2017</h4>
                                <hr>
                                <button type="button" class="btn btn-default btn-xs pull-left"><i class="fa fa-trash-o"></i></button>
                                <button type="button" class="btn btn-default btn-xs pull-right"><i class="fa fa-pencil-square-o"></i></button>
                            </div>
                        </section>
                      </div> -->
                    <!-- <div class="col-md-3">
                        <section class="panel text-center">
                            <div class="panel-heading">Package Heading
                            <div class="price">
                                <h2><span class="text-lg">£</span> <span class="text-xxxl">5000</span></h2>
                            </div>
                            </div>
                            <div class="panel-body">
                                <br>
                                <h4>Validity date: 23-4-2017</h4>
                                <hr>
                                <button type="button" class="btn btn-default btn-xs pull-left"><i class="fa fa-trash-o"></i></button>
                                <button type="button" class="btn btn-default btn-xs pull-right"><i class="fa fa-pencil-square-o"></i></button>
                            </div>
                        </section>
                      </div> -->
                   <!--  <div class="col-md-3">
                        <section class="panel text-center">
                            <div class="panel-heading">Package Heading
                            <div class="price">
                                <h2><span class="text-lg">£</span> <span class="text-xxxl">5000</span></h2>
                            </div>
                            </div>
                            <div class="panel-body">
                                <br>
                                <h4>Validity date: 23-4-2017</h4>
                                <hr>
                                <button type="button" class="btn btn-default btn-xs pull-left"><i class="fa fa-trash-o"></i></button>
                                <button type="button" class="btn btn-default btn-xs pull-right"><i class="fa fa-pencil-square-o"></i></button>
                            </div>
                        </section>
                      </div> -->
                   <!--  <div class="col-md-3">
                        <section class="panel text-center">
                            <div class="panel-heading">Package Heading
                            <div class="price">
                                <h2><span class="text-lg">£</span> <span class="text-xxxl">5000</span></h2>
                            </div>
                            </div>
                            <div class="panel-body">
                                <br>
                                <h4>Validity date: 23-4-2017</h4>
                                <hr>
                                <button type="button" class="btn btn-default btn-xs pull-left"><i class="fa fa-trash-o"></i></button>
                                <button type="button" class="btn btn-default btn-xs pull-right"><i class="fa fa-pencil-square-o"></i></button>
                            </div>
                        </section>
                      </div> -->
                    <!-- <div class="col-md-3">
                        <section class="panel text-center">
                            <div class="panel-heading">Package Heading
                            <div class="price">
                                <h2><span class="text-lg">£</span> <span class="text-xxxl">5000</span></h2>
                            </div>
                            </div>
                            <div class="panel-body">
                                <br>
                                <h4>Validity date: 23-4-2017</h4>
                                <hr>
                                <button type="button" class="btn btn-default btn-xs pull-left"><i class="fa fa-trash-o"></i></button>
                                <button type="button" class="btn btn-default btn-xs pull-right"><i class="fa fa-pencil-square-o"></i></button>
                            </div>
                        </section>
                      </div> -->
                    <!-- <div class="col-md-3">
                        <section class="panel text-center">
                            <div class="panel-heading">Package Heading
                            <div class="price">
                                <h2><span class="text-lg">£</span> <span class="text-xxxl">5000</span></h2>
                            </div>
                            </div>
                            <div class="panel-body">
                                <br>
                                <h4>Validity date: 23-4-2017</h4>
                                <hr>
                                <button type="button" class="btn btn-default btn-xs pull-left"><i class="fa fa-trash-o"></i></button>
                                <button type="button" class="btn btn-default btn-xs pull-right"><i class="fa fa-pencil-square-o"></i></button>
                            </div>
                        </section>
                      </div> -->
                    </div>
                    <!-- page end-->

                  </section>
                </section>