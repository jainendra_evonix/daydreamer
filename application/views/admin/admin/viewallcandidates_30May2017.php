


<link rel="stylesheet" href="js/data-tables/DT_bootstrap.css" />

<!--main content start-->
<section id="adminsection" class="container">
    <section class="wrapper">
        <!-- page start-->
        <div class="row">
            <div class="col-md-12">
                <h4 class="form-heading"><strong>All Candidates</strong></h4>
                <?php echo $this->session->userdata('successmsg');?>
                 <?php echo $this->session->userdata('errormsg');?>
                <p>All Offices information list. <a href="#" class="pull-right">Help <i class="fa fa-question-circle"></i></a></p>

                <br>
                <section class="panel">
                    <div class="panel-body">
                        <div class="row">
                            <div class="col-md-12">
                                <!-- <a><button type="submit" class="btn btn-info pull-right btn-sm" style="margin-bottom:18px;"><strong><i class="fa fa-plus"></i> Create New Office</strong></button><span class="pull-right"> &nbsp; &nbsp; </span></a> -->

                                <!-- <button type="button" class="btn btn-info pull-right btn-sm"><strong><i class="fa fa-times"></i> Cancel</strong></button> -->
                                <!-- <br> -->
                                <!-- <hr> -->
                            </div>
                            <div class="col-md-12">
                                <section class="">
                                    <div class="adv-table">
                                        <table  class="display table table-bordered table-striped" id="dynamic-table">
                                            <thead>
                                                <tr>
                                                    <th style="width: 5%;">Sr.No</th>
                                                    <th style="width: 30%;">Client Name</th>
                                                    <th style="width:20%;">City</th>
                                                    <th style="width: 30%;">Email</th>
                                                    <th style="width:15%;">Action</th>

                                                </tr>
                                            </thead>
                                            <tbody>
                                                <?php

                                                $count=1;
                                                foreach($allCandidates as $key){
                                                    ?>
                                                    <tr class="gradeX">

                                                        <td style="width: 5%;"><?php echo $count++;?></td>
                                                        <td style="width: 30%;"><?php echo $key->first_name.' '.$key->last_name?></td>
                                                        <td style="width:20%;"><?php echo $key->user_city;?></td>
                                                        <td style="width: 30%;"><?php echo $key->user_email;?></td>
                                                        <td style="width:15%;">
                                                            <ul class="list-unstyled list-inline">
                                                                <!-- <li><a href="<?php echo base_url();?>admin/editUserDetails?id=<?php echo $key->id;?>"><i class="fa fa-pencil" aria-hidden="true"></i></a></li> -->
                                                                 <?php
                                                                  $deactivate=$key->deactivate;
                                                                  if($deactivate == '0')
                                                                  {
                                                                 ?>
                                                                <li onclick="return deactivateUser();"><a href="<?php echo base_url();?>admin/disableUser?id=<?php echo $key->id;?>"><i class="fa fa-check-square-o" aria-hidden="true"></i></a></li>
                                                                <?php } else{?>
                                                                   <li onclick="return activateUser();"><a href="<?php echo base_url();?>admin/EnableUser?id=<?php echo $key->id;?>"><i class="fa fa-minus-circle" aria-hidden="true"></i></a></li>
                                                                <?php }?>
                                                            </ul>
                                                        </td>
                                                    </tr>
                                                    <?php }?>
                                                </tbody>
                                            </table>
                                        </div>
                                    </section>
                                </div>
                            </div>
                        </div>
                    </section>
                </div>
            </div>
            <!-- page end-->

        </section>
    </section>
    <!--main content end-->





    
</div>




<!--dynamic table initialization -->
