<script>

var postApp = angular.module('postApp',[]);



postApp.directive('calenderend', function() {
  //console.log('here');
    return {
                require: 'ngModel',
                link: function (scope, el, attr, ngModel) {
                    $(el).datepicker({
                        dateFormat: 'yy-mm-dd',
                        onSelect: function (dateText) {
                          console.log(dateText);
                          scope.addPackage.validity = dateText;

                            scope.$apply(function () {
                                ngModel.$setViewValue(dateText);
                            });
                        }
                    });
                }
            };
});
postApp.controller('postController', function ($scope, $http) {

$scope.timesheet={};
$scope.timesheet.cand_name='<?php echo!empty($candidateData) ? $candidateData->first_name.' '. $candidateData->last_name: $timesheetData->candidate_name ?>';
$scope.timesheet.client_name='<?php echo!empty($timesheetData) ? $timesheetData->clnt_first_name.' '.$timesheetData->clnt_last_name: '' ?>';

$scope.timesheet.type='<?php echo!empty($timesheetData) ? $timesheetData->type: '' ?>';
$scope.timesheet.location='<?php echo!empty($timesheetData) ? $timesheetData->location: '' ?>';
$scope.timesheet.timesheet_id='<?php echo!empty($timesheetData) ? $timesheetData->id : '' ?>';
$scope.timesheet.date='<?php echo!empty($timesheetData) ? date("jS M Y",strtotime($timesheetData->date)) : '' ?>';

$scope.timesheet.hours='<?php echo!empty($timesheetData) ? $timesheetData->hours : '' ?>';
$scope.timesheet.minutes='<?php echo!empty($timesheetData) ? $timesheetData->minutes : '' ?>';
$scope.timesheet.meridium='<?php echo!empty($timesheetData) ? $timesheetData->meridium : '' ?>';
$scope.timesheet.finish_hours='<?php echo!empty($timesheetData) ? $timesheetData->finish_hours : '' ?>';
$scope.timesheet.finish_minutes='<?php echo!empty($timesheetData) ? $timesheetData->finish_minutes : '' ?>';
$scope.timesheet.finish_meridium='<?php echo!empty($timesheetData) ? $timesheetData->finish_meridium : '' ?>';
$scope.timesheet.break_duartion='<?php echo!empty($timesheetData) ? $timesheetData->break_duartion : '' ?>';
$scope.timesheet.pay_rate='<?php echo!empty($timesheetData) ? $timesheetData->pay_rate : '' ?>';
$scope.timesheet.currency='<?php echo!empty($timesheetData) ? $timesheetData->currency_id : '' ?>';
$scope.timesheet.pay_type='<?php echo!empty($timesheetData) ? $timesheetData->pay_type : '' ?>';

// $scope.event.state='<?php echo!empty($eventData) ? $eventData->state : '' ?>';
// $scope.event.desc='<?php echo!empty($eventData) ? preg_replace("/[^A-Za-z0-9]/",' ',$eventData->description) : '' ?>';

// if($scope.event.public =='1'){
// //alert('here');
// $('#public_checkbox').prop('checked', true);
// }else{
// //alert('there');
// $('#public_checkbox').prop('checked', false);    
// }


$scope.submitTimesheetForm = function(){

var date=$("#date").val();
$scope.timesheet.date=date;
var candidate_id='<?php echo isset($candidate_id) ? $candidate_id : ''?>';
//alert(candidate_id);
$scope.timesheet.candidate_id=candidate_id;
//alert($scope.timesheet.timesheet_id);
var clientName=$("#organizationname").val();
$scope.timesheet.client_name=clientName;
var candidate_name=$("#candidateName").val();
//alert(candidate_name);
$scope.timesheet.candidate_name=candidate_name;
if ($scope.timesheetForm.$valid) {
$http({
      method: 'POST',
      dataType: 'json',
      url: '<?php echo base_url();?>admin/createTimesheet',

            data: $scope.timesheet, //forms event object
            headers: {'Content-Type': 'application/json'}
          }).success(function(data){
          if(data.status == '1'){
            
                 
             window.location='<?php echo base_url();?>admin/manageTimesheets';
            }else{
            $scope.errorDuration = data.error.break_duartion.error;
            $scope.errorRate = data.error.pay_rate.error;
            $scope.errorCurrency = data.error.currency.error;
            $scope.errorType = data.error.pay_type.error;
            
            }


          });


}else{



}




};

});


function confirmMsg(){
var result=confirm('Are you sure you want to delete?');
if(result){

return true;
}else{
return false;

}

}

</script>

<script type="text/javascript" src="<?php echo base_url();?>client_assets/js/client_js/autocomplete.js"></script>
<script>
    
    var a = $('#selctionajax').autocomplete({
      serviceUrl:'<?php echo base_url();?>admin/fetchClient',
      minChars:1,
      delimiter: /(,|;)\s*/, // regex or character
      maxHeight:600,
      //width:300,
      zIndex: 9999,
      deferRequestBy: 100, //miliseconds
       onSelect: function(suggestion) {
       //alert();
       if(suggestion.data)
                         {
           $('#organizationname').val(suggestion.data);

                        }
                        else
                        {
                        alert("sd");
                        }
}
      //params: { country:'Yes' }, //aditional parameters
      //noCache: false //default is false, set to true to disable caching
      // callback function:
      //onSelect: function(value, data){ alert('You selected: ' + value + ', ' + data); },
      // local autosugest options:
      //lookup: ['January', 'February', 'March', 'April', 'May'] //local lookup values
    });

    var b=$('#selectionajax').autocomplete({
      serviceUrl:'<?php echo base_url();?>admin/fetchCandidate',
      minChars:1,
      delimiter: /(,|;)\s*/, // regex or character
      maxHeight:600,
      //width:300,
      zIndex: 9999,
      deferRequestBy: 100, //miliseconds
       onSelect: function(suggestion) {
       //alert();
       if(suggestion.data)
                         {
           $('#candidateName').val(suggestion.data);

                        }
                        else
                        {
                        alert("sd");
                        }
}
      //params: { country:'Yes' }, //aditional parameters
      //noCache: false //default is false, set to true to disable caching
      // callback function:
      //onSelect: function(value, data){ alert('You selected: ' + value + ', ' + data); },
      // local autosugest options:
      //lookup: ['January', 'February', 'March', 'April', 'May'] //local lookup values
    });

    </script>