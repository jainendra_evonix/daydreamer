<script>
$(document).ready(function() {

     var dataTable = $('#employee-grid').DataTable({
     	//alert();
            "processing": true,
            "serverSide": true,
            "ajax": {
                url: "<?php echo base_url();?>admin/groupInterviewData", // json datasource
                type: "post", // method  , by default get
                error: function () {  // error handling
                    $(".employee-grid-error").html("");
                    $("#employee-grid").append('<tbody class="employee-grid-error"><tr><th colspan="3">No data found in the server</th></tr></tbody>');
                    $("#employee-grid_processing").css("display", "none");

                }
            }
        });

} );

$("#viewCredentials").addClass('active');

</script>