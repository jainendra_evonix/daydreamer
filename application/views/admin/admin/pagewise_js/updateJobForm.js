<script type="text/javascript">
var postApp = angular.module('postApp', []);

postApp.directive('ngFiles', ['$parse', function ($parse) {

        function fn_link(scope, element, attrs) {
            var onChange = $parse(attrs.ngFiles);
            element.on('change', function (event) {
                onChange(scope, {$files: event.target.files});
            });
        }
        ;

        return {
            link: fn_link
        }
    }]);

  postApp.directive('ckEditor', function() {
  //console.log('here');
  return {
    require: '?ngModel',
    link: function(scope, elm, attr, ngModel) {
      var ck = CKEDITOR.replace(elm[0]);

      if (!ngModel) return;

      ck.on('pasteState', function() {
        scope.$apply(function() {
          ngModel.$setViewValue(ck.getData());
        });
      });

      ngModel.$render = function(value) {
        ck.setData(ngModel.$viewValue);
      };
    }
  };
});

  postApp.directive('calenderopen', function() {
  //console.log('here');
    return {
                require: 'ngModel',
                link: function (scope, el, attr, ngModel) {
                    $(el).datepicker({
                        dateFormat: 'yy-mm-dd',
                        onSelect: function (dateText) {
                          console.log(dateText);
                          scope.addPackage.validity = dateText;

                            scope.$apply(function () {
                                ngModel.$setViewValue(dateText);
                            });
                        }
                    });
                }
            };
});
  postApp.directive('lowerThan', [
  function() {
   //alert();
    var link = function($scope, $element, $attrs, ctrl) {

      var validate = function(viewValue) {
        var comparisonModel = $attrs.lowerThan;
        
        if(!viewValue || !comparisonModel){
          // It's valid because we have nothing to compare against
          ctrl.$setValidity('lowerThan', true);
        }

        // It's valid if model is lower than the model we're comparing against
        ctrl.$setValidity('lowerThan', parseInt(viewValue, 10) < parseInt(comparisonModel, 10) );
        return viewValue;
      };

      ctrl.$parsers.unshift(validate);
      ctrl.$formatters.push(validate);

      $attrs.$observe('lowerThan', function(comparisonModel){
        return validate(ctrl.$viewValue);
      });
      
    };

    return {
      require: 'ngModel',
      link: link
    };

  }
]);
postApp.controller('postController', function ($scope, $http) {
   var formdata = new FormData();
 $scope.job = {};
 $scope.job.job_id='<?php echo!empty($getJobInfo) ? $getJobInfo->j_id : '' ?>';

 $scope.job.job_title='<?php echo!empty($getJobInfo) ? $getJobInfo->job_title : '' ?>';
 $scope.job.contact_name='<?php echo!empty($getJobInfo) ? $getJobInfo->contact_name : '' ?>';
 $scope.job.recruiter='<?php echo!empty($getJobInfo) ? $getJobInfo->recruiter : '' ?>';
 //$scope.job.target_date='<?php echo!empty($getJobData) ? date("d/m/y",strtotime($getJobData->target_date)) : '' ?>';
 $scope.job.job_status='<?php echo!empty($getJobInfo) ? $getJobInfo->status_id : '' ?>';
 

 $scope.job.state='<?php echo!empty($getJobInfo) ? $getJobInfo->state : '' ?>';
 $scope.job.experience='<?php echo!empty($getJobInfo) ? $getJobInfo->experience : '' ?>';
 //$scope.job.parent_client='<?php echo!empty($getJobInfo) ? $getJobInfo->parent_client : '' ?>';
 $scope.job.manager='<?php echo!empty($getJobInfo) ? $getJobInfo->manager : '' ?>';
 //alert($scope.job.manager);
 $scope.job.positions='<?php echo!empty($getJobInfo) ? $getJobInfo->positions : '' ?>';
 $scope.job.open_date='<?php echo!empty($getJobInfo) ? date("y-m-d",strtotime($getJobInfo->open_date)) : '' ?>';
 $scope.job.job_type='<?php echo!empty($getJobInfo) ? $getJobInfo->jobtype_id : '' ?>';
 $scope.job.qualifications=`<?php echo!empty($getJobInfo) ? preg_replace("/[^A-Za-z0-9]/",' ',$getJobInfo->qualifications) : '' ?>`;
 
 $scope.job.working_days='<?php echo!empty($getJobInfo) ? $getJobInfo->working_days : '' ?>';
 //$scope.job.parent_client='<?php echo!empty($getJobInfo) ? $getJobInfo->subclient_name : $getJobInfo->parent_client ?>';
 $scope.job.client_name='<?php echo $client_name;?>';
 $scope.job.company_profile=`<?php echo!empty($getJobInfo) ? preg_replace("/[^A-Za-z0-9]/",' ',$getJobInfo->company_profile) : '' ?>`;
 $scope.job.city='<?php echo!empty($getJobInfo) ? $getJobInfo->city : '' ?>';
 //$scope.job.skill='<?php echo!empty($getJobInfo) ? $getJobInfo->skills : '' ?>';
$scope.job.nationality='<?php echo!empty($getJobInfo) ? $getJobInfo->id : '' ?>';
$scope.job.min='<?php echo!empty($getJobInfo) ? $getJobInfo->min_salary : '' ?>';
$scope.job.max='<?php echo!empty($getJobInfo) ? $getJobInfo->max_salary : '' ?>';
$scope.job.currency='<?php echo!empty($getJobInfo) ? $getJobInfo->currency_id : '' ?>';
$scope.job.job_desc = `<?php echo !empty($getJobInfo) ? preg_replace("/[^A-Za-z0-9]/", ' ', $getJobInfo->job_desc) : '' ?>`;
//$scope.job.job_desc='<?php echo!empty($getJobInfo) ? $getJobInfo->job_desc : '' ?>';
$scope.job.attached_file='<?php echo!empty($getJobInfo) ? preg_replace("/[^A-Za-z0-9]/", ' ', $getJobInfo->job_summary) : $getJobData->job_summary ?>';
console.log($scope.job);

  $scope.getTheFiles = function ($files) {

        angular.forEach($files, function (value, key) {
            formdata.append('file', value);
        });

        var request = {
            method: 'POST',
            url: '<?php echo base_url();?>admin/upload_doc',
            data: formdata,
            enctype: 'multipart/form-data',
            headers: {
                'Content-Type': undefined
            }
        };
        $http(request)
                .success(function (data) {
                  console.log(data)
                    $scope.job.filename = data.filename;
                    
                })
                .error(function () {
                });
    };

$scope.submitjobForm = function () {

  var editor=CKEDITOR.instances.editor;
  $scope.job.job_desc=editor.getData();
  
  
  var open_date=$("#open_date").val();
  var editor=$(".ckeditor").val();

  $scope.job.open_date=open_date;

  $scope.job.parent_client=$("#organizationname").val();
  //alert($scope.job.open_date);
  

   if ($scope.jobForm.$valid) {
  $http({
      method: 'POST',
      dataType: 'json',
      url: '<?php echo base_url();?>admin/update_job_opening',

            data: $scope.job, //forms client object
            headers: {'Content-Type': 'application/json'}
          }).success(function(data){
            if(data.status == 1){

            window.location = '<?php echo base_url('admin/viewJobs') ?>';


           }
           else{
          $scope.errorTitle = data.error.job_title.error;
          $scope.errorContactName = data.error.contact_name.error;
          $scope.errorRecruiter = data.error.recruiter.error;
          $scope.errorTarget = data.error.target_date.error;
          $scope.errorStatus = data.error.job_status.error;
          $scope.errorIndustry = data.error.industry.error;
          $scope.errorState = data.error.state.error;
          $scope.errorExperience = data.error.experience.error;
          $scope.errorClientName = data.error.parent_clienterrorManager.error;
          $scope.errorManager = data.error.manager.error;
          $scope.errorPositions = data.error.positions.error;
          $scope.errorOpen = data.error.open_date.error;
          $scope.errorJobType = data.error.job_type.error;
          $scope.errorCity = data.error.city.error;
          $scope.errorNationality = data.error.nationality.error;
          $scope.errorSalary = data.error.salary.error;
          $scope.errorDescription = data.error.job_desc.error;
          $scope.errorFile = data.error.attached_file.error;

           } 


          });

   
      }else{
         
        console.log($scope.jobForm.$valid);

      }


};


});

function hideopen(){

  $("#msg1").hide();
}

</script>

<script type="text/javascript" src="<?php echo base_url();?>client_assets/js/client_js/autocomplete.js"></script>
<script>
    
    var a = $('#selctionajax').autocomplete({
      serviceUrl:'<?php echo base_url();?>admin/fetchAllClient',
      minChars:1,
      delimiter: /(,|;)\s*/, // regex or character
      maxHeight:600,
      //width:300,
      zIndex: 9999,
      deferRequestBy: 100, //miliseconds
       onSelect: function(suggestion) {
       //alert();
       if(suggestion.data)
                         {
           $('#organizationname').val(suggestion.data);

                        }
                        else
                        {
                        alert("sd");
                        }
}
      //params: { country:'Yes' }, //aditional parameters
      //noCache: false //default is false, set to true to disable caching
      // callback function:
      //onSelect: function(value, data){ alert('You selected: ' + value + ', ' + data); },
      // local autosugest options:
      //lookup: ['January', 'February', 'March', 'April', 'May'] //local lookup values
    });
</script>


