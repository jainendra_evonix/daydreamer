<script type="text/javascript">
var postApp = angular.module('postApp', []);

postApp.directive('ngFiles', ['$parse', function ($parse) {

        function fn_link(scope, element, attrs) {
            var onChange = $parse(attrs.ngFiles);
            element.on('change', function (event) {
                onChange(scope, {$files: event.target.files});
            });
        }
        ;

        return {
            link: fn_link
        }
    }]);

postApp.controller('postController', function ($scope, $http) {
var formdata = new FormData();
$scope.blog = {};
$scope.blog.admin_id='<?php echo!empty($fetchBlogInfo) ? $fetchBlogInfo->id : '' ?>';
$scope.blog.name='<?php echo!empty($fetchBlogInfo) ? $fetchBlogInfo->admin_name : '' ?>';
$scope.blog.blog_title='<?php echo!empty($fetchBlogInfo) ? $fetchBlogInfo->blog_title : '' ?>';
$scope.blog.blog_desc='<?php echo!empty($fetchBlogInfo) ? preg_replace("/[^A-Za-z0-9]/", ' ', $fetchBlogInfo->blog_desc) : '' ?>';
$scope.blog.attached_file='<?php echo!empty($fetchBlogInfo) ? $fetchBlogInfo->blog_image : '' ?>';


$scope.getTheFiles = function ($files) {

        angular.forEach($files, function (value, key) {
            formdata.append('file', value);
        });

        var request = {
            method: 'POST',
            url: '<?php echo base_url();?>admin/upload_blog',
            data: formdata,
            enctype: 'multipart/form-data',
            headers: {
                'Content-Type': undefined
            }
        };
        $http(request)
                .success(function (data) {
                  console.log(data)
                    $scope.blog.filename = data.filename;
                    
                })
                .error(function () {
                });
    };

    $scope.getTheVideos = function ($files) {

        angular.forEach($files, function (value, key) {
            formdata.append('file', value);
        });

        var request = {
            method: 'POST',
            url: '<?php echo base_url();?>admin/upload_blogVideo',
            data: formdata,
            enctype: 'multipart/form-data',
            headers: {
                'Content-Type': undefined
            }
        };
        $http(request)
                .success(function (data) {
                  console.log(data)
                    $scope.blog.video = data.filename;
                    
                })
                .error(function () {
                });
    };


    

$scope.submitBlogForm = function(){

    var image=$("#uploadFile").val();
    if(image == ''){
    $("#message2").html('Please upload blog image');

    }else{

    $("#message2").html('');
    }
console.log($scope.blog);
if ($scope.blogForm.$valid) {

$http({
      method: 'POST',
      dataType: 'json',
      url: '<?php echo base_url();?>admin/insertBlog',

            data: $scope.blog, //forms client object
            headers: {'Content-Type': 'application/json'}
          }).success(function(data){

           if(data.status == '1'){
            
                 
             window.location='<?php echo base_url();?>admin/viewAllBlogs';
            }else if(data.status== 3){

               $("#message2").append(data.error);

           }else{
            $scope.errorName = data.error.name.error;
            $scope.errorBlogTitle = data.error.blog_title.error;
            $scope.errorBlogDesc = data.error.blog_desc.error;
           
            
           

            }

          })
}else{


}

};

});


function hideMsg(){

    $("#message2").hide();
}
</script>