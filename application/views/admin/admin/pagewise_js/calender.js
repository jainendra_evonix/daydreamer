<script>

var postApp = angular.module('postApp',[]);
postApp.directive('calenderopen', function() {
  //console.log('here');
    return {
                require: 'ngModel',
                link: function (scope, el, attr, ngModel) {
                    $(el).datepicker({
                        dateFormat: 'yy-mm-dd',
                        onSelect: function (dateText) {
                          console.log(dateText);
                          scope.addPackage.validity = dateText;

                            scope.$apply(function () {
                                ngModel.$setViewValue(dateText);
                            });
                        }
                    });
                }
            };
});
postApp.controller('postController', function ($scope, $http) {

$scope.event={};
$scope.event.event_title='<?php echo!empty($eventData) ? $eventData->event_title : '' ?>';
$scope.event.event_id='<?php echo!empty($eventData) ? $eventData->id : '' ?>';
$scope.event.client_name='<?php echo!empty($eventData) ? $eventData->client : '' ?>';
$scope.event.candidate_name='<?php echo!empty($eventData) ? $eventData->user : '' ?>';
$scope.event.type='<?php echo!empty($eventData) ? $eventData->type : '' ?>';
$scope.event.public='<?php echo!empty($eventData) ? $eventData->public : '' ?>';
//alert($scope.event.public);
$scope.event.date='<?php echo!empty($eventData) ? date("jS M Y",strtotime($eventData->date)) : '' ?>';
$scope.event.time='<?php echo!empty($eventData) ? $eventData->time : '' ?>';
$scope.event.hours='<?php echo!empty($eventData) ? $eventData->hours : '' ?>';
$scope.event.minutes='<?php echo!empty($eventData) ? $eventData->minutes : '' ?>';
$scope.event.meridium='<?php echo!empty($eventData) ? $eventData->meridien : '' ?>';
$scope.event.industry='<?php echo!empty($eventData) ? $eventData->industry : '' ?>';
$scope.event.state='<?php echo!empty($eventData) ? $eventData->state : '' ?>';
$scope.event.desc='<?php echo!empty($eventData) ? $eventData->description : '' ?>';

if($scope.event.public =='1'){
//alert('here');
$('#public_checkbox').prop('checked', true);
}else{
//alert('there');
$('#public_checkbox').prop('checked', false);	
}


$scope.submitEventForm = function(){
var date=$("#open_date").val();
$scope.event.date=date;	
if ($scope.eventForm.$valid) {
$http({
      method: 'POST',
      dataType: 'json',
      url: '<?php echo base_url();?>admin/createEvent',

            data: $scope.event, //forms event object
            headers: {'Content-Type': 'application/json'}
          }).success(function(data){
          if(data.status == '1'){
            
                 
             window.location='<?php echo base_url();?>admin/viewEvents';
            }else{
            $scope.errorFirstName = data.error.first_name.error;
            $scope.errorLastName = data.error.last_name.error;
            $scope.errorTelephone = data.error.telephone.error;
            $scope.errorTelephone = data.error.telephone.error;
            $scope.errorEmail = data.error.email.error;
            $scope.errorPassword = data.error.password.error;
            $scope.errorCity= data.error.city.error;
            $scope.errorCountry= data.error.country.error;

            }


          });


}else{



}




};

});


function confirmMsg(){
var result=confirm('Are you sure you want to delete?');
if(result){

return true;
}else{
return false;

}

}

</script>