<script>
var postApp = angular.module('postApp', []);
postApp.directive('calenderopen', function() {
  //console.log('here');
    return {
                require: 'ngModel',
                link: function (scope, el, attr, ngModel) {
                    $(el).datepicker({
                        dateFormat: 'yy-mm-dd',
                        onSelect: function (dateText) {
                          console.log(dateText);
                          scope.addPackage.validity = dateText;

                            scope.$apply(function () {
                                ngModel.$setViewValue(dateText);
                            });
                        }
                    });
                }
            };
});

postApp.directive('ngFiles', ['$parse', function ($parse) {

        function fn_link(scope, element, attrs) {
            var onChange = $parse(attrs.ngFiles);
            element.on('change', function (event) {
                onChange(scope, {$files: event.target.files});
            });
        }
        ;

        return {
            link: fn_link
        }
    }]);

postApp.controller('postController', function ($scope, $http) {
var formdata = new FormData();	
$scope.interview={};

$scope.interview.candidate_id='<?php echo $id?>';
$scope.interview.cand_name='<?php echo $candidateFirstName.' '.$candidateLastName?>';
$scope.interview.interview_id='<?php echo!empty($interviewData) ? $interviewData->id : '' ?>';
$scope.interview.location='<?php echo!empty($interviewData) ? $interviewData->location : '' ?>';
$scope.interview.interview_date='<?php echo!empty($interviewData) ? date("jS M Y",strtotime($interviewData->interview_date)) : '' ?>';
$scope.interview.main_contact='<?php echo!empty($interviewData) ? $interviewData->main_contact : '' ?>';
$scope.interview.interview_address='<?php echo!empty($interviewData) ? preg_replace("/[^A-Za-z0-9]/",' ',$interviewData->interview_address) : '' ?>';
$scope.interview.breakdown_details='<?php echo!empty($interviewData) ? preg_replace("/[^A-Za-z0-9]/",' ',$interviewData->breakdown_interview)  : '' ?>';
$scope.interview.additional_notes='<?php echo!empty($interviewData) ? preg_replace("/[^A-Za-z0-9]/",' ',$interviewData->additional_notes) : '' ?>';
$scope.interview.start_time='<?php echo!empty($interviewData) ? $interviewData->event_start_time : '' ?>';
$scope.interview.finish_time='<?php echo!empty($interviewData) ? $interviewData->event_finish_time : '' ?>';
$scope.interview.contact_details='<?php echo!empty($interviewData) ? $interviewData->contact_details : '' ?>';
$scope.interview.site_contact='<?php echo!empty($interviewData) ? $interviewData->site_contact : '' ?>';
$scope.interview.attached_file='<?php echo!empty($interviewData) ? $interviewData->saved_map : '' ?>';
$scope.interview.client_name='<?php echo!empty($interviewData) ? $interviewData->clnt_first_name.' '.$interviewData->clnt_last_name : '' ?>';
//$scope.interview.cand_name='<?php echo!empty($interviewData) ? $interviewData->first_name.' '.$interviewData->last_name : '' ?>';
//alert($scope.interview.candidate_id);
$scope.getTheFiles = function ($files) {
//alert();
        angular.forEach($files, function (value, key) {
            formdata.append('file', value);
        });

        var request = {
            method: 'POST',
            url: '<?php echo base_url();?>admin/upload_interview_map',
            data: formdata,
            enctype: 'multipart/form-data',
            headers: {
                'Content-Type': undefined
            }
        };
        $http(request)
                .success(function (data) {
                  console.log(data)
                    $scope.interview.filename = data.filename;
                    
                })
                .error(function () {
                });
    };
$scope.submitInterviewForm = function () {
var clientName=$("#organizationname").val();
var candidate_id='<?php echo $id?>';
var interview_date=$("#interview_date").val();
var candidateName=$("#candidateName").val();
$scope.interview.client_name=clientName;
$scope.interview.interview_date=interview_date;
if(candidate_id!=''){
$scope.interview.candidate_name=candidate_id;

}else{

$scope.interview.candidate_name=candidateName;

}
 if ($scope.interviewForm.$valid) {
$http({
      method: 'POST',
      dataType: 'json',
      url: '<?php echo base_url();?>admin/create_interview',

            data: $scope.interview, //forms interview object
            headers: {'Content-Type': 'application/json'}
          }).success(function(data){
              if(data.status=='1'){
                
             document.location='<?php echo base_url();?>admin/viewInterviewForm?id='+candidate_id;
              }else{


              }

          });

 }else{


 }   


};

});




</script>

<script type="text/javascript" src="<?php echo base_url();?>client_assets/js/client_js/autocomplete.js"></script>
<script>
    
    var a = $('#selctionajax').autocomplete({
      serviceUrl:'<?php echo base_url();?>admin/fetchClient',
      minChars:1,
      delimiter: /(,|;)\s*/, // regex or character
      maxHeight:600,
      //width:300,
      zIndex: 9999,
      deferRequestBy: 100, //miliseconds
       onSelect: function(suggestion) {
       //alert();
       if(suggestion.data)
                         {
           $('#organizationname').val(suggestion.data);

                        }
                        else
                        {
                        alert("sd");
                        }
}
      //params: { country:'Yes' }, //aditional parameters
      //noCache: false //default is false, set to true to disable caching
      // callback function:
      //onSelect: function(value, data){ alert('You selected: ' + value + ', ' + data); },
      // local autosugest options:
      //lookup: ['January', 'February', 'March', 'April', 'May'] //local lookup values
    });


    var b=$('#selectionajax').autocomplete({
      serviceUrl:'<?php echo base_url();?>admin/fetchCandidate',
      minChars:1,
      delimiter: /(,|;)\s*/, // regex or character
      maxHeight:600,
      //width:300,
      zIndex: 9999,
      deferRequestBy: 100, //miliseconds
       onSelect: function(suggestion) {
       //alert();
       if(suggestion.data)
                         {
           $('#candidateName').val(suggestion.data);

                        }
                        else
                        {
                        alert("sd");
                        }
}
      //params: { country:'Yes' }, //aditional parameters
      //noCache: false //default is false, set to true to disable caching
      // callback function:
      //onSelect: function(value, data){ alert('You selected: ' + value + ', ' + data); },
      // local autosugest options:
      //lookup: ['January', 'February', 'March', 'April', 'May'] //local lookup values
    });


    //$(".populate.m-bot15").select2();
    
  </script>