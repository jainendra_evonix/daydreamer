 <script src="https://cloud.tinymce.com/stable/tinymce.min.js?apiKey=hm7dom1mb6340hos81ewdzcl4lu4rfrxkzp3rqty7wn6rtz7"></script>

  <script>

  tinymce.init({
  selector: 'textarea',
  height: 500,
  theme: 'modern',
  plugins: [
    'advlist autolink lists link image charmap print preview hr anchor pagebreak',
    'searchreplace wordcount visualblocks visualchars code fullscreen',
    'insertdatetime media nonbreaking save table contextmenu directionality',
    'emoticons template paste textcolor colorpicker textpattern imagetools codesample toc help'
  ],
  toolbar1: 'undo redo | insert | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image',
  toolbar2: 'print preview media | forecolor backcolor emoticons | codesample help | mybutton | msgbutton',
  image_advtab: true,
  menubar: false,
  setup:function(editor){
  editor.addButton('mybutton', {
  text: "Subject",
 onclick: function () {
        editor.insertContent('&nbsp;<b>Subject!</b>&nbsp;');
      }
});

  editor.addButton('msgbutton', {
  text: "Message",
 onclick: function () {
        editor.insertContent('&nbsp;<b>Message!</b>&nbsp;');
      }
});

  },


  templates: [
    { title: 'Test template 1', content: 'Test 1' },
    { title: 'Test template 2', content: 'Test 2' }
  ],
  content_css: [
    '//fonts.googleapis.com/css?family=Lato:300,300i,400,400i',
    '//www.tinymce.com/css/codepen.min.css'
  ]
 });


 var postApp = angular.module('postApp',[]);
postApp.controller('postController', function ($scope, $http) {

$scope.email={};
//var a=tinymce.get('#msg').getContent();
//alert(a);
//console.log(tinyMCE.get('msg').getBody().textContent);


$scope.submitEmailForm = function(){
  // $scope.email.message = tinyMCE.get('textarea').getContent();
  //   alert(tinyMCE.get('textarea').getContent());
console.log($('#msg').val())
if ($scope.emailForm.$valid) {

  $http({
      method: 'POST',
      dataType: 'json',
      url: '<?php echo base_url();?>admin/insertEmailFormat',

            data: $scope.email, //forms client object
            headers: {'Content-Type': 'application/json'}
          });


}else{


}


};


});


  </script>

 <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.16.0/jquery.validate.js"></script>
  <script>
  $(document).ready(function(){
  $("#email_format").validate({

rules:{
'template_name':'required',
'subject':'required',
'message':'required'
},
messages:{
'template_name':"Please enter template name",
'subject':"Please enter subject",
'message':'Please enter message'
},

submitHandler:function(form){

form.submit();
}

 });
  
});

  </script>