
<style type="text/css">
.help-block{
color:red;

}
.autocomplete-suggestions {
  background-color: #fff;
  border: 1px solid #ddd;
  box-shadow: 0 3px 10px rgba(0, 0, 0, 0.2);
}
.autocomplete-suggestion {
  padding: 5px 12px;
}
.autocomplete-suggestion:hover {
  background-color: #333;
  color: #fff;
}
</style>
        

        <!--main content start-->
        <section id="adminsection" class="container">
            <section class="wrapper">
                <!-- page start-->
                <div class="row">
                    <div class="col-md-12">
                        <h4 class="form-heading"><strong>Create Group Interview</strong></h4>
                        <?php echo $this->session->flashdata('successmsg');?>
                        <?php echo $this->session->flashdata('errormsg');?>
                        <p>Create a Group Interview. <a href="<?php echo base_url();?>admin/viewGroupInterview" class="btn btn-info pull-right">View Group Interviews</a></p>
                        <br>
                        <section class="panel">
                            <div class="panel-body">
                                <div class="row">
                                    <div class="col-md-12">
                                        <h4>Group Information</h4>
                                        <hr>
                                    </div>
                                    <form class="form-horizontal bucket-form" ng-submit="submitGroupInterviewForm()" name="groupInterviewForm" novalidate>
                                       <input type="hidden" name="group_id" ng-model="interview.group_id"> 
                                    <div class="col-md-12">
                                            <div class="form-group">
                                                <label class="col-sm-3 col-sm-offset-1 control-label">Group Name:</label>
                                                <div class="col-sm-7">
                                                    <input type="text"  class="form-control" name="group_name" ng-model="interview.group_name" placeholder="Please enter Group Name" required> 
                                                    <span ng-show="submitted && groupInterviewForm.group_name.$error.required"  class="help-block has-error ng-hide">Group Name is required.</span>
                                                     <span ng-show="errorGroupName" class="help-block has-error ng-hide">{{errorGroupName}}</span>
                                                </div>
                                            </div>

                                            <div class="col-md-12">
                                            <div class="form-group">
                                                <label class="col-sm-3 col-sm-offset-1 control-label">Job Title:</label>
                                                <div class="col-sm-7">
                                                    <input type="text"  class="form-control" name="job_title" ng-model="interview.job_title" placeholder="Please enter Job Title" required> 
                                                    <span ng-show="submitted && groupInterviewForm.job_title.$error.required"  class="help-block has-error ng-hide">Job Title is required.</span>
                                                     <span ng-show="errorJobTitle" class="help-block has-error ng-hide">{{errorJobTitle}}</span>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label class="col-sm-3 col-sm-offset-1 control-label">Location:</label>
                                                <div class="col-sm-7">
                                                    <input type="text"  class="form-control" name="location" ng-model="interview.location" placeholder="Please enter Location" required> 
                                                    <span ng-show="submitted && groupInterviewForm.location.$error.required"  class="help-block has-error ng-hide">Location is required.</span>
                                                     <span ng-show="errorLocation" class="help-block has-error ng-hide">{{errorLocation}}</span>
                                                </div>
                                            </div>

                                        <div class="form-group">
                                       <label class="col-sm-3 col-sm-offset-1 control-label">Interview Date:</label>
                                       <div class="col-sm-7">
                                        <div class="input-group date" id="demo-date" style="width:100%">
                                          <div class="input-group-content">
                                            <input data-calenderopen type="text" class="form-control" autocomplete="off"  ng-model="interview.date" name="date" placeholder="Please enter Interview Date"  id="open_date"   style="margin-bottom:0px;">
                                            <span id="msg1" ng-show="submitted && groupInterviewForm.open_date.$invalid" class="help-block has-error">Date is required.</span>
                                            <!-- <span ng-show="errorValidity" class="help-block has-error ng-hide">{{errorValidity}}</span> -->
                                            <span ng-show="errorDate" class="help-block has-error ng-hide">{{errorDate}}</span>
                                          </div>
                                          <!-- <a href="javascript:void(0);" class="input-group-addon date"><i class="fa fa-calendar"></i></a> -->
                                        </div>
                                       
                                      </div>
                                    </div>
                   
                                            <div class="form-group">
                                                <label class="col-sm-3 col-sm-offset-1 control-label">Client Name:</label>
                                                <div class="col-sm-7">
                                                    <input type="text"  class="form-control" id="selctionajax" name="client" ng-model="interview.client" placeholder="Place enter Client Name" style="width: 617px; margin-right: 25px;">
                                                    <input type="hidden" name="client_name" ng-model="interview.client_name" id="organizationname">
                                                </div>
                                            </div>
                                          
                                          <br/>
                                    </div>



 
                <!-- page start-->
       
                  <div class="" id="candidates_table">
                    <div class="col-md-12">
                            <h4 class="form-heading"><strong>Candidate List</strong></h4>
                           
                            
                            <br>
               
                                <!-- <a><button type="submit" class="btn btn-info pull-right btn-sm" style="margin-bottom:18px;"><strong><i class="fa fa-plus"></i> Create New Office</strong></button><span class="pull-right"> &nbsp; &nbsp; </span></a> -->

                                <!-- <button type="button" class="btn btn-info pull-right btn-sm"><strong><i class="fa fa-times"></i> Cancel</strong></button> -->
                                <!-- <br> -->
                                <!-- <hr> -->
                            </div>
                            <div class="col-md-12">
                                <section class="">
                                    <div class="table-responsive" style="border: 1px solid #ccc;padding: 6px;">
                                        <table  id="employee-grid"  cellpadding="0" cellspacing="0" border="0" class="display" width="100%">
                                            <thead>
                                                <tr>
                                                    <th style="padding-left: 10px; padding-right: 20px; width: 18px;"><input type="checkbox" id="selectAll"></th>
                                                    <th style="width: 36px;">Sr.No</th>
                                                    <th>Candidate Name</th>
                                                    <th>City</th>
                                                    <th>Email</th>
                                                    <th>Profile Status</th>
                                                    <!-- <th>Job Applied</th> -->
                                                    <th>Action</th>

                                                </tr>
                                            </thead>
                                           
                                            </table>
                                        </div>
                                    </section>
                                </div>
                            </div>
                       
             
          
            <!-- page end-->

      
   
                                   
                                   
                   
                                    
                                    <div class="col-md-12">
                                        <br>
                                        <div class="form-group">
                                            <div class="col-sm-12">
                                                <button type="submit" class="btn btn-info pull-right btn-sm" ng-click="submitted = true"><strong><i class="fa fa-plus"></i> Submit</strong></button><span class="pull-right"> &nbsp; &nbsp; </span>

                                                <button type="button" class="btn btn-danger pull-right btn-sm"><strong><i class="fa fa-times"></i> Cancel</strong></button>
                                            </div>
                                        </div>
                                    </div>
                                    </form>
                                </div>
                            </div>
                        </section>
                    </div>
                </div>
                <!-- page end-->

            </section>
        </section>
        <!--main content end-->





    
</div>


<!-- 
<script type="text/javascript" src="js/ckeditor/ckeditor.js"></script> -->