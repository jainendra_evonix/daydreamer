                                           <style type="text/css">
                                           .help-block{

                                            color:red;
                                        }

                                        </style>
                                        <section class="panel container">
                                            <div class="panel-body">
                                             <h4>Job Advertisement</h4>
                                               <?php echo $this->session->flashdata('successmsg');?>
                                               <?php echo $this->session->flashdata('errormsg');?>
                                             <hr>

                                             <form class="form-horizontal bucket-form" ng-submit="submitJobAddsForm()" name="jobAddsForm" novalidate>
                                                <input type="hidden" name="package_id" ng-model="jobadds.package_id">
                                            <!-- <div class="form-group" >
                                                <label class="col-sm-3 control-label">Job Ads:</label>
                                                <div class="col-sm-9 icheck">
                                                    <div class="square single-row">
                                                        <div class="">
                                                            <input tabindex="3" type="radio" name="jobadds" value="yes" ng-model="jobadds.jobadds" onclick="showJobDiv();">

                                                            <label>Yes </label>
                                                        </div>
                                                        <div class="">
                                                            <input tabindex="3" type="radio" name="jobadds" value="no" ng-model="jobadds.jobadds" onclick="hideJobDiv();">
                                                            <label>No </label>
                                                        </div>
                                                    </div>
                                                    <span ng-show="submitted && jobAddsForm.jobadds.$error.required"  class="help-block has-error ng-hide">Job Ads is required</span>
                                                </div>
                                            </div> -->
                                            <br><br>
                                            <div class="form-group" id="job_adds_div">
                                                <div class="col-sm-3">
                                                    <label for="">No. of job ads:</label>
                                                    <input type="text" class="form-control" id="job_adds" name="job_adds" ng-model="jobadds.job_adds" placeholder="" required>
                                                    <span ng-show="submitted && jobAddsForm.job_adds.$error.required"  class="help-block has-error ng-hide">Job Ads is required</span>
                                                </div>
                                                <div class="col-sm-3">
                                                    <label for="">Type:</label>
                                                    <select class="form-control m-bot15" id="type" name="type" ng-model="jobadds.type"  required>
                                                        <option disabled="disabled" selected="selected">Select type</option>
                                                        <option>Standard</option>
                                                        <option>Premium</option>
                                                    </select>
                                                    <span ng-show="submitted && jobAddsForm.type.$error.required"  class="help-block has-error ng-hide">Type is required</span>
                                                    
                                                </div>
                                                <div class="col-sm-3">
                                                    <label for="">Duration (Days):</label>
                                                    <select class="form-control m-bot15" id="duration" name="duration" ng-model="jobadds.duration" required>
                                                        <option disabled="disabled" selected="selected">Select duration</option>
                                                        <option>1</option>
                                                        <option>2</option>
                                                        <option>3</option>
                                                        <option>4</option>
                                                        <option>5</option>
                                                        <option>6</option>
                                                        <option>7</option>
                                                        <option>8</option>
                                                        <option>9</option>
                                                        <option>10</option>
                                                        <option>11</option>
                                                        <option>12</option>
                                                        <option>13</option>
                                                        <option>14</option>
                                                        <option>15</option>
                                                        <option>16</option>
                                                        <option>18</option>
                                                        <option>19</option>
                                                        <option>20</option>
                                                        <option>21</option>
                                                        <option>22</option>
                                                        <option>23</option>
                                                        <option>24</option>
                                                        <option>25</option>
                                                        <option>26</option>
                                                        <option>28</option>
                                                        <option>29</option>
                                                        <option>30</option>
                                                        <option>31</option>
                                                        
                                                    </select>
                                                     <span ng-show="submitted && jobAddsForm.duration.$error.required"  class="help-block has-error ng-hide">Duration is required</span>
                                                    
                                                </div>
                                                


                                                
                                                <div class="col-sm-9">
                                                    <hr>
                                                    <button type="submit" class="btn btn-info btn-sm pull-right" ng-click="submitted = true" id="btn"><strong><i class="fa fa-plus"></i> Save <i class="fa fa-"></i></i></strong></button><span class="pull-right"> &nbsp; &nbsp; </span>
                                                    <br>
                                                </div>
                                            </form>
                                        </div>

                                        <div class="form-group">
                                            <div class="col-sm-9">
                                                <br>
                                                <table class="table table-bordered table-condensed">
                                                    <thead style="background-color: #474752; color: #fff;">
                                                      <tr>
                                                        <th>Number of Ads</th>
                                                        <th>Type</th>
                                                        <th>Duration (Days)</th>
                                                        <th>Action</th>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                   <?php 
                                                      foreach($fetchPackageData as $key){

                                                   ?>
                                                  <tr>
                                                    <td><?php echo $key->job_adds;?></td>
                                                    <td><?php echo $key->type;?></td>
                                                    <td><?php echo $key->duration;?></td>
                                                    <td><a href="<?php echo base_url();?>admin/deleteJobAdd?id=<?php echo $key->id;?>&&package_id=<?php echo $id;?>" onclick="return deleteJobAdv();"><i class="fa fa-trash"></i> Delete</a></td>
                                                </tr>
                                                <?php }?>



                                            </tbody>
                                        </table>
                                         <a href="<?php echo base_url();?>admin/createPackage"  class="btn btn-danger btn-sm pull-right" ng-click="submitted = true" id="btn"><strong><i class="fa fa-arrow-left"></i> Complete <i class="fa fa-"></i></i></strong></a>
                                    </div>
                                </div>
                            </div>
                        </section>