
<!--main content start-->
<section id="adminsection" class="container">
    <section class="wrapper">
        <!-- page start-->
        <div class="row">
            <div class="col-md-12">
                <?php echo $this->session->flashdata('successmsg');  ?>
                <?php echo $this->session->flashdata('errormsg');  ?>
                <h4 class="form-heading"><strong>All Clients Jobs</strong></h4>
                <!-- <p>Create and showcase all Job Openings. <a href="#" class="pull-right">Help <i class="fa fa-question-circle"></i></a></p> -->

                <br>
                <section class="panel">
                    <div class="panel-body">
                        <div class="row">
                          <form method="post" action="<?php echo base_url();?>admin/viewJobs">
                            <div class="col-md-9 pageSearch">
                                <div class="searchform">
                                  <div class="row">
                                    <div class="col-md-3 col-sm-3">
                                      <input type="text" name="job_title" class="form-control" placeholder="Job Title">
                                  </div>
                                  <div class="col-md-3 col-sm-2">
                                      <!-- <select class="form-control">
                                        <option>Industry</option>
                                    </select> -->
                                    <input  type="text" id="closeDate" name="closing_date"  class="form-control" placeholder="Closing Date">
                                </div>
                                <!--  <div class="col-md-4 col-sm-3">
                                      <input type="text" class="form-control" placeholder="Closing Date">
                                  </div> -->
                                <div class="col-md-3 col-sm-2">
                                  <select class="form-control" name="industry">
                                    <option disabled="disabled" selected="selected" value="">Select your Industry</option>
                                    <?php
                                     foreach($allIndustries as $key){

                                    ?>

                                    <option value="<?php echo $key->ind_id?>"><?php echo $key->industry_name;?></option>
                                    <?php } ?>
                                </select>
                            </div>
                            <div class="col-md-2 col-sm-3">
                              <!-- <select class="form-control">
                                <option>Salary</option>
                            </select> -->
                            <input type="text" name="salary" class="form-control" placeholder="Salary">
                        </div>
                        <div class="col-md-1 col-sm-2 nopadding">
                          <button type="submit" class="btn"><i class="fa fa-search" aria-hidden="true"></i></button>
                      </div>
                  </div>
              </div>
          </div>
          </form>
          <div class="col-md-8">
            <!-- <button type="submit" class="btn btn-info pull-right btn-sm"><strong><i class="fa fa-plus"></i> New Job Opening</strong></button><span class="pull-right"> &nbsp; &nbsp; </span> -->

            <!-- <button type="button" class="btn btn-info pull-right btn-sm"><strong><i class="fa fa-times"></i> Cancel</strong></button> -->
            <!-- <br> -->
            <!-- <hr> -->
        </div>
        <div class="col-md-12">
            <section class="">
                <div class="adv-table">
                    <table  class="display table table-bordered table-striped table-condensed" id="dynamic-table">
                        <thead>
                            <tr>
                             
                             <th style="width: 5%;">Sr.No</th>
                             <th style="width: 20%;">Job Title</th>
                             <th style="width: 15%">Recruiter</th>
                            <!--  <th style="width: 15%;">Manager</th>
                             <th style="width: 15%;" >Positions</th>
                             <th style="width: 15%x">City</th> -->
                             <th style="width: 5%;">Job Status</th>
                             <th style="width: 5%;">Salary</th>
                             <th style="width: 10%;">Client Name</th>
                             <th style="width: 10%;">Created Date</th>
                             <th style="width: 10%;">Closing Date</th>
                             <!-- <th style="width: 10%;">Industry</th> -->
                             <th style="width: 10%">Action</th>
                         </tr>
                     </thead>
                     <tbody>
                        <?php
                        $count=1;
                        foreach($allJobs as $key){
                         
                                                // echo '<pre>';print_r($key);exit;
                            $client=$key->subclient_name;
                            //echo $client;exit;
                            if($client!=''){

                              $client_name=$client;
                            }else{

                              $client_name=$key->parent_client;
                            }
                            ?>
                            <tr class="gradeX">
                                <td style="width: 5%;"><?php echo $count++;?></td>
                                <td style="width: 20%;"><a target="_blank" href="<?php echo base_url();?>admin/viewJobDescription?id=<?php echo $key->j_id;?>"><?php echo $key->job_title;?></a></td>
                                <td style="width: 15%;"><?php echo $key->recruiter;?></td>
                                <!-- <td style="width: 15%;"><?php //echo $key->manager;?></td>
                                
                                <td style="width: 15%;"><?php //echo $key->positions;?></td>  
                                <td style="width: 15%;"><?php //echo $key->city;?></td> --> 
                                <td style="width: 5%;"><?php echo $key->status_name;?></td>
                                <td style="width: 5%;"><?php echo $key->salary;?></td>
                                <td style="width: 10%;"><?php echo $client_name;?></td>
                                <td style="width: 10%;"><?php echo date('jS M Y',strtotime($key->created_date))?></td>
                                <td style="width: 10%;"><?php echo date('jS M Y',strtotime($key->open_date))?></td>
                               <!--  <td style="width: 10%;"><?php //echo $key->industry_name;?></td> -->
                                <td style="width: 10%">
                                   <ul class="list-unstyled list-inline" style="padding-left: 0px;">
                                    <?php if($key->job_approve =='0'){?>
                                    <li onclick="return confirmJobDisapprove();"><a href="<?php echo base_url();?>admin/disApproveJob?id=<?php echo $key->j_id;?>" title="approve job"><i class="fa fa-thumbs-o-up fa-2x" aria-hidden="true"></i></a></li> 
                                    <?php } else {?>
                                    <li onclick="return confirmApprove();"><a href="<?php echo base_url();?>admin/approveJob?id=<?php echo $key->j_id;?>"title="disapprove job"><i class="fa fa-thumbs-down fa-2x" aria-hidden="true"></i></a></li> 
                                    <?php }?>
                                    
                                    <li><a href="<?php echo base_url();?>admin/viewJobDescription?id=<?php echo $key->j_id;?>"><i class="fa fa-eye" aria-hidden="true"></i></a></li>

                                    <li><a href="<?php echo base_url();?>admin/updateJobs?id=<?php echo $key->j_id;?>"><i class="fa fa-edit" aria-hidden="true"></i></a></li>
                                </ul>
                            </td>
                        </tr>
                        <?php }?>
                    </tbody>
                </table>
            </div>
        </section>
    </div>
</div>
</div>
</section>
</div>
</div>
<!-- page end-->

</section>
</section>
<!--main content end-->






</div>



<!--dynamic table initialization -->
