
<style type="text/css">
.help-block{
color:red;

}
.autocomplete-suggestions {
  background-color: #fff;
  border: 1px solid #ddd;
  box-shadow: 0 3px 10px rgba(0, 0, 0, 0.2);
}
.autocomplete-suggestion {
  padding: 5px 12px;
}
.autocomplete-suggestion:hover {
  background-color: #333;
  color: #fff;
}

</style>
        

        <!--main content start-->
        <section id="adminsection" class="container">
            <section class="wrapper">
                <!-- page start-->
                <div class="row">
                    <div class="col-md-12">
                        <h4 class="form-heading"><strong>Create New Interview</strong></h4>
                        <?php echo $this->session->flashdata('successmsg');?>
                        <?php echo $this->session->flashdata('errormsg');?>
                        <p>Create and add a new Interview. <a href="#" class="pull-right">Help <i class="fa fa-question-circle"></i></a></p>
                        <br>
                        <section class="panel">
                            <div class="panel-body">
                                <div class="row">
                                    <div class="col-md-12">
                                        <h4>Create Interview</h4>
                                        <hr>
                                    </div>
                                    <form class="form-horizontal bucket-form" ng-submit="submitInterviewForm()" name="interviewForm" novalidate>
                                       <input type="hidden" name="candidate_id" ng-model="interview.candidate_id"> 
                                       <input type="hidden" name="interview_id" ng-model="interview.interview_id">
                                    <div class="col-md-6">
                                            <div class="form-group">
                                                <label class="col-sm-3 col-sm-offset-1 control-label">Candidate Name:</label>
                                                <div class="col-sm-7">
                                                  <!--   <input type="text"  class="form-control" name="candidate_name" ng-model="interview.candidate_name" required>  -->

                                                   <!--  <span ng-show="submitted && interviewForm.candidate_name.$error.required"  class="help-block has-error ng-hide">Candidate Name is required.</span>
                                                     <span ng-show="errorCandidateName" class="help-block has-error ng-hide">{{errorCandidateName}}</span> -->
                                                     <input type="text"  class="form-control"   id="selectionajax" name="cand_name" ng-model="interview.cand_name">
                                                     <input type="hidden" name="candidate_name" ng-model="interview.candidate_name" id="candidateName">
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label class="col-sm-3 col-sm-offset-1 control-label">Interview location:</label>
                                                <div class="col-sm-7">
                                                    <input type="text"  class="form-control" name="location" ng-model="interview.location" required>
                                                    <span ng-show="submitted && interviewForm.location.$error.required"  class="help-block has-error ng-hide">Interview Location is required.</span>
                                                     <span ng-show="errorlocation" class="help-block has-error ng-hide">{{errorlocation}}</span>
                                                </div>
                                            </div>

                                           <div class="form-group">
                                       <label class="col-sm-4  control-label">Interview date:</label>
                                       <div class="col-sm-7">
                                        <div class="input-group date" id="demo-date" style="width:100%">
                                          <div class="input-group-content">
                                            <input data-calenderopen type="text" class="form-control" autocomplete="off"  ng-model="interview.interview_date" name="interview_date" placeholder=""  id="interview_date"   style="margin-bottom:0px;">
                                            <span id="msg1" ng-show="submitted && interviewForm.interview_date.$invalid" class="help-block has-error">Interview Date is required.</span>
                                            <!-- <span ng-show="errorValidity" class="help-block has-error ng-hide">{{errorValidity}}</span> -->
                                            <span ng-show="errorValidity" class="help-block has-error ng-hide">{{errorValidity}}</span>
                                          </div>
                                          <!-- <a href="javascript:void(0);" class="input-group-addon date"><i class="fa fa-calendar"></i></a> -->
                                        </div>
                                      </div>
                                    </div>


                                            <div class="form-group">
                                                <label class="col-sm-3 col-sm-offset-1 control-label">Main Contact:</label>
                                                <div class="col-sm-7">
                                                    <input type="text"  class="form-control" name="main_contact" ng-model="interview.main_contact" required>
                                                    <span ng-show="submitted && interviewForm.main_contact.$error.required"  class="help-block has-error ng-hide">Main Contact is required.</span>
                                                    <span ng-show="errorMainContact" class="help-block has-error ng-hide">{{errorMainContact}}</span>
                                                </div>
                                            </div>

                                            <div class="form-group">
                                                <label class="col-sm-3 col-sm-offset-1 control-label">Interview Address:</label>
                                                <div class="col-sm-7">
                                                    <textarea rows="4" cols="4" class="form-control" name="interview_address" ng-model="interview.interview_address" style="resize:none;" required></textarea>
                                                    <span ng-show="submitted && interviewForm.interview_address.$error.required"  class="help-block has-error ng-hide">Interview Address is required.</span>
                                                    <span ng-show="errorInterviewAddress" class="help-block has-error ng-hide">{{errorInterviewAddress}}</span>
                                                </div>
                                            </div>

                                            <div class="form-group">
                                                <label class="col-sm-3 col-sm-offset-1 control-label">Breakdown of Interview Details:</label>
                                                <div class="col-sm-7">
                                                    <input type="text"  class="form-control" name="breakdown_details" ng-model="interview.breakdown_details" required>
                                                    <span ng-show="submitted && interviewForm.breakdown_details.$error.required"  class="help-block has-error ng-hide">Breakdown of Interview Details is required.</span>
                                                    <span ng-show="errorBreakdownDetails" class="help-block has-error ng-hide">{{errorBreakdownDetails}}</span>
                                                </div>
                                            </div>


                                            <div class="form-group">
                                                <label class="col-sm-3 col-sm-offset-1 control-label">Additional Notes:</label>
                                                <div class="col-sm-7">
                                                    <textarea rows="4" cols="4"  class="form-control" name="additional_notes" ng-model="interview.additional_notes" style="resize:none;" required></textarea>
                                                    <span ng-show="submitted && interviewForm.additional_notes.$error.required"  class="help-block has-error ng-hide">Additional Notes is required.</span>
                                                    <span ng-show="errorAdditionalNotes" class="help-block has-error ng-hide">{{errorAdditionalNotes}}</span>
                                                </div>
                                            </div>
                                        
                                    </div>
                                    <div class="col-md-6">
                                            <div class="form-group">
                                                <label class="col-sm-3 col-sm-offset-1 control-label">Event start time:</label>
                                                <div class="col-sm-7">
                                                     <input type="text"  class="form-control" name="start_time" ng-model="interview.start_time" required>
                                                    <span ng-show="submitted && interviewForm.start_time.$error.required"  class="help-block has-error ng-hide">Start time is required.</span>
                                                     <span ng-show="errorStarttime" class="help-block has-error ng-hide">{{errorStarttime}}</span>
                                                </div>
                                            </div>
                                         
                                            <div class="form-group">
                                                <label class="col-sm-3 col-sm-offset-1 control-label">Event finish time:</label>
                                                <div class="col-sm-7">
                                                    <input type="text"  class="form-control" name="finish_time" ng-model="interview.finish_time" required>
                                                     <span ng-show="submitted && interviewForm.finish_time.$error.required"  class="help-block has-error ng-hide">Finish time is required.</span>
                                                     <span ng-show="errorFinishtime" class="help-block has-error ng-hide">{{errorFinishtime}}</span>
                                                </div>
                                            </div>


                                            <div class="form-group">
                                                <label class="col-sm-3 col-sm-offset-1 control-label">Client Name:</label>
                                                <div class="col-sm-7">
                                                    <!-- <input type="text"  class="form-control" name="client_name" ng-model="interview.client_name" required>
                                                     <span ng-show="submitted && interviewForm.client_name.$error.required"  class="help-block has-error ng-hide">Client Name is required.</span>
                                                     <span ng-show="errorClientName" class="help-block has-error ng-hide">{{errorClientName}}</span> -->
                                                     <input type="text"  class="form-control"   id="selctionajax" name="client_name" ng-model="interview.client_name">
                                                     <input type="hidden" name="client_name" ng-model="interview.client_name" id="organizationname">

                                                     <span ng-show="submitted && interviewForm.client_name.$error.required"  class="help-block has-error ng-hide">Client Name is required.</span>
                                                     <span ng-show="errorClientName" class="help-block has-error ng-hide">{{errorClientName}}</span>
                                                </div>
                                            </div>

                                             <div class="form-group">
                                                <label class="col-sm-3 col-sm-offset-1 control-label">Contact details:</label>
                                                <div class="col-sm-7">
                                                    <input type="text"  class="form-control" name="contact_details" ng-model="interview.contact_details" required>
                                                     <span ng-show="submitted && interviewForm.contact_details.$error.required"  class="help-block has-error ng-hide">Contact Details is required.</span>
                                                     <span ng-show="errorConatactDetails" class="help-block has-error ng-hide">{{errorConatactDetails}}</span>
                                                </div>
                                            </div>
                                             <div class="form-group">
                                                <label class="col-sm-3 col-sm-offset-1 control-label">Site Contact:</label>
                                                <div class="col-sm-7">
                                                    <input type="text"  class="form-control" name="site_contact" ng-model="interview.site_contact" required>
                                                     <span ng-show="submitted && interviewForm.site_contact.$error.required"  class="help-block has-error ng-hide">Site Contact is required.</span>
                                                     <span ng-show="errorSiteContact" class="help-block has-error ng-hide">{{errorSiteContact}}</span>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label class="control-label col-sm-3 col-sm-offset-1 text-right">Saved Map:</label>
                                                <div class="controls col-sm-7">


                                                    <input id="uploadFile" class="default ng-pristine ng-valid ng-empty ng-touched" file-model="myFile" ng-model="inteview.attached_file" name="attached_file" ng-files="getTheFiles($files)" type="file">
                                                 <span ng-show="submitted && interviewForm.attached_file.$error.required"  class="help-block has-error ng-hide">Image is required.</span>
                                                 <span ng-show="errorFile" class="help-block has-error ng-hide">{{errorFile}}</span><br/>
                                                 <span ng-bind="inteview.attached_file"></span>
                                             </div>
                                         </div>
                                             
                                    </div>



                                   
                   
                                   
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <div class="col-sm-12">
                                                <button type="submit" class="btn btn-info pull-right btn-sm" ng-click="submitted = true"><strong><i class="fa fa-plus"></i> Create Interview</strong></button><span class="pull-right"> &nbsp; &nbsp; </span>

                                                <a href="<?php echo base_url();?>admin/viewInterviewForm?id=<?php echo $id;?>" class="btn btn-danger pull-right btn-sm"><strong><i class="fa fa-times"></i> Cancel</strong></a>
                                            </div>
                                        </div>
                                    </div>
                                    </form>
                                </div>
                            </div>
                        </section>
                    </div>
                </div>
                <!-- page end-->

            </section>
        </section>
        <!--main content end-->





    
</div>


<!-- 
<script type="text/javascript" src="js/ckeditor/ckeditor.js"></script> -->