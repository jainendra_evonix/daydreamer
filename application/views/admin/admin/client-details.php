<?php include 'header.php' ?>

        <!--main content start-->
        <section id="adminsection" class="container">
            <section class="wrapper">
                <!-- page start-->
                <div class="row">
                    <div class="col-md-12">
                        <h4 class="form-heading"><strong>Jainendra Pal</strong></h4>
                        <p>Lorem ipsum dolor sit amet. <a href="#" class="pull-right">Help <i class="fa fa-question-circle"></i></a></p>

                        <br>
                        <section class="panel">
                            <div class="panel-body">
                                <div class="row">
                                    <div class="col-md-12">
                                        <h4>Basic Information</h4>
                                        <hr>
                                    </div>
                                    <form class="form-horizontal bucket-form" method="get" action="education.php">
                                        
                                    <div class="col-md-6 form-group-condensed">
                                        <div class="form-group">
                                            <label class="col-sm-5 control-label">Client Name:</label>
                                            <div class="col-sm-7">
                                                <p class="form-control-static">Infosys Pune</p>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="col-sm-5 control-label">Contact Person:</label>
                                            <div class="col-sm-7">
                                                <p class="form-control-static">Archit Srivastava</p>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="col-sm-5 control-label">Contact No:</label>
                                            <div class="col-sm-7">
                                                <p class="form-control-static">(985) 847-1524</p>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="control-label col-sm-5">Fax:</label>
                                            <div class="col-sm-7">
                                                <p class="form-control-static">(985) 847-1524</p>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-6 form-group-condensed">
                                        <div class="form-group">
                                            <label class="col-sm-5 control-label">Industry Type:</label>
                                            <div class="col-sm-7">
                                                <p class="form-control-static">IT Services</p>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="col-sm-5 control-label">Parent Client:</label>
                                            <div class="col-sm-7">
                                                <p class="form-control-static">Infosys</p>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="control-label col-sm-5">Account Manager:</label>
                                            <div class="col-sm-7">
                                                <p class="form-control-static">Jainendra Pal</p>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="col-sm-5 control-label">Website:</label>
                                            <div class="col-sm-7">
                                                <p class="form-control-static"><a href="#">www.infosys.com/pune</a></p>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="col-md-12">
                                        <br>
                                        <h4>Address Information</h4>
                                        <hr>
                                    </div>
                                    <div class="col-md-12">
                                        <table class="table table-bordered clientinfo">
                                            <tbody>
                                              <tr>
                                                <td class="text-right"><strong>Address Line 1:</strong></td>
                                                <td>Lorem ipsum</td>
                                                <td class="text-right"><strong>Address Line 2:</strong></td>
                                                <td>Lorem ipsum</td>
                                              </tr>
                                              <tr>
                                                <td class="text-right"><strong>Street Name:</strong></td>
                                                <td>Hinjawadi Phase 2 Road</td>
                                                <td class="text-right"><strong>City:</strong></td>
                                                <td>Pune</td>
                                              </tr>
                                              <tr>
                                                <td class="text-right"><strong>Pincode:</strong></td>
                                                <td>411059</td>
                                                <td class="text-right"><strong>Shipping State:</strong></td>
                                                <td>India</td>
                                              </tr>
                                            </tbody>
                                          </table>
                                    </div>

                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <div class="col-sm-12">
                                                <br>
                                                <button type="submit" class="btn btn-info pull-right btn-sm"><strong><i class="fa fa-edit"></i> Edit</strong></button><span class="pull-right"> &nbsp; &nbsp; </span>

                                                <button type="button" class="btn btn-danger pull-right btn-sm"><strong><i class="fa fa-times"></i> Delete</strong></button>
                                            </div>
                                        </div>
                                    </div>
                                    </form>
                                </div>
                            </div>
                        </section>
                    </div>
                </div>
                <!-- page end-->

            </section>
        </section>
        <!--main content end-->

</div>

<?php include 'footer.php' ?>
