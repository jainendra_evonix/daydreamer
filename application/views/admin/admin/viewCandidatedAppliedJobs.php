
<style type="text/css">
    .mylabel.label-success {
        background-color: #5cb85c !important;
    }
</style>

<link rel="stylesheet" href="js/data-tables/DT_bootstrap.css" />

<!--main content start-->
<section id="adminsection" class="container">
    <section class="wrapper">
        <!-- page start-->
        <div class="row">
            <div class="col-md-12">
                <h4 class="form-heading"><strong>Candidate Applied Jobs</strong></h4>
                <?php echo $this->session->userdata('successmsg');?>
                 <?php echo $this->session->userdata('errormsg');?>
                <p> <a href="#" class="pull-right">Help <i class="fa fa-question-circle"></i></a></p>

                <br>
                <section class="panel">
                    <div class="panel-body">
                        <div class="row">
                            <div class="col-md-12">
                                <!-- <a><button type="submit" class="btn btn-info pull-right btn-sm" style="margin-bottom:18px;"><strong><i class="fa fa-plus"></i> Create New Office</strong></button><span class="pull-right"> &nbsp; &nbsp; </span></a> -->

                                <!-- <button type="button" class="btn btn-info pull-right btn-sm"><strong><i class="fa fa-times"></i> Cancel</strong></button> -->
                                <!-- <br> -->
                                <!-- <hr> -->
                            </div>
                            <div class="col-md-12">
                                <section class="">
                                    <div class="table-responsive" style="border: 1px solid #ccc;padding: 6px;">
                                        <table  id="employee-grid"  cellpadding="0" cellspacing="0" border="0" class="display" width="100%">
                                            <thead>
                                                <tr>
                                                    <th>Sr.No</th>
                                                    <!-- <th>Candidate Name</th> -->
                                                    <th>City</th>
                                                    <th>Email</th>
                                                    <th>Job</th>
                                                     <th>Salary</th>
                                                    <th>Action</th>

                                                </tr>
                                            </thead>
                                           
                                            </table>
                                        </div>
                                    </section>
                                </div>
                            </div>
                        </div>
                    </section>
                </div>
            </div>
            <!-- page end-->

        </section>
    </section>
    <!--main content end-->





    
</div>




<!--dynamic table initialization -->
