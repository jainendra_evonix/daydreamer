 <style type="text/css">
 .help-block{

    color:red;
}
</style>

<!--main content start-->
<section id="adminsection" class="container">
    <section class="wrapper">
        <!-- page start-->
        <div class="row">
            <div class="col-md-12">
                <h4 class="form-heading"><strong>Create a Blog</strong></h4>
                <?php echo $this->session->flashdata('successmsg');?>
                <?php echo $this->session->flashdata('errormsg');?>

                <!-- <p>Create and add a new office. <a href="#" class="pull-right">Help <i class="fa fa-question-circle"></i></a></p> -->
                <br>
                <section class="panel">
                    <div class="panel-body">
                        <div class="row">
                            <div class="col-md-12">
                                <h4>Blog Information</h4>
                                <hr>
                            </div>
                            <form class="form-horizontal bucket-form" ng-submit="submitBlogForm()" name="blogForm" novalidate>
                             <input type="hidden" name="admin_id" ng-model="blog.admin_id">
                             <div class="col-md-6">
                                <div class="form-group">
                                    <label class="col-sm-3 col-sm-offset-1 control-label">Name:</label>
                                    <div class="col-sm-7">
                                        <input type="text"  class="form-control" name="name" ng-model="blog.name" required> 
                                        <span ng-show="submitted && blogForm.name.$error.required"  class="help-block has-error ng-hide">Admin Name is required.</span>
                                        <span ng-show="errorName" class="help-block has-error ng-hide">{{errorName}}</span>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-sm-3 col-sm-offset-1 control-label">Blog Title:</label>
                                    <div class="col-sm-7">
                                        <input type="text"  class="form-control" name="blog_title" ng-model="blog.blog_title" required>
                                        <span ng-show="submitted && blogForm.blog_title.$error.required"  class="help-block has-error ng-hide">Blog Title is required.</span>
                                        <span ng-show="errorBlogTitle" class="help-block has-error ng-hide">{{errorBlogTitle}}</span>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label class="control-label col-sm-3 col-sm-offset-1 text-right">Blog Image:</label>
                                    <div class="controls col-sm-7">


                                     <input id="uploadFile" class="default ng-pristine ng-valid ng-empty ng-touched" file-model="myFile" ng-model="blog.attached_file" name="attached_file" ng-files="getTheFiles($files)" type="file" onclick="hideMsg();">
                                     <span ng-show="submitted && blogForm.attached_file.$error.required"  class="help-block has-error ng-hide">Blog Image is required.</span>
                                     <span ng-show="errorFile" class="help-block has-error ng-hide">{{errorFile}}</span><br/>
                                     <span style="color:red;" id="message2"></span>
                                     <span ng-bind="blog.attached_file"></span>
                                 </div>
                             </div>
                             <div class="form-group">
                                <label class="col-sm-3 col-sm-offset-1 control-label">Blog Description:</label>
                                <div class="col-sm-7">
                                    <textarea type="text" rows"8" cols="4"  class="form-control" name="blog_desc" ng-model="blog.blog_desc" required></textarea>
                                    <span ng-show="submitted && blogForm.blog_desc.$error.required"  class="help-block has-error ng-hide">Blog Description is required.</span>
                                    <span ng-show="errorBlogDesc" class="help-block has-error ng-hide">{{errorBlogDesc}}</span>
                                </div>

                            </div>

                            <div class="form-group">
                                <label class="control-label col-sm-3 col-sm-offset-1 text-right">Blog Video:</label>
                                <div class="controls col-sm-7">

                                <input id="uploadFile" class="default ng-pristine ng-valid ng-empty ng-touched" file-model="myFile" ng-model="blog.video_file" name="video_file" ng-files="getTheVideos($files)" type="file" onclick="hideMsg();">
                                 <span ng-show="submitted && blogForm.video_file.$error.required"  class="help-block has-error ng-hide">Blog Video is required.</span>
                                 <span ng-show="errorFile" class="help-block has-error ng-hide">{{errorFile}}</span><br/>
                                 <span style="color:red;" id="message2"></span>
                                 <span ng-bind="blog.attached_file"></span>
                             </div>
                         </div>




                     </div>
                     <div class="col-md-6">



                     </div>
                 </div>

                 <div class="col-md-12">
                    <div class="form-group">
                        <div class="col-sm-12">
                            <button type="submit" class="btn btn-info pull-right btn-sm" ng-click="submitted = true"><strong>Submit</strong></button><span class="pull-right"> &nbsp; &nbsp; </span>

                            <a href="<?php echo base_url();?>admin/viewBlogForm"><button type="button" class="btn btn-danger pull-right btn-sm"><strong><i class="fa fa-times"></i> Cancel</strong></button></a>
                        </div>
                    </div>
                </div>
            </form>
        </div>
    </div>
</section>
</div>
</div>
<!-- page end-->

</section>
</section>
<!--main content end-->




