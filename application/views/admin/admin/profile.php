
<style type="text/css">
.help-block{
  color:red;

}
.profileimg {
  width:250px;
  height:250px;
  position: relative;
  display: block;
  margin: auto;
  background-color: #fff;
}
.editprofileimg {
  position: absolute;
  bottom: 46px;
  right: 46px;
  width: 40px;
  height: 40px;
  background-color: rgba(0, 0, 0, 0.7);
  color: #ccc;
  padding: 7px;
  font-size: 18px;
  text-align: center;
  border-radius: 4px;
}
.editprofileimg:hover {
  color: #fff;
  background-color: rgba(0, 0, 0, 1);
}

.cropit-preview 

{ 
  background-color: #f8f8f8; 
  background-size: cover;
  border: 1px solid #ccc;
  border-radius: 3px; 
  margin-top: 7px;
  width: 202px;
  height: 202px;

}
.cropit-preview-image-container 
{ 
  cursor: move; 
} 
.image-size-label 
{ 
  margin-top: 10px; 
} 
input,.export
{
 display: block; 
} 
button 
{ 
  margin-top: 10px; 
} 
.req{ 
  color:red; 
} 


.socialfgroup {
  position: relative;
}
.socialerror {
    bottom: -30px;
    position: absolute;
}
</style>


<!--main content start-->
<section id="adminsection" class="container">
  <section class="wrapper">
    <!-- page start-->
    <div class="row">
      <div class="col-md-12">
        <h4 class="form-heading"><strong>Edit your profile</strong></h4>
        <?php echo $this->session->flashdata('successmsg');?>
        <?php echo $this->session->flashdata('errormsg');?>
        <!-- <p>Create and add a new office. <a href="#" class="pull-right">Help <i class="fa fa-question-circle"></i></a></p> -->
        <br>
        <section class="panel">
          <div class="panel-body">
            <div class="row">
              <div class="col-md-12">
                <h4>Admin Information</h4>
                <hr>
              </div>
              <form class="form-horizontal bucket-form" ng-submit="submitprofileForm()" name="profileForm" novalidate>

                <div class="col-md-6">
                  <div class="form-group">
                    <label class="col-sm-3 col-sm-offset-1 control-label">First Name:</label>
                    <div class="col-sm-7">
                      <input type="text"  class="form-control" name="first_name" ng-model="profile.first_name" required> 
                      <span ng-show="submitted && profileForm.first_name.$error.required"  class="help-block has-error ng-hide">First Name is required.</span>
                      <span ng-show="errorFirstName" class="help-block has-error ng-hide">{{errorFirstName}}</span>
                    </div>
                  </div>
                  <div class="form-group">
                    <label class="col-sm-3 col-sm-offset-1 control-label">Last Name:</label>
                    <div class="col-sm-7">
                      <input type="text"  class="form-control" name="last_name" ng-model="profile.last_name" required>
                      <span ng-show="submitted && profileForm.last_name.$error.required"  class="help-block has-error ng-hide">Last Name is required.</span>
                      <span ng-show="errorLastName" class="help-block has-error ng-hide">{{errorLastName}}</span>
                    </div>
                  </div>
                  <div class="form-group">
                    <label class="col-sm-3 col-sm-offset-1 control-label">Telephone:</label>
                    <div class="col-sm-7">
                      <input type="text"  class="form-control" name="telephone" ng-model="profile.telephone" required>
                      <span ng-show="submitted && profileForm.telephone.$error.required"  class="help-block has-error ng-hide">Telephone is required.</span>
                      <span ng-show="errorTelephone" class="help-block has-error ng-hide">{{errorTelephone}}</span>
                    </div>


                  </div>

                  <div class="form-group">
                    <label class="col-sm-3 col-sm-offset-1 control-label">Email:</label>
                    <div class="col-sm-7">
                      <input type="text"  class="form-control" name="telephone" ng-model="profile.email" required>
                      <span ng-show="submitted && profileForm.email.$error.required"  class="help-block has-error ng-hide">Email is required.</span>
                      <span ng-show="errorEmail" class="help-block has-error ng-hide">{{errorEmail}}</span>
                    </div>
                  </div>

                  <div class="form-group">
                    <label class="col-sm-3 col-sm-offset-1 control-label">Address:</label>
                    <div class="col-sm-7">
                      <textarea rows="4" cols="4"  class="form-control" name="address" ng-model="profile.address" required style="resize:none;"></textarea>
                      <span ng-show="submitted && profileForm.address.$error.required"  class="help-block has-error ng-hide">Address is required.</span>
                      <span ng-show="errorAddress" class="help-block has-error ng-hide">{{errorAddress}}</span>
                    </div>
                  </div>
                  
                  <div class="form-group">
                    <label class="col-sm-3 col-sm-offset-1 control-label">Facebook:</label>
                    <div class="col-sm-7">
                      <div class="input-group m-bot15 socialfgroup">
                        <div class="input-group-addon"><i class="fa fa-facebook"></i></div>
                        <input type="url" class="form-control" ng-model="profile.fb" name="fb"  placeholder="Facebook"  required>

                        <span ng-show="submitted && profileForm.fb.$invalid" class="socialerror help-block has-error ng-hide warnig">Please enter your facebook profile link</span>
                        <span class="help-block has-error ng-hide warnig" style="color: red;" ng-show="fbError">{{fbError}}</span>
                      </div>
                    </div>
                  </div>

                  <div class="form-group">
                    <label class="col-sm-3 col-sm-offset-1 control-label">Linkedin:</label>
                    <div class="col-sm-7">
                      <div class="input-group m-bot15 socialfgroup">
                        <div class="input-group-addon"><i class="fa fa-linkedin"></i></div>
                        <input type="url" class="form-control" ng-model="profile.linkedin" name="linkedin"  placeholder="Linkedin"  required>

                        <span ng-show="submitted && profileForm.linkedin.$invalid" class="socialerror help-block has-error ng-hide warnig">Please enter your linkedin profile link</span>
                        <span class="help-block has-error ng-hide warnig" style="color: red;" ng-show="LinkedinError">{{LinkedinError}}</span>
                      </div>
                    </div>
                  </div>

                  <div class="form-group">
                    <label class="col-sm-3 col-sm-offset-1 control-label">Twitter:</label>
                    <div class="col-sm-7">
                      <div class="input-group m-bot15 socialfgroup">
                        <div class="input-group-addon"><i class="fa fa-twitter"></i></div>
                        <input type="url" class="form-control" ng-model="profile.twitter" name="twitter"  placeholder="Twitter"  required>

                        <span ng-show="submitted && profileForm.twitter.$invalid" class="socialerror help-block has-error ng-hide warnig">Please enter your twitter profile link</span>
                        <span class="help-block has-error ng-hide warnig" style="color: red;" ng-show="twitterError">{{twitterError}}</span>
                      </div>
                    </div>
                  </div>


                  <div class="form-group">
                    <label class="col-sm-3 col-sm-offset-1 control-label">Pinterest:</label>
                    <div class="col-sm-7">
                      <div class="input-group m-bot15 socialfgroup">
                        <div class="input-group-addon"><i class="fa fa-pinterest"></i></div>
                        <input type="url" class="form-control" ng-model="profile.pinterest" name="pinterest"  placeholder="Pinterest"  required>

                        <span ng-show="submitted && profileForm.pinterest.$invalid" class="socialerror help-block has-error ng-hide warnig">Please enter your pinterest profile link</span>
                        <span class="help-block has-error ng-hide warnig" style="color: red;" ng-show="pinterestError">{{pinterestError}}</span>
                      </div>
                    </div>
                  </div>


                </div>
                <div class="col-md-6">

                 <div class="profileimg"> 
                  <?php
                  $profilepic=$fetchProfilePic->profile_pic;
                  $path=base_url().'client_uploads/profile_pic/'.$profilepic;
                  ?>
                  <img src="<?php echo $path;?>" class="img-responsive img-thumbnail img-thumbnail">
                  <a class="editprofileimg" data ="" data-toggle="modal" data-target="#myModal" href="javascript:void(0);" style="right:22px;top:189px;"><i class="fa fa-edit"></i></a>

                </div>

              </div>
            </div>

            <div class="col-md-12">
              <div class="form-group">
                <div class="col-sm-12">
                  <button type="submit" class="btn btn-info pull-right btn-sm" ng-click="submitted = true"><strong>Submit</strong></button><span class="pull-right"> &nbsp; &nbsp; </span>

                  <a href="<?php echo base_url();?>admin/viewProfile"><button type="button" class="btn btn-danger pull-right btn-sm"><strong><i class="fa fa-times"></i> Cancel</strong></button></a>
                </div>
              </div>
            </div>
          </form>
        </div>
      </div>
    </section>
  </div>
</div>
<!-- page end-->

</section>
</section>
<!--main content end-->









<!--  modal open for change profile picture -->
<form class="form-horizontal bucket-form" id="cropimage"  name="profilePic" ng-submit="submitform()" novalidate>


  <div class="modal fade" id="myModal" role="dialog" style="background-color: rgba(0, 0, 0, 0.5);">
    <div class="modal-dialog">

      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">Change Profile Picture</h4>
        </div>
        <div class="modal-body">
         <div class="form-group">

           <div class="col-sm-10">
            <div class="image-editor">
              <input type="file" accept="image*/" ng-model="brand.userfile" id="FileUpload1" class="cropit-image-input"><br>
              <span style="color:#a94442;">Profie picture must be minimum width 200px and minimum height 200px.</span>


              <div class="cropit-preview"></div>
              <div class="image-size-label">
                Resize image
              </div>
              <input type="range" class="cropit-image-zoom-input">


              <button class="export">Upload</button>
              <span id="succ" style="color: green;" ></span>
            </div>
            <input type="hidden" class="form-control" id="doc_pic" name="doc_pic" ng-model="brand.doc_pic" required/>


            <span ng-show="submitted && profilePic.doc_pic.$invalid" class="help-block has-error ng-hide warnig">Please upload profile picture</span>
            <span class="help-block has-error ng-hide warnig" style="color: red;" ng-show="doc_picError">{{doc_picError}}</span>
          </div>
        </div>

        <div class="row">
         <div class="col-md-12">
          <button type="submit" ng-click="submitted = true" class="btn btn-info pull-right btn-sm"><strong> Update <i class="fa fa-arrow-right"></i></strong></button>
        </div>
      </div>
    </div>
    <div class="modal-footer">
      <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
    </div>
  </div>

</div>
</div>
</form>

