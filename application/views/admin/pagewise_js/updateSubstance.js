<script>

var postApp = angular.module('postApp',[]);
postApp.directive('calenderopen', function() {
  //console.log('here');
    return {
                require: 'ngModel',
                link: function (scope, el, attr, ngModel) {
                    $(el).datepicker({
                        dateFormat: 'dd-mm-yy',
                        onSelect: function (dateText) {
                          console.log(dateText);
                          scope.addPackage.validity = dateText;

                            scope.$apply(function () {
                                ngModel.$setViewValue(dateText);
                            });
                        }
                    });
                }
            };
});


postApp.directive('calenderend', function() {
  //console.log('here');
    return {
                require: 'ngModel',
                link: function (scope, el, attr, ngModel) {
                    $(el).datepicker({
                        dateFormat: 'dd-mm-yy',
                        onSelect: function (dateText) {
                          console.log(dateText);
                          scope.addPackage.validity = dateText;

                            scope.$apply(function () {
                                ngModel.$setViewValue(dateText);
                            });
                        }
                    });
                }
            };
});
postApp.controller('postController', function ($scope, $http) {

$scope.homeless={};

$scope.homeless.date='<?php echo !empty($getHomelessData) ? date("jS M Y",strtotime($getHomelessData->start_date)) : ''?>';
$scope.homeless.end_date='<?php echo !empty($getHomelessData) ? date("jS M Y",strtotime($getHomelessData->end_date)) : ''?>';
$scope.homeless.country='<?php echo !empty($getHomelessData) ? $getHomelessData->id : ''?>';
$scope.homeless.city='<?php echo !empty($getHomelessData) ? $getHomelessData->city : ''?>';
$scope.homeless.story='<?php echo !empty($getHomelessData) ? preg_replace("/[^A-Za-z0-9]/",' ',$getHomelessData->story) : ''?>';
$scope.homeless.help='<?php echo !empty($getHomelessData) ? $getHomelessData->help : ''?>';
//$scope.homeless.rate='<?php echo !empty($getHomelessData) ? $getHomelessData->rate : ''?>';
$scope.homeless.info='<?php echo !empty($getHomelessData) ? preg_replace("/[^A-Za-z0-9]/",' ',$getHomelessData->info) : ''?>';

// $scope.event.date=date; 
$scope.submitHomelessForm = function(){
var date=$("#open_date").val();
$scope.homeless.date=date;
var end_date=$("#end_date").val();
$scope.homeless.end_date=end_date;
var rating=$("#t1").val();
$scope.homeless.rate=rating;
if ($scope.homelessForm.$valid) {
 $http({
      method: 'POST',
      dataType: 'json',
      url: '<?php echo base_url();?>user/insertHomelessData',

            data: $scope.homeless, //forms event object
            headers: {'Content-Type': 'application/json'}
          }).success(function(data){
             if(data.status=='1'){

              window.location.href="<?php echo base_url();?>user/homelessInfo";
             }

          }); 

}else{



}

};

$scope.substance={};
$scope.substance.substance_id='<?php echo !empty($getSubstanceData) ? $getSubstanceData->id :''?>';
$scope.substance.drug_name='<?php echo !empty($getSubstanceData) ? $getSubstanceData->substance_name :''?>';
$scope.substance.start_date='<?php echo !empty($getSubstanceData) ? date("jS M Y",strtotime($getSubstanceData->start_date)) :''?>';
$scope.substance.end_date='<?php echo !empty($getSubstanceData) ? date("jS M Y",strtotime($getSubstanceData->until_date)) :''?>';
$scope.substance.optradio='<?php echo !empty($getSubstanceData) ? $getSubstanceData->frequency :''?>';
$scope.substance.optradio1='<?php echo !empty($getSubstanceData) ? $getSubstanceData->frequency_use_now :''?>';
$scope.substance.reasons='<?php echo !empty($getSubstanceData) ? preg_replace("/[^A-Za-z0-9]/",' ',$getSubstanceData->reasons) :''?>';
$scope.substance.significant_info='<?php echo !empty($getSubstanceData) ? preg_replace("/[^A-Za-z0-9]/",' ',$getSubstanceData->significant_info) :''?>';

$scope.submitSubstanceForm = function(){

var start_date=$("#start_date").val();
$scope.substance.start_date=start_date;
 
var until_date=$("#until_date").val();
$scope.substance.until_date=until_date;

 if ($scope.substanceForm.$valid) {
$http({
      method: 'POST',
      dataType: 'json',
      url: '<?php echo base_url();?>admin/updateSubstanceData',

            data: $scope.substance, //forms event object
            headers: {'Content-Type': 'application/json'}
          }).success(function(data){
             if(data.status=='1'){

              window.location.href="<?php echo base_url()?>admin/viewSubstance";
             }

          });

 }else{


 }

};

});




</script>

<script>
// $(document).ready(function(){
//       $("#homeless_div").hide();
//         $("#substance_div").hide();
//     $('#higher_needs').on('change', function() {
      
//       if ( this.value == 'homeless')
//       {
//         $("#substance_div").hide();
//         $("#homeless_div").show();
//       }else if(this.value == 'substance_use'){
//         $("#homeless_div").hide();
//         $("#substance_div").show();
//       }
//       else
//       {
        
//         $("#homeless_div").hide();
//         $("#substance_div").hide();

//       }
//     });
// });


</script>

<script>
$("#rate").rateYo({
    onSet: function (rating, rateYoInstance) {
     //alert(rating);
     $(this).next().val(rating);
     
    },
    rating: 0,
    starWidth: "40px",
    numStars: 5,
    
});
</script>