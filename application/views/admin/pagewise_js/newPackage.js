<script>
var postApp = angular.module('postApp', []);
postApp.directive('ngFiles', ['$parse', function ($parse) {

        function fn_link(scope, element, attrs) {
            var onChange = $parse(attrs.ngFiles);
            element.on('change', function (event) {
                onChange(scope, {$files: event.target.files});
            });
        }
        ;

        return {
            link: fn_link
        }
    }]);

postApp.directive('calenderopen', function() {
  //console.log('here');
    return {
                require: 'ngModel',
                link: function (scope, el, attr, ngModel) {
                    $(el).datepicker({
                        dateFormat: 'yy-mm-dd',
                        onSelect: function (dateText) {
                          console.log(dateText);
                          scope.addPackage.validity = dateText;

                            scope.$apply(function () {
                                ngModel.$setViewValue(dateText);
                            });
                        }
                    });
                }
            };
});

postApp.controller('postController', function ($scope, $http) {
var formdata = new FormData();
$scope.adds={};
$scope.getTheFiles = function ($files) {
//alert();
        angular.forEach($files, function (value, key) {
            formdata.append('file', value);
        });

        var request = {
            method: 'POST',
            url: '<?php echo base_url();?>admin/upload_image',
            data: formdata,
            enctype: 'multipart/form-data',
            headers: {
                'Content-Type': undefined
            }
        };
        $http(request)
                .success(function (data) {
                  console.log(data)
                    $scope.adds.filename = data.filename;
                    
                })
                .error(function () {
                });
    };

    

    // $("input[type=checkbox]:checked").each(function() {
    // alert( $(this).val() );
    // });
    
    $scope.adds.package_id='<?php echo!empty($fetchPackageData) ? $fetchPackageData->id : '' ?>';
    $scope.adds.package_title='<?php echo!empty($fetchPackageData) ? $fetchPackageData->package_title : '' ?>';
    $scope.adds.reference_number='<?php echo!empty($fetchPackageData) ? $fetchPackageData->reference_number : '' ?>';
    $scope.adds.description='<?php echo!empty($fetchPackageData) ? $fetchPackageData->package_description : '' ?>';
    $scope.adds.currency='<?php echo!empty($fetchPackageData) ? $fetchPackageData->price_currency : '' ?>';
    $scope.adds.amount='<?php echo!empty($fetchPackageData) ? $fetchPackageData->price_amount : '' ?>';
    $scope.adds.offer_amount='<?php echo!empty($fetchPackageData) ? $fetchPackageData->offer_amount : '' ?>';
    $scope.adds.offer_currency='<?php echo!empty($fetchPackageData) ? $fetchPackageData->offer_currency : '' ?>';
    $scope.adds.radpub='<?php echo!empty($fetchPackageData) ? $fetchPackageData->publish : '' ?>';
    $scope.adds.validity_date='<?php echo!empty($fetchPackageData) ? date("jS M Y",strtotime($fetchPackageData->validity_date)) : '' ?>';
    //$scope.adds.months='<?php echo!empty($fetchPackageData) ? $fetchPackageData->month : '' ?>';
    //alert($scope.adds.months);
    //$scope.adds.years='<?php echo!empty($fetchPackageData) ? $fetchPackageData->year : '' ?>';
    $scope.adds.radio1='<?php echo!empty($fetchPackageData) ? $fetchPackageData->facebook : '' ?>';
    $scope.adds.radio2='<?php echo!empty($fetchPackageData) ? $fetchPackageData->twitter : '' ?>';
    $scope.adds.radio3='<?php echo!empty($fetchPackageData) ? $fetchPackageData->linkedin : '' ?>';
    $scope.adds.radio='<?php echo!empty($fetchPackageData) ? $fetchPackageData->package_end_date : '' ?>';
    $scope.adds.attached_file='<?php echo!empty($fetchPackageData) ? $fetchPackageData->package_image : '' ?>';
    //alert($scope.adds.attached_file);
    $scope.submitJobAdvForm=function(){

        var validity_date=$("#open_date").val();
        //alert(validity_date);
        $scope.adds.validity_date=validity_date;
        //console.log($scope.adds.validity_date);
     var allVals = [];

   $scope.adds.test = [];



        $(':checkbox:checked').each(function(i){
            $scope.adds.test.push($(this).val());
          // $scope.adds.service=allVals.push($(this).val());
          // console.log(allVals);
        }); 
         //console.log('$scope.test')  
        //console.log($scope.adds.test)
    //$scope.adds.service=service;
    if ($scope.jobAdvForm.$valid) {
    $http({
      method: 'POST',
      dataType: 'json',
      url: '<?php echo base_url();?>admin/insertNewPackage',

            data: $scope.adds, //forms client object
            headers: {'Content-Type': 'application/json'}
          }).success(function(data){
            if(data.status == 1){
           window.location = '<?php echo base_url('admin/createPackage') ?>';


            }else{
             
             
            }


          });

    }else{



    }



    };

});



</script>

<script>
function saveJobAdv(){

var job_adds=$("#job_adds").val();
var type=$("#type").val();
var duration=$("#duration").val();
var package_id=$("#package_id").val();


if(job_adds =='' || type =='' || duration == ''){
$("#msg1").html("Job Adds,Type and Duration is required");

}else{

$.ajax({
url:'<?php echo base_url();?>admin/insertJobAdv',
type:'POST',
data:{job_adds:job_adds,type:type,duration:duration},
success:function(response){

if(response == 'success' && package_id ==''){

 window.location.href='<?php echo base_url('admin/addNewPackage') ?>';
// alert("Job Adds has been added successfully");
}else{

document.location="<?php echo base_url();?>admin/addNewPackage?id=" + package_id;
}

}

});
}
}

//function for saving service
function saveService(){
var service_name=$("#service_name").val();
var package_id=$("#package_id").val();
//alert(package_id);
if(service_name==''){

$("#msg").html("Service Name is required");
}else{
$.ajax({
url:'<?php echo base_url();?>admin/saveServiceName',
type:'POST',
data:{service_name:service_name},
success:function(response){
if(response == 'success' && package_id ==''){

 window.location.href='<?php echo base_url('admin/addNewPackage') ?>';
//alert("Service has been added successfully");

}else{

document.location="<?php echo base_url();?>admin/addNewPackage?id=" + package_id;
}

}

});

}

}



</script>

<script>
function confirmMsg(){
var result=confirm('Are you sure you want to delete?');
if(result){

return true;
}else{
return false;

}

}

function hideMsg(){

   $("#msg").html(''); 
}

function hideDiv(){

  $("#date-div").hide();  
}

function showDiv(){

$("#date-div").show(); 

}

function hideReqMsg(){

 $("#msg1").html('');   
}

$(document).ready(function(){

$("#chkPassport").click(function () {
            if ($(this).is(":checked")) {
                $("#job_adds_div").show();
            } else {
                $("#job_adds_div").hide();
            }
        });

});

function showJobDiv(){

$("#job_adds_div").show();

}

function hideJobDiv(){

 $("#job_adds_div").hide();   
}
</script>