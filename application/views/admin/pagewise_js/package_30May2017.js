<script>

// var postApp = angular.module('postApp', ['ngMaterial']);angular.module('postApp').run(function($rootScope) {

// // $rootScope.gmtDate = new Date('2015-01-01');
// $rootScope.myDate = new Date();
// // $rootScope.myDate ='user_dob; } else { new Date(); } ?>';
// $rootScope.minDate = new Date($rootScope.myDate.getFullYear() - 50,
// $rootScope.myDate.getMonth() ,
// $rootScope.myDate.getDate());


// $rootScope.maxDate = new Date($rootScope.myDate.getFullYear(),
// $rootScope.myDate.getMonth() + 0 ,
// $rootScope.myDate.getDate());
// });


var postApp = angular.module('postApp', []);
postApp.controller('postController', function ($scope, $http) {

  <!--for addpackage-->
  $scope.addPackage = {};


  $scope.submitPackageForm = function () {
    $scope.addPackage.validity=$("#validity").val();
        //alert($scope.addPackage.validity);

        if ($scope.packageForm.$valid) {
          $http({

            method: 'POST',
            dataType: 'json',
            url: '<?php echo base_url();?>admin/insertPackage',
            data: $scope.addPackage,
            headers: {'Content-Type': 'application/json'}
          }).success(function(data){
            if(data.status==1){
              window.location='<?php echo base_url();?>admin/createPackage';

            }

            else{
              $scope.errorTitle = data.error.package_title.error;
              $scope.errorDescription = data.error.package_description.error;
              $scope.errorAmount = data.error.amount.error;
              $scope.errorValidity = data.error.validity.error;
            }

          });

        }else{
          console.log($scope.packageForm.$valid);

        }

      };


      $scope.edit_package = function(id){
        $http({
         method: 'POST',
         dataType: 'json',
         url: '<?php echo base_url() ?>admin/get_package',
         data: {id: id},
         headers: {'Content-Type': 'application/json'}
       }).success(function(data){

  //alert(data.package.validity);
  $('#edit_package_id').val(data.package.id);
  $scope.package = {
    'package_title': data.package.package_title,
    'package_description': data.package.package_description,
    'amount': data.package.amount,
    'validity': data.package.validity_date,
    
  }
//alert(data.package.validity);
});
     };


     $scope.submitEditPackageForm = function(){

      $scope.package.validity=$("#package_validity").val();
//alert($scope.package.validity);

if ($scope.editpackageForm.$valid) {

  var id = $('#edit_package_id').val();
  $http({
    method: 'POST',
    dataType: 'json',
    url: '<?php echo base_url() ?>admin/update_package/' + id,
                    data: $scope.package, //forms user object
                    headers: {'Content-Type': 'application/json'}
                  }).success(function(data){
                    if (data.status == 1) {
                      window.location = '<?php echo base_url('admin/createPackage') ?>';
                    } else {
                      
                      $scope.errorEditTitle = data.error.package_title.error;
                      $scope.errorEditDescription = data.error.package_desc.error;
                      $scope.errorEditAmount = data.error.amount.error;
                      $scope.errorEditValidity = data.error.validity.error;

                    }

                  })

                }

              }
            });

function hideMsg(){
  $("#msg").hide();

}

$(document).ready(function() {
  
  var showChar = 100;
  var ellipsestext = "...";
  var moretext = "more";
  var lesstext = "less";
  $('.more').each(function() {
    var content = $(this).html();

    if(content.length > showChar) {

      var c = content.substr(0, showChar);
      var h = content.substr(showChar-1, content.length - showChar);

      var html = c + '<span class="moreelipses">'+ellipsestext+'</span>&nbsp;<span class="morecontent"><span>' + h + '</span>&nbsp;&nbsp;<a href="" class="morelink">'+moretext+'</a></span>';

      $(this).html(html);
    }

  });

  $(".morelink").click(function(){
    if($(this).hasClass("less")) {
      $(this).removeClass("less");
      $(this).html(moretext);
    } else {
      $(this).addClass("less");
      $(this).html(lesstext);
    }
    $(this).parent().prev().toggle();
    $(this).prev().toggle();
    return false;
  });
});
</script>