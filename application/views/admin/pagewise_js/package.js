<script>

// var postApp = angular.module('postApp', ['ngMaterial']);angular.module('postApp').run(function($rootScope) {

// // $rootScope.gmtDate = new Date('2015-01-01');
// $rootScope.myDate = new Date();
// // $rootScope.myDate ='user_dob; } else { new Date(); } ?>';
// $rootScope.minDate = new Date($rootScope.myDate.getFullYear() - 50,
// $rootScope.myDate.getMonth() ,
// $rootScope.myDate.getDate());


// $rootScope.maxDate = new Date($rootScope.myDate.getFullYear(),
// $rootScope.myDate.getMonth() + 0 ,
// $rootScope.myDate.getDate());
// });


var postApp = angular.module('postApp', []);

postApp.directive('calendar', function () {
  
            return {
                require: 'ngModel',
                link: function (scope, el, attr, ngModel) {
                  //alert();
                    $(el).datepicker({
                        dateFormat: 'yy-mm-dd',
                        onSelect: function (dateText) {
                          //alert(dateText);
                            scope.$apply(function () {
                                ngModel.$setViewValue(dateText);
                            });
                        }
                    });
                }
            };
        });
postApp.directive('calendar1', function () {
  
            return {
                require: 'ngModel',
                link: function (scope, el, attr, ngModel) {
                  //alert();
                    $(el).datepicker({
                        dateFormat: 'yy-mm-dd',
                        onSelect: function (dateText) {
                          //alert(dateText);
                            scope.$apply(function () {
                                ngModel.$setViewValue(dateText);
                            });
                        }
                    });
                }
            };
        });
postApp.controller('postController', function ($scope, $http) {

  <!--for addpackage-->
  $scope.addPackage = {};


  $scope.submitPackageForm = function () {
    $scope.addPackage.validity=$("#validity").val();
   // alert($scope.addPackage.validity);
    $scope.addPackage.total_jobs=$("#total_jobs").val();
    $scope.addPackage.premium_jobs=$("#premium_jobs").val();
        //alert($scope.addPackage.validity);

        if ($scope.packageForm.$valid) {
          $http({

            method: 'POST',
            dataType: 'json',
            url: '<?php echo base_url();?>admin/insertPackage',
            data: $scope.addPackage,
            headers: {'Content-Type': 'application/json'}
          }).success(function(data){
            if(data.status==1){
              window.location='<?php echo base_url();?>admin/createPackage';

            }

            else{
              $scope.errorTitle = data.error.package_title.error;
              $scope.errorDescription = data.error.package_description.error;
              $scope.errorAmount = data.error.amount.error;
              $scope.errorValidity = data.error.validity.error;
            }

          });

        }else{
          console.log($scope.packageForm.$valid);

        }

      };


      $scope.edit_package = function(id){
        $http({
         method: 'POST',
         dataType: 'json',
         url: '<?php echo base_url() ?>admin/get_package',
         data: {id: id},
         headers: {'Content-Type': 'application/json'}
       }).success(function(data){

  //alert(data.package.validity);
  $('#edit_package_id').val(data.package.id);
  //$('#totalJobs').val(data.package.package_title);
  $scope.package = {
    'package_title': data.package.package_title,
    'package_description': data.package.package_description,
    'amount': data.package.amount,
    'validity': data.package.validity_date,
    'update_jobs': data.package.job_adds,
    'premiumJobs' : data.package.premium_jobs
    
  }
  //console.log($scope.package);

     //var val = data.package.job_adds;
    //alert(val);  
    //document.querySelector('#sel [value="' + val + '"]').selected = true;

    if(data.package.job_adds!='0')
    {
          //alert('here');
      $('#jobAds').prop('checked', true);
    }else{
       //alert('there');
      $('#jobAds').prop('checked', false);
    }


    if(data.package.premium_jobs!='0')
    {
          //alert('here');
      $('#updatePremium').prop('checked', true);
    }else{
       //alert('there');
      $('#updatePremium').prop('checked', false);
    }
  //console.log($scope.package);
//alert(data.package.package_title);
});

    
     };


     $scope.submitEditPackageForm = function(){
     // alert('here');

      $scope.package.validity=$("#package_validity").val();
      $scope.package.update_jobs=$("#updateAds").val();
      $scope.package.premiumJobs=$("#total_premium_jobs").val();

//alert($scope.package.premiumJobs);

if ($scope.editpackageForm.$valid) {

  var id = $('#edit_package_id').val();
  $http({
    method: 'POST',
    dataType: 'json',
    url: '<?php echo base_url() ?>admin/update_package/' + id,
                    data: $scope.package, //forms user object
                    headers: {'Content-Type': 'application/json'}
                  }).success(function(data){
                    if (data.status == 1) {
                      window.location = '<?php echo base_url('admin/createPackage') ?>';
                    } else {
                      
                      $scope.errorEditTitle = data.error.package_title.error;
                      $scope.errorEditDescription = data.error.package_desc.error;
                      $scope.errorEditAmount = data.error.amount.error;
                      $scope.errorEditValidity = data.error.validity.error;

                    }

                  })

                }

              }
            });

function hideMsg(){
  $("#msg").hide();

}

$(document).ready(function() {
  
  var showChar = 100;
  var ellipsestext = "...";
  var moretext = "more";
  var lesstext = "less";
  $('.more').each(function() {
    var content = $(this).html();

    if(content.length > showChar) {

      var c = content.substr(0, showChar);
      var h = content.substr(showChar-1, content.length - showChar);

      var html = c + '<span class="moreelipses">'+ellipsestext+'</span>&nbsp;<span class="morecontent"><span>' + h + '</span>&nbsp;&nbsp;<a href="" class="morelink">'+moretext+'</a></span>';

      $(this).html(html);
    }

  });

  $(".morelink").click(function(){
    if($(this).hasClass("less")) {
      $(this).removeClass("less");
      $(this).html(moretext);
    } else {
      $(this).addClass("less");
      $(this).html(lesstext);
    }
    $(this).parent().prev().toggle();
    $(this).prev().toggle();
    return false;
  });
});

function ShowTotalJobs(chkPassport) {
        var dvPassport = document.getElementById("dvPassport");
        dvPassport.style.display = chkPassport.checked ? "block" : "none";
        

        //  if($("#chkPassport").is(":checked")){
        //  var id=$("#totalJobs").val();
        //  //alert('here');
        // }

        // else{
        
        // var id='';
        // }
        //  $.ajax({
        //  url:'<?php echo base_url();?>admin/getJobs',
        //  type:'POST',
        //  data:{id:id},
        //  success:function(response){
        //    //var myArr = $.parseJSON(response);
        //    //var total_jobs = myArr.total_jobs;
        //    //var  premium_jobs=myArr.premium_jobs;
        //    if(response!=''){

        //     //alert(response['total_jobs']);

        //    $("#total_jobs").val(response);
        //    //$("#total_premium_jobs").val(premium_jobs);

        //    }else{

        //    $("#total_jobs").val('');
        //    //$("#total_premium_jobs").val('');
        //    }
          
        //  }

        // });

    }


    function updateJobs(jobAds){
     //alert($("#package_id").val());
    var dvPassport = document.getElementById("updateJobAds");
    dvPassport.style.display = jobAds.checked ? "block" : "none"; 

    // var job_adds=$("#job_adds").val();
    // alert(job_adds);
     // alert($("#edit_package_id").val());
    // if($("#jobAds").is(":checked")){
    //     //alert('here');
    //      var id=$("#totalJobs").val();
         
    //     }

    //     else{
    //     //alert('there');
    //     var id='';
    //     }

    //     $.ajax({
    //      url:'<?php echo base_url();?>admin/getJobs',
    //      type:'POST',
    //      data:{id:id},
    //      success:function(response){
    //        //var myArr = $.parseJSON(response);
    //        //var total_jobs = myArr.total_jobs;
    //        //var  premium_jobs=myArr.premium_jobs;
    //        if(response!=''){

    //         //alert(response['total_jobs']);

    //        $("#updateAds").val(response);
    //        //$("#total_premium_jobs").val(premium_jobs);

    //        }else{

    //        $("#updateAds").val('');
    //        //$("#total_premium_jobs").val('');
    //        }
          
    //      }

    //     });


    }



    function showPremiumJobs(chkPremium){

        var dvPassport = document.getElementById("dvPremium");
        dvPassport.style.display = chkPremium.checked ? "block" : "none";
        //var id=$("#totalJobs").val();

        // if($("#chkPremium").is(":checked")){
        //  var id=$("#totalJobs").val();
        //  //alert('here');
        // }

        // else{
         
        // var id='';
        // }
        // $.ajax({
        //  url:'<?php echo base_url();?>admin/getPremiumJobs',
        //  type:'POST',
        //  data:{id:id},
        //  success:function(response){
        //    //var myArr = $.parseJSON(response);
        //    //var total_jobs = myArr.total_jobs;
        //    //var  premium_jobs=myArr.premium_jobs;
        //    if(response!=''){

        //     //alert(response['total_jobs']);

        //    $("#premium_jobs").val(response);
        //    //$("#total_premium_jobs").val(premium_jobs);

        //    }else{

        //    $("#premium_jobs").val('');
        //    //$("#total_premium_jobs").val('');
        //    }
          
        //  }

        // });
    }

    function updatePremiumJobs(updatePremium){
     var dvPassport = document.getElementById("premium");
      dvPassport.style.display = updatePremium.checked ? "block" : "none";
       // if($("#updatePremium").is(":checked")){
       //   var id=$("#totalJobs").val();
       //   //alert('here');
       //  }

       //  else{
         
       //  var id='';
       //  }
       //  $.ajax({
       //   url:'<?php echo base_url();?>admin/getPremiumJobs',
       //   type:'POST',
       //   data:{id:id},
       //   success:function(response){
       //     //var myArr = $.parseJSON(response);
       //     //var total_jobs = myArr.total_jobs;
       //     //var  premium_jobs=myArr.premium_jobs;
       //     if(response!=''){

       //      //alert(response['total_jobs']);

       //     $("#total_premium_jobs").val(response);
       //     //$("#total_premium_jobs").val(premium_jobs);

       //     }else{

       //     $("#total_premium_jobs").val('');
       //     //$("#total_premium_jobs").val('');
       //     }
          
       //   }

       //  });
    }
</script>
<script>
    function selectedPackage(value)
    {
         var id=value;
         var a= $("#totalJobs").val(id);
       

   
    }

  //   $('#demo-date1').datepicker({

  //           format: "yyyy/mm/dd",
  //           autoclose:true
  //       });

  // $('#demo-date2').datepicker({

  //           format: "yyyy/mm/dd",
  //           autoclose:true
  //       });
</script>