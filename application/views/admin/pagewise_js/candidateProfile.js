<script>
var postApp = angular.module('postApp',[]);
postApp.controller('postController', function ($scope, $http) {

$scope.education={};
$scope.education.instName='<?php echo!empty($educationInfo) ? $educationInfo->institute_name : '' ?>';
$scope.education.fromMonth='<?php echo!empty($educationInfo) ? $educationInfo->from_month : '' ?>';
$scope.education.fromYear='<?php echo!empty($educationInfo) ? $educationInfo-> 	from_year : '' ?>';
$scope.education.toYear='<?php echo!empty($educationInfo) ? $educationInfo-> 	to_year : '' ?>';
$scope.education.toMonth='<?php echo!empty($educationInfo) ? $educationInfo-> 	to_month : '' ?>';
$scope.education.courseStudy='<?php echo!empty($educationInfo) ? $educationInfo->course : '' ?>';
$scope.education.qualificationType='<?php echo!empty($educationInfo) ? $educationInfo->q_id : '' ?>';
$scope.education.grade='<?php echo!empty($educationInfo) ? $educationInfo->g_id : '' ?>';
$scope.education.achieved='<?php echo!empty($educationInfo) ? $educationInfo->a_id : '' ?>';
var candidate=$("#candidate_id").val();	
$scope.education.candidate_id=candidate;
//console.log($scope.candidate_profile);
$scope.submitCandidateProfileForm = function(){

if ($scope.candidateProfileForm.$valid) {
	$http({
      method: 'POST',
      dataType: 'json',
      url: '<?php echo base_url();?>admin/updatePersonalInfo',

            data: $scope.education, //forms candidate education object
            headers: {'Content-Type': 'application/json'}
          });

}else{


}

};

$scope.personal={};
$scope.personal.gender='<?php echo!empty($personalInfo) ? $personalInfo->user_gender : '' ?>';
$scope.personal.rstatus='<?php echo!empty($personalInfo) ? $personalInfo->user_rel_status : '' ?>';
$scope.personal.day='<?php echo!empty($personalInfo) ? $personalInfo->user_day : '' ?>';
$scope.personal.month='<?php echo!empty($personalInfo) ? $personalInfo->user_month : '' ?>';
$scope.personal.year='<?php echo!empty($personalInfo) ? $personalInfo->user_year : '' ?>';
$scope.personal.nationality='<?php echo!empty($personalInfo) ? $personalInfo->user_nationality : '' ?>';
$scope.personal.ethnicorigin='<?php echo!empty($personalInfo) ? $personalInfo->user_ethinic_origin : '' ?>';
$scope.personal.disabilityDesc='<?php echo!empty($personalInfo) ? $personalInfo->user_disabilities : '' ?>';
$scope.personal.desiredJobtype='<?php echo!empty($personalInfo) ? $personalInfo->user_desiredjobType : '' ?>';
$scope.personal.permittedtowork='<?php echo!empty($personalInfo) ? $personalInfo->user_permission : '' ?>';



$scope.userMediaInfo={};
$scope.userMediaInfo.fb='<?php echo!empty($userSocialMediaLink) ? $userSocialMediaLink->facebook : '' ?>';
$scope.userMediaInfo.youtube='<?php echo!empty($userSocialMediaLink) ? $userSocialMediaLink->youtube : '' ?>';
$scope.userMediaInfo.twitter='<?php echo!empty($userSocialMediaLink) ? $userSocialMediaLink->twitter : '' ?>';
$scope.userMediaInfo.linkedIn='<?php echo!empty($userSocialMediaLink) ? $userSocialMediaLink->linkedIn : '' ?>';
$scope.userMediaInfo.instagram='<?php echo!empty($userSocialMediaLink) ? $userSocialMediaLink->instagram : '' ?>';


});


function removeReadOnly(){
    //$(document).rad
	$(':input').removeAttr('readonly');
}
</script>