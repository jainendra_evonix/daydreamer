<script>

var postApp = angular.module('postApp',[]);
postApp.directive('calenderopen', function() {
  //console.log('here');
    return {
                require: 'ngModel',
                link: function (scope, el, attr, ngModel) {
                    $(el).datepicker({
                        dateFormat: 'yy-mm-dd',
                        onSelect: function (dateText) {
                          console.log(dateText);
                          scope.addPackage.validity = dateText;

                            scope.$apply(function () {
                                ngModel.$setViewValue(dateText);
                            });
                        }
                    });
                }
            };
});



postApp.controller('postController', function ($scope, $http) {

$scope.interview={};
$scope.interview.group_id='<?php echo !empty($groupData) ? $groupData->id : ''?>';
$scope.interview.group_name='<?php echo !empty($groupData) ? $groupData->group_name : ''?>';
$scope.interview.job_title='<?php echo !empty($groupData) ? $groupData->job_title : ''?>';
$scope.interview.location='<?php echo !empty($groupData) ? $groupData->location : ''?>';
$scope.interview.date='<?php echo !empty($groupData) ? date("jS M Y",strtotime($groupData->interview_date)) : ''?>';
$scope.interview.client='<?php echo !empty($groupData) ? $groupData->clnt_first_name.' '.$groupData->clnt_last_name : ''?>';

$scope.submitGroupInterviewForm = function(){

var date=$("#open_date").val();
$scope.interview.date=date;

var client_name=$("#organizationname").val();
$scope.interview.client_name=client_name;

$scope.interview.test = [];
   
      $(':checkbox:checked').each(function(i){
        //alert('here');
         $scope.interview.test.push($(this).val());
          // $scope.adds.service=allVals.push($(this).val());
           //console.log($scope.interview.test);
        });

$http({
      method: 'POST',
      dataType: 'json',
      url: '<?php echo base_url();?>admin/insertGroupInterview',

            data: $scope.interview, //forms event object
            headers: {'Content-Type': 'application/json'}
          }).success(function(data){
             if(data.status=='1'){

              window.location.href="<?php echo base_url();?>admin/viewGroupInterview";

             }else{


             }


          })

};

});




</script>

<script type="text/javascript" src="<?php echo base_url();?>client_assets/js/client_js/autocomplete.js"></script>
<script>
    
    var a = $('#selctionajax').autocomplete({
      serviceUrl:'<?php echo base_url();?>admin/fetchClient',
      minChars:1,
      delimiter: /(,|;)\s*/, // regex or character
      maxHeight:600,
      //width:300,
      zIndex: 9999,
      deferRequestBy: 100, //miliseconds
       onSelect: function(suggestion) {
       //alert();
       if(suggestion.data)
                         {
           $('#organizationname').val(suggestion.data);

                        }
                        else
                        {
                        alert("sd");
                        }
}
      //params: { country:'Yes' }, //aditional parameters
      //noCache: false //default is false, set to true to disable caching
      // callback function:
      //onSelect: function(value, data){ alert('You selected: ' + value + ', ' + data); },
      // local autosugest options:
      //lookup: ['January', 'February', 'March', 'April', 'May'] //local lookup values
    });


</script>

<script>
$(function() {
    $('#candidates_table').hide(); 

    $('#selctionajax').change(function(){
        
            $('#candidates_table').show(); 
        
    });
});

</script>

<script type="text/javascript">
$(document).ready(function(){

var dataTable = $('#employee-grid').DataTable({
      //alert();
            "processing": true,
            "serverSide": true,
             "ordering":false,
            "ajax": {
                url: "<?php echo base_url();?>admin/viewCandidatesForGroupInterview", // json datasource
                type: "post", // method  , by default get
                error: function () {  // error handling
                    $(".employee-grid-error").html("");
                    $("#employee-grid").append('<tbody class="employee-grid-error"><tr><th colspan="3">No data found in the server</th></tr></tbody>');
                    $("#employee-grid_processing").css("display", "none");

                }
            }
        });



});



</script>

<script type="text/javascript">
$('#selectAll').click(function(e){
  var table= $(e.target).closest('table');
  $('td input:checkbox',table).attr('checked',e.target.checked);
});
</script>