 
                                            
                                            <section class="panel container">
                                            <div class="panel-body">
                                             <h4>Create Service</h4>
                                               <?php echo $this->session->flashdata('successmsg');?>
                                               <?php echo $this->session->flashdata('errormsg');?>
                                            

                                           <form class="form-horizontal bucket-form" ng-submit="submitServiceForm()" name="serviceForm" novalidate>
                                           <input type="hidden" name="package_id" ng-model="service.package_id">
                                            
                                            
                                            <?php

                                               foreach($fetchServiceData as $key){

                                            ?>
                                            <div class="form-group" style="padding-bottom: 0;">
                                                <label class="col-sm-4 text-right"><?php echo $key->service_name;?>:</label>
                                                <div class="col-sm-5 icheck">
                                                    <div class="square">
                                                       
                                                    <input type="checkbox" id="service" name="<?php echo 'service'.$key->id;?>"  value=<?php echo $key->id;?> <?php echo isset($key->id) && in_array($key->id, $service) ? 'checked="checked"':''; ?>>
                                                    
                                                    </div>
                                                    <div class="col-sm-3">
                                                        <a href="<?php echo base_url();?>admin/deleteService?id=<?php echo $key->id;?>&&package_id=<?php echo $package_id;?>" onclick="return confirmMsg();"><i class="fa fa-trash"></i> Delete</a>
                                                    </div>
                                                </div>
                                            </div>
                                            <?php }?>


                                             

                                            <div class="form-group">
                                                <div class="col-sm-6 col-sm-offset-1">
                                                    <input type="text" class="form-control"  id="service_name"  placeholder="Enter service name" required>
                                                    
                                                </div>
                                                <div class="col-sm-2">
                                                    <button class="btn btn-success btn-sm" onclick="saveService();"><strong>Add <i class="fa fa-plus"></i></strong></button>
                                                </div>
                                            </div>
                                                


                                                
                                                <div class="col-sm-8">
                                                    <hr>
                                                    <a href="<?php echo base_url();?>admin/createPackage" type="submit" class="btn btn-danger btn-sm pull-right" ng-click="submitted = true" id="btn"><strong><i class="fa fa-arrow-left"></i> Complete <i class="fa fa-"></i></i></strong></a><span class="pull-right"> &nbsp; &nbsp; </span>
                                                    <button type="submit" class="btn btn-info btn-sm pull-right" ng-click="submitted = true" id="btn"><strong><i class="fa fa-plus"></i> Save <i class="fa fa-"></i></i></strong></button>
                                                    
                                                    <br>
                                                </div>
                                            </form>
                                        </div>

                                       
                            </div>
                        </section>