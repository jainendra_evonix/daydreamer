

<div class="row" id="homeless_div">

    <div class="col-md-8 col-md-offset-2">
     <section class="panel">
      <section class="panel-body">
       <div  style="padding: 10px 0;" >
        <div class="col-sm-12">
          <h4><strong>Homeless Form</strong></h4>
          <br>


        </div>
        <form class="form-horizontal" ng-submit="submitHomelessForm()" name="homelessForm" novalidate>
         <input type="hidden" name="homeless_id" ng-model="homeless.homeless_id">
              <div class="form-group">
               <label class="col-sm-3  control-label">Start Date:</label>
               <div class="col-sm-9">
                <div class="input-group date" id="demo-date" style="width:100%">
                  <div class="input-group-content">
                    <input data-calenderopen type="text" class="form-control" autocomplete="off"  ng-model="homeless.date" name="date" placeholder="Please enter Date"  id="open_date"   style="margin-bottom:0px;">
                    <span id="msg1" ng-show="submitted && eventForm.open_date.$invalid" class="help-block has-error">Date is required.</span>
                    <!-- <span ng-show="errorValidity" class="help-block has-error ng-hide">{{errorValidity}}</span> -->
                    <span ng-show="errorDate" class="help-block has-error ng-hide">{{errorDate}}</span>
                  </div>
                  <!-- <a href="javascript:void(0);" class="input-group-addon date"><i class="fa fa-calendar"></i></a> -->
                </div>
                <br>
              </div>
            </div>

            <div class="form-group">
             <label class="col-sm-3  control-label">End Date:</label>
             <div class="col-sm-9">
              <div class="input-group date" id="demo-date" style="width:100%">
                <div class="input-group-content">
                  <input data-calenderend type="text" class="form-control" autocomplete="off"  ng-model="homeless.end_date" name="end_date" placeholder="Please enter end Date"  id="end_date"   style="margin-bottom:0px;">
                  <span id="msg1" ng-show="submitted && eventForm.end_date.$invalid" class="help-block has-error">End Date is required.</span>
                  <!-- <span ng-show="errorValidity" class="help-block has-error ng-hide">{{errorValidity}}</span> -->
                  <span ng-show="errorEndDate" class="help-block has-error ng-hide">{{errorEndDate}}</span>
                </div>
                <!-- <a href="javascript:void(0);" class="input-group-addon date"><i class="fa fa-calendar"></i></a> -->
              </div>
              <br>
            </div>
          </div>

          <div class="form-group">
            <label class="col-sm-3 control-label">Country:</label>
            <div class="col-sm-9">
              <select class="form-control" type="text" name="country" ng-model="homeless.country" required>
                <option value="" required="true">(Select Your Country)</option>  
                <?php
                foreach($allCountries as $key){



                  ?>
                  <option value="<?php echo $key->id;?>"><?php echo $key->country_name;?></option>
                  <?php }?>

                </select>
                <span ng-show="submitted && homelessForm.country.$error.required"  class="help-block has-error ng-hide">Country is required.</span>
                <span ng-show="errorcountry" class="help-block has-error ng-hide">{{errorcountry}}</span>
                <br><br>
              </div>
            </div>

            <div class="form-group">
              <label class="col-sm-3 control-label">City:</label>
              <div class="col-sm-9">
                <input class="form-control" type="text" name="city" ng-model="homeless.city" required>
                <span ng-show="submitted && homelessForm.city.$error.required"  class="help-block has-error ng-hide">City is required.</span>
                <span ng-show="errorCity" class="help-block has-error ng-hide">{{errorCity}}</span>
                <br><br>
              </div>
            </div>


            <div class="form-group">
              <label class="col-sm-3 control-label">Tell us your story(if possible, in as much detail as you can)</label>
              <div class="col-sm-9">
                <textarea class="form-control" rows="4" cols="4" type="text" name="story" ng-model="homeless.story" required></textarea>
                <span ng-show="submitted && homelessForm.story.$error.required"  class="help-block has-error ng-hide">Story is required.</span>
                <span ng-show="errorStory" class="help-block has-error ng-hide">{{errorStory}}</span>
                <br><br>
              </div>
            </div>

            <div class="form-group">
              <label class="col-sm-3 control-label">What help did you receive?</label>
              <div class="col-sm-9">
                <input class="form-control" type="text" name="help" ng-model="homeless.help" required>
                <span ng-show="submitted && homelessForm.help.$error.required"  class="help-block has-error ng-hide">Help is required.</span>
                <span ng-show="errorHelp" class="help-block has-error ng-hide">{{errorHelp}}</span>
                <br><br>
              </div>
            </div>

            <div class="form-group">
              <label class="col-sm-3 control-label">How would you rate the help you received?</label>
              <div class="col-sm-9">
                <p id="rate"  class="col-sm-7"></p>
                <input type="hidden" name="rate" ng-model="homeless.rate" id="t1">
                <span ng-show="submitted && homelessForm.rate.$error.required"  class="help-block has-error ng-hide">Rating is required</span>
                <br><br><br>
              </div>
              <br>
            </div>


            <div class="form-group">
              <label class="col-sm-3 control-label">Any other significant information you wish to share?(in as much detail as possible please)</label>
              <div class="col-sm-9">
                <textarea class="form-control" rows="4" cols="4" type="text" name="info" ng-model="homeless.info" required></textarea>
                <span ng-show="submitted && homelessForm.info.$error.required"  class="help-block has-error ng-hide">Significant Information is required.</span>
                <span ng-show="errorInfo" class="help-block has-error ng-hide">{{errorInfo}}</span>
                <br><br>
              </div>
            </div>



            <div class="form-group">
              <div class="col-sm-12">
                <button type="submit" class="btn btn-info pull-right btn-sm" ng-click="submitted = true"><strong><i class="fa fa-plus"></i> Submit</strong></button><span class="pull-right"> &nbsp; &nbsp; </span>
              </div>
            </div>
          </form>
        </div>
      </section>
    </section>

  </div>

</div>