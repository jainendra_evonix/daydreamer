 <style type="text/css">
 #packagesblk .panel-heading {
  
  border-color: #474752;
  color: #fff;
  font-weight: 600;
  min-height: 165px;
  font-size: 16px;
  background-size: cover;
  background-position: center;
  position: relative;
}
#packagesblk .panel-heading::before {
    content: '';
    width: 100%;
    height: 100%;
    display: block;
    position: absolute;
    background: rgba(0, 0, 0, 0.4);
    top: 0;
    left: 0;
    z-index: 0;
}
#packagesblk .panel-heading .price {
  bottom: -45px;
}
#packagesblk .panel-heading .price, #packagesblk .panel-heading span {
    position: relative;
}
#packagesblk .panel-heading .text-xxxl {
  font-size: 46px;
  font-weight: 600;
}
#packagesblk .panel-body .editbtn {
  background-color: transparent;
  border: 1px solid #aaa;
  width: 30px;
  height: 30px;
  border-radius: 50%;
  color: #777;
  transition: all .3s ease;
  font-size: 15px;
}
#packagesblk .panel-body button:hover {
  background-color: #777;
  color: #fff;
  border: 1px solid #777;
}
#packagesblk .panel {
  margin-bottom: 30px;
  box-shadow: 0 2px 15px rgba(0,0,0,0.1);
}

.help-block{

  color:red;
}

.help-block.has-error{

  display: block;
  position: absolute;
  top: 36px;
}
.input-group {
  width: 100%;
}
.form-signin input {
  margin-bottom: 30px !important;
}

.input-group-addon.date{

  position: absolute;
  right: 0;
  padding: 11px;
  width: 40px;
  height: 38px;
}
.comment {
  width: 210px;
  background-color: #fff;
  margin: 10px;
  min-height:54px;
}
a.morelink {
  text-decoration:none;
  outline: none;
}
.morecontent span {
  display: none;

}

.datepicker.datepicker-dropdown{

  z-index:9999 !important;

}

.end_date{

    bottom: 13px;
    color: black;
    font-size: 15px;
    font-weight: bold;
    }

/*.input-group.amount{

    height: 38px;
    padding: 11px;
    position: absolute;
    right: 0;
    width: 40px;


}
*/
.job_adds {
text-align: left;
}
.premium_jobs {
text-align: right;
}
.job_adds, .premium_jobs {
  font-size: 11px;
  font-weight: bold;
  padding-bottom: 10px;
}

.editprofileimg.packageimg {
    bottom: -2px;
    color: #ddd;
    position: absolute;
    right: 1px;
    transition: all 0.3s ease;
}
.editprofileimg.packageimg:hover {
  color: #fff;
}

.profileimg {
  width:250px;
  
  position: relative;
  display: block;
  margin: auto;
}

.editprofileimg {
  position: absolute;
  bottom: 46px;
  right: 46px;
  width: 40px;
  height: 40px;
  background-color: rgba(0, 0, 0, 0.7);
  color: #ccc;
  padding: 7px;
  font-size: 18px;
  text-align: center;
  border-radius: 4px;
}
.editprofileimg:hover {
  color: #fff;
  background-color: rgba(0, 0, 0, 1);
}

.cropit-preview 

{ 
  background-color: #f8f8f8; 
  background-size: cover;
  border: 1px solid #ccc;
  border-radius: 3px; 
  margin-top: 7px;
  width: 202px;
  height: 202px;

}
.cropit-preview-image-container 
{ 
  cursor: move; 
} 
.image-size-label 
{ 
  margin-top: 10px; 
} 
input,.export
{
 display: block; 
} 
button 
{ 
  margin-top: 10px; 
} 
.req{ 
  color:red; 
} 


.socialfgroup {
  position: relative;
}
.socialerror {
    bottom: -30px;
    position: absolute;
}
</style>


<!--main content start-->
<section id="packagesblk" class="container">
  <section class="wrapper">
    <!-- page start-->
    <div class="row">
      <div class="col-md-9">
        <br>

        <?php echo $this->session->flashdata('successmsg');?>
        <?php echo $this->session->flashdata('errormsg');?>

        <br>
      </div>
      <div class="col-md-3">
        <br>
        <!--<button href="#myModal-1" data-toggle="modal" type="button" class="btn btn-success btn-sm pull-right"><i class="fa fa-plus"></i> <strong>Add Package</strong></button><br>-->
         <a href="<?php echo base_url();?>admin/addNewPackage" class="btn btn-success">Add New Package</a>
        <!-- Modal -->
        <form class="form-signin" name="packageForm" ng-submit="submitPackageForm()"  novalidate>

          <div aria-hidden="true" aria-labelledby="myModalLabel" role="dialog" tabindex="-1" id="myModal-1" class="modal fade">
            <div class="modal-dialog">
              <div class="modal-content">
                <div class="modal-header">
                  <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                  <h4 class="modal-title">Add Package</h4>
                </div>

                <div class="modal-body">
                  <div id="message2"></div>
                  <div class="input-group date">
                    <input type="text" class="form-control placeholder-no-fix" placeholder="Package Title" autocomplete="off" name="package_title" ng-model="addPackage.package_title" required>
                    
                     <span ng-show="submitted && packageForm.package_title.$invalid" class="help-block has-error">Package Title is required.</span>
                    <span ng-show="errorTitle" class="help-block has-error ng-hide">{{errorTitle}}</span>
                  </div>

                  <div class="input-group date">
                    <textarea rows="8" cols="4" class="form-control placeholder-no-fix" placeholder="Package Description" autocomplete="off" name="package_description" ng-model="addPackage.package_description" required style="height:85px;width:559px;"></textarea>
                    <span style="padding-top:42px;" ng-show="submitted && packageForm.package_description.$invalid" class="help-block has-error">Package Decription is required.</span>
                    <span ng-show="errorDescription" class="help-block has-error ng-hide">{{errorDescription}}</span>
                  </div></br>
                  <div class="input-group date"><!-- <span class="input-group-addon">$</span> -->
                    <input type="text" class="form-control placeholder-no-fix" placeholder="Amount" autocomplete="off" name="amount" ng-model="addPackage.amount" required>
                    <span ng-show="submitted && packageForm.amount.$invalid" class="help-block has-error">Amount is required.</span>
                    <span ng-show="errorAmount" class="help-block has-error ng-hide">{{errorAmount}}</span>
                  </div>

                           <!-- <md-input-container> 
                           <md-datepicker ng-model="myDate" name ="myDate" md-placeholder="Enter DOB" md-min-date="minDate" md-max-date="maxDate" value="" ng-change="license.expirationdate = myDate.toISOString()" ng-required="true" ></md-datepicker> 
                           <div ng-message="required">This dob is required!</div>
                         </md-input-container> -->
                         <div class="input-group date" id="demo-date2">
                          <div class="input-group-content">
                            <label class="end_date" style="font-weight:bold;">Package End Date</label>
                            <input calendar type="text" class="form-control" autocomplete="off"  ng-model="addPackage.validity" name="validity" placeholder="Validity Date"  id="validity" onchange="hideMsg();" required style="margin-bottom:0px;">
                            <span id="msg" ng-show="submitted && packageForm.validity.$invalid" class="help-block has-error">Validity Date is required.</span>
                            <span ng-show="errorValidity" class="help-block has-error ng-hide">{{errorValidity}}</span>
                          </div>
                          <a href="javascript:void(0);" class="input-group-addon date"><i class="fa fa-calendar"></i></a>
                        </div>

                         <!-- <div class="checkbox" style="margin-top:-12px;">
                        <label class="end_date" style="font-weight:bold;"><input type="checkbox" value="">End Date Yes/No?</label>
                        <br><span id="emailHelp" class="form-text small" style="color: #555;">(If you click on End Date it will be your package validity date)</span>
                        </div> -->

                         <div class="checkbox">
                        <label class="end_date" style="font-weight:bold;">
                          <input id="chkPassport" type="checkbox" value="" onclick="ShowTotalJobs(this)">Job Ads</label>
                        </div>

                         <div id="dvPassport" style="display:none">
                            Number of Job Ads:
                            
                            <input type="text" class="form-control" name="total_jobs" ng-model="addPackage.total_jobs"  id="total_jobs" style="width:75px;" />
                            <span style="padding-top:42px;" ng-show="submitted && packageForm.total_jobs.$invalid" class="help-block has-error">Total Jobs is required.</span>
                            <span ng-show="errorTotalJobs" class="help-block has-error ng-hide">{{errorTotalJobs}}</span>
                        </div>

                        <div class="checkbox">
                        <label class="end_date" style="font-weight:bold;">
                          <input id="chkPremium" type="checkbox"  value="" onclick="showPremiumJobs(this)">Premium Jobs</label>
                          
                        </div>


                         <div id="dvPremium"  style="display:none">
                            Number of Premium Jobs:

                            <input type="text" class="form-control" name="premium_jobs" id="premium_jobs" ng-model="addPackage.premium_jobs"  id="total_premium_jobs" style="width:75px;" />
                            <span style="padding-top:42px;" ng-show="submitted && packageForm.premium_jobs.$invalid" class="help-block has-error">Premium Jobs is required.</span>
                            <span ng-show="errorPremiumJobs" class="help-block has-error ng-hide">{{errorPremiumJobs}}</span>
                        </div>

                      </div>
                      <div class="modal-footer">
                        <button data-dismiss="modal" class="btn btn-default" type="button">Cancel</button>
                        <button class="btn btn-success" ng-click="submitted = true" type="submit">Submit</button>
                      </div>

                    </div>
                  </div>
                </div>
              </form>



              <form class="form-signin" name="editpackageForm" ng-submit="submitEditPackageForm()" novalidate>

                <div aria-hidden="true" aria-labelledby="myModalLabel" role="dialog" tabindex="-1" id="myModal-2" class="modal fade">
                  <div class="modal-dialog">
                    <div class="modal-content">
                      <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                        <h4 class="modal-title">Edit Package</h4>
                      </div>
                      
                      <div class="modal-body">
                        <div id="message2"></div>
                        <input type="hidden" class="form-control" value="" name="edit_package_id"  id="edit_package_id">
                        <input type="hidden" class="form-control" value="" name="job_adds"  id="job_adds">
                         <input type="hidden" class="form-control" name="premium_jobs" id="premium_jobs">
                         <div class="input-group date">
                   <input type="text" class="form-control placeholder-no-fix" placeholder="Package Title" autocomplete="off" name="package_title" ng-model="package.package_title" required>
                     <span ng-show="submitted && packageForm.package_title.$invalid" class="help-block has-error">Package Title is required.</span>
                    <span ng-show="errorTitle" class="help-block has-error ng-hide">{{errorTitle}}</span>
                  </div></br>
                        <span ng-show="editpackagesubmitted && editPackageForm.package_title.$invalid" class="help-block has-error">Package Title is required.</span>
                        <span ng-show="errorEditTitle" class="help-block has-error ng-hide">{{errorEditTitle}}</span>


                        <textarea rows="8" cols="4" class="form-control placeholder-no-fix" placeholder="Package Description" autocomplete="off" name="package_description" ng-model="package.package_description" required style="height:85px;width:559px;"></textarea>
                        <span ng-show="editpackagesubmitted && editPackageForm.package_description.$invalid" class="help-block has-error">Package Decription is required.</span>
                        <span ng-show="errorEditDescription" class="help-block has-error ng-hide">{{errorEditDescription}}</span></br>


                        <input type="text" class="form-control placeholder-no-fix" placeholder="Amount" autocomplete="off" name="amount" ng-model="package.amount" required>
                        <span ng-show="editpackagesubmitted && editPackageForm.amount.$invalid" class="help-block has-error">Amount is required.</span>
                        <span ng-show="errorEditAmount" class="help-block has-error ng-hide">{{errorEditAmount}}</span>




                        <div class="input-group date" id="demo-date1">
                          <div class="input-group-content">
                            <label class="end_date" style="font-weight:bold;">Package End Date</label>
                            <input calendar1 type="text" class="form-control" autocomplete="off"   name="validity" ng-model="package.validity" placeholder="Validity Date"  id="package_validity" required style="margin-bottom:0px;">
                            <span ng-show="editpackagesubmitted && editPackageForm.validity.$invalid" class="help-block has-error">Validity Date is required.</span>
                            <span ng-show="errorEditValidity" class="help-block has-error ng-hide">{{errorEditValidity}}</span>
                          </div>
                          <a href="javascript:void(0);" class="input-group-addon date"><i class="fa fa-calendar"></i></a>


                        </div>

                         <!-- <div class="checkbox" style="margin-top:-12px;">
                        <label class="end_date" style="font-weight:bold;"><input type="checkbox" value="">End Date Yes/No?</label>
                        </div><br/> -->


                        <div class="checkbox">
                        <label class="end_date" style="font-weight:bold;">
                          <input id="jobAds" type="checkbox" value="" onclick="updateJobs(this)">Job Ads</label>
                        </div>

                         <div id="updateJobAds" style="display:none">
                            Number of Job Ads:
                            
                            <input type="text" class="form-control" name="update_jobs" ng-model="package.update_jobs"  id="updateAds" style="width:75px;"/>
                            <span style="padding-top:42px;" ng-show="editpackagesubmitted && editPackageForm.update_jobs.$invalid" class="help-block has-error">Total Jobs is required.</span>
                            <!-- <span ng-show="errorTotalJobs" class="help-block has-error ng-hide">{{errorTotalJobs}}</span> -->
                        </div>

                        <div class="checkbox">
                        <label class="end_date" style="font-weight:bold;">
                          <input id="updatePremium" type="checkbox"  value="" onclick="updatePremiumJobs(this)">Premium Jobs</label>
                          
                        </div>
                        <div id="premium"  style="display:none">
                            Number of Premium Jobs:

                            <input type="text" class="form-control" name="premiumJobs"  ng-model="package.premiumJobs"  id="total_premium_jobs" style="width:75px;"/>
                            <span style="padding-top:42px;" ng-show="editpackagesubmitted && editPackageForm.premiumJobs.$invalid" class="help-block has-error">Premium Jobs is required.</span>
                            <!-- <span ng-show="errorPremiumJobs" class="help-block has-error ng-hide">{{errorPremiumJobs}}</span> -->
                        </div>

                      
                      </div>
                      <div class="modal-footer">

                        <button data-dismiss="modal" class="btn btn-default" type="button">Cancel</button>
                        <button class="btn btn-success" ng-click="editpackagesubmitted = true" type="submit">Submit</button>
                      </div>

                    </div>
                  </div>
                </div>
              </form>









              <!-- modal -->
            </div>
            <div class="clearfix"></div>
            <?php
            if(!empty($package_data)){
              foreach($package_data as $key){
                $image=base_url().'package_images/'.$key->package_pic;
                ?>
                <div class="col-md-3">
                    <h4 class="text-center"><strong><?php echo $key->package_title;?></strong></h4>
                  <section class="panel text-center" style="width: 102%;">
                    <div class="panel-heading" style="background-image: url('<?php echo $image;?>');">
                     
                      <!-- <span><?php //echo $key->package_title;?></span> -->
                      <br>
                      <div class="price">
                        <h2><span class="text-lg">£</span> <span class="text-xxxl"><?php echo $key->price_amount;?></span></h2>
                      </div>
                      <div class="">
                      <!-- <a href="#" class="packageimg"><i class="fa fa-edit"></i></a> -->
                      <a class="editprofileimg packageimg"  href="<?php echo base_url();?>admin/changePackagePic?id=<?php echo $key->id;?>"><i class="fa fa-edit"></i></a>
                    </div>
                    </div>
                    <div class="panel-body">
                      <!-- <h5>Validity date: <?php //echo date('jS M Y',strtotime($key->validity_date));?></h5> -->
                       <div class="comment more">
                        <?php echo $key->package_description;?>
                      </div>
                      
                      <table class="table table-condensed">
                        <tbody>
                          <tr>
                            <td width="33%"><strong><a href="<?php echo base_url();?>admin/createJobAdd?id=<?php echo $key->id;?>" class="btn btn-primary btn-block btn-sm">Job Adds</a></strong></td>
                            <td width="33%"><strong><a href="<?php echo base_url();?>admin/viewAndSaveCV?id=<?php echo $key->id;?>" class="btn btn-success btn-block btn-sm">Save CV</a></strong></td>
                            <td width="33%"><strong><a href="<?php echo base_url();?>admin/createService?id=<?php echo $key->id;?>" class="btn btn-warning btn-block btn-sm">Service</a></strong></td>
                          </tr>
                          <tr>
                            <td colspan="4"><strong><?php echo $key->totalJobAdds;?> </strong>Job Adds</td>
                          </tr>
                          <tr>
                           <?php
                             $service=$key->totalServices;
                           // echo '<pre>'; print_r($service);
                             $service_array=json_decode($service);
                            //print_r($service_array);
                             if(isset($service_array)){
                              $serviceName='';
                             foreach($service_array as $serv){
                                 error_reporting(0);
                               $serviceName.=ucfirst($showService[$serv]).',';
                            

                             
                             
                          }
                          $serviceName=rtrim($serviceName,',');
                        }?>

                            <td colspan="4"><strong>Service Name</strong> <?php echo isset($service_array)&&isset($serviceName)?$serviceName:'No Service Name';?></td>

                            
                            
                          </tr>
                          <tr>
                            <td colspan="4"><strong><?php echo date('jS M Y',strtotime($key->validity_date))?></strong> Validity Date</td>
                          </tr>
                        </tbody>
                      </table>
                      <div class="row">
                        <div class="col-xs-6">
                          <!-- <div class="job_adds">Job Ads:<?php //echo $key->job_adds?></div> -->
                        </div>
                        <div class="col-xs-6">
                          <!-- <div class="premium_jobs">Premium Jobs:<?php //echo $key->premium_jobs?></div> -->
                        </div>
                        
                      </div>
                     

                      <a href="<?php echo base_url();?>admin/deletePackage?id=<?php echo $key->id;?>" class="btn btn-default btn-xs pull-left editbtn" onclick="return confirmDelete();"><i class="fa fa-trash-o"></i></a>
                      <!-- <a  id="edit" data="<?php //echo $key->id; ?>" href="#myModal-2" data-toggle="modal" data-dismiss="modal" ng-click="edit_package(<?php //echo $key->id ?>)" class="btn btn-default btn-xs pull-right editbtn" ><i class="fa fa-pencil-square-o"></i></a> -->
                      <a  id="edit"  href="<?php echo base_url()?>admin/addNewPackage?id=<?php echo $key->id;?>"  class="btn btn-default btn-xs pull-right editbtn" ><i class="fa fa-pencil-square-o"></i></a>
                    </div>
                  </section>
                </div>
                <!-- <div class="clearfix"></div> -->
                <?php }}else{

                 echo "No packages found"; 

               }?>
                    <!-- <div class="col-md-3">
                        <section class="panel text-center">
                            <div class="panel-heading">Package Heading
                            <div class="price">
                                <h2><span class="text-lg">£</span> <span class="text-xxxl">5000</span></h2>
                            </div>
                            </div>
                            <div class="panel-body">
                                <br>
                                <h4>Validity date: 23-4-2017</h4>
                                <hr>
                                <button type="button" class="btn btn-default btn-xs pull-left"><i class="fa fa-trash-o"></i></button>
                                <button type="button" class="btn btn-default btn-xs pull-right"><i class="fa fa-pencil-square-o"></i></button>
                            </div>
                        </section>
                      </div> -->
                   <!--  <div class="col-md-3">
                        <section class="panel text-center">
                            <div class="panel-heading">Package Heading
                            <div class="price">
                                <h2><span class="text-lg">£</span> <span class="text-xxxl">5000</span></h2>
                            </div>
                            </div>
                            <div class="panel-body">
                                <br>
                                <h4>Validity date: 23-4-2017</h4>
                                <hr>
                                <button type="button" class="btn btn-default btn-xs pull-left"><i class="fa fa-trash-o"></i></button>
                                <button type="button" class="btn btn-default btn-xs pull-right"><i class="fa fa-pencil-square-o"></i></button>
                            </div>
                        </section>
                      </div> -->
                    <!-- <div class="col-md-3">
                        <section class="panel text-center">
                            <div class="panel-heading">Package Heading
                            <div class="price">
                                <h2><span class="text-lg">£</span> <span class="text-xxxl">5000</span></h2>
                            </div>
                            </div>
                            <div class="panel-body">
                                <br>
                                <h4>Validity date: 23-4-2017</h4>
                                <hr>
                                <button type="button" class="btn btn-default btn-xs pull-left"><i class="fa fa-trash-o"></i></button>
                                <button type="button" class="btn btn-default btn-xs pull-right"><i class="fa fa-pencil-square-o"></i></button>
                            </div>
                        </section>
                      </div> -->
                   <!--  <div class="col-md-3">
                        <section class="panel text-center">
                            <div class="panel-heading">Package Heading
                            <div class="price">
                                <h2><span class="text-lg">£</span> <span class="text-xxxl">5000</span></h2>
                            </div>
                            </div>
                            <div class="panel-body">
                                <br>
                                <h4>Validity date: 23-4-2017</h4>
                                <hr>
                                <button type="button" class="btn btn-default btn-xs pull-left"><i class="fa fa-trash-o"></i></button>
                                <button type="button" class="btn btn-default btn-xs pull-right"><i class="fa fa-pencil-square-o"></i></button>
                            </div>
                        </section>
                      </div> -->
                   <!--  <div class="col-md-3">
                        <section class="panel text-center">
                            <div class="panel-heading">Package Heading
                            <div class="price">
                                <h2><span class="text-lg">£</span> <span class="text-xxxl">5000</span></h2>
                            </div>
                            </div>
                            <div class="panel-body">
                                <br>
                                <h4>Validity date: 23-4-2017</h4>
                                <hr>
                                <button type="button" class="btn btn-default btn-xs pull-left"><i class="fa fa-trash-o"></i></button>
                                <button type="button" class="btn btn-default btn-xs pull-right"><i class="fa fa-pencil-square-o"></i></button>
                            </div>
                        </section>
                      </div> -->
                    <!-- <div class="col-md-3">
                        <section class="panel text-center">
                            <div class="panel-heading">Package Heading
                            <div class="price">
                                <h2><span class="text-lg">£</span> <span class="text-xxxl">5000</span></h2>
                            </div>
                            </div>
                            <div class="panel-body">
                                <br>
                                <h4>Validity date: 23-4-2017</h4>
                                <hr>
                                <button type="button" class="btn btn-default btn-xs pull-left"><i class="fa fa-trash-o"></i></button>
                                <button type="button" class="btn btn-default btn-xs pull-right"><i class="fa fa-pencil-square-o"></i></button>
                            </div>
                        </section>
                      </div> -->
                    <!-- <div class="col-md-3">
                        <section class="panel text-center">
                            <div class="panel-heading">Package Heading
                            <div class="price">
                                <h2><span class="text-lg">£</span> <span class="text-xxxl">5000</span></h2>
                            </div>
                            </div>
                            <div class="panel-body">
                                <br>
                                <h4>Validity date: 23-4-2017</h4>
                                <hr>
                                <button type="button" class="btn btn-default btn-xs pull-left"><i class="fa fa-trash-o"></i></button>
                                <button type="button" class="btn btn-default btn-xs pull-right"><i class="fa fa-pencil-square-o"></i></button>
                            </div>
                        </section>
                      </div> -->
                    </div>
                    <!-- page end-->

                  </section>
                </section>