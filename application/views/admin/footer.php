        <section id="footer">
            <div class="container">
                <div class="row">
                    <div class="col-md-4" id="fabout">
                        <h3>DAY <span>DREAMER</span></h3>
                        <p class="text-justify">Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
                        tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,
                        quis nostrud exercitation ullamco laboris.</p>
                        <img src="<?php echo base_url();?>client_assets/images/app.png">
                    </div>
                    <div class="col-md-3">
                        <h4>QUICK LINKS</h4>
                        <ul class="list-unstyled">
                            <li><a href="#">About Us</a></li>
                            <li><a href="#">Support</a></li>
                            <li><a href="#">License</a></li>
                            <li><a href="#">Affiliate</a></li>
                            <li><a href="#">Pricing</a></li>
                            <li><a href="#">Terms &amp; Condition</a></li>
                            <li><a href="#">Contact</a></li>
                        </ul>
                    </div>
                    <div class="col-md-3">
                        <h4>TRENDING JOBS</h4>
                        <ul class="list-unstyled">
                            <li><a href="#">UI Designer</a></li>
                            <li><a href="#">Frontend Developer</a></li>
                            <li><a href="#">Junior Tester</a></li>
                            <li><a href="#">Senior Teamleader</a></li>
                            <li><a href="#">iOS Developer</a></li>
                            <li><a href="#">Android Developer</a></li>
                            <li><a href="#">Technical Support</a></li>
                        </ul>
                    </div>
                    <div class="col-md-2">
                        <h4>FOLLOW US</h4>
                        <ul class="list-unstyled social">
                            <li><a href="#" class="fb"><i class="fa fa-facebook"></i> <strong>Facebook</strong></a></li>
                            <li><a href="#" class="tw"><i class="fa fa-twitter"></i> <strong>Twiter</strong></a></li>
                            <li><a href="#" class="gp"><i class="fa fa-google-plus"></i> <strong>Google Plus</strong></a></li>
                        </ul>
                    </div>
                </div>
            </div>
        </section>

        <footer class="footer-section">
          <div class="text-center">
              2016 © Day Dreamer | Crafted by Evonix Technologies
              <a href="#" class="go-top">
                  <i class="fa fa-angle-up"></i>
              </a>
          </div>
        </footer>
</section>

<!-- Placed js at the end of the document so the pages load faster -->




