  <style type="text/css">
 .help-block{
  
  color:red;
}
.autocomplete-suggestions {
  background-color: #fff;
  border: 1px solid #ddd;
  box-shadow: 0 3px 10px rgba(0, 0, 0, 0.2);
}
.autocomplete-suggestion {
  padding: 5px 12px;
}
.autocomplete-suggestion:hover {
  background-color: #333;
  color: #fff;
}

 </style>
 <div class="container">
    <div class="row">
        <div class="col-sm-6 col-sm-offset-3">

 <div class="row well" style="padding: 10px 0;">
                                    <div class="col-sm-12">
                                        <h4><strong>Create Timesheet</strong></h4><br>
                                        <?php echo $this->session->flashdata('successmsg');?>
                                        <?php echo $this->session->flashdata('errormsg');?>
                                        
                                    </div>
                                    <form class="" ng-submit="submitTimesheetForm()" name="timesheetForm" novalidate>
                                        <input type="hidden" name="timesheet_id" ng-model="timesheet.timesheet_id">
                                        <input type="hidden" name="candidate_id" ng-model="timesheet.candidate_id">  
                                  <div class="form-group">
                                        <label class="col-sm-3 control-label">Type:</label>
                                        <div class="col-sm-9" style="padding-left: 0;">
                                            <!-- <input type="text" class="form-control"  name="currency" ng-model="timesheet.currency" style="resize:none;" required> -->
                                            <select class="form-control" name="type" ng-model="timesheet.type">

                                             <option  disabled="disabled" selected="selected">Select Type</option>
                                            
                                             <option value="interview">Interview</option>
                                             <option value="training">Training</option>
                                            
                                            </select>
                                            <span ng-show="submitted && timesheetForm.type.$error.required"  class="help-block has-error ng-hide">Type is required.</span>
                                            <span ng-show="errorType" class="help-block has-error ng-hide">{{errorType}}</span>
                                            <br>
                                        </div>
                                    </div>
                                  <!--  <div class="col-sm-3 form-group" style="padding-left: 0;">
                                    <label class="col-sm-3 control-label">Day:</label>
                                            <select class="form-control" style="padding: 6px 0px;" name="day" ng-model="timesheet.day">
                                                <option disabled="disabled" selected="selected" value="">Select Day</option>
                                                <option value="Monday">Monday</option>
                                                <option value="Tuesday">Tuesday</option>
                                                <option value="Wednesday">Wednesday</option>
                                                <option value="Thursday">Thursday</option>
                                                <option value="Friday">Friday</option>
                                                <option value="Saturday">Saturday</option>
                                                <option value="Sunday">Sunday</option>
                                            </select>
                                            <span ng-show="submitted && timesheetForm.day.$error.required"  class="help-block has-error ng-hide">Day is required.</span>
                                            <span ng-show="errorDay" class="help-block has-error ng-hide">{{errorDay}}</span>
                                        </div> -->
                                    
                      
                                    <div class="form-group">
                                       <label class="col-sm-3  control-label">Date:</label>
                                       <div class="col-sm-9" style="padding-left: 0;">
                                        <div class="input-group date" id="demo-date" style="width:100%">
                                          <div class="input-group-content">
                                            <input data-calenderend type="text" class="form-control" autocomplete="off"  ng-model="timesheet.date" name="date" placeholder="Please enter Date"  id="date"   style="margin-bottom:0px;">
                                            <!-- <span id="msg1" ng-show="submitted && timesheetForm.date.$invalid" class="help-block has-error">Date is required.</span> -->
                                            <!-- <span ng-show="errorValidity" class="help-block has-error ng-hide">{{errorValidity}}</span> -->
                                            <span ng-show="errorEndDate" class="help-block has-error ng-hide">{{errorEndDate}}</span>
                                          </div>
                                          <!-- <a href="javascript:void(0);" class="input-group-addon date"><i class="fa fa-calendar"></i></a> -->
                                        </div>
                                        <br>
                                      </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-sm-3 control-label">Start Time:</label>
                                        <div class="col-xs-3" style="padding-left: 0;">
                                            <select class="form-control" style="padding: 6px 0px;" name="hours" ng-model="timesheet.hours">
                                                <option disabled="disabled" selected="selected" value="">Select hours</option>
                                                <option value="1">1</option>
                                                <option value="2">2</option>
                                                <option value="3">3</option>
                                                <option value="4">4</option>
                                                <option value="5">5</option>
                                                <option value="6">6</option>
                                                <option value="7">7</option>
                                                <option value="8">8</option>
                                                <option value="9">9</option>
                                                <option value="10">10</option>
                                                <option value="11">11</option>
                                                <option value="12">12</option>
                                                <option value="13">13</option>
                                                <option value="14">14</option>
                                                <option value="15">15</option>
                                                <option value="16">16</option>
                                                <option value="17">17</option>
                                                <option value="18">18</option>
                                                <option value="19">19</option>
                                                <option value="20">20</option>
                                                <option value="21">21</option>
                                                <option value="22">22</option>
                                                <option value="23">23</option>
                                                <option value="24">24</option>

                                            </select>
                                            <!-- <span ng-show="submitted && eventForm.hours.$error.required"  class="help-block has-error ng-hide">Hours is required.</span>
                                            <span ng-show="errorEventTime" class="help-block has-error ng-hide">{{errorEventTime}}</span> -->
                                        </div>
                                        <div class="col-xs-3" style="padding-left: 0;">
                                            <select class="form-control" style="padding: 6px 0px;" name="minutes" ng-model="timesheet.minutes">
                                                <option disabled="disabled" selected="selected" value="">Select minutes</option>
                                                <option value="00">00</option>
                                                <option value="15">15</option>
                                                <option value="30">30</option>
                                                <option value="45">45</option>
                                                <option value="60">60</option>
                                            </select>
                                            <!-- <span ng-show="submitted && eventForm.minutes.$error.required"  class="help-block has-error ng-hide">Minutes is required.</span>
                                            <span ng-show="errorMinutes" class="help-block has-error ng-hide">{{errorMinutes}}</span> -->
                                        </div>
                                        <div class="col-xs-3" style="padding-left: 0;" >
                                            <select class="form-control" style="padding: 6px 0px;" name="meridium" ng-model="timesheet.meridium">
                                                                                                <option disabled="disabled" selected="selected" value="">Select minutes</option>
                                             <option disabled="disabled" selected="selected" value="">Select seconds</option>
                                                <option value="AM" selected="true">AM</option>
                                                <option value="PM">PM</option>
                                            </select>
                                            <!-- <span ng-show="submitted && eventForm.meridium.$error.required"  class="help-block has-error ng-hide">Merīdiem is required.</span>
                                            <span ng-show="errorMeridium" class="help-block has-error ng-hide">{{errorMeridium}}</span> -->
                                            <br>
                                        </div>
                                    </div>

                                      <div class="form-group">
                                        <label class="col-sm-3 control-label">Finish Time:</label>
                                        <div class="col-xs-3" style="padding-left: 0;">
                                            <select class="form-control" style="padding: 6px 0px;" name="finish_hours" ng-model="timesheet.finish_hours">
                                                <option disabled="disabled" selected="selected" value="">Select hours</option>
                                                <option value="1">1</option>
                                                <option value="2">2</option>
                                                <option value="3">3</option>
                                                <option value="4">4</option>
                                                <option value="5">5</option>
                                                <option value="6">6</option>
                                                <option value="7">7</option>
                                                <option value="8">8</option>
                                                <option value="9">9</option>
                                                <option value="10">10</option>
                                                <option value="11">11</option>
                                                <option value="12">12</option>
                                                <option value="13">13</option>
                                                <option value="14">14</option>
                                                <option value="15">15</option>
                                                <option value="16">16</option>
                                                <option value="17">17</option>
                                                <option value="18">18</option>
                                                <option value="19">19</option>
                                                <option value="20">20</option>
                                                <option value="21">21</option>
                                                <option value="22">22</option>
                                                <option value="23">23</option>
                                                <option value="24">24</option>
                                            </select>
                                            <!-- <span ng-show="submitted && eventForm.hours.$error.required"  class="help-block has-error ng-hide">Hours is required.</span>
                                            <span ng-show="errorEventTime" class="help-block has-error ng-hide">{{errorEventTime}}</span> -->
                                        </div>
                                        <div class="col-xs-3" style="padding-left: 0;">
                                            <select class="form-control" style="padding: 6px 0px;" name="finish_minutes" ng-model="timesheet.finish_minutes">
                                                <option disabled="disabled" selected="selected" value="">Select minutes</option>
                                                 <option value="00">00</option>
                                                <option value="15">15</option>
                                                <option value="30">30</option>
                                                <option value="45">45</option>
                                                <option value="60">60</option>
                                            </select>
                                            <!-- <span ng-show="submitted && eventForm.minutes.$error.required"  class="help-block has-error ng-hide">Minutes is required.</span>
                                            <span ng-show="errorMinutes" class="help-block has-error ng-hide">{{errorMinutes}}</span> -->
                                        </div>
                                        <div class="col-xs-3" style="padding-left: 0;" >
                                            <select class="form-control" style="padding: 6px 0px;" name="finish_meridium" ng-model="timesheet.finish_meridium">
                                             <option disabled="disabled" selected="selected" value="">Select minutes</option>
                                             <option disabled="disabled" selected="selected" value="">Select seconds</option>
                                                <option value="AM" selected="true">AM</option>
                                                <option value="PM">PM</option>
                                            </select>
                                            <!-- <span ng-show="submitted && eventForm.meridium.$error.required"  class="help-block has-error ng-hide">Merīdiem is required.</span>
                                            <span ng-show="errorMeridium" class="help-block has-error ng-hide">{{errorMeridium}}</span> -->
                                            <br>
                                        </div>
                                    </div>

                                    <div class="form-group">
                                                <label class="col-sm-12  control-label">Candidate Name:</label>
                                                <div class="col-sm-12">
                                                  
                                                     <input type="text"  class="form-control" name="cand_name" ng-model="timesheet.cand_name">
                                                    <!--  <input type="hidden" name="candidate_name" ng-model="timesheet.candidate_name" id="candidateName"> -->
                                                      <span ng-show="submitted && timesheetForm.cand_name.$error.required"  class="help-block has-error ng-hide">Candidate Name is required.</span>
                                                     <span ng-show="errorCandName" class="help-block has-error ng-hide">{{errorCandName}}</span>
                                                      <br>
                                                </div>

                                            </div>

                                    <div class="form-group">
                                                <label class="col-sm-12 control-label">Client Name:</label>
                                                <div class="col-sm-12">
                                                    
                                                     <input type="text"  class="form-control"   id="selctionajax" name="client_name" ng-model="timesheet.client_name">
                                                     <input type="hidden" name="client_name" ng-model="timesheet.client_name" id="organizationname">

                                                     <span ng-show="submitted && timesheetForm.client_name.$error.required"  class="help-block has-error ng-hide">Client Name is required.</span>
                                                     <span ng-show="errorClientName" class="help-block has-error ng-hide">{{errorClientName}}</span>
                                                </div>
                                            </div>
                                    <div class="form-group">
                                        <label class="col-sm-12 control-label">Unpaid Break Duration:</label>
                                        <div class="col-sm-12">
                                            <input class="form-control" type="text" name="break_duartion" ng-model="timesheet.break_duartion" required>
                                            <span ng-show="submitted && timesheetForm.break_duartion.$error.required"  class="help-block has-error ng-hide">Unpaid Break Duration is required.</span>
                                            <span ng-show="errorDuration" class="help-block has-error ng-hide">{{errorDuration}}</span>
                                            <br>
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label class="col-sm-12 control-label">Pay Rate:</label>
                                        <div class="col-sm-12">
                                            <input class="form-control" type="text" name="pay_rate" ng-model="timesheet.pay_rate" required>
                                            <span ng-show="submitted && timesheetForm.pay_rate.$error.required"  class="help-block has-error ng-hide">Pay Rate is required.</span>
                                            <span ng-show="errorRate" class="help-block has-error ng-hide">{{errorRate}}</span>
                                            <br>
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label class="col-sm-12 control-label">Location:</label>
                                        <div class="col-sm-12">
                                            <input class="form-control" type="text" name="location" ng-model="timesheet.location" required>
                                            <span ng-show="submitted && timesheetForm.location.$error.required"  class="help-block has-error ng-hide">Location is required.</span>
                                            <span ng-show="errorLocation" class="help-block has-error ng-hide">{{errorLocation}}</span>
                                            <br>
                                        </div>
                                    </div>
                                     <div class="form-group">
                                        <label class="col-sm-12 control-label">Currency:</label>
                                        <div class="col-sm-12">
                                            <!-- <input type="text" class="form-control"  name="currency" ng-model="timesheet.currency" style="resize:none;" required> -->
                                            <select class="form-control" name="currency" ng-model="timesheet.currency">

                                             <option  disabled="disabled" selected="selected">Select Currency</option>
                                             <?php
                                               foreach($currency as $key)
                                                {
                                             ?>
                                             <option value="<?php echo $key->currency_id;?>"><?php echo $key->currency_code;?></option>
                                             <?php }?>
                                            </select>
                                            <span ng-show="submitted && timesheetForm.currency.$error.required"  class="help-block has-error ng-hide">Currency is required.</span>
                                            <span ng-show="errorCurrency" class="help-block has-error ng-hide">{{errorCurrency}}</span>
                                            <br>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-sm-12 control-label">Pay Type:</label>
                                        <div class="col-sm-12">
                                            <input type="text" class="form-control"  name="pay_type" ng-model="timesheet.pay_type" style="resize:none;" required>
                                            <span ng-show="submitted && timesheetForm.pay_type.$error.required"  class="help-block has-error ng-hide">Pay Type is required.</span>
                                            <span ng-show="errorType" class="help-block has-error ng-hide">{{errorType}}</span>
                                            <br>
                                        </div>
                                    </div>
                                    
                                    
                                    
                                    <div class="form-group">
                                        <div class="col-sm-12">
                                            <button type="submit" class="btn btn-info pull-right btn-sm" ng-click="submitted = true"><strong><i class="fa fa-plus"></i> Submit</strong></button><span class="pull-right"> &nbsp; &nbsp; </span>
                                        </div>
                                    </div>
                                </form>
                                </div>

                                </div>


        </div>
    </div>