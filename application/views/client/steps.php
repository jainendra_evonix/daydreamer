<?php include 'header.php' ?>

        <section class="container-fluid" id="topbanner">
            <div class="row">
                <div class="col-md-6 col-md-offset-3">
                    <h2 class="text-center">Customer Online Form</h2>
                    <p class="text-center">Lorem ipsum dolor sit amet, consectetur adipisicing elit.</p>
                </div>
            </div>
        </section>
        <!--main content start-->
        <section id="" class="container">
            <section class="wrapper">
                <!-- page start-->

                <div class="row" id="formsteps">
                    <div class="col-md-6 col-md-offset-3">
                        <ul class="breadcrumbs-alt">
                            <li>
                                <a href="#">Basic Information</a>
                            </li>
                            <li>
                                <a href="#">Pages</a>
                            </li>
                            <li>
                                <a href="#">Pages</a>
                            </li>
                            <li>
                                <a href="#">Pages</a>
                            </li>
                            <li>
                                <a class="current" href="#">Elements</a>
                            </li>
                        </ul>
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-6 col-md-offset-3">
                        <section class="panel">
                            <div class="panel-body">
                                <form class="form-horizontal bucket-form" method="get">
                                    <div class="form-group">
                                        <label class="col-sm-3 control-label">Default</label>
                                        <div class="col-sm-9">
                                            <input type="text" class="form-control">
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-sm-3 control-label">Help text</label>
                                        <div class="col-sm-9">
                                            <input type="text" class="form-control">
                                            <span class="help-block">A block of help text that breaks onto a new line and may extend beyond one line.</span>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-sm-3 control-label">Placeholder</label>
                                        <div class="col-sm-9">
                                            <input type="text"  class="form-control" placeholder="placeholder">
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-sm-3 control-label">Password</label>
                                        <div class="col-sm-9">
                                            <input type="password"  class="form-control" placeholder="">
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </section>
                    </div>
                </div>
                <!-- page end-->
            </section>
        </section>
        <!--main content end-->

<?php include 'footer.php' ?>