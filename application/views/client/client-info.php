<style>

.form-horizontal.bucket-form .form-group-condensed .form-group {
    padding-bottom: 0px;
    margin-bottom: 0px;
}
.clientinfo tr td {
    width: 25%;
}
.clientinfo tr td a, .table tr td a {
    color: #428bca;
}
.clientinfo .text-right {
    background-color: #fcfcfc;
}
.collapsibleh4 {
    cursor: pointer;
}
.collapsibleh4.collapsed i {
    transform: rotate(-90deg);
    transition: all .3s ease;
}
.collapsibleh4 i {
    transition: all .3s ease;
}

</style>

<?php
$client_session_data=$this->session->userdata(base_url().'client_login_session');
$ParentName=$client_session_data['first_name'].'   '.$client_session_data['last_name'];
?>

        <!--main content start-->
        <section id="adminsection" class="container">
            <section class="wrapper">
                <!-- page start-->
                <div class="row">
                    <div class="col-md-12">
                        <h4 class="form-heading"><strong><?php echo $getClientInfo->subclient_name;?></strong><a class="btn btn-primary btn-sm pull-right" href="<?php echo base_url();?>client/insertSubclientJobOpening?id=<?php echo $getClientInfo->id;?>"><strong><i class="fa fa-plus"></i> Create Job Opening</strong></a></h4>
                       <!--  <p>Lorem ipsum dolor sit amet. <a href="#" class="pull-right">Help <i class="fa fa-question-circle"></i></a></p> -->

                        <br>
                        <section class="panel">
                            <div class="panel-body">
                                <div class="row">
                                    <div class="col-md-12">
                                        <h4>Basic Information</h4>
                                        <hr>
                                    </div>
                                    <form class="form-horizontal bucket-form" method="get" action="education.php">
                                        
                                    <div class="col-md-6 form-group-condensed">
                                        <!-- <div class="form-group">
                                            <label class="col-sm-5 control-label">Last Update:</label>
                                            <div class="col-sm-7">
                                                <p class="form-control-static">02:59 PM</p>
                                            </div>
                                        </div> -->
                                        <div class="form-group">
                                            <label class="col-sm-5 control-label">Account Manager:</label>
                                            <div class="col-sm-7">
                                                <p class="form-control-static"><?php echo $ParentName;?></p>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="col-sm-5 control-label">Industry:</label>
                                            <div class="col-sm-7">
                                                <p class="form-control-static"><?php echo $getClientInfo->industry_name;?></p>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="control-label col-sm-5">Contact Number:</label>
                                            <div class="col-sm-7">
                                                <p class="form-control-static"><?php echo $getClientInfo->contact_number;?></p>
                                            </div>
                                        </div>
                                    </div>
                                    <!-- <div class="col-md-6 form-group-condensed">
                                        <div class="form-group">
                                            <label class="col-sm-5 control-label">Last Update:</label>
                                            <div class="col-sm-7">
                                                <p class="form-control-static">02:59 PM</p>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="col-sm-5 control-label">Account Manager:</label>
                                            <div class="col-sm-7">
                                                <p class="form-control-static">Archit Srivastava</p>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="col-sm-5 control-label">Industry:</label>
                                            <div class="col-sm-7">
                                                <p class="form-control-static">Technology</p>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="control-label col-sm-5">Contact Number:</label>
                                            <div class="col-sm-7">
                                                <p class="form-control-static">(985) 847-1524</p>
                                            </div>
                                        </div>
                                    </div> -->

                                    <div class="col-md-12">
                                        <br>
                                        <h4>Client Information</h4>
                                        <hr>
                                    </div>
                                    <div class="col-md-12">
                                        <table class="table table-bordered clientinfo">
                                            <tbody>
                                              <tr>
                                                <td class="text-right"><strong>Office Name:</strong></td>
                                                <td><?php echo $getClientInfo->subclient_name;?></td>
                                                <td class="text-right"><strong>Main Office:</strong></td>
                                                <td><a href="#"><?php echo $ParentName;?></a></td>
                                              </tr>
                                              <tr>
                                                <td class="text-right"><strong>Contact Number:</strong></td>
                                                <td><?php echo $getClientInfo->contact_number;?></td>
                                                <td class="text-right"></td>
                                                <td></td>
                                              </tr>
                                              <tr>
                                                <td class="text-right"><strong>Account Manager:</strong></td>
                                                <td><a href="#"><?php echo $ParentName;?></a></td>
                                                <td class="text-right"><strong>Website:</strong></td>
                                                <td><a href="#"><?php echo $getClientInfo->website;?></a></td>
                                              </tr>
                                              <tr>
                                                <td class="text-right"><strong>Industry:</strong></td>
                                                <td><?php echo $getClientInfo->industry_name;?></td>
                                                <td class="text-right"></td>
                                                <td></td>
                                              </tr>
                                              <tr>
                                                <td class="text-right"><strong>About:</strong></td>
                                                <td><?php echo $getClientInfo->about;?></td>
                                                <td class="text-right"></td>
                                                <td></td>
                                              </tr>
                                              <tr>
                                                <td class="text-right"><strong>Source:</strong></td>
                                                <td><?php echo $getClientInfo->source_name;?></td>
                                                <td class="text-right"></td>
                                                <td></td>
                                              </tr>
                                              <!-- <tr>
                                                <td class="text-right"><strong>Created By:</strong></td>
                                                <td><a href="#">Archit Srivastava</a> <br>Sat, 4 Mar 2017 02:56 PM</td>
                                                <td class="text-right"></td>
                                                <td></td>
                                              </tr> -->
                                            </tbody>
                                          </table>
                                    </div>

                                    <div class="col-md-12">
                                        <br>
                                        <h4>Address Information</h4>
                                        <hr>
                                    </div>
                                    <div class="col-md-12">
                                        <table class="table table-bordered clientinfo">
                                            <tbody>
                                              <tr>
                                                <td class="text-right"><strong>Billing Street:</strong></td>
                                                <td><?php echo $getClientInfo->billing_street;?></td>
                                                <td class="text-right"><strong>Shipping Street:</strong></td>
                                                <td><?php echo $getClientInfo->shipping_street;?></td>
                                              </tr>
                                              <tr>
                                                <td class="text-right"><strong>Billing City:</strong></td>
                                                <td><?php echo $getClientInfo->billing_city;?></td>
                                                <td class="text-right"><strong>Shipping City:</strong></td>
                                                <td><?php echo $getClientInfo->shipping_city;?></td>
                                              </tr>
                                              <tr>
                                                <td class="text-right"><strong>Billing State:</strong></td>
                                                <td><?php echo $getClientInfo->billing_state;?></td>
                                                <td class="text-right"><strong>Shipping State:</strong></td>
                                                <td><?php echo $getClientInfo->shipping_state;?></td>
                                              </tr>
                                              <tr>
                                                <td class="text-right"><strong>Billing Code:</strong></td>
                                                <td><?php echo $getClientInfo->billing_code;?></td>
                                                <td class="text-right"><strong>Shipping Code:</strong></td>
                                                <td><?php echo $getClientInfo->shipping_code;?></td>
                                              </tr>
                                              <tr>
                                                <td class="text-right"><strong>Billing Country:</strong></td>
                                                <td><?php echo $getClientInfo->billing_country;?></td>
                                                <td class="text-right"><strong>Shipping Country:</strong></td>
                                                <td><?php echo $getClientInfo->shipping_country;?></td>
                                              </tr>
                                            </tbody>
                                          </table>
                                    </div>

                                    <div class="col-md-12">
                                        <br>
                                        <h4 data-toggle="collapse" data-target="#assactivities" class="collapsibleh4">Activities <i class="fa fa-angle-down"></i></h4>
                                        <hr>
                                    </div>
                                    <div class="col-md-12">
                                        <div id="assactivities" class="collapse in">
                                            <table class="table">
                                                <tbody>
                                                  <!-- <tr>
                                                    <td><a href="#">Archit Srivastava</a> updated the details of client(s) <a href="#">jainy</a></td>
                                                    <td class="text-right">Yesterday - 02:59 PM</td>
                                                  </tr> -->
                                                  <tr>
                                                    <td><a href="#"><?php echo $ParentName;?></a> added client <a href="#"><?php echo $getClientInfo->subclient_name;?></a></td>
                                                    <!-- <td class="text-right">03/04/2017 - 02:56 PM</td> -->
                                                  </tr>
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>

                                    <div class="col-md-12">
                                        <br>
                                        <h4 data-toggle="collapse" data-target="#assjobopnings" class="collapsibleh4">Associated Job Openings <i class="fa fa-angle-down"></i></h4>
                                        <hr>
                                    </div>
                                    <div class="col-md-12">
                                        <div id="assjobopnings" class="collapse in">
                                                <div class="adv-table">
                                            <table  class="display table table-bordered table-striped" id="dynamic-table">
                                            <thead>
                                            <tr>
                                               <!--  <th><input type="checkbox"></th> -->
                                                <th>Job Opening ID</th>
                                                <th>Posting Title</th>
                                                <th>Open Date</th>
                                                <th>Job Opening Status</th>
                                                <!-- <th>Client Name</th> -->
                                                <th>Assigned Recruiter</th>
                                                <!-- <th>Action</th> -->
                                            </tr>
                                            </thead>
                                            <tbody>
                                                <?php
                                                $count=1;
                                              foreach($getJobs as $key){
                                            //echo date('jS M Y',strtotime($key->open_date));exit;
                                                $deactivate=$key->deactivate;

                                                ?>
                                            <tr class="gradeX">
                                                <td><?php echo $count++;?></td>
                                                <td><?php echo $key->job_title;?></td>
                                                <td><?php echo date('jS M Y',strtotime($key->open_date));?></td>
                                                <td><?php echo $key->status_name;?></td>
                                                <!-- <td><?php //echo $key->parent_client;?></td> -->
                                                <td><?php echo $key->recruiter;?></td>
                                                
                                            </tr>
                                          <?php }?>
                                            </tbody>
                                            </table>
                                            </div>
                                        </div>
                                    </div>

                                   <!--  <div class="col-md-12">
                                        <br>
                                        <h4 data-toggle="collapse" data-target="#asscontacts" class="collapsibleh4">Associated Contacts <i class="fa fa-angle-down"></i></h4>
                                        <hr>
                                    </div> -->
                                    <div class="col-md-12">
                                        <div id="asscontacts" class="collapse in">
                                            
                                        </div>
                                    </div>

                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <!-- <div class="col-sm-12">
                                                <hr>
                                                <button type="submit" class="btn btn-info pull-right btn-sm"><strong><i class="fa fa-edit"></i> Edit</strong></button><span class="pull-right"> &nbsp; &nbsp; </span>

                                                <button type="button" class="btn btn-danger pull-right btn-sm"><strong><i class="fa fa-times"></i> Delete</strong></button>
                                            </div> -->
                                        </div>
                                    </div>
                                    </form>
                                </div>
                            </div>
                        </section>
                    </div>
                </div>
                <!-- page end-->

            </section>
        </section>
        <!--main content end-->

</div>


