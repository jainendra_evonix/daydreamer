<?php // include 'header.php' ?>

    <style type="text/css">
        .paymentmessage {
            margin: 100px 0;
        }
        .paymentmessage h1 {
            font-size: 64px;
            color: #333;
            font-weight: 700;
            letter-spacing: 2px;
        }
        .paymentmessage .fa-exclamation-triangle {
            font-size: 100px;
            color: #f00;
            margin: 5px 0 10px;
        }
        .paymentmessage a p {
            color: #428bca;
            text-decoration: underline;
        }
        .paymentmessage p {
            font-size: 14px;
            color: #333;
        }
    </style>


    <!--main content start-->
    <section id="" class="container">
        <section class="wrapper">
            <!-- page start-->
            <div class="row">
                <div class="col-md-8 col-md-offset-2">
                    <br>
                    <section class="panel paymentmessage">
                        <div class="panel-body text-center">
                            <h1>ERROR!</h1>
                            <i class="fa fa-exclamation-triangle"></i>
                            <p>Your payment unsuccessful</p>
                            <a href="<?php echo base_url() ?>client/viewPackages"><p>Click here and do payment agian</p></a>
                           <!--  <p>Duis aute irure dolor in reprehenderit in voluptate velit.</p> -->
                            <br>
                        </div>
                    </section>
                </div>
            </div>
            <!-- page end-->

        </section>
    </section>
    <!--main content end-->

</div>

<?php // include 'footer.php' ?>