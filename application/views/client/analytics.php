<?php
 //echo "<pre>"; print_r($counts);

//exit;
$temp=array();
if(!empty($month_array) && !empty($client_array)){

	foreach ($month_array as $key => $value) {
		if(!empty($client_array[$key])){
			$temp[]=array(
				'label'=>$value,
				'y'=>(int)$client_array[$key]->clientid

				);

		}else{
			$temp[]=array(
				'label'=>$value,
				'y'=>(int)''

				);

		}
	}

}
$temp=json_encode($temp);

$temp1=array();
if(!empty($month_array) && !empty($job_array)){
	foreach ($month_array as $key => $value) {
		if(!empty($job_array[$key])){
			$temp1[]=array(
				'label'=>$value,
				'y'=>(int)$job_array[$key]->jobid

				);

		}else{

			$temp1[]=array(
				'label'=>$value,
				'y'=>(int)''

				);
		}
	}

}

$temp1=json_encode($temp1);
?>
<script type="text/javascript">
window.onload = function () {

	
	var dps = <?php echo $temp ?>;
 //console.log(dps);
 var chart = new CanvasJS.Chart("chartContainer", {

    theme: "theme2",//theme1
    title:{
    	text: "Number of Sub Offices"              
    },
    animationEnabled: true,   // change to true

    data: [              
    {
      // Change type to "bar", "area", "spline", "pie",etc.
       showInLegend: true, 
      legendText: "SubOffices",
      type: "column",
      dataPoints: dps
  }
  ]

});
 chart.render();



 var dps1 = <?php echo $temp1 ?>;
 //console.log(dps);
 var chart1 = new CanvasJS.Chart("chartContainer1", {

    theme: "theme2",//theme1
    title:{
    	text: "Number of Jobs Created"              
    },
    animationEnabled: true,   // change to true
    data: [              
    {
      // Change type to "bar", "area", "spline", "pie",etc.
      type: "column",
       showInLegend: true, 
      legendText: "Jobs",
      dataPoints: dps1
  }
  ]

});
 chart1.render();



// 	var chart = new CanvasJS.Chart("chartContainer", {
//     theme: "theme2",//theme1
//     title:{
//     	text: "Number of Jobs"              
//     },
//     animationEnabled: false,   // change to true
//     data: [              
//     {
//       // Change type to "bar", "area", "spline", "pie",etc.
//       type: "column",
//       dataPoints: [
//       { label: "apple",  y: 10  },
//       { label: "orange", y: 15  },
//       { label: "banana", y: 25  },
//       { label: "mango",  y: 30  },
//       { label: "grape",  y: 28  }
//       ]
//   }
//   ]
// });
// 	chart.render();

}

</script>
<div class="container" id="adminsection">
	<h4 class="form-heading">Welcome, <?php echo $firstname.' '.$lastname?></h4></br>
      <?php if($this->session->flashdata('successmsg')){ ?>
              <div class="alert alert-success alert-dismissable">
    <a href="#" class="close" data-dismiss="alert" aria-label="close">×</a>
    <strong>Note!</strong>  <?php echo $this->session->flashdata('successmsg'); ?> 
  </div>
<?php } elseif ($this->session->flashdata('errormsg')) { ?>
  
         <div class="alert alert-warning alert-dismissable">
    <a href="#" class="close" data-dismiss="alert" aria-label="close">×</a>
    <strong>Warning!</strong><?php echo $this->session->flashdata('errormsg');?>
  </div>

<?php   } ?>

  <div class="row">

                 <?php 
                    $client_data=$this->session->userdata(base_url().'client_login_session');
                    $role=$client_data['role'];

                    if($role=='mainclient'){
                    ?>
                    <div class="col-md-3">

                       <section class="panel panelhover" style="box-shadow:4px 4px 15px rgba(136, 136, 136, 0.2);">
                    <div class="panel-body">
                        <span class="mini-stat-icon orange"><i class="fa fa-group"></i></span>
                        <div class="clearfix"></div>
                        <div class="top-stats-panel">
                            <div class="gauge-canvas">
                                <h4 class="widget-h"><a href="<?php echo base_url();?>client/viewAllClients">Total Sub Offices</a></h4>
                                <!-- <p>You can create new job opening by clicking the create icon.</p> -->
                            </div>
                            <ul class="gauge-meta clearfix">
                                <li><strong class="text-xl" style="font-size:25px;padding-left: 100px;"><?php echo $totalSubClients;?></strong></li>
                            </ul>
                        </div>
                    </div>
                </section>
               </div><!--end .col -->
               <?php }else {?>
                <div class="col-md-3" style="display:none;">

                       <section class="panel panelhover" style="box-shadow:4px 4px 15px rgba(136, 136, 136, 0.2);">
                    <div class="panel-body">
                        <span class="mini-stat-icon orange"><i class="fa fa-group"></i></span>
                        <div class="clearfix"></div>
                        <div class="top-stats-panel">
                            <div class="gauge-canvas">
                                <h4 class="widget-h"><a href="<?php echo base_url();?>client/viewAllClients">Total Sub Offices</a></h4>
                                <!-- <p>You can create new job opening by clicking the create icon.</p> -->
                            </div>
                            <ul class="gauge-meta clearfix">
                                <li><strong class="text-xl" style="font-size:25px;padding-left: 100px;"><?php echo $totalSubClients;?></strong></li>
                            </ul>
                        </div>
                    </div>
                </section>
               </div><!--end .col -->
               <?php }?>



               <div class="col-md-3">

                       <section class="panel panelhover" style="box-shadow:4px 4px 15px rgba(136, 136, 136, 0.2);">
                    <div class="panel-body">
                        <span class="mini-stat-icon orange"><i class="fa fa-briefcase"></i></span>
                        <div class="clearfix"></div>
                        <div class="top-stats-panel">
                            <div class="gauge-canvas">
                            
                                <h4 class="widget-h"><a href="<?php echo base_url();?>client/subscribedPackages">Subscribed Packages</a></h4>

                                <!-- <p>You can create new job opening by clicking the create icon.</p> -->
                            </div>
                            <ul class="gauge-meta clearfix">
                                <li><strong class="text-xl" style="font-size:19px;padding-left: 60px;"><?php if(!empty($counts)) { echo count($counts).' '.'Packages'; } else { echo 'No Packages'; }?></strong></li>
                            </ul>
                        </div>
                    </div>
                </section>
               </div><!--end .col -->

                <div class="col-md-3">

                       <section class="panel panelhover" style="box-shadow:4px 4px 15px rgba(136, 136, 136, 0.2);">
                    <div class="panel-body">
                        <span class="mini-stat-icon orange"><i class="fa fa-briefcase"></i></span>
                        <div class="clearfix"></div>
                        <div class="top-stats-panel">
                            <div class="gauge-canvas">
                                <h4 class="widget-h"><a href="<?php echo base_url();?>client/viewJobs">Total Jobs</a></h4>
                                <!-- <p>You can create new job opening by clicking the create icon.</p> -->
                            </div>
                            <ul class="gauge-meta clearfix">
                                <li><strong class="text-xl" style="font-size:25px;padding-left: 100px;"><?php echo $totalJobs;?></strong></li>
                            </ul>
                        </div>
                    </div>
                </section>
               </div>
                     
                     <div class="col-md-3">

                       <section class="panel panelhover" style="box-shadow:4px 4px 15px rgba(136, 136, 136, 0.2);">
                    <div class="panel-body">
                        <span class="mini-stat-icon orange"><i class="fa fa-briefcase"></i></span>
                        <div class="clearfix"></div>
                        <div class="top-stats-panel">
                            <div class="gauge-canvas">
                                <h4 class="widget-h"><a href="<?php echo base_url();?>client/publishedJobs">Published Jobs</a></h4>
                                <!-- <p>You can create new job opening by clicking the create icon.</p> -->
                            </div>
                            <ul class="gauge-meta clearfix">
                                <li><strong class="text-xl" style="font-size:25px;padding-left: 100px;"><?php echo count($publishedJobs);?></strong></li>
                            </ul>
                        </div>
                    </div>
                </section>
               </div>

               <!--end .col -->

               <div class="clearfix"></div>

               <div class="col-md-3">

                       <section class="panel panelhover" style="box-shadow:4px 4px 15px rgba(136, 136, 136, 0.2);">
                    <div class="panel-body">
                        <span class="mini-stat-icon orange"><i class="fa fa-briefcase"></i></span>
                        <div class="clearfix"></div>
                        <div class="top-stats-panel">
                            <div class="gauge-canvas">
                                <h4 class="widget-h"><a href="javascript:void(0);">Remaining Job Ads</a></h4>
                                <!-- <p>You can create new job opening by clicking the create icon.</p> -->
                            </div>
                            
                            <ul class="gauge-meta clearfix">
                                <li><strong class="text-xl" style="font-size:25px;padding-left: 50px;"><?php echo $remainingJobs;echo '  Out of '.$totalAllowedJobs; ?></strong></li>
                            </ul>
                        </div>
                    </div>
                </section>
               </div>


               <!-- <div class="clearfix"></div> -->

               <div class="col-md-3">

                       <section class="panel panelhover" style="box-shadow:4px 4px 15px rgba(136, 136, 136, 0.2);">
                    <div class="panel-body">
                        <span class="mini-stat-icon orange"><i class="fa fa-file"></i></span>
                        <div class="clearfix"></div>
                        <div class="top-stats-panel">
                            <div class="gauge-canvas">
                                <h4 class="widget-h"><a href="<?php echo base_url(); ?>client/jobApplications">Applications</a></h4>
                                <!-- <p>You can create new job opening by clicking the create icon.</p> -->
                            </div>
                            <ul class="gauge-meta clearfix">
                                <li><strong class="text-xl" style="font-size:25px;padding-left: 100px;"><?php $k=0;foreach ($applications as $key  ) {
                                         $k+=($key->counts);
                                         //echo sum($k);
                                 }echo $k;?></strong></li>
                            </ul>
                        </div>
                    </div>
                </section>
               </div>

               <!--end .col -->

  </div>

   <?php
    if($role == 'mainclient')
     {

   ?>

	<div class="" style="margin-bottom:51px;">
		
		<form method="post" id="eventForm" method="post" action="<?php echo base_url();?>client/viewDashboard">
			<div class="panel panel-default">
				<div class="panel-body row">
					<div class="col-md-3 col-md-offset-2 col-sm-6">
						<label>From:</label>
						<div class="input-group date" id="date_of_birth1">
							<div class="input-group-content">

								<input type="text" datepicker="" id="start_date" class="form-control" name="start_date"  autocomplete="off" />

							</div>
							<a href="javascript:void(0);" class="input-group-addon date"><i class="fa fa-calendar"></i></a>
						</div> 
					</div>

					<div class="col-md-3 col-sm-6">
						<label>To:</label>
						<div class="input-group date" id="date_of_birth2">
							<div class="input-group-content">

								<input type="text" datepicker="" id="end_date" class="form-control" name="end_date"  autocomplete="off"/>

							</div>
							<a href="javascript:void(0);" class="input-group-addon date"><i class="fa fa-calendar"></i></a>
						</div>
					</div>

					<div class="col-md-3 col-sm-6">
						<input type="submit" name="name" value="submit" class="btn btn-success lgnsnup-btn" style="margin-left: 27px;
						margin-top: 22px;">
					</div>

				</div>
			</div>
		</form>
	</div>
	<div class="row">
		<div class="col-md-6" >
			<div class="panel panel-default">
				<div class="panel-body">
					<div id="chartContainer" style="height: 300px; width: 100%;"></div>
				</div>
			</div>
			<br>
		</div>

		<div class="col-md-6">
			<div class="panel panel-default">
				<div class="panel-body">
					<div id="chartContainer1" style="height: 300px; width: 100%;"></div>
				</div>
			</div>
			<br>
		</div>
	</div>
  <?php }else {?>
   <div class="" style="margin-bottom:51px;" style="display:none;">
    
    <form method="post" id="eventForm" method="post" action="<?php echo base_url();?>client/viewDashboard">
      <div class="panel panel-default" style="display:none;">
        <div class="panel-body row" style="display:none;">
          <div class="col-md-3 col-md-offset-2 col-sm-6">
            <label>From:</label>
            <div class="input-group date" id="date_of_birth1">
              <div class="input-group-content">

                <input type="text" datepicker="" id="start_date" class="form-control" name="start_date"  autocomplete="off" />

              </div>
              <a href="javascript:void(0);" class="input-group-addon date"><i class="fa fa-calendar"></i></a>
            </div> 
          </div>

          <div class="col-md-3 col-sm-6">
            <label>To:</label>
            <div class="input-group date" id="date_of_birth2">
              <div class="input-group-content">

                <input type="text" datepicker="" id="end_date" class="form-control" name="end_date"  autocomplete="off"/>

              </div>
              <a href="javascript:void(0);" class="input-group-addon date"><i class="fa fa-calendar"></i></a>
            </div>
          </div>

          <div class="col-md-3 col-sm-6">
            <input type="submit" name="name" value="submit" class="btn btn-success lgnsnup-btn" style="margin-left: 27px;
            margin-top: 22px;">
          </div>

        </div>
      </div>
    </form>
  </div>
  <div class="row" style="display:none;">
    <div class="col-md-6" >
      <div class="panel panel-default">
        <div class="panel-body">
          <div id="chartContainer" style="height: 300px; width: 100%;"></div>
        </div>
      </div>
      <br>
    </div>

    <div class="col-md-6">
      <div class="panel panel-default">
        <div class="panel-body">
          <div id="chartContainer1" style="height: 300px; width: 100%;"></div>
        </div>
      </div>
      <br>
    </div>
  </div>
  <?php } ?>


	<div class="row">
		<div class="col-md-6">



		</div>


	</div>


</div>