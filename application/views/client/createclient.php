
<style type="text/css">
.help-block{

    color:red;
}
.autocomplete-suggestions {
    background-color: #fff;
    border: 1px solid #ddd;
    box-shadow: 0 3px 10px rgba(0, 0, 0, 0.2);
}
.autocomplete-suggestion {
    padding: 5px 12px;
}
.autocomplete-suggestion:hover {
    background-color: #333;
    color: #fff;
}
</style>


<!--main content start-->
<section id="adminsection" class="container">
    <section class="wrapper">
        <!-- page start-->
        <div class="row">
            <div class="col-md-12">
                
                <h4 class="form-heading"><strong>Create Office</strong></h4>
                <!--  <p>Create and showcase all Job Openings. <a href="#" class="pull-right">Help <i class="fa fa-question-circle"></i></a></p> -->

                <br>
                <section class="panel">
                    <div class="panel-body">
                        <div class="row">
                            <div class="col-md-12">
                                <h4>Office Information</h4>
                                <hr>
                            </div>
                            <form class="form-horizontal bucket-form" ng-submit="submitsubclientForm()" name="subClientForm" novalidate>
                             <input type="hidden" name="subclient_id" ng-model="subClient.subclient_id">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label class="col-sm-3 col-sm-offset-1 control-label">*Sub Office Name:</label>
                                        <div class="col-sm-7">
                                            <input type="text" name="subclient_name" ng-model="subClient.subclient_name" class="form-control" required>
                                            <span ng-show="submitted && subClientForm.subclient_name.$error.required"  class="help-block has-error ng-hide">Client Name is required.</span>
                                            <span ng-show="errorClientName" class="help-block has-error ng-hide">{{errorClientName}}</span>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-sm-3 col-sm-offset-1 control-label">Contact Number:</label>
                                        <div class="col-sm-7">
                                            <input type="text"  class="form-control" name="contact_number" ng-model="subClient.contact_number" ng-pattern="/^\+?\d{2}[- ]?\d{3}[- ]?\d{5}$/" required>
                                            <span ng-show="submitted && subClientForm.contact_number.$error.required"  class="help-block has-error ng-hide">Contact Number is required.</span>
                                            <span ng-show="submitted && subClientForm.contact_number.$error.pattern"  class="help-block has-error ng-hide">Valid Contact Number is required.</span>
                                            <span ng-show="errorContactNumber" class="help-block has-error ng-hide">{{errorContactNumber}}</span>
                                        </div>
                                    </div>
                                    <!-- <div class="form-group">
                                        <label class="col-sm-3 col-sm-offset-1 control-label">Account Manager:</label>
                                        <div class="col-sm-7">
                                            <input type="text"  class="form-control" name="manager" ng-model="subClient.manager" required>
                                            <span ng-show="submitted && subClientForm.manager.$error.required"  class="help-block has-error ng-hide">Manager is required.</span>
                                            <span ng-show="errorManager" class="help-block has-error ng-hide">{{errorManager}}</span>
                                        </div>
                                    </div> -->
                                    <!-- <div class="form-group">
                                        <label class="control-label col-sm-3 col-sm-offset-1">Target Date:</label>
                                        <div class="col-sm-7">
                                            <input class="form-control form-control-inline input-medium birth_date" name="target_date" ng-model="job.target_date" size="16" type="text" value="" placeholder="Please enter target date" / required>
                                            <span ng-show="submitted && jobForm.target_date.$error.required"  class="help-block has-error ng-hide">Target Date is required.</span>
                                        </div>
                                    </div> -->
                                  

                                    <div class="form-group">
                                        <label class="col-sm-3 col-sm-offset-1 control-label">Industry:</label>
                                        <div class="col-sm-7">
                                            <select class="form-control" name="industry" ng-model="subClient.industry" required>
                                                <option value="">-None-</option>
                                                <?php
                                                  foreach($allIndustries as $key){
                                                ?>
                                                <option value="<?php echo $key->ind_id?>"><?php echo $key->industry_name;?></option>
                                                <?php }?>
                                            </select>
                                            <span ng-show="submitted && subClientForm.industry.$error.required"  class="help-block has-error ng-hide">Industry is required.</span>
                                           <span ng-show="errorIndustry" class="help-block has-error ng-hide">{{errorIndustry}}</span>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-sm-3 col-sm-offset-1 control-label">About:</label>
                                        <div class="col-sm-7">
                                           <textarea class="form-control" ng-model="subClient.about" name="about"  ng-pattern="/.*[a-zA-Z]+.*/" rows="2" required></textarea>
                                            <span ng-show="submitted && subClientForm.about.$error.required"  class="help-block has-error ng-hide">About is required.</span>  
                                           <span ng-show="errorAbout" class="help-block has-error ng-hide">{{errorAbout}}</span>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-sm-3 col-sm-offset-1 control-label">Source:</label>
                                        <div class="col-sm-7">
                                            <!-- <input type="text"  class="form-control" placeholder="Enter your nationality"> -->
                                            <select class="form-control" name="source" ng-model="subClient.source" required>
                                                <option value="">-None-</option>
                                                <?php

                                                 foreach($allSources as $key){


                                                ?>
                                                <option value="<?php echo $key->s_id;?>"><?php echo $key->source_name;?></option>
                                                <?php }?>
                                            </select>
                                            <span ng-show="submitted && subClientForm.source.$error.required"  class="help-block has-error ng-hide">Sources is required.</span>  
                                           <span ng-show="errorSource" class="help-block has-error ng-hide">{{errorSource}}</span>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label class="col-sm-3 col-sm-offset-1 control-label">Office Name:</label>
                                        <div class="col-sm-7">
                                            
                                            <input type="text"  class="form-control" name="parent_client" ng-model="subClient.parent_client" readonly="true"  required>
                                            
                                            <span ng-show="submitted && subClientForm.parent_client.$error.required"  class="help-block has-error ng-hide">Office is required.</span>
                                          <span ng-show="errorClientName" class="help-block has-error ng-hide">{{errorClientName}}</span>
                                         </div>
                                    </div>
                                    <!-- <div class="form-group">
                                        <label class="col-sm-3 col-sm-offset-1 control-label">Fax:</label>
                                        <div class="col-sm-7">
                                            <input type="text"  class="form-control" name="manager" ng-model="job.manager" required>
                                            <span ng-show="submitted && jobForm.manager.$error.required"  class="help-block has-error ng-hide">Account Manager is required.</span>
                                         <span ng-show="errorManager" class="help-block has-error ng-hide">{{errorManager}}</span>
                                        </div>
                                    </div> -->
                                    <div class="form-group">
                                        <label class="col-sm-3 col-sm-offset-1 control-label">Website:</label>
                                        <div class="col-sm-7">
                                            <input type="url"  class="form-control" name="website" ng-model="subClient.website" required>
                                            <span ng-show="submitted && subClientForm.website.$error.required"  class="help-block has-error ng-hide">Website is required.</span>
                                             <span ng-show="submitted && subClientForm.website.$error.url"  class="help-block has-error ng-hide">Valid URL is required.</span>
                                       <span ng-show="errorWebsite" class="help-block has-error ng-hide">{{errorWebsite}}</span>
                                        </div>
                                    </div>
                                    <!-- <div class="form-group">
                                        <label class="control-label col-sm-3 col-sm-offset-1">Date Opened:</label>
                                        <div class="col-sm-7">
                                            <input class="form-control form-control-inline input-medium birth_date" name="open_date" ng-model="job.open_date"  size="16" type="text" value="" placeholder="Please enter Opened Date" / required>
                                            <span ng-show="submitted && jobForm.open_date.$error.required"  class="help-block has-error ng-hide">Date Opened is required.</span>
                                            <!-- <span class="help-block">Select date</span> -->
                                        <!--</div>
                                    </div> -->
                                      
                                    <!-- <div class="form-group">
                                        <label class="col-sm-3 col-sm-offset-1 control-label">Job Type:</label>
                                        <div class="col-sm-7">
                                            <!-- <input type="text"  class="form-control" placeholder="Enter your nationality"> -->
                                          <!--   <select class="form-control" name="job_type" ng-model="job.job_type" required>
                                                <option value="Full time" selected="true">Full time</option>
                                                <option value="Part time">Part time</option>
                                                <option value="Temporary">Temporary</option>
                                                <option value="Contract">Contract</option>
                                                <option value="Temporary to permanent">Temporary to permanent</option>
                                            </select>
                                            <span ng-show="submitted && jobForm.job_type.$error.required"  class="help-block has-error ng-hide">Job Type is required.</span>
                                       <span ng-show="errorJobType" class="help-block has-error ng-hide">{{errorJobType}}</span>
                                        </div>
                                    </div> -->
                                   <!--  <div class="form-group">
                                        <label class="col-sm-3 col-sm-offset-1 control-label">City:</label>
                                        <div class="col-sm-7">
                                            <input type="text"  class="form-control" name="city" ng-model="job.city" required>
                                            <span ng-show="submitted && jobForm.city.$error.required"  class="help-block has-error ng-hide">City is required.</span>
                                        <span ng-show="errorCity" class="help-block has-error ng-hide">{{errorCity}}</span>

                                        </div>
                                    </div> -->
                                    <!-- <div class="form-group">
                                        <label class="col-sm-3 col-sm-offset-1 control-label">Country:</label>
                                        <div class="col-sm-7">
                                            <!-- <input type="text"  class="form-control" placeholder="Enter your nationality"> -->
                                           <!--  <select class="form-control" name="nationality" ng-model="job.nationality" required>
                                                <option disabled="disabled" selected="selected">Select your nationality</option>

                                                <option>Option 1</option>
                                                
                                            </select>
                                            <span ng-show="submitted && jobForm.nationality.$error.required"  class="help-block has-error ng-hide">Country is required.</span>
                                        <span ng-show="errorNationality" class="help-block has-error ng-hide">{{errorNationality}}</span>
                                        </div>
                                    </div> --> 
                                    <!-- <div class="form-group">
                                        <label class="col-sm-3 col-sm-offset-1 control-label">Salary:</label>
                                        <div class="col-sm-7">
                                            <input type="text"  class="form-control" name="salary" ng-model="job.salary" required>
                                            <span ng-show="submitted && jobForm.salary.$error.required"  class="help-block has-error ng-hide">Salary is required.</span>
                                        <span ng-show="errorSalary" class="help-block has-error ng-hide">{{errorSalary}}</span>
                                        </div>
                                    </div> -->
                                </div>
                                <div class="col-md-12">
                                    <br>
                                    <h4>Address Information</h4>
                                    <hr>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label class="col-sm-2 control-label col-sm-2">Billing Street:</label>
                                        <div class="col-sm-7">
                                            <input type="text"  class="form-control" name="billing_street" ng-model="subClient.billing_street" ng-pattern="/^[a-zA-Z ]*$/"  required>
                                            <span ng-show="submitted && subClientForm.billing_street.$error.required"  class="help-block has-error ng-hide">Billing Street is required.</span>
                                            <span ng-show="submitted && subClientForm.billing_street.$error.pattern"  class="help-block has-error ng-hide">Valid Billing Street is required.</span>
                                         <span ng-show="errorBillingStreet" class="help-block has-error ng-hide">{{errorBillingStreet}}</span>

                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label class="col-sm-2 control-label col-sm-2">Billing City:</label>
                                        <div class="col-sm-7">
                                            <input type="text"  class="form-control" name="billing_city" ng-model="subClient.billing_city" ng-pattern="/^[a-zA-Z ]*$/" required>
                                            <span ng-show="submitted && subClientForm.billing_city.$error.required"  class="help-block has-error ng-hide">Billing City is required.</span>
                                            <span ng-show="submitted && subClientForm.billing_city.$error.pattern"  class="help-block has-error ng-hide">Valid Billing City is required.</span>
                                         <span ng-show="errorBillingCity" class="help-block has-error ng-hide">{{errorBillingCity}}</span>

                                        </div>
                                    </div>


                                    <div class="form-group">
                                        <label class="col-sm-2 control-label col-sm-2">Billing State:</label>
                                        <div class="col-sm-7">
                                            <input type="text"  class="form-control" name="billing_state" ng-model="subClient.billing_state" ng-pattern="/^[a-zA-Z ]*$/" required>
                                            <span ng-show="submitted && subClientForm.billing_state.$error.required"  class="help-block has-error ng-hide">Billing State is required.</span>
                                            <span ng-show="submitted && subClientForm.billing_state.$error.pattern"  class="help-block has-error ng-hide">Valid Billing State is required.</span>
                                         <span ng-show="errorBillingState" class="help-block has-error ng-hide">{{errorBillingState}}</span>

                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label class="col-sm-2 control-label col-sm-2">Billing Code:</label>
                                        <div class="col-sm-7">
                                            <input type="text"  class="form-control" name="billing_code" ng-model="subClient.billing_code" ng-pattern="/^[0-9A-Za-z ]{4,10}$/" required>
                                            <span ng-show="submitted && subClientForm.billing_code.$error.required"  class="help-block has-error ng-hide">Billing Code is required.</span>
                                            <span ng-show="submitted && subClientForm.billing_code.$error.pattern"  class="help-block has-error ng-hide">Valid Billing Code is required.</span>
                                         <span ng-show="errorBillingCode" class="help-block has-error ng-hide">{{errorBillingCode}}</span>

                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label class="col-sm-2 control-label col-sm-2">Billing Country:</label>
                                        <div class="col-sm-7">
                                            <input type="text"  class="form-control" name="billing_country" ng-model="subClient.billing_country" ng-pattern="/^[a-zA-Z ]*$/" required>
                                            <span ng-show="submitted && subClientForm.billing_country.$error.required"  class="help-block has-error ng-hide">Billing Country is required.</span>
                                            <span ng-show="submitted && subClientForm.billing_country.$error.pattern"  class="help-block has-error ng-hide">Valid Billing Country is required.</span>
                                         <span ng-show="errorBillingCountry" class="help-block has-error ng-hide">{{errorBillingCountry}}</span>

                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                 <div class="form-group">
                                        <label class="col-sm-2 control-label col-sm-2">Shipping Street:</label>
                                        <div class="col-sm-7">
                                            <input type="text"  class="form-control" name="shipping_street" ng-model="subClient.shipping_street" ng-pattern="/^[a-zA-Z ]*$/" required>
                                            <span ng-show="submitted && subClientForm.shipping_street.$error.required"  class="help-block has-error ng-hide">Shipping Street is required.</span>
                                            <span ng-show="submitted && subClientForm.shipping_street.$error.pattern"  class="help-block has-error ng-hide">Shipping Street is required.</span>
                                         <span ng-show="errorShippingStreet" class="help-block has-error ng-hide">{{errorShippingStreet}}</span>

                                        </div>
                                    </div>

                                     <div class="form-group">
                                        <label class="col-sm-2 control-label col-sm-2">Shipping City:</label>
                                        <div class="col-sm-7">
                                            <input type="text"  class="form-control" name="shipping_city" ng-model="subClient.shipping_city" ng-pattern="/^[a-zA-Z ]*$/" required>
                                            <span ng-show="submitted && subClientForm.shipping_city.$error.required"  class="help-block has-error ng-hide">Shipping City is required.</span>
                                            <span ng-show="submitted && subClientForm.shipping_city.$error.pattern"  class="help-block has-error ng-hide">Valid Shipping City is required.</span>
                                         <span ng-show="errorShippingCity" class="help-block has-error ng-hide">{{errorShippingCity}}</span>

                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label class="col-sm-2 control-label col-sm-2">Shipping State:</label>
                                        <div class="col-sm-7">
                                            <input type="text"  class="form-control" name="shipping_state" ng-model="subClient.shipping_state" ng-pattern="/^[a-zA-Z ]*$/" required>
                                            <span ng-show="submitted && subClientForm.shipping_state.$error.required"  class="help-block has-error ng-hide">Shipping State is required.</span>
                                            <span ng-show="submitted && subClientForm.shipping_state.$error.pattern"  class="help-block has-error ng-hide">Valid Shipping State is required.</span>
                                         <span ng-show="errorShippingState" class="help-block has-error ng-hide">{{errorShippingState}}</span>

                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label class="col-sm-2 control-label col-sm-2">Shipping Code:</label>
                                        <div class="col-sm-7">
                                            <input type="text"  class="form-control" name="shipping_code" ng-model="subClient.shipping_code" ng-pattern="/^[0-9A-Za-z ]{4,10}$/" required>
                                            <span ng-show="submitted && subClientForm.shipping_code.$error.required"  class="help-block has-error ng-hide">Shipping Code is required.</span>
                                            <span ng-show="submitted && subClientForm.shipping_code.$error.pattern"  class="help-block has-error ng-hide">Valid Shipping Code is required.</span>
                                         <span ng-show="errorShippingCode" class="help-block has-error ng-hide">{{errorShippingCode}}</span>

                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label class="col-sm-2 control-label col-sm-2">Shipping Country:</label>
                                        <div class="col-sm-7">
                                            <input type="text"  class="form-control" name="shipping_country" ng-model="subClient.shipping_country" ng-pattern="/^[a-zA-Z ]*$/" required>
                                            <span ng-show="submitted && subClientForm.shipping_country.$error.required"  class="help-block has-error ng-hide">Shipping Country is required.</span>
                                            <span ng-show="submitted && subClientForm.shipping_country.$error.pattern"  class="help-block has-error ng-hide">Valid Shipping Country is required.</span>
                                         <span ng-show="errorShippingCountry" class="help-block has-error ng-hide">{{errorShippingCountry}}</span>

                                        </div>
                                    </div>

                                </div>
                                <div class="col-md-12">
                                    <br>
                                    <h4>Attachment Information</h4>
                                    <hr>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label class="control-label col-sm-3 col-sm-offset-1 text-right">Attachments:</label>
                                        <div class="controls col-sm-7">
                                           <!-- <input type="file" accept="application/pdf,application/msword,application/vnd.openxmlformats-officedocument.wordprocessingml.document" file-model="uploadFile" nv-file-select="" uploader="uploader2"/>

                                           <button type="submit" ng-click="uploader2.uploadAll()">Upload</button> 
                                           <span id="succ" style="color: green;" ></span> 
                                           <input type="hidden" class="form-control" id="pdf" name="pdf" ng-model="job.pdf" required/>

                                           <span id="uplErr" class="warning" style="color: red;"></span> 
                                           <span ng-show="submitted && jobForm.pdf.$invalid" class="help-block has-error ng-hide warnig">Please upload Job Summary</span> <span class="help-block has-error ng-hide warnig" style="color: red;" ng-show="pdfError">{{pdfError}}</span> -->

                                           <input id="uploadFile" class="default ng-pristine ng-valid ng-empty ng-touched" file-model="myFile" ng-model="subClient.attached_file" name="attached_file" ng-files="getTheFiles($files)" type="file">
                                           <span ng-show="submitted && subClientForm.attached_file.$error.required"  class="help-block has-error ng-hide">Job Summary is required.</span>
                                       <span ng-show="errorFile" class="help-block has-error ng-hide">{{errorFile}}</span>
                                       </div>
                                   </div>
                                        <!-- <div class="form-group">
                                            <label class="control-label col-sm-3 col-sm-offset-1 text-right">Others:</label>
                                            <div class="controls col-sm-7">
                                                <div class="fileupload fileupload-new" data-provides="fileupload">
                                                            <span class="btn btn-white btn-file">
                                                            <span class="fileupload-new"><i class="fa fa-paper-clip"></i> Select file</span>
                                                            <span class="fileupload-exists"><i class="fa fa-undo"></i> Change</span>
                                                            <input type="file" class="default" />
                                                            </span>
                                                    <span class="fileupload-preview" style="margin-left:5px;"></span>
                                                    <a href="#" class="close fileupload-exists" data-dismiss="fileupload" style="float: none; margin-left:5px;"></a>
                                                </div>
                                            </div>
                                        </div> -->
                                    </div>
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <div class="col-sm-12">
                                                <button type="submit" class="btn btn-info pull-right btn-sm" ng-click="submitted = true"><strong><i class="fa fa-save"></i> Save &amp; Publish</strong></button><span class="pull-right"> &nbsp; &nbsp; </span>

                                                <button type="button" class="btn btn-danger pull-right btn-sm"><strong><i class="fa fa-times"></i> Cancel</strong></button>
                                            </div>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </section>
                </div>
            </div>
            <!-- page end-->

        </section>
    </section>
    <!--main content end-->





    
</div>



