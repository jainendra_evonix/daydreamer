
<style type="text/css">
.help-block{

  color:red;
}
.autocomplete-suggestions {
  background-color: #fff;
  border: 1px solid #ddd;
  box-shadow: 0 3px 10px rgba(0, 0, 0, 0.2);
}
.autocomplete-suggestion {
  padding: 5px 12px;
}
.autocomplete-suggestion:hover {
  background-color: #333;
  color: #fff;
}


/*mutiselect style*/

.multiselect {
    width:23em;
    height:15em;
    border:solid 1px #c0c0c0;
    overflow:auto;
}
 
.multiselect label {
    display:block;
}
 
.multiselect-on {
    color:#ffffff;
    background-color:#000099;
}








</style>

<?php //echo "<pre>"; print_r($getJobData); exit;

$requiredSkills =  $getJobData->skills; 
$k = explode(",", $requiredSkills);

     // echo "<pre>"; print_r($k);

   // exit;
?>
<!--main content start-->
<section id="adminsection" class="container">
  <section class="wrapper">
    <!-- page start-->
    <div class="row">
      <div class="col-md-12">

        <?php echo $this->session->flashdata('successmsg');  ?>
        <?php echo $this->session->flashdata('errormsg');  ?>
         <div id="showStatus"></div>

        <h4 class="form-heading"><strong>Job Opening</strong></h4>
        <!--  <p>Create and showcase all Job Openings. <a href="#" class="pull-right">Help <i class="fa fa-question-circle"></i></a></p> -->

        <br>
        <section class="panel">
          <div class="panel-body">
            <div class="row">
              <div class="col-md-12">
                <h4>Job Information</h4>
                <hr>
              </div>
              <form class="form-horizontal bucket-form" ng-submit="submitjobForm()" name="jobForm" id="sales" novalidate>
                <input type="hidden" name="sub_client_id" ng-model="job.sub_client_id">
                <input type="hidden" name="job_id" ng-model="job.job_id">
                <input type="hidden" name="skill_id" ng-model="job.skill_id">
                <input type="hidden" name="published" ng-model="job.published" value="0">
                

                <div class="col-md-6">
                  <div class="form-group">
                    <label class="col-sm-3 col-sm-offset-1 control-label">Posting Title:</label>
                    <div class="col-sm-7">
                      <input type="text" name="job_title" ng-model="job.job_title" class="form-control" ng-pattern="/^[a-zA-Z ]*$/" required>
                      <span ng-show="submitted && jobForm.job_title.$error.required"  class="help-block has-error ng-hide">Job Title is required.</span>
                      <span ng-show="submitted && jobForm.job_title.$error.pattern"  class="help-block has-error ng-hide">Valid Job Title is required.</span>
                      <span ng-show="errorTitle" class="help-block has-error ng-hide">{{errorTitle}}</span>
                    </div>
                  </div>
                  <div class="form-group">
                    <label class="col-sm-3 col-sm-offset-1 control-label">Contact Name:</label>
                    <div class="col-sm-7">
                      <input type="text"  class="form-control" name="contact_name" ng-model="job.contact_name" ng-pattern="/^[a-zA-Z ]*$/" required>
                      <span ng-show="submitted && jobForm.contact_name.$error.required"  class="help-block has-error ng-hide">Contact Name is required.</span>
                      <span ng-show="submitted && jobForm.contact_name.$error.pattern"  class="help-block has-error ng-hide">Contact Name is required.</span>
                      <span ng-show="errorContactName" class="help-block has-error ng-hide">{{errorContactName}}</span>
                    </div>
                  </div>
                  <div class="form-group">
                    <label class="col-sm-3 col-sm-offset-1 control-label">Assigned Recruiter:</label>
                    <div class="col-sm-7">
                      <input type="text"  class="form-control" name="recruiter" ng-model="job.recruiter" ng-pattern="/^[a-zA-Z ]*$/" readonly="true">
                      <span ng-show="submitted && jobForm.recruiter.$error.required"  class="help-block has-error ng-hide">Recruiter is required.</span>
                      <span ng-show="submitted && jobForm.recruiter.$error.pattern"  class="help-block has-error ng-hide">Recruiter is required.</span>
                      <span ng-show="errorRecruiter" class="help-block has-error ng-hide">{{errorRecruiter}}</span>
                    </div>
                  </div>
                                   

                                  <div class="form-group">
                                    <label class="col-sm-3 col-sm-offset-1 control-label">Job Opening Status:</label>
                                    <div class="col-sm-7">
                                      <!-- <input type="text"  class="form-control" placeholder="Enter your nationality"> -->
                                      <select class="form-control" name="job_status" ng-model="job.job_status" required>
                                        <option disabled="disabled" selected="selected" value="">Select your status</option>
                                        <?php

                                        foreach($allStatus as $key)
                                        {

                                          ?>
                                          <option value="<?php echo $key->status_id?>"><?php echo $key->status_name;?></option>
                                          <?php }?>
                                        </select>
                                        <span ng-show="submitted && jobForm.job_status.$error.required"  class="help-block has-error ng-hide">Job Status is required.</span>
                                        <span ng-show="errorStatus" class="help-block has-error ng-hide">{{errorStatus}}</span>  
                                      </div>
                                    </div>





                                  

                                      <div class="form-group">
                                        <label class="col-sm-3 col-sm-offset-1 control-label">Work Experience:</label>
                                        <div class="col-sm-7">
                                          <!-- <input type="text"  class="form-control" placeholder="Enter your nationality"> -->
                                          <select class="form-control" name="experience" ng-model="job.experience" required>
                                            <option value="">-None-</option>
                                            <?php
                                            foreach($allExperience as $key){

                                              ?>
                                              <option value="<?php echo $key->ex_id?>"><?php echo $key->experience;?></option>
                                              <?php }?>
                                            </select>
                                            <span ng-show="submitted && jobForm.experience.$error.required"  class="help-block has-error ng-hide">Experience is required.</span>  
                                            <span ng-show="errorExperience" class="help-block has-error ng-hide">{{errorExperience}}</span>
                                          </div>
                                        </div>

                                        <div class="form-group">
                                         <label class="col-sm-3 col-sm-offset-1 control-label">Qualifications:</label>
                                         <div class="col-sm-7">
                                           <textarea rows="4" cols="33" name="qualifications" ng-model="job.qualifications" style="resize:none;" required></textarea>
                                           <span ng-show="submitted && jobForm.qualifications.$error.required"  class="help-block has-error ng-hide">Qualifications is required.</span>  
                                           <span ng-show="errorQualifications" class="help-block has-error ng-hide">{{errorQualifications}}</span>
                                         </div>
                                       </div>

                                       <div class="form-group">
                                         <label class="col-sm-3 col-sm-offset-1 control-label">Working Hours:</label>
                                         <div class="col-sm-7">
                                          <input type="text" class="form-control" name="working_days" ng-model="job.working_days" required>
                                          <span ng-show="submitted && jobForm.working_days.$error.required"  class="help-block has-error ng-hide">Working Days is required.</span>  
                                          <span ng-show="errorQualifications" class="help-block has-error ng-hide">{{errorQualifications}}</span>
                                        </div>
                                      </div>

                                     



                                   <div class="form-group" id="hours">
                                        <label class="control-label col-md-3 col-sm-offset-1">Hours<span class="req">*</span></label>
                                        <div class="col-md-8" id="hours">
                                          <?php
                                          foreach($allHours as $key){

                                                        //print_r($hours);exit;

                                             ?>
                                             <input type="checkbox" name="<?php echo 'hours'.$key->id;?>" value=<?php echo $key->id;?> <?php echo isset($key->id) && in_array($key->id, $savedHours) ? 'checked="checked"' : '';?> required ><span class="small" style="margin-right: 5px;" ><strong><?php echo $key->hours;?></strong></span>


                                             <?php }?>
                                            

                                         </div>
                                     </div>

                                     <div class="form-group" id="shifts">
                                        <label class="control-label col-md-3 col-sm-offset-1">Shifts<span class="req">*</span></label>
                                        <div class="col-md-8" id="shifts">
                                          <?php
                                          foreach($allShifts as $key){

                                             ?>
                                             <input type="checkbox" name="<?php echo 'shifts'.$key->id;?>" value=<?php echo $key->id;?> <?php echo isset($key->id) && in_array($key->id, $savedShifts) ? 'checked="checked"' : '';?> required><span class="small" style="margin-right: 5px;"><strong><?php echo $key->shifts;?></strong></span>
                                             <?php }?>
                                             

                                         </div>
                                     </div>

                                     <div class="form-group" id="contarctType">
                                        <label class="control-label col-md-3 col-sm-offset-1">Contract Type<span class="req">*</span></label>
                                        <div class="col-md-8" id="contract_type">
                                          <?php
                                          foreach($contract_type as $key){

                                             ?>
                                             <span style="width: 155px; height: 31px; display: inline-block;"><input type="checkbox" name="<?php echo 'contract'.$key->id;?>" value=<?php echo $key->id;?> <?php echo isset($key->id) && in_array($key->id, $savedContract) ? 'checked="checked"' : '';?> required><span class="small" style="margin-right: 5px; margin-bottom: 15px;"><strong><?php echo $key->contract_type;?></strong></span></span>
                                             <?php }?>
                                            
                                            

                                         </div>
                                     </div>
 
                                     







                                    </div>
                                    <div class="col-md-6">
                                     <?php
                                     $logged_in_userdata=$this->session->userdata(base_url().'client_login_session');
                                     $role=$logged_in_userdata['role'];
                                     if($role == 'mainclient'){?>
                                     <div class="form-group">
                                      <label class="col-sm-3 col-sm-offset-1 control-label">Office Name:</label>
                                      <div class="col-sm-7">

                                        <input type="text"  class="form-control"   id="selctionajax" name="office_name" ng-model="job.office_name">
                                        <input type="hidden" name="parent_client" ng-model="job.parent_client" id="organizationname">

                                        <span ng-show="submitted && jobForm.parent_client.$error.required"  class="help-block has-error ng-hide">Office is required.</span>
                                        <span ng-show="errorClientName" class="help-block has-error ng-hide">{{errorClientName}}</span>
                                      </div>
                                    </div>
                                    <?php }else{?>
                                    <div class="form-group" style="display:none;">
                                      <label class="col-sm-3 col-sm-offset-1 control-label">Office Name:</label>
                                      <div class="col-sm-7">

                                        <input type="text"  class="form-control"   id="selctionajax">
                                        <input type="hidden" name="parent_client" ng-model="job.parent_client" id="organizationname">

                                        <span ng-show="submitted && jobForm.parent_client.$error.required"  class="help-block has-error ng-hide">Office is required.</span>
                                        <span ng-show="errorClientName" class="help-block has-error ng-hide">{{errorClientName}}</span>
                                      </div>
                                    </div>
                                    <?php }?>
                                    <div class="form-group">
                                      <label class="col-sm-3 col-sm-offset-1 control-label">Account Manager:</label>
                                      <div class="col-sm-7">
                                        <input type="text"  class="form-control" name="manager" ng-model="job.manager"  readonly="true">
                                        <span ng-show="submitted && jobForm.manager.$error.required"  class="help-block has-error ng-hide">Account Manager is required.</span>
                                        <span ng-show="errorManager" class="help-block has-error ng-hide">{{errorManager}}</span>
                                      </div>
                                    </div>
                                    <div class="form-group">
                                      <label class="col-sm-3 col-sm-offset-1 control-label">Number of Positions:</label>
                                      <div class="col-sm-7">
                                        <input type="text"  class="form-control" name="positions" ng-model="job.positions" ng-pattern="/^[0-9]/" required>
                                        <span ng-show="submitted && jobForm.positions.$error.required"  class="help-block has-error ng-hide">Positions is required.</span>
                                        <span ng-show="submitted && jobForm.positions.$error.pattern"  class="help-block has-error ng-hide">Valid Positions is required.</span>
                                        <span ng-show="errorPositions" class="help-block has-error ng-hide">{{errorPositions}}</span>
                                      </div>
                                    </div>
                             
                                      <div class="form-group">
                                       <label class="col-sm-3 col-sm-offset-1 control-label">Open Date:</label>
                                       <div class="col-sm-7">
                                        <div class="input-group date" id="demo-date" style="width:100%">
                                          <div class="input-group-content">
                                            <input data-calenderopen type="text" class="form-control" autocomplete="off"  ng-model="job.open_date" name="open_date" placeholder="Please enter Opened Date"  id="open_date"   style="margin-bottom:0px;" onchange="hideopen();">
                                            <span id="msg1" ng-show="submitted && jobForm.open_date.$invalid" class="help-block has-error">Open Date is required.</span>
                                           
                                            <span ng-show="errorOpen" class="help-block has-error ng-hide">{{errorOpen}}</span>
                                          </div>
                                        
                                        </div>
                                      </div>
                                    </div>

                                    <div class="form-group">
                                      <label class="col-sm-3 col-sm-offset-1 control-label">Job Type:</label>
                                      <div class="col-sm-7">
                                        
                                       

                                          <div class="multiselect" id='jobType' >
                                             <?php  foreach($allJobType as $key) {  ?>
                                          
                                              <label><input type="checkbox" name="<?php  echo 'option'.$key->id ?>" value="<?php echo $key->id;?>" <?php echo isset($key->id) && in_array($key->id, $savedJobType) ? 'checked="checked"' : '';?> required
                                               ><?php echo $key->job_type;?></label>
                                            

                                              <?php  } ?>
                                          </div>



                                         
                                        </div>
                                      </div>

                                     
                                     <!--  <div class="form-group">
                                        <label class="col-sm-3 col-sm-offset-1 control-label">Country:</label>
                                        <div class="col-sm-7">
                                          
                                          <select class="form-control" name="nationality" ng-model="job.nationality" required>
                                            <option disabled="disabled" selected="selected" value="">Select your nationality</option>
                                            <?php

                                            foreach($allCountries as $key){
                                                 //$id=$key->id;
                                              ?>

                                              <option value="<?php echo $key->id;?>"><?php echo $key->country_name;?></option>
                                              <?php }?>
                                            </select>
                                            <span ng-show="submitted && jobForm.nationality.$error.required"  class="help-block has-error ng-hide">Country is required.</span>
                                            <span ng-show="errorNationality" class="help-block has-error ng-hide">{{errorNationality}}</span>
                                          </div>
                                        </div> -->


<!-- 
                                      <div class="form-group">
                                        <label class="col-sm-3 col-sm-offset-1 control-label">State:</label>
                                        <div class="col-sm-7">
                                          <input type="text"  class="form-control" name="state" ng-model="job.state" required>
                                          <span ng-show="submitted && jobForm.state.$error.required"  class="help-block has-error ng-hide">State is required.</span>  
                                          <span ng-show="errorState" class="help-block has-error ng-hide">{{errorState}}</span>
                                        </div>
                                      </div> -->


                                      <!--  <div class="form-group">
                                        <label class="col-sm-3 col-sm-offset-1 control-label">City:</label>
                                        <div class="col-sm-7">
                                          <input type="text"  class="form-control" name="city" ng-model="job.city" ng-pattern="/^[a-zA-Z ]*$/" required>
                                          <span ng-show="submitted && jobForm.city.$error.required"  class="help-block has-error ng-hide">City is required.</span>
                                          <span ng-show="submitted && jobForm.city.$error.pattern"  class="help-block has-error ng-hide">City is required.</span>
                                          <span ng-show="errorCity" class="help-block has-error ng-hide">{{errorCity}}</span>

                                        </div>
                                      </div> -->


                                      <div class="form-group">
                                        <label class="col-sm-3 col-sm-offset-1 control-label">Country:</label>
                                        <div class="col-sm-7">
                                            <!-- <input type="text"  class="form-control" placeholder="Enter your nationality"> -->
                                            <select class="form-control" name="nationality" ng-model="job.nationality" onchange="getState(this.value);" required>
                                                <option disabled="disabled" selected="selected" value="">Select your nationality</option>
                                                <?php

                                                foreach($Countries as $key){
                                                 //$id=$key->id;
                                                    ?>

                                                    <option value="<?php echo $key->id;?>"><?php echo $key->name;?></option>
                                                    <?php }?>
                                                </select>
                                                <span ng-show="submitted && jobForm.nationality.$error.required"  class="help-block has-error ng-hide">Country is required.</span>
                                                <span ng-show="errorNationality" class="help-block has-error ng-hide">{{errorNationality}}</span>
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <label class="col-sm-3 col-sm-offset-1 control-label">State:</label>
                                            <div class="col-sm-7">
                                                <select  class="form-control" name="state" ng-model="job.state" id="state-list" onchange="getCity(this.value);" required>
                                                  <option selected="true" value=""></option>

                                              </select>
                                              <span ng-show="submitted && jobForm.state.$error.required"  class="help-block has-error ng-hide">State is required.</span>  
                                              <span ng-show="errorState" class="help-block has-error ng-hide">{{errorState}}</span>
                                          </div>
                                      </div>

                                      <div class="form-group">
                                        <label class="col-sm-3 col-sm-offset-1 control-label">City:</label>
                                        <div class="col-sm-7">
                                            <select  class="form-control" name="city" ng-model="job.city" id="city-list"  required>
                                               <option selected="true" value=""></option>

                                           </select>
                                           <span ng-show="submitted && jobForm.city.$error.required"  class="help-block has-error ng-hide">City is required.</span>
                                           <span ng-show="submitted && jobForm.city.$error.pattern"  class="help-block has-error ng-hide">City is required.</span>
                                           <span ng-show="errorCity" class="help-block has-error ng-hide">{{errorCity}}</span>

                                       </div>
                                   </div>
                                    
                                      <div class="form-group">
                                      <label class="col-sm-3 col-sm-offset-1 control-label">Min Salary:</label>
                                      <div class="col-sm-7">
                                        <div class="input-group"><span class="input-group-addon">$</span>
                                      <input name="min" type="text" class="form-control" ng-model="job.min" ng-pattern="/^[0-9]+(?:\.[0-9]+)?$/" lower-than="{{job.max}}" required/>
                                      <span ng-show="submitted && jobForm.min.$error.required"  class="help-block has-error ng-hide">Minimum Salary is required.</span>
                                      <span ng-show="submitted && jobForm.min.$error.pattern"  class="help-block has-error ng-hide">Valid Salary is required.</span>
                                    </div>
                                    </div>
                                    </div>
                                     <span class="error" ng-show="jobForm.min.$error.lowerThan" style="color:red;padding-left:171px;font-size:14px;">
                                    Minimum salary cannot exceed maximum salary.
                                     </span>
      
                                      <br />
                                      <div class="form-group">
                                        <label class="col-sm-3 col-sm-offset-1 control-label">Max Salary:</label>
                                        <div class="col-sm-7">
                                          <div class="input-group"><span class="input-group-addon">$</span>
                                        <input name="max" type="text" class="form-control" ng-model="job.max" required/>
                                      <span ng-show="submitted && jobForm.max.$error.required"  class="help-block has-error ng-hide">Maximum Salary is required.</span>
                                      <span ng-show="submitted && jobForm.max.$error.pattern"  class="help-block has-error ng-hide">Valid Salary is required.</span>
                                      </div>
                                      </div>
                                     </div>



                                     <div class="form-group">
                                      <label class="col-sm-3 col-sm-offset-1 control-label">Currency:</label>
                                      <div class="col-sm-7">
                                        
                                        <select class="form-control" name="currency" ng-model="job.currency" required>
                                          <option disabled="disabled" selected="selected" value="">Select your Currency</option>
                                          <?php
                                          foreach($allCurrency as $key)
                                          {

                                            ?>
                                            <option value="<?php echo $key->currency_id;?>"><?php echo $key->currency_code;?></option>
                                            <?php }?>
                                          </select>
                                          <span ng-show="submitted && jobForm.currency.$error.required"  class="help-block has-error ng-hide">Currency is required.</span>
                                          <span ng-show="errorJobType" class="help-block has-error ng-hide">{{errorJobType}}</span>
                                        </div>
                                      </div>




                                   


                                    <div class="form-group">
                                     <label class="col-sm-3 col-sm-offset-1 control-label">Company Profile:</label>
                                     <div class="col-sm-7">
                                       <input type="text" class="form-control" name="company_profile" ng-model="job.company_profile" required>
                                       <span ng-show="submitted && jobForm.company_profile.$error.required"  class="help-block has-error ng-hide">Company Profile is required.</span>  
                                      
                                     </div>
                                   </div>

                                 </div>
                                 <div class="col-md-12">
                                  <br>
                                  <h4>Description</h4>
                                  <hr>
                                </div>
                                <div class="col-md-12">
                                  <div class="form-group">
                                    <label class="col-sm-2 control-label col-sm-2">Job Description:</label>
                                    <div class="col-sm-10">
                                      <textarea  class="form-control ckeditor" id="editor"  rows="6" name="job_desc" ng-model="job.job_desc"></textarea>
                                      <span ng-show="submitted && jobForm.job_desc.$error.required"  class="help-block has-error ng-hide">Job Description is required.</span>
                                      <span ng-show="errorDescription" class="help-block has-error ng-hide">{{errorDescription}}</span>

                                    </div>
                                  </div>
                                </div>
                                <div class="col-md-12">
                                  <br>
                                  <h4>Attachments</h4>
                                  <hr>
                                </div>
                                <div class="col-md-6">
                                  <div class="form-group">
                                    <label class="control-label col-sm-3 col-sm-offset-1 text-right">Job Summary:</label>
                                    <div class="controls col-sm-7">
                                       

                                           <input id="uploadFile" class="default ng-pristine ng-valid ng-empty ng-touched" file-model="myFile" ng-model="job.attached_file" name="attached_file" ng-files="getTheFiles($files)" type="file">
                                           <span ng-show="submitted && jobForm.attached_file.$error.required"  class="help-block has-error ng-hide">Job Summary is required.</span>
                                           <span ng-show="errorFile" class="help-block has-error ng-hide">{{errorFile}}</span><br/>
                                           <span ng-bind="job.attached_file"></span>
                                         </div>
                                       </div>
                                        


                                         </div>
                                         <div class="col-md-12">
                                          <div class="form-group">
                                            <div class="col-sm-12">
                                              <button type="submit" class="btn btn-info pull-right btn-sm" ng-click="submitted = true"><strong><i class="fa fa-save"></i> Save &amp; Preview</strong></button><span class="pull-right"> &nbsp; &nbsp; </span>

                                              <a href="<?php echo base_url();?>client/viewJobForm"><button type="button" class="btn btn-danger pull-right btn-sm"><strong><i class="fa fa-times"></i> Cancel</strong></button></a>
                                            </div>
                                          </div>
                                        </div>
                                      </form>
                                    </div>
                                  </div>
                                </section>
                              </div>
                            </div>
                            <!-- page end-->

                          </section>
                        </section>
                        <!--main content end-->






                      </div>



<script type="text/javascript">
  

  jQuery.fn.multiselect = function() {
    $(this).each(function() {
        var checkboxes = $(this).find("input:checkbox");
        checkboxes.each(function() {
            var checkbox = $(this);
            // Highlight pre-selected checkboxes
            if (checkbox.prop("checked"))
                checkbox.parent().addClass("multiselect-on");
 
            // Highlight checkboxes that the user selects
            checkbox.click(function() {
                if (checkbox.prop("checked"))
                    checkbox.parent().addClass("multiselect-on");
                else
                    checkbox.parent().removeClass("multiselect-on");
            });
        });
    });
};


$(function() {
     $(".multiselect").multiselect();
});
</script>