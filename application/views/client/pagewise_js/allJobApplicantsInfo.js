<script>
$(document).ready(function() {

    //alert(123)

     var dataTable = $('#employee-grid').DataTable({
     	//alert();
            "processing": true,
            "serverSide": true,
            "ajax": {
                url: "<?php echo base_url();?>client/inneruserAppliedJobsData", // json datasource
                type: "post", // method  , by default get
                error: function () {  // error handling
                    $(".employee-grid-error").html("");
                    $("#employee-grid").append('<tbody class="employee-grid-error"><tr><th colspan="3">No data found in the server</th></tr></tbody>');
                    $("#employee-grid_processing").css("display", "none");

                }
            }
        });

} );
</script>