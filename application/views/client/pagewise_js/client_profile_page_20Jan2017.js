<script>
var postApp = angular.module('postApp', []);
postApp.controller('postController', function ($scope, $http) {

$scope.profile = {};

$scope.clientOnlineForm = function () {

 if ($scope.onlineForm.$valid) {
     

    $http({
      method: 'POST',
      dataType: 'json',
      url: '<?php echo base_url();?>client/client_online_data',

            data: $scope.profile, //forms client object
            headers: {'Content-Type': 'application/json'}
            }).success(function(data){
            
            if(data.status == 1){

           window.location='<?php echo base_url('onlineForm') ?>';

            }else{

          $scope.errorCompany = data.error.company.error;
          $scope.errorAddress1 = data.error.address1.error;
          $scope.errorAddress2 = data.error.address2.error;
          $scope.errorAddress3 = data.error.address3.error;
          $scope.errorCity = data.error.city.error;
          $scope.errorCountry = data.error.country.error;
          $scope.errorPostcode = data.error.postcode.error;
          $scope.errorCompanyNumber = data.error.company_number.error;
          $scope.errorvat_number = data.error.vat_number.error;
          $scope.errorestimated_turnover = data.error.estimated_turnover.error;
          $scope.errornumberOfStaff = data.error.numberOfStaff.error;
          $scope.errortitle = data.error.title.error;
          $scope.errordetailAddress1 = data.error.detailAddress1.error;
          $scope.errordetailAddress2 = data.error.detailAddress2.error;
          $scope.errordetailAddress3 = data.error.detailAddress3.error;
          $scope.errordetailCity = data.error.detailCity.error;
          $scope.errordetailCountry = data.error.detailCountry.error;
          $scope.errordetailPostcode = data.error.detailPostcode.error;
          $scope.errorindustry1 = data.error.industry1.error;
          $scope.errorindustry2 = data.error.industry2.error;
          $scope.errorindustry3 = data.error.industry3.error;
          $scope.errorcsr_policy = data.error.csr_policy.error;
            }

            });

   
    }else{

      console.log($scope.clientOnlineForm.$valid);


    }






};
});



</script>