<script>
var postApp = angular.module('postApp',[]);
postApp.directive('ngFiles', ['$parse', function ($parse) {

        function fn_link(scope, element, attrs) {
            var onChange = $parse(attrs.ngFiles);
            element.on('change', function (event) {
                onChange(scope, {$files: event.target.files});
            });
        }
        ;

        return {
            link: fn_link
        }
    }]);
postApp.controller('postController', function ($scope, $http) {
var formdata = new FormData();	
$scope.subClient = {};
$scope.subClient.subclient_id='<?php echo!empty($getSubClientData) ? $getSubClientData->id : '' ?>';
//alert($scope.subClient.subclient_id);
$scope.subClient.subclient_name = '<?php echo!empty($getSubClientData) ? $getSubClientData->subclient_name : '' ?>';
$scope.subClient.contact_number = '<?php echo!empty($getSubClientData) ? $getSubClientData->contact_number : '' ?>';
//$scope.subClient.manager = '<?php echo!empty($getSubClientData) ? $getSubClientData->manager : '' ?>';
$scope.subClient.industry = '<?php echo!empty($getSubClientData) ? $getSubClientData->ind_id : '' ?>';
$scope.subClient.about = '<?php echo !empty($getSubClientData) ? preg_replace("/[^A-Za-z0-9]/", ' ', $getSubClientData->about) : '' ?>';


//alert($scope.subClient.about);
$scope.subClient.source = '<?php echo!empty($getSubClientData) ? $getSubClientData->s_id : '' ?>';
$scope.subClient.parent_client = '<?php echo $parentName ?>';
//alert($scope.subClient.parent_client);
$scope.subClient.website = '<?php echo!empty($getSubClientData) ? $getSubClientData->website : '' ?>';
$scope.subClient.billing_street = '<?php echo!empty($getSubClientData) ? $getSubClientData->billing_street : '' ?>';
$scope.subClient.billing_city = '<?php echo!empty($getSubClientData) ? $getSubClientData->billing_city : '' ?>';
$scope.subClient.billing_state = '<?php echo!empty($getSubClientData) ? $getSubClientData->billing_state : '' ?>';
$scope.subClient.billing_code = '<?php echo!empty($getSubClientData) ? $getSubClientData->billing_code : '' ?>';
$scope.subClient.billing_country = '<?php echo!empty($getSubClientData) ? $getSubClientData->billing_country : '' ?>';
$scope.subClient.shipping_street = '<?php echo!empty($getSubClientData) ? $getSubClientData->shipping_street : '' ?>';
$scope.subClient.shipping_city = '<?php echo!empty($getSubClientData) ? $getSubClientData->shipping_city : '' ?>';
$scope.subClient.shipping_state = '<?php echo!empty($getSubClientData) ? $getSubClientData->shipping_state : '' ?>';
$scope.subClient.shipping_code = '<?php echo!empty($getSubClientData) ? $getSubClientData->shipping_code : '' ?>';
$scope.subClient.shipping_country = '<?php echo!empty($getSubClientData) ? $getSubClientData->shipping_country : '' ?>';
$scope.subClient.attached_file = '<?php echo!empty($getSubClientData) ? $getSubClientData->attached_file : '' ?>';

 var parent_client='<?php echo $parentName;?>';
//    //alert(parent_client);
 $scope.subClient.parent_client=parent_client;
$scope.getTheFiles = function ($files) {

        angular.forEach($files, function (value, key) {
            formdata.append('file', value);
        });

        var request = {
            method: 'POST',
            url: '<?php echo base_url();?>client/upload_clientdoc',
            data: formdata,
            enctype: 'multipart/form-data',
            headers: {
                'Content-Type': undefined
            }
        };
        $http(request)
                .success(function (data) {
                  console.log(data)
                    $scope.subClient.filename = data.filename;
                })
                .error(function () {
                });
    };
$scope.submitsubclientForm = function () {
var parent_client='<?php echo $parentName;?>';
   //alert(parent_client);
$scope.subClient.parent_client=parent_client;


if ($scope.subClientForm.$valid) {
$http({
      method: 'POST',
      dataType: 'json',
      url: '<?php echo base_url();?>client/create_sub_client',

            data: $scope.subClient, //forms client object
            headers: {'Content-Type': 'application/json'}
          }).success(function(data){
           if(data.status == 1){

            window.location = '<?php echo base_url('client/viewAllClients') ?>';


           }
           else{
          $scope.errorClientName = data.error.subclient_name.error;
          $scope.errorContactNumber = data.error.contact_number.error;
          $scope.errorManager = data.error.manager.error;
          $scope.errorIndustry = data.error.industry.error;
          $scope.errorAbout = data.error.regret.error;
          $scope.errorSource = data.error.source.error;
          $scope.errorParentClient = data.error.parent_client.error;
          $scope.errorWebsite = data.error.website.error;
          $scope.errorBillingStreet = data.error.billing_street.error;
          $scope.errorBillingCity = data.error.billing_city.error;
          $scope.errorBillingState = data.error.billing_state.error;
          $scope.errorBillingCode = data.error.billing_code.error;
          $scope.errorBillingCountry = data.error.billing_country.error;
          $scope.errorShippingStreet = data.error.shipping_street.error;
          $scope.errorShippingCity = data.error.shipping_city.error;
          $scope.errorShippingState = data.error.shipping_state.error;
          $scope.errorShippingCode = data.error.shipping_code.error;
          $scope.errorShippingCountry = data.error.shipping_country.error;
          $scope.errorFile = data.error.attached_file.error;
           }


         });


}else{


}

};


});
$('#client').addClass('active');
</script>
<script type="text/javascript" src="<?php echo base_url();?>client_assets/js/client_js/autocomplete.js"></script>
<script>
    
    var a = $('#selctionajax').autocomplete({
      serviceUrl:'<?php echo base_url();?>client/fetchSubClient',
      minChars:1,
      delimiter: /(,|;)\s*/, // regex or character
      maxHeight:600,
      //width:300,
      zIndex: 9999,
      deferRequestBy: 100, //miliseconds
       onSelect: function(suggestion) {
       //alert();
       if(suggestion.data)
                         {
                 //alert(suggestion.data)
           $('#organizationname').val(suggestion.data);
                        }
                        else
                        {
                        alert("sd");
                        }
}
      //params: { country:'Yes' }, //aditional parameters
      //noCache: false //default is false, set to true to disable caching
      // callback function:
      //onSelect: function(value, data){ alert('You selected: ' + value + ', ' + data); },
      // local autosugest options:
      //lookup: ['January', 'February', 'March', 'April', 'May'] //local lookup values
    });
    
  </script>