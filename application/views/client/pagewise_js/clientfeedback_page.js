<script>
var postApp = angular.module('postApp', []);

postApp.directive('calender', function() {
  //console.log('here');
  return {
  	require: 'ngModel',
  	link: function (scope, el, attr, ngModel) {
  		$(el).datepicker({
  			dateFormat: 'yy-mm-dd',
  			onSelect: function (dateText) {
  				console.log(dateText);
  				scope.feedback.start_date = dateText;

  				scope.$apply(function () {
  					ngModel.$setViewValue(dateText);
  				});
  			}
  		});
  	}
  };
});

postApp.directive('calenderend', function() {
  //console.log('here');
  return {
  	require: 'ngModel',
  	link: function (scope, el, attr, ngModel) {
  		$(el).datepicker({
  			dateFormat: 'yy-mm-dd',
  			onSelect: function (dateText) {
  				console.log(dateText);
  				scope.feedback.start_date = dateText;

  				scope.$apply(function () {
  					ngModel.$setViewValue(dateText);
  				});
  			}
  		});
  	}
  };
});
postApp.controller('postController', function ($scope, $http) {

	$scope.feedback = {};

	  $scope.clientFeedbackForm = function () {
   
  var start_date=$("#start_date").val();
  var end_date=$("#end_date").val();
  var punctuality=$("#t1").val();
  var appearance=$("#t2").val();
  // alert(appearance);
  var skill=$("#t3").val();
  var performance=$("#t4").val();

  $scope.feedback.start_date=start_date;
  $scope.feedback.end_date=end_date;
  $scope.feedback.punctuality=punctuality;
  $scope.feedback.apperance=appearance;
  $scope.feedback.skill=skill;
  $scope.feedback.performance=performance;

  

   if ($scope.feedbackForm.$valid) {
      //alert('here');

    //console.log('here');
    $http({
      method: 'POST',
      dataType: 'json',
      url: '<?php echo base_url();?>client/insertClientFeedback',

            data: $scope.feedback, //forms client object
            headers: {'Content-Type': 'application/json'}
          }).success(function(data){
            if(data.status == 1){

             window.location = '<?php echo base_url('client/viewFeedbackForm') ?>';


           }
           else{
          $scope.errorMember = data.error.member.error;
          $scope.errorLocation = data.error.location.error;
          $scope.errorStartDate = data.error.start_date.error;
          $scope.errorEndDate = data.error.end_date.error;
          $scope.errorStaffList = data.error.staff_list.error;
          $scope.errorComment = data.error.comments.error;
          $scope.errorName = data.error.name.error;
          $scope.errorPosition = data.error.position.error;
          $scope.errorEmail = data.error.email.error;
          

            }


         });


      }else{
         console.log('there');
        console.log($scope.feedbackForm.$valid);

      }
    };

});

function hideMsg(){
	$("#msg").hide();


}

function hideMsg1(){

$("#msg1").val();	
}

</script>

<script>
   
   $("#punctuality").rateYo({
    onSet: function (rating, rateYoInstance) {
     //alert(rating);
     $(this).next().val(rating);
     
    },
    rating: 0,
    starWidth: "40px",
    numStars: 5,
    
});

   $("#apperance").rateYo({
    onSet: function (rating, rateYoInstance) {
     //alert(rating);
     $(this).next().val(rating);
     
    },
    rating: 0,
    starWidth: "40px",
    numStars: 5,
    
});

   $("#skill").rateYo({
    onSet: function (rating, rateYoInstance) {
     //alert(rating);
     $(this).next().val(rating);
     
    },
    rating: 0,
    starWidth: "40px",
    numStars: 5,
    
});

   $("#performance").rateYo({
    onSet: function (rating, rateYoInstance) {
     //alert(rating);
     $(this).next().val(rating);
     
    },
    rating: 0,
    starWidth: "40px",
    numStars: 5,
    
});

   $("#feedback").addClass('active');
 </script>