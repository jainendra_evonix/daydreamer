<script>
var postApp = angular.module('postApp', []);
postApp.controller('postController', function ($scope, $http) {

$scope.profile = {};
$scope.profile.company = '<?php echo!empty($online_data) ? $online_data->cmpny_name : $register_data->company_name; ?>';
$scope.profile.address1 = '<?php echo!empty($online_data) ? $online_data->clnt_cmpny_add1 : '' ?>';
$scope.profile.address2 = '<?php echo!empty($online_data) ? $online_data->clnt_cmpny_add2 : '' ?>';
$scope.profile.address3 = '<?php echo!empty($online_data) ? $online_data->clnt_cmpny_add3 : '' ?>';
$scope.profile.city = '<?php echo!empty($online_data) ? $online_data->clnt_cmpny_city : $register_data->clnt_city ?>';
$scope.profile.country = '<?php echo!empty($online_data) ? $online_data->id : $register_data->clnt_country ?>';
$scope.profile.postcode = '<?php echo!empty($online_data) ? $online_data->clnt_cmpny_postcode : '' ?>';
$scope.profile.company_number = '<?php echo!empty($online_data) ? $online_data->clnt_cmpnyNo : '' ?>';
$scope.profile.vat_number = '<?php echo!empty($online_data) ? $online_data->  clnt_cmpny_vatNo   : '' ?>';
$scope.profile.estimated_turnover = '<?php echo!empty($online_data) ? $online_data->clnt_estd_trnovr : '' ?>';
$scope.profile.numberOfStaff = '<?php echo!empty($online_data) ? $online_data->clnt_estd_staffNo : '' ?>';
$scope.profile.title = '<?php echo!empty($online_data) ? $online_data->clnt_job_title : '' ?>';
$scope.profile.detailAddress1 = '<?php echo!empty($online_data) ? $online_data->clnt_dt_add1 : '' ?>';
$scope.profile.detailAddress2 = '<?php echo!empty($online_data) ? $online_data->clnt_dt_add2 : '' ?>';
$scope.profile.detailAddress3 = '<?php echo!empty($online_data) ? $online_data->clnt_dt_add3 : '' ?>';
$scope.profile.detailCity = '<?php echo!empty($online_data) ? $online_data->clnt_dt_city : '' ?>';
$scope.profile.detailCountry = '<?php echo!empty($online_data) ? $online_data->clnt_dt_cntry : '' ?>';
$scope.profile.detailPostcode = '<?php echo!empty($online_data) ? $online_data->clnt_dt_postcode : '' ?>';
$scope.profile.industry1 = '<?php echo!empty($online_data) ? $online_data->clnt_dt_indstry1 : '' ?>';
$scope.profile.industry2 = '<?php echo!empty($online_data) ? $online_data->clnt_dt_indstry2 : '' ?>';
$scope.profile.industry3 = '<?php echo!empty($online_data) ? $online_data->clnt_dt_indstry3 : '' ?>';
$scope.profile.radio1 = '<?php echo!empty($online_data) ? $online_data->clnt_dt_apprentices : '' ?>';
$scope.profile.csr_policy = '<?php echo!empty($online_data) ? $online_data->clnt_dt_csrpolicy : '' ?>';
$scope.profile.radio2 = '<?php echo!empty($online_data) ? $online_data->clnt_exoffenders : '' ?>';

$scope.clientOnlineForm = function () {

 if ($scope.onlineForm.$valid) {
     

    $http({
      method: 'POST',
      dataType: 'json',
      url: '<?php echo base_url();?>client/client_online_data',

            data: $scope.profile, //forms client object
            headers: {'Content-Type': 'application/json'}
            }).success(function(data){
            
            if(data.status == 1){

           window.location='<?php echo base_url('client/viewDashboard') ?>';

            }else{

          $scope.errorCompany = data.error.company.error;
          $scope.errorAddress1 = data.error.address1.error;
          $scope.errorAddress2 = data.error.address2.error;
          $scope.errorAddress3 = data.error.address3.error;
          $scope.errorCity = data.error.city.error;
          $scope.errorCountry = data.error.country.error;
          $scope.errorPostcode = data.error.postcode.error;
          $scope.errorCompanyNumber = data.error.company_number.error;
          $scope.errorvat_number = data.error.vat_number.error;
          $scope.errorestimated_turnover = data.error.estimated_turnover.error;
          $scope.errornumberOfStaff = data.error.numberOfStaff.error;
          $scope.errortitle = data.error.title.error;
          $scope.errordetailAddress1 = data.error.detailAddress1.error;
          $scope.errordetailAddress2 = data.error.detailAddress2.error;
          $scope.errordetailAddress3 = data.error.detailAddress3.error;
          $scope.errordetailCity = data.error.detailCity.error;
          $scope.errordetailCountry = data.error.detailCountry.error;
          $scope.errordetailPostcode = data.error.detailPostcode.error;
          $scope.errorindustry1 = data.error.industry1.error;
          $scope.errorindustry2 = data.error.industry2.error;
          $scope.errorindustry3 = data.error.industry3.error;
          $scope.errorcsr_policy = data.error.csr_policy.error;
            }

            });

   
    }else{

      console.log($scope.clientOnlineForm.$valid);


    }






};
});



</script>