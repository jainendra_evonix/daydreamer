<script>
var postApp = angular.module('postApp', []);
postApp.controller('postController', function ($scope, $http) {

	$scope.reset={};
	$scope.reset.emailaddress='<?php echo $data->clnt_email;?>';
	// alert($scope.reset.emailaddress);
	$scope.submitForm = function () {

		if ($scope.reset.npassword != $scope.reset.cpassword) {
			$scope.IsMatch=true;
			return false;
		}
		else{
			if ($scope.clientResetPassword.$valid) {

				$http({
					method: 'POST',
					dataType: 'json',
					url: '<?php echo base_url() ?>client/check_reset_password',
                    data: $scope.reset, //forms user object
                    headers: {'Content-Type': 'application/json'}
                }).success(function(data){
                	if (data.status == 1) {
                		$('#message').html('');
                		$('#message').append(data.message);
                		$('#npassword').val("");
                		$('#cpassword').val("");
                		$scope.user.npassword='';
                		$scope.user.cpassword='';
                		$('.help-block').css('display','none');
                	} else if (data.status == 0)
                	{
                		$('#message').html('');
                		$('#message').append(data.message);
                		
                	} else if (data.status == 3)
                	{
                		$('#message').html('');
                		$('#message').append('<div class="alert alert-danger"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>Password are required !.</div>');
                		
                	} else {
                        		  //$('#message').html('');
                        		  $scope.npasswordError = data.error.npassword.error;
                        		  $scope.cpasswordError = data.error.cpassword.error;
                        		}

                        	});

}else{
	console.log($scope.clientResetPassword.$valid);

}
}
};
});
</script>