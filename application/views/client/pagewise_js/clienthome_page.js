<script>

var postApp = angular.module('postApp', []);
postApp.directive('ngFiles', ['$parse', function ($parse) {

	function fn_link(scope, element, attrs) {
		var onChange = $parse(attrs.ngFiles);
		element.on('change', function (event) {
			onChange(scope, {$files: event.target.files});
		});
	}
	;

	return {
		link: fn_link
	}
}]);
postApp.controller('postController', function ($scope, $http) {
	var formdata = new FormData();
	$scope.dashboard={};

	$scope.getTheFiles = function ($files) {

		angular.forEach($files, function (value, key) {
			formdata.append('file', value);
		});

		var request = {
			method: 'POST',
			url: '<?php echo base_url();?>client/upload_company_logo',
			data: formdata,
			enctype: 'multipart/form-data',
			headers: {
				'Content-Type': undefined
			}
		};
		$http(request)
		.success(function (data) {
			console.log(data)
			$scope.dashboard.filename = data.filename;
			//console.log($scope);
             $("#message2").hide();
		})
		.error(function () {
		});
	};

	$scope.uploadCompanyLogo = function () {

		if ($scope.upload_logo.$valid) {

			$http({
				method: 'POST',
				dataType: 'json',
				url: '<?php echo base_url();?>client/clientLogo',

            data: $scope.dashboard, //forms client object
            headers: {'Content-Type': 'application/json'}
        }).success(function(data){
         if(data.status == 1){
             //$("#message").append(data.message);
            window.location = '<?php echo base_url('client/viewDashboard') ?>';
           

            }else if(data.status== 3){

               $("#message2").append(data.error);

           }


        });


		}else{



		}



	};


});




</script>