<script type="text/javascript">
var postApp = angular.module('postApp', []);
postApp.controller('postController', function ($scope, $http) {

  $scope.client = {};

  $scope.registration = function () {

   if ($scope.registrationForm.$valid) {


    $http({
      method: 'POST',
      dataType: 'json',
      url: '<?php echo base_url();?>client/create_account',

            data: $scope.client, //forms client object
            headers: {'Content-Type': 'application/json'}
          }).success(function(data){
           if(data.status == 1){

            window.location = '<?php echo base_url('client/register') ?>';


           }
           else{
          $scope.errorFirstname = data.error.firstname.error;
          $scope.errorLastname = data.error.lastname.error;
          $scope.errorCity = data.error.city.error;
          $scope.errorCountry = data.error.country.error;
          $scope.errorTelephone = data.error.telephone.error;
          $scope.errorEmail = data.error.email.error;
          $scope.errorValidateEmail = data.error.validate_email.error;
          $scope.errorPassword = data.error.password.error;
          $scope.errorValidatePassword = data.error.validate_password.error;
          $scope.errorAgreement = data.error.agree.error;

           }


         });


      // }else{

      //   console.log($scope.registrationForm.$valid);

      // }
    }else{

      console.log($scope.registrationForm.$valid);


    }
  };

});
</script>