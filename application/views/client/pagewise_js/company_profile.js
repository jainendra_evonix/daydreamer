<script>
var postApp = angular.module('postApp', []);

postApp.directive('ngFiles', ['$parse', function ($parse) {

        function fn_link(scope, element, attrs) {
            var onChange = $parse(attrs.ngFiles);
            element.on('change', function (event) {
                onChange(scope, {$files: event.target.files});
            });
        }
        ;

        return {
            link: fn_link
        }
    }]);

postApp.controller('postController', function ($scope, $http) {

 var formdata = new FormData();


	$scope.profile = {};
  $scope.profile.company_name='<?php echo!empty($fetchCompanyData) ? $fetchCompanyData->company_name : '' ?>';
  $scope.profile.company_overview='<?php echo!empty($fetchCompanyData) ? preg_replace("/[^A-Za-z0-9]/", ' ', $fetchCompanyData->company_overview) : '' ?>';
  $scope.profile.founded='<?php echo!empty($fetchCompanyData) ? $fetchCompanyData->founded : '' ?>';
  $scope.profile.headquarters='<?php echo!empty($fetchCompanyData) ? $fetchCompanyData->headquarters : '' ?>';
  $scope.profile.staff='<?php echo!empty($fetchCompanyData) ? $fetchCompanyData->staff : '' ?>';
  $scope.profile.web_address='<?php echo!empty($fetchCompanyData) ? $fetchCompanyData->web_address : '' ?>';
  $scope.profile.ratings='<?php echo!empty($fetchCompanyData) ? $fetchCompanyData->rating : '' ?>';
  $scope.profile.attached_file='<?php echo!empty($fetchCompanyData) ? $fetchCompanyData->banner_pic : '' ?>';
  

	  $scope.submitprofileForm = function () {

      var rating=$("#t2").val();
      $scope.profile.ratings=rating;


   if ($scope.profileForm.$valid) {
      //alert('here');

    //console.log('here');
    $http({
      method: 'POST',
      dataType: 'json',
      url: '<?php echo base_url();?>client/insertClientProfile',

            data: $scope.profile, //forms client object
            headers: {'Content-Type': 'application/json'}
          }).success(function(data){
             if(data.status == 1){

             window.location = '<?php echo base_url('client/companyProfile') ?>';


           }
            else{
           $scope.errorMember = data.error.member.error;
           $scope.errorLocation = data.error.location.error;
           $scope.errorStartDate = data.error.start_date.error;
           $scope.errorEndDate = data.error.end_date.error;
           $scope.errorStaffList = data.error.staff_list.error;
           $scope.errorComment = data.error.comments.error;
           $scope.errorName = data.error.name.error;
           $scope.errorPosition = data.error.position.error;
           $scope.errorEmail = data.error.email.error;
          

           }


          });


      }else{
         console.log('there');
        console.log($scope.feedbackForm.$valid);

      }
     
    
    };
    $scope.info={};
    $scope.info.mobile_number='<?php echo!empty($fetchCompanyinfo) ? $fetchCompanyinfo->mobile_number : '' ?>';
    $scope.info.address='<?php echo!empty($fetchCompanyinfo) ? preg_replace("/[^A-Za-z0-9]/", ' ', $fetchCompanyinfo->address) : '' ?>';
    $scope.info.email='<?php echo!empty($fetchCompanyinfo) ? $fetchCompanyinfo->email : '' ?>';
    $scope.info.website='<?php echo!empty($fetchCompanyinfo) ? $fetchCompanyinfo->website : '' ?>';
    $scope.info.fb='<?php echo!empty($fetchCompanyinfo) ? $fetchCompanyinfo->fb : '' ?>';
    $scope.info.twitter='<?php echo!empty($fetchCompanyinfo) ? $fetchCompanyinfo->twitter : '' ?>';
    $scope.info.linkedin='<?php echo!empty($fetchCompanyinfo) ? $fetchCompanyinfo->linkedin : '' ?>';
    $scope.info.instagram='<?php echo!empty($fetchCompanyinfo) ? $fetchCompanyinfo->instagram : '' ?>';
    $scope.info.googleplus='<?php echo!empty($fetchCompanyinfo) ? $fetchCompanyinfo->googleplus : '' ?>';
    $scope.info.pinterest='<?php echo!empty($fetchCompanyinfo) ? $fetchCompanyinfo->pinterest : '' ?>';

    $scope.submitInfoForm=function(){
     
   if ($scope.InfoForm.$valid) {
    $http({
      method: 'POST',
      dataType: 'json',
      url: '<?php echo base_url();?>client/inserClientInfo',

            data: $scope.info, //forms client object
            headers: {'Content-Type': 'application/json'}
          }).success(function(data){

          if(data.status == 1){

             window.location = '<?php echo base_url('client/companyProfile') ?>';


           }
            else{
           // $scope.errorMember = data.error.member.error;
           // $scope.errorLocation = data.error.location.error;
           // $scope.errorStartDate = data.error.start_date.error;
           // $scope.errorEndDate = data.error.end_date.error;
           // $scope.errorStaffList = data.error.staff_list.error;
           // $scope.errorComment = data.error.comments.error;
           // $scope.errorName = data.error.name.error;
           // $scope.errorPosition = data.error.position.error;
           // $scope.errorEmail = data.error.email.error;
          

           }

          });
    
  }else{

   
  }

    };
  $scope.banner_pic={};
  $scope.getTheFiles = function ($files) {


        angular.forEach($files, function (value, key) {
            formdata.append('file', value);
        });

        var request = {
            method: 'POST',
            url: '<?php echo base_url();?>client/uploadBanner',
            data: formdata,
            enctype: 'multipart/form-data',
            headers: {
                'Content-Type': undefined
            }
        };
        $http(request)
                .success(function (data) {
                  //console.log(data)
                    $scope.banner_pic.filename = data.filename;
                    
                })
                .error(function () {
                });
    };

 //console.log($scope.banner_pic);
  $scope.submitBannerPic=function(){
  if ($scope.BannerPicUpload.$valid) {
  $http({
      method: 'POST',
      dataType: 'json',
      url: '<?php echo base_url();?>client/inserBannerPic',

            data: $scope.banner_pic, //forms client object
            headers: {'Content-Type': 'application/json'}
          }).success(function(data){
           if(data.status == 1){

             window.location = '<?php echo base_url('client/companyProfile') ?>';


           }else{


           }

          });
  
  }else{


  }

  };

    

    $scope.brand={};
    $scope.submitform=function(){

    var imageData = $('.image-editor').cropit('export');
                //alert(imageData);
                var fu1 = document.getElementById("FileUpload1");
                       var imgname = fu1.files[0].name;
                       //alert(imgname);
                       //$("#doc_pic").val(imgname);
                      $scope.brand.imgdata=imageData;
                      $scope.brand.userfile=imgname;
                //alert($scope.uploadme);
                $scope.brand.doc_pic = $('#doc_pic').val();
    
                        // Posting data to php file
                        if ($scope.profilePic.$valid) {
                            //alert('if');
                   //alert('login');
                                $http({
                                method: 'POST',
                                        dataType: 'json',
                                        url: '<?php echo base_url() ?>client/sbmtCompanyLogo',
                                        data: $scope.brand, //forms user object
                                        headers: {'Content-Type': 'application/json'}
                                }).success(function (data) {
                                    
                                
                                $('#succ').html('File uploaded');    
                               //console.log(data);
                                    //alert(data.status);
                        //console.log(data);
                                // console.log(data.error.password.error);
                                if (data.status == 1) {
                                     $('.help-block').css('display','none');
                                     /*setTimeout(function(){
                                        //$('#message2').fadeOut();
                                        window.location = '<?php echo base_url();?>admin/addbrand';
                                    }, 5000);*/
                                    window.location = '<?php echo base_url();?>client/companyProfile';
                        } 
                        else {
                            //console.log('dataaa')
                            //console.log(data.error.brandname.error)
                                $scope.brandnameError = data.error.brandname.error;
                                $scope.descriptionError = data.error.description.error;
                                $scope.doc_picError = data.error.doc_pic.error;
                        }     
                        
                        });
                        }

    };




});





</script>

<script>
   
   

   $("#ratings").rateYo({
    onSet: function (rating, rateYoInstance) {
     //alert(rating);
     $(this).next().val(rating);
     
    },
    rating: 0,
    starWidth: "40px",
    numStars: 5,
    
});

   //$("#feedback").addClass('active');

   $(function() {
        $('.image-editor').cropit({
          imageState: {
            //src: 'http://lorempixel.com/500/400/',
          },
        });

        $('.rotate-cw').click(function() {
          $('.image-editor').cropit('rotateCW');
        });
        $('.rotate-ccw').click(function() {
          $('.image-editor').cropit('rotateCCW');
        });

        $('.export').click(function() {
          var imageData = $('.image-editor').cropit('export');
          var fu1 = document.getElementById("FileUpload1");
          var imgname = fu1.files[0].name;
         // alert(imgname);
          if(imageData!=undefined) {
              $("#doc_pic").val(imgname);
           }
           else {
               $("#doc_pic").val('');
           }
          //window.open(imageData);
        });
        
      });
 </script>


 <script>
$(document).ready(function()
{


/* Uploading Profile BackGround Image */
$('body').on('change','#bgphotoimg', function()
{

$("#bgimageform").ajaxForm({target: '#timelineBackground',
beforeSubmit:function(){},
success:function(){

$("#timelineShade").hide();
$("#bgimageform").hide();
},
error:function(){

} }).submit();
});



/* Banner position drag */
$("body").on('mouseover','.headerimage',function ()
{
var y1 = $('#timelineBackground').height();
var y2 =  $('.headerimage').height();
$(this).draggable({
scroll: false,
axis: "y",
drag: function(event, ui) {
if(ui.position.top >= 0)
{
ui.position.top = 0;
}
else if(ui.position.top <= y1 - y2)
{
ui.position.top = y1 - y2;
}
},
stop: function(event, ui)
{
}
});
});


/* Bannert Position Save*/
$("body").on('click','.bgSave',function ()
{
var id = $(this).attr("id");
var p = $("#timelineBGload").attr("style");
var Y =p.split("top:");
var Z=Y[1].split(";");
var dataString ='position='+Z[0];
$.ajax({
type: "POST",
url: "http://localhost/daydreamer/client/cover_image_save",
data: dataString,
cache: false,
beforeSend: function(){ },
success: function(html)
{
if(html)
{
$(".bgImage").fadeOut('slow');
$(".bgSave").fadeOut('slow');
$("#timelineShade").fadeIn("slow");
$("#timelineBGload").removeClass("headerimage");
$("#timelineBGload").css({'margin-top':html});
return false;
}
}
});
return false;
});



});
</script>