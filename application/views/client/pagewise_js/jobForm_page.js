<script type="text/javascript">

//var t =(JSON.parse($('#selected_values').val())).length > 0 ? JSON.parse($('#selected_values').val()) : [];

//console.log(t);

var postApp = angular.module('postApp', []);
 postApp.directive('ngFiles', ['$parse', function ($parse) {

        function fn_link(scope, element, attrs) {
            var onChange = $parse(attrs.ngFiles);
            element.on('change', function (event) {
                onChange(scope, {$files: event.target.files});
            });
        }
        ;

        return {
            link: fn_link
        }
    }]);


 postApp.directive('calender', function() {
  //console.log('here');
    return {
                require: 'ngModel',
                link: function (scope, el, attr, ngModel) {
                    $(el).datepicker({
                        dateFormat: 'yy-mm-dd',
                        onSelect: function (dateText) {
                          console.log(dateText);
                          scope.addPackage.validity = dateText;

                            scope.$apply(function () {
                                ngModel.$setViewValue(dateText);
                            });
                        }
                    });
                }
            };
});

postApp.directive('calenderopen', function() {
  //console.log('here');
    return {
                require: 'ngModel',
                link: function (scope, el, attr, ngModel) {
                    $(el).datepicker({
                        dateFormat: 'yy-mm-dd',
                        onSelect: function (dateText) {
                          console.log(dateText);
                          scope.addPackage.validity = dateText;

                            scope.$apply(function () {
                                ngModel.$setViewValue(dateText);
                            });
                        }
                    });
                }
            };
});



postApp.directive('ckEditor', function() {
  console.log('here');
  return {
    require: '?ngModel',
    link: function(scope, elm, attr, ngModel) {
      var ck = CKEDITOR.replace(elm[0]);

      if (!ngModel) return;

      ck.on('pasteState', function() {
        scope.$apply(function() {
          ngModel.$setViewValue(ck.getData());
        });
      });

      ngModel.$render = function(value) {
        ck.setData(ngModel.$viewValue);
      };
    }
  };
});

postApp.directive('lowerThan', [
  function() {
   //alert();
    var link = function($scope, $element, $attrs, ctrl) {

      var validate = function(viewValue) {
        var comparisonModel = $attrs.lowerThan;
        
        if(!viewValue || !comparisonModel){
          // It's valid because we have nothing to compare against
          ctrl.$setValidity('lowerThan', true);
        }

        // It's valid if model is lower than the model we're comparing against
        ctrl.$setValidity('lowerThan', parseInt(viewValue, 10) < parseInt(comparisonModel, 10) );
        return viewValue;
      };

      ctrl.$parsers.unshift(validate);
      ctrl.$formatters.push(validate);

      $attrs.$observe('lowerThan', function(comparisonModel){
        return validate(ctrl.$viewValue);
      });
      
    };

    return {
      require: 'ngModel',
      link: link
    };

  }
]);


// postApp.controller('postController', ['$scope', 'FileUploader', '$http', function ($scope, FileUploader, $http) {
postApp.controller('postController', function ($scope, $http) {



 var formdata = new FormData();
  $scope.job = {};
 

 $scope.job.sub_client_id='<?php echo!empty($subClientId) ? $subClientId : '' ?>';
 $scope.job.job_id='<?php echo!empty($getJobData) ? $getJobData->j_id : '' ?>';
 
 $scope.job.job_title='<?php echo!empty($getJobData) ? $getJobData->job_title : '' ?>';
 $scope.job.contact_name='<?php echo!empty($getJobData) ? $getJobData->contact_name : '' ?>';
 $scope.job.recruiter='<?php echo!empty($getJobData) ? $getJobData->recruiter : '' ?>';
 //$scope.job.target_date='<?php echo!empty($getJobData) ? date("d/m/y",strtotime($getJobData->target_date)) : '' ?>';
 $scope.job.job_status='<?php echo!empty($getJobData) ? $getJobData->status_id : '' ?>';
 //$scope.job.industry='<?php echo!empty($getJobData) ? $getJobData->ind_id : '' ?>';

 $scope.job.state='<?php echo!empty($getJobData) ? $getJobData->state : '' ?>';
 $scope.job.experience='<?php echo!empty($getJobData) ? $getJobData->experience : '' ?>';
 //$scope.job.office_name='<?php echo!empty($getJobData) ? $getJobData->subclient_name : '' ?>';
 $scope.job.office_name='<?php echo $officeName; ?>';
 $scope.job.manager='<?php echo!empty($getJobData) ? $getJobData->manager : '' ?>';
 //alert($scope.job.manager);
 $scope.job.positions='<?php echo!empty($getJobData) ? $getJobData->positions : '' ?>';
 $scope.job.open_date='<?php echo!empty($getJobData) ? date("y-m-d",strtotime($getJobData->open_date)) : '' ?>';
 $scope.job.job_type='<?php echo!empty($getJobData) ? $getJobData->jobtype_id : '' ?>';
 $scope.job.qualifications='<?php echo!empty($getJobData) ? preg_replace("/[^A-Za-z0-9]/", ' ', $getJobData->qualifications) : '' ?>';
 //$scope.job.radio2='<?php echo!empty($getJobData) ? $getJobData->gender : '' ?>';
 $scope.job.working_days='<?php echo!empty($getJobData) ? $getJobData->working_days : '' ?>';
 $scope.job.company_profile='<?php echo!empty($getJobData) ? preg_replace("/[^A-Za-z0-9]/", ' ', $getJobData->company_profile) : '' ?>';
 $scope.job.city='<?php echo !empty($getJobData) ? $getJobData->city : '' ?>';
 $scope.job.published = '<?php echo !empty($getJobData) ? $getJobData->job_publish : '' ?>';

 //$scope.job.skill='<?php echo!empty($getJobData) ? $getJobData->skills : '' ?>';

  //var a= '<?php echo $getJobData->skills ?>';
  

  //console.log(JSON.parse(b));
  // a = a.split(',');
  // var $e1 = $("#e9").select2();

  // $e1.val(a).trigger("change");

$scope.job.nationality='<?php echo!empty($getJobData) ? $getJobData->id : '' ?>';
 //$scope.job.skills=a;
 
  //console.log($scope.job.nationality);
$scope.job.min='<?php echo!empty($getJobData) ? $getJobData->min_salary : '' ?>';
//alert($scope.job.min);
$scope.job.max='<?php echo!empty($getJobData) ? $getJobData->max_salary : '' ?>';
$scope.job.currency='<?php echo!empty($getJobData) ? $getJobData->currency_id : '' ?>';
//$scope.job.job_nature='<?php echo!empty($getJobData) ? $getJobData->job_nature : '' ?>';
$scope.job.job_desc='<?php echo!empty($getJobData) ? preg_replace("/[^A-Za-z0-9]/", ' ', $getJobData->job_desc) : '' ?>';
$scope.job.attached_file='<?php echo!empty($getJobData) ? preg_replace("/[^A-Za-z0-9]/", ' ', $getJobData->job_summary) : '' ?>';
$scope.job.published=0;
//console.log($scope.job);
//alert($scope.job.attached_file);
//$scope.job.showImgName='<?php echo $getJobData->job_summary?>';
//alert($scope.job.showImgName);
//var b='<?php echo $skills;?>';
//b = JSON.parse(b);
//var b = [2,4];
//$('#e9').val(b).trigger('change');
var subclient_name='<?php echo $subClient_name;?>';

$scope.job.subclient_name=subclient_name;

if($scope.job.office_name!=''){

$("#selctionajax").attr('readonly','true');
}else{

//$("#selctionajax").attr('readonly','false');

}


  $scope.getTheFiles = function ($files) {

        angular.forEach($files, function (value, key) {
            formdata.append('file', value);
        });

        var request = {
            method: 'POST',
            url: '<?php echo base_url();?>client/upload_doc',
            data: formdata,
            enctype: 'multipart/form-data',
            headers: {
                'Content-Type': undefined
            }
        };
        $http(request)
                .success(function (data) {
                  console.log(data)
                    $scope.job.filename = data.filename;
                    
                })
                .error(function () {
                });
    };
     // var b='<?php echo $skills;?>';
     // var c= $("#e9").select2('data', JSON.parse(b));
     // console.log(c);

  $scope.submitjobForm = function () {


     $scope.job.jobType=[];

        


     $('#jobType input:checkbox:checked').each(function() {
        
      
      
    // alert(check)
    $scope.job.jobType.push($(this).val());
   
 //  console.log($scope.job.jobType)
    
   });
   

   
   $scope.job.hours=[];

     $('#hours input:checkbox:checked').each(function() {
        
       
      
   //  alert(check)
    $scope.job.hours.push($(this).val());

    
   });   

   $scope.job.shifts=[];

     $('#shifts input:checkbox:checked').each(function() {
        
            
    // alert(check)
    $scope.job.shifts.push($(this).val());

    
   });   

  $scope.job.contarctType=[];

     $('#contarctType input:checkbox:checked').each(function(){
        
        //alert()
       
      
    // alert(check)
    $scope.job.contarctType.push($(this).val());

    
   });   




    
  var editor=CKEDITOR.instances.editor;
  $scope.job.job_desc=editor.getData();
  
 
  var target_date=$("#target_date").val();
  var open_date=$("#open_date").val();
  var editor=$(".ckeditor").val();
  

  $scope.job.target_date=target_date;
  $scope.job.open_date=open_date;

  var parent_client=$("#organizationname").val();
  

  $scope.job.parent_client=parent_client;
  

   if ($scope.jobForm.$valid){


    
    $http({
      method: 'POST',
      dataType: 'json',
      url: '<?php echo base_url();?>client/create_job_opening',

            data: $scope.job, //forms client object
            headers: {'Content-Type': 'application/json'}
          }).success(function(data){
           if(data.status == 1){

           // window.location = '<?php echo base_url('client/viewJobs') ?>';

            window.location='<?php echo base_url('client/previewJob') ?>';


           }

          else if(data.status == 4)
           {

               window.location = '<?php echo base_url('client/viewPackages') ?>';

           }

           else if(data.status == 5)
           {
              window.location='<?php echo base_url('client/viewPackages') ?>';
           }else if(data.status==12)
           {

            // window.location='<?php echo base_url('client/viewJobForm') ?>';
             $("#showStatus").html('<div class="alert alert-warning alert-dismissable"><a type="button" class="close" data-dismiss="alert" aria-hidden="true">×</a>All fields are mandatory.</div>');


           }

           else{
          $scope.errorTitle = data.error.job_title.error;
          $scope.errorContactName = data.error.contact_name.error;
          $scope.errorRecruiter = data.error.recruiter.error;
          $scope.errorTarget = data.error.target_date.error;
          $scope.errorStatus = data.error.job_status.error;
          $scope.errorIndustry = data.error.industry.error;
          $scope.errorState = data.error.state.error;
          $scope.errorExperience = data.error.experience.error;
          $scope.errorClientName = data.error.parent_clienterrorManager.error;
          $scope.errorManager = data.error.manager.error;
          $scope.errorPositions = data.error.positions.error;
          $scope.errorOpen = data.error.open_date.error;
          $scope.errorJobType = data.error.job_type.error;
          $scope.errorCity = data.error.city.error;
          $scope.errorNationality = data.error.nationality.error;
          $scope.errorSalary = data.error.salary.error;
          $scope.errorDescription = data.error.job_desc.error;
          $scope.errorFile = data.error.attached_file.error;

           }


         });


      }else{
         console.log('there');
        console.log($scope.jobForm.$valid);

      }
    };

  

});

function hideMsg(){
$("#msg").hide();


}

function hideopen(){

  $("#msg1").hide();
}

$('#job').addClass('active');




</script>
<script type="text/javascript" src="<?php echo base_url();?>client_assets/js/client_js/autocomplete.js"></script>
<script>
    
    var a = $('#selctionajax').autocomplete({
      serviceUrl:'<?php echo base_url();?>client/fetchSubClient',
      minChars:1,
      delimiter: /(,|;)\s*/, // regex or character
      maxHeight:600,
      //width:300,
      zIndex: 9999,
      deferRequestBy: 100, //miliseconds
       onSelect: function(suggestion) {
       //alert();
       if(suggestion.data)
                         {
           $('#organizationname').val(suggestion.data);

                        }
                        else
                        {
                        alert("sd");
                        }
}
      //params: { country:'Yes' }, //aditional parameters
      //noCache: false //default is false, set to true to disable caching
      // callback function:
      //onSelect: function(value, data){ alert('You selected: ' + value + ', ' + data); },
      // local autosugest options:
      //lookup: ['January', 'February', 'March', 'April', 'May'] //local lookup values
    });


    $(".populate.m-bot15").select2();
    
  </script>
  <script src="<?php echo base_url();?>client_assets/js/client_js/jquery.validate.min.js"></script>
  <script>
//   $.validator.addMethod("greaterThan",

// function (value, element, param) {
//   var $element = $(element)
//       , $min;
//   if (typeof(param) === "string") {
//       $min = $(param);
//   } else {
//       $min = $("#" + $element.data("min"));
//   }
//   if (this.settings.onfocusout) {
//     $min.off(".validate-greaterThan").on("blur.validate-greaterThan", function () {
//       $element.valid();
//     });
//   }
//   return parseInt(value) > parseInt($min.val());
// }, "Max must be greater than min");

// $.validator.addClassRules({
//   greaterThan: {
//     greaterThan: true
//   }
// });
// $('#sales').validate();
  </script>
<script type="text/javascript">
  

  function getCity(val){
$.ajax({
url:'<?php echo base_url();?>client/fetchCities',
type:'POST',
data:'state_id='+val,
success: function(data){
  //alert(data);
    $("#city-list").html(data);
    $("#shipping_city-list").html(data);
  }

});

}

 function getState(val){
   // alert(val);
$.ajax({
url:'<?php echo base_url();?>client/fetchStates',
type:'POST',
data:'country_id='+val,
success: function(data){
  //alert(data);
    $("#state-list").html(data);
    
  }

});


}


</script>