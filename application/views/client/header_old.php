
<body class="login-body" ng-app="postApp" ng-controller="postController">

<section id="container">
        <header class="header fixed-top clearfix">
            <!--logo start-->
            <div class="brand">

                <a href="index.html" class="logo">
                    <!-- <img src="images/logo.png" alt=""> -->
                    DAY DREAMER
                </a>
            </div>
            <!--logo end-->


            <div class="horizontal-menu navbar-collapse collapse ">
              <ul class="nav navbar-nav">
                  <li><a href="#">Home</a></li>
                  <li class="active"><a href="#">Jobs</a></li>
                  <li class="dropdown">
                      <a data-toggle="dropdown" data-hover="dropdown" class="dropdown-toggle" href="#">Recruiters <b class=" fa fa-angle-down"></b></a>
                      <ul class="dropdown-menu">
                          <li><a href="#">Browse All Recruiters</a></li>
                          <li><a href="#">Add Recruiters</a></li>
                          <li><a href="#">Search Recruiters</a></li>
                      </ul>
                  </li>
                  <li><a href="#">Services</a></li>
                  <li class="dropdown">
                      <a data-toggle="dropdown" data-hover="dropdown" class="dropdown-toggle" href="#">Companies <b class=" fa fa-angle-down"></b></a>
                      <ul class="dropdown-menu">
                          <li><a href="#">All Companies</a></li>
                          <li><a href="#">Add Companies</a></li>
                          <li><a href="#">Search Companies</a></li>
                      </ul>
                  </li>
              </ul>

            </div>

            <div class="top-nav clearfix">
                <!--search & user info start-->
                <ul class="nav pull-right top-menu">
                    <li>
                        <input type="text" class="form-control search" placeholder=" Search" style="background-image:url('<?php echo base_url();?>client_assets/images/search-icon.png');">
                    </li>
                    <!-- user login dropdown start-->
                    <?php
                    $client_session_data=$this->session->userdata(base_url().'login_session');
                    $client_name=$client_session_data['first_name'].' '.$client_session_data['last_name'];
  
                      ?>
                      <?php if(!empty($client_session_data)){?>
                    <li class="dropdown">
                        <a data-toggle="dropdown" class="dropdown-toggle" href="#">
                            <img alt="" src="<?php echo base_url();?>client_assets/images/avatar1_small.jpg">
                            <span class="username"><?php echo $client_name;?></span>
                            <b class="caret"></b>
                        </a>
                        <ul class="dropdown-menu extended logout">
                            <li><a href="#"><i class=" fa fa-suitcase"></i>Profile</a></li>
                            <li><a href="#"><i class="fa fa-cog"></i> Settings</a></li>
                            <li><a href="<?php echo base_url();?>client/logout"><i class="fa fa-key"></i> Log Out</a></li>
                        </ul>
                    </li>
                    <?php }?>
                    <!-- user login dropdown end -->
                </ul>
                <!--search & user info end-->
            </div>
        </header>