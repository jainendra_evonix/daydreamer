<style type="text/css">
.well{
    
    height: 243px;
    width: 201px;
}
.text-primary{

    color:red;
    padding-left: 114px;
}
.help-block{

    color:red;
}
</style>

<div class="container" id="content">
    <section>

        <?php echo $this->session->flashdata('successmsg') ?>
        <?php echo $this->session->flashdata('errormsg') ?>
        <div class="section-body contain-lg">
            <div class="row">

                <div class="col-md-12">
                    <div class="panel panel-default">

                        <div class="panel-body">

                            <h4 class="text-primary" style="font-size:19px;">Change Password<span class="helpnote">(<i class="fa fa-asterisk"></i> All fields are Mandatory)</span></h4>


                            <div class="col-md-8" style="margin-top:22px;">
                                <!--<i><small style="color: red">(All fields are mandatory)</small></i>-->
                                <form class="form" ng-submit="submitForm()" name="changePasswordForm" novalidate>
                                    <div class="card">
                                        <!--                            <div class="card-head style-primary">
                                                                        <header>Change Password</header>
                                                                    </div>-->
                                                                    <div class="card-body floating-label">
                                                                        <div class="row">

                                                                            <div class="col-md-4">
                                                                                <p class="side-label text-right"><strong>Old Password :</strong></p>

                                                                            </div>
                                                                            <div class="col-md-8">
                                                                                <!--<span>Old Password</span>-->
                                                                                <div class="form-group">
                                                                                    <input type="password" class="form-control" ng-model="client.oldpassword" name="oldpassword" placeholder="Please enter your old password"  id="Firstname2" required>
                                                                                    <label for="Firstname2"></label>
                                                                                    <span ng-show="submitted && changePasswordForm.oldpassword.$error.required"  class="help-block has-error ng-hide">Password is required.</span>
                                                                                    <span ng-show="errorOldPassword" class="help-block has-error ng-hide">{{errorOldPassword}}</span>
                                                                                </div>
                                                                            </div>

                                                                        </div>
                                                                        <div class="row">
                                                                            <div class="col-md-4">
                                                                                <p class="side-label text-right"><strong>New Password :</strong></p>

                                                                            </div>
                                                                            <div class="col-md-8">
                                                                                <div class="form-group">
                                                                                    <input type="password" id="password" class="form-control"  ng-model="client.password" name="password" placeholder="Please enter your new password"  id="Lastname2" required>
                                                                                    
                                                                                    <label for="Password"></label>
                                                                                    <span id="result"></span>
                                                                                    <span ng-show="submitted && changePasswordForm.password.$error.required"  class="help-block has-error ng-hide">New password is required.</span>
                                                                                    <!-- <span ng-show="submitted && changePasswordForm.password.$error.pattern"  class="help-block has-error ng-hide">Not valid password.</span> -->
                                                                                    <span ng-show="errorPassword" class="help-block has-error ng-hide">{{errorPassword}}</span>
                                                                                </div>
                                                                            </div>

                                                                        </div>
                                                                        <!--<br/>-->
                                                                        <div class="row">
                                                                            <div class="col-md-4">
                                                                                <p class="side-label text-right"><strong>Confirm Password :</strong></p>

                                                                            </div>
                                                                            <div class="col-md-8 ">
                                                                                <div class="form-group">
                                                                                    <input type="password" class="form-control" ng-model="client.confirm_password" name="confirm_password" placeholder="Please confirm your new password" valid-password-c="user.password"  required>
                                                                                    <label for="Lastname2"></label>
                                                                                    <span ng-show="submitted && changePasswordForm.confirm_password.$error.noMatch" class="help-block has-error ng-hide">Passwords do not match.</span>
                                                                                   <!--  <span ng-show="submitted && changePasswordForm.confirm_password.$error.pattern"  class="help-block has-error ng-hide">Not valid password.</span> -->
                                                                                    <span ng-show="errorConfirmPassword" class="help-block has-error ng-hide">{{errorConfirmPassword}}</span>
                                                                                </div>
                                                                            </div>

                                                                        </div>
                                                                        <div class="row">
                                                                            <div class="col-md-6 col-md-offset-4">
                                                                                <div class="card-actionbar">
                                                                                    <br>
                                                                                    <button type="submit" ng-click="submitted = true"  class="btn ink-reaction btn-raised btn-info">Change Password</button>
                                                                                </div>
                                                                            </div>

                                                                        </div><!--end .card-body -->
                                                                    </div>

                                                                </div>

                                                            </form>
                                                        </div>
                                                        <div class="col-md-4">
                                                            <div class="well well-lg">
                                                                <p>Enter a new password for your account. We highly recommend you create a unique password - one that you don't use for any other website. </p>
                                                                <p>Note : You can't reuse your old password once you change it.</p>
                                                                <!--<p><a href="#.">Learn more about choosing a smart password.</a></p>-->
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </section>
                            </div>