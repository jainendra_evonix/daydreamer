
  <style type="text/css">
.help-block{

    color:red;
}

</style>
  <div>

    <div class="container">

      <form class="form-signin" ng-submit="login()" name="loginForm" novalidate>
         <?php echo $this->session->flashdata('errormsg');?>
         <span id="message"></span>
        <h2 class="form-signin-heading"><strong>sign in now</strong></h2>
        <div class="login-wrap">
            <div class="user-login-info">
                <input type="text" class="form-control" name="email" ng-model="clntLogin.email" placeholder="Email ID" required ng-pattern='/^(([^<>()\[\]\.,;:\s@\"]+(\.[^<>()\[\]\.,;:\s@\"]+)*)|(\".+\"))@(([^<>()[\]\.,;:\s@\"]+\.)+[^<>()[\]\.,;:\s@\"]{2,})$/i' autofocus>
                <span ng-show="submitted && loginForm.email.$error.required"  class="help-block has-error ng-hide">EmailId is required.</span>
                <span ng-show="submitted && loginForm.email.$error.pattern"  class="help-block has-error ng-hide">EmailId is required.</span>
                <span ng-show="errorEmail" class="help-block has-error ng-hide">{{errorEmail}}</span>

                <input type="password" class="form-control" name="password" ng-model="clntLogin.password" placeholder="Password" required>
                <span ng-show="submitted && loginForm.password.$error.required"  class="help-block has-error ng-hide">Password is required.</span>
                <span ng-show="errorPassword" class="help-block has-error ng-hide">{{errorPassword}}</span>
            </div>
            <label class="checkbox">
                <input type="checkbox" value="remember-me" name="rememberMe"> Remember me
                <!-- <span ng-show="submitted && loginForm.rememberMe.$error.required"  class="help-block has-error ng-hide">Remember me is required.</span>
                <span ng-show="errorPassword" class="help-block has-error ng-hide">{{errorRemember}}</span> -->
                <span class="pull-right">
                    <a data-toggle="modal" href="#myModal"> Forgot Password?</a>

                </span>
            </label>
            <button class="btn btn-lg btn-login btn-block" ng-click="submitted = true" type="submit">Sign in</button>

            <div class="registration">
                Don't have an account yet?
                <a class="" href="<?php echo base_url();?>register">
                    Create an account
                </a>
            </div>

        </div>
</form>
          <!-- Modal -->
          <form class="form-signin" name="forget_password" ng-submit="forgetPassword()" novalidate>
          <div aria-hidden="true" aria-labelledby="myModalLabel" role="dialog" tabindex="-1" id="myModal" class="modal fade">
              <div class="modal-dialog">
                  <div class="modal-content">
                      <div class="modal-header">
                          <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                          <h4 class="modal-title">Forgot Password ?</h4>
                      </div>
                      
                      <div class="modal-body">
                        <div id="message2"></div>
                          <p>Enter your e-mail address below to reset your password.</p>
                          <input type="text" id="email" name="email"  placeholder="Email" autocomplete="off" class="form-control placeholder-no-fix" ng-model="forget.email" required>
                          <span ng-show="forgotpasswordsubmitted && forget_password.email.$invalid" class="help-block has-error">Valid Email ID is required.</span>
                      </div>
                      <div class="modal-footer">
                          <button data-dismiss="modal" class="btn btn-default" type="button">Cancel</button>
                          <button class="btn btn-success" ng-click="forgotpasswordsubmitted = true" type="submit">Submit</button>
                      </div>
                    
                  </div>
              </div>
          </div>
        </form>
          <!-- modal -->

      

    </div>

</div>
