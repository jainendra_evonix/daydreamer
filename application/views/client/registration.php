<style type="text/css">
.help-block{

    color:red;
}
select.form-control {
    border: 1px solid #eaeaea;
    border-radius: 5px;
    box-shadow: none;
    font-size: 12px !important;
    color: #777;
    margin-bottom: 15px;
}
</style>

<div>

    <div class="container">

      <form class="form-signin" ng-submit="registration()" name="registrationForm" novalidate>
        <?php echo $this->session->flashdata('successmsg');  ?>
        <?php echo $this->session->flashdata('errormsg');  ?>
        <h2 class="form-signin-heading"><strong>Sign Up</strong></h2>
        <div class="login-wrap">
            <p>Enter your personal details below</p>
            <input type="text" class="form-control" name='firstname' ng-model="client.firstname" placeholder="First Name" required ng-pattern="/^[a-zA-Z ]*$/" / autofocus>
            <span ng-show="submitted && registrationForm.firstname.$error.required"  class="help-block has-error ng-hide">Name is required.</span>
            <span ng-show="submitted && registrationForm.firstname.$error.pattern"  class="help-block has-error ng-hide">Valid name is required.</span>
            <span ng-show="errorFirstname" class="help-block has-error ng-hide">{{errorFirstname}}</span>

            <input type="text" class="form-control" name='lastname' ng-model="client.lastname"  placeholder="Last Name" required ng-pattern="/^[a-zA-Z ]*$/" / autofocus>
            <span ng-show="submitted && registrationForm.lastname.$error.required"  class="help-block has-error ng-hide">Lastname is required.</span>
            <span ng-show="submitted && registrationForm.lastname.$error.pattern"  class="help-block has-error ng-hide">Lastname is required.</span>
            <span ng-show="errorLastname" class="help-block has-error ng-hide">{{errorLastname}}</span>

            <input type="text" class="form-control" name='company_name' ng-model="client.company_name"  placeholder="Company Name" required ng-pattern="/^[a-zA-Z ]*$/" / autofocus>
            <span ng-show="submitted && registrationForm.company_name.$error.required"  class="help-block has-error ng-hide">Company Name is required.</span>
            <span ng-show="submitted && registrationForm.company_name.$error.pattern"  class="help-block has-error ng-hide">Company Name is required.</span>
            <span ng-show="errorLastname" class="help-block has-error ng-hide">{{errorCompanyname}}</span>

            <select class="form-control" name='country' ng-model='client.country'  placeholder="Country" required>
                <option value="" required="true">Select Country</option>
                <?php foreach($allCountries as $key){?>
                <option value="<?php echo $key->id;?>"><?php echo $key->country_name;?></option>
                <?php } ?>
            </select>

            <input type="text" class="form-control" name='city' ng-model='client.city' placeholder="City/Town" required autofocus>
            <span ng-show="submitted && registrationForm.city.$error.required"  class="help-block has-error ng-hide">City is required.</span>
            <span ng-show="errorCity" class="help-block has-error ng-hide">{{errorCity}}</span>

            <!-- <input type="text" class="form-control" name='country' ng-model='client.country'  placeholder="Country" required autofocus>
            <span ng-show="submitted && registrationForm.country.$error.required"  class="help-block has-error ng-hide">Country is required.</span>
            <span ng-show="errorCountry" class="help-block has-error ng-hide">{{errorCountry}}</span> -->

            <input type="text" class="form-control" name='telephone' ng-model='client.telephone' placeholder="Telephone" required ng-pattern="/^\+?\d{2}[- ]?\d{3}[- ]?\d{5}$/"/ autofocus>
            <span ng-show="submitted && registrationForm.telephone.$error.required"  class="help-block has-error ng-hide">Telephone is required.</span>
              <span ng-show="submitted && registrationForm.telephone.$error.pattern"  class="help-block has-error ng-hide">Telephone is required.</span>
            <span ng-show="errorTelephone" class="help-block has-error ng-hide">{{errorTelephone}}</span>

            <!-- <div class="radios">
                <label class="label_radio col-lg-6 col-sm-6" for="radio-01">
                    <input name="sample-radio" id="radio-01" value="1" type="radio" checked /> Male
                </label>
                <label class="label_radio col-lg-6 col-sm-6" for="radio-02">
                    <input name="sample-radio" id="radio-02" value="1" type="radio" /> Female
                </label>
            </div> -->

            <p> Enter your account details below</p>

            <input type="text" id="email" class="form-control" name='email' ng-model='client.email' placeholder="Email" required ng-pattern='/^(([^<>()\[\]\.,;:\s@\"]+(\.[^<>()\[\]\.,;:\s@\"]+)*)|(\".+\"))@(([^<>()[\]\.,;:\s@\"]+\.)+[^<>()[\]\.,;:\s@\"]{2,})$/i' autofocus onkeyup="checkEmail();">
            <span ng-show="submitted && registrationForm.email.$error.required"  class="help-block has-error ng-hide">Email is required.</span>
            <span ng-show="submitted && registrationForm.email.$error.pattern"  class="help-block has-error ng-hide">Email is required.</span>
            <span ng-show="errorEmail" class="help-block has-error ng-hide">{{errorEmail}}</span>
            <span id="showMsg" style="color:red;"></span>

            <input type="text" class="form-control" name='validate_email' ng-model='client.validate_email'  placeholder="Validate Email" required autofocus>
            <span ng-show="submitted && registrationForm.validate_email.$error.required"  class="help-block has-error ng-hide">validate email is required.</span>
            <span ng-show="errorValidateEmail" class="help-block has-error ng-hide">{{errorValidateEmail}}</span>
            <!-- <span><?php //echo $this->session->flashdata('errormsg');?></span> -->
            <span ng-show="client.email !== client.validate_email" class="help-block has-error ng-hide">Emails have to match!</span>
            <input type="password" class="form-control" name='password' ng-model='client.password'  placeholder="Password" required>
            <span ng-show="submitted && registrationForm.password.$error.required"  class="help-block has-error ng-hide">Password is required.</span>
            <span ng-show="errorPassword" class="help-block has-error ng-hide">{{errorPassword}}</span>


            <input type="password" class="form-control" name='validate_password' ng-model='client.validate_password' placeholder="Validate Password" required>
            <span ng-show="submitted && registrationForm.validate_password.$error.required"  class="help-block has-error ng-hide">validate password is required.</span>
            <span ng-show="errorValidatePassword" class="help-block has-error ng-hide">{{errorValidatePassword}}</span>
            <span ng-show="client.password !== client.validate_password" class="help-block has-error ng-hide">Passwords have to match!</span>
            <label class="checkbox">
                <input type="checkbox" name='agree' ng-model='client.agree' value="agree this condition" required> I agree to the Terms of Service and Privacy Policy
                <span ng-show="submitted && registrationForm.agree.$error.required"  class="help-block has-error ng-hide">Agreement field is required.</span>
                <span ng-show="errorAgreement" class="help-block has-error ng-hide">{{errorAgreement}}</span>
            </label>
            <button id="submitBtn" class="btn btn-lg btn-login btn-block" ng-click="submitted = true" type="submit">Submit</button>

            <div class="registration">
                Already Registered.
                <a class="" href="<?php echo base_url();?>client/login">
                    Login
                </a>
            </div>

        </div>

    </form>

</div>

</div>


