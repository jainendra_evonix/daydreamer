
<style type="text/css">
    #packagesblk .panel-heading {
        background-color: #474752;
        border-color: #474752;
        color: #fff;
        font-weight: 600;
        min-height: 135px;
        font-size: 16px;
    }
    #packagesblk .panel-heading .text-xxxl {
        font-size: 46px;
        font-weight: 600;
    }
    #packagesblk .panel-body button {
        background-color: transparent;
        border: 1px solid #aaa;
        width: 30px;
        height: 30px;
        border-radius: 50%;
        color: #777;
        transition: all .3s ease;
    }
    #packagesblk .panel-body button:hover {
        background-color: #777;
        color: #fff;
        border: 1px solid #777;
    }
    #packagesblk .panel {
        margin-bottom: 30px;
        box-shadow: 0 2px 15px rgba(0,0,0,0.1);
    }

    .help-block{

        color:red;
    }

    .help-block.has-error{

      display: block;
     position: absolute;
    top: 36px;
    }
    .input-group {
        width: 100%;
    }
    .form-signin input {
      margin-bottom: 30px !important;
    }

    .input-group-addon.date{

      position: absolute;
      right: 0;crs_
      padding: 11px;
      width: 40px;
    }
</style>
        <section class="container-fluid" style="margin-top:-21px;">
            <div class="row" id="topbanner" style="background-image: url('<?php echo base_url();?>client_assets/images/pexels-photo.jpg');">
                <div class="col-md-6 col-md-offset-3">
                    <h2 class="text-center">Packages</h2>
                   
                </div>
                 
            </div>
        </section>

        <!--main content start-->
        <section id="packagesblk" class="container">
            <section class="wrapper">
                <!-- page start-->
                <div class="row">
                    <div class="col-md-12">
                        <br>
                            
                          <?php if($this->session->flashdata('successmsg')){ ?>
                           <div class="alert alert-danger alert-dismissable">
    <a href="#" class="close" data-dismiss="alert" aria-label="close">×</a>
    <strong>Note!</strong>  <?php echo $this->session->flashdata('successmsg'); ?> 
  </div>
<?php } elseif ($this->session->flashdata('errormsg')) { ?>
  
         <div class="alert alert-warning alert-dismissable">
    <a href="#" class="close" data-dismiss="alert" aria-label="close">×</a>
    <strong>Warning!</strong><?php echo $this->session->flashdata('errormsg');?>
  </div>

<?php   } ?>
                      
                        <br>
                    </div>
                    
                    <div class="clearfix"></div>
                    <?php
                    if(!empty($packages)){

                    foreach($packages as $key){

                     $image=base_url().'package_images/'.$key->package_pic;
                    ?>
                    <div class="col-md-3">
                    <h4 class="text-center"><strong><?php echo $key->package_title;?></strong></h4>
                        <section class="panel text-center">
                            <div class="">
                               
                             <img src="<?php echo $image;?>">
                            <div class="price">
                                <h2><span class="text-lg">£</span> <span class="text-xxxl"><?php echo $key->price_amount;?></span></h2>
                            </div>

                            </div>
                            <div class="panel-body">

                            <table class="table table-condensed">
                        <tbody>
                          <tr>

                            <td width="33%"><strong><a href="javascript:void(0);" class="btn btn-primary btn-block btn-sm">Job Adds</a></strong></td>

                            <td width="33%"><strong><a href="javascript:void(0);" class="btn btn-success btn-block btn-sm">Save CV</a></strong></td>
                            <td width="33%"><strong><a href="javascript:void(0);" class="btn btn-warning btn-block btn-sm">Service</a></strong></td>
                          </tr>
                          <tr>
                            <td colspan="4"><strong><?php echo $key->totalJobAdds;?> </strong>Job Adds</td>
                          </tr>
                          <tr>
                           <?php

                             $service=$key->totalServices;
                           // echo '<pre>'; print_r($service);
                             $service_array=json_decode($service);
                            
                          //  print_r($service_array);

                             if(isset($service_array))
                             {
                              $serviceName='';
                             foreach($service_array as $serv){
                                 error_reporting(0);
                               $serviceName.=ucfirst($showService[$serv]).',';
                            

                             
                             
                          }
                          $serviceName=rtrim($serviceName,',');
                        }?>

                            <td colspan="4"><strong>Service Name</strong> <?php echo isset($service_array)&&isset($serviceName)?$serviceName:'No Service Name';?></td>

                            
                            
                          </tr>
                         <!--  <tr>
                            <td colspan="4"><strong><?php echo date('jS M Y',strtotime($key->validity_date))?></strong> Validity Date</td>
                          </tr> -->
                        </tbody>
                      </table>









                                <br>
                                <h4>Validity date: <?php echo date('jS M Y',strtotime($key->validity_date));?></h4>
                                <hr>
                                <a href="<?php echo base_url();?>client/viewPackage?package_id=<?php echo $key->id;?>" class="btn btn-success btn-xs pull-left" style="  font-size: 18px;height: 33px;margin-left: 65px;text-align: center;width: 114px;">View Details</a>
                                
                        </section>
                    </div>
                    <?php }}else{

                         echo "No packages found"; 

                    }?>
                   
                </div>
                <!-- page end-->

            </section>
        </section>
        <!--main content end-->





    <!--right sidebar start-->
    <div class="right-sidebar">
        <div class="search-row">
            <input type="text" placeholder="Search" class="form-control">
        </div>
        <div class="right-stat-bar">
            <ul class="right-side-accordion">
                <li class="widget-collapsible">
                    <a href="#" class="head widget-head red-bg active clearfix">
                        <span class="pull-left">work progress (5)</span>
                        <span class="pull-right widget-collapse"><i class="ico-minus"></i></span>
                    </a>
                    <ul class="widget-container">
                        <li>
                            <div class="prog-row side-mini-stat clearfix">
                                <div class="side-graph-info">
                                    <h4>Target sell</h4>
                                    <p>
                                        25%, Deadline 12 june 13
                                    </p>
                                </div>
                                <div class="side-mini-graph">
                                    <div class="target-sell">
                                    </div>
                                </div>
                            </div>
                            <div class="prog-row side-mini-stat">
                                <div class="side-graph-info">
                                    <h4>product delivery</h4>
                                    <p>
                                        55%, Deadline 12 june 13
                                    </p>
                                </div>
                                <div class="side-mini-graph">
                                    <div class="p-delivery">
                                        <div class="sparkline" data-type="bar" data-resize="true" data-height="30" data-width="90%" data-bar-color="#39b7ab" data-bar-width="5" data-data="[200,135,667,333,526,996,564,123,890,564,455]">
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="prog-row side-mini-stat">
                                <div class="side-graph-info payment-info">
                                    <h4>payment collection</h4>
                                    <p>
                                        25%, Deadline 12 june 13
                                    </p>
                                </div>
                                <div class="side-mini-graph">
                                    <div class="p-collection">
                                      <span class="pc-epie-chart" data-percent="45">
                                          <span class="percent"></span>
                                      </span>
                                  </div>
                              </div>
                          </div>
                          <div class="prog-row side-mini-stat">
                            <div class="side-graph-info">
                                <h4>delivery pending</h4>
                                <p>
                                    44%, Deadline 12 june 13
                                </p>
                            </div>
                            <div class="side-mini-graph">
                                <div class="d-pending">
                                </div>
                            </div>
                        </div>
                        <div class="prog-row side-mini-stat">
                            <div class="col-md-12">
                                <h4>total progress</h4>
                                <p>
                                    50%, Deadline 12 june 13
                                </p>
                                <div class="progress progress-xs mtop10">
                                    <div style="width: 50%" aria-valuemax="100" aria-valuemin="0" aria-valuenow="20" role="progressbar" class="progress-bar progress-bar-info">
                                        <span class="sr-only">50% Complete</span>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </li>
                </ul>
            </li>
            <li class="widget-collapsible">
                <a href="#" class="head widget-head terques-bg active clearfix">
                    <span class="pull-left">contact online (5)</span>
                    <span class="pull-right widget-collapse"><i class="ico-minus"></i></span>
                </a>
                <ul class="widget-container">
                    <li>
                        <div class="prog-row">
                            <div class="user-thumb">
                                <a href="#"><img src="images/avatar1_small.jpg" alt=""></a>
                            </div>
                            <div class="user-details">
                                <h4><a href="#">Jonathan Smith</a></h4>
                                <p>
                                    Work for fun
                                </p>
                            </div>
                            <div class="user-status text-danger">
                                <i class="fa fa-comments-o"></i>
                            </div>
                        </div>
                        <div class="prog-row">
                            <div class="user-thumb">
                                <a href="#"><img src="images/avatar1.jpg" alt=""></a>
                            </div>
                            <div class="user-details">
                                <h4><a href="#">Anjelina Joe</a></h4>
                                <p>
                                    Available
                                </p>
                            </div>
                            <div class="user-status text-success">
                                <i class="fa fa-comments-o"></i>
                            </div>
                        </div>
                        <div class="prog-row">
                            <div class="user-thumb">
                                <a href="#"><img src="images/chat-avatar2.jpg" alt=""></a>
                            </div>
                            <div class="user-details">
                                <h4><a href="#">John Doe</a></h4>
                                <p>
                                    Away from Desk
                                </p>
                            </div>
                            <div class="user-status text-warning">
                                <i class="fa fa-comments-o"></i>
                            </div>
                        </div>
                        <div class="prog-row">
                            <div class="user-thumb">
                                <a href="#"><img src="images/avatar1_small.jpg" alt=""></a>
                            </div>
                            <div class="user-details">
                                <h4><a href="#">Mark Henry</a></h4>
                                <p>
                                    working
                                </p>
                            </div>
                            <div class="user-status text-info">
                                <i class="fa fa-comments-o"></i>
                            </div>
                        </div>
                        <div class="prog-row">
                            <div class="user-thumb">
                                <a href="#"><img src="images/avatar1.jpg" alt=""></a>
                            </div>
                            <div class="user-details">
                                <h4><a href="#">Shila Jones</a></h4>
                                <p>
                                    Work for fun
                                </p>
                            </div>
                            <div class="user-status text-danger">
                                <i class="fa fa-comments-o"></i>
                            </div>
                        </div>
                        <p class="text-center">
                            <a href="#" class="view-btn">View all Contacts</a>
                        </p>
                    </li>
                </ul>
            </li>
            <li class="widget-collapsible">
                <a href="#" class="head widget-head purple-bg active">
                    <span class="pull-left"> recent activity (3)</span>
                    <span class="pull-right widget-collapse"><i class="ico-minus"></i></span>
                </a>
                <ul class="widget-container">
                    <li>
                        <div class="prog-row">
                            <div class="user-thumb rsn-activity">
                                <i class="fa fa-clock-o"></i>
                            </div>
                            <div class="rsn-details ">
                                <p class="text-muted">
                                    just now
                                </p>
                                <p>
                                    <a href="#">Jim Doe </a>Purchased new equipments for zonal office setup
                                </p>
                            </div>
                        </div>
                        <div class="prog-row">
                            <div class="user-thumb rsn-activity">
                                <i class="fa fa-clock-o"></i>
                            </div>
                            <div class="rsn-details ">
                                <p class="text-muted">
                                    2 min ago
                                </p>
                                <p>
                                    <a href="#">Jane Doe </a>Purchased new equipments for zonal office setup
                                </p>
                            </div>
                        </div>
                        <div class="prog-row">
                            <div class="user-thumb rsn-activity">
                                <i class="fa fa-clock-o"></i>
                            </div>
                            <div class="rsn-details ">
                                <p class="text-muted">
                                    1 day ago
                                </p>
                                <p>
                                    <a href="#">Jim Doe </a>Purchased new equipments for zonal office setup
                                </p>
                            </div>
                        </div>
                    </li>
                </ul>
            </li>
            <li class="widget-collapsible">
                <a href="#" class="head widget-head yellow-bg active">
                    <span class="pull-left"> shipment status</span>
                    <span class="pull-right widget-collapse"><i class="ico-minus"></i></span>
                </a>
                <ul class="widget-container">
                    <li>
                        <div class="col-md-12">
                            <div class="prog-row">
                                <p>
                                    Full sleeve baby wear (SL: 17665)
                                </p>
                                <div class="progress progress-xs mtop10">
                                    <div class="progress-bar progress-bar-success" role="progressbar" aria-valuenow="20" aria-valuemin="0" aria-valuemax="100" style="width: 40%">
                                        <span class="sr-only">40% Complete</span>
                                    </div>
                                </div>
                            </div>
                            <div class="prog-row">
                                <p>
                                    Full sleeve baby wear (SL: 17665)
                                </p>
                                <div class="progress progress-xs mtop10">
                                    <div class="progress-bar progress-bar-info" role="progressbar" aria-valuenow="20" aria-valuemin="0" aria-valuemax="100" style="width: 70%">
                                        <span class="sr-only">70% Completed</span>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </li>
                </ul>
            </li>
        </ul>
    </div>
</div>

