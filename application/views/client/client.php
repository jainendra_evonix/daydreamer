<style>
.help-block.has-error{

    color:red;
}



</style>

        <section class="container-fluid">
            <div class="row" id="topbanner" style="background-image: url('<?php echo base_url();?>client_assets/images/pexels-photo.jpg');margin-top:-20px;">
                <div class="col-md-6 col-md-offset-3">
                   
                    <h2 class="text-center">Client Online Form</h2>
                   <!--  <p class="text-center">Lorem ipsum dolor sit amet, consectetur adipisicing elit.</p> -->
                </div>
            </div>

            <!-- <div class="row" id="formsteps">
                <div class="col-md-10 col-md-offset-2">
                    <ul class="breadcrumbs-alt">
                        <li>
                            <a class="current" href="client.php">Basic Information</a>
                        </li>
                        <li>
                            <a href="education.php">Education</a>
                        </li>
                        <li>
                            <a href="pre-employment.php">Pre-Employment</a>
                        </li>
                        <li>
                            <a href="employment.php">Employment Information</a>
                        </li>
                        <li>
                            <a href="miscellaneous.php">Miscellaneous</a>
                        </li>
                        <li>
                            <a href="social-media.php">Social Media</a>
                        </li>
                    </ul>
                </div>
            </div> -->
        </section>
       
        <!--main content start-->
        <section id="" class="container">
            <section class="wrapper">
                <!-- page start-->
                <div class="row">
                    <div class="col-md-8 col-md-offset-2">
                        <br>
                         <?php echo $this->session->flashdata('successmsg');?>
                         <?php echo $this->session->flashdata('errormsg');?>
                        <h3 class="form-heading">Company Details and registered Head Office Address</h3>
                        <!-- <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit.</p> -->
                        <section class="panel">
                            <div class="panel-body">
                                
                                <form class="form-horizontal bucket-form" ng-submit="clientOnlineForm()" name="onlineForm" novalidate>
                                    <div class="form-group">
                                        <label class="col-sm-3 control-label">Company Name</label>
                                        <div class="col-sm-6">

                                            <input type="text" name="company" class="form-control" placeholder="Enter company name" ng-model="profile.company" value="" ng-pattern="/^[a-zA-Z ]*$/" required>
                                             <span ng-show="submitted && onlineForm.company.$error.required"  class="help-block has-error ng-hide">Company is required</span>
                                             <span ng-show="submitted && onlineForm.company.$error.pattern"  class="help-block has-error ng-hide">Company is required</span>
                                             <span ng-show="errorCompany" class="help-block has-error ng-hide">{{errorCompany}}</span>
                                        </div>
                                        <div class="col-md-3">
                                            <!-- <span class="help-block">Display additional instructions here.</span> -->
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-sm-3 control-label">Address Line 1</label>
                                        <div class="col-sm-6">
                                            <input type="text" name="address1" ng-model="profile.address1" class="form-control" placeholder="" required>
                                             <span ng-show="submitted && onlineForm.address1.$error.required"  class="help-block has-error ng-hide">Address1 is required</span>
                                             <span ng-show="errorAddress1" class="help-block has-error ng-hide">{{errorAddress1}}</span>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-sm-3 control-label">Address Line 2</label>
                                        <div class="col-sm-6">
                                            <input type="text" name="address2" ng-model="profile.address2" class="form-control" placeholder="">
                                            
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-sm-3 control-label">Address Line 3</label>
                                        <div class="col-sm-6">
                                            <input type="text" name="address3" ng-model="profile.address3"  class="form-control" placeholder="">
                                             
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label class="col-sm-3 control-label">Country</label>
                                        <div class="col-sm-6">
                                           <select class="form-control" name="country" ng-model="profile.country" required>
                                               <option value="" required="true">(Select Country)</option>
                                               <?php foreach($allCountries as $key){?>
                                              <option value="<?php echo $key->id;?>"><?php echo $key->country_name;?></option>
                                              <?php }?>
 
                                           </select>
                                            <span ng-show="submitted && onlineForm.country.$error.required"  class="help-block has-error ng-hide">Country is required</span>
                                            <span ng-show="submitted && onlineForm.country.$error.pattern"  class="help-block has-error ng-hide">Country is required</span>
                                             <span ng-show="errorCountry" class="help-block has-error ng-hide">{{errorCountry}}</span>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-sm-3 control-label">City</label>
                                        <div class="col-sm-6">
                                            <input type="text"  class="form-control" name="city" ng-model="profile.city" placeholder="Enter your city / county" ng-pattern="/^[a-zA-Z ]*$/" required>
                                            <span ng-show="submitted && onlineForm.city.$error.required"  class="help-block has-error ng-hide">City is required</span>
                                            <span ng-show="submitted && onlineForm.city.$error.pattern"  class="help-block has-error ng-hide">City is required</span>
                                             <span ng-show="errorCity" class="help-block has-error ng-hide">{{errorCity}}</span>
                                        </div>
                                    </div>
                                    

                                    <div class="form-group">
                                        <label class="col-sm-3 control-label">Postcode</label>
                                        <div class="col-sm-6">
                                            <input type="text"  class="form-control" name="postcode" ng-model="profile.postcode" placeholder="Enter your area postcode" ng-pattern="/^(0|[1-9][0-9]*)$/" required>
                                             <span ng-show="submitted && onlineForm.postcode.$error.required"  class="help-block has-error ng-hide">Postcode is required</span>
                                             <span ng-show="submitted && onlineForm.postcode.$error.pattern" class="help-block has-error ng-hide">Postcode is required</span>
                                             <span ng-show="errorPostcode" class="help-block has-error ng-hide">{{errorPostcode}}</span>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-sm-3 control-label">Company Number</label>
                                        <div class="col-sm-6">
                                            <input type="text"  class="form-control" name="company_number" ng-model="profile.company_number" placeholder="Enter company number" required>
                                             <span ng-show="submitted && onlineForm.company_number.$error.required"  class="help-block has-error ng-hide">Company Number is required</span>
                                             <span ng-show="errorCompanyNumber" class="help-block has-error ng-hide">{{errorCompanyNumber}}</span>
                                        </div>
                                        <div class="col-md-3">
                                            <!-- <span class="help-block">Display additional instructions here.</span> -->
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-sm-3 control-label">VAT Number</label>
                                        <div class="col-sm-6">
                                            <input type="text"  class="form-control" name="vat_number" ng-model="profile.vat_number" placeholder="Enter VAT number" required>
                                            <span ng-show="submitted && onlineForm.vat_number.$error.required"  class="help-block has-error ng-hide">VAT Number is required</span>
                                            <span ng-show="errorvat_number" class="help-block has-error ng-hide">{{errorvat_number}}</span>
                                        </div>
                                        <div class="col-md-3">
                                            <!-- <span class="help-block">Display additional instructions here.</span> -->
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-sm-3 control-label">Estimated Annual Turnover</label>
                                        <div class="col-sm-6">
                                            <input type="text"  class="form-control" name="estimated_turnover" ng-model="profile.estimated_turnover"  placeholder="Specify annual turnover"  required>
                                            <span ng-show="submitted && onlineForm.estimated_turnover.$error.required"  class="help-block has-error ng-hide">Estimated Turnover is required</span>
                                            <span ng-show="errorestimated_turnover" class="help-block has-error ng-hide">{{errorestimated_turnover}}</span>
                                        </div>
                                        <div class="col-md-3">
                                            <!-- <span class="help-block">Display additional instructions here.</span> -->
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-sm-3 control-label">Estimated number of staff</label>
                                        <div class="col-sm-6">
                                            <input type="text"  class="form-control" name="numberOfStaff" ng-model="profile.numberOfStaff" placeholder="Number of staff" ng-pattern="/^(0|[1-9][0-9]*)$/"  required>
                                            <span ng-show="submitted && onlineForm.numberOfStaff.$error.required"  class="help-block has-error ng-hide">Number of staff is required</span>
                                            <span ng-show="submitted && onlineForm.numberOfStaff.$error.pattern"  class="help-block has-error ng-hide">Number of staff is required</span>
                                            <span ng-show="errorenumberOfStaff" class="help-block has-error ng-hide">{{errornumberOfStaff}}</span>
                                        </div>
                                    </div>

                                    <hr>

                                    <h4>My Details</h4>
                                    <div class="form-group">
                                        <label class="col-sm-3 control-label">Job Title</label>
                                        <div class="col-sm-6">
                                            <input type="text"  class="form-control" name="title" ng-model="profile.title" placeholder="Enter your job title" ng-pattern="/^[a-zA-Z ]*$/" required>
                                            <span ng-show="submitted && onlineForm.title.$error.required"  class="help-block has-error ng-hide">Job Title is required</span>
                                            <span ng-show="submitted && onlineForm.title.$error.pattern"  class="help-block has-error ng-hide">Job Title is required</span>
                                            <span ng-show="erroretitle" class="help-block has-error ng-hide">{{errortitle}}</span>
                                        </div>
                                        <div class="col-md-3">
                                            <!-- <span class="help-block">Display additional instructions here.</span> -->
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-sm-3 control-label">Address Line 1</label>
                                        <div class="col-sm-6">
                                            <input type="text"  class="form-control" name="detailAddress1" ng-model="profile.detailAddress1" placeholder="" required>
                                            <span ng-show="submitted && onlineForm.detailAddress1.$error.required"  class="help-block has-error ng-hide">Address Line 1 is required</span>
                                            <span ng-show="erroredetailAddress1" class="help-block has-error ng-hide">{{errordetailAddress1}}</span>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-sm-3 control-label">Address Line 2</label>
                                        <div class="col-sm-6">
                                            <input type="text"  class="form-control" name="detailAddress2" ng-model="profile.detailAddress2" placeholder="" >
                                            
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-sm-3 control-label">Address Line 3</label>
                                        <div class="col-sm-6">
                                            <input type="text"  class="form-control" name="detailAddress3" ng-model="profile.detailAddress3" placeholder="" >
                                            
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-sm-3 control-label">Country</label>
                                        <div class="col-sm-6">
                                            <input type="text"  class="form-control" name="detailCountry" ng-model="profile.detailCountry" placeholder="Enter your country" required>
                                            <span ng-show="submitted && onlineForm.detailCountry.$error.required"  class="help-block has-error ng-hide">Country is required</span>
                                            <span ng-show="erroredetailCountry" class="help-block has-error ng-hide">{{errordetailCountry}}</span>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-sm-3 control-label">City</label>
                                        <div class="col-sm-6">
                                            <input type="text"  class="form-control" name="detailCity" ng-model="profile.detailCity" placeholder="Enter your city / county" required>
                                            <span ng-show="submitted && onlineForm.detailCity.$error.required"  class="help-block has-error ng-hide">City is required</span>
                                            <span ng-show="erroredetailCity" class="help-block has-error ng-hide">{{errordetailCity}}</span>
                                        </div>
                                    </div>
                                    

                                    <div class="form-group">
                                        <label class="col-sm-3 control-label">Postcode</label>
                                        <div class="col-sm-6">
                                            <input type="text"  class="form-control" name="detailPostcode" ng-model="profile.detailPostcode" placeholder="Enter your area postcode" ng-pattern="/^(0|[1-9][0-9]*)$/" required>
                                            <span ng-show="submitted && onlineForm.detailPostcode.$error.required"  class="help-block has-error ng-hide">Postcode is required</span>
                                            <span ng-show="submitted && onlineForm.detailPostcode.$error.pattern"  class="help-block has-error ng-hide">Postcode is required</span>
                                            <span ng-show="erroredetailPostcode" class="help-block has-error ng-hide">{{errordetailPostcode}}</span>
                                        </div>
                                    </div>

                                    <hr>

                                    <h4>A bit more about your company</h4>
                                    <div class="form-group">
                                        <label class="col-sm-3 control-label">Industry 1</label>
                                        <div class="col-sm-6">
                                            <input type="text"  class="form-control" name="industry1" ng-model="profile.industry1" placeholder="" required>
                                            <span ng-show="submitted && onlineForm.industry1.$error.required"  class="help-block has-error ng-hide">Industry 1 is required</span>
                                            <span ng-show="erroreindustry1" class="help-block has-error ng-hide">{{errorindustry1}}</span>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-sm-3 control-label">Industry 2</label>
                                        <div class="col-sm-6">
                                            <input type="text"  class="form-control" name="industry2" ng-model="profile.industry2" placeholder="" >
                                            
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-sm-3 control-label">Industry 3</label>
                                        <div class="col-sm-6">
                                            <input type="text"  class="form-control" name="industry3" ng-model="profile.industry3" placeholder="" >
                                             
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label class="col-sm-3 control-label">Interested in Apprentices?</label>
                                        <div class="col-sm-6 icheck">
                                            <div class="square single-row">
                                                <div class="">
                                                    <input tabindex="3" type="radio"  name="radio1" value="yes" ng-model="profile.radio1" required>
                                                    
                                                     
                                                    <label>Yes </label>
                                                </div>
                                                <div class="">
                                                    <input tabindex="3" type="radio"  name="radio1" value="no" ng-model="profile.radio1">
                                                   
                                                     
                                                    <label>No </label>
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label class="col-sm-3 control-label">CSR policy geared towards helping homeless?</label>
                                        <div class="col-sm-6">
                                            <input type="text"  class="form-control" name="csr_policy" ng-model="profile.csr_policy" placeholder="Enter your CSR Policy" required>
                                             <span ng-show="submitted && onlineForm.csr_policy.$error.required"  class="help-block has-error ng-hide">CSR Policy is required</span>
                                            <span ng-show="errorecsr_policy" class="help-block has-error ng-hide">{{errorcsr_policy}}</span>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-sm-3 control-label">Do you accept ex-offenders?</label>
                                        <div class="col-sm-6 icheck">
                                            <div class="square single-row">
                                                <div class="">
                                                    <input tabindex="3" type="radio"  name="radio2" value="yes" ng-model="profile.radio2">
                                                    <label>Yes </label>
                                                </div>
                                                <div class="">
                                                    <input tabindex="3" type="radio"  name="radio2" value="no" ng-model="profile.radio2">
                                                    <label>No </label>
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <div class="col-md-12">
                                            <button type="submit" ng-click="submitted = true" class="btn btn-info btn-sm pull-right"><strong>Save &amp; Finish <i class="fa fa-save"></i></strong></button>
                                        </div>
                                    </div>
                                </form>
                               
                            </div>
                        </section>
                    </div>
                </div>
                <!-- page end-->

            </section>
        </section>
        <!--main content end-->





    <!--right sidebar start-->
    <div class="right-sidebar">
        <div class="search-row">
            <input type="text" placeholder="Search" class="form-control">
        </div>
        <div class="right-stat-bar">
            <ul class="right-side-accordion">
                <li class="widget-collapsible">
                    <a href="#" class="head widget-head red-bg active clearfix">
                        <span class="pull-left">work progress (5)</span>
                        <span class="pull-right widget-collapse"><i class="ico-minus"></i></span>
                    </a>
                    <ul class="widget-container">
                        <li>
                            <div class="prog-row side-mini-stat clearfix">
                                <div class="side-graph-info">
                                    <h4>Target sell</h4>
                                    <p>
                                        25%, Deadline 12 june 13
                                    </p>
                                </div>
                                <div class="side-mini-graph">
                                    <div class="target-sell">
                                    </div>
                                </div>
                            </div>
                            <div class="prog-row side-mini-stat">
                                <div class="side-graph-info">
                                    <h4>product delivery</h4>
                                    <p>
                                        55%, Deadline 12 june 13
                                    </p>
                                </div>
                                <div class="side-mini-graph">
                                    <div class="p-delivery">
                                        <div class="sparkline" data-type="bar" data-resize="true" data-height="30" data-width="90%" data-bar-color="#39b7ab" data-bar-width="5" data-data="[200,135,667,333,526,996,564,123,890,564,455]">
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="prog-row side-mini-stat">
                                <div class="side-graph-info payment-info">
                                    <h4>payment collection</h4>
                                    <p>
                                        25%, Deadline 12 june 13
                                    </p>
                                </div>
                                <div class="side-mini-graph">
                                    <div class="p-collection">
                                      <span class="pc-epie-chart" data-percent="45">
                                          <span class="percent"></span>
                                      </span>
                                  </div>
                              </div>
                          </div>
                          <div class="prog-row side-mini-stat">
                            <div class="side-graph-info">
                                <h4>delivery pending</h4>
                                <p>
                                    44%, Deadline 12 june 13
                                </p>
                            </div>
                            <div class="side-mini-graph">
                                <div class="d-pending">
                                </div>
                            </div>
                        </div>
                        <div class="prog-row side-mini-stat">
                            <div class="col-md-12">
                                <h4>total progress</h4>
                                <p>
                                    50%, Deadline 12 june 13
                                </p>
                                <div class="progress progress-xs mtop10">
                                    <div style="width: 50%" aria-valuemax="100" aria-valuemin="0" aria-valuenow="20" role="progressbar" class="progress-bar progress-bar-info">
                                        <span class="sr-only">50% Complete</span>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </li>
                </ul>
            </li>
            <li class="widget-collapsible">
                <a href="#" class="head widget-head terques-bg active clearfix">
                    <span class="pull-left">contact online (5)</span>
                    <span class="pull-right widget-collapse"><i class="ico-minus"></i></span>
                </a>
                <ul class="widget-container">
                    <li>
                        <div class="prog-row">
                            <div class="user-thumb">
                                <a href="#"><img src="images/avatar1_small.jpg" alt=""></a>
                            </div>
                            <div class="user-details">
                                <h4><a href="#">Jonathan Smith</a></h4>
                                <p>
                                    Work for fun
                                </p>
                            </div>
                            <div class="user-status text-danger">
                                <i class="fa fa-comments-o"></i>
                            </div>
                        </div>
                        <div class="prog-row">
                            <div class="user-thumb">
                                <a href="#"><img src="images/avatar1.jpg" alt=""></a>
                            </div>
                            <div class="user-details">
                                <h4><a href="#">Anjelina Joe</a></h4>
                                <p>
                                    Available
                                </p>
                            </div>
                            <div class="user-status text-success">
                                <i class="fa fa-comments-o"></i>
                            </div>
                        </div>
                        <div class="prog-row">
                            <div class="user-thumb">
                                <a href="#"><img src="images/chat-avatar2.jpg" alt=""></a>
                            </div>
                            <div class="user-details">
                                <h4><a href="#">John Doe</a></h4>
                                <p>
                                    Away from Desk
                                </p>
                            </div>
                            <div class="user-status text-warning">
                                <i class="fa fa-comments-o"></i>
                            </div>
                        </div>
                        <div class="prog-row">
                            <div class="user-thumb">
                                <a href="#"><img src="images/avatar1_small.jpg" alt=""></a>
                            </div>
                            <div class="user-details">
                                <h4><a href="#">Mark Henry</a></h4>
                                <p>
                                    working
                                </p>
                            </div>
                            <div class="user-status text-info">
                                <i class="fa fa-comments-o"></i>
                            </div>
                        </div>
                        <div class="prog-row">
                            <div class="user-thumb">
                                <a href="#"><img src="images/avatar1.jpg" alt=""></a>
                            </div>
                            <div class="user-details">
                                <h4><a href="#">Shila Jones</a></h4>
                                <p>
                                    Work for fun
                                </p>
                            </div>
                            <div class="user-status text-danger">
                                <i class="fa fa-comments-o"></i>
                            </div>
                        </div>
                        <p class="text-center">
                            <a href="#" class="view-btn">View all Contacts</a>
                        </p>
                    </li>
                </ul>
            </li>
            <li class="widget-collapsible">
                <a href="#" class="head widget-head purple-bg active">
                    <span class="pull-left"> recent activity (3)</span>
                    <span class="pull-right widget-collapse"><i class="ico-minus"></i></span>
                </a>
                <ul class="widget-container">
                    <li>
                        <div class="prog-row">
                            <div class="user-thumb rsn-activity">
                                <i class="fa fa-clock-o"></i>
                            </div>
                            <div class="rsn-details ">
                                <p class="text-muted">
                                    just now
                                </p>
                                <p>
                                    <a href="#">Jim Doe </a>Purchased new equipments for zonal office setup
                                </p>
                            </div>
                        </div>
                        <div class="prog-row">
                            <div class="user-thumb rsn-activity">
                                <i class="fa fa-clock-o"></i>
                            </div>
                            <div class="rsn-details ">
                                <p class="text-muted">
                                    2 min ago
                                </p>
                                <p>
                                    <a href="#">Jane Doe </a>Purchased new equipments for zonal office setup
                                </p>
                            </div>
                        </div>
                        <div class="prog-row">
                            <div class="user-thumb rsn-activity">
                                <i class="fa fa-clock-o"></i>
                            </div>
                            <div class="rsn-details ">
                                <p class="text-muted">
                                    1 day ago
                                </p>
                                <p>
                                    <a href="#">Jim Doe </a>Purchased new equipments for zonal office setup
                                </p>
                            </div>
                        </div>
                    </li>
                </ul>
            </li>
            <li class="widget-collapsible">
                <a href="#" class="head widget-head yellow-bg active">
                    <span class="pull-left"> shipment status</span>
                    <span class="pull-right widget-collapse"><i class="ico-minus"></i></span>
                </a>
                <ul class="widget-container">
                    <li>
                        <div class="col-md-12">
                            <div class="prog-row">
                                <p>
                                    Full sleeve baby wear (SL: 17665)
                                </p>
                                <div class="progress progress-xs mtop10">
                                    <div class="progress-bar progress-bar-success" role="progressbar" aria-valuenow="20" aria-valuemin="0" aria-valuemax="100" style="width: 40%">
                                        <span class="sr-only">40% Complete</span>
                                    </div>
                                </div>
                            </div>
                            <div class="prog-row">
                                <p>
                                    Full sleeve baby wear (SL: 17665)
                                </p>
                                <div class="progress progress-xs mtop10">
                                    <div class="progress-bar progress-bar-info" role="progressbar" aria-valuenow="20" aria-valuemin="0" aria-valuemax="100" style="width: 70%">
                                        <span class="sr-only">70% Completed</span>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </li>
                </ul>
            </li>
        </ul>
    </div>
</div>

