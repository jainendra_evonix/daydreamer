<?php include 'header.php' ?>

        <section class="container-fluid">
            <div class="row" id="topbanner">
                <div class="col-md-6 col-md-offset-3">
                    <h2 class="text-center">Customer Online Form</h2>
                    <p class="text-center">Lorem ipsum dolor sit amet, consectetur adipisicing elit.</p>
                </div>
            </div>

            <div class="row" id="formsteps">
                <div class="col-md-10 col-md-offset-2">
                    <ul class="breadcrumbs-alt">
                        <li>
                            <a href="client.php">Basic Information</a>
                        </li>
                        <li>
                            <a href="education.php">Education</a>
                        </li>
                        <li>
                            <a class="current" href="pre-employment.php">Pre-Employment</a>
                        </li>
                        <li>
                            <a href="employment.php">Employment Information</a>
                        </li>
                        <li>
                            <a href="miscellaneous.php">Miscellaneous</a>
                        </li>
                        <li>
                            <a href="social-media.php">Social Media</a>
                        </li>
                    </ul>
                </div>
            </div>
        </section>

        <!--main content start-->
        <section id="" class="container">
            <section class="wrapper">
                <!-- page start-->
                <div class="row">
                    <div class="col-md-8 col-md-offset-2">
                        <h3 class="form-heading">Pre-Employment Details</h3>
                        <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit.</p>

                        <section class="panel">
                            <div class="panel-body">
                                <form class="form-horizontal bucket-form" method="get" action="employment.php">
                                    <div class="form-group">
                                        <label class="col-sm-3 control-label">Currently Employed</label>
                                        <div class="col-sm-6 icheck">
                                            <div class="square single-row">
                                                <div class="">
                                                    <input tabindex="3" type="radio"  name="demo-radio">
                                                    <label>Yes </label>
                                                </div>
                                                <div class="">
                                                    <input tabindex="3" type="radio"  name="demo-radio">
                                                    <label>No </label>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="control-label col-md-3">Employment Duration</label>
                                        <div class="col-sm-2 padingr">
                                            <select class="form-control m-bot15">
                                                <option disabled="disabled" selected="selected">Year</option>
                                                <option>2016</option>
                                                <option>2015</option>
                                                <option>2014</option>
                                                <option>2013</option>
                                                <option>2012</option>
                                                <option>2011</option>
                                                <option>2010</option>
                                                <option>2009</option>
                                                <option>2008</option>
                                                <option>2007</option>
                                                <option>2006</option>
                                                <option>2005</option>
                                                <option>2004</option>
                                                <option>2003</option>
                                                <option>2002</option>
                                                <option>2001</option>
                                                <option>2000</option>
                                                <option>1999</option>
                                            </select>
                                        </div>
                                        <div class="col-sm-2 padingr">
                                            <select class="form-control m-bot15">
                                                <option disabled="disabled" selected="selected">Month</option>
                                                <option>Jan</option>
                                                <option>Feb</option>
                                                <option>Mar</option>
                                                <option>Apr</option>
                                                <option>May</option>
                                                <option>June</option>
                                                <option>July</option>
                                                <option>Aug</option>
                                                <option>Sept</option>
                                                <option>Oct</option>
                                                <option>Nov</option>
                                                <option>Dec</option>
                                            </select>
                                        </div>
                                        <div class="col-sm-1 text-center">
                                            <span class="text-center">To</span>
                                        </div>
                                        <div class="col-sm-2 padingl">
                                            <select class="form-control m-bot15">
                                                <option disabled="disabled" selected="selected">Year</option>
                                                <option>2016</option>
                                                <option>2015</option>
                                                <option>2014</option>
                                                <option>2013</option>
                                                <option>2012</option>
                                                <option>2011</option>
                                                <option>2010</option>
                                                <option>2009</option>
                                                <option>2008</option>
                                                <option>2007</option>
                                                <option>2006</option>
                                                <option>2005</option>
                                                <option>2004</option>
                                                <option>2003</option>
                                                <option>2002</option>
                                                <option>2001</option>
                                                <option>2000</option>
                                                <option>1999</option>
                                            </select>
                                        </div>
                                        <div class="col-sm-2 padingl">
                                            <select class="form-control m-bot15">
                                                <option disabled="disabled" selected="selected">Month</option>
                                                <option>Jan</option>
                                                <option>Feb</option>
                                                <option>Mar</option>
                                                <option>Apr</option>
                                                <option>May</option>
                                                <option>June</option>
                                                <option>July</option>
                                                <option>Aug</option>
                                                <option>Sept</option>
                                                <option>Oct</option>
                                                <option>Nov</option>
                                                <option>Dec</option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-sm-3 control-label">Present Income</label>
                                        <div class="col-sm-3">
                                            <div class="input-group m-bot15">
                                                <span class="input-group-addon"><i class="fa fa-gbp"></i></span>
                                                <input type="text" class="form-control" placeholder="00.00 K">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-sm-3 control-label">Desired Income</label>
                                        <div class="col-sm-3">
                                            <div class="input-group m-bot15">
                                                <span class="input-group-addon"><i class="fa fa-gbp"></i></span>
                                                <input type="text" class="form-control" placeholder="00.00 K">
                                            </div>
                                        </div>
                                    </div>
                                    <hr>
                                    <div class="form-group">
                                        <label class="col-sm-3 control-label">Obtaining Work / Better Work</label>
                                        <div class="col-sm-6 icheck">
                                            <div class="square single-row">
                                                <div class="">
                                                    <input tabindex="3" type="radio"  name="demo-radio">
                                                    <label>Yes </label>
                                                </div>
                                                <div class="">
                                                    <input tabindex="3" type="radio"  name="demo-radio">
                                                    <label>No </label>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-3">
                                            <span class="help-block">Display additional instructions here.</span>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-sm-3 control-label">Currently Actively applying</label>
                                        <div class="col-sm-6 icheck">
                                            <div class="square single-row">
                                                <div class="">
                                                    <input tabindex="3" type="radio"  name="demo-radio1">
                                                    <label>Yes </label>
                                                </div>
                                                <div class="">
                                                    <input tabindex="3" type="radio"  name="demo-radio1">
                                                    <label>No </label>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-sm-3 control-label">If yes, where?</label>
                                        <div class="col-sm-6 input-group m-bot15">
                                                <input type="text" class="form-control">
                                                <span class="input-group-btn">
                                                    <button class="btn btn-success" type="button">Add +</button>
                                                </span>
                                        </div>
                                    </div>
                                    <hr>
                                    <div class="form-group">
                                        <label class="col-sm-3 control-label">Undertaken Voluntary / Charity work</label>
                                        <div class="col-sm-6 icheck">
                                            <div class="square single-row">
                                                <div class="">
                                                    <input tabindex="3" type="radio"  name="demo-radio2">
                                                    <label>Yes </label>
                                                </div>
                                                <div class="">
                                                    <input tabindex="3" type="radio"  name="demo-radio2">
                                                    <label>No </label>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-3">
                                            <span class="help-block">Display additional instructions here.</span>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-sm-3 control-label">Interested in Volunteering / Assisting charities</label>
                                        <div class="col-sm-6 icheck">
                                            <div class="square single-row">
                                                <div class="">
                                                    <input tabindex="3" type="radio"  name="demo-radio3">
                                                    <label>Yes </label>
                                                </div>
                                                <div class="">
                                                    <input tabindex="3" type="radio"  name="demo-radio3">
                                                    <label>No </label>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-sm-3 control-label">If yes, what sort of organisations?</label>
                                        <div class="col-sm-6">
                                            <select multiple name="e9" id="e9" style="width:100%;" class="populate m-bot15">
                                                <optgroup label="select organisations">
                                                    <option value="1">Armed forces</option>
                                                    <option value="2">Hospices</option>
                                                    <option value="3">Aged</option>
                                                    <option value="4">Cultural</option>
                                                    <option value="5">International</option>
                                                    <option value="6">Education</option>
                                                    <option value="7">Health</option>
                                                    <option value="8">Hospitals</option>
                                                    <option value="9">Family Welfare</option>
                                                    <option value="10">Animals</option>
                                                    <option value="11">Ex-offenders</option>
                                                    <option value="12">Sight/Hearing</option>
                                                </optgroup>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <div class="col-md-9">
                                            <button type="submit" class="btn btn-info pull-right">Save &amp; Continue</button>
                                        </div>
                                    </div>
                                    
                                </form>
                            </div>
                        </section>
                    </div>
                </div>
                <!-- page end-->

            </section>
        </section>
        <!--main content end-->





    <!--right sidebar start-->
    <div class="right-sidebar">
        <div class="search-row">
            <input type="text" placeholder="Search" class="form-control">
        </div>
        <div class="right-stat-bar">
            <ul class="right-side-accordion">
                <li class="widget-collapsible">
                    <a href="#" class="head widget-head red-bg active clearfix">
                        <span class="pull-left">work progress (5)</span>
                        <span class="pull-right widget-collapse"><i class="ico-minus"></i></span>
                    </a>
                    <ul class="widget-container">
                        <li>
                            <div class="prog-row side-mini-stat clearfix">
                                <div class="side-graph-info">
                                    <h4>Target sell</h4>
                                    <p>
                                        25%, Deadline 12 june 13
                                    </p>
                                </div>
                                <div class="side-mini-graph">
                                    <div class="target-sell">
                                    </div>
                                </div>
                            </div>
                            <div class="prog-row side-mini-stat">
                                <div class="side-graph-info">
                                    <h4>product delivery</h4>
                                    <p>
                                        55%, Deadline 12 june 13
                                    </p>
                                </div>
                                <div class="side-mini-graph">
                                    <div class="p-delivery">
                                        <div class="sparkline" data-type="bar" data-resize="true" data-height="30" data-width="90%" data-bar-color="#39b7ab" data-bar-width="5" data-data="[200,135,667,333,526,996,564,123,890,564,455]">
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="prog-row side-mini-stat">
                                <div class="side-graph-info payment-info">
                                    <h4>payment collection</h4>
                                    <p>
                                        25%, Deadline 12 june 13
                                    </p>
                                </div>
                                <div class="side-mini-graph">
                                    <div class="p-collection">
                                      <span class="pc-epie-chart" data-percent="45">
                                          <span class="percent"></span>
                                      </span>
                                  </div>
                              </div>
                          </div>
                          <div class="prog-row side-mini-stat">
                            <div class="side-graph-info">
                                <h4>delivery pending</h4>
                                <p>
                                    44%, Deadline 12 june 13
                                </p>
                            </div>
                            <div class="side-mini-graph">
                                <div class="d-pending">
                                </div>
                            </div>
                        </div>
                        <div class="prog-row side-mini-stat">
                            <div class="col-md-12">
                                <h4>total progress</h4>
                                <p>
                                    50%, Deadline 12 june 13
                                </p>
                                <div class="progress progress-xs mtop10">
                                    <div style="width: 50%" aria-valuemax="100" aria-valuemin="0" aria-valuenow="20" role="progressbar" class="progress-bar progress-bar-info">
                                        <span class="sr-only">50% Complete</span>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </li>
                </ul>
            </li>
            <li class="widget-collapsible">
                <a href="#" class="head widget-head terques-bg active clearfix">
                    <span class="pull-left">contact online (5)</span>
                    <span class="pull-right widget-collapse"><i class="ico-minus"></i></span>
                </a>
                <ul class="widget-container">
                    <li>
                        <div class="prog-row">
                            <div class="user-thumb">
                                <a href="#"><img src="images/avatar1_small.jpg" alt=""></a>
                            </div>
                            <div class="user-details">
                                <h4><a href="#">Jonathan Smith</a></h4>
                                <p>
                                    Work for fun
                                </p>
                            </div>
                            <div class="user-status text-danger">
                                <i class="fa fa-comments-o"></i>
                            </div>
                        </div>
                        <div class="prog-row">
                            <div class="user-thumb">
                                <a href="#"><img src="images/avatar1.jpg" alt=""></a>
                            </div>
                            <div class="user-details">
                                <h4><a href="#">Anjelina Joe</a></h4>
                                <p>
                                    Available
                                </p>
                            </div>
                            <div class="user-status text-success">
                                <i class="fa fa-comments-o"></i>
                            </div>
                        </div>
                        <div class="prog-row">
                            <div class="user-thumb">
                                <a href="#"><img src="images/chat-avatar2.jpg" alt=""></a>
                            </div>
                            <div class="user-details">
                                <h4><a href="#">John Doe</a></h4>
                                <p>
                                    Away from Desk
                                </p>
                            </div>
                            <div class="user-status text-warning">
                                <i class="fa fa-comments-o"></i>
                            </div>
                        </div>
                        <div class="prog-row">
                            <div class="user-thumb">
                                <a href="#"><img src="images/avatar1_small.jpg" alt=""></a>
                            </div>
                            <div class="user-details">
                                <h4><a href="#">Mark Henry</a></h4>
                                <p>
                                    working
                                </p>
                            </div>
                            <div class="user-status text-info">
                                <i class="fa fa-comments-o"></i>
                            </div>
                        </div>
                        <div class="prog-row">
                            <div class="user-thumb">
                                <a href="#"><img src="images/avatar1.jpg" alt=""></a>
                            </div>
                            <div class="user-details">
                                <h4><a href="#">Shila Jones</a></h4>
                                <p>
                                    Work for fun
                                </p>
                            </div>
                            <div class="user-status text-danger">
                                <i class="fa fa-comments-o"></i>
                            </div>
                        </div>
                        <p class="text-center">
                            <a href="#" class="view-btn">View all Contacts</a>
                        </p>
                    </li>
                </ul>
            </li>
            <li class="widget-collapsible">
                <a href="#" class="head widget-head purple-bg active">
                    <span class="pull-left"> recent activity (3)</span>
                    <span class="pull-right widget-collapse"><i class="ico-minus"></i></span>
                </a>
                <ul class="widget-container">
                    <li>
                        <div class="prog-row">
                            <div class="user-thumb rsn-activity">
                                <i class="fa fa-clock-o"></i>
                            </div>
                            <div class="rsn-details ">
                                <p class="text-muted">
                                    just now
                                </p>
                                <p>
                                    <a href="#">Jim Doe </a>Purchased new equipments for zonal office setup
                                </p>
                            </div>
                        </div>
                        <div class="prog-row">
                            <div class="user-thumb rsn-activity">
                                <i class="fa fa-clock-o"></i>
                            </div>
                            <div class="rsn-details ">
                                <p class="text-muted">
                                    2 min ago
                                </p>
                                <p>
                                    <a href="#">Jane Doe </a>Purchased new equipments for zonal office setup
                                </p>
                            </div>
                        </div>
                        <div class="prog-row">
                            <div class="user-thumb rsn-activity">
                                <i class="fa fa-clock-o"></i>
                            </div>
                            <div class="rsn-details ">
                                <p class="text-muted">
                                    1 day ago
                                </p>
                                <p>
                                    <a href="#">Jim Doe </a>Purchased new equipments for zonal office setup
                                </p>
                            </div>
                        </div>
                    </li>
                </ul>
            </li>
            <li class="widget-collapsible">
                <a href="#" class="head widget-head yellow-bg active">
                    <span class="pull-left"> shipment status</span>
                    <span class="pull-right widget-collapse"><i class="ico-minus"></i></span>
                </a>
                <ul class="widget-container">
                    <li>
                        <div class="col-md-12">
                            <div class="prog-row">
                                <p>
                                    Full sleeve baby wear (SL: 17665)
                                </p>
                                <div class="progress progress-xs mtop10">
                                    <div class="progress-bar progress-bar-success" role="progressbar" aria-valuenow="20" aria-valuemin="0" aria-valuemax="100" style="width: 40%">
                                        <span class="sr-only">40% Complete</span>
                                    </div>
                                </div>
                            </div>
                            <div class="prog-row">
                                <p>
                                    Full sleeve baby wear (SL: 17665)
                                </p>
                                <div class="progress progress-xs mtop10">
                                    <div class="progress-bar progress-bar-info" role="progressbar" aria-valuenow="20" aria-valuemin="0" aria-valuemax="100" style="width: 70%">
                                        <span class="sr-only">70% Completed</span>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </li>
                </ul>
            </li>
        </ul>
    </div>
</div>

<?php include 'footer.php' ?>