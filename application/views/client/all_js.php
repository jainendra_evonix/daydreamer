<!--Core js-->
<script src="<?php echo base_url();?>client_assets/js/client_js/jquery.js"></script>
<script src="<?php echo base_url();?>client_assets/js/client_js/jquery-1.8.3.min.js"></script>
<script src="<?php echo base_url();?>client_assets/bs3/js/bootstrap.min.js"></script>
<script src="<?php echo base_url();?>client_assets/js/client_js/jquery-ui-1.9.2.custom.min.js"></script>

<script src="<?php echo base_url();?>client_assets/js/client_js/ui-bootstrap-tpls-0.14.3.min.js"></script>
<!-- <script src="js/bootstrap-switch.js"><<?php echo base_url();?>assets//script> -->
<script src="//cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.3.0/js/bootstrap-datepicker.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/rateYo/2.2.0/jquery.rateyo.min.js"></script>


<!--common script init for all pages-->
<script src="<?php echo base_url();?>client_assets/js/client_js/scripts.js"></script>

<!-- <script src="js/toggle-init.js"></script> -->

<script type="text/javascript" src="<?php echo base_url(); ?>web_assets/js/jquery.smartmenus.js"></script>
<!-- SmartMenus jQuery Bootstrap Addon -->
<script type="text/javascript" src="<?php echo base_url(); ?>web_assets/js/jquery.smartmenus.bootstrap.js"></script>

<script src="<?php echo base_url(); ?>web_assets/js/main.js"></script> <!-- Resource jQuery -->

<script src="<?php echo base_url(); ?>web_assets/owl/owl.carousel.min.js"></script>
<!--<script type="text/javascript" src="<?php //echo base_url();?>client_assets/js/client_js/ckeditor/ckeditor.js"></script>-->
<script src="//cdn.ckeditor.com/4.5.6/standard/ckeditor.js"></script> 
<script type="text/javascript" src="<?php echo base_url();?>client_assets/js/client_js/data-tables/jquery.dataTables.js"></script>
<script type="text/javascript" src="<?php echo base_url();?>client_assets/js/client_js/data-tables/DT_bootstrap.js"></script>
<script type="text/javascript" src="<?php echo base_url();?>client_assets/js/client_js/dynamic_table_init.js"></script>
<script src="<?php echo base_url();?>client_assets/js/client_js/canvasjs.min.js"></script>
<script src="<?php echo base_url();?>client_assets/js/client_js/advanced-form.js"></script>
<script src="<?php echo base_url();?>client_assets/js/client_js/jquery.wallform.js"></script>
<!--<script src="<?php echo base_url();?>client_assets/js/client_js/jquery-ui.min.js"></script>-->



<script src="<?php echo base_url();?>client_assets/js/client_js/select2/select2.js"></script>
 <script src="https://cdn.datatables.net/1.10.15/js/jquery.dataTables.min.js"></script>
 <script type="text/javascript" src="<?php echo base_url();?>client_assets/js/client_js/jquery.cropit.js"></script>
<!-- <script src="<?php //echo base_url();?>client_assets/js/client_js/jquery.cropit.js"></script>-->

<script>
$(document).ready(function(){
	$('.owl-complogo').owlCarousel({
	    loop:true,
	    dots:false,
	    margin:50,
	    autoplay:true,
		autoplayTimeout:5000,
		autoplayHoverPause:false,
		nav:true,
	    navText:[
	        "<i class='fa fa-angle-left fa-2x'></i>",
	        "<i class='fa fa-angle-right fa-2x'></i>"
	    ],
	    responsiveClass:true,
	    responsive:{
	        0:{
	            items:3,
	            nav:true
	        },
	        600:{
	            items:4,
	            nav:true
	        },
	        1000:{
	            items:6,
	            nav:true
	        }
	    }
	});


  // Add smooth scrolling to all links in navbar + footer link
  $("a[href='#myPage']").on('click', function(event) {

   // Make sure this.hash has a value before overriding default behavior
  if (this.hash !== "") {

    // Prevent default anchor click behavior
    event.preventDefault();

    // Store hash
    var hash = this.hash;

    // Using jQuery's animate() method to add smooth page scroll
    // The optional number (900) specifies the number of milliseconds it takes to scroll to the specified area
    $('html, body').animate({
      scrollTop: $(hash).offset().top
    }, 900, function(){

      // Add hash (#) to URL when done scrolling (default click behavior)
      window.location.hash = hash;
      });
    } // End if
  });




})


//function for deactivating client.
function confirmDeactivate(){
var result=confirm("Do you really want to Deactivate?");
if(result){
return true;

}else{

return false;
}

}

//function for activating client.
function confirmActivate(){
var result=confirm("Do you really want to Activate?");
if(result){
return true;

}else{

return false;
}


} 

//function for deactivating client credential
function confirmClientDisable(){
var result=confirm("Do you really want to Deactivate Client?");
if(result){
return true;

}else{

return false;
}


}

//function for activating client credential
function confirmClientActivate(){
var result=confirm("Do you really want to activate Client?");
if(result){
return true;

}else{

return false;
}


}

$(document).ready(function(){

$('#date_of_birth1').datepicker({

            format: "yyyy/mm/dd",
            autoclose:true
        });


        

        $('#date_of_birth2').datepicker({

            format: "yyyy/mm/dd",
            autoclose:true 


        });

	
});

</script>



</body>
</html>