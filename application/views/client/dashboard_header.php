

<body ng-app="postApp" ng-controller="postController as ctrl">

<section id="container">
      <header class="header clearfix" id="adminheader">
          <!--logo start-->
          <div class="brand">

              <a href="index.html" class="logo">
                  <!-- <img src="images/logo.png" alt=""> -->
                  DAY DREAMER
              </a>
          </div>
          <!--logo end-->


          <div class="horizontal-menu navbar-collapse collapse ">
           <!--  <ul class="nav navbar-nav">
                <!-- <li><a href="#">Home</a></li> -->
                <!-- <li class="active"><a href="#">Jobs</a></li> -->
                <!-- <li class="dropdown">
                    <a data-toggle="dropdown" data-hover="dropdown" class="dropdown-toggle" href="#">Recruiters <b class=" fa fa-angle-down"></b></a>
                    <ul class="dropdown-menu">
                        <li><a href="#">Browse All Recruiters</a></li>
                        <li><a href="#">Add Recruiters</a></li>
                        <li><a href="#">Search Recruiters</a></li>
                    </ul>
                </li>
                <li><a href="#">Services</a></li>
                <li class="dropdown">
                    <a data-toggle="dropdown" data-hover="dropdown" class="dropdown-toggle" href="#">Companies <b class=" fa fa-angle-down"></b></a>
                    <ul class="dropdown-menu">
                        <li><a href="#">All Companies</a></li>
                        <li><a href="#">Add Companies</a></li>
                        <li><a href="#">Search Companies</a></li>
                    </ul>
                </li> -->
            <!--</ul> -->

          </div>

          <div class="top-nav clearfix">
              <!--search & user info start-->
              <ul class="nav pull-right top-menu">
                  <li>
                      <input type="text" class="form-control search" placeholder=" Search" style="background-image: url('<?php echo base_url();?>client_assets/images/search-icon.png');">
                  </li>
                  <!-- user login dropdown start-->
                   <?php
            $client_session_data=$this->session->userdata(base_url().'client_login_session');
            $client_name=$client_session_data['company_name'];
            $role=$client_session_data['role'];

           // echo $client_name;exit;
	         ?>
	         <?php if(!empty($client_session_data)){?>
                  <li class="dropdown">
                      <a data-toggle="dropdown" class="dropdown-toggle" href="#">
                          <img alt="" src="<?php echo base_url();?>client_assets/images/avatar1_small.jpg">
                          <span class="username"><?php echo $client_name;?></span>
                          <b class="caret"></b>
                      </a>
                      <ul class="dropdown-menu extended logout">
                        <?php if($role == 'subclient'){?>

                          <li style="display:none;"><a href="<?php echo base_url();?>onlineForm"><i class=" fa fa-suitcase"></i>Profile</a></li>
                          <li style="display:none;"><a href="<?php echo base_url();?>client/companyProfile"><i class="fa fa-suitcase"></i>Company Profile</a></li>
                          <?php } else {?>
                          <li><a href="<?php echo base_url();?>onlineForm"><i class=" fa fa-suitcase"></i>Profile</a></li>
                          <li><a href="<?php echo base_url();?>client/companyProfile"><i class="fa fa-suitcase"></i>Company Profile</a></li>
                          <?php } ?>

                          
                          <li><a href="<?php echo base_url();?>client/changePassword"><i class="fa fa-cog"></i> Settings</a></li>
                          <li><a href="<?php echo base_url();?>client/logout"><i class="fa fa-key"></i> Log Out</a></li>
                      </ul>
                  </li>
                  <?php }?>
                  <!-- user login dropdown end -->
              </ul>
              <!--search & user info end-->
          </div>
      </header>
</section>

<section id="adminheadermain">
    <!--navigation start-->
    <nav class="navbar navbar-inverse" role="navigation">
        <!-- Brand and toggle get grouped for better mobile display -->
        <div class="navbar-header">
            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-ex1-collapse">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <!-- <a class="navbar-brand" href="#">Daydreamer</a> -->
        </div>

        <!-- Collect the nav links, forms, and other content for toggling -->
        <div class="collapse navbar-collapse navbar-ex1-collapse">
            <ul class="nav navbar-nav">
                <li id="home"><a href="<?php echo base_url();?>client/viewDashboard">Home</a></li>
                <li class="dropdown" id="job">
                    <a href="javascript:;" class="dropdown-toggle" data-toggle="dropdown">Job Openings <b class="caret"></b></a>
                    <ul class="dropdown-menu">
                        <li><a href="<?php echo base_url();?>client/viewJobForm">Create Job Opening</a></li>
                        <li><a href="<?php echo base_url();?>client/viewJobs">All Job Openings</a></li>
                         <?php 
                         error_reporting(0);

                         if($role=='mainclient'){

                            ?>
                        <li><a href="<?php echo base_url();?>client/viewSubOfficesJobs">All SubOffices Openings</a></li>
                        <?php } else {?>

                        <li style="display:none;"><a href="<?php echo base_url();?>client/viewSubOfficesJobs">All SubOffices Openings</a></li>
                        <?php }?>
                    </ul>
                </li>
             
                <?php 
                      error_reporting(0);
                      if($role=='mainclient'){
                        
                         
                      ?>
                <li class="dropdown" id="client">
                    <a href="javascript:;" class="dropdown-toggle" data-toggle="dropdown">Offices <b class="caret"></b></a>
                    <ul class="dropdown-menu">
                      
                        <li><a href="<?php echo base_url();?>client/viewSubClientForm">Create New Office</a></li>
                       
                        
                       
                        <li ><a href="<?php echo base_url();?>client/viewAllClients">All Offices</a></li>
                        <li id="viewCredentials" id="viewCredentials"><a href="<?php echo base_url();?>client/viewAllCredentials">View Credentials</a></li>
                    </ul>
                </li>
                 <?php }else {?>
                  <li class="dropdown" style="display:none;" id="client">
                    <a href="javascript:;" class="dropdown-toggle" data-toggle="dropdown">Offices <b class="caret"></b></a>
                    <ul class="dropdown-menu">
                      
                        <li><a href="<?php echo base_url();?>client/viewSubClientForm">Create New Office</a></li>
                       
                        
                       
                        <li ><a href="<?php echo base_url();?>client/viewAllClients">All Offices</a></li>
                    </ul>
                </li>
                  <?php }?>
             
                <li><a href="<?php echo base_url();?>client/viewPackages">Services<b class="caret"></b></a>
                <ul class="dropdown-menu">
                 <li><a href="<?php echo base_url();?>client/subscribedPackages">My packages</a>
                <li><a href="javascript:void(0);">Individual ads</a>
                <li><a href="<?php echo base_url();?>client/viewPackages">Packages</a>
                <li><a href="javascript:void(0);">Other services</a>
                

                   


                </li>
             </ul>
             </li>
              

                <li id="feedback" id="feedback"><a href="<?php echo base_url();?>client/viewFeedbackForm">Feedback</a></li>
                
            </ul>

            <!-- <ul class="nav navbar-nav navbar-right">
                <li><a href="javascript:;">Link</a></li>
                <li class="dropdown">
                    <a href="javascript:;" class="dropdown-toggle" data-toggle="dropdown">Dropdown <b class="caret"></b></a>
                    <ul class="dropdown-menu">
                        <li><a href="#">Action</a></li>
                        <li><a href="#">Another action</a></li>
                        <li><a href="#">Something else here</a></li>
                        <li><a href="#">Separated link</a></li>
                    </ul>
                </li>
            </ul> -->
        </div><!-- /.navbar-collapse -->
    </nav>
    <!--navigation end-->
</section>