<style type="text/css">
.help-block{

  color:red;
}

.profileimg {
    width:250px;
    height:250px;
    position: relative;
    display: block;
    margin: auto;
}
.editprofileimg {
    position: absolute;
    bottom: 0px;
    right: 0px;
    width: 40px;
    height: 40px;
    background-color: rgba(0, 0, 0, 0.7);
    color: #ccc;
    padding: 7px;
    font-size: 18px;
    text-align: center;
    border-radius: 4px;
}
.editprofileimg:hover {
    color: #fff;
    background-color: rgba(0, 0, 0, 1);
}

.cropit-preview 

{ 
    background-color: #f8f8f8; 
    background-size: cover;
    border: 1px solid #ccc;
    border-radius: 3px; 
    margin-top: 7px;
    width: 202px;
    height: 202px;

         }
.cropit-preview-image-container 
{ 
  cursor: move; 
} 

.image-size-label 
  { 
margin-top: 10px; 
  } 

  input,.export
  {
    display: block; 
  } 
   button 
  { 
  margin-top: 10px; 
 } 
  .req{ 
    color:red; 
  } 


</style>

<div class="container">
  <h1>Company Profile</h1>
  <?php echo $this->session->flashdata('successmsg');?>
  <?php echo $this->session->flashdata('errormsg');?>
  <hr>
  <div class="row">
    <!-- left column -->

    <div class="col-md-3">
      <form ng-submit="submitProfilePicForm()" name="profilePicsForm" novalidate>
        <div class="text-center">
          <!-- <img src="//placehold.it/100" class="avatar img-circle" alt="avatar"> -->
          <h6>Please Upload your Company Logo</h6>

          <div class="form-group" style="padding-top: 15px; padding-left: 16px;">
            <!-- <label class="control-label col-sm-3 col-sm-offset-1 text-right">Job Summary:</label> -->
            <div class="controls col-sm-7">
              <div class="profileimg thumbnail"> 
              <?php
              $path=base_url().'client_uploads/client_logo/'.$company_logo;
 
              ?>
              <img src="<?php echo $path;?>">
              <a class="editprofileimg" data ="" data-toggle="modal" data-target="#myModal" href="javascript:void(0);"><i class="fa fa-edit"></i></a>

              </div>

            
           </div>
         </div>
       </div>
     </form>
   </div>

   <!-- edit form column -->
   <div class="col-md-9 personal-info">
        <!-- <div class="alert alert-info alert-dismissable">
          <a class="panel-close close" data-dismiss="alert">×</a> 
          <i class="fa fa-coffee"></i>
          This is an <strong>.alert</strong>. Use this to show important messages to the user.
        </div> -->
        <h3 style="text-align:center;">Company Information</h3></br>
        
       
      <form class="form-horizontal bucket-form" ng-submit="submitprofileForm()" name="profileForm" novalidate>

       
          <div class="form-group">
            <label class="col-lg-3 control-label">Company Name:</label>
            <div class="col-lg-8">
              <input type="text"  class="form-control" name="company_name" ng-model="profile.company_name" required> 
              <span ng-show="submitted && profileForm.company_name.$error.required"  class="help-block has-error ng-hide">Company Name is required.</span>
              <span ng-show="errorCompanyName" class="help-block has-error ng-hide">{{errorCompanyName}}</span>
            </div>
          </div>
          <div class="form-group">
            <label class="col-lg-3 control-label">Company Overview:</label>
            <div class="col-lg-8">
            <textarea rows="8" cols="4"  class="form-control" name="company_overview" ng-model="profile.company_overview" required></textarea>
              <span ng-show="submitted && profileForm.company_overview.$error.required"  class="help-block has-error ng-hide">Company Overview is required.</span>
              <span ng-show="errorCompanyOverview" class="help-block has-error ng-hide">{{errorCompanyOverview}}</span>
            </div>
          </div>
          <div class="form-group">
            <label class="col-lg-3 control-label">Founded:</label>
            <div class="col-lg-8">
              <input type="text"  class="form-control" name="founded" ng-model="profile.founded" required>
              <span ng-show="submitted && profileForm.founded.$error.required"  class="help-block has-error ng-hide">Founded is required.</span>
              <span ng-show="errorFounded" class="help-block has-error ng-hide">{{errorFounded}}</span>
            </div>


          </div>

          <div class="form-group">
            <label class="col-lg-3  control-label">HeadQuarters:</label>
            <div class="col-lg-8">
              <input type="text"  class="form-control" name="headquarters" ng-model="profile.headquarters" required>
              <span ng-show="submitted && profileForm.headquarters.$error.required"  class="help-block has-error ng-hide">HeadQuarters is required.</span>
              <span ng-show="errorHeadQuarters" class="help-block has-error ng-hide">{{errorHeadQuarters}}</span>
            </div>
          </div>


          <div class="form-group">
            <label class="col-lg-3  control-label">Total staff:</label>
            <div class="col-lg-8">
              <input type="text"  class="form-control" name="staff" ng-model="profile.staff" required>
              <span ng-show="submitted && profileForm.staff.$error.required"  class="help-block has-error ng-hide">Staff is required.</span>
              <span ng-show="errorStaff" class="help-block has-error ng-hide">{{errorStaff}}</span>
            </div>
          </div>

           <div class="form-group">
            <label class="col-lg-3  control-label">Web address:</label>
            <div class="col-lg-8">
              <input type="text"  class="form-control" name="web_address" ng-model="profile.web_address" required>
              <span ng-show="submitted && profileForm.web_address.$error.required"  class="help-block has-error ng-hide">Web Address is required.</span>
              <span ng-show="errorWebAddress" class="help-block has-error ng-hide">{{errorWebAddress}}</span>
            </div>
          </div>

          <div class="form-group">
            <label class="col-lg-3 control-label">Ratings:</label>
            <div class="col-lg-8">
              <p id="ratings" class="col-sm-7"></p>
              <input class="form-control" type="hidden" name="ratings" ng-model="profile.ratings" id="t2">
              <span ng-show="submitted && profileForm.ratings.$error.required"  class="help-block has-error ng-hide">Ratings is required.</span>
            </div>
          </div>


            <div class="form-group">
            <label class="control-label col-lg-3 text-right">Banner Header:</label>
            <div class="controls col-sm-7">


             <input id="uploadFile" class="default ng-pristine ng-valid ng-empty ng-touched" file-model="myFile" ng-model="profile.attached_file" name="attached_file" ng-files="getTheFiles($files)" type="file">
             <span ng-show="submitted && profileForm.attached_file.$error.required"  class="help-block has-error ng-hide">Banner Header is required.</span>
             <span ng-show="errorFile" class="help-block has-error ng-hide">{{errorFile}}</span><br/>
             <span ng-bind="profile.attached_file"></span>
           </div>
         </div>


       

      </div>

      <div class="col-md-12">
        <div class="form-group">
          <div class="col-sm-12">
            <button type="submit" class="btn btn-info pull-right btn-sm" ng-click="submitted = true"><strong>Submit</strong></button><span class="pull-right"> &nbsp; &nbsp; </span>

            <a href="<?php echo base_url();?>admin/viewProfile"><button type="button" class="btn btn-danger pull-right btn-sm"><strong><i class="fa fa-times"></i> Cancel</strong></button></a>
          </div>
        </div>
      </div>
    </form>
  </div>
</div>
</div>
<hr>


 <!--  modal open for change profile picture -->
          <form class="form-horizontal bucket-form" id="cropimage"  name="profilePic" ng-submit="submitform()" novalidate>

                              
                                    <div class="modal fade" id="myModal" role="dialog" style="background-color: rgba(0, 0, 0, 0.5);">
                                        <div class="modal-dialog">

                                          <!-- Modal content-->
                                          <div class="modal-content">
                                            <div class="modal-header">
                                              <button type="button" class="close" data-dismiss="modal">&times;</button>
                                              <h4 class="modal-title">Change Profile Picture</h4>
                                          </div>
                                      <div class="modal-body">
                                       <div class="form-group">
                              
                                         <div class="col-sm-10">
                                  <div class="image-editor">
                          <input type="file" accept="image*/" ng-model="brand.userfile" id="FileUpload1" class="cropit-image-input"><br>
                          <span style="color:#a94442;">Profie picture must be minimum width 200px and minimum height 200px.</span>

                           
                          <div class="cropit-preview"></div>
                          <div class="image-size-label">
                              Resize image
                          </div>
                          <input type="range" class="cropit-image-zoom-input">
                        

                          <button class="export">Upload</button>
                          <span id="succ" style="color: green;" ></span>
                         </div>
                         <input type="hidden" class="form-control" id="doc_pic" name="doc_pic" ng-model="brand.doc_pic" required/>
                        
                         
                         <span ng-show="submitted && profilePic.doc_pic.$invalid" class="help-block has-error ng-hide warnig">Please upload profile picture</span>
                            <span class="help-block has-error ng-hide warnig" style="color: red;" ng-show="doc_picError">{{doc_picError}}</span>
                              </div>
                          </div>

                                  <div class="row">
                       <div class="col-md-12">
                          <button type="submit" ng-click="submitted = true" class="btn btn-info pull-right btn-sm"><strong> Update <i class="fa fa-arrow-right"></i></strong></button>
                      </div>
                  </div>
                                  </div>
              <div class="modal-footer">
                  <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
              </div>
          </div>
          
      </div>
  </div>
</form>