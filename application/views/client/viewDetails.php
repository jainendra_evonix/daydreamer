<?php // echo "<pre>"; print_r($getPackageDetails); exit; 


   $this->session->set_userdata('package_amt',$getPackageDetails->price_amount);
  // $this->session->set_userdata('package_amt',$getPackageDetails->price_amount);


?>
<div id="content">
    <section>
        <!-- <div class="section-header">
            <ol class="breadcrumb">
                <li class="active">Package Details</li>
            </ol>
        </div> -->
        <h2 style="color:black;padding-left:75px;padding-top: 29px;font-weight:bold;">Package Details</h2>
        
        <a href="<?php echo base_url();?>client/subscribedPackages" class="btn btn-success pull-right" style="left: 1170px;position: absolute;top: 152px;"><i class="fa fa-arrow-left"></i> Back</a>
        <div class="section-body container">
            <div class="row">
                <div class="col-lg-12">
                    <div class="card card-printable style-default-light">
                        <div class="panel panel-default">
                             <div class="panel-body" style="margin-top:53px;">
                        <div class="card-body style-default-bright" style="padding: 24px;">

                            <!-- BEGIN INVOICE HEADER -->
                            <div class="row">
                                <div class="col-xs-8">
                                    <h2><i class="fa fa-cube"></i> <strong class="text-accent-dark"><?php echo $getPackageDetails->package_title ?></strong></h2>
                                </div>
                                        <div class="col-xs-4 text-right">
                                             <!-- <h1 class="text-light text-default-light" style="color:#474752;">Invoice</h1> -->
                                        </div>
                            </div><!--end .row -->
                            <!-- END INVOICE HEADER -->

                            <br/>

                            <!-- BEGIN INVOICE DESCRIPTION -->
                            <div class="row">
                                <div class="col-xs-12">
                                    <form  action="https://secure.worldpay.com/wcc/purchase" method="post">


                       <!-- worldpay payment code -->
                             
                             
                             <input type="hidden" name="testMode" value="100">

                            <!-- This next line contains a mandatory parameter. Put your Installation ID inside the quotes after value= -->
                            <!-- You will need to get the installation ID from your Worldpay account. Login to your account, click setting and under installations 
                            you should have an option called select junior and a number, put the number between “” e.g. “123456”-->

                           <!--  <input type="hidden" name=”instId” value="1211616> -->


                           <input type="hidden" name="instId" value="1211615">
                            <!-- Another mandatory parameter. Put your own reference identifier for the item purchased inside the quotes after value= -->
                            <input type="hidden" name="cartId" value="<?php echo $getPackageDetails->id; ?>">
                            <!-- Another mandatory parameter. Put the total cost of the item inside the quotes  -->
                            <input type="hidden" name="amount" value="<?php echo $getPackageDetails->price_amount; ?>">
                            <!-- Another mandatory parameter. Put the code for the purchase currency inside the quotes after value= -->
                            <input type="hidden" name="currency" value="GBP">
                            <!-- This creates the button. When it is selected in the browser, the form submits the purchase details to us. -->
                           
                                      
                                    <div class="row"  style="font-family: bold;font-size: 25px;">
                                        <div class="text-light col-md-4">Package Title :</div>
                                        <div class="col-md-8"><?php echo $getPackageDetails->package_title ?></div>
                                    </div>

                                    <div class="row" style="font-family: bold;font-size: 25px;">
                                        <div class="text-light col-md-4">Validity Date:</div>
                                        <div class="col-md-8"><?php echo date('jS M Y', strtotime($getPackageDetails->validity_date)); ?></div>
                                        </div>


                                    <div class="row" style="font-family: bold;font-size: 25px;">
                                        <div class="text-light col-md-4">Jobs ads :</div>
                                        <div class="col-md-8"><?php  if(!empty($getPackageDetails)) { echo $getPackageDetails->totalJobAdds;} else { echo "No Jobs ads"; } ?></div>

                                    </div>


                                    <div class="row" style="font-family: bold;font-size: 25px;">
                                        <div class="text-light col-md-4">Services :</div>
                                        <div class="col-md-8"><?php
                   
                             $service=$getPackageDetails->totalServices;
                           // echo '<pre>'; print_r($service);
                             $service_array=json_decode($service);
                            
                            //print_r($service_array);

                             if(isset($service_array))
                             {
                              $serviceName='';
                             foreach($service_array as $serv){
                                 error_reporting(0);
                               $serviceName.=ucfirst($showService[$serv]).',';
                            

                             
                             
                          }
                          $serviceName=rtrim($serviceName,',');

                          echo $serviceName; 

                        } else{  echo "No services available";  } ?></div>

                                    </div>


                                    <div class="row" style="font-family: bold;font-size: 25px;">
                                        <div class="text-light col-md-4">Package Description :</div>
                                        <div class="col-md-8"><?php echo $getPackageDetails->package_description; ?></div>

                                    </div>


                                    <div class="row" style="font-family: bold;font-size: 25px;">
                                        <div class="text-light col-md-4">Price :</div>
                                        <div class="col-md-8">£<?php echo $getPackageDetails->price_amount; ?></div>

                                    </div>



                                    <div class="row" style="font-family: bold;font-size: 25px;">
                                        <div class="text-light col-md-4"></div> 
                                        <div class="col-md-8">
                          
                          <?php if(!empty($subscribedPackage)){ ?>          
                         <button type="button"  class="btn btn-success" style="margin-top:15px;" value="Submit">Subscribed</button><img src="<?php echo base_url(); ?>client_assets/images/check_icon.ico" style="width:30px; margin-left: 05px; padding-top:13px;" >
                        <?php  } else{ ?>

                        
                           
                           <button type="submit"  class="btn btn-success" style="margin-top:15px;" value="Submit">Subscribe</button>
                        <?php }?>
                                    </div>
                                   
                                    </form>
                                </div>
                            </div>


                        </div><!--end .card-body -->
                    </div><!--end .card -->
                    </div>
                </div>
                </div><!--end .col -->
            </div><!--end .row -->
        </div><!--end .section-body -->
    </section>
</div>