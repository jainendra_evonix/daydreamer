
<style type="text/css">
.help-block{

    color:red;
}


</style>



<!--main content start-->
<section id="adminsection" class="container">
    <section class="wrapper">
        <!-- page start-->
        <div class="row">
            <div class="col-md-12">
                <h4 class="form-heading">Welcome <?php echo $firstname.' '.$lastname?></h4>
                <?php echo $this->session->flashdata('successmsg');?>
                 <?php echo $this->session->flashdata('errormsg');?>
                <br>
                <div class="row">
                    <?php 
                   $client_data=$this->session->userdata(base_url().'client_login_session');
                    $role=$client_data['role'];

                    if($role=='mainclient'){
                    ?>
                    <div class="col-md-3">

                       <section class="panel panelhover" style="box-shadow:4px 4px 15px rgba(136, 136, 136, 0.2);">
                    <div class="panel-body">
                        <span class="mini-stat-icon orange"><i class="fa fa-group"></i></span>
                        <div class="clearfix"></div>
                        <div class="top-stats-panel">
                            <div class="gauge-canvas">
                                <h4 class="widget-h"><a href="<?php echo base_url();?>client/viewAllClients">Total Sub Offices</a></h4>
                                <!-- <p>You can create new job opening by clicking the create icon.</p> -->
                            </div>
                            <ul class="gauge-meta clearfix">
                                <li><strong class="text-xl" style="font-size:25px;padding-left: 100px;"><?php echo $totalSubClients;?></strong></li>
                            </ul>
                        </div>
                    </div>
                </section>
               </div><!--end .col -->
               <?php }else {?>
                <div class="col-md-3" style="display:none;">

                       <section class="panel panelhover" style="box-shadow:4px 4px 15px rgba(136, 136, 136, 0.2);">
                    <div class="panel-body">
                        <span class="mini-stat-icon orange"><i class="fa fa-group"></i></span>
                        <div class="clearfix"></div>
                        <div class="top-stats-panel">
                            <div class="gauge-canvas">
                                <h4 class="widget-h"><a href="<?php echo base_url();?>client/viewAllClients">Total Sub Offices</a></h4>
                                <!-- <p>You can create new job opening by clicking the create icon.</p> -->
                            </div>
                            <ul class="gauge-meta clearfix">
                                <li><strong class="text-xl" style="font-size:25px;padding-left: 100px;"><?php echo $totalSubClients;?></strong></li>
                            </ul>
                        </div>
                    </div>
                </section>
               </div><!--end .col -->
               <?php }?>

               
              <div class="col-md-3">

                       <section class="panel panelhover" style="box-shadow:4px 4px 15px rgba(136, 136, 136, 0.2);">
                    <div class="panel-body">
                        <span class="mini-stat-icon orange"><i class="fa fa-briefcase"></i></span>
                        <div class="clearfix"></div>
                        <div class="top-stats-panel">
                            <div class="gauge-canvas">
                                <h4 class="widget-h"><a href="<?php echo base_url();?>client/viewPackages">Total Packages</a></h4>
                                <!-- <p>You can create new job opening by clicking the create icon.</p> -->
                            </div>
                            <ul class="gauge-meta clearfix">
                                <li><strong class="text-xl" style="font-size:25px;padding-left: 100px;"><?php echo $packageCount;?></strong></li>
                            </ul>
                        </div>
                    </div>
                </section>
               </div><!--end .col -->

               <div class="col-md-3">

                       <section class="panel panelhover" style="box-shadow:4px 4px 15px rgba(136, 136, 136, 0.2);">
                    <div class="panel-body">
                        <span class="mini-stat-icon orange"><i class="fa fa-briefcase"></i></span>
                        <div class="clearfix"></div>
                        <div class="top-stats-panel">
                            <div class="gauge-canvas">
                                <h4 class="widget-h"><a href="<?php echo base_url();?>client/viewJobs">Total Jobs</a></h4>
                                <!-- <p>You can create new job opening by clicking the create icon.</p> -->
                            </div>
                            <ul class="gauge-meta clearfix">
                                <li><strong class="text-xl" style="font-size:25px;padding-left: 100px;"><?php echo $totalJobs;?></strong></li>
                            </ul>
                        </div>
                    </div>
                </section>
               </div><!--end .col -->


            <!--    <div class="col-md-3">
                                <section class="panel panelhover">
                                    <div class="panel-body">
                                        <span class="mini-stat-icon pink"><i class="fa fa-upload"></i></span>
                                        <div class="clearfix"></div>
                                        <div class="top-stats-panel">
                                            <div class="gauge-canvas">
                                                <h4 class="widget-h">Company Logo</h4>
                                                <p>You can upload your company logo by clicking the upload icon.</p>
                                            </div>
                                            <ul class="gauge-meta clearfix">
                                                <li class="pull-right gauge-title"><a data-toggle="modal" href="#myModal"><strong><i class="fa fa-upload"></i> Upload</strong></a></li>
                                            </ul>
                                        </div>
                                    </div>
                                </section>
                            </div>
 -->

           </div>

           <br/>
           <div class="row">
            <div class="col-md-3">
                <section class="panel panelhover">
                    <div class="panel-body">
                        <span class="mini-stat-icon orange"><i class="fa fa-gavel"></i></span>
                        <div class="clearfix"></div>
                        <div class="top-stats-panel">
                            <div class="gauge-canvas">
                                <h4 class="widget-h">Create Job Openings</h4>
                                <p>You can create new job opening by clicking the create icon.</p>
                            </div>
                            <ul class="gauge-meta clearfix">
                                <li class="pull-right gauge-title"><a href="<?php echo base_url();?>client/viewJobForm" class="text-success"><strong><i class="fa fa-plus"></i> Create</strong></a></li>
                            </ul>
                        </div>
                    </div>
                </section>
            </div>
                            <!-- <div class="col-md-3">
                                <section class="panel panelhover">
                                    <div class="panel-body">
                                                <span class="mini-stat-icon tar"><i class="fa fa-tag"></i></span>
                                                <div class="clearfix"></div>
                                        <div class="top-stats-panel">
                                            <div class="gauge-canvas">
                                                <h4 class="widget-h">Create New Candidate</h4>
                                                <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
                                                tempor incididunt.</p>
                                            </div>
                                            <ul class="gauge-meta clearfix">
                                                <li class="pull-right gauge-title"><a href="#" class="text-success"><strong><i class="fa fa-plus"></i> Create</strong></a></li>
                                            </ul>
                                        </div>
                                    </div>
                                </section>
                            </div> -->
                            <!-- <div class="col-md-3">
                                <section class="panel panelhover">
                                    <div class="panel-body">
                                                <span class="mini-stat-icon pink"><i class="fa fa-money"></i></span>
                                                <div class="clearfix"></div>
                                        <div class="top-stats-panel">
                                            <div class="gauge-canvas">
                                                <h4 class="widget-h">Create Interview</h4>
                                                <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
                                                tempor incididunt.</p>
                                            </div>
                                            <ul class="gauge-meta clearfix">
                                                <li class="pull-right gauge-title"><a href="#" class="text-success"><strong><i class="fa fa-plus"></i> Create</strong></a></li>
                                            </ul>
                                        </div>
                                    </div>
                                </section>
                            </div> -->
                            <div class="col-md-3">
                                <section class="panel panelhover">
                                    <div class="panel-body">
                                        <span class="mini-stat-icon yellow-b"><i class="fa fa-users" aria-hidden="true"></i></span>
                                        <div class="clearfix"></div>
                                        <div class="top-stats-panel">
                                            <div class="gauge-canvas">
                                                <h4 class="widget-h">Create New Client</h4>
                                                <p>You can create new client by clicking the create icon.</p>
                                            </div>
                                            <ul class="gauge-meta clearfix">
                                                <li class="pull-right gauge-title"><a href="<?php echo base_url();?>client/viewSubClientForm" class="text-success"><strong><i class="fa fa-users" aria-hidden="true"></i> Create</strong></a></li>
                                            </ul>
                                        </div>
                                    </div>
                                </section>
                            </div>


                            <!-- <div class="row"> -->
                            <div class="col-md-3">
                                <section class="panel panelhover">
                                    <div class="panel-body">
                                        <span class="mini-stat-icon orange"><i class="fa fa-gavel"></i></span>
                                        <div class="clearfix"></div>
                                        <div class="top-stats-panel">
                                            <div class="gauge-canvas">
                                                <h4 class="widget-h">Create Login Credentials</h4>
                                                <p>You can create new client by clicking the create icon.</p>
                                            </div>
                                            <ul class="gauge-meta clearfix">
                                                <li class="pull-right gauge-title"><a href="<?php echo base_url();?>client/createLoginCredentials" class="text-success"><strong><i class="fa fa-plus"></i> Create</strong></a></li>
                                            </ul>
                                        </div>
                                    </div>
                                </section>
                            </div>
                            <!-- <div class="col-md-3">
                                <section class="panel panelhover">
                                    <div class="panel-body">
                                                <span class="mini-stat-icon tar"><i class="fa fa-tag"></i></span>
                                                <div class="clearfix"></div>
                                        <div class="top-stats-panel">
                                            <div class="gauge-canvas">
                                                <h4 class="widget-h">Create Job Openings</h4>
                                                <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
                                                tempor incididunt.</p>
                                            </div>
                                            <ul class="gauge-meta clearfix">
                                                <li class="pull-right gauge-title"><a href="#" class="text-success"><strong><i class="fa fa-plus"></i> Create</strong></a></li>
                                            </ul>
                                        </div>
                                    </div>
                                </section>
                            </div> -->
                            <div class="col-md-3">
                                <section class="panel panelhover">
                                    <div class="panel-body">
                                        <span class="mini-stat-icon pink"><i class="fa fa-money"></i></span>
                                        <div class="clearfix"></div>
                                        <div class="top-stats-panel">
                                            <div class="gauge-canvas">
                                                <h4 class="widget-h">View Packages</h4>
                                                <p>You can view all the packages by clicking the view icon.</p>
                                            </div>
                                            <ul class="gauge-meta clearfix">
                                                <li class="pull-right gauge-title"><a href="<?php echo base_url();?>client/viewPackages" class="text-success"><strong><i class="fa fa-eye"></i> View</strong></a></li>
                                            </ul>
                                        </div>
                                    </div>
                                </section>
                            </div>

                            
                            <!-- <div class="col-md-3">
                                <section class="panel panelhover">
                                    <div class="panel-body">
                                                <span class="mini-stat-icon yellow-b"><i class="fa fa-eye"></i></span>
                                                <div class="clearfix"></div>
                                        <div class="top-stats-panel">
                                            <div class="gauge-canvas">
                                                <h4 class="widget-h">Create Job Openings</h4>
                                                <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
                                                tempor incididunt.</p>
                                            </div>
                                            <ul class="gauge-meta clearfix">
                                                <li class="pull-right gauge-title"><a href="#" class="text-success"><strong><i class="fa fa-plus"></i> Create</strong></a></li>
                                            </ul>
                                        </div>
                                    </div>
                                </section>
                            </div> -->
                            <!-- </div> -->
                        </div>
                    </div>
                </div>
                <!-- page end-->

            </section>
        </section>
        <!--main content end-->





        <!--right sidebar start-->
        <div class="right-sidebar">
            <div class="search-row">
                <input type="text" placeholder="Search" class="form-control">
            </div>
            <div class="right-stat-bar">
                <ul class="right-side-accordion">
                    <li class="widget-collapsible">
                        <a href="#" class="head widget-head red-bg active clearfix">
                            <span class="pull-left">work progress (5)</span>
                            <span class="pull-right widget-collapse"><i class="ico-minus"></i></span>
                        </a>
                        <ul class="widget-container">
                            <li>
                                <div class="prog-row side-mini-stat clearfix">
                                    <div class="side-graph-info">
                                        <h4>Target sell</h4>
                                        <p>
                                            25%, Deadline 12 june 13
                                        </p>
                                    </div>
                                    <div class="side-mini-graph">
                                        <div class="target-sell">
                                        </div>
                                    </div>
                                </div>
                                <div class="prog-row side-mini-stat">
                                    <div class="side-graph-info">
                                        <h4>product delivery</h4>
                                        <p>
                                            55%, Deadline 12 june 13
                                        </p>
                                    </div>
                                    <div class="side-mini-graph">
                                        <div class="p-delivery">
                                            <div class="sparkline" data-type="bar" data-resize="true" data-height="30" data-width="90%" data-bar-color="#39b7ab" data-bar-width="5" data-data="[200,135,667,333,526,996,564,123,890,564,455]">
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="prog-row side-mini-stat">
                                    <div class="side-graph-info payment-info">
                                        <h4>payment collection</h4>
                                        <p>
                                            25%, Deadline 12 june 13
                                        </p>
                                    </div>
                                    <div class="side-mini-graph">
                                        <div class="p-collection">
                                          <span class="pc-epie-chart" data-percent="45">
                                              <span class="percent"></span>
                                          </span>
                                      </div>
                                  </div>
                              </div>
                              <div class="prog-row side-mini-stat">
                                <div class="side-graph-info">
                                    <h4>delivery pending</h4>
                                    <p>
                                        44%, Deadline 12 june 13
                                    </p>
                                </div>
                                <div class="side-mini-graph">
                                    <div class="d-pending">
                                    </div>
                                </div>
                            </div>
                            <div class="prog-row side-mini-stat">
                                <div class="col-md-12">
                                    <h4>total progress</h4>
                                    <p>
                                        50%, Deadline 12 june 13
                                    </p>
                                    <div class="progress progress-xs mtop10">
                                        <div style="width: 50%" aria-valuemax="100" aria-valuemin="0" aria-valuenow="20" role="progressbar" class="progress-bar progress-bar-info">
                                            <span class="sr-only">50% Complete</span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </li>
                    </ul>
                </li>
                <li class="widget-collapsible">
                    <a href="#" class="head widget-head terques-bg active clearfix">
                        <span class="pull-left">contact online (5)</span>
                        <span class="pull-right widget-collapse"><i class="ico-minus"></i></span>
                    </a>
                    <ul class="widget-container">
                        <li>
                            <div class="prog-row">
                                <div class="user-thumb">
                                    <a href="#"><img src="images/avatar1_small.jpg" alt=""></a>
                                </div>
                                <div class="user-details">
                                    <h4><a href="#">Jonathan Smith</a></h4>
                                    <p>
                                        Work for fun
                                    </p>
                                </div>
                                <div class="user-status text-danger">
                                    <i class="fa fa-comments-o"></i>
                                </div>
                            </div>
                            <div class="prog-row">
                                <div class="user-thumb">
                                    <a href="#"><img src="images/avatar1.jpg" alt=""></a>
                                </div>
                                <div class="user-details">
                                    <h4><a href="#">Anjelina Joe</a></h4>
                                    <p>
                                        Available
                                    </p>
                                </div>
                                <div class="user-status text-success">
                                    <i class="fa fa-comments-o"></i>
                                </div>
                            </div>
                            <div class="prog-row">
                                <div class="user-thumb">
                                    <a href="#"><img src="images/chat-avatar2.jpg" alt=""></a>
                                </div>
                                <div class="user-details">
                                    <h4><a href="#">John Doe</a></h4>
                                    <p>
                                        Away from Desk
                                    </p>
                                </div>
                                <div class="user-status text-warning">
                                    <i class="fa fa-comments-o"></i>
                                </div>
                            </div>
                            <div class="prog-row">
                                <div class="user-thumb">
                                    <a href="#"><img src="images/avatar1_small.jpg" alt=""></a>
                                </div>
                                <div class="user-details">
                                    <h4><a href="#">Mark Henry</a></h4>
                                    <p>
                                        working
                                    </p>
                                </div>
                                <div class="user-status text-info">
                                    <i class="fa fa-comments-o"></i>
                                </div>
                            </div>
                            <div class="prog-row">
                                <div class="user-thumb">
                                    <a href="#"><img src="images/avatar1.jpg" alt=""></a>
                                </div>
                                <div class="user-details">
                                    <h4><a href="#">Shila Jones</a></h4>
                                    <p>
                                        Work for fun
                                    </p>
                                </div>
                                <div class="user-status text-danger">
                                    <i class="fa fa-comments-o"></i>
                                </div>
                            </div>
                            <p class="text-center">
                                <a href="#" class="view-btn">View all Contacts</a>
                            </p>
                        </li>
                    </ul>
                </li>
                <li class="widget-collapsible">
                    <a href="#" class="head widget-head purple-bg active">
                        <span class="pull-left"> recent activity (3)</span>
                        <span class="pull-right widget-collapse"><i class="ico-minus"></i></span>
                    </a>
                    <ul class="widget-container">
                        <li>
                            <div class="prog-row">
                                <div class="user-thumb rsn-activity">
                                    <i class="fa fa-clock-o"></i>
                                </div>
                                <div class="rsn-details ">
                                    <p class="text-muted">
                                        just now
                                    </p>
                                    <p>
                                        <a href="#">Jim Doe </a>Purchased new equipments for zonal office setup
                                    </p>
                                </div>
                            </div>
                            <div class="prog-row">
                                <div class="user-thumb rsn-activity">
                                    <i class="fa fa-clock-o"></i>
                                </div>
                                <div class="rsn-details ">
                                    <p class="text-muted">
                                        2 min ago
                                    </p>
                                    <p>
                                        <a href="#">Jane Doe </a>Purchased new equipments for zonal office setup
                                    </p>
                                </div>
                            </div>
                            <div class="prog-row">
                                <div class="user-thumb rsn-activity">
                                    <i class="fa fa-clock-o"></i>
                                </div>
                                <div class="rsn-details ">
                                    <p class="text-muted">
                                        1 day ago
                                    </p>
                                    <p>
                                        <a href="#">Jim Doe </a>Purchased new equipments for zonal office setup
                                    </p>
                                </div>
                            </div>
                        </li>
                    </ul>
                </li>
                <li class="widget-collapsible">
                    <a href="#" class="head widget-head yellow-bg active">
                        <span class="pull-left"> shipment status</span>
                        <span class="pull-right widget-collapse"><i class="ico-minus"></i></span>
                    </a>
                    <ul class="widget-container">
                        <li>
                            <div class="col-md-12">
                                <div class="prog-row">
                                    <p>
                                        Full sleeve baby wear (SL: 17665)
                                    </p>
                                    <div class="progress progress-xs mtop10">
                                        <div class="progress-bar progress-bar-success" role="progressbar" aria-valuenow="20" aria-valuemin="0" aria-valuemax="100" style="width: 40%">
                                            <span class="sr-only">40% Complete</span>
                                        </div>
                                    </div>
                                </div>
                                <div class="prog-row">
                                    <p>
                                        Full sleeve baby wear (SL: 17665)
                                    </p>
                                    <div class="progress progress-xs mtop10">
                                        <div class="progress-bar progress-bar-info" role="progressbar" aria-valuenow="20" aria-valuemin="0" aria-valuemax="100" style="width: 70%">
                                            <span class="sr-only">70% Completed</span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </li>
                    </ul>
                </li>
            </ul>
        </div>
    </div>


     <!-- Modal -->
          <form class="form-signin" name="upload_logo" ng-submit="uploadCompanyLogo()" novalidate>
          <div aria-hidden="true" aria-labelledby="myModalLabel" role="dialog" tabindex="-1" id="myModal" class="modal fade">
              <div class="modal-dialog">
                  <div class="modal-content">
                      <div class="modal-header">
                          <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                          <h4 class="modal-title">Company Logo</h4>
                      </div>
                      
                      <div class="modal-body">
                        
                          <p>Upload your Company Logo.</p>
                          <input id="uploadFile" class="default ng-pristine ng-valid ng-empty ng-touched" file-model="myFile" ng-model="dashboard.logo" name="logo" ng-files="getTheFiles($files)" type="file">
                          <span id="msg" ng-show="submitted && upload_logo.logo.$invalid" class="help-block has-error">Please upload your company logo.</span>
                      </div>
                       <div id="message2" style="color:red;"></div>
                      <div class="modal-footer">
                          <button data-dismiss="modal" class="btn btn-default" type="button">Cancel</button>
                          <button class="btn btn-success" ng-click="submitted = true" type="submit">Submit</button>
                      </div>
                   
                  </div>
              </div>
          </div>
        </form>
          <!-- modal -->



