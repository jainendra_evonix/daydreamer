<!--main content start-->
<section id="adminsection" class="container">
    <section class="wrapper">
        <!-- page start-->
        <div class="row">
            <div class="col-md-12">

                <h4 class="form-heading"><strong>All Published Job</strong></h4>
                <!-- <a href="<?php echo base_url();?>client/viewJobForm" class="btn btn-success pull-right" style="margin-left: 868px;position: absolute;
                    top: 8px;">Create Job Opening <i class="fa fa-plus"></i></a></p>
                    <?php echo $this->session->flashdata('successmsg');  ?>
                    <?php echo $this->session->flashdata('errormsg');  ?>

                    <br> -->
                    <?php
                    if(!empty($allJobs)){
                        ?>
                        <section class="panel">
                            <div class="panel-body">
                                <div class="row">
                                    <div class="col-md-4">

                                    </div>
                                    <div class="col-md-8">

                                    </div>

                                    <div class="col-md-12">
                                        <section class="">
                                            <div class="table-responsive" style="border: 1px solid #ccc;padding: 6px;">

                                                <table  id="employee-grid"  cellpadding="0" cellspacing="0" border="0" class="display" width="100%">
                                                    <thead>
                                                        <tr>

                                                         <th>Sr.No</th>
                                                         <th>Job Title</th>
                                                         <th>Recruiter</th>
                                                         
                                                         <th>Job Status</th>
                                                         <th >Salary</th>
                                                         <th >Currency</th>
                                                         <th >Client Name</th>
                                                         <th >Created Date</th>
                                                         <th >Closing Date</th>
                                                        <!--  <th>Job Type</th> -->
                                                         <th >Action</th>
                                                     </tr>
                                                 </thead>
                                                 <!--  -->
                                             </table>
                                         </div>
                                     </section>
                                 </div>



                             </div>
                         </div>
                     </section>
                     <?php }else{?>

                     <section class="panel" style="display:none;">
                        <div class="panel-body">
                            <div class="row">
                                <div class="col-md-4">

                                </div>
                                <div class="col-md-8">

                                </div>

                                <div class="col-md-12">
                                    <section class="">
                                       <div class="table-responsive" style="border: 1px solid #ccc;padding: 6px;">

                                        <table id="employee-grid"  cellpadding="0" cellspacing="0" border="0" class="display" width="100%">
                                            <thead>
                                                <tr>
                                                    <th>Sr.No</th>
                                                    <th>Posting Title</th>
                                                    <th>Open Date</th> 
                                                    <th>Job Opening Status</th>
                                                    <th>Client Name</th>
                                                    <th>Assigned Recruiter</th>
                                                    <th>Action</th>
                                                </tr>
                                            </thead>
                                        </table>
                                    </div><!--end .table-responsive -->
                                </section>
                            </div>



                        </div>
                    </div>
                </section>

                <?php }?>
            </div>
        </div>
        <!-- page end-->

    </section>
</section>
<!--main content end-->






</div>



<!--dynamic table initialization -->
