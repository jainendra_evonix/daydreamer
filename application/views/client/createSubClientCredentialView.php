   <style type="text/css">
   .help-block{

    color:red;
  }
.panel {
    box-shadow: 0 1px 4px rgba(0, 0, 0, 0.1);
    transition: all 0.3s ease 0s;
}
.mandatory{

color: red;

}
  </style>


  <div class="container">
    <div class="row">
      <div class="col-md-8 col-md-offset-2">
       <h4 class="form-heading"><strong>Create Credentials</strong></h4>
       <?php echo $this->session->flashdata('successmsg');?>
       <?php echo $this->session->flashdata('errormsg');?>
        <span id="message"></span>
       <br>
       <div class="panel panel-default">
        <div class="panel-body">
         <div class="row">


          <form class="form-horizontal bucket-form" ng-submit="submitCreateLoginForm()" name="createLoginForm" novalidate>
            <input type="hidden" name="subclient_id" ng-model="login.subclient_id">
            <div class="col-md-8">
              <div class="form-group">
                <label class="col-sm-3 col-sm-offset-1 control-label"><span class="mandatory">*</span>Name:</label>
                <div class="col-sm-7">
                  <input type="text" name="first_name" ng-model="login.first_name" class="form-control" ng-pattern="/^[a-zA-Z ]*$/"  required>
                  <span ng-show="submitted && createLoginForm.first_name.$error.required"  class="help-block has-error ng-hide">First Name is required.</span>
                  <span ng-show="submitted && createLoginForm.first_name.$error.pattern"  class="help-block has-error ng-hide">Please enter valid name.</span>
                  <span ng-show="errorFirstName" class="help-block has-error ng-hide">{{errorFirstName}}</span>
                </div>
              </div>
             <!--  <div class="form-group">
                <label class="col-sm-3 col-sm-offset-1 control-label"><span class="mandatory">*</span>Last Name:</label>
                <div class="col-sm-7">
                  <input type="text" name="last_name" ng-model="login.last_name" class="form-control" ng-pattern="/^[a-zA-Z ]*$/" required>
                  <span ng-show="submitted && createLoginForm.last_name.$error.required"  class="help-block has-error ng-hide">Last Name is required.</span>
                  <span ng-show="submitted && createLoginForm.last_name.$error.pattern"  class="help-block has-error ng-hide">Please enter valid first name.</span>
                  <span ng-show="errorLastName" class="help-block has-error ng-hide">{{errorLastName}}</span>
                </div>
              </div> -->
              <div class="form-group">
                <label class="col-sm-3 col-sm-offset-1 control-label"><span class="mandatory">*</span>Email:</label>
                <div class="col-sm-7">
                  <input type="text" name="email" id="email" ng-model="login.email" class="form-control" ng-pattern='/^(([^<>()\[\]\.,;:\s@\"]+(\.[^<>()\[\]\.,;:\s@\"]+)*)|(\".+\"))@(([^<>()[\]\.,;:\s@\"]+\.)+[^<>()[\]\.,;:\s@\"]{2,})$/i' onchange="checkEmail();" required>
                  <span ng-show="submitted && createLoginForm.email.$error.required"  class="help-block has-error ng-hide">Email is required.</span>
                  <span ng-show="submitted && createLoginForm.email.$error.pattern"  class="help-block has-error ng-hide">Please enter valid email.</span>
                  <span ng-show="errorEmail" class="help-block has-error ng-hide">{{errorEmail}}</span>
                  <span id="showMsg" style="color:red;"></span>
                </div>
              </div>


              <div class="form-group">
                <label class="col-sm-3 col-sm-offset-1 control-label"><span class="mandatory">*</span>Password:</label>
                <div class="col-sm-7">
                  <input type="password" name="password" ng-model="login.password" class="form-control" ng-minlength="6" ng-maxlength="20" required>
                  <span ng-show="submitted && createLoginForm.password.$error.required"  class="help-block has-error ng-hide">Password is required.</span>
                  <span ng-show="errorPassword" class="help-block has-error ng-hide">{{errorPassword}}</span>
                </div>
              </div>


              <div class="form-group">
                <div class="col-sm-11">
                  <button type="submit" class="btn btn-info pull-right btn-sm" ng-click="submitted = true"><strong><i class="fa fa-save"></i> Save &amp; Publish</strong></button><span class="pull-right"> &nbsp; &nbsp; </span>

                  <a href="<?php echo base_url();?>client/createLoginCredentials" class="btn btn-danger pull-right btn-sm"><strong><i class="fa fa-times"></i> Cancel</strong></a>
                </div>
              </div>


            </div>
            <div class="col-md-4">
              <div class="well" style="height:228px;">
                <h4>Note:</h4>
                <p class="text-justify">You can create Sub Client Login Credentials here by filling the given fields.</p>
              </div>
            </div>
          </form>

        </div>


      </div>

    </div>
    <br><br>
  </div>
</div>
</div>