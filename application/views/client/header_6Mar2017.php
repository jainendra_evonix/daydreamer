<!-- SITE HEADER -->


<body id="myPage" ng-app="postApp" ng-controller="postController as ctrl">
<header class="cd-auto-hide-header">

	<!-- Navbar -->
    <div class="main-nav-container">
      
	    
	   
	    <div class="container-fluid navbar navbar-default main-nav" role="navigation">
	      <div class="navbar-header">
	        <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
	          <span class="sr-only">Toggle navigation</span>
	          <span class="icon-bar"></span>
	          <span class="icon-bar"></span>
	          <span class="icon-bar"></span>
	        </button>
	        <a class="navbar-brand" href="<?php echo base_url(); ?>user/index">
	          <!-- <img src="images/logo.png" class="logotop"> -->
	          <p class="logo-text">DAYDREAMER</span></p>
	        </a>
	      </div>

	      <div class="navbar-collapse collapse">
	       

	        <!-- Right nav -->
	        <ul class="nav navbar-nav navbar-right">
	         <!-- <li class="active"><a href="#">View All Jobs</a></li>
	          <li><a href="#">Blog</a></li>
	          <li><a href="#">How to apply</a></li>
	          <li><a href="#">Post CV</a></li>
	          <li><a href="#">Company Profiles <span class="caret"></span></a>
	            <ul class="dropdown-menu">
	              <li><a href="#">Action</a></li>
	              <li><a href="#">Another action</a></li>
	              <li><a href="#">Something else here</a></li>
	              <li class="divider"></li>
	              <li class="dropdown-header">Nav header</li>
	              <li><a href="#">A sub menu <span class="caret"></span></a>
	                <ul class="dropdown-menu">
	                  <li><a href="#">Action</a></li>
	                  <li><a href="#">Another action</a></li>
	                  <li><a href="#">Something else here</a></li>
	                  <li class="disabled"><a class="disabled" href="#">Disabled item</a></li>
	                  <li><a href="#">One more link</a></li>
	                </ul>
	              </li>
	              <li><a href="#">A separated link</a></li>
	            </ul>
	          </li> -->
	         <?php
            $client_session_data=$this->session->userdata(base_url().'login_session');
            $client_name=$client_session_data['first_name'].' '.$client_session_data['last_name'];
	         ?>
	         <?php if(!empty($client_session_data)){?>
	           <li class="dropdown">
                        <a data-toggle="dropdown" class="dropdown-toggle" href="#">
                            <img alt="" src="<?php echo base_url();?>client_assets/images/avatar1_small.jpg">
                            <span class="username"><?php echo $client_name;?></span>
                            <b class="caret"></b>
                        </a>
                        <ul class="dropdown-menu extended logout">
                            <li><a href="<?php echo base_url();?>onlineForm"><i class=" fa fa-suitcase"></i>Profile</a></li>
                            <li><a href="#"><i class="fa fa-cog"></i> Settings</a></li>
                            <!-- <li><a href="<?php echo base_url();?>client/logout"><i class="fa fa-key"></i> Log Out</a></li> -->
                        </ul>
                    </li>

           
	          
	          <?php }?>
	          <li><a href="<?php echo base_url(); ?>client/logout"><i class="fa fa-power-off" aria-hidden="true"></i>&nbsp;
 Log Out</a></li>
	        </ul>

	      </div><!--/.nav-collapse -->
	    </div>
    </div>

	<!-- <div class="logo"><a href="#0"><img src="img/cd-logo.svg" alt="Logo"></a></div>

	<nav class="cd-primary-nav">
		<a href="#cd-navigation" class="nav-trigger">
			<span>
				<em aria-hidden="true"></em>
				Menu
			</span>
		</a>

		<ul id="cd-navigation">
			<li><a href="#0">The team</a></li>
			<li><a href="#0">Our Products</a></li>
			<li><a href="#0">Our Services</a></li>
			<li><a href="#0">Shopping tools</a></li>
			<li><a href="#0">Contact Us</a></li>
		</ul>
	</nav> -->
</header> <!-- .cd-auto-hide-header -->