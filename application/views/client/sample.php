<?php include 'header.php' ?>

        <section class="container-fluid">
            <div class="row" id="topbanner">
                <div class="col-md-6 col-md-offset-3">
                    <h2 class="text-center">Customer Online Form</h2>
                    <p class="text-center">Lorem ipsum dolor sit amet, consectetur adipisicing elit.</p>
                </div>
            </div>

            <div class="row" id="formsteps">
                <div class="col-md-10 col-md-offset-2">
                    <ul class="breadcrumbs-alt">
                        <li>
                            <a class="current" href="client.php">Basic Information</a>
                        </li>
                        <li>
                            <a href="education.php">Education</a>
                        </li>
                        <li>
                            <a href="pre-employment.php">Pre-Employment</a>
                        </li>
                        <li>
                            <a href="employment.php">Employment Information</a>
                        </li>
                        <li>
                            <a href="miscellaneous.php">Miscellaneous</a>
                        </li>
                        <li>
                            <a href="social-media.php">Social Media</a>
                        </li>
                    </ul>
                </div>
            </div>
        </section>

        <!--main content start-->
        <section id="" class="container">
            <section class="wrapper">
                <!-- page start-->
                <div class="row">
                    <div class="col-md-8 col-md-offset-2">
                        <h3 class="form-heading">Educational Information</h3>
                        <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit.</p>

                        <section class="panel">
                            <div class="panel-body">
                                <form class="form-horizontal bucket-form" method="get">
                                    <div class="form-group">
                                        <label class="col-sm-3 control-label">Gender</label>
                                        <div class="col-sm-6 icheck">
                                            <div class="square single-row">
                                                <div class="">
                                                    <input tabindex="3" type="radio"  name="demo-radio">
                                                    <label>Male </label>
                                                </div>
                                                <div class="">
                                                    <input tabindex="3" type="radio"  name="demo-radio">
                                                    <label>Female </label>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="control-label col-md-3">Date Of Birth</label>
                                        <div class="col-md-6">
                                            <input class="form-control form-control-inline input-medium default-date-picker"  size="16" type="text" value="" />
                                            <span class="help-block">Select date</span>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-sm-3 control-label">Nationality</label>
                                        <div class="col-sm-6">
                                            <input type="text"  class="form-control" placeholder="Enter your nationality">
                                        </div>
                                        <div class="col-md-3">
                                            <span class="help-block">Display additional instructions here.</span>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-sm-3 control-label">Ethnic Origin</label>
                                        <div class="col-sm-6">
                                            <input type="text"  class="form-control" placeholder="Enter your ethnic origin">
                                        </div>
                                        <div class="col-md-3">
                                            <span class="help-block">Display additional instructions here.</span>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-sm-3 control-label">Disabilities</label>
                                        <div class="col-sm-6 icheck">
                                            <div class="square single-row">
                                                <div class="">
                                                    <input tabindex="3" type="radio"  name="demo-radio">
                                                    <label>Yes </label>
                                                </div>
                                                <div class="">
                                                    <input tabindex="3" type="radio"  name="demo-radio">
                                                    <label>No </label>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-sm-3 control-label">If yes, please provide info</label>
                                        <div class="col-sm-6">
                                            <textarea class="form-control" rows="3"></textarea>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-sm-3 control-label">Permitted to work in UK and Ireland</label>
                                        <div class="col-sm-6 icheck">
                                            <div class="square single-row">
                                                <div class="">
                                                    <input tabindex="3" type="radio"  name="demo-radio">
                                                    <label>Yes </label>
                                                </div>
                                                <div class="">
                                                    <input tabindex="3" type="radio"  name="demo-radio">
                                                    <label>No </label>
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="form-group last">
                                        <label class="control-label col-md-3">Job Type</label>
                                        <div class="col-md-6">
                                            <select name="country" class="multi-select" multiple="" id="my_multi_select3" >
                                                <option value="1">Accounting</option>
                                                <option value="2">Electronics</option>
                                                <option value="3">Management &amp; Executive</option>
                                                <option value="4">Automotive</option>
                                                <option value="5">Energy &amp; Utilities</option>
                                                <option value="6">Manufacturing</option>
                                                <option value="7">Aviation</option>
                                                <option value="8">Engineering</option>
                                                <option value="9">Marketing</option>
                                                <option value="10">Banking &amp; Finance</option>
                                                <option value="11">Graduate</option>
                                                <option value="12">Media</option>
                                                <option value="13">Charity &amp; Voluntary Work</option>
                                                <option value="14">Health &amp; Safety</option>
                                                <option value="15">Military</option>
                                                <option value="16">Construction</option>
                                                <option value="17">Health/Healthcare/Social care</option>
                                                <option value="18">Oil &amp; Gas</option>
                                                <option value="19">Consultancy</option>
                                                <option value="20">Human Resources</option>
                                                <option value="21">Production &amp; Operations</option>
                                                <option value="22">Customer Service</option>
                                                <option value="23">Information Technology</option>
                                                <option value="24">Public Sector</option>
                                                <option value="25">Defence</option>
                                                <option value="26">Insurance</option>
                                                <option value="27">Purchasing</option>
                                                <option value="28">Distribution</option>
                                                <option value="29">Legal</option>
                                                <option value="30">Recruitment</option>
                                                <option value="31">Education</option>
                                                <option value="32">Logistics &amp; Transport</option>
                                                <option value="33">Retail</option>
                                                <option value="34">Sales</option>
                                                <option value="35">Scientific</option>
                                                <option value="36">Secretarial &amp; Administration</option>
                                                <option value="37">Telecommunications</option>
                                                <option value="38">Training</option>
                                                <option value="39">Travel, Catering &amp; Hospitality</option>
                                            </select>
                                            
                                            <div class="help-block"><span class="label label-danger">NOTE!</span> Please select minimum of 1 and maximum of 4</div>
                                        </div>
                                    </div>
                                    
                                </form>
                            </div>
                        </section>
                    </div>
                </div>
                <!-- page end-->


        <div class="row">
            <div class="col-md-12">
                <section class="panel">
                    <header class="panel-heading">
                        Spinner
                        <span class="tools pull-right">
                            <a class="fa fa-chevron-down" href="javascript:;"></a>
                            <a class="fa fa-cog" href="javascript:;"></a>
                            <a class="fa fa-times" href="javascript:;"></a>
                        </span>
                    </header>
                    <div class="panel-body">
                        <form action="#" class="form-horizontal ">
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label class="control-label col-md-3">Spinner 1</label>
                                        <div class="col-md-5">
                                            <div id="spinner1">
                                                <div class="input-group input-small">
                                                    <input type="text" class="spinner-input form-control" maxlength="3" readonly>
                                                    <div class="spinner-buttons input-group-btn btn-group-vertical">
                                                        <button type="button" class="btn spinner-up btn-xs btn-default">
                                                            <i class="fa fa-angle-up"></i>
                                                        </button>
                                                        <button type="button" class="btn spinner-down btn-xs btn-default">
                                                            <i class="fa fa-angle-down"></i>
                                                        </button>
                                                    </div>
                                                </div>
                                            </div>
                                            <span class="help-block">
                                                basic example
                                            </span>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label class="control-label col-md-3">Spinner 2</label>
                                        <div class="col-md-5">
                                            <div id="spinner2">
                                                <div class="input-group input-small">
                                                    <input type="text" class="spinner-input form-control" maxlength="3" readonly>
                                                    <div class="spinner-buttons input-group-btn btn-group-vertical">
                                                        <button type="button" class="btn spinner-up btn-xs btn-info">
                                                            <i class="fa fa-angle-up"></i>
                                                        </button>
                                                        <button type="button" class="btn spinner-down btn-xs btn-info">
                                                            <i class="fa fa-angle-down"></i>
                                                        </button>
                                                    </div>
                                                </div>
                                            </div>
                                            <span class="help-block">
                                                disabled state
                                            </span>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label class="control-label col-md-3">Spinner 3</label>
                                        <div class="col-md-9">
                                            <div id="spinner3">
                                                <div class="input-group" style="width:150px;">
                                                    <input type="text" class="spinner-input form-control" maxlength="3" readonly>
                                                    <div class="spinner-buttons input-group-btn">
                                                        <button type="button" class="btn btn-default spinner-up">
                                                            <i class="fa fa-angle-up"></i>
                                                        </button>
                                                        <button type="button" class="btn btn-default spinner-down">
                                                            <i class="fa fa-angle-down"></i>
                                                        </button>
                                                    </div>
                                                </div>
                                            </div>
                                            <span class="help-block">
                                               with max value: 10
                                           </span>
                                       </div>
                                   </div>
                               </div>
                               <div class="col-md-6">
                                <div class="form-group ">
                                    <label class="control-label col-md-3">Spinner 4</label>
                                    <div class="col-md-9">
                                        <div id="spinner4">
                                            <div class="input-group" style="width:150px;">
                                                <div class="spinner-buttons input-group-btn">
                                                    <button type="button" class="btn spinner-up btn-primary">
                                                        <i class="fa fa-plus"></i>
                                                    </button>
                                                </div>
                                                <input type="text" class="spinner-input form-control" maxlength="3" readonly>
                                                <div class="spinner-buttons input-group-btn">
                                                    <button type="button" class="btn spinner-down btn-warning">
                                                        <i class="fa fa-minus"></i>
                                                    </button>
                                                </div>
                                            </div>
                                        </div>
                                        <span class="help-block">
                                            with step: 5
                                        </span>
                                    </div>
                                </div>

                            </div>
                        </div>

                    </form>
                </div>
            </section>

                </div>
            </div>
            <div class="row">
                <div class="col-md-12">
                    <section class="panel">
                        <header class="panel-heading">
                            Date Pickers
                            <span class="tools pull-right">
                                <a class="fa fa-chevron-down" href="javascript:;"></a>
                                <a class="fa fa-cog" href="javascript:;"></a>
                                <a class="fa fa-times" href="javascript:;"></a>
                            </span>
                        </header>
                        <div class="panel-body">
                            <form action="#" class="form-horizontal ">

                                <div class="form-group">
                                    <label class="control-label col-md-3">Default Datepicker</label>
                                    <div class="col-md-4 col-xs-11">
                                        <input class="form-control form-control-inline input-medium default-date-picker"  size="16" type="text" value="" />
                                        <span class="help-block">Select date</span>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="control-label col-md-3">Start with years viewMode</label>
                                    <div class="col-md-4 col-xs-11">

                                        <div data-date-viewmode="years" data-date-format="dd-mm-yyyy" data-date="12-02-2012"  class="input-append date dpYears">
                                            <input type="text" readonly="" value="12-02-2012" size="16" class="form-control">
                                            <span class="input-group-btn add-on">
                                                <button class="btn btn-primary" type="button"><i class="fa fa-calendar"></i></button>
                                            </span>
                                        </div>
                                        <span class="help-block">Select date</span>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="control-label col-md-3">Months Only</label>
                                    <div class="col-md-4 col-xs-11">
                                        <div data-date-minviewmode="months" data-date-viewmode="years" data-date-format="mm/yyyy" data-date="102/2012"  class="input-append date dpMonths">
                                            <input type="text" readonly="" value="02/2012" size="16" class="form-control">
                                            <span class="input-group-btn add-on">
                                                <button class="btn btn-primary" type="button"><i class="fa fa-calendar"></i></button>
                                            </span>
                                        </div>


                                        <span class="help-block">Select month only</span>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="control-label col-md-3">Date Range</label>
                                    <div class="col-md-4">
                                        <div class="input-group input-large" data-date="13/07/2013" data-date-format="mm/dd/yyyy">
                                            <input type="text" class="form-control dpd1" name="from">
                                            <span class="input-group-addon">To</span>
                                            <input type="text" class="form-control dpd2" name="to">
                                        </div>
                                        <span class="help-block">Select date range</span>
                                    </div>
                                </div>

                            </form>
                        </div>
                    </section>
                    <section class="panel">
                        <header class="panel-heading">
                            Datetime Pickers
                            <span class="tools pull-right">
                                <a class="fa fa-chevron-down" href="javascript:;"></a>
                                <a class="fa fa-cog" href="javascript:;"></a>
                                <a class="fa fa-times" href="javascript:;"></a>
                            </span>
                        </header>
                        <div class="panel-body">
                            <form class="form-horizontal  " action="#">
                                <div class="form-group">
                                    <label class="control-label col-md-3">Default input Datetimepicker</label>
                                    <div class="col-md-4">
                                        <input size="16" type="text" value="2012-06-15 14:45" readonly class="form_datetime form-control">
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label class="control-label col-md-3"> Component Datetimepicker</label>
                                    <div class="col-md-4">
                                        <div class="input-group date form_datetime-component">
                                            <input type="text" class="form-control" readonly="" size="16">
                                            <span class="input-group-btn">
                                                <button type="button" class="btn btn-primary date-set"><i class="fa fa-calendar"></i></button>
                                            </span>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="control-label col-md-3">Advance Datetimepicker</label>
                                    <div class="col-md-4">
                                        <div data-date="2012-12-21T15:25:00Z" class="input-group date form_datetime-adv">
                                            <input type="text" class="form-control" readonly="" size="16">
                                            <div class="input-group-btn">
                                                <button type="button" class="btn btn-primary date-reset"><i class="fa fa-times"></i></button>
                                                <button type="button" class="btn btn-warning date-set"><i class="fa fa-calendar"></i></button>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label class="control-label col-md-3">Meridian Format</label>
                                    <div class="col-md-4">
                                        <div data-date="2012-12-21T15:25:00Z" class="input-group date form_datetime-meridian">
                                            <input type="text" class="form-control" readonly="" size="16">
                                            <div class="input-group-btn">
                                                <button type="button" class="btn btn-primary date-reset"><i class="fa fa-times"></i></button>
                                                <button type="button" class="btn btn-warning date-set"><i class="fa fa-calendar"></i></button>
                                            </div>
                                        </div>
                                    </div>
                                </div>



                            </form>
                        </div>
                    </section>

                  <section class="panel">
                    <header class="panel-heading">
                        Advanced File Input
                        <span class="tools pull-right">
                            <a class="fa fa-chevron-down" href="javascript:;"></a>
                            <a class="fa fa-cog" href="javascript:;"></a>
                            <a class="fa fa-times" href="javascript:;"></a>
                        </span>
                    </header>
                    <div class="panel-body">
                        <form action="#" class="form-horizontal ">
                            <div class="form-group">
                                <label class="control-label col-md-3">Default</label>
                                <div class="col-md-4">
                                    <input type="file" class="default" />
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="control-label col-md-3">Without input</label>
                                <div class="controls col-md-9">
                                    <div class="fileupload fileupload-new" data-provides="fileupload">
                                        <span class="btn btn-white btn-file">
                                            <span class="fileupload-new"><i class="fa fa-paper-clip"></i> Select file</span>
                                            <span class="fileupload-exists"><i class="fa fa-undo"></i> Change</span>
                                            <input type="file" class="default" />
                                        </span>
                                        <span class="fileupload-preview" style="margin-left:5px;"></span>
                                        <a href="#" class="close fileupload-exists" data-dismiss="fileupload" style="float: none; margin-left:5px;"></a>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group last">
                                <label class="control-label col-md-3">Image Upload</label>
                                <div class="col-md-9">
                                    <div class="fileupload fileupload-new" data-provides="fileupload">
                                        <div class="fileupload-new thumbnail" style="width: 200px; height: 150px;">
                                            <img src="http://www.placehold.it/200x150/EFEFEF/AAAAAA&amp;text=no+image" alt="" />
                                        </div>
                                        <div class="fileupload-preview fileupload-exists thumbnail" style="max-width: 200px; max-height: 150px; line-height: 20px;"></div>
                                        <div>
                                         <span class="btn btn-white btn-file">
                                             <span class="fileupload-new"><i class="fa fa-paper-clip"></i> Select image</span>
                                             <span class="fileupload-exists"><i class="fa fa-undo"></i> Change</span>
                                             <input type="file" class="default" />
                                         </span>
                                         <a href="#" class="btn btn-danger fileupload-exists" data-dismiss="fileupload"><i class="fa fa-trash"></i> Remove</a>
                                     </div>
                                 </div>
                                 <span class="label label-danger">NOTE!</span>
                                 <span>
                                   Attached image thumbnail is
                                   supported in Latest Firefox, Chrome, Opera,
                                   Safari and Internet Explorer 10 only
                               </span>
                           </div>
                       </div>

                   </form>
               </div>
           </section>
       </div>
   </div>

   <div class="row">
    <div class="col-md-12">
        <section class="panel">
            <header class="panel-heading">
                Tags Input
                <span class="tools pull-right">
                    <a href="javascript:;" class="fa fa-chevron-down"></a>
                    <a href="javascript:;" class="fa fa-cog"></a>
                    <a href="javascript:;" class="fa fa-times"></a>
                </span>
            </header>
            <div class="panel-body">
                <form action="#" class="form-horizontal">
                    <div class="form-group">
                        <label class=" col-md-2 control-label">Default</label>
                        <div class="col-md-10">
                            <input id="tags_1" type="text" class="tags" value="php,ios,javascript,ruby,android,kindle" />
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-md-2 control-label">Fixed Width</label>
                        <div class="col-md-10">
                            <input id="tags_2" type="text" class="tags" value="tag1,tag2" />
                        </div>
                    </div>
                </form>
            </div>
        </section>
    </div>
</div>
<div class="row">
    <div class="col-md-12">
        <section class="panel">
            <header class="panel-heading">
                WYSIWYG Editors
                <span class="tools pull-right">
                    <a class="fa fa-chevron-down" href="javascript:;"></a>
                    <a class="fa fa-cog" href="javascript:;"></a>
                    <a class="fa fa-times" href="javascript:;"></a>
                </span>
            </header>
            <div class="panel-body">
                <form action="#" class="form-horizontal ">
                    <div class="form-group">
                        <label class="control-label col-md-2">WYSIHTML5 Editor</label>
                        <div class="col-md-10">
                            <textarea class="wysihtml5 form-control" rows="9"></textarea>
                        </div>
                    </div>
                </form>
            </div>
        </section>
    </div>
</div>
<div class="row">
    <div class="col-md-12">
        <section class="panel">
            <header class="panel-heading">
                Multiple Select
                <span class="tools pull-right">
                    <a class="fa fa-chevron-down" href="javascript:;"></a>
                    <a class="fa fa-cog" href="javascript:;"></a>
                    <a class="fa fa-times" href="javascript:;"></a>
                </span>
            </header>
            <div class="panel-body">
                <form action="#" class="form-horizontal ">
                    <div class="form-group">
                        <label class="control-label col-md-3">Default</label>
                        <div class="col-md-9">
                            <select multiple="multiple" class="multi-select" id="my_multi_select1" name="my_multi_select1[]">
                                <option>Dallas Cowboys</option>
                                <option>New York Giants</option>
                                <option selected>Philadelphia Eagles</option>
                                <option selected>Washington Redskins</option>
                                <option>Chicago Bears</option>
                                <option>Detroit Lions</option>
                                <option>Green Bay Packers</option>
                                <option>Minnesota Vikings</option>
                                <option selected>Atlanta Falcons</option>
                                <option>Carolina Panthers</option>
                                <option>New Orleans Saints</option>
                                <option>Tampa Bay Buccaneers</option>
                                <option>Arizona Cardinals</option>
                                <option>St. Louis Rams</option>
                                <option>San Francisco 49ers</option>
                                <option>Seattle Seahawks</option>
                            </select>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-md-3">Grouped Options</label>
                        <div class="col-md-9">
                            <select multiple="multiple" class="multi-select" id="my_multi_select2" name="my_multi_select2[]">
                                <optgroup label="NFC EAST">
                                    <option>Dallas Cowboys</option>
                                    <option>New York Giants</option>
                                    <option>Philadelphia Eagles</option>
                                    <option>Washington Redskins</option>
                                </optgroup>
                                <optgroup label="NFC NORTH">
                                    <option>Chicago Bears</option>
                                    <option>Detroit Lions</option>
                                    <option>Green Bay Packers</option>
                                    <option>Minnesota Vikings</option>
                                </optgroup>
                                <optgroup label="NFC SOUTH">
                                    <option>Atlanta Falcons</option>
                                    <option>Carolina Panthers</option>
                                    <option>New Orleans Saints</option>
                                    <option>Tampa Bay Buccaneers</option>
                                </optgroup>
                                <optgroup label="NFC WEST">
                                    <option>Arizona Cardinals</option>
                                    <option>St. Louis Rams</option>
                                    <option>San Francisco 49ers</option>
                                    <option>Seattle Seahawks</option>
                                </optgroup>
                            </select>
                        </div>
                    </div>
                    <!-- <div class="form-group last">
                        <label class="control-label col-md-3">Searchable</label>
                        <div class="col-md-9">
                            <select name="country" class="multi-select" multiple="" id="my_multi_select3" >
                                <option value="AF">Afghanistan</option>
                                <option value="AL">Albania</option>
                                <option value="DZ">Algeria</option>
                                <option value="AS">American Samoa</option>
                                <option value="AD">Andorra</option>
                                <option value="AO">Angola</option>
                                <option value="AI">Anguilla</option>
                                <option value="AQ">Antarctica</option>
                                <option value="AR">Argentina</option>
                                <option value="AM">Armenia</option>
                                <option value="AW">Aruba</option>
                            </select>
                        </div>
                    </div> -->
                </form>
            </div>
        </section>
    </div>
</div>



<div class="row">
    <div class="col-lg-12">
        <section class="panel">
            <header class="panel-heading">
                Select2 Components
                <span class="tools pull-right">
                    <a class="fa fa-chevron-down" href="javascript:;"></a>
                    <a class="fa fa-cog" href="javascript:;"></a>
                    <a class="fa fa-times" href="javascript:;"></a>
                </span>
            </header>
            <div class="panel-body">
                <form class="form-horizontal" action="#">
                    <div class="form-group">
                        <label class="col-lg-2 col-sm-2 control-label">Basic select </label>
                        <div class="col-lg-6">
                            <select id="e1" class="populate " style="width: 300px">
                                <optgroup label="Alaskan/Hawaiian Time Zone">
                                    <option value="AK">Alaska</option>
                                    <option value="HI">Hawaii</option>
                                </optgroup>
                                <optgroup label="Pacific Time Zone">
                                    <option value="CA">California</option>
                                    <option value="NV">Nevada</option>
                                    <option value="OR">Oregon</option>
                                    <option value="WA">Washington</option>
                                </optgroup>
                            </select>
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-lg-2 col-sm-2 control-label">Multi-Value Select</label>
                        <div class="col-lg-6">
                            <select multiple name="e9" id="e9" style="width:300px" class="populate">
                                <optgroup label="Alaskan/Hawaiian Time Zone">
                                    <option value="AK">Alaska</option>
                                    <option value="HI">Hawaii</option>
                                </optgroup>
                                <optgroup label="Pacific Time Zone">
                                    <option value="CA">California</option>
                                    <option value="NV">Nevada</option>
                                    <option value="OR">Oregon</option>
                                    <option value="WA">Washington</option>
                                </optgroup>
                            </select>
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-lg-2 col-sm-2 control-label">Placeholders </label>
                        <div class="col-lg-6">
                            <select id="e2" style="width:300px" class="populate placeholder">
                                <optgroup label="Alaskan/Hawaiian Time Zone">
                                    <option value="AK">Alaska</option>
                                    <option value="HI">Hawaii</option>
                                </optgroup>
                                <optgroup label="Pacific Time Zone">
                                    <option value="CA">California</option>
                                    <option value="NV">Nevada</option>
                                    <option value="OR">Oregon</option>
                                    <option value="WA">Washington</option>
                                </optgroup>
                            </select>
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-lg-2 col-sm-2 control-label">Minimum Input </label>
                        <div class="col-lg-6">
                            <select id="e3" style="width:300px" class="populate ">
                                <optgroup label="Alaskan/Hawaiian Time Zone">
                                    <option value="AK">Alaska</option>
                                    <option value="HI">Hawaii</option>
                                </optgroup>
                                <optgroup label="Pacific Time Zone">
                                    <option value="CA">California</option>
                                    <option value="NV">Nevada</option>
                                    <option value="OR">Oregon</option>
                                    <option value="WA">Washington</option>
                                </optgroup>
                            </select>
                        </div>
                    </div>

                </form>
            </div>
        </section>
    </div>
</div>
            </section>
        </section>
        <!--main content end-->





    <!--right sidebar start-->
    <div class="right-sidebar">
        <div class="search-row">
            <input type="text" placeholder="Search" class="form-control">
        </div>
        <div class="right-stat-bar">
            <ul class="right-side-accordion">
                <li class="widget-collapsible">
                    <a href="#" class="head widget-head red-bg active clearfix">
                        <span class="pull-left">work progress (5)</span>
                        <span class="pull-right widget-collapse"><i class="ico-minus"></i></span>
                    </a>
                    <ul class="widget-container">
                        <li>
                            <div class="prog-row side-mini-stat clearfix">
                                <div class="side-graph-info">
                                    <h4>Target sell</h4>
                                    <p>
                                        25%, Deadline 12 june 13
                                    </p>
                                </div>
                                <div class="side-mini-graph">
                                    <div class="target-sell">
                                    </div>
                                </div>
                            </div>
                            <div class="prog-row side-mini-stat">
                                <div class="side-graph-info">
                                    <h4>product delivery</h4>
                                    <p>
                                        55%, Deadline 12 june 13
                                    </p>
                                </div>
                                <div class="side-mini-graph">
                                    <div class="p-delivery">
                                        <div class="sparkline" data-type="bar" data-resize="true" data-height="30" data-width="90%" data-bar-color="#39b7ab" data-bar-width="5" data-data="[200,135,667,333,526,996,564,123,890,564,455]">
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="prog-row side-mini-stat">
                                <div class="side-graph-info payment-info">
                                    <h4>payment collection</h4>
                                    <p>
                                        25%, Deadline 12 june 13
                                    </p>
                                </div>
                                <div class="side-mini-graph">
                                    <div class="p-collection">
                                      <span class="pc-epie-chart" data-percent="45">
                                          <span class="percent"></span>
                                      </span>
                                  </div>
                              </div>
                          </div>
                          <div class="prog-row side-mini-stat">
                            <div class="side-graph-info">
                                <h4>delivery pending</h4>
                                <p>
                                    44%, Deadline 12 june 13
                                </p>
                            </div>
                            <div class="side-mini-graph">
                                <div class="d-pending">
                                </div>
                            </div>
                        </div>
                        <div class="prog-row side-mini-stat">
                            <div class="col-md-12">
                                <h4>total progress</h4>
                                <p>
                                    50%, Deadline 12 june 13
                                </p>
                                <div class="progress progress-xs mtop10">
                                    <div style="width: 50%" aria-valuemax="100" aria-valuemin="0" aria-valuenow="20" role="progressbar" class="progress-bar progress-bar-info">
                                        <span class="sr-only">50% Complete</span>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </li>
                </ul>
            </li>
            <li class="widget-collapsible">
                <a href="#" class="head widget-head terques-bg active clearfix">
                    <span class="pull-left">contact online (5)</span>
                    <span class="pull-right widget-collapse"><i class="ico-minus"></i></span>
                </a>
                <ul class="widget-container">
                    <li>
                        <div class="prog-row">
                            <div class="user-thumb">
                                <a href="#"><img src="images/avatar1_small.jpg" alt=""></a>
                            </div>
                            <div class="user-details">
                                <h4><a href="#">Jonathan Smith</a></h4>
                                <p>
                                    Work for fun
                                </p>
                            </div>
                            <div class="user-status text-danger">
                                <i class="fa fa-comments-o"></i>
                            </div>
                        </div>
                        <div class="prog-row">
                            <div class="user-thumb">
                                <a href="#"><img src="images/avatar1.jpg" alt=""></a>
                            </div>
                            <div class="user-details">
                                <h4><a href="#">Anjelina Joe</a></h4>
                                <p>
                                    Available
                                </p>
                            </div>
                            <div class="user-status text-success">
                                <i class="fa fa-comments-o"></i>
                            </div>
                        </div>
                        <div class="prog-row">
                            <div class="user-thumb">
                                <a href="#"><img src="images/chat-avatar2.jpg" alt=""></a>
                            </div>
                            <div class="user-details">
                                <h4><a href="#">John Doe</a></h4>
                                <p>
                                    Away from Desk
                                </p>
                            </div>
                            <div class="user-status text-warning">
                                <i class="fa fa-comments-o"></i>
                            </div>
                        </div>
                        <div class="prog-row">
                            <div class="user-thumb">
                                <a href="#"><img src="images/avatar1_small.jpg" alt=""></a>
                            </div>
                            <div class="user-details">
                                <h4><a href="#">Mark Henry</a></h4>
                                <p>
                                    working
                                </p>
                            </div>
                            <div class="user-status text-info">
                                <i class="fa fa-comments-o"></i>
                            </div>
                        </div>
                        <div class="prog-row">
                            <div class="user-thumb">
                                <a href="#"><img src="images/avatar1.jpg" alt=""></a>
                            </div>
                            <div class="user-details">
                                <h4><a href="#">Shila Jones</a></h4>
                                <p>
                                    Work for fun
                                </p>
                            </div>
                            <div class="user-status text-danger">
                                <i class="fa fa-comments-o"></i>
                            </div>
                        </div>
                        <p class="text-center">
                            <a href="#" class="view-btn">View all Contacts</a>
                        </p>
                    </li>
                </ul>
            </li>
            <li class="widget-collapsible">
                <a href="#" class="head widget-head purple-bg active">
                    <span class="pull-left"> recent activity (3)</span>
                    <span class="pull-right widget-collapse"><i class="ico-minus"></i></span>
                </a>
                <ul class="widget-container">
                    <li>
                        <div class="prog-row">
                            <div class="user-thumb rsn-activity">
                                <i class="fa fa-clock-o"></i>
                            </div>
                            <div class="rsn-details ">
                                <p class="text-muted">
                                    just now
                                </p>
                                <p>
                                    <a href="#">Jim Doe </a>Purchased new equipments for zonal office setup
                                </p>
                            </div>
                        </div>
                        <div class="prog-row">
                            <div class="user-thumb rsn-activity">
                                <i class="fa fa-clock-o"></i>
                            </div>
                            <div class="rsn-details ">
                                <p class="text-muted">
                                    2 min ago
                                </p>
                                <p>
                                    <a href="#">Jane Doe </a>Purchased new equipments for zonal office setup
                                </p>
                            </div>
                        </div>
                        <div class="prog-row">
                            <div class="user-thumb rsn-activity">
                                <i class="fa fa-clock-o"></i>
                            </div>
                            <div class="rsn-details ">
                                <p class="text-muted">
                                    1 day ago
                                </p>
                                <p>
                                    <a href="#">Jim Doe </a>Purchased new equipments for zonal office setup
                                </p>
                            </div>
                        </div>
                    </li>
                </ul>
            </li>
            <li class="widget-collapsible">
                <a href="#" class="head widget-head yellow-bg active">
                    <span class="pull-left"> shipment status</span>
                    <span class="pull-right widget-collapse"><i class="ico-minus"></i></span>
                </a>
                <ul class="widget-container">
                    <li>
                        <div class="col-md-12">
                            <div class="prog-row">
                                <p>
                                    Full sleeve baby wear (SL: 17665)
                                </p>
                                <div class="progress progress-xs mtop10">
                                    <div class="progress-bar progress-bar-success" role="progressbar" aria-valuenow="20" aria-valuemin="0" aria-valuemax="100" style="width: 40%">
                                        <span class="sr-only">40% Complete</span>
                                    </div>
                                </div>
                            </div>
                            <div class="prog-row">
                                <p>
                                    Full sleeve baby wear (SL: 17665)
                                </p>
                                <div class="progress progress-xs mtop10">
                                    <div class="progress-bar progress-bar-info" role="progressbar" aria-valuenow="20" aria-valuemin="0" aria-valuemax="100" style="width: 70%">
                                        <span class="sr-only">70% Completed</span>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </li>
                </ul>
            </li>
        </ul>
    </div>
</div>

<?php include 'footer.php' ?>