        <section id="footer">
            <div class="container">
                <div class="row">
                    <div class="col-md-4" id="fabout">
                        <h3>DAY <span>DREAMER</span></h3>
                        <p class="text-justify">Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
                        tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,
                        quis nostrud exercitation ullamco laboris.</p>
                        <img src="images/app.png">
                    </div>
                    <div class="col-md-3">
                        <h4>QUICK LINKS</h4>
                        <ul class="list-unstyled">
                            <li><a href="#">About Us</a></li>
                            <li><a href="#">Support</a></li>
                            <li><a href="#">License</a></li>
                            <li><a href="#">Affiliate</a></li>
                            <li><a href="#">Pricing</a></li>
                            <li><a href="#">Terms &amp; Condition</a></li>
                            <li><a href="#">Contact</a></li>
                        </ul>
                    </div>
                    <div class="col-md-3">
                        <h4>TRENDING JOBS</h4>
                        <ul class="list-unstyled">
                            <li><a href="#">UI Designer</a></li>
                            <li><a href="#">Frontend Developer</a></li>
                            <li><a href="#">Junior Tester</a></li>
                            <li><a href="#">Senior Teamleader</a></li>
                            <li><a href="#">iOS Developer</a></li>
                            <li><a href="#">Android Developer</a></li>
                            <li><a href="#">Technical Support</a></li>
                        </ul>
                    </div>
                    <div class="col-md-2">
                        <h4>FOLLOW US</h4>
                        <ul class="list-unstyled social">
                            <li><a href="#" class="fb"><i class="fa fa-facebook"></i> <strong>Facebook</strong></a></li>
                            <li><a href="#" class="tw"><i class="fa fa-twitter"></i> <strong>Twiter</strong></a></li>
                            <li><a href="#" class="gp"><i class="fa fa-google-plus"></i> <strong>Google Plus</strong></a></li>
                        </ul>
                    </div>
                </div>
            </div>
        </section>

        <footer class="footer-section">
          <div class="text-center">
              2016 © Day Dreamer | Crafted by Evonix Technologies
              <a href="#" class="go-top">
                  <i class="fa fa-angle-up"></i>
              </a>
          </div>
        </footer>
</section>

<!-- Placed js at the end of the document so the pages load faster -->

<!--Core js-->
<script src="js/jquery.js"></script>
<script src="js/jquery-1.8.3.min.js"></script>
<script src="bs3/js/bootstrap.min.js"></script>
<script src="js/jquery-ui-1.9.2.custom.min.js"></script>
<script class="include" type="text/javascript" src="js/jquery.dcjqaccordion.2.7.js"></script>
<script src="js/jquery.scrollTo.min.js"></script>
<script src="js/easypiechart/jquery.easypiechart.js"></script>
<script src="js/jQuery-slimScroll-1.3.0/jquery.slimscroll.js"></script>
<script src="js/jquery.nicescroll.js"></script>
<script src="js/jquery.nicescroll.js"></script>

<!-- <script src="js/bootstrap-switch.js"></script> -->

<script type="text/javascript" src="js/fuelux/js/spinner.min.js"></script>
<script type="text/javascript" src="js/bootstrap-fileupload/bootstrap-fileupload.js"></script>
<script type="text/javascript" src="js/bootstrap-wysihtml5/wysihtml5-0.3.0.js"></script>
<script type="text/javascript" src="js/bootstrap-wysihtml5/bootstrap-wysihtml5.js"></script>
<script type="text/javascript" src="js/bootstrap-datepicker/js/bootstrap-datepicker.js"></script>
<script type="text/javascript" src="js/bootstrap-datetimepicker/js/bootstrap-datetimepicker.js"></script>
<script type="text/javascript" src="js/bootstrap-daterangepicker/moment.min.js"></script>
<script type="text/javascript" src="js/bootstrap-daterangepicker/daterangepicker.js"></script>
<script type="text/javascript" src="js/bootstrap-colorpicker/js/bootstrap-colorpicker.js"></script>
<script type="text/javascript" src="js/bootstrap-timepicker/js/bootstrap-timepicker.js"></script>
<script type="text/javascript" src="js/jquery-multi-select/js/jquery.multi-select.js"></script>
<script type="text/javascript" src="js/jquery-multi-select/js/jquery.quicksearch.js"></script>

<script type="text/javascript" src="js/bootstrap-inputmask/bootstrap-inputmask.min.js"></script>

<script src="js/jquery-tags-input/jquery.tagsinput.js"></script>

<script src="js/select2/select2.js"></script>
<script src="js/select-init.js"></script>

<script type="text/javascript" language="javascript" src="js/advanced-datatable/js/jquery.dataTables.js"></script>
<script type="text/javascript" src="js/data-tables/DT_bootstrap.js"></script>
<!--common script init for all pages-->
<script src="js/scripts.js"></script>
<script src="js/dynamic_table_init.js"></script>
<!-- <script src="js/toggle-init.js"></script> -->

<script src="js/advanced-form.js"></script>
<!--Easy Pie Chart-->
<script src="js/easypiechart/jquery.easypiechart.js"></script>
<!--Sparkline Chart-->
<script src="js/sparkline/jquery.sparkline.js"></script>
<!--jQuery Flot Chart-->
<!-- <script src="js/flot-chart/jquery.flot.js"></script>
<script src="js/flot-chart/jquery.flot.tooltip.min.js"></script>
<script src="js/flot-chart/jquery.flot.resize.js"></script>
<script src="js/flot-chart/jquery.flot.pie.resize.js"></script> -->


</body>
</html>