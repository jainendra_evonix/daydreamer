<?php // echo "<pre>"; print_r($Jobname); exit; 

  error_reporting(0);

?>



<style type="text/css">
.help-block{

  color:red;
}
.autocomplete-suggestions {
  background-color: #fff;
  border: 1px solid #ddd;
  box-shadow: 0 3px 10px rgba(0, 0, 0, 0.2);
}
.autocomplete-suggestion {
  padding: 5px 12px;
}
.autocomplete-suggestion:hover {
  background-color: #333;
  color: #fff;
}
.textval {
  line-height: 30px;
  font-weight: 600;
  color: #333;
}
</style>

<?php //echo "<pre>"; print_r($getJobData); exit;

$requiredSkills =  $getJobData->skills; 
$k = explode(",", $requiredSkills);

     // echo "<pre>"; print_r($k);

   // exit;
?>
<!--main content start-->
<section id="adminsection" class="container">
  <section class="wrapper">
    <!-- page start-->
    <div class="row">
      <div class="col-md-12">

        <?php echo $this->session->flashdata('successmsg');  ?>
        <?php echo $this->session->flashdata('errormsg');  ?>
        <h4 class="form-heading"><strong>Preview</strong></h4>
        <!--  <p>Create and showcase all Job Openings. <a href="#" class="pull-right">Help <i class="fa fa-question-circle"></i></a></p> -->

        <br>
        <section class="panel">
          <div class="panel-body">
            <div class="row">
              <div class="col-md-12">
                <h4>Job Information</h4>
                <hr>
              </div>
              <form class="form-horizontal bucket-form" ng-submit="submitjobForm()" name="jobForm" id="sales" novalidate>
                <input type="hidden" name="sub_client_id" ng-model="job.sub_client_id">
                <input type="hidden" name="job_id" ng-model="job.job_id">
                <input type="hidden" name="skill_id" ng-model="job.skill_id">
                <input type="hidden" name="published" ng-model="job.published" value="0">
                

                <div class="col-md-6">
                  <div class="form-group">
                    <label class="col-sm-3 col-sm-offset-1 control-label">Posting Title:</label>
                    <div class="col-sm-7">
                          
                          <span class="textval"><?php echo $lastSavedJob->job_title; ?></span>

                    </div>

                  </div>


                  <div class="form-group">
                    <label class="col-sm-3 col-sm-offset-1 control-label">Contact Name:</label>
                    <div class="col-sm-7">
                     <span class="textval"><?php echo $lastSavedJob->contact_name; ?></span>
                    </div>
                  </div>


                

                                  <div class="form-group">
                                    <label class="col-sm-3 col-sm-offset-1 control-label">Job Opening Status:</label>
                                    <div class="col-sm-7">
                                      <span class="textval"><?php echo $lastSavedJob->status_name; ?></span>
                                      
                                         
                                      </div>
                                    </div>
                                  
                                     

                                      <div class="form-group">
                                        <label class="col-sm-3 col-sm-offset-1 control-label">Work Experience:</label>
                                        <div class="col-sm-7">
                                          
                                          
                                           <span class="textval"><?php echo $lastSavedJob->experience." "."Years"; ?></span>
                                          </div>
                                        </div>

                                        <div class="form-group">
                                         <label class="col-sm-3 col-sm-offset-1 control-label">Qualifications:</label>
                                         <div class="col-sm-7">
                                           
                                           <span class="textval"><?php echo $lastSavedJob->qualifications; ?></span>


                                         </div>
                                       </div>

                                       <div class="form-group">
                                         <label class="col-sm-3 col-sm-offset-1 control-label">Working Hours:</label>
                                         <div class="col-sm-7">
                                          
                                          <span class="textval"><?php echo $lastSavedJob->working_days." ".'Hours'; ?></span>

                                        </div>
                                      </div>

                                    

                                <div class="form-group">
                                      <label class="col-sm-3 col-sm-offset-1 control-label">Hours:</label>
                                      <div class="col-sm-7">
                                     <?php foreach($hours as $key) { ?>

                                      <span class="textval"><?php echo ucfirst($key->hours).' ,'; ?></span>
                                      <?php  }?>
                                        </div>
                                      </div>

                                   <div class="form-group">
                                      <label class="col-sm-3 col-sm-offset-1 control-label">Shifts:</label>
                                      <div class="col-sm-7">
                                     <?php foreach($shifts as $key) { ?>

                                      <span class="textval"><?php echo ucfirst($key->shifts).' ,'; ?></span>
                                      <?php  }?>
                                        </div>
                                      </div>
                                     
                                 <div class="form-group">
                                      <label class="col-sm-3 col-sm-offset-1 control-label">Contract Type:</label>
                                      <div class="col-sm-7">
                                     <?php foreach($contract as $key) { ?>

                                      <span class="textval"><?php echo ucfirst($key->contract_type).' ,'; ?></span>
                                      <?php  }?>
                                        </div>
                                      </div>


                                    </div>
                                    <div class="col-md-6">
                            
                                    <div class="form-group">
                                      <label class="col-sm-3 col-sm-offset-1 control-label">Number of Positions:</label>
                                      <div class="col-sm-7">
                                       <span class="textval"><?php echo $lastSavedJob->positions; ?></span>
                                      </div>
                                    </div>
                                  
                                      <div class="form-group">
                                       <label class="col-sm-3 col-sm-offset-1 control-label">Open Date:</label>
                                       <div class="col-sm-7">
                                       <span class="textval"><?php echo date('jS M Y',strtotime($lastSavedJob->open_date)); ?></span>
                                      </div>
                                    </div>

                                    <div class="form-group">
                                      <label class="col-sm-3 col-sm-offset-1 control-label">Job Type:</label>
                                      <div class="col-sm-7">
                                     <?php foreach($Jobname as $key) { ?>

                                      <span class="textval"><?php echo ucfirst($key->job_type).' ,'; ?></span>
                                      <?php  }?>
                                        </div>
                                      </div>

                                     

                                      <div class="form-group">
                                        <label class="col-sm-3 col-sm-offset-1 control-label">Country:</label>
                                        <div class="col-sm-7">
                                           <span class="textval"><?php echo $lastSavedJob->countryName; ?></span>
                                          </div>
                                        </div>
                                    
                                     <div class="form-group">
                                        <label class="col-sm-3 col-sm-offset-1 control-label">State:</label>
                                        <div class="col-sm-7">
                                          
                                          <span class="textval"><?php echo $lastSavedJob->statename; ?></span>

                                        </div>
                                      </div>
                                     
                                      <div class="form-group">
                                        <label class="col-sm-3 col-sm-offset-1 control-label">City:</label>
                                        <div class="col-sm-7">
                                         <span class="textval"><?php echo $lastSavedJob->cityName; ?></span>

                                        </div>
                                      </div>


                                      <div class="form-group">
                                      <label class="col-sm-3 col-sm-offset-1 control-label">Min Salary:</label>
                                      <div class="col-sm-7">
                                        <!-- <div class="input-group"><span class="input-group-addon">$</span>
                                      </div> -->
                                       <span class="textval"><?php echo $lastSavedJob->min_salary;  ?></span>

                                    
                                    </div>

                                    </div>
                                     
      
                                      <div class="form-group">
                                        <label class="col-sm-3 col-sm-offset-1 control-label">Max Salary:</label>
                                        <div class="col-sm-7">
                                          <!-- <div class="input-group"><span class="input-group-addon">$</span>  </div>-->
                                           
                                           <span class="textval"><?php echo $lastSavedJob->max_salary; ?></span>
                                      
                                      </div>
                                     </div>



                                     <div class="form-group">
                                      <label class="col-sm-3 col-sm-offset-1 control-label">Currency:</label>
                                      <div class="col-sm-7">
                                       
                                        <span class="textval"><?php echo $lastSavedJob->currency_code; ?></span>
                                       
                                        </div>
                                      </div>





                                    <div class="form-group">
                                     <label class="col-sm-3 col-sm-offset-1 control-label">Company Profile:</label>
                                     <div class="col-sm-7">
                                       <span class="textval"><?php echo $lastSavedJob->company_profile; ?></span>
                                     </div>
                                   </div>

                                 </div>
                                 <div class="col-md-12">
                                  <br>
                                  <h4>Description</h4>
                                  <hr>
                                </div>
                                <div class="col-md-12">
                                  <div class="form-group">
                                    <label class="col-sm-2 control-label col-sm-2">Job Description:</label>
                                    <div class="col-sm-10">
                                    <p class="textval"><?php echo $lastSavedJob->job_desc; ?></p>

                                    </div>
                                  </div>
                                </div>

                                <hr>
                                <div class="col-md-12">
                                  <br>
                                  <h4>Attachments</h4>
                                  <hr>
                                </div>
                                <div class="col-md-6">
                                  <div class="form-group">
                                    <label class="control-label col-sm-3 col-sm-offset-1 text-right">Job Summary:</label>
                                    <div class="controls col-sm-7">
                                          
                                         <span class="textval"><?php echo $lastSavedJob->job_summary; ?></span>
                                          
                                         </div>
                                       </div>
                                        


                                         </div>
                                         <div class="col-md-12">
                                          <div class="form-group">
                                            <div class="col-sm-12">
                                             <a href="<?php echo base_url(); ?>client/publishJob?id=<?php echo $lastSavedJob->j_id; ?>" > <button type="button" class="btn btn-info pull-right btn-sm"><strong><i class="fa fa-save"></i>Publish</strong></button><span class="pull-right"> &nbsp; &nbsp; </span></a>

                                              <a href="<?php echo base_url();?>client/updateJob?id=<?php echo $lastSavedJob->j_id; ?>"><button type="button" class="btn btn-danger pull-right btn-sm"><strong><i class="fa fa-pencil"></i>Edit</strong></button></a>
                                            </div>
                                          </div>
                                        </div>
                                      </form>
                                    </div>
                                  </div>
                                </section>
                              </div>
                            </div>
                            <!-- page end-->

                          </section>
                        </section>
                        <!--main content end-->






                      </div>



