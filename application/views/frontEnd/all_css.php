<!doctype html>
<html lang="en" class="no-js">
<head>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
     <link rel="shortcut icon" href="<?php  echo base_url();?>user_assets/images/fevicon.jpg">
	<link href="https://fonts.googleapis.com/css?family=David+Libre|Hind:300,400,600,700" rel="stylesheet">
	<link href="https://fonts.googleapis.com/css?family=Lato" rel="stylesheet">
	<link href="https://fonts.googleapis.com/css?family=Sanchez" rel="stylesheet">
	<link href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css" rel="stylesheet">
	<link rel="stylesheet" href="<?php echo base_url();?>web_assets/font-awesome/css/font-awesome.min.css">

	<link rel="stylesheet" href="<?php echo base_url();?>web_assets/bootstrap/css/bootstrap.min.css">
  	<link rel="stylesheet" href="<?php echo base_url();?>web_assets/css/jquery.smartmenus.bootstrap.css">
	<!-- <link rel="stylesheet" href="css/reset.css"> -->
	<link rel="stylesheet" href="<?php echo base_url();?>web_assets/owl/owl.carousel.css">
	<link rel="stylesheet" href="<?php echo base_url();?>web_assets/css/style.css">
	 <script src="https://ajax.googleapis.com/ajax/libs/angularjs/1.5.0/angular.min.js"></script>
  	
	
   <title><?php echo isset($title) ? $title : 'Day Dreamer'; ?></title>
</head>