<!-- SITE FOOTER -->

<div class="ct_footer_bg ct_top">
	<div class="container-fluid">
	    <div class="row">
	        <div class="col-md-3 col-sm-6 footbox">
	          <a href="#myPage"><i class="ion-ios-search"></i> Search Jobs</a>
	        </div>
	        <div class="col-md-3 col-sm-6 footbox even">
	          <a href="#0"><i class="ion-ios-cloud-upload-outline"></i> Upload CV</a>
	        </div>
	        <div class="col-md-3 col-sm-6 footbox even">
	          <a href="#0"><i class="ion-ios-paper-outline"></i> Search CVs</a>
	        </div>
	        <div class="col-md-3 col-sm-6 footbox">
	          <a href="#0"><i class="ion-ios-email-outline"></i> Contact Us</a>
	        </div>
	    </div>
	</div>
</div>


<div class="ct_footer_bg">
	<div class="container">
	    <div class="row">
	        <div class="col-md-3 col-sm-6">
	            <div class="footer_col_1 widget">
	                <a href="#" class="footer-logo">DAYDREAMER</a>
	                  <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Consequatur, dolore, impedit eveniet necessitatibus voluptate distinctio.</p>
	                  <span class="footerdivider"></span>

	                  <div class="foo_get_qoute">
	                    <!-- <a href="contactus.php">GET In Touch</a> -->
	                    <p>Call us : +44 1800 1234 5678
	                    <br>Email : <a href="mailto:contact@daydreamer.com">contact@daydreamer.com</a></p>
	                  </div>
	              </div>
	          </div>
	          <div class="col-md-6">
	          	<div class="row">
		          <div class="col-md-4 col-sm-4">
		            <div class="foo_col_2 widget">
		                <h5>DayDreamer</h5>
		                  <ul class="clearfix">
		                    <li><a href="#0">About Us</a></li>
		                    <li><a href="#0">Contact Us</a></li>
		                    <li><a href="#0">Career Center</a></li>
		                    <li><a href="#0">Site Map</a></li>
		                  </ul>
		            </div>
		          </div>
		          
		          <div class="col-md-4 col-sm-4">
		            <div class="foo_col_2 widget">
	                	<h5>For Job Seekers</h5>
	                  	<ul class="clearfix">
							<li><a href="#0">Search Jobs</a></li>
							<li><a href="#0">Upload CV</a></li>
							<li><a href="#0">Login</a></li>
							<li><a href="#0">Help</a></li>
						</ul>
		            </div>
		          </div>
		          
		          <div class="col-md-4 col-sm-4">
		            <div class="foo_col_2 widget">
	                	<h5>For Employers</h5>
		                <ul class="clearfix">
		                    <li><a href="#0">Post Jobs</a></li>
		                    <li><a href="#0">Search CVs</a></li>
		                    <li><a href="#0">Advertise With Us</a></li>
		                    <li><a href="#0">Help</a></li>
		                </ul>
		            </div>
		          </div>

		          <div class="col-md-12 col-sm-12">
			          <ul class="list-inline implinks nav-justified">
			          	<li><a href="#0">About Us</a></li>
			          	<li><a href="#0">Blog</a></li>
			          	<li><a href="#0">Terms</a></li>
			          	<li><a href="#0">Privacy</a></li>
			          	<li><a href="#0">Cookies</a></li>
			          	<li><a href="#0">Careers</a></li>
			          	<li><a href="#0">Partners</a></li>
			          </ul>
		          </div>
		        </div>
	          </div>
	          <div class="col-md-3 col-sm-6">
	            <div class="foo_col_4 widget">
	                <h5>Mobile App</h5>
	                  <ul>
		                  <li>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Consequatur, dolore, impedit eveniet necessitatibus voluptate distinctio.</li>
	                      <li></li>
	                  </ul>
	                  <a href="#0"><img src="<?php echo base_url(); ?>web_assets/img/home_app_download_appstore.png"></a>
	                  <a href="#0"><img src="<?php echo base_url(); ?>web_assets/img/home_app_download_google.png"></a>
	              </div>
	          </div>
	          
	      </div>
	  </div>
	</div>

<div class="ct_copyright_bg">
	<div class="container">
	    <div class="row">
	        <div class="col-md-6">
	            <div class="copyright_text">
	                &copy; Daydreamer. All right reserved | Crafted by <a href="http://evonix.co/" target="_blank">Evonix Technologies</a>
	              </div>
	          </div>
	          <div class="col-md-6">
	            <div class="copyright_social_icon">
	                <ul class="list-unstyled">
	                	<li>
	                		<a href="#myPage" title="To Top">
								<span class="glyphicon glyphicon-chevron-up"></span>
							</a>
						</li>
	                    <li><a href="#."><i class="fa fa-instagram"></i></a></li>
	                    <li><a href="#."><i class="fa fa-linkedin"></i></a></li>
	                    <li><a href="#."><i class="fa fa-twitter"></i></a></li>
	                    <li><a href="#."><i class="fa fa-facebook"></i></a></li>
	                	<li>Find us on social media:</li>
	                  </ul>
	              </div>
	          </div>
	      </div>
	  </div>
</div>




</body>
</html>