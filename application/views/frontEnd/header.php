<!-- SITE HEADER -->


<body id="myPage">
<header class="cd-auto-hide-header">

	<!-- Navbar -->
    <div class="main-nav-container">
	    <div class="top-sub-nav">
	    	<a href="#">Are you an employer?</a>
	    	<span class="pull-right">Recruiting? Call us on <strong>01772 639042</strong></span>
	    </div>
	    <div class="container-fluid navbar navbar-default main-nav" role="navigation">
	      <div class="navbar-header">
	        <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
	          <span class="sr-only">Toggle navigation</span>
	          <span class="icon-bar"></span>
	          <span class="icon-bar"></span>
	          <span class="icon-bar"></span>
	        </button>
	        <a class="navbar-brand" href="index.php">
	          <!-- <img src="images/logo.png" class="logotop"> -->
	          <p class="logo-text">DAYDREAMER</span></p>
	        </a>
	      </div>

	      <div class="navbar-collapse collapse">
	        <!-- Left nav -->
	        <!-- <ul class="nav navbar-nav navbar-right">
	          <li><a href="index.php">Home</a></li>
	          <li><a href="#.">About Us <span class="caret"></span></a>
	            <ul class="dropdown-menu">
	              <li><a href="about-hillrange.php">About Hill Range School</a></li>
	              <li><a href="our-mission.php">Our Mission</a></li>
	              <li><a href="aim-objective.php">Aim &amp; Objective</a></li>
	              <li><a href="our-founder.php">Our Founder</a></li>
	              <li><a href="chairmans-message.php">Chairman's Message</a></li>
	              <li><a href="principals-message.php">Principal's Message</a></li>
	            </ul>
	          </li>
	          <li><a href="#.">Admission <span class="caret"></span></a>
	            <ul class="dropdown-menu">
	              <li><a href="why-boarding-school.php">Why Boarding School</a></li>
	              <li><a href="admission-pre-requirement.php">Admission Pre-Requirement</a></li>
	              <li><a href="fees-structure.php">Fees Structure</a></li>
	              <li><a href="clothing-outfit.php">Clothing and Personnel Outfit</a></li>
	            </ul>
	          </li>
	          <li><a href="#.">Curriculum <span class="caret"></span></a>
	            <ul class="dropdown-menu">
	              <li><a href="school-curriculum.php">School Curriculum</a></li>
	              <li><a href="course-description.php">Course Description</a></li>
	            </ul>
	          </li>
	          <li><a href="#.">Facilities <span class="caret"></span></a>
	            <ul class="dropdown-menu">
	              <li><a href="school-facilities.php">School Facilities</a></li>
	              <li><a href="salient-features.php">Salient Features</a></li>
	              <li><a href="dormitory.php">Dormitory</a></li>
	              <li><a href="mess.php">Mess</a></li>
	              <li><a href="health-care.php">Health Care for Students</a></li>
	              <li><a href="sports-facilities.php">Sports Facilities</a></li>
	            </ul>
	          </li><li><a href="#.">Campus Life <span class="caret"></span></a>
	            <ul class="dropdown-menu">
	              <li><a href="daily-routine.php">Daily Routine</a></li>
	              <li><a href="rules-regulations.php">Rules and Regulations</a></li>
	              <li><a href="gallery.php">Photo Gallery</a></li>
	            </ul>
	          </li>
	          <li><a href="contact-us.php">Contact Us</a></li>
	        </ul> -->

	        <!-- Right nav -->
	        <ul class="nav navbar-nav navbar-right">
	          <li class="active"><a href="#">View All Jobs</a></li>
	          <li><a href="#">Blog</a></li>
	          <li><a href="#">How to apply</a></li>
	          <li><a href="#">Post CV</a></li>
	          <li><a href="#">Company Profiles <span class="caret"></span></a>
	            <ul class="dropdown-menu">
	              <li><a href="#">Action</a></li>
	              <li><a href="#">Another action</a></li>
	              <li><a href="#">Something else here</a></li>
	              <li class="divider"></li>
	              <li class="dropdown-header">Nav header</li>
	              <li><a href="#">A sub menu <span class="caret"></span></a>
	                <ul class="dropdown-menu">
	                  <li><a href="#">Action</a></li>
	                  <li><a href="#">Another action</a></li>
	                  <li><a href="#">Something else here</a></li>
	                  <li class="disabled"><a class="disabled" href="#">Disabled item</a></li>
	                  <li><a href="#">One more link</a></li>
	                </ul>
	              </li>
	              <li><a href="#">A separated link</a></li>
	            </ul>
	          </li>
	          <li><a href="<?php echo base_url(); ?>user/login"><i class="fa fa-sign-in"></i> Login</a></li>
	          <li><a href="<?php echo base_url(); ?>user/registration"><i class="fa fa-user-plus"></i> Register</a></li>
	        </ul>

	      </div><!--/.nav-collapse -->
	    </div>
    </div>

	<!-- <div class="logo"><a href="#0"><img src="img/cd-logo.svg" alt="Logo"></a></div>

	<nav class="cd-primary-nav">
		<a href="#cd-navigation" class="nav-trigger">
			<span>
				<em aria-hidden="true"></em>
				Menu
			</span>
		</a>

		<ul id="cd-navigation">
			<li><a href="#0">The team</a></li>
			<li><a href="#0">Our Products</a></li>
			<li><a href="#0">Our Services</a></li>
			<li><a href="#0">Shopping tools</a></li>
			<li><a href="#0">Contact Us</a></li>
		</ul>
	</nav> -->
</header> <!-- .cd-auto-hide-header -->