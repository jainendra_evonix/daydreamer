<?php if($this->session->flashdata('succmsg')){ echo "<script> alert('Your email id has been verified. Login with your credentials'); </script>";  }?>

       

<section class="cd-hero">
	<div class="cd-hero-content">
		<!-- your content here -->
		<div class="container">
			<div class="row">
				<div class="col-md-12">
					<h2>Find your dream job</h2>
					<p>Start your search now...</p>

					  <form class="form-inline" method="post" action="<?php echo base_url(); ?>user/searchedJob">
					  	<div class="formbg">
						    <div class="form-group">
						      <!-- <label for="email">Email:</label> -->
						      <input type="text" class="form-control" name="keyword" id="keyword" placeholder="Job title, skill or company name">
						    </div>
						    <div class="form-group">
						      <!-- <label for="pwd">Password:</label> -->
						      <input type="text" class="form-control" name="location" id="location" placeholder="Town, city or postcode">
						    </div>
						    <button type="submit" class="btn btn-success"><i class="ion-ios-search-strong"></i> Search</button>
					    </div>
					  </form>

					  <ul class="list-inline list-unstyled trending">
					  	<li>Popular searches: </li>
					  	<li><a href="#0">IT</a></li>
					  	<li><a href="#0">Engineering</a></li>
					  	<li><a href="#0">Management</a></li>
					  	<li><a href="#0">Retail</a></li>
					  	<li><a href="#0">Accounts &amp; Finance</a></li>
					  	<li><a href="#0">Sales</a></li>
					  	<li><a href="#0">Banking</a></li>
					  	<li><a href="#0">Digital Marketing</a></li>
					  	<li><a href="#0">Architecture</a></li>
					  </ul>
				</div>
			</div>
		</div>
	</div>
</section> <!-- .cd-hero -->


    


	<nav class="cd-secondary-nav">
		<ul>
			<li class="active"><a data-toggle="tab" href="#tab1">Jobs by Company</a></li>
			<li><a data-toggle="tab" href="#tab2">Jobs by Location</a></li>
			<li><a data-toggle="tab" href="#tab3">Jobs by Sector</a></li>
			<li><a data-toggle="tab" href="#tab4">Popular Searches</a></li>
			<li><a data-toggle="tab" href="#tab5">How to Apply</a></li>
		</ul>
	</nav> <!-- .cd-secondary-nav -->
	<div class="container-fluid sub-nav-hero">
	  <div class="tab-content container">
		    <div id="tab1" class="tab-pane fade in active">
			    <div class="row">
			    	<div class="col-md-3">
				      <ul class="list-unstyled tab-pane-list">
				      	<li><a href="#0">Accounts &amp; Finance <span>(324)</span> <i class="fa fa-angle-right pull-right"></i></a></li>
				      	<li><a href="#0">Engineering Jobs <i class="fa fa-angle-right pull-right"></i></a></li>
				      	<li><a href="#0">Recruitment <i class="fa fa-angle-right pull-right"></i></a></li>
				      	<li><a href="#0">Retail Jobs <i class="fa fa-angle-right pull-right"></i></a></li>
				      	<li><a href="#0">Graduate Jobs <i class="fa fa-angle-right pull-right"></i></a></li>
				      </ul>
				    </div>
				    <div class="col-md-3">
				      <ul class="list-unstyled tab-pane-list">
				      	<li><a href="#0">Marketing Jobs <i class="fa fa-angle-right pull-right"></i></a></li>
				      	<li><a href="#0">Sales Jobs <i class="fa fa-angle-right pull-right"></i></a></li>
				      	<li><a href="#0">Construction Jobs <i class="fa fa-angle-right pull-right"></i></a></li>
				      	<li><a href="#0">IT Jobs <i class="fa fa-angle-right pull-right"></i></a></li>
				      	<li><a href="#0">Management &amp; Executive <i class="fa fa-angle-right pull-right"></i></a></li>
				      </ul>
				    </div>
			    	<div class="col-md-3">
				      <ul class="list-unstyled tab-pane-list">
				      	<li><a href="#0">Accounts &amp; Finance <i class="fa fa-angle-right pull-right"></i></a></li>
				      	<li><a href="#0">Engineering Jobs <i class="fa fa-angle-right pull-right"></i></a></li>
				      	<li><a href="#0">Recruitment <i class="fa fa-angle-right pull-right"></i></a></li>
				      	<li><a href="#0">Retail Jobs <i class="fa fa-angle-right pull-right"></i></a></li>
				      	<li><a href="#0">Graduate Jobs <i class="fa fa-angle-right pull-right"></i></a></li>
				      </ul>
				    </div>
				    <div class="col-md-3">
				      <ul class="list-unstyled tab-pane-list">
				      	<li><a href="#0">Graduate Jobs <i class="fa fa-angle-right pull-right"></i></a></li>
				      	<li><a href="#0">Engineering Jobs <i class="fa fa-angle-right pull-right"></i></a></li>
				      	<li><a href="#0">Recruitment <i class="fa fa-angle-right pull-right"></i></a></li>
				      	<li><a href="#0">Retail Jobs <i class="fa fa-angle-right pull-right"></i></a></li>
				      	<li><a href="#0">More Jobs +</a></li>
				      </ul>
				    </div>
			    </div>
		    </div>
		    <div id="tab2" class="tab-pane fade">
		      <div class="row">
		    	<div class="col-md-3">
			      <ul class="list-unstyled tab-pane-list">
			      	<li><a href="#0">Accounts &amp; Finance <i class="fa fa-angle-right pull-right"></i></a></li>
			      	<li><a href="#0">Engineering Jobs <i class="fa fa-angle-right pull-right"></i></a></li>
			      	<li><a href="#0">Recruitment <i class="fa fa-angle-right pull-right"></i></a></li>
			      	<li><a href="#0">Retail Jobs <i class="fa fa-angle-right pull-right"></i></a></li>
			      	<li><a href="#0">Graduate Jobs <i class="fa fa-angle-right pull-right"></i></a></li>
			      </ul>
			    </div>
			    <div class="col-md-3">
			      <ul class="list-unstyled tab-pane-list">
			      	<li><a href="#0">Marketing Jobs <i class="fa fa-angle-right pull-right"></i></a></li>
			      	<li><a href="#0">Sales Jobs <i class="fa fa-angle-right pull-right"></i></a></li>
			      	<li><a href="#0">Construction Jobs <i class="fa fa-angle-right pull-right"></i></a></li>
			      	<li><a href="#0">IT Jobs <i class="fa fa-angle-right pull-right"></i></a></li>
			      	<li><a href="#0">Management &amp; Executive <i class="fa fa-angle-right pull-right"></i></a></li>
			      </ul>
			    </div>
		    	<div class="col-md-3">
			      <ul class="list-unstyled tab-pane-list">
			      	<li><a href="#0">Accounts &amp; Finance <i class="fa fa-angle-right pull-right"></i></a></li>
			      	<li><a href="#0">Engineering Jobs <i class="fa fa-angle-right pull-right"></i></a></li>
			      	<li><a href="#0">Recruitment <i class="fa fa-angle-right pull-right"></i></a></li>
			      	<li><a href="#0">Retail Jobs <i class="fa fa-angle-right pull-right"></i></a></li>
			      	<li><a href="#0">Graduate Jobs <i class="fa fa-angle-right pull-right"></i></a></li>
			      </ul>
			    </div>
			    <div class="col-md-3">
			      <ul class="list-unstyled tab-pane-list">
			      	<li><a href="#0">Graduate Jobs <i class="fa fa-angle-right pull-right"></i></a></li>
			      	<li><a href="#0">Engineering Jobs <i class="fa fa-angle-right pull-right"></i></a></li>
			      	<li><a href="#0">Recruitment <i class="fa fa-angle-right pull-right"></i></a></li>
			      	<li><a href="#0">Retail Jobs <i class="fa fa-angle-right pull-right"></i></a></li>
			      	<li><a href="#0">More Jobs +</a></li>
			      </ul>
			    </div>
		    </div>
		    </div>
		    <div id="tab3" class="tab-pane fade">
		      <div class="row">
		    	<div class="col-md-3">
			      <ul class="list-unstyled tab-pane-list">
			      	<li><a href="#0">Accounts &amp; Finance <i class="fa fa-angle-right pull-right"></i></a></li>
			      	<li><a href="#0">Engineering Jobs <i class="fa fa-angle-right pull-right"></i></a></li>
			      	<li><a href="#0">Recruitment <i class="fa fa-angle-right pull-right"></i></a></li>
			      	<li><a href="#0">Retail Jobs <i class="fa fa-angle-right pull-right"></i></a></li>
			      	<li><a href="#0">Graduate Jobs <i class="fa fa-angle-right pull-right"></i></a></li>
			      </ul>
			    </div>
			    <div class="col-md-3">
			      <ul class="list-unstyled tab-pane-list">
			      	<li><a href="#0">Marketing Jobs <i class="fa fa-angle-right pull-right"></i></a></li>
			      	<li><a href="#0">Sales Jobs <i class="fa fa-angle-right pull-right"></i></a></li>
			      	<li><a href="#0">Construction Jobs <i class="fa fa-angle-right pull-right"></i></a></li>
			      	<li><a href="#0">IT Jobs <i class="fa fa-angle-right pull-right"></i></a></li>
			      	<li><a href="#0">Management &amp; Executive <i class="fa fa-angle-right pull-right"></i></a></li>
			      </ul>
			    </div>
		    	<div class="col-md-3">
			      <ul class="list-unstyled tab-pane-list">
			      	<li><a href="#0">Accounts &amp; Finance <i class="fa fa-angle-right pull-right"></i></a></li>
			      	<li><a href="#0">Engineering Jobs <i class="fa fa-angle-right pull-right"></i></a></li>
			      	<li><a href="#0">Recruitment <i class="fa fa-angle-right pull-right"></i></a></li>
			      	<li><a href="#0">Retail Jobs <i class="fa fa-angle-right pull-right"></i></a></li>
			      	<li><a href="#0">Graduate Jobs <i class="fa fa-angle-right pull-right"></i></a></li>
			      </ul>
			    </div>
			    <div class="col-md-3">
			      <ul class="list-unstyled tab-pane-list">
			      	<li><a href="#0">Graduate Jobs <i class="fa fa-angle-right pull-right"></i></a></li>
			      	<li><a href="#0">Engineering Jobs <i class="fa fa-angle-right pull-right"></i></a></li>
			      	<li><a href="#0">Recruitment <i class="fa fa-angle-right pull-right"></i></a></li>
			      	<li><a href="#0">Retail Jobs <i class="fa fa-angle-right pull-right"></i></a></li>
			      	<li><a href="#0">More Jobs +</a></li>
			      </ul>
			    </div>
		    </div>
		    </div>
		    <div id="tab4" class="tab-pane fade">
		      <div class="row">
		    	<div class="col-md-3">
			      <ul class="list-unstyled tab-pane-list">
			      	<li><a href="#0">Accounts &amp; Finance <i class="fa fa-angle-right pull-right"></i></a></li>
			      	<li><a href="#0">Engineering Jobs <i class="fa fa-angle-right pull-right"></i></a></li>
			      	<li><a href="#0">Recruitment <i class="fa fa-angle-right pull-right"></i></a></li>
			      	<li><a href="#0">Retail Jobs <i class="fa fa-angle-right pull-right"></i></a></li>
			      	<li><a href="#0">Graduate Jobs <i class="fa fa-angle-right pull-right"></i></a></li>
			      </ul>
			    </div>
			    <div class="col-md-3">
			      <ul class="list-unstyled tab-pane-list">
			      	<li><a href="#0">Marketing Jobs <i class="fa fa-angle-right pull-right"></i></a></li>
			      	<li><a href="#0">Sales Jobs <i class="fa fa-angle-right pull-right"></i></a></li>
			      	<li><a href="#0">Construction Jobs <i class="fa fa-angle-right pull-right"></i></a></li>
			      	<li><a href="#0">IT Jobs <i class="fa fa-angle-right pull-right"></i></a></li>
			      	<li><a href="#0">Management &amp; Executive <i class="fa fa-angle-right pull-right"></i></a></li>
			      </ul>
			    </div>
		    	<div class="col-md-3">
			      <ul class="list-unstyled tab-pane-list">
			      	<li><a href="#0">Accounts &amp; Finance <i class="fa fa-angle-right pull-right"></i></a></li>
			      	<li><a href="#0">Engineering Jobs <i class="fa fa-angle-right pull-right"></i></a></li>
			      	<li><a href="#0">Recruitment <i class="fa fa-angle-right pull-right"></i></a></li>
			      	<li><a href="#0">Retail Jobs <i class="fa fa-angle-right pull-right"></i></a></li>
			      	<li><a href="#0">Graduate Jobs <i class="fa fa-angle-right pull-right"></i></a></li>
			      </ul>
			    </div>
			    <div class="col-md-3">
			      <ul class="list-unstyled tab-pane-list">
			      	<li><a href="#0">Graduate Jobs <i class="fa fa-angle-right pull-right"></i></a></li>
			      	<li><a href="#0">Engineering Jobs <i class="fa fa-angle-right pull-right"></i></a></li>
			      	<li><a href="#0">Recruitment <i class="fa fa-angle-right pull-right"></i></a></li>
			      	<li><a href="#0">Retail Jobs <i class="fa fa-angle-right pull-right"></i></a></li>
			      	<li><a href="#0">More Jobs +</a></li>
			      </ul>
			    </div>
		    </div>
		    </div>
		    <div id="tab5" class="tab-pane fade">
			    <div class="row">
			    	<div class="col-md-12">
				      <h3>How to Apply</h3>
				      <p>Ipsum, quaerat, vero, adipisci enim autem inventore eum maiores consectetur culpa molestiae cumque sed qui dolorem. Placeat, quae deleniti molestiae minima cupiditate quaerat sit est perspiciatis error iste. Ratione, minus, commodi, magni laborum doloribus libero ullam quos tenetur quis molestias ipsam consequuntur harum asperiores culpa nostrum omnis.</p>
				      <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Consequatur, dolore, impedit eveniet necessitatibus voluptate distinctio quam repellendus voluptates voluptatum inventore rem sapiente minus esse saepe iste harum architecto numquam quis vero dignissimos beatae est id libero adipisci enim odio natus commodi explicabo modi similique nesciunt deserunt vel consectetur velit omnis quaerat corrupti. Cumque, perspiciatis.</p>
				    </div>
				</div>
		    </div>
	  </div>
	</div>


<main class="">

	<section id="latestjobs">
		<div class="container">
			<div class="row">
				<div class="col-md-12">
					<h3 class="sheading">Popular Jobs</h3>
				</div>
			</div>
			<div class="row freelancer-list">
                <div class="col-md-6 col-sm-6 col-xs-12">
                    <div class="freelancer-wrap row-fluid clearfix">
                        <div class="col-md-3 text-center">
                            <div class="post-media">
                                <img class="img-responsive" src="<?php echo base_url(); ?>web_assets/img/companylogo/5.jpg" alt="">
                            </div>
                        </div><!-- end col -->

                        <div class="col-md-9">
                            <div class="rating">
                                <i class="fa fa-star"></i>
                                <i class="fa fa-star"></i>
                                <i class="fa fa-star"></i>
                                <i class="fa fa-star"></i>
                                <i class="fa fa-star"></i>
                            </div>
                            <h4><a href="#">Full Stack Web Developer</a></h4>
                            <p class="skillsreq">C# / ASP.NET / MVC</p>
                            <ul class="list-inline">
                                <li><small>£10 - £12 an hour</small></li>
                                <li><small>3-5 years experience</small></li>
                            </ul>
                            <p>Lorem ipsum dolor sit amet, non odio tincidunt ut ante, lorem a euismod suspendisse vel...</p>
                        </div><!-- end col -->
                        <a href="#" class="btn btn-primary">APPLY</a>
                    </div><!-- end freelancer-wrap -->
                </div><!-- end col -->

                <div class="col-md-6 col-sm-6 col-xs-12">
                    <div class="freelancer-wrap row-fluid clearfix">
                        <div class="col-md-3 text-center">
                            <div class="post-media">
                                <img class="img-responsive" src="<?php echo base_url(); ?>web_assets/img/companylogo/12.jpg" alt="">
                            </div>
                        </div><!-- end col -->

                        <div class="col-md-9">
                            <div class="rating">
                                <i class="fa fa-star"></i>
                                <i class="fa fa-star"></i>
                                <i class="fa fa-star"></i>
                                <i class="fa fa-star"></i>
                                <i class="fa fa-star"></i>
                            </div>
                            <h4><a href="#">Web Developer / PHP &amp; Drupal Ninja!</a></h4>
                            <p class="skillsreq">PHP, MySQL, Linux, Wordpress</p>
                            <ul class="list-inline">
                                <li><small>£35,000 per year</small></li>
                                <li><small>3-5 years experience</small></li>
                            </ul>
                            <p>Lorem ipsum dolor sit amet, non odio tincidunt ut ante, lorem a euismod suspendisse vel...</p>
                        </div><!-- end col -->
                        <a href="#" class="btn btn-primary">APPLY</a>
                    </div><!-- end freelancer-wrap -->
                </div><!-- end col -->

                <div class="col-md-6 col-sm-6 col-xs-12">
                    <div class="freelancer-wrap row-fluid clearfix">
                        <div class="col-md-3 text-center">
                            <div class="post-media">
                                <img class="img-responsive" src="<?php echo base_url(); ?>web_assets/img/companylogo/3.jpg" alt="">
                            </div>
                        </div><!-- end col -->

                        <div class="col-md-9">
                            <div class="rating">
                                <i class="fa fa-star"></i>
                                <i class="fa fa-star"></i>
                                <i class="fa fa-star"></i>
                                <i class="fa fa-star"></i>
                                <i class="fa fa-star"></i>
                            </div>
                            <h4><a href="#">Front-End UI Web Developer - ReactJS</a></h4>
                            <p class="skillsreq">TML, CSS, PHP, MySQL</p>
                            <ul class="list-inline">
                                <li><small>£10 - £12 an hour</small></li>
                                <li><small>3-5 years experience</small></li>
                            </ul>
                            <p>Lorem ipsum dolor sit amet, non odio tincidunt ut ante, lorem a euismod suspendisse vel...</p>
                        </div><!-- end col -->
                        <a href="#" class="btn btn-primary">APPLY</a>
                    </div><!-- end freelancer-wrap -->
                </div><!-- end col -->

                <div class="col-md-6 col-sm-6 col-xs-12">
                    <div class="freelancer-wrap row-fluid clearfix">
                        <div class="col-md-3 text-center">
                            <div class="post-media">
                                <img class="img-responsive" src="<?php echo base_url(); ?>web_assets/img/companylogo/6.jpg" alt="">
                            </div>
                        </div><!-- end col -->

                        <div class="col-md-9">
                            <div class="rating">
                                <i class="fa fa-star"></i>
                                <i class="fa fa-star"></i>
                                <i class="fa fa-star"></i>
                                <i class="fa fa-star"></i>
                                <i class="fa fa-star"></i>
                            </div>
                            <h4><a href="#">UX DESIGNER</a></h4>
                            <p class="skillsreq">Photoshop, Adwords, Wordpress</p>
                            <ul class="list-inline">
                                <li><small>£10 - £12 an hour</small></li>
                                <li><small>3 years experience</small></li>
                            </ul>
                            <p>Lorem ipsum dolor sit amet, non odio tincidunt ut ante, lorem a euismod suspendisse vel...</p>
                        </div><!-- end col -->
                        <a href="#" class="btn btn-primary">APPLY</a>
                    </div><!-- end freelancer-wrap -->
                </div><!-- end col -->

                <div class="col-md-12">
                	<a href="#0" class="btn btn-primary morejobs">View more</a>
                </div>
            </div>
		</div>
	</section>

	<section id="featuredemp">
		<div class="container">
			<div class="row">
				<div class="col-md-12">
					<h3 class="sheading">Featured employers</h3>
					<div class="owl-carousel owl-complogo">
					    <div class="item">
					    	<a href="#0"><img src="<?php echo base_url(); ?>web_assets/img/companylogo/14.jpg" class="img-responsive"></a>
					    	<a href="#0" class="companyname">Orbium</a>
					    </div>
					    <div class="item">
					    	<a href="#0"><img src="<?php echo base_url(); ?>web_assets/img/companylogo/2.jpg" class="img-responsive"></a>
					    	<a href="#0" class="companyname">Randstad</a>
					    </div>
					    <div class="item">
					    	<a href="#0"><img src="<?php echo base_url(); ?>web_assets/img/companylogo/3.jpg" class="img-responsive"></a>
					    	<a href="#0" class="companyname">Nielsen</a>
					    </div>
					    <div class="item">
					    	<a href="#0"><img src="<?php echo base_url(); ?>web_assets/img/companylogo/4.jpg" class="img-responsive"></a>
					    	<a href="#0" class="companyname">Care Control</a>
					    </div>
					    <div class="item">
					    	<a href="#0"><img src="<?php echo base_url(); ?>web_assets/img/companylogo/5.jpg" class="img-responsive"></a>
					    	<a href="#0" class="companyname">Accenture</a>
					    </div>
					    <div class="item">
					    	<a href="#0"><img src="<?php echo base_url(); ?>web_assets/img/companylogo/13.jpg" class="img-responsive"></a>
					    	<a href="#0" class="companyname">Visix</a>
					    </div>
					    <div class="item">
					    	<a href="#0"><img src="<?php echo base_url(); ?>web_assets/img/companylogo/7.jpg" class="img-responsive"></a>
					    	<a href="#0" class="companyname">Loyalty Lion</a>
					    </div>
					    <div class="item">
					    	<a href="#0"><img src="<?php echo base_url(); ?>web_assets/img/companylogo/8.jpg" class="img-responsive"></a>
					    	<a href="#0" class="companyname">Kaluma Travel</a>
					    </div>
					    <div class="item">
					    	<a href="#0"><img src="<?php echo base_url(); ?>web_assets/img/companylogo/15.jpg" class="img-responsive"></a>
					    	<a href="#0" class="companyname">Prysm</a>
					    </div>
					    <div class="item">
					    	<a href="#0"><img src="<?php echo base_url(); ?>web_assets/img/companylogo/10.jpg" class="img-responsive"></a>
					    	<a href="#0" class="companyname">Scott Dunn</a>
					    </div>
					    <div class="item">
					    	<a href="#0"><img src="<?php echo base_url(); ?>web_assets/img/companylogo/11.jpg" class="img-responsive"></a>
					    	<a href="#0" class="companyname">TUI Group</a>
					    </div>
					    <div class="item">
					    	<a href="#0"><img src="<?php echo base_url(); ?>web_assets/img/companylogo/12.jpg" class="img-responsive"></a>
					    	<a href="#0" class="companyname">Thatchers</a>
					    </div>
					</div>
				</div>
			</div>
		</div>
	</section>
</main> <!-- .cd-main-content -->

