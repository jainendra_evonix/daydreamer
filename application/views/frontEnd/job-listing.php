<?php  // echo "<pre>"; print_r($jobsByTitle); exit;
   
 //  echo $this->session->userdata('user_id'); exit;


?>
<style type="text/css">
  
 #loader {
  visibility: hidden;
  background-color: rgba(255,255,255,0.7);
  position: fixed;
  z-index: +100 !important;
  top: 0;
  right: 0;
  bottom: 0;
  left: 0;
}

#loader img {
  position: absolute;
  top:50%;
  left:50%;
}

.new {
  bottom: 4px;
  color: #fff;
  font-size: 10px;
  font-weight: 700;
  position: absolute;
  right: 3px;
  z-index: 1;
  text-transform: uppercase;
}
.freelancer-wrap {
  overflow: hidden;
}
.freelancer-wrap:after {
    content: '';
    position: absolute;
    background-color: #4CAF50;
    right: -62px;
    bottom: -62px;
    width: 100px;
    height: 100px;
    transform: rotate(45deg);
    z-index: 0;
}
.savealert {
  padding-top: 10px;
}
.savealert li a .fa {
    font-size: 22px;
    vertical-align: text-top;
}
.freelancer-wrap ul {
  border: none;
}
.freelancer-wrap h4 .label {
      font-size: 11px;
    padding: 1px 10px;
    margin-left: 15px;
    vertical-align: middle;
}

.fa.fa-star-o.alertsave{

color:#464861;


}
</style>
    <script type="text/javascript" src="//s7.addthis.com/js/300/addthis_widget.js#pubid=ra-591d68517aa68070"></script>

<!-- Go to www.addthis.com/dashboard to customize your tools -->
<!-- <script type="text/javascript" src="//s7.addthis.com/js/300/addthis_widget.js#pubid=ra-591d68517aa68070"></script> -->


<div class="cd-hero-inner">
  <div class="container">
    <div class="row">
      <div class="col-md-6 col-sm-6">
        <h1>Job Listing</h1>
      </div>
      <div class="col-md-6 col-sm-6">
        <div class="breadcmb"><a href="<?php echo base_url(); ?>user/index">Home</a> / <span>Job Search</span></div>
      </div>
    </div>
  </div>
</div>


<div id="loader">
                 <img  src="http://127.0.0.1/daydreamer/uploads/ajax-loader.gif">
               </div>

<div class="listpgWraper">
  <div class="container"> 
    
    <!-- Page Title start -->
    <div class="pageSearch">
      <div class="row">
        <div class="col-md-3"><a href="<?php if(@($this->session->userdata('user_id'))){ echo base_url().'user/resume'; } else{ echo base_url().'user/registration'; } ?>" class="btn"><i class="fa fa-file-text" aria-hidden="true"></i> Upload Your Resume</a></div>
        <div class="col-md-9">
        <form action="<?php echo base_url(); ?>user/searchedJob" method="post">
          <div class="searchform">
            <div class="row">
              <div class="col-md-6 col-sm-3">
                <input type="text" class="form-control" name="keyword" placeholder="Jobs by Title">
              </div>
             
              <div class="col-md-5 col-sm-2">
                
                 <input type="text" class="form-control" name="location" placeholder="Jobs by City">
              </div>
              <!-- <div class="col-md-3 col-sm-3">
                <select class="form-control">
                  <option>Min. Salary</option>
                </select>
              </div> -->
              <div class="col-md-1 col-sm-2 nopadding">
                <button class="btn" type="submit" ><i class="fa fa-search" aria-hidden="true"></i></button>
              </div>
            </div>
          </div>
        </div>
        </form>
      </div>

    </div>




    <!-- Page Title end --> 
    

  

 <?php if($this->session->flashdata('succmsg')){  ?>

<div class="alert alert-success">
   <strong>Success!</strong> <?php echo $this->session->flashdata('succmsg'); ?>
   <!-- <button type="button" class="close" data-dismiss="modal">&times;</button> -->
   <a type="button" class="close" data-dismiss="alert" aria-hidden="true">×</a>
</div>

<?php    } ?>


    <!-- Search Result and sidebar start -->
    <div class="row">
      <div class="col-md-3 col-sm-6"> 
        <!-- Side Bar start -->
        <div class="sidebar"> 

           <!-- <input type="checkbox" name="mycheck" value="checked">Demo -->



       <?php // echo "<pre>";  print_r($industries); exit; ?>

            <!-- Jobs By Industry -->
          <div class="widget">
            <h4 class="widget-title">Jobs By Industry</h4>
             
            <!--  <input type="checkbox" name="mycheck" value="checked"> Demo -->

            <ul class="optionlist">
             <?php if($industries){  

              foreach ($industries as $key ) { ?>
              <li>
                <input type="checkbox" name="industries[]" id="<?php echo $key->industry_name; ?>" value="<?php echo $key->industry; ?>" class="industries">
                <!-- <input type="checkbox" name="industries[]"> -->
                <label for="<?php echo $key->industry_name; ?>"></label>
                <?php echo $key->industry_name; ?> <span><?php echo $key->total_ind; ?></span>
               </li>
               <?php } }?>
               </ul>
               <a href="<?php echo base_url() ?>user/jobsByIndustry">View More</a> 
            </div>
          <!-- Jobs By Industry end --> 

              

          <!-- Jobs By Title -->
          <div class="widget">
            <h4 class="widget-title">Jobs By Title</h4>
            <?php if($jobsByTitle){ foreach ($jobsByTitle as $key ) {
             
             ?>
            <ul class="optionlist">
              <li>
                <input type="checkbox" name="jobTitle[]" id="<?php echo $key->job_title; ?>" class='jobsByTitle' value="<?php echo $key->job_title; ?>">
                <label for="<?php echo $key->job_title; ?>"></label>
                <?php echo $key->job_title; ?> <span><?php echo $key->total_title; ?></span> 
                </li>
              
            </ul>
            <?php  } }?>
            <!-- title end --> 
            <a href="#.">View More</a> </div>
          
          <!-- Jobs By City -->
          <div class="widget">
            <h4 class="widget-title">Jobs By City</h4>
              <?php if($jobsByCity) {

                foreach ($jobsByCity as $key){?>
            <ul class="optionlist">
              <li>
                <input type="checkbox" name="city[]" id="<?php echo $key->city; ?>" value="<?php echo $key->city;  ?>">
                <label for="<?php echo $key->city; ?>"></label>
               <?php echo $key->city; ?> <span><?php echo $key->total_city; ?></span> </li>

              
            </ul>
            <?php } }?>
            <a href="#.">View More</a> </div>
          <!-- Jobs By City end--> 
          
          <!-- Jobs By Experience -->
          <div class="widget">
            <h4 class="widget-title">Jobs By Experience</h4>
            <?php if($jobsByExperience) { 

                foreach ($jobsByExperience as $key) {
                 

             ?>
            <ul class="optionlist">
             
              <li>
                <input type="checkbox" name="checkname" id="<?php echo $key->job_type;  ?>" value="<?php echo $key->total_exp;  ?>">
                <label for="<?php echo $key->job_type;  ?>"></label>
               <?php echo $key->job_type; ?> <span><?php echo $key->total_exp; ?></span> 
                </li>
            </ul>

            <?php  } }?>
            <a href="#.">View More</a> </div>
          <!-- Jobs By Experience end --> 
          
         
          
          <!-- Top Companies -->
          <div class="widget">
            <h4 class="widget-title">Top Companies</h4>
            <ul class="optionlist">
              <li>
                <input type="checkbox" name="checkname" id="Envato">
                <label for="Envato"></label>
                Envato <span>12</span> </li>
              <li>
                <input type="checkbox" name="checkname" id="Themefores">
                <label for="Themefores"></label>
                Themefores <span>33</span> </li>
              <li>
                <input type="checkbox" name="checkname" id="GraphicRiver">
                <label for="GraphicRiver"></label>
                Graphic River <span>33</span> </li>
              <li>
                <input type="checkbox" name="checkname" id="Codecanyon">
                <label for="Codecanyon"></label>
                Codecanyon <span>33</span> </li>
              <li>
                <input type="checkbox" name="checkname" id="AudioJungle">
                <label for="AudioJungle"></label>
                Audio Jungle <span>33</span> </li>
              <li>
                <input type="checkbox" name="checkname" id="Videohive">
                <label for="Videohive"></label>
                Videohive <span>33</span> </li>
            </ul>
            <a href="#.">View More</a> </div>
          <!-- Top Companies end --> 
          
          <!-- Salary -->
          <div class="widget">
            <h4 class="widget-title">Salary Range</h4>
            <ul class="optionlist">
              <li>
                <input type="checkbox" name="checkname" id="price1">
                <label for="price1"></label>
                0 to $100 <span>12</span> </li>
              <li>
                <input type="checkbox" name="checkname" id="price2">
                <label for="price2"></label>
                $100 to $199 <span>41</span> </li>
              <li>
                <input type="checkbox" name="checkname" id="price3">
                <label for="price3"></label>
                $199 to $499 <span>33</span> </li>
              <li>
                <input type="checkbox" name="checkname" id="price4">
                <label for="price4"></label>
                $499 to $999 <span>66</span> </li>
              <li>
                <input type="checkbox" name="checkname" id="price5">
                <label for="price5"></label>
                $999 to $4999 <span>159</span> </li>
              <li>
                <input type="checkbox" name="checkname" id="price6">
                <label for="price6"></label>
                Above $4999 <span>865</span> </li>
            </ul>
            <a href="#.">View More</a> </div>
          <!-- Salary end --> 
          
          <!-- button -->
          <div class="searchnt">
           <a href="<?php echo base_url(); ?>user/refineSearch"> <button class="btn"><i class="fa fa-search" aria-hidden="true"></i> Advanced Search</button></a>
          </div>
          <!-- button end--> 
        </div>
        <!-- Side Bar end --> 
      </div>

      <!-- <div class="col-md-3 col-sm-6 pull-right"> 
       
        <div class="sidebar">
          <h4 class="widget-title">Popular Company</h4>
			<ul class="sidebar-popular list-unstyled">
				<li><a href="#">Coca Cola</a></li>
			    <li><a href="#">Amazon</a></li>
			    <li><a href="#">HP </a></li>
			    <li><a href="#">Ebay</a></li>
			    <li><a href="#">Tieto</a></li>
			    <li><a href="#">IAI System</a></li>
			    <li><a href="#">NBC</a></li>
			    <li><a href="#">Allegro</a></li>
			    <li><a href="#">Onet</a></li>
			</ul>
        </div>
        
       
        <div class="sidebar">
          <h4 class="widget-title">Popular Categories</h4>
			<ul class="sidebar-popular list-unstyled">
              	<li><a href="#">Accounts &amp; Finance</a></li>
              	<li><a href="#">Banking Jobs</a></li>
				<li><a href="#">Construction Jobs</a></li>
                <li><a href="#">Engineering Jobs</a></li>
                <li><a href="#">Graduate Jobs</a></li>
                <li><a href="#">IT Jobs</a></li>
                <li><a href="#">Recruitment</a></li>
                <li><a href="#">Marketing Jobs</a></li>
                <li><a href="#">Sales Jobs</a></li>
            </ul>
        </div>
      </div>
 -->
    <div id="data">
    
      <div class="col-md-9 col-sm-12"> 

      <ul class="list-unstyled"> 
         <?php if($searchedJobs){ 
                    
                
            foreach ($searchedJobs as $jobs) {
             
            
          ?>
          <li>
            <div class="row">
              <div class="col-md-12 col-sm-6 col-xs-12">
                  <div class="freelancer-wrap row-fluid clearfix">
                    <div class="row">
                        <div class="col-md-9">
                            <!-- <div class="rating">
                                <i class="fa fa-star"></i>
                                <i class="fa fa-star"></i>
                                <i class="fa fa-star"></i>
                                <i class="fa fa-star"></i>
                                <i class="fa fa-star"></i>
                            </div> -->
                            <h4><a href="<?php echo base_url().'user/applyJob/'.$jobs->j_id; ?>"><?php echo $jobs->job_title; ?></a> <span class="label label-primary">Featured</span></h4>
                            <h5><?php echo $jobs->company_name." ,  ".$jobs->city; ?></h5>
                            <p class="skillsreq">PHP, MySQL, Linux, Wordpress</p>
                            <ul class="list-inline">
                                <li><small>£35,000 per year</small></li>
                                <li><small><?php echo $jobs->experience; ?></small></li>
                            </ul>
                            <p><?php echo $jobs->job_desc; ?></p>
                        </div><!-- end col -->

                        <div class="col-md-3 text-center">
                            <div class="post-media">
                                <img class="img-responsive" src="<?php echo base_url(); ?>client_uploads/client_logo/<?php echo $jobs->company_logo; ?>" alt="">
                            </div>
                        </div><!-- end col -->
                        <div class="col-md-12">
                          <ul class="list-unstyled list-inline savealert">
                             <?php  if(!$this->session->userdata('user_id')){


                              ?>

                              <li id="status"><a href="<?php echo base_url(); ?>user/"><i class="fa fa-star"></i>Save</a></li> 
                             <?php  } else {  ?>

                            <li><a href="javascript:void(0);" onclick="saveUserJob(<?php echo $jobs->j_id; ?>);"><i class="fa fa-star" id="star_<?php echo $jobs->j_id; ?>" <?php if($jobs->saved ==1 && $jobs->u_id==$this->session->userdata('user_id')){ echo ' style="color:#464861"';} ?> ></i> <span id="status_<?php echo $jobs->j_id; ?>"><?php if($jobs->saved ==1 && $jobs->u_id==$this->session->userdata('user_id')){ echo ' Saved';} else{ echo ' Save'; } ?></span></a></li>
                            <?php   }?>
                             
                             <?php if($this->session->userdata('user_id')){ 
                                 
                              ?>
                                   
                                 <li><a href="javascript:void(0);" onclick="saveAlertJobtype(<?php echo $jobs->j_id; ?>);"><i class="fa fa-star-o" id="star2_<?php echo $jobs->j_id; ?>" <?php if($jobs->jobAlrt == 1 && $jobs->jau_id==$this->session->userdata('user_id')){ echo 'style="color:#464861"';  }?> ></i><span id="saveJT_<?php echo $jobs->j_id; ?>"><?php if($jobs->jobAlrt == 1 && $jobs->jau_id==$this->session->userdata('user_id')){ echo ' Unset alert for job type';  } else{ echo ' Set alert for job type';} ?></span></a></li>

                            <?php } else{?>

                            <li><a href="<?php echo base_url(); ?>user/"><i class="fa fa-star-o"></i>Set alert for job type</a></li>
                            <?php }?>

                          </ul>
                          <br>
                          <!-- Go to www.addthis.com/dashboard to customize your tools -->
                        <div class="addthis_inline_share_toolbox"></div>
                        </div>

                        <span class="new">New</span>



                       <!--  <a data="<?php echo $jobs->j_id; ?>" data-toggle="modal" data-target="" href="javascript:void(0);" ng-click="applyJob(<?php echo $jobs->j_id; ?>);" class="btn btn-primary">APPLY</a> -->
                        
                      
                       
                        <!-- <?php if($jobs->j_id == $jobs->job_id && $jobs->user_id == $this->session->userdata('user_id')){ ?> --> 
                        <!-- <a href="#" class="btn btn-primary">Applied</a> 
                           <?php  } else{?> -->

                         <!-- <a href="<?php if($this->session->userdata('user_id')){ echo base_url().'user/applyJob/'.$jobs->j_id; }  else { echo base_url().'user/login'; } ?>" class="btn btn-primary">APPLY</a> -->

                     <?php }?>
                    </div>
                  </div><!-- end freelancer-wrap -->
                </div><!-- end col -->
          
       

        <?php   } } ?>     

      </ul>


        <!-- Pagination Start -->
        <!-- <div class="pagiWrap">
          <div class="row">
            <div class="col-md-4 col-sm-4">
              <div class="showreslt">Showing 1-10</div>
            </div>
            <div class="col-md-8 col-sm-8 text-right">
              <ul class="pagination">
                <li class="active"><a href="#.">1</a></li>
                <li><a href="#.">2</a></li>
                <li><a href="#.">3</a></li>
                <li><a href="#.">4</a></li>
                <li><a href="#.">5</a></li>
                <li><a href="#.">6</a></li>
                <li><a href="#.">7</a></li>
                <li><a href="#.">8</a></li>
              </ul>
            </div>
          </div>
        </div> -->
        <!-- Pagination end --> 
      </div>


      </div>
                      

          
    </div>


  </div>
</div>

<?php // include 'footer.php' ?>