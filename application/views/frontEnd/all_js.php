	
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>

<script src="<?php echo base_url(); ?>web_assets/bootstrap/js/bootstrap.min.js"></script>

<!-- SmartMenus jQuery plugin -->
<script type="text/javascript" src="<?php echo base_url(); ?>web_assets/js/jquery.smartmenus.js"></script>
<!-- SmartMenus jQuery Bootstrap Addon -->
<script type="text/javascript" src="<?php echo base_url(); ?>web_assets/js/jquery.smartmenus.bootstrap.js"></script>

<script src="<?php echo base_url(); ?>web_assets/js/main.js"></script> <!-- Resource jQuery -->
<script src="<?php echo base_url(); ?>user_assets/js/jquery-ias.min.js"></script>
<script src="<?php echo base_url(); ?>web_assets/owl/owl.carousel.min.js"></script>


<script>
$(document).ready(function(){
	$('.owl-complogo').owlCarousel({
	    loop:true,
	    dots:false,
	    margin:50,
	    autoplay:true,
		autoplayTimeout:5000,
		autoplayHoverPause:false,
		nav:true,
	    navText:[
	        "<i class='fa fa-angle-left fa-2x'></i>",
	        "<i class='fa fa-angle-right fa-2x'></i>"
	    ],
	    responsiveClass:true,
	    responsive:{
	        0:{
	            items:3,
	            nav:true
	        },
	        600:{
	            items:4,
	            nav:true
	        },
	        1000:{
	            items:6,
	            nav:true
	        }
	    }
	});


  // Add smooth scrolling to all links in navbar + footer link
  $("a[href='#myPage']").on('click', function(event) {

   // Make sure this.hash has a value before overriding default behavior
  if (this.hash !== "") {

    // Prevent default anchor click behavior
    event.preventDefault();

    // Store hash
    var hash = this.hash;

    // Using jQuery's animate() method to add smooth page scroll
    // The optional number (900) specifies the number of milliseconds it takes to scroll to the specified area
    $('html, body').animate({
      scrollTop: $(hash).offset().top
    }, 900, function(){

      // Add hash (#) to URL when done scrolling (default click behavior)
      window.location.hash = hash;
      });
    } // End if
  });

})

</script>