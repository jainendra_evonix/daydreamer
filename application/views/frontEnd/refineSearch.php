  <style type="text/css">
    .checkbox.well {
      padding: 9px 15px 11px;
      margin: 0;
    }
    .jobtitle {
      font-size: 15px;
      font-weight: 600 !important;
    }
    .radiobtn {
        background: #eee none repeat scroll 0 0;
      border-radius: 5px;
      margin-top: -10px;
      padding: 10px;
      padding: 10px;
      min-width: 90px;
    }
    .radiobtn input {
      vertical-align: bottom;
    }
  </style>
          <!-- <body class="login-body" ng-app="postApp" ng-controller="postController" ng-cloak>
       -->
           
                  <!--main content start-->

  <div class="cd-hero-inner">
    <div class="container">
      <div class="row">
        <div class="col-md-6 col-sm-6">
          <h1>Advanced Search</h1>
        </div>
        <div class="col-md-6 col-sm-6">
          <div class="breadcmb"><a href="<?php echo base_url(); ?>user/index">Home</a> / <span>Job Search</span></div>
        </div>
      </div>
    </div>
  </div>
                  <section id="" class="container">
                      <section class="wrapper">
                          <!-- page start-->
                          <div class="row">
                              <div class="col-md-8 col-md-offset-2">
                                  <h3 class="form-heading">Looking for</h3>
                                 <!--  <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit.</p> -->
                                  <section class="panel">
                                      <div class="panel-body">
                                          <form class="form-horizontal bucket-form" action="<?php echo base_url(); ?>user/advancedSearch" method="post">
                                          <input type="hidden" name="submit1" value="search1">
                                           <div class="form-group">
                                                  <label class="col-sm-3 control-label">Keywords</label>
                                                  <div class="col-sm-6">
                                                <input type="text"   name="Keywords" class="form-control"  placeholder="e.g. Java Developer" >

                                            </div>
                                                 
                                       </div>
                                        <div class="form-group">
                                                  <label class="col-sm-1 col-sm-offset-3">Contains </label>
                                                  <div class="col-sm-6 icheck">
                                                      <div class="square single-row">
                                                          <div class="radiobtn">
                                                              
                                                               <input name="KeywordsContains" type="radio"  value="any" <?php echo  set_radio('KeywordsContains', 'any', TRUE); ?> > Any
                                                              
                                                          </div>
                                                          <div>or</div>
                                                          <div class="radiobtn">
                                                              
                                                              <input name="KeywordsContains" type="radio"  value="every" <?php echo  set_radio('KeywordsContains', 'every'); ?> > Every
                                                             
                                                           
                                                          </div>
                                                          <div>word</div>
                                                          
                                                      </div>
                                                  </div>
                                              </div>

                                         

                                              <div class="form-group">
                                                  <label class="col-sm-3 control-label">Locations</label>
                                                  <div class="col-sm-6">
                                                <input type="text"   class="form-control" placeholder="e.g. California" name="locations" >

                                            </div>
                                                 
                                       </div> 

                                       <div class="form-group">
                                                  <label class="col-sm-3 control-label">Job Title</label>
                                                  <div class="col-sm-6">
                                                <input type="text"   name="jobtitle" class="form-control" placeholder="e.g. Manager" >

                                                 <p>Will show jobs with a matching vacancy title</p>

                                            </div>
                                                 
                                       </div>

                                              <div class="form-group">
                                                  <label class="col-sm-1 col-sm-offset-3">Contains </label>
                                                  <div class="col-sm-6 icheck">
                                                      <div class="square single-row">
                                                          <div class="radiobtn">
                                                              
                                                               <input name="jt" type="radio"  value="any" <?php echo  set_radio('jt', 'any', TRUE); ?> > Any
                                                              
                                                          </div>
                                                          <div>or</div>
                                                          <div class="radiobtn">
                                                              
                                                              <input name="jt" type="radio" value="every" <?php echo  set_radio('jt', 'every'); ?> required> Every
                                                             
                                                           
                                                          </div>
                                                          <div>word</div>
                                                         
                                                      </div>
                                                  </div>
                                              </div>
                                                  
                                                  <div class="row">
                                                  <div class="col-sm-5"></div>
                                                  <div class="col-sm-2">
                                                    <button type="submit" class="btn btn-success">Search</button>
                                                  </div>
                                                    
                                                    <div class="col-sm-5"></div>
                                                    </div>
                                                  
                                                 
                                                </form>
                                      </div>
                                  </section>
                              </div>
                          </div>
                          <!-- page end-->

                      </section>
                  </section>
                  <!--main content end-->
                

                   <section id="" class="container">
                      <section class="wrapper">
                          <!-- page start-->
                          <div class="row">
                              <div class="col-md-8 col-md-offset-2">
                                  <h3 class="form-heading">Job Type & Salary</h3>
                                 <!--  <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit.</p> -->
                                  <section class="panel">
                                      <div class="panel-body">

                                          <form class="form-horizontal bucket-form" action="<?php echo base_url(); ?>user/advancedSearch" method="post">

                                          <input type="hidden" name="submit2" value="search2">
  
                                   <div class="col-sm-6">
                                      <div class="checkbox well">
                                        <label class="jobtitle"><input type="checkbox" name="jobNature[]" value="Full time">Full-Time</label>
                                      </div>
                                      <br>
                                    </div>

                                    <div class="col-sm-6">
                                      <div class="checkbox well">
                                        <label class="jobtitle"><input type="checkbox" name="jobNature[]" value="Contract">Contract/Interim</label>
                                      </div>
                                      <br>
                                    </div>


                                    <div class="col-sm-6">
                                      <div class="checkbox well">
                                        <label class="jobtitle"><input type="checkbox" name="jobNature[]" value="Temporary">Temporary/Seasonal</label>
                                      </div>
                                      <br>
                                    </div>

                                    <div class="col-sm-6">
                                      <div class="checkbox well">
                                        <label class="jobtitle"><input type="checkbox" name="jobNature[]" value="Part time">Part-Time</label>
                                      </div>
                                      <br>
                                    </div>

                                  
                                       <!-- <div class="row">
                                                            
                                                              <label for="daysback">Date Posted</label>
                                                           
                                                            
                                                              <select name="daysback" id="daysback" value="" data-ga-label="daysback">
                                                                <option value="A">any time</option>
                                                                <option value="2d">in last 3 days</option>
                                                                <option value="1d">in last 2 days</option>
                                                                <option value="0d">today</option>
                                                                <option value="4h">in last 4 hours</option>
                                                                <option value="2h">in last 2 hours</option>
                                                              </select>
                                                            
                                            </div> -->


                                            <div class="form-group">
                                        <label class="col-sm-3 control-label">Date Posted</label>
                                        <div class="col-sm-6">
                                            <select class="form-control m-bot15" ng-model="user.grade" name="grade">
                                                <option value=""  selected="selected">Select posted date</option>
                                                

                                                                <option value="A">any time</option>
                                                                <option value="2d">in last 3 days</option>
                                                                <option value="1d">in last 2 days</option>
                                                                <option value="0d">today</option>
                                                                <option value="4h">in last 4 hours</option>
                                                                <option value="2h">in last 2 hours</option>

                                                  


                                               </select>
                                              
                                           </div>
                                       </div>



                                  <div class="form-group">
                                                <label class="col-sm-1 col-sm-offset-3">Salary </label>
                                                  <div class="col-sm-6 icheck">
                                                      <div class="square single-row">
                                                          <div class="radiobtn">
                                                              
                                                              &nbsp;&nbsp; <input name="salaryType" type="radio" value="annualy" <?php echo  set_radio('salaryType', 'annualy', TRUE); ?> onClick="showHideDiv1();" > Annual
                                                              
                                                          </div>
                                                          <div>&nbsp;&nbsp;&nbsp;&nbsp;</div>
                                                          <div class="radiobtn">
                                                              
                                                              <input name="salaryType" type="radio" value="hourly"  <?php echo  set_radio('salaryType', 'hourly'); ?> onClick="showHideDiv2();" > Hourly
                                                             
                                                           
                                                          </div>
                                                          <!-- <div>word</div> -->
                                                         
                                                      </div>
                                                  </div>
                                              </div>

                       <div class="form-group" id="salaryRange" >
                        <label class="control-label col-md-4"></label>
                        <div class="col-sm-2">
                            <select class="form-control m-bot15"  name="salaryRangeFrom" value="" >
                                <option value="" selected="selected">Salary</option>
                               

                                 <option value="10000" >10000</option>
                                 <option value="10000" >10000</option>
                                 <option value="10000" >10000</option>
                                 <option value="10000" >10000</option>
                                 <option value="10000" >10000</option>
                                 <option value="10000" >10000</option>


                                 
                             </select>
                             
                         </div>
                         <div class="col-sm-2">
                         <select class="form-control m-bot15"  name="salaryRangeTo"  value="" >
                                    <option value="" selected="selected">Salary</option>
                                    <option value="20000" >20000</option>
                                    <option value="20000" >20000</option>
                                    <option value="20000" >20000</option>
                                    <option value="20000" >20000</option>
                                    <option value="20000" >20000</option>
                                    <option value="20000" >20000</option>
                               </select>
                             </div>
                       </div>

                       <div class="form-group" id="hourlyPrice">
                          <label class="control-label col-md-4"></label>
                          <div class="col-sm-2">
                              <select class="form-control m-bot15"  name="hourlyRangeFrom" value="" >
                                  <option value="" selected="selected">Hour</option>
                                 

                                   <option value="10000" >10000</option>
                                   <option value="10000" >10000</option>
                                   <option value="10000" >10000</option>
                                   <option value="10000" >10000</option>
                                   <option value="10000" >10000</option>
                                   <option value="10000" >10000</option>


                                   
                               </select>
                               
                           </div>
                           <div class="col-sm-2">
                           <select class="form-control m-bot15"  name="hourlyRangeTo"  value="">
                                      <option value="" selected="selected">Hour</option>
                                      <option value="20000" >20000</option>
                                      <option value="20000" >20000</option>
                                      <option value="20000" >20000</option>
                                      <option value="20000" >20000</option>
                                      <option value="20000" >20000</option>
                                      <option value="20000" >20000</option>
                                 </select>
                               </div>
                         </div>
                        


                      </div>
                  </section>
              </div>
          </div>
                          <!-- page end-->

                      </section>
                  </section>

         
                 <section id="" class="container">
                      <section class="wrapper">
                          <!-- page start-->
                          <div class="row">
                              <div class="col-md-8 col-md-offset-2">
                                  <h3 class="form-heading">Sectors</h3>
                                 <!--  <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit.</p> -->
                                  <section class="panel">
                                      <div class="panel-body">
                                     <?php foreach ($jobsSectors as $sectors) {  ?>
                                      
                                       <div class="col-sm-6">
                                      <div class="checkbox well">
                                        <label class="jobtitle"><input type="checkbox" name="jobsSector[]" value="<?php echo $sectors->id; ?>"><?php echo $sectors->job_type; ?></label>
                                      </div>
                                      <br>
                                    </div>

                                    <?php } ?>
                                  
                          

                      </div><!-- fieldset-data -->
                            <div class="row">
                                                  <div class="col-sm-5"></div>
                                                  <div class="col-sm-2">
                                                    <button type="submit" class="btn btn-success">Search</button>
                                                  </div>
                                                    
                                                    <div class="col-sm-5"></div>
                                                    </div>
                                                    <br>
                    </div><!-- fieldset-group -->

                                  

         
                                      </div>
                                  </section>
                              </div>
                          </div>
                          <!-- page end-->

                      </section>
                  </section>



                                     <!--main content end-->



              <!--right sidebar start-->
              <div class="right-sidebar">
                  <div class="search-row">
                      <input type="text" placeholder="Search" class="form-control">
                  </div>
                  <div class="right-stat-bar">
                      <ul class="right-side-accordion">
                          <li class="widget-collapsible">
                              <a href="#" class="head widget-head red-bg active clearfix">
                                  <span class="pull-left">work progress (5)</span>
                                  <span class="pull-right widget-collapse"><i class="ico-minus"></i></span>
                              </a>
                              <ul class="widget-container">
                                  <li>
                                      <div class="prog-row side-mini-stat clearfix">
                                          <div class="side-graph-info">
                                              <h4>Target sell</h4>
                                              <p>
                                                  25%, Deadline 12 june 13
                                              </p>
                                          </div>
                                          <div class="side-mini-graph">
                                              <div class="target-sell">
                                              </div>
                                          </div>
                                      </div>
                                      <div class="prog-row side-mini-stat">
                                          <div class="side-graph-info">
                                              <h4>product delivery</h4>
                                              <p>
                                                  55%, Deadline 12 june 13
                                              </p>
                                          </div>
                                          <div class="side-mini-graph">
                                              <div class="p-delivery">
                                                  <div class="sparkline" data-type="bar" data-resize="true" data-height="30" data-width="90%" data-bar-color="#39b7ab" data-bar-width="5" data-data="[200,135,667,333,526,996,564,123,890,564,455]">
                                                  </div>
                                              </div>
                                          </div>
                                      </div>
                                      <div class="prog-row side-mini-stat">
                                          <div class="side-graph-info payment-info">
                                              <h4>payment collection</h4>
                                              <p>
                                                  25%, Deadline 12 june 13
                                              </p>
                                          </div>
                                          <div class="side-mini-graph">
                                              <div class="p-collection">
                                                <span class="pc-epie-chart" data-percent="45">
                                                    <span class="percent"></span>
                                                </span>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="prog-row side-mini-stat">
                                      <div class="side-graph-info">
                                          <h4>delivery pending</h4>
                                          <p>
                                              44%, Deadline 12 june 13
                                          </p>
                                      </div>
                                      <div class="side-mini-graph">
                                          <div class="d-pending">
                                          </div>
                                      </div>
                                  </div>
                                  <div class="prog-row side-mini-stat">
                                      <div class="col-md-12">
                                          <h4>total progress</h4>
                                          <p>
                                              50%, Deadline 12 june 13
                                          </p>
                                          <div class="progress progress-xs mtop10">
                                              <div style="width: 50%" aria-valuemax="100" aria-valuemin="0" aria-valuenow="20" role="progressbar" class="progress-bar progress-bar-info">
                                                  <span class="sr-only">50% Complete</span>
                                              </div>
                                          </div>
                                      </div>
                                  </div>
                              </li>
                          </ul>
                      </li>
                      <li class="widget-collapsible">
                          <a href="#" class="head widget-head terques-bg active clearfix">
                              <span class="pull-left">contact online (5)</span>
                              <span class="pull-right widget-collapse"><i class="ico-minus"></i></span>
                          </a>
                          <ul class="widget-container">
                              <li>
                                  <div class="prog-row">
                                      <div class="user-thumb">
                                          <a href="#"><img src="<?php echo base_url(); ?>user_assets/images/avatar1_small.jpg" alt=""></a>
                                      </div>
                                      <div class="user-details">
                                          <h4><a href="#">Jonathan Smith</a></h4>
                                          <p>
                                              Work for fun
                                          </p>
                                      </div>
                                      <div class="user-status text-danger">
                                          <i class="fa fa-comments-o"></i>
                                      </div>
                                  </div>
                                  <div class="prog-row">
                                      <div class="user-thumb">
                                          <a href="#"><img src="<?php echo base_url(); ?>user_assets/images/avatar1.jpg" alt=""></a>
                                      </div>
                                      <div class="user-details">
                                          <h4><a href="#">Anjelina Joe</a></h4>
                                          <p>
                                              Available
                                          </p>
                                      </div>
                                      <div class="user-status text-success">
                                          <i class="fa fa-comments-o"></i>
                                      </div>
                                  </div>
                                  <div class="prog-row">
                                      <div class="user-thumb">
                                          <a href="#"><img src="<?php echo base_url(); ?>user_assets/images/chat-avatar2.jpg" alt=""></a>
                                      </div>
                                      <div class="user-details">
                                          <h4><a href="#">John Doe</a></h4>
                                          <p>
                                              Away from Desk
                                          </p>
                                      </div>
                                      <div class="user-status text-warning">
                                          <i class="fa fa-comments-o"></i>
                                      </div>
                                  </div>
                                  <div class="prog-row">
                                      <div class="user-thumb">
                                          <a href="#"><img src="<?php echo base_url(); ?>user_assets/images/avatar1_small.jpg" alt=""></a>
                                      </div>
                                      <div class="user-details">
                                          <h4><a href="#">Mark Henry</a></h4>
                                          <p>
                                              working
                                          </p>
                                      </div>
                                      <div class="user-status text-info">
                                          <i class="fa fa-comments-o"></i>
                                      </div>
                                  </div>
                                  <div class="prog-row">
                                      <div class="user-thumb">
                                          <a href="#"><img src="<?php echo base_url(); ?>user_assets/images/avatar1.jpg" alt=""></a>
                                      </div>
                                      <div class="user-details">
                                          <h4><a href="#">Shila Jones</a></h4>
                                          <p>
                                              Work for fun
                                          </p>
                                      </div>
                                      <div class="user-status text-danger">
                                          <i class="fa fa-comments-o"></i>
                                      </div>
                                  </div>
                                  <p class="text-center">
                                      <a href="#" class="view-btn">View all Contacts</a>
                                  </p>
                              </li>
                          </ul>
                      </li>
                      <li class="widget-collapsible">
                          <a href="#" class="head widget-head purple-bg active">
                              <span class="pull-left"> recent activity (3)</span>
                              <span class="pull-right widget-collapse"><i class="ico-minus"></i></span>
                          </a>
                          <ul class="widget-container">
                              <li>
                                  <div class="prog-row">
                                      <div class="user-thumb rsn-activity">
                                          <i class="fa fa-clock-o"></i>
                                      </div>
                                      <div class="rsn-details ">
                                          <p class="text-muted">
                                              just now
                                          </p>
                                          <p>
                                              <a href="#">Jim Doe </a>Purchased new equipments for zonal office setup
                                          </p>
                                      </div>
                                  </div>
                                  <div class="prog-row">
                                      <div class="user-thumb rsn-activity">
                                          <i class="fa fa-clock-o"></i>
                                      </div>
                                      <div class="rsn-details ">
                                          <p class="text-muted">
                                              2 min ago
                                          </p>
                                          <p>
                                              <a href="#">Jane Doe </a>Purchased new equipments for zonal office setup
                                          </p>
                                      </div>
                                  </div>
                                  <div class="prog-row">
                                      <div class="user-thumb rsn-activity">
                                          <i class="fa fa-clock-o"></i>
                                      </div>
                                      <div class="rsn-details ">
                                          <p class="text-muted">
                                              1 day ago
                                          </p>
                                          <p>
                                              <a href="#">Jim Doe </a>Purchased new equipments for zonal office setup
                                          </p>
                                      </div>
                                  </div>
                              </li>
                          </ul>
                      </li>
                      <li class="widget-collapsible">
                          <a href="#" class="head widget-head yellow-bg active">
                              <span class="pull-left"> shipment status</span>
                              <span class="pull-right widget-collapse"><i class="ico-minus"></i></span>
                          </a>
                          <ul class="widget-container">
                              <li>
                                  <div class="col-md-12">
                                      <div class="prog-row">
                                          <p>
                                              Full sleeve baby wear (SL: 17665)
                                          </p>
                                          <div class="progress progress-xs mtop10">
                                              <div class="progress-bar progress-bar-success" role="progressbar" aria-valuenow="20" aria-valuemin="0" aria-valuemax="100" style="width: 40%">
                                                  <span class="sr-only">40% Complete</span>
                                              </div>
                                          </div>
                                      </div>
                                      <div class="prog-row">
                                          <p>
                                              Full sleeve baby wear (SL: 17665)
                                          </p>
                                          <div class="progress progress-xs mtop10">
                                              <div class="progress-bar progress-bar-info" role="progressbar" aria-valuenow="20" aria-valuemin="0" aria-valuemax="100" style="width: 70%">
                                                  <span class="sr-only">70% Completed</span>
                                              </div>
                                          </div>
                                      </div>
                                  </div>
                              </li>
                          </ul>
                      </li>
                  </ul>
              </div>
           </div>

