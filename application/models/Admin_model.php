<?php
class Admin_model extends CI_Model{


//function for login
  function check_admin_login($emailaddress, $password){

    $result=$this->db->select('tbl_admin.*,tbl_profile_picture.profile_pic')
    ->from('tbl_admin')
    ->join('tbl_profile_picture','tbl_admin.id=tbl_profile_picture.admin_id')
    ->where('admin_email',$emailaddress)
    ->where('admin_password',$password)
    ->where('flag','1')
    ->get()->result();
    //echo '<pre>';print_r($this->db->last_query());exit;
    return $result;

  }
//function for adding admin
  function add_admin($info){

    $result=$this->db->insert('tbl_admin',$info);
    return $this->db->insert_id();

  }
 //function for activating client
  function activateAdminLogin($data,$token){
    $result=$this->db->where('id',$token)->update('tbl_admin',$data);
    return $result;

  }

//function for adding package
  function addClientPackage($insert_package){
    $result=$this->db->insert('tbl_package',$insert_package);
    return $result;


  }


//function for fetching package data by client id
  function fetchPackageData($admin_id){
    
    $result=$this->db->select('tbl_package.*,(select SUM(job_adds) from tbl_job_adds  where tbl_job_adds.package_id = tbl_package.id) as totalJobAdds,(select service from tbl_service_package where tbl_service_package.package_id=tbl_package.id) as totalServices,(select package_image from tbl_package_image where tbl_package_image.package_id=tbl_package.id) as package_pic')
    ->from('tbl_package')
    //->join('tbl_job_adds','tbl_job_adds.admin_id=tbl_package.package_title')
    ->where('tbl_package.validity_date >=',date('Y-m-d H:i:s'))
    ->get()->result();
   // print_r($this->db->last_query());exit;
    return $result;

  }

//function for deleting package
  function deletePackageById($id){
    $this->db->where('id',$id);
    $this->db->delete('tbl_package');
    return true;
  }

//function for getting package
  function getpackage($id){
    $result=$this->db->select('*')
                      ->from('tbl_package')
                      //->join('tbl_mst_package','tbl_mst_package.p_id=tbl_package.package_title')
                      ->where('tbl_package.id',$id)
                      ->get()->row();
                      return $result;


  }

//function for updating package details
  function updatePackage($update_package,$id){
    $result=$this->db->where('id',$id)->update('tbl_package',$update_package);
    return $result;
  }

//function to check email

  function checkEmailExist($email){
    $result=$this->db->select('*')
    ->from('tbl_admin')
    ->where('admin_email',$email)
    ->get()->row();
    return $result;
  }

 //function for checking email
  function check_admin_forgot_password($emailaddress){
   $result=$this->db->select('*')
   ->from('tbl_admin')
   ->where('admin_email',$emailaddress)
   ->get()->row();
   return $result;

 }

 //function for updating keystring

 function update_keystring($emailaddress,$randomkeystring){
  date_default_timezone_set('UTC');
          //date_default_timezone_set("Asia/Kolkata");
  $newkeystring = array(
    'keystring' => $randomkeystring,
    'isused' => 0,
    'updatedtimestamp' => date("Y-m-d H:i:s")
    );
  $result=$this->db->where('admin_email',$emailaddress)->update('tbl_admin',$newkeystring);
  return $result;


}

//function for checking random key
function check_randomkey_string($randomkeystring){
  $result=$this->db->select('*')
  ->from('tbl_admin')
  ->where('keystring',$randomkeystring)
  ->get()->row();
  return $result;

}

 //function for reseting password
function update_password($emailaddress,$npassword) {
  $newpassword = array(
    'admin_password' => md5($npassword),
    'isused' => 1
    );
  $this->db->where('admin_email', $emailaddress);
  $this->db->update('tbl_admin', $newpassword);
        //print_r($this->db->last_query()); exit;
  return TRUE;

}

//function for viewing all offices
function getOffices(){
  $result=$this->db->select('*')
  ->from('tbl_client')
  ->where('client_id','0')
  ->get()->result();
  return $result;


} 

//function for fetching all packages
function getPackages(){
  $result=$this->db->select('count(p_id) as count')
  ->from('tbl_mst_package')
  ->get()->row();
  return $result;
}

//function for fetching all offices
function getAllOffices(){

  $result=$this->db->select('count(id) as offices')
  ->from('tbl_client')
  ->get()->row();
  return $result;

}

//function for viewing all candidates
function getCandidates(){

  $result=$this->db->select('*')
  ->from('tbl_user')
  ->get()->result();
  return $result;
}

//function for viewing all jobs
function getJobs($job_title,$closing_date,$salary,$industry){
  

  if($job_title!='' || $closing_date!='' || $salary!='' || $industry!=''){
    
   //echo 'here';
  $result=$this->db->select('tbl_job_create.*,tbl_mst_status.status_name,tbl_client.clnt_first_name,tbl_client.clnt_last_name,tbl_mst_industry.industry_name,tbl_subClient_create.subclient_name')
  ->from('tbl_job_create')
  ->join('tbl_mst_status','tbl_job_create.job_status=tbl_mst_status.status_id')
  ->join('tbl_client','tbl_client.id=tbl_job_create.client_id')
  ->join('tbl_mst_industry','tbl_mst_industry.ind_id=tbl_job_create.industry')
  ->join('tbl_subClient_create','tbl_subClient_create.id=tbl_job_create.parent_client','left')
  ->where("(tbl_job_create.job_title LIKE '$job_title' OR tbl_job_create.open_date LIKE '$closing_date' OR tbl_job_create.industry LIKE '$industry'  OR tbl_job_create.salary LIKE '$salary')")
  ->get()->result();

  


  }else{
   //echo 'there';
 $result=$this->db->select('tbl_job_create.*,tbl_mst_status.status_name,tbl_client.clnt_first_name,tbl_client.clnt_last_name,tbl_mst_industry.industry_name,tbl_subClient_create.subclient_name')
  ->from('tbl_job_create')
  ->join('tbl_mst_status','tbl_job_create.job_status=tbl_mst_status.status_id')
  ->join('tbl_client','tbl_client.id=tbl_job_create.client_id')
  ->join('tbl_mst_industry','tbl_mst_industry.ind_id=tbl_job_create.industry')
  ->join('tbl_subClient_create','tbl_subClient_create.id=tbl_job_create.parent_client','left')
  ->get()->result();


  }
 
                  //echo '<pre>';print_r($this->db->last_query());
   return $result;

}



//function for fetching all jobs
function fetchJobs(){
$result=$this->db->select('tbl_job_create.*,tbl_mst_status.status_name,tbl_client.clnt_first_name,tbl_client.clnt_last_name,tbl_subClient_create.subclient_name')
  ->from('tbl_job_create')
  ->join('tbl_mst_status','tbl_job_create.job_status=tbl_mst_status.status_id')
  ->join('tbl_client','tbl_client.id=tbl_job_create.client_id')
  //->join('tbl_mst_industry','tbl_mst_industry.ind_id=tbl_job_create.industry')
  ->join('tbl_subClient_create','tbl_subClient_create.id=tbl_job_create.parent_client','left')
  ->get()->result();
//print_r($this->db->last_query());exit;
return $result;

}


//function for viewing sub offices jobs
function getSubOfficesJobs($job_title,$closing_date,$salary,$industry){

if($job_title!='' || $closing_date!='' || $salary!='' || $industry!=''){

$result=$this->db->select('tbl_job_create.*,tbl_mst_status.status_name,tbl_subClient_create.subclient_name,tbl_mst_industry.industry_name')
  ->from('tbl_job_create')
  ->join('tbl_mst_status','tbl_job_create.job_status=tbl_mst_status.status_id')
  ->join('tbl_subClient_create','tbl_subClient_create.id=tbl_job_create.subClient_id')
  ->join('tbl_mst_industry','tbl_mst_industry.ind_id=tbl_job_create.industry')
  ->where("(tbl_job_create.job_title LIKE '$job_title' OR tbl_job_create.open_date LIKE '$closing_date' OR tbl_job_create.industry LIKE '$industry'  OR tbl_job_create.salary LIKE '$salary')")
  ->get()->result();

//print_r($result);exit;
}else{

$result=$this->db->select('tbl_job_create.*,tbl_mst_status.status_name,tbl_subClient_create.subclient_name,tbl_mst_industry.industry_name')
  ->from('tbl_job_create')
  ->join('tbl_mst_status','tbl_job_create.job_status=tbl_mst_status.status_id')
  ->join('tbl_subClient_create','tbl_subClient_create.id=tbl_job_create.subClient_id')
  ->join('tbl_mst_industry','tbl_mst_industry.ind_id=tbl_job_create.industry')
  //->where('tbl_client.client_id','0')
  ->get()->result();


}

  
                //echo '<pre>';print_r($this->db->last_query());exit;
  return $result;


}
//function for fetching all suboffices jobs
function fetchSubOfficesJobs(){
$result=$this->db->select('tbl_job_create.*,tbl_mst_status.status_name,tbl_subClient_create.subclient_name,tbl_mst_industry.industry_name,tbl_subClient_create.subclient_name')
  ->from('tbl_job_create')
  ->join('tbl_mst_status','tbl_job_create.job_status=tbl_mst_status.status_id')
  ->join('tbl_subClient_create','tbl_subClient_create.id=tbl_job_create.subClient_id')

  ->join('tbl_mst_industry','tbl_mst_industry.ind_id=tbl_job_create.industry')
                 //->where('tbl_client.client_id','0')
  ->get()->result();
 //echo '<pre>';print_r($this->db->last_query());exit;
  return $result;
}


//function for disapproving client job
function disapproveClientJob($updateArr,$id){

  $result=$this->db->where('j_id',$id)->update('tbl_job_create',$updateArr);
  return $result;
}

//function for approving client job
function approveClientJob($updateArr,$id){
  $result=$this->db->where('j_id',$id)->update('tbl_job_create',$updateArr);
  return $result;



}

//function for fetching job description
function getJobDescription($id){

  $result=$this->db->select('tbl_job_create.*,tbl_mst_status.status_name,tbl_mst_experience.experience,tbl_subClient_create.subclient_name,tbl_company_logo.company_logo,tbl_mst_job_type.job_type')
  ->from('tbl_job_create')
  ->join('tbl_mst_status','tbl_job_create.job_status=tbl_mst_status.status_id')
  ->join('tbl_mst_experience','tbl_job_create.experience=tbl_mst_experience.ex_id')
  ->join('tbl_mst_job_type','tbl_job_create.job_type=tbl_mst_job_type.id')
  ->join('tbl_subClient_create','tbl_subClient_create.id=tbl_job_create.parent_client','left')
  ->join('tbl_company_logo','tbl_job_create.client_id=tbl_company_logo.client_id')
  ->where('j_id',$id)
  ->get()->row();
  //print_r($this->db->last_query());exit;
  return $result;
}

//function for getting count of candidates
function getAllCandidates(){
  $result=$this->db->select('count(id) as candidates')
  ->from('tbl_user')
  ->get()->row();
  return $result;

}

//function for getting count of jobs
function getAllJobs(){
  $result=$this->db->select('count(j_id) as jobs')
  ->from('tbl_job_create')
  ->get()->row();
  return $result;


}

//function for disabling user
function disableUserByAdmin($updateArr,$id){
  $result=$this->db->where('id',$id)->update('tbl_user',$updateArr);
  return $result;
}

//function for enabling user
function enableUserByAdmin($updateArr,$id){
  $result=$this->db->where('id',$id)->update('tbl_user',$updateArr);
  return $result;

}

//function for disabling client
function enableOfficeByAdmin($updateArr,$id){

  $result=$this->db->where('id',$id)->update('tbl_client',$updateArr);
  return $result;

}

//function for enabling office
function disableOfficeByAdmin($updateArr,$id){

  $result=$this->db->where('id',$id)->update('tbl_client',$updateArr);
  return $result;

} 

//function for fetching all industries
function getIndustries(){
  $result=$this->db->select('*')
  ->from('tbl_mst_industry')
  ->get()->result();
  return $result;

}

//function for fetching all countries
function getCountries(){
  $result=$this->db->select('*')
  ->from('tbl_mst_countries')
  ->get()->result();
  return $result;

}

//function for creating a new client
function addClient($clientCreate){

  $result=$this->db->insert('tbl_client',$clientCreate);
  return $result;

}

//function for fetching client email
function fetchClientEmail($id){
  $result=$this->db->select('*')
  ->from('tbl_client')
  ->where('id',$id)
  ->get()->row();
  return $result;


}
//function for updating client information
function getClientDetails($id){
  $result=$this->db->select('tbl_client.*,tbl_mst_countries.country_name')
  ->from('tbl_client')
  ->join('tbl_mst_countries','tbl_mst_countries.id=tbl_client.clnt_country')
  ->where('tbl_client.id',$id)
  ->get()->row();

  return $result;


}

//function for checking client data
function checkingClientData($client_id){
  $result=$this->db->select('*')
  ->from('tbl_client')
  ->where('id',$client_id)
  ->get()->row();
  return $result;


}

//function for updating client information
function updateClientInfo($clientCreate,$client_id){

  $result=$this->db->where('id',$client_id)->update('tbl_client',$clientCreate);
  return $result;

}

//function for fetching suboffices
function getSubOffices($id){
  $result=$this->db->select('*')
  ->from('tbl_subClient_create')
  ->where('client_id',$id)
  ->get()->result();
  return $result;

}

//function for counting all Candidates
function countAllCandidates($start_date,$end_date){
  $result=$this->db->select('DATE_FORMAT(created_date, "%Y") as year,DATE_FORMAT(created_date, "%m") as month,count(*) as userid')

  ->from('tbl_user')
  ->where('created_date >=',$start_date)
  ->where('created_date <=',$end_date)
       // ->where('client_id',$id)

  ->group_by('DATE_FORMAT(created_date, "%Y%m")')
  ->get()->result();
       //echo "<pre>";print_r($this->db->last_query());
  return $result;


}

//function for counting all offices
function countAllOffices($start_date,$end_date){
  $result=$this->db->select('DATE_FORMAT(created_date, "%Y") as year,DATE_FORMAT(created_date, "%m") as month,count(*) as clientid')

  ->from('tbl_client')
  ->where('created_date >=',$start_date)
  ->where('created_date <=',$end_date)
  ->where('client_id','0')
       // ->where('client_id',$id)

  ->group_by('DATE_FORMAT(created_date, "%Y%m")')
  ->get()->result();
       //echo "<pre>";print_r($this->db->last_query());
  return $result;


}

//function for counting whole users
function countWholeUser($start_date, $end_date){
  $result=$this->db->select('count(id) as user_id')
  ->from('tbl_user')
  ->where('created_date >=',$start_date)
  ->where('created_date <=',$end_date)
  ->get()->row();
    //echo "<pre>";print_r($this->db->last_query());
  return $result;


}

function countWholeClients($start_date, $end_date){
  $result=$this->db->select('count(id) as client_id')
  ->from('tbl_client')
  ->where('client_id','0')
  ->where('created_date >=',$start_date)
  ->where('created_date <=',$end_date)
  ->get()->row();
    //echo "<pre>";print_r($this->db->last_query());
  return $result;


}

//function for fetching admin details
function fetchAdminDetails(){
  $result=$this->db->select('*')
  ->from('tbl_admin')
  ->get()->row();
                 //print_r($this->db->last_query());exit;
  return $result;



}

//function for editing admin profile
function editAdminProfile($edit_profile,$id){
  $result=$this->db->where('id',$id)->update('tbl_admin',$edit_profile);
  return $result;

}

//function for saving profile pic
function saveProfilePic($updateArr,$uid){

  $result=$this->db->where('admin_id',$uid)->update('tbl_profile_picture',$updateArr);
  return $result;
}

//function for fetching profile picture
function getProfilePicture(){
  $result=$this->db->select('*')
  ->from('tbl_profile_picture')
  ->where('admin_id','1')
  ->get()->row();
  return $result;


}

//function for checking old password
function check_old_password($admin_id, $password){

  $result=$this->db->select('*')
  ->from('tbl_admin')
  ->where('id',$admin_id)
  ->where('admin_password',$password)
  ->get()->row();
  return $result;


}

//function for changing old password
function change_password($admin_id, $updateArr){
  $result=$this->db->where('id',$admin_id)->update('tbl_admin',$updateArr);
  return $result;

}

//function for fetching job data
function fetchJobData($id){
  $result=$this->db->select('tbl_job_create.*,tbl_mst_countries.id,tbl_mst_status.status_id,tbl_mst_experience.ex_id,tbl_mst_job_type.id as jobtype_id,tbl_subClient_create.subclient_name,tbl_mst_currency.currency_id')
  ->from('tbl_job_create')
  ->join('tbl_mst_countries','tbl_job_create.nationality=tbl_mst_countries.id')
  ->join('tbl_mst_currency','tbl_mst_currency.currency_id=tbl_job_create.currency')
  //->join('tbl_mst_industry','tbl_job_create.industry=tbl_mst_industry.ind_id')
  ->join('tbl_mst_status','tbl_job_create.job_status=tbl_mst_status.status_id')
  ->join('tbl_mst_experience','tbl_job_create.experience=tbl_mst_experience.ex_id')
  ->join('tbl_mst_job_type','tbl_job_create.job_type=tbl_mst_job_type.id')
  ->join('tbl_subClient_create','tbl_subClient_create.id=tbl_job_create.parent_client','left')
                 //->join('tbl_mst_skill','tbl_job_create.skills=tbl_mst_skill.id')
  ->where('tbl_job_create.j_id',$id)
  ->get()->row();
                 //print_r($this->db->last_query());exit;
  return $result;

}

//function for fetching all currency
  function fetchCurrencies(){
  $result=$this->db->select('*')
                   ->from('tbl_mst_currency')
                   ->get()->result();
                   return $result;

  }

//function for updating job
function updateJobCreate($update_job,$job_id){
  $result=$this->db->where('j_id',$job_id)->update('tbl_job_create',$update_job);
  //echo '<pre>';print_r($this->db->last_query());exit;
  return $result;


}

//function for inserting blog details
function insertBlogDetails($insert_blog){
$result=$this->db->insert('tbl_blog',$insert_blog);
return $result;

}

//function for fetching blog list
function get_blog_list(){

$sql="select * from tbl_blog ";
$query=$this->db->query($sql);
    //echo "<pre>";print_r($query);exit;
    if($query->result_id->num_rows > 0){
     return $query->result();
    }else{
      return false;
    }
}

//function for searching a blog
function getBlogfilteredList($requestData){
$sql = "select * from tbl_blog";
 //$sql.=" WHERE `tbl_client`.`client_id` = '".$clientId."'";
        if (!empty($requestData['search']['value'])) {   // if there is a search parameter, $requestData['search']['value'] contains search parameter
            $sql.=" where admin_name LIKE '" . $requestData['search']['value'] . "%' ";
            $sql.=" OR blog_title LIKE '" . $requestData['search']['value'] . "%' ";
  //          $sql.=" OR first_name LIKE '" . $requestData['search']['value'] . "%' ";
//            $sql.=" OR last_name LIKE '" . $requestData['search']['value'] . "%' ";
            //$sql.=" OR (link LIKE '" . $requestData['search']['value'] . "%' )";
            //$sql.=" OR (logo LIKE '" . $requestData['search']['value'] . "%' )";
}
$query = $this->db->query($sql);
if ($query->result_id->num_rows > 0) {
    return $query->result();
} else {
    return FALSE;
}


}

//function for fetching blogs
function getBlogList($requestData){
$columns = array(
        0 => 'id',
        1 => 'admin_name',
        2 => 'blog_title',
        3 => 'blog_desc',
        4 => 'created_date'
        );
    $sql = "select * from tbl_blog";
    //$sql.=" WHERE `user_mobile_numbers`.`isPrimary` = '1'";
        if (!empty($requestData['search']['value'])) {   // if there is a search parameter, $requestData['search']['value'] contains search parameter
        $sql.=" where admin_name LIKE '" . $requestData['search']['value'] . "%' ";
        $sql.=" OR blog_title LIKE '" . $requestData['search']['value'] . "%' ";

//            $sql.=" OR first_name LIKE '" . $requestData['search']['value'] . "%' ";
//            $sql.=" OR    last_name LIKE '" . $requestData['search']['value'] . "%' ";
            //$sql.=" OR (link LIKE '" . $requestData['search']['value'] . "%' )";
            //$sql.=" OR (logo LIKE '" . $requestData['search']['value'] . "%' )";
}
$sql.=" ORDER BY " . $columns[$requestData['order'][0]['column']] . "   " . $requestData['order'][0]['dir'] . "  LIMIT " . $requestData['start'] . " ," . $requestData['length'] . "   ";
$query = $this->db->query($sql);
if ($query->result_id->num_rows > 0) {
    return $query->result();
} else {
    return FALSE;
}



}

//function for fetching main job list
function get_mainJob_list(){
//$sql="SELECT `tbl_job_create`.*, `tbl_mst_status`.`status_name`, `tbl_client`.`clnt_first_name`, `tbl_client`.`clnt_last_name`, `tbl_mst_industry`.`industry_name`, `tbl_subClient_create`.`subclient_name` FROM `tbl_job_create` JOIN `tbl_mst_status` ON `tbl_job_create`.`job_status`=`tbl_mst_status`.`status_id` JOIN `tbl_client` ON `tbl_client`.`id`=`tbl_job_create`.`client_id` JOIN `tbl_mst_industry` ON `tbl_mst_industry`.`ind_id`=`tbl_job_create`.`industry` LEFT JOIN `tbl_subClient_create` ON `tbl_subClient_create`.`id`=`tbl_job_create`.`parent_client`";
  $sql="select tbl_job_create.*,tbl_mst_currency.currency_code,tbl_mst_status.status_name,tbl_subClient_create.subclient_name,tbl_mst_job_type.job_type from tbl_job_create join tbl_mst_status on tbl_mst_status.status_id=tbl_job_create.job_status left join tbl_subClient_create on tbl_subClient_create.id=tbl_job_create.parent_client join tbl_mst_job_type on tbl_mst_job_type.id=tbl_job_create.job_type inner join tbl_mst_currency on tbl_mst_currency.currency_id=tbl_job_create.currency ";
$query=$this->db->query($sql);
    //echo "<pre>";print_r($query);exit;
    if($query->result_id->num_rows > 0){
     return $query->result();
    }else{
      return false;
    }

}

//function for searching job list
function getMainJobfilteredList($requestData,$job_title,$min_salary,$max_salary,$job_type,$client_name){
  //echo 'here';exit;
//$sql = "SELECT `tbl_job_create`.*, `tbl_mst_status`.`status_name`, `tbl_client`.`clnt_first_name`, `tbl_client`.`clnt_last_name`, `tbl_mst_industry`.`industry_name`, `tbl_subClient_create`.`subclient_name` FROM `tbl_job_create` JOIN `tbl_mst_status` ON `tbl_job_create`.`job_status`=`tbl_mst_status`.`status_id` JOIN `tbl_client` ON `tbl_client`.`id`=`tbl_job_create`.`client_id` JOIN `tbl_mst_industry` ON `tbl_mst_industry`.`ind_id`=`tbl_job_create`.`industry` LEFT JOIN `tbl_subClient_create` ON `tbl_subClient_create`.`id`=`tbl_job_create`.`parent_client`";
  $sql="select tbl_job_create.*,tbl_mst_currency.currency_code,tbl_mst_status.status_name,tbl_subClient_create.subclient_name,tbl_mst_job_type.job_type from tbl_job_create join tbl_mst_status on tbl_mst_status.status_id=tbl_job_create.job_status left join tbl_subClient_create on tbl_subClient_create.id=tbl_job_create.parent_client join tbl_mst_job_type on tbl_mst_job_type.id=tbl_job_create.job_type inner join tbl_mst_currency on tbl_mst_currency.currency_id=tbl_job_create.currency ";
 //$sql.=" WHERE `tbl_client`.`client_id` = '".$clientId."'";
        if (!empty($requestData['search']['value'])) {  
          //echo 'here';exit;
         // if there is a search parameter, $requestData['search']['value'] contains search parameter
            $sql.=" where `tbl_job_create`.`job_title` LIKE '" . $requestData['search']['value'] . "%' ";
            $sql.=" OR `tbl_job_create`.`recruiter` LIKE '" . $requestData['search']['value'] . "%' ";
            $sql.=" OR `tbl_job_create`.`job_status` LIKE '" . $requestData['search']['value'] . "%' ";
//            $sql.=" OR last_name LIKE '" . $requestData['search']['value'] . "%' ";
            //$sql.=" OR (link LIKE '" . $requestData['search']['value'] . "%' )";
            //$sql.=" OR (logo LIKE '" . $requestData['search']['value'] . "%' )";
}
else{
     if($job_title!='' || $job_type!='' || $client_name!=''){
     $sql.=" where tbl_job_create.job_title LIKE '$job_title' OR tbl_job_create.job_type LIKE '$job_type' OR tbl_job_create.parent_client LIKE '$client_name'";
   }else if($min_salary!='' && $max_salary!=''){
      $sql.=" where tbl_job_create.min_salary>='$min_salary' AND tbl_job_create.max_salary<='$max_salary'";
    }
}
//echo $sql;exit;
$query = $this->db->query($sql);
//echo '<pre>';print_r($this->db->last_query());exit;

if ($query->result_id->num_rows > 0) {
    return $query->result();
} else {
    return FALSE;
}


}

//function for fetching main jobs columns
function getMainJobList($requestData,$job_title,$min_salary,$max_salary,$job_type,$client_name){
$columns = array(
        0 => 'id',
        1 => 'job_title',
        2 => 'recruiter',
        3 => 'min_salary'.'-'.'max_salary',
        4 => 'parent_client',
        5 =>'created_date',
        6=>'open_date',
        7=>'industry'
        );
    //$sql = "SELECT `tbl_job_create`.*, `tbl_mst_status`.`status_name`, `tbl_client`.`clnt_first_name`, `tbl_client`.`clnt_last_name`, `tbl_mst_industry`.`industry_name`, `tbl_subClient_create`.`subclient_name` FROM `tbl_job_create` JOIN `tbl_mst_status` ON `tbl_job_create`.`job_status`=`tbl_mst_status`.`status_id` JOIN `tbl_client` ON `tbl_client`.`id`=`tbl_job_create`.`client_id` JOIN `tbl_mst_industry` ON `tbl_mst_industry`.`ind_id`=`tbl_job_create`.`industry` LEFT JOIN `tbl_subClient_create` ON `tbl_subClient_create`.`id`=`tbl_job_create`.`parent_client`";
$sql="select tbl_job_create.*,tbl_mst_currency.currency_code,tbl_mst_status.status_name,tbl_subClient_create.subclient_name,tbl_mst_job_type.job_type from tbl_job_create join tbl_mst_status on tbl_mst_status.status_id=tbl_job_create.job_status left join tbl_subClient_create on tbl_subClient_create.id=tbl_job_create.parent_client join tbl_mst_job_type on tbl_mst_job_type.id=tbl_job_create.job_type inner join tbl_mst_currency on tbl_mst_currency.currency_id=tbl_job_create.currency ";
    //$sql.=" WHERE `user_mobile_numbers`.`isPrimary` = '1'";
        if (!empty($requestData['search']['value'])) {  
          //echo 'here';exit;
         // if there is a search parameter, $requestData['search']['value'] contains search parameter
            $sql.=" where `tbl_job_create`.`job_title` LIKE '" . $requestData['search']['value'] . "%' ";
            $sql.=" OR `tbl_job_create`.`recruiter` LIKE '" . $requestData['search']['value'] . "%' ";
            $sql.=" OR `tbl_job_create`.`job_status` LIKE '" . $requestData['search']['value'] . "%' ";
//            $sql.=" OR last_name LIKE '" . $requestData['search']['value'] . "%' ";
            //$sql.=" OR (link LIKE '" . $requestData['search']['value'] . "%' )";
            //$sql.=" OR (logo LIKE '" . $requestData['search']['value'] . "%' )";
}
else{
     if($job_title!='' || $job_type!='' || $client_name!=''){
     $sql.=" where tbl_job_create.job_title LIKE '$job_title' OR tbl_job_create.job_type LIKE '$job_type' OR tbl_job_create.parent_client LIKE '$client_name'";
   }else if($min_salary!='' && $max_salary!=''){
      $sql.=" where tbl_job_create.min_salary>='$min_salary' AND tbl_job_create.max_salary<='$max_salary'";
    }

}
//$sql.=" ORDER BY " . $columns[$requestData['order'][0]['column']] . "   " . $requestData['order'][0]['dir'] . "  LIMIT " . $requestData['start'] . " ," . $requestData['length'] . "   ";
$query = $this->db->query($sql);
if ($query->result_id->num_rows > 0) {
    return $query->result();
} else {
    return FALSE;
}

}

//function for fetching all subclients
function getAllSubClients(){
$result=$this->db->select('*')
                 ->from('tbl_subClient_create')
                 ->get()->result();
                 return $result;

}
//functtion for fetching all job type
function getJobType(){
$result=$this->db->select('*')
                 ->from('tbl_mst_job_type')
                 ->get()->result();
                 return $result;

}


//function for fetching all offices
function getAllOfficesList(){
$sql="SELECT * from tbl_client where client_id='0'";
$query=$this->db->query($sql);
    //echo "<pre>";print_r($query);exit;
    if($query->result_id->num_rows > 0){
     return $query->result();
    }else{
      return false;
    }

}

//function for fetching all offices filtered list
function getOfficesfilteredList($requestData){
$sql = "SELECT * from tbl_client where client_id='0'";
 //$sql.=" WHERE `tbl_client`.`client_id` = '".$clientId."'";
        if (!empty($requestData['search']['value'])) {  
          //echo 'here';exit;
         // if there is a search parameter, $requestData['search']['value'] contains search parameter
            //$sql.=" where clnt_first_name LIKE '" . $requestData['search']['value'] . "%' ";
            $sql.=" AND clnt_first_name LIKE '" . $requestData['search']['value'] . "%' ";
            // $sql.=" OR `tbl_job_create`.`job_status` LIKE '" . $requestData['search']['value'] . "%' ";
//            $sql.=" OR last_name LIKE '" . $requestData['search']['value'] . "%' ";
            //$sql.=" OR (link LIKE '" . $requestData['search']['value'] . "%' )";
            //$sql.=" OR (logo LIKE '" . $requestData['search']['value'] . "%' )";
}
//echo $sql;exit;
$query = $this->db->query($sql);
//echo '<pre>';print_r($query);exit;
if ($query->result_id->num_rows > 0) {
    return $query->result();
} else {
    return FALSE;
}



} 

//function for fetching all office data list
function getOfficeList($requestData){

$columns = array(
        0 => 'id',
        1 => 'clnt_first_name'
       
        );
    $sql = "SELECT * from tbl_client where client_id='0'";
    //$sql.=" WHERE `user_mobile_numbers`.`isPrimary` = '1'";
        if (!empty($requestData['search']['value'])) {   // if there is a search parameter, $requestData['search']['value'] 

         $sql.=" AND clnt_first_name LIKE '" . $requestData['search']['value'] . "%' ";

           }
$sql.=" ORDER BY " . $columns[$requestData['order'][0]['column']] . "   " . $requestData['order'][0]['dir'] . "  LIMIT " . $requestData['start'] . " ," . $requestData['length'] . "   ";
$query = $this->db->query($sql);
if ($query->result_id->num_rows > 0) {
    return $query->result();
} else {
    return FALSE;
}

}

//function for viewing all candidates
function getAllCandidatesList(){
$sql="SELECT tbl_user.*,tbl_user_profile_progress_info.* from tbl_user join tbl_user_profile_progress_info on tbl_user.id=tbl_user_profile_progress_info.u_id";
$query=$this->db->query($sql);
    //echo "<pre>";print_r($query);exit;
    if($query->result_id->num_rows > 0){
     return $query->result();
    }else{
      return false;
    }


}

//function for viewing all apply jobs
function getAllCandidatesApplyJobs($id){
$sql="select tbl_job_create.job_title,tbl_job_create.min_salary,tbl_job_create.max_salary,tbl_user.first_name,tbl_user.last_name,tbl_user.user_city,tbl_user_applied_job.user_id,tbl_user.user_email from tbl_job_create join tbl_user_applied_job on tbl_user_applied_job.job_id=tbl_job_create.j_id join tbl_user on tbl_user.id=tbl_user_applied_job.user_id where tbl_user_applied_job.user_id='".$id."'";
$query=$this->db->query($sql);
    //echo "<pre>";print_r($query);exit;
    if($query->result_id->num_rows > 0){
     return $query->result();
    }else{
      return false;
    }

}

//function for searching candidates record
function getCandidatesfilteredList($requestData,$candidate_first_name,$candidate_last_name,$email,$city){

$sql = "SELECT tbl_user.*,tbl_user_profile_progress_info.* from tbl_user join tbl_user_profile_progress_info on tbl_user.id=tbl_user_profile_progress_info.u_id";
 //$sql.=" WHERE `tbl_client`.`client_id` = '".$clientId."'";
       if (!empty($requestData['search']['value'])) {  
          //echo 'here';exit;
         // if there is a search parameter, $requestData['search']['value'] contains search parameter
            //$sql.=" where clnt_first_name LIKE '" . $requestData['search']['value'] . "%' ";
            $sql.=" where first_name LIKE '" . $requestData['search']['value'] . "%' ";
            $sql.=" OR last_name LIKE '" . $requestData['search']['value'] . "%' ";
            $sql.=" OR user_email LIKE '" . $requestData['search']['value'] . "%' ";
            $sql.=" OR user_city LIKE '" . $requestData['search']['value'] . "%' ";
            //$sql.=" OR (link LIKE '" . $requestData['search']['value'] . "%' )";
            //$sql.=" OR (logo LIKE '" . $requestData['search']['value'] . "%' )";
}else{
            $sql.=" where(tbl_user.first_name LIKE '$candidate_first_name' OR tbl_user.last_name LIKE '$candidate_last_name' OR tbl_user.user_email LIKE '$email'  OR tbl_user.user_city LIKE '$city')";
            // if($candidate_last_name)
            
            // $sql.=" OR last_name LIKE '" . $candidate_last_name . "%' ";
            // if($email)
            // $sql.=" OR user_email LIKE '" . $email . "%' ";
            
            // if($city)
            // $sql.=" OR user_city LIKE '" . $city . "%' ";


}
//echo $sql;exit;
$query = $this->db->query($sql);
//echo '<pre>';print_r($query);exit;
if ($query->result_id->num_rows > 0) {
    return $query->result();
} else {
    return FALSE;
}



}

//function for searching candidates apply jobs
function getCandidatesApplyJobsfilteredList($requestData,$id){
$sql = "select tbl_job_create.job_title,tbl_user.first_name,tbl_job_create.min_salary,tbl_job_create.max_salary,tbl_user.last_name,tbl_user.user_city,tbl_user_applied_job.user_id,tbl_user.user_email from tbl_job_create join tbl_user_applied_job on tbl_user_applied_job.job_id=tbl_job_create.j_id join tbl_user on tbl_user.id=tbl_user_applied_job.user_id where tbl_user_applied_job.user_id='".$id."'";
 //$sql.=" WHERE `tbl_client`.`client_id` = '".$clientId."'";
        if (!empty($requestData['search']['value'])) {  
          //echo 'here';exit;
         // if there is a search parameter, $requestData['search']['value'] contains search parameter
            //$sql.=" where clnt_first_name LIKE '" . $requestData['search']['value'] . "%' ";
            //$sql.=" where first_name LIKE '" . $requestData['search']['value'] . "%' ";
            $sql.=" OR `tbl_job_create`.`job_title` LIKE '" . $requestData['search']['value'] . "%' ";
            $sql.=" OR `tbl_user`.`user_city` LIKE '" . $requestData['search']['value'] . "%' ";
            //$sql.=" OR (link LIKE '" . $requestData['search']['value'] . "%' )";
            //$sql.=" OR (logo LIKE '" . $requestData['search']['value'] . "%' )";
}
//echo $sql;exit;
$query = $this->db->query($sql);
//echo '<pre>';print_r($query);exit;
if ($query->result_id->num_rows > 0) {
    return $query->result();
} else {
    return FALSE;
}


}



//function for fetching all candidates columns
function getCandidatesList($requestData,$candidate_first_name,$candidate_last_name,$email,$city){
$columns = array(
        0 => 'tbl_user.id',
        1 => 'first_name'.' '.'last_name',
        2 => 'user_city',
        3 => 'user_email'
        );
    $sql = "SELECT tbl_user.*,tbl_user_profile_progress_info.* from tbl_user join tbl_user_profile_progress_info on tbl_user.id=tbl_user_profile_progress_info.u_id";
    //$sql.=" WHERE `user_mobile_numbers`.`isPrimary` = '1'";
         if (!empty($requestData['search']['value'])) {   // if there is a search parameter, $requestData['search']['value'] 

            $sql.=" where first_name LIKE '" . $requestData['search']['value'] . "%' ";
            $sql.=" where last_name LIKE '" . $requestData['search']['value'] . "%' ";
            $sql.=" OR user_email LIKE '" . $requestData['search']['value'] . "%' ";
            $sql.=" OR user_city LIKE '" . $requestData['search']['value'] . "%' ";

      }else{
          //   $sql.=" where first_name LIKE '" . $candidate_first_name . "%' ";
          //   if($candidate_last_name)
            
          //   $sql.=" OR last_name LIKE '" . $candidate_last_name . "%' ";
          // if($email)
          //   $sql.=" OR user_email LIKE '" . $email . "%' ";
            
          //   if($city)
          //   $sql.=" OR user_city LIKE '" . $city . "%' ";
        if($candidate_first_name!='' || $candidate_last_name!='' || $email!='' || $city!='')
        $sql.=" where(tbl_user.first_name LIKE '$candidate_first_name' OR tbl_user.last_name LIKE '$candidate_last_name' OR tbl_user.user_email LIKE '$email'  OR tbl_user.user_city LIKE '$city')";
      }
//$sql.=" ORDER BY " . $columns[$requestData['order'][0]['column']] . "   " . $requestData['order'][0]['dir'] . "  LIMIT " . $requestData['start'] . " ," . $requestData['length'] . "   ";
$query = $this->db->query($sql);
if ($query->result_id->num_rows > 0) {
    return $query->result();
} else {
    return FALSE;
}


}

//function for fetching apply jobs columns
function getCandidatesApplyJobsList($requestData,$id){
$columns = array(
        0 => 'tbl_user.id',
        1 => 'first_name'.' '.'last_name',
        2 => 'user_city',
        3 => 'job_title',
        4 => 'user_email'
        );
    $sql = "select tbl_job_create.job_title,tbl_user.first_name,tbl_job_create.min_salary,tbl_job_create.max_salary,tbl_user.last_name,tbl_user.user_city,tbl_user_applied_job.user_id,tbl_user.user_email from tbl_job_create join tbl_user_applied_job on tbl_user_applied_job.job_id=tbl_job_create.j_id join tbl_user on tbl_user.id=tbl_user_applied_job.user_id where tbl_user_applied_job.user_id='".$id."'";
    //$sql.=" WHERE `user_mobile_numbers`.`isPrimary` = '1'";
        if (!empty($requestData['search']['value'])) {   // if there is a search parameter, $requestData['search']['value'] 

            //$sql.=" where first_name LIKE '" . $requestData['search']['value'] . "%' ";
           // $sql.=" OR user_email LIKE '" . $requestData['search']['value'] . "%' ";
            $sql.=" OR `tbl_job_create`.`job_title` LIKE '" . $requestData['search']['value'] . "%' ";
            $sql.=" OR user_city LIKE '" . $requestData['search']['value'] . "%' ";

      }
$sql.=" ORDER BY " . $columns[$requestData['order'][0]['column']] . "   " . $requestData['order'][0]['dir'] . "  LIMIT " . $requestData['start'] . " ," . $requestData['length'] . "   ";
$query = $this->db->query($sql);
if ($query->result_id->num_rows > 0) {
    return $query->result();
} else {
    return FALSE;
}



}

//function for viewing sub offices list
function getAllSubOfficesList(){
$sql="SELECT `tbl_job_create`.*, `tbl_mst_status`.`status_name`, `tbl_subClient_create`.`subclient_name`, `tbl_mst_job_type`.`job_type`, `tbl_subClient_create`.`subclient_name`FROM `tbl_job_create` JOIN `tbl_mst_status` ON `tbl_job_create`.`job_status`=`tbl_mst_status`.`status_id` JOIN `tbl_subClient_create` ON `tbl_subClient_create`.`id`=`tbl_job_create`.`subClient_id` JOIN `tbl_mst_job_type` ON `tbl_mst_job_type`.`id`=`tbl_job_create`.`job_type`";
$query=$this->db->query($sql);
 if($query->result_id->num_rows > 0){
     return $query->result();
    }else{
      return false;
    }

}

//function for viewing sub offices filtered list
function getSubOfficesfilteredList($requestData){
$sql = "SELECT `tbl_job_create`.*, `tbl_mst_status`.`status_name`, `tbl_subClient_create`.`subclient_name`, `tbl_mst_job_type`.`job_type`, `tbl_subClient_create`.`subclient_name`FROM `tbl_job_create` JOIN `tbl_mst_status` ON `tbl_job_create`.`job_status`=`tbl_mst_status`.`status_id` JOIN `tbl_subClient_create` ON `tbl_subClient_create`.`id`=`tbl_job_create`.`subClient_id` JOIN `tbl_mst_job_type` ON `tbl_mst_job_type`.`id`=`tbl_job_create`.`job_type`";
 //$sql.=" WHERE `tbl_client`.`client_id` = '".$clientId."'";
        if (!empty($requestData['search']['value'])) {  
          //echo 'here';exit;
         // if there is a search parameter, $requestData['search']['value'] contains search parameter
            //$sql.=" where clnt_first_name LIKE '" . $requestData['search']['value'] . "%' ";
            $sql.=" where `tbl_job_create`.`job_title` LIKE '" . $requestData['search']['value'] . "%' ";
            $sql.=" OR `tbl_job_create`.`recruiter` LIKE '" . $requestData['search']['value'] . "%' ";
            $sql.=" OR `tbl_job_create`.`job_status` LIKE '" . $requestData['search']['value'] . "%' ";
            //$sql.=" OR (link LIKE '" . $requestData['search']['value'] . "%' )";
            //$sql.=" OR (logo LIKE '" . $requestData['search']['value'] . "%' )";
}
//echo $sql;exit;
$query = $this->db->query($sql);
//echo '<pre>';print_r($query);exit;
if ($query->result_id->num_rows > 0) {
    return $query->result();
} else {
    return FALSE;
}

}

//function for viewing sub offices columns
function getSubOfficesList($requestData){

$columns = array(
        0 => 'id',
        1 => 'job_title',
        2 => 'recruiter',
        3 => 'min_salary'.'-'.'max_salary',
        4 => 'parent_client',
        5 =>'created_date',
        6=>'open_date',
        7=>'industry'
        );
    $sql = "SELECT `tbl_job_create`.*, `tbl_mst_status`.`status_name`, `tbl_subClient_create`.`subclient_name`, `tbl_mst_job_type`.`job_type`, `tbl_subClient_create`.`subclient_name`FROM `tbl_job_create` JOIN `tbl_mst_status` ON `tbl_job_create`.`job_status`=`tbl_mst_status`.`status_id` JOIN `tbl_subClient_create` ON `tbl_subClient_create`.`id`=`tbl_job_create`.`subClient_id` JOIN `tbl_mst_job_type` ON `tbl_mst_job_type`.`id`=`tbl_job_create`.`job_type`";
    //$sql.=" WHERE `user_mobile_numbers`.`isPrimary` = '1'";
        if (!empty($requestData['search']['value'])) {   // if there is a search parameter, $requestData['search']['value'] contains search parameter
        //$sql.=" where admin_name LIKE '" . $requestData['search']['value'] . "%' ";
        //$sql.=" OR blog_title LIKE '" . $requestData['search']['value'] . "%' ";
            $sql.=" where `tbl_job_create`.`job_title` LIKE '" . $requestData['search']['value'] . "%' ";
            $sql.=" OR `tbl_job_create`.`recruiter` LIKE '" . $requestData['search']['value'] . "%' ";
            $sql.=" OR `tbl_job_create`.`job_status` LIKE '" . $requestData['search']['value'] . "%' ";

//            $sql.=" OR first_name LIKE '" . $requestData['search']['value'] . "%' ";
//            $sql.=" OR    last_name LIKE '" . $requestData['search']['value'] . "%' ";
            //$sql.=" OR (link LIKE '" . $requestData['search']['value'] . "%' )";
            //$sql.=" OR (logo LIKE '" . $requestData['search']['value'] . "%' )";
}
//$sql.=" ORDER BY " . $columns[$requestData['order'][0]['column']] . "   " . $requestData['order'][0]['dir'] . "  LIMIT " . $requestData['start'] . " ," . $requestData['length'] . "   ";
$query = $this->db->query($sql);
if ($query->result_id->num_rows > 0) {
    return $query->result();
} else {
    return FALSE;
}


}

//function for fetching for blog information
function getBlogInfo($id){
$result=$this->db->select('*')
                 ->from('tbl_blog')
                 ->where('id',$id)
                 ->get()->row();
                 return $result;
 }

 //function for fetching blog data 
 function fetchBlogData($id){
 $result=$this->db->select('*')
                  ->from('tbl_blog')
                  ->where('id',$id)
                  ->get()->row();
                  return $result;
 }

 //function for updating blog details
 function updateBlogDetails($insert_blog,$admin_id){
 $result=$this->db->where('admin_id',$admin_id)->update('tbl_blog',$insert_blog);
 return $result;

 }

 //function for deleting a blog
 function deleteBlog($id){
$result=$this->db->where('id',$id)->delete('tbl_blog');
return $result;

 }

 //function for fetching all Subclients
 function getClientsName(){
$result=$this->db->select('*')
                 ->from('tbl_subClient_create')
                 ->get()->result();
                 return $result;


 }

 //function for fetching main cleint name
 function getMainClientsName(){
$result=$this->db->select('*')
                 ->from('tbl_client')
                 ->where('client_id','0')
                 ->where('subClient_id','0')
                 ->get()->result();
                 return $result;

 }

 //function for fetching package details
 function getPackageDetails(){
$result=$this->db->select('*')
                 ->from('tbl_mst_package')
                 
                 ->get()->result();
                 return $result;

 }
 //function for fetching particular package job
function fetchParticularPackageJob($id){
$result=$this->db->select('*')
                 ->from('tbl_mst_package')
                 ->where('p_id',$id)
                 ->get()->row();
                 return $result;
}

//function for fetching all days
function getDays(){
$result=$this->db->select('*')
                 ->from('tbl_mst_day')
                 ->get()->result();
                 return $result;

}

//function for fetching all years
function getYears(){
$result=$this->db->select('*')
                 ->from('tbl_mst_year')
                 ->get()->result();
                 return $result;

}

//function for fetching candidate education info
function getCandidateEducationInfo($user_id){
$result=$this->db->select('*')
                 ->from('tbl_user_education_info')
                 ->where('user_id',$user_id)
                 ->get()->row();
               return $result;
}

//function for fetching personal info
function getCandidatePersonalInfo($user_id){
$result=$this->db->select('*')
                 ->from('tbl_user_personal_info')
                 ->where('user_id',$user_id)
                 ->get()->row();
               return $result;


}

//function for inserting CSV file
function insertCSV($data)
  {
      $this->db->insert('tbl_user', $data);
      return TRUE;
  }

//function for inserting client CSV file
  function insertClientCSV($data){
    $this->db->insert('tbl_client', $data);
      return TRUE;

  }

  //funtion for  inserting email data
  function insertEmailFormatData($insertArr){
  $result=$this->db->insert('tbl_admin_email_format',$insertArr);
  return $result;

  }

//function for inserting job advertisement
  function insertJobAdvertisement($insertArr){
  $result=$this->db->insert('tbl_job_adds',$insertArr);
  return $result;

  }

   //function for fetching job advertisement
  function getJobAdv($id){
  $result=$this->db->select('*')
                   ->from('tbl_job_adds')
                   ->where('package_id',$id)
                   ->get()->result();
                   return $result;
  }

  //function for inserting service
  function insertService($insertArr){

  $result=$this->db->insert('tbl_service',$insertArr);
  return $result;

  }

  //function for fetching service 
  function getService($admin_id){
  $result=$this->db->select('*')
                   ->from('tbl_service')
                   ->where('admin_id',$admin_id)
                   ->get()->result();
                   return $result;

  }

  //function for deleting service
  function deleteServiceById($id){

  $result=$this->db->where('id',$id)->delete('tbl_service');
  return $result;

  }

  //function for inserting package
  function insertPackageData($insert_package){
   
   $result=$this->db->insert('tbl_package',$insert_package);
   return $result;

  }

  //function for fetching package data
  function getPackageData($id){
  $result=$this->db->select('*')
                   ->from('tbl_package')
                   //->join('tbl_service','tbl_service.admin_id=tbl_package.admin_id')
                   ->where('tbl_package.id',$id)
                   ->get()->row();
                   return $result;

  }

  //function for fetching package data
  function checkPackageData($id){
  $result=$this->db->select('*')
                   ->from('tbl_package')
                   ->where('id',$id)
                   ->get()->row();
                   return $result;


  }

  //function for updating package data
  function updatePackageData($insert_package,$id){
  $result=$this->db->where('id',$id)->update('tbl_package',$insert_package);
  return $result;
 

  }
  //function for fetching candidate employment info
function getCandidateEmploymentInfo($user_id){
$result=$this->db->select('*')
                 ->from('tbl_user_pre_employment_info')
                 ->where('user_id',$user_id)
                 ->get()->row();
                 return $result;

}
//function for fetching reference info
function getCandidateReferenceInfo($user_id){
$result=$this->db->select('*')
                 ->from('tbl_user_references')
                 ->where('user_id',$user_id)
                 ->get()->row();
                 return $result;


}
//function for fetching references
function getReferences(){
$result=$this->db->select('*')
                 ->from('tbl_mst_references')
                 ->get()->result();
                  return $result;
}

//function for fetching acheived
function getAchieved(){
$result=$this->db->select('*')
                 ->from('tbl_mst_achieved')
                 ->get()->result();
               return $result;
}

//function for fetching qualifications
function getQualifications(){
$result=$this->db->select('*')
                 ->from('tbl_mst_qualification')
                 ->get()->result();
                 return $result;

}

//function for fetching grades
function getGrades(){
$result=$this->db->select('*')
                 ->from('tbl_mst_grade')
                 ->get()->result();
                 return $result;
}

//function for fetching rel status
function getRelStatus(){
$result=$this->db->select('*')
                 ->from('tbl_mst_relationship_status')
                 ->get()->result();
            return $result;

}

//function for fetching origins
function getOrigin(){
$result=$this->db->select('*')
                 ->from('tbl_mst_ethnic_origin')
                 ->get()->result();
            return $result;

}

//function for fetching count of employment details
function getEmploymentCount(){
$result=$this->db->select('count(id) as empcount')
                 ->from('tbl_user_pre_employment_info')
                 ->get()->row();
                 return $result;
}

//function for fetching count of education
function getEducationCount(){
$result=$this->db->select('count(id) as educationcount')
                 ->from('tbl_user_education_info')
                 ->get()->row();
                 return $result;


}
//function for fetching profile picture
  function getUserProfilePicture($user_id){
  $result=$this->db->select('*')
                   ->from('tbl_user_personal_info')
                   ->where('user_id',$user_id)
                   ->get()->row();
                   return $result;

  }

  //function for fetching all clients
  function getAllClients(){
  $result=$this->db->select('*')
                   ->from('tbl_client')
                   ->where('client_id','0')
                   ->where('subClient_id','0')          
                   ->get()->result();
                   return $result;

  }

  //function for fetching all candidates
  function fetchCandidates(){
  $result=$this->db->select('*')
                   ->from('tbl_user')          
                   ->get()->result();
                   return $result;


  }

  //function for inserting event info
  function insertEventInfo($insert_eventInfo){
  $result=$this->db->insert('tbl_event',$insert_eventInfo);
  return $result;

  }

  //function for fetching event data
  function fetchEventData($id){
  $result=$this->db->select('tbl_event.*,tbl_client.id as client,tbl_user.id as user')
                   ->from('tbl_event')
                   ->join('tbl_client','tbl_client.id=tbl_event.client_name')
                   ->join('tbl_user','tbl_user.id=tbl_event.candidate_name')
                   ->where('tbl_event.id',$id)
                   ->get()->row();
                   return $result;

  }

  //function for checking event data
  function checkEventData($event_id){
  $result=$this->db->select('*')
                   ->from('tbl_event') 
                   ->where('id',$event_id)         
                   ->get()->row();
                   return $result;


  }

  //function for updating event information
  function updateEventInfo($insert_eventInfo,$event_id){
  $result=$this->db->where('id',$event_id)->update('tbl_event',$insert_eventInfo);
  return $result;


  }

  //function for deleting events
  function deleteEventsId($id){
  $result=$this->db->where('id',$id)->delete('tbl_event');
  return $result;


  }
  //function for fetching events
function getAllEvents($admin_id){
$sql="select tbl_event.*,tbl_client.clnt_first_name,tbl_client.clnt_last_name,tbl_user.first_name,tbl_user.last_name from tbl_event join tbl_client on tbl_client.id=tbl_event.client_name join tbl_user on tbl_user.id=tbl_event.candidate_name where admin_id='".$admin_id."'";
$query=$this->db->query($sql);
    //echo "<pre>";print_r($query);exit;
    if($query->result_id->num_rows > 0){
     return $query->result();
    }else{
      return false;
    }

}

//function for searching events
function getEventsfilteredList($requestData,$admin_id){
$sql="select tbl_event.*,tbl_client.clnt_first_name,tbl_client.clnt_last_name,tbl_user.first_name,tbl_user.last_name from tbl_event join tbl_client on tbl_client.id=tbl_event.client_name join tbl_user on tbl_user.id=tbl_event.candidate_name where admin_id='".$admin_id."'";
 //$sql.=" WHERE `tbl_client`.`client_id` = '".$clientId."'";
        if (!empty($requestData['search']['value'])) {  
          //echo 'here';exit;
         // if there is a search parameter, $requestData['search']['value'] contains search parameter
            //$sql.=" where `tbl_job_create`.`job_title` LIKE '" . $requestData['search']['value'] . "%' ";
            $sql.=" OR `tbl_event`.`event_title` LIKE '" . $requestData['search']['value'] . "%' ";
            $sql.=" OR `tbl_event`.`type` LIKE '" . $requestData['search']['value'] . "%' ";
//            $sql.=" OR last_name LIKE '" . $requestData['search']['value'] . "%' ";
            //$sql.=" OR (link LIKE '" . $requestData['search']['value'] . "%' )";
            //$sql.=" OR (logo LIKE '" . $requestData['search']['value'] . "%' )";
}
//echo $sql;exit;
$query = $this->db->query($sql);

if ($query->result_id->num_rows > 0) {
    return $query->result();
} else {
    return FALSE;
}


}

//function for fetching event columns
function getEventsList($requestData,$admin_id){
$columns = array(
        0 => 'id',
        1 => 'event_title',
        2 => 'type',
        3 => 'date',
        4 => 'state',
        5 =>'description',
        6=>'created_date'
        //7=>'industry'
        );
    
$sql="select tbl_event.*,tbl_client.clnt_first_name,tbl_client.clnt_last_name,tbl_user.first_name,tbl_user.last_name from tbl_event join tbl_client on tbl_client.id=tbl_event.client_name join tbl_user on tbl_user.id=tbl_event.candidate_name where admin_id='".$admin_id."'";
    //$sql.=" WHERE `user_mobile_numbers`.`isPrimary` = '1'";
        if (!empty($requestData['search']['value'])) {   // if there is a search parameter, $requestData['
            //$sql.=" where `tbl_job_create`.`job_title` LIKE '" . $requestData['search']['value'] . "%' ";
            $sql.=" OR `tbl_event`.`event_title` LIKE '" . $requestData['search']['value'] . "%' ";
            $sql.=" OR `tbl_event`.`type` LIKE '" . $requestData['search']['value'] . "%' ";


}
//$sql.=" ORDER BY " . $columns[$requestData['order'][0]['column']] . "   " . $requestData['order'][0]['dir'] . "  LIMIT " . $requestData['start'] . " ," . $requestData['length'] . "   ";
$query = $this->db->query($sql);
if ($query->result_id->num_rows > 0) {
    return $query->result();
} else {
    return FALSE;
}


}

//function for fetching all events
  function fetchAllEvents($admin_id){
   $result=$this->db->select('tbl_event.*,tbl_client.clnt_first_name,tbl_client.clnt_last_name ,tbl_user.first_name,tbl_user.last_name')
                    ->from('tbl_event')
                    ->join('tbl_client','tbl_client.id=tbl_event.client_name')
                    ->join('tbl_user','tbl_user.id=tbl_event.candidate_name')
                    ->where('admin_id',$admin_id)
                    ->get()->result();
                    return $result;

  }

  //function for fetching event information
  function getEventInformation($event_id){
  $result=$this->db->select('tbl_event.*,tbl_client.clnt_first_name,tbl_client.clnt_last_name,tbl_user.first_name,tbl_user.last_name')
                   ->from('tbl_event')
                   ->join('tbl_client','tbl_client.id=tbl_event.client_name')
                   ->join('tbl_user','tbl_user.id=tbl_event.candidate_name')
                   ->where('tbl_event.id',$event_id)
                   ->get()->row();
                   return $result;


  }

  //function for inserting job adds
  function insertJobAdv($insertJobAdds){
   $result=$this->db->insert('tbl_job_adds',$insertJobAdds);
   return $result;


  }

  
//function of view and save cv's
function insertCVInfo($insertCVInfo){
$result=$this->db->insert('tbl_view_and_save_cv',$insertCVInfo);
return $result;

}

//function fetching cv info
function getCVInfo($id){
$result=$this->db->select('*')
                 ->from('tbl_view_and_save_cv')
                 ->where('package_id',$id)
                 ->get()->result();
                 return $result;

}

//function for deleting view and save cv'c
function deleteViewAndSaveCV($id){
$result=$this->db->where('id',$id)->delete('tbl_view_and_save_cv');
return $result;
}  

//function for fetching all job adds
function getAllJobAdds($id){
$result=$this->db->select('sum(job_adds) as jobAdv')
                 ->from('tbl_job_adds')
                 ->where_in('package_id',$id)
                 ->get()->row();
                 return $result;

}

//function for deleting job adds
function delete_job_adds($id){
$result=$this->db->where('id',$id)->delete('tbl_job_adds');
return $result;

  
}

//function for fetching Candidate Name
function getCandidateName($id){
$result=$this->db->select('tbl_user.first_name,tbl_user.last_name')
                 ->from('tbl_user')
                 ->where('id',$id)
                 ->get()->row();
                 return $result;
}

//function for inserting interview data
function insertinterviewInfo($insert_interviewInfo){
$result=$this->db->insert('tbl_interview',$insert_interviewInfo);
return $result;
  
}

//function for fetching all Interviews
function fetchAllInterviews($admin_id){
$result=$this->db->select('tbl_interview.*,tbl_user.first_name,tbl_user.last_name,tbl_client.clnt_first_name,tbl_client.clnt_last_name')
                 ->from('tbl_interview')
                 ->join('tbl_user','tbl_user.id=tbl_interview.candidate_name')
                 ->join('tbl_client','tbl_client.id=tbl_interview.client_name')
                 ->where('admin_id',$admin_id)
                 ->get()->result();
                 return $result;
}

//function for fetching interview info
function getInterviewInformation($interview_id){
$result=$this->db->select('tbl_interview.*,tbl_user.first_name,tbl_user.last_name,tbl_client.clnt_first_name,tbl_client.clnt_last_name')
                 ->from('tbl_interview')
                 ->join('tbl_user','tbl_user.id=tbl_interview.candidate_name')
                 ->join('tbl_client','tbl_client.id=tbl_interview.client_name')
                 ->where('tbl_interview.id',$interview_id)
                 ->get()->row();

                 return $result;

}

//function for fetching Service Data
function getServiceData($package_id){
$result=$this->db->select('*')
                 ->from('tbl_service')
                 ->where('package_id',$package_id)
                 ->get()->result();
                return $result;

}
//function for inserting services
function insertServices($serviceArray){
$result=$this->db->insert('tbl_service_package',$serviceArray);
return $result;

}

//function for fetching services
function getPackageServices($package_id){
$result=$this->db->select('service')
                 ->from('tbl_service_package')
                 ->where('package_id',$package_id)
                 ->get()->row();
                 return $result;

}

//function for updating services
function updateServices($insertService,$package_id){
$result=$this->db->where('package_id',$package_id)->update('tbl_service_package',$insertService);
return $result;

}

//function for showing services
public function get_service() {
  //$this->db->where('status', 1);
  $query = $this->db->get('tbl_service');
  $out = array();
  
  if ($query->num_rows() > 0) 
    {
      $result = $query->result();
      foreach ($result as $grp)$out[$grp->id] = $grp->service_name
        ;
    }
    return $out;
  }


//function for fetching interviews
function getAllInterviews($admin_id){
$sql="select tbl_interview.*,tbl_client.clnt_first_name,tbl_client.clnt_last_name,tbl_user.first_name,tbl_user.last_name from tbl_interview join tbl_client on tbl_client.id=tbl_interview.client_name join tbl_user on tbl_user.id=tbl_interview.candidate_name where admin_id='".$admin_id."'";
$query=$this->db->query($sql);
    //echo "<pre>";print_r($query);exit;
    if($query->result_id->num_rows > 0){
     return $query->result();
    }else{
      return false;
    }


}

//function for searching interview list
function getInterviewsfilteredList($requestData,$admin_id){

$sql="select tbl_interview.*,tbl_client.clnt_first_name,tbl_client.clnt_last_name,tbl_user.first_name,tbl_user.last_name from tbl_interview join tbl_client on tbl_client.id=tbl_interview.client_name join tbl_user on tbl_user.id=tbl_interview.candidate_name where admin_id='".$admin_id."'";
 //$sql.=" WHERE `tbl_client`.`client_id` = '".$clientId."'";
        if (!empty($requestData['search']['value'])) {  
          //echo 'here';exit;
         // if there is a search parameter, $requestData['search']['value'] contains search parameter
             $sql.=" OR `tbl_user`.`first_name` LIKE '" . $requestData['search']['value'] . "%' ";
             $sql.=" OR `tbl_user`.`last_name` LIKE '" . $requestData['search']['value'] . "%' ";
//            $sql.=" OR last_name LIKE '" . $requestData['search']['value'] . "%' ";
            //$sql.=" OR (link LIKE '" . $requestData['search']['value'] . "%' )";
            //$sql.=" OR (logo LIKE '" . $requestData['search']['value'] . "%' )";
}
//echo $sql;exit;
$query = $this->db->query($sql);

if ($query->result_id->num_rows > 0) {
    return $query->result();
} else {
    return FALSE;
}

}

//function for searching interviews
function getInterviewsList($requestData,$admin_id){

$columns = array(
        0 => 'id',
        1 => 'first_name'.' '.'last_name',
        2 => 'clnt_first_name'.''.'clnt_last_name',
        3 => 'interview_date',
        4 => 'interview_address',
        5 =>'main_contact',
        6=>'contact_details',
        7=>'created_date'
        //7=>'industry'
        );
    
$sql="select tbl_interview.*,tbl_client.clnt_first_name,tbl_client.clnt_last_name,tbl_user.first_name,tbl_user.last_name from tbl_interview join tbl_client on tbl_client.id=tbl_interview.client_name join tbl_user on tbl_user.id=tbl_interview.candidate_name where admin_id='".$admin_id."'";
    //$sql.=" WHERE `user_mobile_numbers`.`isPrimary` = '1'";
        if (!empty($requestData['search']['value'])) {   // if there is a search parameter, $requestData['
            //$sql.=" where `tbl_job_create`.`job_title` LIKE '" . $requestData['search']['value'] . "%' ";
            $sql.=" OR `tbl_user`.`first_name` LIKE '" . $requestData['search']['value'] . "%' ";
            $sql.=" OR `tbl_user`.`last_name` LIKE '" . $requestData['search']['value'] . "%' ";


}
//$sql.=" ORDER BY " . $columns[$requestData['order'][0]['column']] . "   " . $requestData['order'][0]['dir'] . "  LIMIT " . $requestData['start'] . " ," . $requestData['length'] . "   ";
$query = $this->db->query($sql);
if ($query->result_id->num_rows > 0) {
    return $query->result();
} else {
    return FALSE;
}
  
}

//function for fetching interview Data
function fetchInterviewDataById($id){
$result=$this->db->select('tbl_interview.*,tbl_user.first_name,tbl_user.last_name,tbl_client.clnt_first_name,tbl_client.clnt_last_name')
                 ->from('tbl_interview')
                 ->join('tbl_user','tbl_user.id=tbl_interview.candidate_name')
                 ->join('tbl_client','tbl_client.id=tbl_interview.client_name')
                 ->where('tbl_interview.id',$id)
                 ->get()->row();

                 return $result;


  
}

//function for checking interview data
function checkInterviewData($interview_id){
$result=$this->db->select('*')
                 ->from('tbl_interview')
                 ->where('id',$interview_id)
                 ->get()->row();
                 return $result;
  
}

//function for updating interview info
function updateinterviewInfo($insert_interviewInfo,$interview_id){
$result=$this->db->where('id',$interview_id)->update('tbl_interview',$insert_interviewInfo);
return $result;

}

//function for fetching package image data
function fetchPackageImgData($package_id){
$result=$this->db->select('*')
                 ->from('tbl_package_image')
                 ->where('package_id',$package_id)
                 ->get()->row();
                 return $result;

}

//function for inserting package pic
function insertPackagePic($insertArr){
$result=$this->db->insert('tbl_package_image',$insertArr);
return $result;

}

//function for saving package pic
function savePackagePic($updateArr,$package_id){
$result=$this->db->where('package_id',$package_id)->update('tbl_package_image',$updateArr);
return $result;

}
//function for fetching package pic
  function getPackagePic($id){
  $result=$this->db->select('*')
                   ->from('tbl_package_image')
                   ->where('package_id',$id)
                   ->get()->row();
                   return $result;


  }

  //function for fetching interviews
function getAllTimesheets($admin_id){
$sql="select * from tbl_timesheet where admin_id='".$admin_id."'";
$query=$this->db->query($sql);
    //echo "<pre>";print_r($query);exit;
    if($query->result_id->num_rows > 0){
     return $query->result();
    }else{
      return false;
    }


}

//function for searching timesheet list
function getTimesheetsfilteredList($requestData,$admin_id){
$sql="select tbl_interview.*,tbl_client.clnt_first_name,tbl_client.clnt_last_name,tbl_user.first_name,tbl_user.last_name from tbl_interview join tbl_client on tbl_client.id=tbl_interview.client_name join tbl_user on tbl_user.id=tbl_interview.candidate_name where admin_id='".$admin_id."'";
 //$sql.=" WHERE `tbl_client`.`client_id` = '".$clientId."'";
        if (!empty($requestData['search']['value'])) {  
          //echo 'here';exit;
         // if there is a search parameter, $requestData['search']['value'] contains search parameter
             $sql.=" OR `tbl_user`.`first_name` LIKE '" . $requestData['search']['value'] . "%' ";
             $sql.=" OR `tbl_user`.`last_name` LIKE '" . $requestData['search']['value'] . "%' ";
//            $sql.=" OR last_name LIKE '" . $requestData['search']['value'] . "%' ";
            //$sql.=" OR (link LIKE '" . $requestData['search']['value'] . "%' )";
            //$sql.=" OR (logo LIKE '" . $requestData['search']['value'] . "%' )";
}
//echo $sql;exit;
$query = $this->db->query($sql);

if ($query->result_id->num_rows > 0) {
    return $query->result();
} else {
    return FALSE;
}


}
//function for fetching timesheet columns
function getTimesheetsList($requestData,$admin_id){
$columns = array(
        0 => 'id',
        1 => 'type',
        2 => 'date',
        3 => 'Start Time',
        4 => 'Finish Time',
        5 => 'Unpaid Break Duration',
        6 =>'Pay Rate',
        7=>'currency',
        8=>'Pay Type'
        );
    
$sql="select * from tbl_timesheet where admin_id='".$admin_id."'";
    //$sql.=" WHERE `user_mobile_numbers`.`isPrimary` = '1'";
        if (!empty($requestData['search']['value'])) {   // if there is a search parameter, $requestData['
            //$sql.=" where `tbl_job_create`.`job_title` LIKE '" . $requestData['search']['value'] . "%' ";
             $sql.=" OR `break_duartion` LIKE '" . $requestData['search']['value'] . "%' ";
             $sql.=" OR `pay_rate` LIKE '" . $requestData['search']['value'] . "%' ";
             $sql.=" OR `currency` LIKE '" . $requestData['search']['value'] . "%' ";
             $sql.=" OR `pay_type` LIKE '" . $requestData['search']['value'] . "%' ";


}
//$sql.=" ORDER BY " . $columns[$requestData['order'][0]['column']] . "   " . $requestData['order'][0]['dir'] . "  LIMIT " . $requestData['start'] . " ," . $requestData['length'] . "   ";
$query = $this->db->query($sql);
if ($query->result_id->num_rows > 0) {
    return $query->result();
} else {
    return FALSE;
}


}

//function for inserting timesheet info
  function insertTimesheetInfo($insertTimesheetInfo){
  $result=$this->db->insert('tbl_timesheet',$insertTimesheetInfo);
  return $result;


  }

  //function for feching timesheet data
  function fetchTimesheetData($id){
  $result=$this->db->select('tbl_timesheet.*,tbl_user.first_name,tbl_user.last_name,tbl_client.clnt_first_name,tbl_client.clnt_last_name,tbl_mst_currency.currency_id')
                   ->from('tbl_timesheet')
                   ->join('tbl_user','tbl_user.id=tbl_timesheet.candidate_id')
                   ->join('tbl_client','tbl_client.id=tbl_timesheet.client_name')
                   ->join('tbl_mst_currency','tbl_mst_currency.currency_id=tbl_timesheet.currency')
                   ->where('tbl_timesheet.id',$id)
                   ->get()->row();
                   return $result;

  }

  //function for updating timesheet info
  function updateTimesheetInfo($insertTimesheetInfo,$id){
  $result=$this->db->where('id',$id)->update('tbl_timesheet',$insertTimesheetInfo);
  return $result;

  }

  //function for deleting timesheet
  function deleteTimesheet($id){
  $this->db->where('id',$id)->delete('tbl_timesheet');
  return true;


  }

  //function for fetching candidate data
  function fetchCandidateData($id){
  $result=$this->db->select('*')
                   ->from('tbl_user')
                   ->where('id',$id)
                   ->get()->row();
                   return $result;

  }

  //function for fetching group interview list
function getAllGroupInterviews($admin_id){

$sql="select tbl_group_interview.*,tbl_client.clnt_first_name,tbl_client.clnt_last_name from tbl_group_interview join tbl_client on tbl_client.id=tbl_group_interview.client_name where admin_id='".$admin_id."'";
$query=$this->db->query($sql);
    //echo "<pre>";print_r($query);exit;
    if($query->result_id->num_rows > 0){
     return $query->result();
    }else{
      return false;
    }

}


//function for fetching group interview candidates view
function getAllGroupInterviewCandidates($candidates){
//echo $candidates;exit;
$sql="select * from tbl_user where id in(".$candidates.")";
$query=$this->db->query($sql);
    //echo "<pre>";print_r($this->db->last_query());exit;
    if($query->result_id->num_rows > 0){
     return $query->result();
    }else{
      return false;
    }



}
//function for searching a group interview
function getGroupInterviewsfilteredList($requestData,$admin_id){
$sql = "select tbl_group_interview.*,tbl_client.clnt_first_name,tbl_client.clnt_last_name from tbl_group_interview join tbl_client on tbl_client.id=tbl_group_interview.client_name where admin_id='".$admin_id."'";
 //$sql.=" WHERE `tbl_client`.`client_id` = '".$clientId."'";
        if (!empty($requestData['search']['value'])) {   // if there is a search parameter, $requestData['search']['value'] contains search parameter
            $sql.=" OR group_name LIKE '" . $requestData['search']['value'] . "%' ";
            $sql.=" OR job_title LIKE '" . $requestData['search']['value'] . "%' ";
  //          $sql.=" OR first_name LIKE '" . $requestData['search']['value'] . "%' ";
//            $sql.=" OR last_name LIKE '" . $requestData['search']['value'] . "%' ";
            //$sql.=" OR (link LIKE '" . $requestData['search']['value'] . "%' )";
            //$sql.=" OR (logo LIKE '" . $requestData['search']['value'] . "%' )";
}
$query = $this->db->query($sql);
if ($query->result_id->num_rows > 0) {
    return $query->result();
} else {
    return FALSE;
}


}

//function for searching a candidates group interview
function getGroupInterviewCandidatesfilteredList($requestData,$candidates){
$sql = "select * from tbl_user where id in(".$candidates.")";
 //$sql.=" WHERE `tbl_client`.`client_id` = '".$clientId."'";
        if (!empty($requestData['search']['value'])) {   // if there is a search parameter, $requestData['search']['value'] contains search parameter
            $sql.=" OR group_name LIKE '" . $requestData['search']['value'] . "%' ";
            $sql.=" OR job_title LIKE '" . $requestData['search']['value'] . "%' ";
  //          $sql.=" OR first_name LIKE '" . $requestData['search']['value'] . "%' ";
//            $sql.=" OR last_name LIKE '" . $requestData['search']['value'] . "%' ";
            //$sql.=" OR (link LIKE '" . $requestData['search']['value'] . "%' )";
            //$sql.=" OR (logo LIKE '" . $requestData['search']['value'] . "%' )";
}
$query = $this->db->query($sql);
if ($query->result_id->num_rows > 0) {
    return $query->result();
} else {
    return FALSE;
}


}
//function for fetching group interviews
function getGroupInterviewsList($requestData,$admin_id){

$columns = array(
        0 => 'id',
        1 => 'group_name',
        2 => 'job_title',
        3 => 'client_name',
        4 => 'created_date'
        );
    $sql = "select tbl_group_interview.*,tbl_client.clnt_first_name,tbl_client.clnt_last_name from tbl_group_interview join tbl_client on tbl_client.id=tbl_group_interview.client_name where admin_id='".$admin_id."'";
    //$sql.=" WHERE `user_mobile_numbers`.`isPrimary` = '1'";
        if (!empty($requestData['search']['value'])) {   // if there is a search parameter, $requestData['search']['value'] contains search parameter
        $sql.=" OR group_name LIKE '" . $requestData['search']['value'] . "%' ";
        $sql.=" OR job_title LIKE '" . $requestData['search']['value'] . "%' ";

//            $sql.=" OR first_name LIKE '" . $requestData['search']['value'] . "%' ";
//            $sql.=" OR    last_name LIKE '" . $requestData['search']['value'] . "%' ";
            //$sql.=" OR (link LIKE '" . $requestData['search']['value'] . "%' )";
            //$sql.=" OR (logo LIKE '" . $requestData['search']['value'] . "%' )";
}
$sql.=" ORDER BY " . $columns[$requestData['order'][0]['column']] . "   " . $requestData['order'][0]['dir'] . "  LIMIT " . $requestData['start'] . " ," . $requestData['length'] . "   ";
$query = $this->db->query($sql);
if ($query->result_id->num_rows > 0) {
    return $query->result();
} else {
    return FALSE;
}


}

//function for fetching group interview
function getGroupInterviewCandidatesList($requestData,$candidates){
$columns = array(
        0 => 'id',
        1 => 'candidates',
        2 => 'city',
        3 => 'email'
       
        );
    $sql = "select * from tbl_user where id in(".$candidates.")";
    //$sql.=" WHERE `user_mobile_numbers`.`isPrimary` = '1'";
        if (!empty($requestData['search']['value'])) {   // if there is a search parameter, $requestData['search']['value'] contains search parameter
        $sql.=" OR first_name LIKE '" . $requestData['search']['value'] . "%' ";
        $sql.=" OR last_name LIKE '" . $requestData['search']['value'] . "%' ";

//            $sql.=" OR first_name LIKE '" . $requestData['search']['value'] . "%' ";
//            $sql.=" OR    last_name LIKE '" . $requestData['search']['value'] . "%' ";
            //$sql.=" OR (link LIKE '" . $requestData['search']['value'] . "%' )";
            //$sql.=" OR (logo LIKE '" . $requestData['search']['value'] . "%' )";
}
$sql.=" ORDER BY " . $columns[$requestData['order'][0]['column']] . "   " . $requestData['order'][0]['dir'] . "  LIMIT " . $requestData['start'] . " ," . $requestData['length'] . "   ";
$query = $this->db->query($sql);
if ($query->result_id->num_rows > 0) {
    return $query->result();
} else {
    return FALSE;
}



}

//function for fetching group candidates
  function fetchGroupCandidates($group_id){
  $result=$this->db->select('*')
                   ->from('tbl_group_interview')
                   ->where('id',$group_id)
                   ->get()->row();
                   return $result;

  }
  //function for inserting group interview
  function insertGroupInterviewDetails($insert_group_interview){
   $result=$this->db->insert('tbl_group_interview',$insert_group_interview);
   return $result;


  }
  //function for showing candidate Name
  function get_candidates(){
  $query = $this->db->get('tbl_user');
  $out = array();
  
  if ($query->num_rows() > 0) 
    {
      $result = $query->result();
      foreach ($result as $grp)
        $out[$grp->id] = $grp->first_name.' '.$grp->last_name;
        //$out[$grp->id] = $grp->user_city;

    }
    return $out;


  }

  //function for fetching group data
  function fetchGroupData($id){

  $result=$this->db->select('tbl_group_interview.*,tbl_client.clnt_first_name,tbl_client.clnt_last_name')
                   ->from('tbl_group_interview')
                   ->join('tbl_client','tbl_client.id=tbl_group_interview.client_name')
                   ->where('tbl_group_interview.id',$id)
                   ->get()->row();
                   return $result;

  }

  //function for updating group interview details
  function updateGroupInterviewDetails($insert_group_interview,$id){
  $result=$this->db->where('id',$id)->update('tbl_group_interview',$insert_group_interview);
  return $result;

  }
  //function for deleting group 
  function deleteGroupById($id){
  $result=$this->db->where('id',$id)->delete('tbl_group_interview');
  return $result;

  }

  //function for fetching homeless data
  function fetchHomelessdata($id){
   $result=$this->db->select('tbl_homeless.*,tbl_mst_countries.id as c_id')
                    ->from('tbl_homeless')
                    ->join('tbl_mst_countries','tbl_mst_countries.id=tbl_homeless.country')
                    ->where('tbl_homeless.id',$id)
                    ->get()->row();
                    return $result;

  }

  //function for fetching substance data
  function fetchSubstancedata($id){
  $result=$this->db->select('*')
                    ->from('tbl_substance')
                    
                    ->where('id',$id)
                    ->get()->row();
                    return $result;

  }


  //function for updating homeless data
  function updateHomelessData($insertHomelessData,$id){
  $result=$this->db->where('id',$id)->update('tbl_homeless',$insertHomelessData);
  return $result;

  }

  //function for updating substance data
  function updateSubstanceData($insertSubstanceData,$id){
  $result=$this->db->where('id',$id)->update('tbl_substance',$insertSubstanceData);
  return $result;

  }
  //function for fetching homeless candidates view
function getAllHomelessCandidates(){
$sql="select * from tbl_homeless ";
$query=$this->db->query($sql);
    //echo "<pre>";print_r($query);exit;
    if($query->result_id->num_rows > 0){
     return $query->result();
    }else{
      return false;
    }


}


//function for fetching substance candidates view
function getAllSubstanceCandidates(){
$sql="select * from tbl_substance ";
$query=$this->db->query($sql);
    //echo "<pre>";print_r($query);exit;
    if($query->result_id->num_rows > 0){
     return $query->result();
    }else{
      return false;
    }

}

//function for seraching a homeless candidate
function getHomelessCandidatesfilteredList($requestData){
$sql = "select tbl_homeless.*,tbl_mst_countries.country_name,tbl_user.first_name,tbl_user.last_name from tbl_homeless join tbl_mst_countries on tbl_mst_countries.id=tbl_homeless.country join tbl_user on tbl_user.id=tbl_homeless.user_id";
 //$sql.=" WHERE `tbl_client`.`client_id` = '".$clientId."'";
        if (!empty($requestData['search']['value'])) {   // if there is a search parameter, $requestData['search']['value'] contains search parameter
            $sql.=" where country LIKE '" . $requestData['search']['value'] . "%' ";
            $sql.=" OR city LIKE '" . $requestData['search']['value'] . "%' ";
  //          $sql.=" OR first_name LIKE '" . $requestData['search']['value'] . "%' ";
//            $sql.=" OR last_name LIKE '" . $requestData['search']['value'] . "%' ";
            //$sql.=" OR (link LIKE '" . $requestData['search']['value'] . "%' )";
            //$sql.=" OR (logo LIKE '" . $requestData['search']['value'] . "%' )";
}
$query = $this->db->query($sql);
if ($query->result_id->num_rows > 0) {
    return $query->result();
} else {
    return FALSE;
}

}

//function for searching a substance candidate
function getSubstanceCandidatesfilteredList($requestData){
$sql = "select tbl_substance.*,tbl_user.first_name,tbl_user.last_name from tbl_substance  join tbl_user on tbl_user.id=tbl_substance.user_id";
 //$sql.=" WHERE `tbl_client`.`client_id` = '".$clientId."'";
        if (!empty($requestData['search']['value'])) {   // if there is a search parameter, $requestData['search']['value'] contains search parameter
            $sql.=" where substance_name LIKE '" . $requestData['search']['value'] . "%' ";
            $sql.=" OR frequency LIKE '" . $requestData['search']['value'] . "%' ";
            $sql.=" OR frequency_use_now LIKE '" . $requestData['search']['value'] . "%' ";
  //          $sql.=" OR first_name LIKE '" . $requestData['search']['value'] . "%' ";
//            $sql.=" OR last_name LIKE '" . $requestData['search']['value'] . "%' ";
            //$sql.=" OR (link LIKE '" . $requestData['search']['value'] . "%' )";
            //$sql.=" OR (logo LIKE '" . $requestData['search']['value'] . "%' )";
}
$query = $this->db->query($sql);
if ($query->result_id->num_rows > 0) {
    return $query->result();
} else {
    return FALSE;
}


}
//function for deleting homeless
function deleteHomeless($id){
$result=$this->db->where('id',$id)->delete('tbl_homeless');
return $result;

}

//function for deleting substance
function deleteSubstance($id){
$result=$this->db->where('id',$id)->delete('tbl_substance');
return $result;

}
//function for homeless candidates
function getHomelessCandidatesList($requestData){
$columns = array(
        0 => 'id',
        1 => 'start_date',
        2 => 'end_date',
        3 => 'country',
        4 => 'city',
        5 => 'help_received',
        6 => 'help_rating',
        7 => 'significant_info'
        );
    $sql = "select tbl_homeless.*,tbl_mst_countries.country_name,tbl_user.first_name,tbl_user.last_name from tbl_homeless join tbl_mst_countries on tbl_mst_countries.id=tbl_homeless.country join tbl_user on tbl_user.id=tbl_homeless.user_id";
    //$sql.=" WHERE `user_mobile_numbers`.`isPrimary` = '1'";
        if (!empty($requestData['search']['value'])) {   // if there is a search parameter, $requestData['search']['value'] contains search parameter
        $sql.=" where country LIKE '" . $requestData['search']['value'] . "%' ";
        $sql.=" OR city LIKE '" . $requestData['search']['value'] . "%' ";

//            $sql.=" OR first_name LIKE '" . $requestData['search']['value'] . "%' ";
//            $sql.=" OR    last_name LIKE '" . $requestData['search']['value'] . "%' ";
            //$sql.=" OR (link LIKE '" . $requestData['search']['value'] . "%' )";
            //$sql.=" OR (logo LIKE '" . $requestData['search']['value'] . "%' )";
}
$sql.=" ORDER BY " . $columns[$requestData['order'][0]['column']] . "   " . $requestData['order'][0]['dir'] . "  LIMIT " . $requestData['start'] . " ," . $requestData['length'] . "   ";
$query = $this->db->query($sql);
if ($query->result_id->num_rows > 0) {
    return $query->result();
} else {
    return FALSE;
}

}

//function for substance candidates
function getSubstanceCandidatesList($requestData){
$columns = array(
        0 => 'id',
        1 => 'start_date',
        2 => 'until_date',
        3 => 'frequency',
        4 => 'frequency_use_now',
        5 => 'reasons',
        6 => 'significant_info'
        );
    $sql = "select tbl_substance.*,tbl_user.first_name,tbl_user.last_name from tbl_substance  join tbl_user on tbl_user.id=tbl_substance.user_id";
    //$sql.=" WHERE `user_mobile_numbers`.`isPrimary` = '1'";
        if (!empty($requestData['search']['value'])) {   // if there is a search parameter, $requestData['search']['value'] contains search parameter
            $sql.=" where substance_name LIKE '" . $requestData['search']['value'] . "%' ";
            $sql.=" OR frequency LIKE '" . $requestData['search']['value'] . "%' ";
            $sql.=" OR frequency_use_now LIKE '" . $requestData['search']['value'] . "%' ";

//            $sql.=" OR first_name LIKE '" . $requestData['search']['value'] . "%' ";
//            $sql.=" OR    last_name LIKE '" . $requestData['search']['value'] . "%' ";
            //$sql.=" OR (link LIKE '" . $requestData['search']['value'] . "%' )";
            //$sql.=" OR (logo LIKE '" . $requestData['search']['value'] . "%' )";
}
$sql.=" ORDER BY " . $columns[$requestData['order'][0]['column']] . "   " . $requestData['order'][0]['dir'] . "  LIMIT " . $requestData['start'] . " ," . $requestData['length'] . "   ";
$query = $this->db->query($sql);
if ($query->result_id->num_rows > 0) {
    return $query->result();
} else {
    return FALSE;
}


}



}
?>