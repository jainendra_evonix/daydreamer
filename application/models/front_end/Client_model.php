<?php
class Client_model extends CI_Model{

//function for adding client..

	function add_client($info){

     //echo "<pre>";print_r($info);exit;
		$result=$this->db->insert('tbl_client',$info);
     return $this->db->insert_id();

 }
//function for checking login

 function check_user_login($emailaddress, $password){

    $result=$this->db->select('*')
    ->from('tbl_client')
    ->where('clnt_email',$emailaddress)
    ->where('clnt_password',$password)
    ->where('flag','1')
    ->get()->row();
    return $result;



}

    //function for inserting client online data.
function insert_online_data($insert_client_online_data){
    $result=$this->db->insert('tbl_client_profile',$insert_client_online_data);
    return $result;


}

    //function for activating client
function activateClientLogin($data,$token){
    $result=$this->db->where('id',$token)->update('tbl_client',$data);
    return $result;

}

    //function for checking email
function check_client_forgot_password($emailaddress){
 $result=$this->db->select('*')
 ->from('tbl_client')
 ->where('clnt_email',$emailaddress)
 ->get()->row();
 return $result;

}

    //function for updating keystring

function update_keystring($emailaddress,$randomkeystring){
    date_default_timezone_set('UTC');
          //date_default_timezone_set("Asia/Kolkata");
    $newkeystring = array(
        'keystring' => $randomkeystring,
        'isused' => 0,
        'updatedtimestamp' => date("Y-m-d H:i:s")
        );
    $result=$this->db->where('clnt_email',$emailaddress)->update('tbl_client',$newkeystring);
    return $result;


}

   //function for checking random key
function check_randomkey_string($randomkeystring){
    $result=$this->db->select('*')
    ->from('tbl_client')
    ->where('keystring',$randomkeystring)
    ->get()->row();
    return $result;

} 

   //function for reseting password
function update_password($emailaddress,$npassword) {
    $newpassword = array(
        'clnt_password' => md5($npassword),
        'isused' => 1
        );
    $this->db->where('clnt_email', $emailaddress);
    $this->db->update('tbl_client', $newpassword);
        //print_r($this->db->last_query()); exit;
    return TRUE;

}

//function for checking email
function checkEmailId($emailid){

    $result=$this->db->select('*')
    ->from('tbl_client')
    ->where('clnt_email',$emailid)
    ->get()->row();
    return $result;
}

//funtion for check online form data
function checkOnlineFormData($id){
$result=$this->db->select('*')
                 ->from('tbl_client_profile')
                 ->where('client_id',$id)
                 ->get()->row();
                 return $result;

}

}

?>