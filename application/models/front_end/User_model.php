<?php 
defined('BASEPATH') OR exit('No direct script access allowed');

/**
* 
*/
class User_model extends CI_Model
{
	
	function __construct()
    {
        // Call the Model constructor
        parent::__construct();

       $this->load->database();
    }


/*========FUNCTION FOR LOGIN=============*/

 public function check_user_login($emailaddress, $password) {

    $matchpass = md5($password);

 	//echo $emailaddress."  ".$password; exit;
        $query = $this->db->query("SELECT * FROM tbl_user WHERE   flag =1 and   user_email='$emailaddress' and 
        user_password='".$matchpass."'"); 
        //echo $query->num_rows(); exit;
        if ($query->num_rows() > 0) {
            return $query->row();
        } else {
            return false;
        }
    }


/*========== CHECK DUPLICATE EMAIL ===========*/


public function checkEmail($email)
{

  $chkEmail =  $this->db->select('*')
                        ->from('tbl_user')
                        ->where('user_email',$email)
                        ->get()->row();

                        return $chkEmail;

}





/*========== FUNCTION FOR SIGNUP================*/

public function sigUp($info)
{

   $response = $this->db->insert('tbl_user',$info);

   return $this->db->insert_id();

}
 
 /*========= FUNCTION FOR ACTIVATE LOGIN ========*/

 public function activateLogin($data,$token)

 {

     $status =  $this->db->where('id',$token)
                       ->update('tbl_user',$data);

                       return $status;
 }

/*=========== FUNCTION FOR GET ALL JOB TYPE =============*/

      public function  getAllJobType()
      {

         $job_type = $this->db->select('*')
                              ->from('tbl_mst_job_type')
                              ->get()->result();
                    
                   // echo "<pre>"; print_r($job_type); exit;
                              return $job_type;




      }

      /*============= FUNCTION FOR GET ALL COUNTRIES ============*/
    public function getAllContries()
    {


   $countries = $this->db->select('*')
                         ->from('tbl_mst_countries')
                         ->get()->result();

                         return $countries;



    }

    /*================FUNCTION FOR GET ALL ETHNIC ORIGIN ===============*/

    public function getAllEthnicOrigin(){


            $ethnicOrigin = $this->db->select('*')
                                     ->from('tbl_mst_ethnic_origin')
                                     ->get()->result();

                 return $ethnicOrigin; 


    }


    /*============= FUNCRION FOR SAVE USER PERSONAL INFO =================*/

    public function saveUserPersonalInfo($infoArray)
    {


      $lastId = $this->db->insert('tbl_user_personal_info',$infoArray);
            
            return $this->db->insert_id();




    }


    /*================= FUNCTION FOR SAVE USER JOB TYPES ==============*/


    public function saveUserJobType($data1)
    {
          
        $saveJT =  $this->db->insert('tbl_user_job_type',$data1);
           
           return $saveJT;
 


    }

    /*================ FUNCTION FOR GET USER PROFILE DATA ================*/


   
}

?>