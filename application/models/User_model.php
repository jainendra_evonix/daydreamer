<?php 
ob_start();

defined('BASEPATH') OR exit('No direct script access allowed');

/**
* 
*/
class User_model extends CI_Model
{
	
	function __construct()
  {
        // Call the Model constructor
    parent::__construct();

    $this->load->database();
  }


  /*========FUNCTION FOR LOGIN=============*/

  public function check_user_login($emailaddress, $password) {

    $matchpass = md5($password);

 	//echo $emailaddress."  ".$password; exit;
    $query = $this->db->query("SELECT * FROM tbl_user WHERE   flag =1 and   user_email='$emailaddress' and 
      user_password='".$matchpass."'"); 
        //echo $query->num_rows(); exit;
    if ($query->num_rows() > 0) {
      return $query->row();
    } else {
      return false;
    }
  }



  /*===========FUNCTION FOR SAVE USER LOG DETAIL==========*/

  public function saveLoginLogOut($logDetail)
  {

   $saveLog = $this->db->insert('tbl_log',$logDetail);



 }

 /*========== CHECK DUPLICATE EMAIL ===========*/


 public function checkEmail($email)
 {

  $chkEmail =  $this->db->select('*')
  ->from('tbl_user')
  ->where('user_email',$email)
  ->get()->row();

  return $chkEmail;

}

/*========= =============*/

 public function userJobDeselect($user_id,$data)
 {

    $userDeselect = $this->db->where('user_id',$user_id)
                             ->update('tbl_user_job_type',$data);

                             return $userDeselect;

 }


/*=========FUNCTION FOR SAVE USER PROFILE PIC ==========*/

public function saveProfilePic($file_name,$uid)
{

 // echo $file_name."<br>".$uid; exit;;

 $data= $this->db->select('*')
 ->from('tbl_user_personal_info')
 ->where('user_id',$uid)
 ->get()->row();

 $arr = array('user_id'=>$uid,'user_profilePic' =>$file_name );

 if($data !=null )
 {

  $up=$this->db->where('user_id',$uid)
  ->update('tbl_user_personal_info',$arr);

  return $up;
}else{

 $up = $this->db->insert('tbl_user_personal_info',$arr);

 return $up; 

}

}




/*========== FUNCTION FOR SIGNUP================*/

public function sigUp($info)
{

 $response = $this->db->insert('tbl_user',$info);

 return $this->db->insert_id();

}

/*========= FUNCTION FOR ACTIVATE LOGIN ========*/

public function activateLogin($data,$token)

{

 $status =  $this->db->where('activation_token',$token)
 ->update('tbl_user',$data);

 return $status;
}

/*=========== FUNCTION FOR GET ALL JOB TYPE =============*/

public function  getAllJobType()
{

 $job_type = $this->db->select('*')
 ->from('tbl_mst_job_type')
 ->get()->result();

                   // echo "<pre>"; print_r($job_type); exit;
 return $job_type;




}

/*============= FUNCTION FOR GET ALL COUNTRIES ============*/
public function getAllContries()
{


 $countries = $this->db->select('*')
 ->from('tbl_mst_countries')
 ->get()->result();

 return $countries;



}

/*================FUNCTION FOR GET ALL ETHNIC ORIGIN ===============*/

public function getAllEthnicOrigin()
{


  $ethnicOrigin = $this->db->select('*')
  ->from('tbl_mst_ethnic_origin')
  ->get()->result();

  return $ethnicOrigin; 


}


/*============= FUNCRION FOR SAVE USER PERSONAL INFO =================*/

public function saveUserPersonalInfo($infoArray)
{



  $lastId = $this->db->insert('tbl_user_personal_info',$infoArray);

  return $this->db->insert_id();




}


/*================= FUNCTION FOR SAVE USER JOB TYPES ==============*/


public function saveUserJobType($data1)

{

  $saveJT =  $this->db->insert('tbl_user_job_type',$data1);

  return $saveJT;



}

/*================ FUNCTION FOR SAVE COMPLETE USER STEPS ================*/

public function saveCompleteSteps($steps)
{

 $completeSteps = $this->db->insert('tbl_user_profile_progress_info',$steps);

 return $completeSteps;
}
/*============== FUNCTION FOR UPDATE JOB-TYPE ===============*/

public function updateUserSteps($steps,$user_id)
{



 $updateSteps = $this->db->where('u_id',$user_id)
 ->update('tbl_user_profile_progress_info',$steps);


 return $updateSteps;


}


/*============ FUNCTION FOR SAVE USER RESUME DETAIL =========== */

public function saveUserResume($user_id,$profileName)
{
     

        $coverLetters = $this->input->post('covrLetter');
        $resumeTitle = $this->input->post('resumeTitle');
        $resumeUser =  $this->input->post('resumeUser');
        $profileName = $this->input->post('profileName');
       


     $data = array('profile_name' =>$profileName);
       
     $check = $this->db->select('*')
                       ->from('tbl_mst_profile')
                       ->where('profile_name',$profileName)
                       ->get()->row();
                     
        // echo "<pre>";print_r($check); exit;

                       if(empty($check))
                      {
                          $save = $this->db->insert('tbl_mst_profile',$data); 
                          $insert_id = $this->db->insert_id();


                           $resumeInfo = array(
                                    
                                        'user_id' => $user_id,
                                        'resume_title' => $resumeTitle,
                                        'cover_letter' => $user_id.'-'.$coverLetters,
                                        'resume'  =>   $user_id.'-'.$resumeUser,
                                        'profile' => $profileName,
                                        'profile_id' => $insert_id

                                        );


                           if($save)
                          {

                              $saveDetail = $this->db->insert('tbl_user_resumes',$resumeInfo);
           
                                return $saveDetail;
                           }

                      }else{


                                 $insert_id = $check->id;
                                     
                                    // echo $insert_id; exit;

                           $resumeInfo = array(
                                    
                                        'user_id' => $user_id,
                                        'resume_title' => $resumeTitle,
                                        'cover_letter' => $user_id.'-'.$coverLetters,
                                        'resume'  =>   $user_id.'-'.$resumeUser,
                                        'profile' => $profileName,
                                        'profile_id' => $insert_id

                                        );


                              $saveDetail = $this->db->insert('tbl_user_resumes',$resumeInfo);
           
                                return $saveDetail;
                          

                        }
                         // $saveDetail = $this->db->insert('tbl_user_resumes',$resumeInfo);

                             //return  $saveDetail;
          }

                                      /*========== FUNCTION FOR GET USER RESUME INFO =======*/

                                      public function getUserResumeInfo($user_id){

                                       /*$userResumes = $this->db->select('*,tbl_mst_profile.*')
                                                               ->from('tbl_mst_profile')
                                                               ->join('tbl_user_resumes','tbl_user_resumes.profile_id=tbl_mst_profile.id','left')
                                                               ->where('user_id',$user_id)
                                                               ->get()->result();*/
                                                       // echo  $this->db->last_query(); exit;
                                                         //      echo "<pre>"; print_r($userResumes); exit;
                                          
                                   $query = "SELECT *, tbl_mst_profile.profile_name as profilename FROM tbl_mst_profile LEFT JOIN tbl_user_resumes ON tbl_mst_profile.id=tbl_user_resumes.profile_id  where tbl_user_resumes.user_id = '".$user_id."' order by tbl_user_resumes.id desc"; 

                                  //echo $query ; exit;

                                          $userResumes = $this->db->query($query);

                                       return  $userResumes;

                                     }


                                     /*============ GET RESUMES FOR APPLY JOB ============*/
                                      
                                      public function getUserResumeInfoForApply($user_id){




                                       $userResumes = $this->db->select('*')
                                                                ->from('tbl_user_resumes')
                                                                ->where('user_id',$user_id)
                                                                ->get()->result();

                                                               return  $userResumes;
                                                       // echo  $this->db->last_query(); exit;
                                                         //      echo "<pre>"; print_r($userResumes); exit;

                                                                }

                                     /*============= FUNCTION FOR FORGOT PASSWORD =============*/

                                     public function check_admin_forgot_password($emailaddress) {
                                      $query = $this->db->query("SELECT * FROM tbl_user WHERE user_email='$emailaddress'");
        //echo $query->num_rows(); exit;
                                      if ($query->num_rows() > 0) {
                                        return $query->row();
                                      } else {
                                        return false;
                                      }
                                    }

                                    /*=================FUNCTION FOR UPDATE KEY STRING================*/


                                    public function update_keystring($emailaddress,$randomkeystring) {

             //  echo $emailaddress."  ".$randomkeystring; exit;


                                      date_default_timezone_set('UTC');
          //date_default_timezone_set("Asia/Kolkata");
                                      $newkeystring = array(
                                        'keystring' => $randomkeystring,
                                        'isused' => 0,
                                        'updatedtimestamp' => date("Y-m-d H:i:s")
                                        );
                                      $this->db->where('user_email', $emailaddress);
                                      $this->db->update('tbl_user', $newkeystring);
                                      return TRUE;
                                    }



                                    /*=========== FUNCTION FOR CHECK DUPLICATE EMAIL =============*/

                                    public function findEmail($email)
                                    {

                                      $checkEmail = $this->db->select('*')
                                                             ->from('tbl_user')
                                                             ->where('user_email',$email)
                                                             ->get()->row();
                                      return $checkEmail;

                                    }



                                    /*============FUNCTION FOR UPDATE PASSWORD ===========*/


                                    public function update_password($emailaddress,$npassword) {
                                      $newpassword = array(
                                        'user_password' => md5($npassword),
                                        'isused' => 1
                                        );
                                      $this->db->where('user_email', $emailaddress);
                                      $this->db->update('tbl_user', $newpassword);
        //print_r($this->db->last_query()); exit;
                                      return TRUE;
                                    }
                                    /*=============== FUNCTION FOR MATCH KEY STRING ================*/


                                    public function check_randomkey_string($randomkeystring) 
                                    {

                                      $query = $this->db->query("SELECT * FROM tbl_user WHERE keystring='$randomkeystring'");
        //echo $query->num_rows(); exit;
                                      if ($query->num_rows() > 0) {
                                        return $query->row();
                                      } else {
                                        return false;
                                      }
                                    }


                                    /*==================== FUNCTION FOR GET USER COMPLETE STEPS ===============*/

                                    public function getCompleteSteps($user_id)
                                    {

                                     $status  = $this->db->select('*')
                                     ->from('tbl_user_profile_progress_info')
                                     ->where('p_id',$user_id)
                                     ->get()->row();

                                     return $status;

                                   }
                                   /*============= FUNCTION FOR GET FILLED USER PROFILE DATA ===========*/

  /* public function getUserProfileData($user_id)
   {
 
 
    $this->db->select('tbl_user_personal_info.*');
    $this->db->select('tbl_mst_job_type.job_type');
    $this->db->select('tbl_user_profile_progress_info.profile,tbl_user_profile_progress_info.education,tbl_user_profile_progress_info.pre_employment,tbl_user_profile_progress_info.employment,tbl_user_profile_progress_info.references,tbl_user_profile_progress_info.additional_info,tbl_user_profile_progress_info.additional_info,tbl_user_profile_progress_info.miscellaneous,tbl_user_profile_progress_info.social_media');
    $this->db->from('tbl_user_personal_info, tbl_user_job_type, tbl_user_profile_progress_info,tbl_mst_job_type');
    $this->db->where('tbl_user_personal_info.user_id', $user_id);
    $where = 'tbl_user_personal_info.id = tbl_user_job_type.pro_tbl_id AND tbl_user_personal_info.id = tbl_user_profile_progress_info.p_id AND tbl_user_job_type.job_type = tbl_mst_job_type.id';
    $this->db->where($where);
  $UserProfileData=  $this->db->get()->row();
// echo   $this->db->last_query(); exit;
  //echo "<pre>"; print_r($UserProfileData); exit;
   return $UserProfileData;




 }*/
 /*============= FUNCTION FOR GET FILLED USER PROFILE DATA ===========*/

 public function getUserProfileData($user_id)
 {


  $UserProfileData = $this->db->select('tbl_user_personal_info.*');
                     $this->db->select('tbl_mst_countries.country_name');
                     $this->db->select('tbl_mst_ethnic_origin.ethnic_origin');
                     $this->db->select('tbl_user_profile_progress_info.profile,tbl_user_profile_progress_info.education,tbl_user_profile_progress_info.pre_employment,tbl_user_profile_progress_info.employment,tbl_user_profile_progress_info.resume,tbl_user_profile_progress_info.references,tbl_user_profile_progress_info.additional_info,tbl_user_profile_progress_info.additional_info,tbl_user_profile_progress_info.miscellaneous,tbl_user_profile_progress_info.social_media');
                    $this->db->select('tbl_mst_relationship_status.rel_status');
                    $this->db->from('tbl_user_personal_info,tbl_mst_countries,tbl_mst_ethnic_origin,tbl_user_profile_progress_info,tbl_mst_relationship_status');
                    $this->db->where('tbl_user_personal_info.user_id',$user_id);
                    $where = 'tbl_user_personal_info.user_id = tbl_user_profile_progress_info.u_id AND tbl_user_personal_info.user_ethinic_origin = tbl_mst_ethnic_origin.id AND tbl_user_personal_info.user_nationality = tbl_mst_countries.country_code AND tbl_user_personal_info.user_rel_status = tbl_mst_relationship_status.id';
                    $this->db->where($where);
  $UserProfileData=  $this->db->get()->row();
       // print_r($this->db->last_query()); exit;
                 //  echo "<pre>"; print_r($UserProfileData); exit;
  return $UserProfileData;


}


/*============FUNCTION FOR GET PROFILE jOB TYPE ======*/
 
 public function getProfileJobType()
 {
     $profileJobType = $this->db->select('*')
                                ->from('tbl_mst_profile_job_type')
                                ->get()->result();

                                return $profileJobType;


 }



 /*============FUNCTION FOR GET ContractType TYPE ======*/
 
 public function getContractType()
 {
     $contractType = $this->db->select('*')
                                ->from('tbl_mst_contract_type')
                                ->get()->result();

                                return $contractType;


 }
   

   /*============FUNCTION FOR GET HOUR TYPE ======*/
 
 public function getHour()
 {
     $hourType = $this->db->select('*')
                                ->from('tbl_mst_hour')
                                ->get()->result();

                                return $hourType;


 }


/*============== FUNCTION FOR GET FEED USER JOB TYPE ==============*/

public function getUserFeedJobType($user_id)
{


                   /* $this->db->select('tbl_user_job_type.job_type');
                    $this->db->select('tbl_mst_job_type.job_type');
                    $this->db->from('tbl_user_job_type,tbl_mst_job_type') ; 
                    $this->db->where('tbl_user_job_type.user_id',$user_id);
                    $where="tbl_user_job_type.job_type = tbl_mst_job_type.id";
                    $this->db->where($where);
                    $user_jobType =  $this->db->get()->result();*/

                    $user_jobType = $this->db->select('job_type')
                                            ->from('tbl_user_job_type')
                                            ->where('user_id',$user_id)
                                            ->get()->result();

                           // echo "<pre>"; print_r($user_jobType); exit;
                   // $jobs=explode(",",$user_jobType->job_type);
                    $jobs = array(); 


                    foreach ($user_jobType as $key ) {


                     $jobs[] = $key->job_type;



                   }    

                   /*echo "".$user_jobType->job_type;
                   echo "<br>";
                   echo "<pre>";
                  print_r (explode(",",$user_jobType->job_type)); */

                 // echo "<pre>"; print_r($jobs); exit;

                 //  return json_encode($jobs); 

                             return $jobs;             
                 }
                 /*===================== FUCTION FOR CHECK USER PROFILE STATUS ==============*/

                 public function checkUserProfileStatus($user_id)
                 {

                   $userProfileStatus = $this->db->select('*')
                                                 ->from('tbl_user_personal_info') 
                                                 ->where('user_id',$user_id)
                                                 ->get()->row();

                              return $userProfileStatus;


                 }


                 /*================ FUNCTION FOR UPDATE USER PERSONAL INFO ============*/

                 public function updateUserPinfo($infoArray,$user_id)
                 {

    //echo "<pre>";  print_r($infoArray); exit;

                  $saveJT =   $this->db->where('user_id',$user_id)
                                      ->update('tbl_user_personal_info',$infoArray);

                  $lastId  =  $this->db->select('id')
                                       ->from('tbl_user_personal_info')
                                       ->where('user_id',$user_id)
                                       ->get()->row();                    

                  /*echo "<pre>"; print_r($lastId); exit;*/
                       return $lastId;

                }


                /*========== FUNCTION FOR CHECK PASSWORD TO CHANGE PASSWORD======*/

                public function checkCurrentPswrd($currntPswd,$user_id)
                {

                  $check = $this->db->select('*')
                                    ->from('tbl_user')
                                    ->where('id',$user_id)
                                    ->where('user_password',$currntPswd)
                                    ->get()->row();

                       // echo "<pre>"; print_r($check); exit;

                  return $check;

                }

                /*================ FUNCTION FOR UPDATE USER NEW PASSWORD===========*/

                public function updateUserPassword($user_id)
                {

                  $cpass = md5($this->input->post('cnfrmPass')); 

                  $newPass = array(
                   'user_password' => $cpass
                   );

  // echo 'asd'."<pre>"; print_r($newPass); exit;

                  $updatePass = $this->db->where('id',$user_id)
                                        ->update('tbl_user',$newPass);


                  return $updatePass;


                }


                /*=============FUNCTION FOR UDATE USER JOB TYPE ============*/


                public function updateUserJobType($user_id,$p_id,$date)
                {

                  
                //  print_r($_POST['jobtype']); exit;


                  $deleteJobs = $this->db->where('user_id',$user_id)
                                         ->delete('tbl_user_job_type');


                  foreach ($_POST['jobtype'] as $selected)
                  { 

                    $data1 = array(

                     'user_id'  => $user_id,                           
                     'pro_tbl_id'    => $p_id,
                     'job_type' => $selected,
                     'updated_date' => $date
                     );

           // echo "<pre>"; print_r($data1);
                    $updateInfo = $this->db->insert('tbl_user_job_type',$data1);

                    }//exit;

                    return $updateInfo;
                  }




                  /*=============== FUNCTION FOR GET MONTHS =============*/

                  public function getMonths()
                  {


                   $months =  $this->db->select('*')
                   ->from('tbl_mst_month')
                   ->get()->result();

                   return $months;

                 }



                 /*================= FUNCTION FOR GET YEARS ============*/

                 public function getYears()
                 {


                  $years =  $this->db->select('*')
                  ->from('tbl_mst_year')
                  ->get()->result();

                  return $years;



                }


                /*==============FUNCTION FOR GET DAYS FROM MST TBL===========*/

                public function getdays()
                {

                 $days = $this->db->select('*')
                 ->from('tbl_mst_day')
                 ->get()->result();

                 return $days;

               }



               /*=============== FUNCTION FOR GET QUALIFICATIONS TYPES ===============*/

               public function getQualificationTypes()
               {

                $qualification = $this->db->select('*')
                ->from('tbl_mst_qualification')
                ->get()->result();


                return $qualification;


              }



              /*================ FUNCTION FOR GET GRADE FROM MST TABLE ============= */

              public function getGrade()
              {

               $grade = $this->db->select('*')
               ->from('tbl_mst_grade')
               ->get()->result();

               return $grade;

             }

             /*================ FUNCTION FOR GET achieved FROM MST TABLE ============= */

             public function getAchieved(){

               $achieved = $this->db->select('*')
               ->from('tbl_mst_achieved')
               ->get()->result();

               return $achieved;

             }

             /*================ FUNCTION FOR GET RELATIONSHIP STATUS FROM MST TABLE ================*/
             public function getRelationship()
             {

               $relationshipStatus = $this->db->select('*')
               ->from('tbl_mst_relationship_status') 
               ->get()->result(); 


               return $relationshipStatus;

             }




             /*================= FUNCTION FOR SAVE USER EDUCATION INFO ==================*/

             public function saveUserEducationInfo($educationArray)

             {

              $saveEducationInfo = $this->db->insert('tbl_user_education_info',$educationArray);

              return $saveEducationInfo;




            }
            /*================= FUNCTION FOR USER EDUCATION DETAILS ===================*/


            public function getUserEducationDetails($user_id)
            {
   //echo $user_id; exit;

//$query = "select tu.id,tu.institute_name, tu.studying,tu.course ,tu.grade,tu.qualification_type,tu.from_year,tu.from_month,tu.to_month,tu.from_year,tu.achieved,tu.to_year,tu.qualification_type  ,tblm.month_name as fromMonth1,mth.month_name as toMonth1,tblq.qualification,tblg.grade as grd, ta.achieved as achvd,tpi.education as edu from tbl_user_education_info tu  inner join tbl_mst_month tblm on tu.from_month=tblm.id inner join tbl_mst_month mth on tu.to_month=mth.id inner join tbl_mst_qualification tblq on tu.qualification_type=tblq.id inner join tbl_mst_grade tblg on tu.grade=tblg.id inner join tbl_mst_achieved ta on tu.achieved=ta.id inner join tbl_user_profile_progress_info tpi on tu.user_id=tpi.u_id where tu.user_id= ORDER BY tu.id DESC ";

              $query = "select tu.id,tu.institute_name, tu.studying,tu.course ,tu.grade,tu.qualification_type,tu.from_year,tu.from_month,tu.to_month,tu.from_year,tu.achieved,tu.to_year,tu.qualification_type,tblm.month_name as fromMonth1,mth.month_name as toMonth1,tu.qualification_type,tblq.qualification, ta.achieved as achvd from tbl_user_education_info tu  inner join tbl_mst_month tblm on tu.from_month=tblm.id  left join tbl_mst_month mth on tu.to_month=mth.id inner join tbl_mst_qualification tblq on tu.qualification_type=tblq.id inner join tbl_mst_achieved ta on tu.achieved=ta.id  where tu.user_id='".$user_id."' ORDER BY tu.id DESC ";

              /* "select tu.id,tu.institute_name, tu.studying,ty.year as fromyear1,tu.course ,tu.grade,tu.qualification_type,tu.from_year,tu.from_month,tu.to_month,tu.from_year,tu.achieved,tu.to_year,tu.qualification_type ,tyt.year as toyear1 ,tblm.month_name as fromMonth1,mth.month_name as toMonth1,tblq.qualification,tblg.grade as grd, ta.achieved as achvd,tpi.education as edu from tbl_user_education_info tu inner join tbl_mst_year ty on tu.from_year=ty.id inner join tbl_mst_year tyt on tu.to_year=tyt.id inner join tbl_mst_month tblm on tu.from_month=tblm.id inner join tbl_mst_month mth on tu.to_month=mth.id inner join tbl_mst_qualification tblq on tu.qualification_type=tblq.id inner join tbl_mst_grade tblg on tu.grade=tblg.id inner join tbl_mst_achieved ta on tu.achieved=ta.id inner join tbl_user_profile_progress_info tpi on tu.user_id=tpi.u_id where tu.user_id='".$user_id."' ORDER BY tu.id DESC "*/
              $result= $this->db->query($query);

              $userEducationDetails= $result->result();

    // echo "<pre>"; print_r($userEducationDetails); exit;
              return  $userEducationDetails;


            }

            /*=============== FUNCTION FOR CHECK USER EDUCATION STATUS=============== */

            public function checkEducationStatus($user_id)
            {
   //echo $user_id;

              $educationStatus = $this->db->select('*')
              ->from('tbl_user_education_info')
              ->where('user_id',$user_id)
              ->get()->row();

         // echo ($this->db->last_query()); exit;
              return $educationStatus;


            }

            /*=========== FUNCTION FOR GET EDU ================*/

            public function getEdu($user_id){

  // echo $user_id; exit;
             $userEdu = $this->db->select('*')
             ->from('tbl_user_education_info')
             ->where('user_id',$user_id)
             ->get()->row();

                     //  echo ($this->db->last_query()); exit;
//print_r($userEdu); exit;
             return $userEdu; 


           }
           /*============ FUNCTION FOR UPDATE USER EDUCATION INFO ============*/


           public function updateUserEducationInfo($educationArray,$id)
           {

  //echo "<pre>"; print_r($educationArray); exit;

            $updateEducationInfo = $this->db->where('id',$id)
            ->update('tbl_user_education_info',$educationArray);

            return $updateEducationInfo;


          }



          /*====================== FUNCTION FOR GET USER EDUCATION INFO FOR EDIT ===============*/

          public function userEducationInfo($id)
          {



           $userEductionDetail = $this->db->select('*')
           ->from('tbl_user_education_info')
           ->where('id',$id)
           ->get()->row();

  // echo "<pre>"; print_r($userEductionDetail); exit;

           return $userEductionDetail;
         }   


         /*============== FUNCTION FOR DELETE USER EDUCATION =================*/

         public function deleteUserEducation($id)
         {

          $deleteEdu = $this->db->where('id',$id)
          ->delete('tbl_user_education_info');

          return $deleteEdu;

        }

        /*============== FUNCTION FOR DELETE USER RESUME =================*/

        public function deleteResume($id)
        {

          $deleteEdu = $this->db->where('id',$id)
          ->delete('tbl_user_resumes');

          return $deleteEdu;

        }

        /*============== FUNCTION FOR GET ORGANISATIONS FROM MST TABLE ===============*/
        public function getOrganisations()
        {

         $organisations = $this->db->select('*')
         ->from('tbl_mst_organisations')
         ->get()->result();


         return $organisations;


       }


       /*=========== FUNCTION FOR GET SAVE USER PRE EMPLOYMENT INFO ================*/

       public function saveUserPreEmploymentInfo($preEmploymentArray)

       {
      
      //echo "<pre>"; print_r($preEmploymentArray); exit;

        $this->db->insert('tbl_user_pre_employment_info',$preEmploymentArray);
      
      //  echo $this->db->last_query(); exit;

        $lastId = $this->db->insert_id();
            //echo $lastId; exit;
        return $lastId;           



      }




      /*====================== FUNCTION FOR SAVE SELECTED ORGANISATION ====================*/


      public function saveSelectedOrg($orgArray)
      {


        $saveUserOrg =   $this->db->insert('tbl_user_organisation',$orgArray);

        return $saveUserOrg;
      }


      /*=============FUNCTION FOR GET USER PRE-EMPLOYMENT DETAIL ===============*/


      public function getUserPreEmploymentInfo($user_id)
      {

        $query="select tu.id,tu.employer_name,tu.designation,tu.job_profile,tblm.month_name as fromMonth,mth.month_name as toMonth ,tu.from_year,tu.from_month,tu.to_month,tu.current_emplymnt_status,tu.to_year,tu.present_income,tu.desired_income,tu.currently_actively_applying_status,tu.currently_actively_applying_desc,tu.undertaken_voluntary_charity_work,tu.interested_in_volunteering_assisting_charities_status,tu.interested_in_volunteering_assisting_charities_desc,tu.prsnt_income_freq,tu.de_income_freq from tbl_user_pre_employment_info tu  inner join tbl_mst_month tblm on tu.from_month=tblm.id inner join tbl_mst_month mth on tu.to_month=mth.id  where tu.user_id='".$user_id."' ORDER BY tu.id DESC";



/*"select tu.id,tu.employer_name,tu.designation,tu.job_profile,tu.resume,tu.employer_reference,tu.from_year,tu.from_month,tu.to_month,tu.current_emplymnt_status,tu.to_year,tu.present_income,tu.desired_income,tu.currently_actively_applying_status,tu.currently_actively_applying_desc,tu.undertaken_voluntary_charity_work,tu.interested_in_volunteering_assisting_charities_status,tu.interested_in_volunteering_assisting_charities_desc,tblm.month_name as fromMonth1,mth.month_name as toMonth1  from tbl_user_pre_employment_info tu  inner join tbl_mst_month tblm on tu.from_month=tblm.id inner join tbl_mst_month mth on tu.to_month=mth.id  where tu.user_id=1 ORDER BY tu.id DESC "



"select tu.id,tu.employer_name,tu.designation,tu.job_profile,tu.resume,tu.employer_reference,tu.from_year,tu.from_month,tu.to_month,tu.current_emplymnt_status,tu.to_year,tu.present_income,tu.desired_income,tu.currently_actively_applying_status,tu.currently_actively_applying_desc,tu.undertaken_voluntary_charity_work,tu.interested_in_volunteering_assisting_charities_status,tu.interested_in_volunteering_assisting_charities_desc ,tu.from_month as FromMonth,tu.to_month as ToMonth  from tbl_user_pre_employment_info tu  inner join tbl_mst_month tblm on tu.from_month=tblm.id  inner join tbl_mst_month mth on tu.to_month=mth.id  where tu.user_id=1 ORDER BY tu.id DESC "

*/

  //echo $query; exit;
$result= $this->db->query($query);

$PreEmploymentInfo= $result->result();
     //echo "<pre>"; print_r($PreEmploymentInfo); exit;

   /* $PreEmploymentInfo = $this->db->select('*')
                                  ->from('tbl_user_pre_employment_info') 
                                  ->where('user_id',$user_id)
                                  ->order_by('id','desc')
                                  ->get()->result();
                                  */

                                  return $PreEmploymentInfo;  
                                }

                                /*================== FUNCTION  FOR GET SAVED USER  ORGANISATION =============*/
                                public function getUserOrganisation($id)
                                {

                                  $userOrganisation = $this->db->select('organisation')
                                  ->from('tbl_user_organisation')
                                  ->where('pre_id',$id)
                                  ->get()->result();

                                  $org = array();

                                  foreach ($userOrganisation as $key) { 

                                   $org[]=$key->organisation;
                                 }


                     //json_encode($org); exit;

                                 //  echo "<pre>"; print_r($org); exit;
                                 return $org;  


                               }

                               /*=================== FUNCTION FOR UPDATE USER PRE-EMPLOYMENT INFO ==============*/

                               public function updateUserPreEmploymentInfo($editUserPreEmployementArr,$id)
                               {


                                 $updatePreEmplymntInfo  = $this->db->where('id',$id)
                                 ->update('tbl_user_pre_employment_info',$editUserPreEmployementArr);


                                 return $updatePreEmplymntInfo;



                               }


                               /*===================== FUNCTION FOR UPDATE ORGANISATION =================*/

                               public function updateSelectedOrg($user_id,$id)
                               {



                                 $delOrg = $this->db->where('pre_id',$id)
                                 ->delete('tbl_user_organisation');



    //  $save = $this->db->insert('tbl_user_organisation',$orgArray);

                                 foreach ($_POST['eorganisations'] as $selected) {


                                   $orgArray = array(

                                    'u_id' => $user_id,
                                    'organisation' => $selected,
                                    'pre_id' => $id
                                    );

                                   $save = $this->db->insert('tbl_user_organisation',$orgArray);
                                 }

                                 return $save;

                               }


                               /*================= FUNCTION FOR GET TRAINIGS NAME ==============*/

                               public function getTrainings()
                               {

                                $trainings = $this->db->select('*')
                                ->from('tbl_mst_training')
                                ->get()->result();

                                return $trainings;



                              }


                              /*=============== FUNCTION FOR CHECK USER EMPLOYMENT DETAIL ===============*/


                              public function checkUserEmployment($user_id)
                              {


                               $status = $this->db->select('*')
                               ->from('tbl_user_employment_info')
                               ->where('user_id',$user_id)
                               ->get()->row();


                               return $status;


                             }

                             /*============== FUNCTION FOR SAVE USER EMPLOYMENT ============*/
                             public function saveUserEmploymentInfo($employmentArray)
                             {

                              $saveUserEmntInfo = $this->db->insert('tbl_user_employment_info',$employmentArray); 

                              return $saveUserEmntInfo;

                            }

                            /*================= FUNCTION FOR UPDATE USER EMPLOYMENT DETAIL =============*/

                            public function updateUserEmployment($user_id,$employmentArray)
                            {

                              $updateUserEmployment = $this->db->where('user_id',$user_id)
                              ->update('tbl_user_employment_info',$employmentArray);

                              return $updateUserEmployment;


                            }

                            /*============= FUNCTION FOR GET USER EMPLOYMENT DETAIL ===========*/

                            public function userEmploymentInfo($user_id)
                            {

                                     /* $userEmploymentDetail = $this->db->select('*')
                                      ->from('tbl_user_employment_info')
                                      ->where('user_id')*/

                                      $query = "select tu.id,tu.position_held, ty.year as fromyear1,tu.company_name ,tu.location_city,tu.location_country,tu.from_year,tu.from_month,tu.to_month,tu.from_year,tu.purpose_of_role,tu.to_year,tu.rspnsibilities_achvmnts,tu.internal_training ,tyt.year as toyear1 ,tblm.month_name as fromMonth1,mth.month_name as toMonth1 from tbl_user_employment_info tu inner join tbl_mst_year ty on tu.from_year=ty.id inner join tbl_mst_year tyt on tu.to_year=tyt.id inner join tbl_mst_month tblm on tu.from_month=tblm.id inner join tbl_mst_month mth on tu.to_month=mth.id  where tu.user_id='".$user_id."' ";
                                      $result= $this->db->query($query);

                                      $userEmploymentDetails= $result->row();

                                    //  echo "<pre>"; print_r($userEmploymentDetails); exit;

                                      return $userEmploymentDetails; 

                                    }


                                    /*================ FUNCTION FOR GET USER REGRENCES FROM MST TBL ================*/

                                    public function getUserRefeneces()
                                    {


                                     $userRefences = $this->db->select('*')
                                     ->from('tbl_mst_references')
                                     ->get()->result();

                                     return $userRefences;


                                   }
                                   /*================FUNCTION FOR SAVE USER REFERENCES==================*/
                                   public function saveUserReferences($referenceArray)
                                   {

                                    $userReferences = $this->db->insert('tbl_user_references',$referenceArray);

                                    return $userReferences;
                                  }

                                  /*=============== FUNCTION FOR GET SAVED USER REFERENCES ==========*/

                                  public function getSavedUserReferences($user_id)
                                  {

                                   $savedUserReferences = $this->db->select('*')
                                   ->from('tbl_user_references')
                                   ->where('user_id',$user_id)
                                   ->get()->row();

                                   return $savedUserReferences;

                                 }



                                 /*==========FUNCTION FOR CHECK USER REFERENCES ============ */

                                 public function checkUserReferences($user_id)
                                 {

                                   $check = $this->db->select('*')
                                   ->from('tbl_user_references')  
                                   ->where('user_id',$user_id)
                                   ->get()->row();


                                   return $check;



                                 }

                                 /*=========== FUNCTION  FOR UPDATE USER REFERENCES ==========*/

                                 public function updateUserReferences($referenceArray,$user_id)
                                 {

                                   $deleteUserReferences =  $this->db->where('user_id',$user_id)
                                   ->delete('tbl_user_references');



                                   $updateUserReferences = $this->db->insert('tbl_user_references',$referenceArray);   

                                   return   $updateUserReferences;                     


                                 }


                                 /*============= FUNCTION FOR GET USER COMPLETE STEPS =================*/

                                 public function getUserCompSteps($user_id)
                                 {

                                   $userProfileSteps =  $this->db->select('*')
                                                                 ->from('tbl_user_profile_progress_info')
                                                                 ->where('u_id',$user_id)
                                                                 ->get()->row();

                                                  return $userProfileSteps;



                                 }




                                 /*=================== FUNCTION FOR GET SAVED USER ADDITIONAL INFO ===========*/
                                 public function getSavedUserAdditionalInfo($user_id)
                                 {

                                  $savedUserAdditionalInfo = $this->db->select('*')
                                  ->from('tbl_user_additional_info')
                                  ->where('user_id',$user_id)
                                  ->get()->row();
                                  return $savedUserAdditionalInfo;

                                }
                                /*================== FUNCTION FOR SAVE USER ADDITINAL INFORMATION ==================*/


                                public function saveuserAdditionalInfo($userAddArray)
                                {

                                 $userAdditionalInfo = $this->db->insert('tbl_user_additional_info',$userAddArray);

                                 return $userAdditionalInfo;




                               }


                               /*==================== FUNCTION FOR UPDATE USER ADDITIONAL INFORMATION ==============*/

                               public function updateUserAdditionalInfo($userAddArray,$user_id)
                               {

                                 $updateAdditional = $this->db->where('user_id',$user_id)
                                 ->update('tbl_user_additional_info',$userAddArray);


                                 return $updateAdditional;



                               }


                               /*==================FUNCTION FOR GET SAVED MISCELLANEOUS USER INFO ============*/

                               public function getSaveduserMiscellaneous($user_id)
                               {

                                 $userMiscellaneousInfo = $this->db->select('*')
                                 ->from('tbl_user_miscellaneous')
                                 ->where('user_id',$user_id)
                                 ->get()->row();


                                 return $userMiscellaneousInfo;




                               }


                               /*=============== FUNCTION FOR SAVE USER MISCELLANEOUS INFORMATION ===========*/

                               public function saveMiscellaneousInfo($miscellaneousArray)
                               {

                                 $miscellaneous = $this->db->insert('tbl_user_miscellaneous',$miscellaneousArray);


                                 return $miscellaneous;


                               }



                               /*=================== FUNCTION FOR UPDATE USER MISCELLANEOUS INFORMATION ================*/


                               public function updateUserMiscellaneous($miscellaneousArray,$user_id)
                               {

                                $updateMiscellaneous = $this->db->where('user_id',$user_id)
                                ->update('tbl_user_miscellaneous',$miscellaneousArray);

                                return $updateMiscellaneous;




                              }



                              /*============= FUNCTION FOR SAVE USER SOCIAL MEDIA ===========*/

                              public function saveUserSocialMedia($saveUserSocial)
                              {


                                $saveUserSocial = $this->db->insert('tbl_user_social_media_info',$saveUserSocial);

                                return $saveUserSocial;


                              }

                              /*============== FUNCTION FOR GET SAVED USER SOCIAL MEDIA LINKS =============*/


                              public function  getUserSavesLink($user_id)
                              {

                                $links = $this->db->select('*')
                                ->from('tbl_user_social_media_info')
                                ->where('user_id',$user_id)
                                ->get()->row();

                               //  echo "<pre>"; print_r($links); exit;
                                return $links;



                              }


                              /*================= FUNCTION FOR UPDATE USER SOCIAL LINKS ===========*/

                              public function updateUserSocialMedia($user_id,$socialmediaArray)
                              {

                                $updateUserLinks = $this->db->where('user_id',$user_id)
                                ->update('tbl_user_social_media_info',$socialmediaArray);


                                return $updateUserLinks;



                              }




                              /*================ FUNCTION FOR GET USER SAVED PRE EMPLOYMENT DETAIL ==============*/


                              public function userPreEmplymentInfo($id)
                              {

         /*$userPreEmployementInfo = $this->db->select('*')
                                            ->from('tbl_user_pre_employment_info')
                                            ->where('id',$id)
                                            ->get()->row();*/




                                           // $query="select tu.id,tu.employer_name,tu.designation,tu.job_profile,tu.employer_reference,tblm.month_name as fromMonth,mth.month_name as toMonth ,tu.from_year,tu.from_month,tu.to_month,tu.current_emplymnt_status,tu.to_year,tu.present_income,tu.desired_income,tu.currently_actively_applying_status,tu.currently_actively_applying_desc,tu.undertaken_voluntary_charity_work,tu.interested_in_volunteering_assisting_charities_status,tu.interested_in_volunteering_assisting_charities_desc from tbl_user_pre_employment_info tu  inner join tbl_mst_month tblm on tu.from_month=tblm.id inner join tbl_mst_month mth on tu.to_month=mth.id  where tu.id='".$id."' ORDER BY tu.id DESC";
                          $query = "select tu.id,tu.employer_name,tu.designation,tu.job_profile,tblm.month_name as
 fromMonth,mth.month_name as toMonth ,tu.from_year,tu.from_month,tu.to_month,tu.current_emplymnt_status
,tu.to_year,tu.present_income,tu.prsnt_income_freq,tu.desired_income,tu.de_income_freq,tu.internal_training,tu.national_insurance_no,tu.wrkexp,tu.currently_actively_applying_status,tu.currently_actively_applying_desc
,tu.undertaken_voluntary_charity_work,tu.interested_in_volunteering_assisting_charities_status,tu.interested_in_volunteering_assisting_charities_desc
 from tbl_user_pre_employment_info tu  inner join tbl_mst_month tblm on tu.from_month=tblm.id inner join
 tbl_mst_month mth on tu.to_month=mth.id  where tu.id='".$id."'";
                                         // echo $query; exit;

                                            $result= $this->db->query($query);

                                            $userPreEmployementInfo= $result->row();


    // echo "<pre>"; print_r($userPreEmployementInfo); exit;
                                            return $userPreEmployementInfo;



                                          }



                                          /*============== FUNCTION FOR DELETE USER PRE-EMPLOYMENT INFO============*/


                                          public function delUserPreEmploymentInfo($id)
                                          {

                                           $delTbl = $this->db->where('id',$id)
                                           ->delete('tbl_user_pre_employment_info');

                                           if($delTbl){


                                             $delOrg = $this->db->where('pre_id',$id)
                                             ->delete('tbl_user_organisation');

                                             return $delOrg;


                                           }


                                         }


                                         /*============ FUNCTIONS FOR JOB SEARCH WITH KEYWORD OR LOCATION  ===============*/

                                         public function searchJobs($keyword,$location)
                                         {


  // $searchedJobs = $this->db->select('*')


                                          // $query ="SELECT a.*,b.*,c.*,d.* FROM tbl_job_create a LEFT JOIN tbl_company_logo b ON b.client_id = a.client_id LEFT JOIN tbl_user_applied_job c ON a.j_id=c.job_id LEFT JOIN tbl_client_profile d ON a.client_id=d.client_id WHERE  a.job_approve=0 AND (a.job_title LIKE '%$keyword%' OR a.job_desc LIKE '%$keyword%' OR a.city LIKE '%$location%' OR a.state LIKE '%$location%') ORDER BY a.j_id DESC ";
                                            /* select a.*,b.*,d.*,e.subclient_name from tbl_job_create a inner join tbl_company_profile b left join tbl_user_applied_job d on a.j_id=d.job_id left join tbl_subClient_create e on a.parent_client=e.id  where  a.job_approve=0 and (a.client_id=b.client_id or a.client_id !=b.client_id and a.job_title LIKE '%developer%' OR a.job_desc LIKE '%%' OR a.city LIKE '%pune%' OR a.state LIKE '%maharastra%')  group by a.j_id*/

                                           //  $query = "select a.*,b.*,d.*,e.subclient_name,f.saved,f.user_id as u_id,g.saved as jobAlrt,g.user_id as jau_id from tbl_job_create a inner join tbl_company_profile b left join tbl_user_applied_job d on a.j_id=d.job_id left join tbl_subClient_create e on a.parent_client=e.id left join tbl_user_saved_jobs f on a.j_id=f.job_id left join tbl_user_job_alert g on a.j_id =g.job_id LEFT JOIN tbl_company_logo f ON a.client_id = f.client_id where  a.job_approve=0 and (a.client_id=b.client_id or a.client_id !=b.client_id and job_title LIKE '%$keyword%' OR job_desc LIKE '%$keyword%' OR a.city LIKE '%$location%' OR state LIKE '%$location%')  group by a.j_id order by a.j_id DESC";

                                         $query = "select a.*,b.*,d.*,e.subclient_name,f.saved,f.user_id as u_id,g.saved as jobAlrt,g.user_id as jau_id,i.company_logo from tbl_job_create a inner join tbl_company_profile b left join tbl_user_applied_job d on a.j_id=d.job_id left join tbl_subClient_create e on a.parent_client=e.id left join tbl_user_saved_jobs f on a.j_id=f.job_id left join tbl_user_job_alert g on a.j_id =g.job_id inner JOIN tbl_company_logo i ON b.client_id = i.client_id where a.job_approve=0 and (a.client_id=b.client_id or a.client_id !=b.client_id and job_title LIKE '%web developer%' OR job_desc LIKE '%web developer%' OR a.city LIKE '%pune%' OR state LIKE '%pune%') group by a.j_id order by a.j_id DESC";


                                          //  echo $query; exit;


                                           $result= $this->db->query($query);

                                           $searchedJobs = $result->result();

                                          //  echo "<pre>"; print_r($searchedJobs); exit;

                                          return $searchedJobs;
                     //    
                                         }


                                         /*=============== FUNCTION FOR JOB SEARCH WITH OUT KEYWORD ===============*/


                                         public function searchJobs2()
                                         {


                                          /*$query =   "SELECT a.*,b.* FROM tbl_job_create a LEFT JOIN tbl_company_logo b ON b.client_id = a.client_id LEFT JOIN tbl_user_applied_job c ON a.j_id=c.job_id WHERE  a.job_approve=1 ";*/

                                         // $query = "SELECT a.*,b.*,c.*,d.* FROM tbl_job_create a LEFT JOIN tbl_company_logo b ON b.client_id = a.client_id LEFT JOIN tbl_user_applied_job c ON a.j_id=c.job_id LEFT JOIN tbl_client_profile d ON a.client_id=d.client_id WHERE  a.job_approve=0";
                                               
                                             //  INNER JOIN Table2 ON (Table1.id = Table2.id AND Table1.sub-id = Table2.sub-id)




                                        //  $query ="SELECT a.*,b.*,c.*,d.*  FROM tbl_job_create a left join tbl_company_logo b ON b.client_id = a.client_id LEFT JOIN tbl_user_applied_job c ON a.j_id=c.job_id inner join tbl_client_profile d ON a.client_id=d.client_id WHERE a.job_approve=0 ORDER BY a.j_id DESC";

                                        //  $query = "select a.*,b.*,d.*,e.subclient_name from tbl_job_create a inner join tbl_company_profile b left join tbl_user_applied_job d on a.j_id=d.job_id left join tbl_subClient_create e on a.parent_client=e.id  where  a.job_approve=0 and (a.client_id=b.client_id or a.client_id !=b.client_id)  group by a.j_id order by a.j_id DESC";
                                          $query = "select a.*,b.*,d.*,e.subclient_name,f.saved,f.user_id as u_id ,g.saved as jobAlrt,g.user_id as jau_id ,i.company_logo from tbl_job_create a inner join tbl_company_profile b left join tbl_user_applied_job d on a.j_id=d.job_id left join tbl_subClient_create e on a.parent_client=e.id left join tbl_user_saved_jobs f on a.j_id=f.job_id left join tbl_user_job_alert g on a.j_id=g.job_id  inner JOIN tbl_company_logo i ON b.client_id = i.client_id  where  a.job_approve=0 and (a.client_id=b.client_id or a.client_id !=b.client_id)  group by a.j_id order by a.j_id DESC";

                                  //echo $query; exit;


                                          $result= $this->db->query($query);

                                          $searchedJobs = $result->result();

                       // echo "<pre>"; print_r($searchedJobs); exit;

                                          return  $searchedJobs;


                                        }

                             /*============FUNCTION FOR GET JOBS BY INDUSRTY===============*/

                           public function getJobsByIndustry()

                             {  
                              

                             /* $JobsByIndustry = $this->db->select('tbl_job_create.*, tbl_mst_industry.industry_name')
                                                         ->from('tbl_job_create')
                                                         ->join('tbl_mst_industry','tbl_job_create.industry =tbl_mst_industry.ind_id')
                                                         ->get()->result();

                                  echo $this->db->last_query(); exit;*/

                                  $query = "SELECT `tbl_job_create`.*,(select count(tbl_job_create.industry)) as total_ind , `tbl_mst_industry`.`industry_name` FROM `tbl_job_create` JOIN `tbl_mst_industry` ON `tbl_job_create`.`industry` =`tbl_mst_industry`.`ind_id`  WHERE tbl_job_create.job_approve = 0  group by tbl_job_create.industry ";

                                                    $result= $this->db->query($query);
                                                    $JobsByIndustry = $result->result();

                                          return $JobsByIndustry;

                            }



                               /*=========== FUNCTION FOR GET ALL INDUSTRIES ================ */
                                      public function getAllIndustries()
                                      {

                                        /* $allIndustries = $this->db->select('*')
                                                                   ->from('tbl_mst_industry')
                                                                   ->get()->result();*/
                                                
                                              //  echo "<pre>"; print_r($allIndustries); exit;

                                         $query = "SELECT `tbl_job_create`.*,(select count(tbl_job_create.industry)) as total_ind , `tbl_mst_industry`.`industry_name` FROM `tbl_job_create` JOIN `tbl_mst_industry` ON `tbl_job_create`.`industry` =`tbl_mst_industry`.`ind_id` WHERE tbl_job_create.job_approve = 0 group by tbl_job_create.industry ";

                                                    $result= $this->db->query($query);
                                                    $allIndustries = $result->result();

                                                   // echo "<pre>"; print_r($allIndustries); exit;
                                                       return $allIndustries;


                                      }

                                   /*=============== FUNCTION FOR GET ALL JOBS BY TITLE =============*/

                                      public function getJobsByTitle()
                                      {
                                        $query = "SELECT DISTINCT(job_title), count(job_title) as total_title FROM `tbl_job_create` where tbl_job_create.job_approve=0 GROUP BY `job_title`";
                                                     $result= $this->db->query($query);
                                                    $jobsByTitle = $result->result();

                                                   // echo "<pre>"; print_r($jobsByTitle); exit;
                                                         return $jobsByTitle;

                                      }

                                   /*=========== FUNCTION FOR GET JOBS BY CITY ==========*/
                                     
                                     public function getJobsByCity()
                                     {
                                         $query = "SELECT DISTINCT(city), count(city) as total_city FROM `tbl_job_create` WHERE tbl_job_create.job_approve=0  GROUP BY `city`";
                                       
                                             $result= $this->db->query($query);
                                             $jobsByCity = $result->result();
                                            // echo "<pre>"; print_r($jobsByCity); exit;
                                                 return $jobsByCity;

                                     }


                                     /*=========== FUNCTION FOR GET JOBS BY EXPERIENCE ===========*/

                                      public function getJobsByExperience()
                                      {
                                       //  $query = "SELECT DISTINCT(job_type), count(job_type) as total_exp FROM tbl_job_create where tbl_job_create.job_approve=0 GROUP BY  tbl_job_create.job_type ";
                                              
                                             // echo $query; exit;
                                      $query = "SELECT DISTINCT(tblmjob.job_type),count(tbl_job_create.job_type) as Job_Count ,tbl_job_create.experience as total_exp FROM tbl_job_create inner join tbl_mst_job_type tblmjob on tbl_job_create.job_type=tblmjob.id where  tbl_job_create.job_approve=0 GROUP BY tbl_job_create.job_type";
                                           $result = $this->db->query($query);
                                           $experienceJobs = $result->result();
                                          //  echo "<pre>"; print_r($experienceJobs); exit;
                                           return $experienceJobs;

                                      }

                                     /*========== FUNCTION FOR GET OPPENNING ON CHECK INDUSTRY =============*/


                                     public function getOppenningByIndustry()
                                     {
                                           
                                          // echo "<pre>"; print_r($_POST); exit;

                                       if(!empty(($_POST))){


                                      

                                      // $query = "SELECT a.industry_name, b.*,c.company_logo,d.*,count(b.industry) as total_ind from  tbl_mst_industry a  INNER JOIN  tbl_job_create b ON a.ind_id=b.industry LEFT JOIN tbl_company_logo c ON c.client_id = b.client_id INNER JOIN tbl_client_profile d ON b.client_id=d.client_id WHERE b.job_approve = 0 AND ";  
                                         
                                         $query ="select a.*,b.*,d.*,e.subclient_name ,f.saved,f.user_id as u_id ,g.saved as jobAlrt,g.user_id as jau_id,i.company_logo from tbl_job_create a inner join tbl_company_profile b left join tbl_user_applied_job d on a.j_id=d.job_id left join tbl_subClient_create e on a.parent_client=e.id left join tbl_user_saved_jobs f on a.j_id=f.job_id left join tbl_user_job_alert g on a.j_id=g.job_id inner JOIN tbl_company_logo i ON b.client_id = i.client_id where  a.job_approve=0 and (a.client_id=b.client_id or a.client_id !=b.client_id) AND ";
     


                                          foreach ($_POST['type'] as $indsType) {
                        
                                       $query.= "(a.industry LIKE '%$indsType%')"  ;
                                       //$query.="GROUP BY b.job_title";
                                       $query.=" OR ";
                                       $query.="(a.job_title LIKE '%$indsType%')";
                                       $query.=" OR ";
                                       $query.="(a.city LIKE '%$indsType%')";
                                       $query.=" OR ";
                                       $query.= "(a.job_type LIKE '%$indsType%' )";
                                       $query.=" OR ";
                                       $query.= "(a.experience LIKE '%$indsType%')";
                                       $query.=" OR ";


                            }
                     $q = rtrim($query, ' OR ');
                    
                     $q.='GROUP BY a.job_title ';
                     $q.=' ORDER BY a.j_id DESC';

                      // echo $q; exit;


                                     
                                         // echo $query; exit;
                                       $result = $this->db->query($q);

                                        $searchedResult = $result->result();

                                            return $searchedResult;


                                         }else{


                                             //$query ="SELECT a.*,b.*,c.*,d.*  FROM tbl_job_create a left join tbl_company_logo b ON b.client_id = a.client_id LEFT JOIN tbl_user_applied_job c ON a.j_id=c.job_id inner join tbl_client_profile d ON a.client_id=d.client_id WHERE a.job_approve=0 ORDER BY a.j_id DESC";
                                             
                                           //  $query="select a.*,b.*,d.*,e.subclient_name from tbl_job_create a inner join tbl_company_profile b left join tbl_user_applied_job d on a.j_id=d.job_id left join tbl_subClient_create e on a.parent_client=e.id  where  a.job_approve=0 and (a.client_id=b.client_id or a.client_id !=b.client_id)  group by a.j_id order by a.j_id DESC";
                                           
                                           $query = "select a.*,b.*,d.*,e.subclient_name,f.saved,f.user_id as u_id ,g.saved as jobAlrt,g.user_id as jau_id ,i.company_logo,d.created_date as applied_date from tbl_job_create a inner join tbl_company_profile b left join tbl_user_applied_job d on a.j_id=d.job_id left join tbl_subClient_create e on a.parent_client=e.id left join tbl_user_saved_jobs f on a.j_id=f.job_id left join tbl_user_job_alert g on a.j_id=g.job_id  inner JOIN tbl_company_logo i ON b.client_id = i.client_id  where  a.job_approve=0 and (a.client_id=b.client_id or a.client_id !=b.client_id)  group by a.j_id order by a.j_id DESC";
                                        $result = $this->db->query($query);

                                        $searchedResult = $result->result();

                                            return $searchedResult;

                                         }    

                                      }
                                    

                                        /*============= FUNCTION FOR GET JOB DESCRIPTION ============*/

                                        public function getJobDescription($j_id)
                                        {

                                      //   $query = "SELECT a.*,b.*,c.*, insd.industry_name,tblcp.cmpny_name FROM tbl_job_create a LEFT JOIN tbl_company_logo b ON b.client_id = a.client_id  INNER JOIN  tbl_mst_industry insd ON a.industry=insd.ind_id INNER JOIN tbl_client_profile tblcp ON a.client_id=tblcp.client_id INNER JOIN tbl_user_applied_job c ON a.j_id=c.job_id WHERE  a.j_id='".$j_id."' ";
                                 // $query ="SELECT a.*,b.*,c.*, insd.industry_name,tblcp.cmpny_name FROM tbl_job_create a LEFT JOIN tbl_company_logo b ON b.client_id = a.client_id INNER JOIN tbl_mst_industry insd ON a.industry=insd.ind_id left JOIN tbl_client_profile tblcp ON a.client_id=tblcp.client_id left JOIN tbl_user_applied_job c ON a.j_id=c.job_id WHERE a.j_id='".$j_id."'";

                                        $query ="select a.*,b.*,d.*,e.subclient_name,f.saved,f.user_id as u_id ,g.saved as jobAlrt,g.user_id as jau_id,h.job_type as industry_name,i.company_logo,d.created_date as applied_date from tbl_job_create a inner join tbl_company_profile b left join tbl_user_applied_job d on a.j_id=d.job_id left join tbl_subClient_create e on a.parent_client=e.id left join tbl_user_saved_jobs f on a.j_id=f.job_id left join tbl_user_job_alert g on a.j_id=g.job_id  inner join tbl_mst_job_type h on a.job_type=h.id  inner JOIN tbl_company_logo i ON b.client_id = i.client_id where  a.job_approve=0 and a.j_id='".$j_id."' and (a.client_id=b.client_id or a.client_id !=b.client_id)  group by a.j_id";



                                         // echo $query; exit;

                                         $result= $this->db->query($query);

                                         $jd = $result->row();

                              // echo "<pre>"; print_r($jd); exit;

                                         return  $jd;
                                       }





                                       /*================= FUNCTION FOR SAVE APPLY JOB BY USER===============*/

                                       public function saveApplyJobsByUser($jobs)
                                       {

                                        $saveJob = $this->db->insert('tbl_user_applied_job',$jobs);

                                        if($saveJob)
                                        {

                                          return $saveJob;
                                        }

                                      }


                                      /*========== FUNCTION FOR GET USER APPLIED JOBS ========*/

                                      public function getUserAppliedJobs($user_id)
                                      {

                                        /*$appliedJobs = $this->db->select('job_id')
                                                                ->from('tbl_user_applied_job')
                                                                ->where('user_id',$user_id)
                                                                ->get()->result();

                                                  echo "<pre>"; print_r($appliedJobs); exit;
                                                  return $appliedJobs;*/



                                                  $query ="SELECT a.*,b.*,c.*,d.*,c.created_date as aplliedDate FROM  tbl_user_applied_job a inner join tbl_job_create b on a.job_id=b.j_id inner join tbl_company_profile d left join tbl_user_applied_job c on b.j_id=c.job_id   WHERE a.user_id ='".$user_id."'  and (b.client_id=d.client_id or b.client_id !=d.client_id) group by a.job_id";



                                        //  echo $query; exit;

                                         $result= $this->db->query($query);

                                         $appliedJobs = $result->result();

                                  // echo "<pre>"; print_r($appliedJobs); exit;

                                         return  $appliedJobs;

                                      }



      /*================ FUNCTION FOR GET APPLIED JOBS ==================*/

       public function getAppliedUserJobs($user_id)
       {

         $appliedJobs  = $this->db->select('*')
                                  ->from('tbl_user_applied_job')
                                  ->where('user_id',$user_id)
                                  ->get()->result();

                        return     (count($appliedJobs));


       }
         
         /*============== FUNCTION FOR GET LAST UPDATE OF USER PROFILE============*/

         public function getLastUpdateuserProfile($user_id)
         {
           $lastUpdate = $this->db->select('*')
                                  ->from('tbl_user_profile_progress_info')
                                  ->where('u_id',$user_id)
                                  ->get()->row();


                                  return $lastUpdate;
             


         }
   
   /*================ FUNCTION FOR CHECK USER EDUCATION ===========*/
    public function checkUserEducation($user_id)
    {

     $query ="select a.*,b.* from tbl_user_education_info a left join tbl_user_profile_progress_info b on a.user_id=b.u_id where a.user_id='".$user_id."' ";
        
        //echo $query; exit;
         $result= $this->db->query($query);

        $status = $result->num_rows();
        // echo $status; exit;

          return $status;

    }


    /*================ FUNCTION FOR CHECK USER PRE EMPLOYMENT ==============*/

     public function checkUserPreEmployment($user_id)
     {
        
        $query = " select a.*, b.* from tbl_user_pre_employment_info a left join tbl_user_profile_progress_info b on a.user_id=b.u_id where a.user_id='".$user_id."'";

           $result= $this->db->query($query);

           $status = $result->num_rows();
          // echo $status; exit;

          return $status;


     }
  /*=============== FUNCTION FOR GET JOBS ADVANCED SEARCH 1 ===================*/

          public function getJobsByAdSearch1($Keywords,$KeywordsContains,$jobtitle,$jt,$locations)
          {

           // echo  $Keywords."  ".$KeywordsContains."  ".$jobtitle."  ".$jt; exit;

            if($KeywordsContains == 'any')
            {
               
                // echo "contains".$KeywordsContains;
                $filterdKeyword =  str_replace(',',' ',$Keywords,$i);

               // echo $filterdKeyword; exit;



                $sql = "SELECT a.*,b.*,c.*,d.* FROM tbl_job_create a LEFT JOIN tbl_company_logo b ON b.client_id = a.client_id LEFT JOIN tbl_user_applied_job c ON a.j_id=c.job_id LEFT JOIN tbl_client_profile d ON a.client_id=d.client_id WHERE   a.job_approve=0 AND(job_title LIKE '%$jobtitle%'  OR job_title LIKE  '%$Keywords%' OR  qualifications LIKE '%$Keywords%' OR  qualifications LIKE '%$jobtitle%' OR  city LIKE '%$locations%') ";

                //  echo $sql; exit;

                    $result = $this->db->query($sql);

                    $getJobs = $result->result();


                    return $getJobs;


                   
            }

             elseif($KeywordsContains == 'every') {

              
               $filterdKeyword =  str_replace(',',' ',$Keywords,$i);
                
                  $keys = explode(" ",$filterdKeyword);

                   // echo "<pre>"; print_r($keys); exit;

                    $sql = "SELECT a.*,b.*,c.*,d.* FROM tbl_job_create a LEFT JOIN tbl_company_logo b ON b.client_id = a.client_id LEFT JOIN tbl_user_applied_job c ON a.j_id=c.job_id LEFT JOIN tbl_client_profile d ON a.client_id=d.client_id WHERE  a.job_approve=0 AND (job_title LIKE '%$filterdKeyword%' OR job_title LIKE '%$jobtitle%' OR  qualifications LIKE '%filterdKeyword%' OR  qualifications LIKE '%$jobtitle%' OR city LIKE '%$locations%' ) ";

                    foreach($keys as $k){
                        $sql .= " OR a.job_approve=0  AND(job_title LIKE '%$k%' OR qualifications LIKE '%$k%') ";
                    }
          
                 //  echo $sql; exit;
                    $result = $this->db->query($sql);

                    $getJobs = $result->result();


                   // echo "<pre>"; print_r($getJobs); exit;

                    return $getJobs;


               
             }



          }
        
        /*============ FUNCTION FOR GET JOBS ADVANCED SEARCH2 ============*/

         public function getJobsByAdSearch2($salaryType,$salaryRangeFrom,$salaryRangeTo,$hourlyRangeFrom,$hourlyRangeTo)

           {

             
            $sql = "SELECT a.*,b.*,c.*,d.* FROM tbl_job_create a LEFT JOIN tbl_company_logo b ON b.client_id = a.client_id LEFT JOIN tbl_user_applied_job c ON a.j_id=c.job_id LEFT JOIN tbl_client_profile d ON a.client_id=d.client_id WHERE  a.job_approve=0 AND( a.salary BETWEEN '".$salaryRangeFrom."' AND '".$salaryRangeTo."') ";
                   
                    foreach ($_POST['jobNature'] as $jobType) {
                        
                        $sql.= "OR a.job_approve=0 AND (job_type LIKE '%$jobType%')";

                    }

                    foreach($_POST['jobsSector'] as $k){
                        $sql .= " OR a.job_approve=0  AND(industry LIKE '%$k%') ";
                    }
          
                  // echo $sql; exit;
                    $result = $this->db->query($sql);

                    $getJobs = $result->result();


                   // echo "<pre>"; print_r($getJobs); exit;

                    return $getJobs;



           }


           /*================ FUNCTION SAVE JOB ============*/


           public function saveUserJob($user_id,$job_id)
           {
           
              $data = array(
                'user_id' => $user_id,
                'job_id' => $job_id,
                 'saved' =>1
                );
                  
                

                $check =$this->db->select('*')
                                 ->from('tbl_user_saved_jobs')
                                 ->where('user_id',$user_id)
                                 ->where('job_id',$job_id)
                                 ->get()->row();
                      
               // echo "<pre>"; print_r($check); exit;

               if(!empty($check))
               {
                    // echo 123; exit;

                     $i=  $check->saved;

                      if($i==0)
                      {

                        $saved = 1;

                      }
                       else{

                        $saved = 0;
                       }
                           $data1 = array(
                          'user_id' => $user_id,
                          'job_id' => $job_id,
                           'saved' =>$saved
                          );


                     $saveJob = $this->db->where('job_id',$job_id) 
                                         ->where('user_id',$user_id)
                                         ->update('tbl_user_saved_jobs',$data1);   
                                       
                                        //$this->db->last_query();

                                         return $saveJob;

               }else{
                   
                    $saveJob = $this->db->insert('tbl_user_saved_jobs',$data);


                        return $saveJob;
               }  

            
 

           }
          
          /*========== FUNCTION FOR GET SAVED JOBS ==========*/

          public function getSavedJobs($user_id,$job_id)
          {

            $records = $this->db->select('saved')
                                ->from('tbl_user_saved_jobs')
                                ->where('user_id',$user_id)
                                ->where('job_id',$job_id)
                                ->get()->row();

                        //  echo "<pre>"; print_r($records); exit;
                                return $records;


          }



          /*=============== FUNCTION FOR SET JOB ALERT ========= */

          public function setJobAlert($user_id,$job_id)
          {

              
                $data = array(
                'user_id' => $user_id,
                'job_id' => $job_id,
                 'saved' =>1
                );
                  
                

                $check =$this->db->select('*')
                                 ->from('tbl_user_job_alert')
                                 ->where('user_id',$user_id)
                                 ->where('job_id',$job_id)
                                 ->get()->row();
                      
               // echo "<pre>"; print_r($check); exit;

               if(!empty($check))
               {
                    // echo 123; exit;

                     $i=  $check->saved;

                      if($i==0)
                      {

                        $saved = 1;

                      }
                       else{

                        $saved = 0;
                       }
                           $data1 = array(
                          'user_id' => $user_id,
                          'job_id' => $job_id,
                           'saved' =>$saved
                          );


                     $saveJob = $this->db->where('job_id',$job_id) 
                                         ->where('user_id',$user_id)
                                         ->update('tbl_user_job_alert',$data1);   
                                       
                                        //$this->db->last_query();

                                         return $saveJob;

               }else{
                   
                    $saveJob = $this->db->insert('tbl_user_job_alert',$data);


                        return $saveJob;
               }  


          }


           /*========== FUNCTION FOR GET SET JOB ALERT ==========*/

          public function getSavedJobsAlert($user_id,$job_id)
          {

            $records = $this->db->select('saved')
                                ->from('tbl_user_job_alert')
                                ->where('user_id',$user_id)
                                ->where('job_id',$job_id)
                                ->get()->row();

                        //  echo "<pre>"; print_r($records); exit;
                                return $records;


          }


          /*=========== FUNCTION FOR GET SAVED JOBS ===========*/

          public function getUserSavedJobs($user_id)
          {

             $savedJobs = $this->db->select('*')
                                   ->from('tbl_user_saved_jobs')
                                   ->where('user_id',$user_id)
                                   ->where('saved',1)
                                   ->get()->result();

                                      
                                    return ($savedJobs);
                                   //echo "<pre>"; print_r($savedJobs); exit;



          }


          /*==================FUNCTION FOR SHOW SAVED JOBD================*/

          public function getUserSavedJobsForView($user_id)
          {

             
                 $query ="SELECT a.*,b.*,c.*,d.* FROM  tbl_user_saved_jobs a inner join tbl_job_create b on a.job_id=b.j_id inner join tbl_company_profile d left join tbl_user_applied_job c on b.j_id=c.job_id   WHERE a.user_id ='".$user_id."' and a.saved=1 and (b.client_id=d.client_id or b.client_id !=d.client_id) group by a.job_id";



                                        //  echo $query; exit;

                                         $result= $this->db->query($query);

                                         $userSavedJobs = $result->result();

                                  // echo "<pre>"; print_r($jd); exit;

                                         return  $userSavedJobs;

          }




          /*=========FUNCTION FOR GET BENEFITS FROM MST TABLE ========*/



          public function getBenefits()
          {


              $benefits = $this->db->select('*')
                                   ->from('tbl_mst_type_of_benefits')
                                   ->get()->result();

                                   return $benefits;



          }


          /*=============== FUNCTION FOR GET FREQUENCY FORM MST TBL ================*/

          public function getFrequency()
          {
              $frequency = $this->db->select('*')
                                    ->from('tbl_mst_frequency') 
                                    ->get()->result();


                                    return $frequency;



          }


          /*=============== FUNCTION FOR GET INCOME FREQUECY FROM MST TBL===========*/

           public function getIncomeFrequency()
           {
               
               $incomeFrequency = $this->db->select('*')
                                           ->from('tbl_mst_income_frequency')
                                           ->get()->result();

                                           return $incomeFrequency;
                                 }


           /*========== FUNCTION FOR GET CURRENCY FROM MST TBL ==========*/
           

            public function getCurrency()
            {

              $currency = $this->db->select('*')
                                   ->from('tbl_mst_currency')
                                   ->get()->result();

                                   return $currency;



            }    

            /*============= FUNCTION FOR SAVE USER BENEFITS ================*/
            public  function  saveUserRecieptBenefits($recieptbenefitsData)

            {
                  
                //  echo "<pre>"; print_r($recieptbenefitsData); exit;

                $userbenefits = $this->db->insert('tbl_user_reciept_benefits',$recieptbenefitsData);


                 return  $userbenefits;
               
            }        


            /*========== FUNCTION FOR GET USER RECIEPT BENEFITS =============*/

             public function getUserRecieptBenefits($user_id)
             {
                $userbenefits = $this->db->select('*')
                                         ->from('tbl_user_reciept_benefits')
                                         ->get()->row();

                                   //  echo "<pre>"; print_r($userbenefits); exit;
                                         return  $userbenefits;





             }     


             /*================= FUNCTION FOR GET USER SAVED BENEFITS ============*/

              public function getUserSaveBenefits($user_id)
              {
              
                /* $userSaveBenefits = $this->db->select('*')
                                              ->from('tbl_user_reciept_benefits')
                                              ->join('')
                                              ->where('user_id',$user_id)
                                              ->get()->result();
                              
                           //   echo "<pre>"; print_r($userSaveBenefits); exit;
                                              return $userSaveBenefits;*/


                                      $query ="SELECT a.id,a.reciept_benefit,mstbene.benefits,a.other_benefit,c.frequency as benefitFreq,a.amount,d.currency_code,d.currency_name,a.reciept_f_day,a.reciept_f_mnth,a.reciept_f_yr,a.receiving,a.reciept_t_day,a.reciept_t_mnth,a.reciept_t_yr FROM tbl_user_reciept_benefits a left join tbl_mst_type_of_benefits mstbene on a.benefit_type=mstbene.id left join tbl_mst_frequency c on a.benefit_frequency=c.id inner join tbl_mst_currency d on a.currency=d.currency_id where a.user_id='".$user_id."' and a.flag=1";     
                                      

                                      $result= $this->db->query($query);

                                         $userSaveBenefits = $result->result();   

                                        //  echo "<pre>"; print_r($userSaveBenefits); exit;

                                         return $userSaveBenefits;


              }      


              /*============== FUNCTION FOR GET USER SAVED BENFITS FOR UPDATE ======== */

              public function getUserBenefitsInfo($id)
              {

                  $query ="SELECT  a.id,a.user_id,a.reciept_benefit,a.benefit_type,a.other_benefit,a.benefit_frequency,a.amount,a.currency,a.reciept_f_day,a.reciept_f_mnth,a.reciept_f_yr,a.receiving,a.reciept_t_day,a.reciept_t_mnth,a.reciept_t_yr FROM tbl_user_reciept_benefits a where a.id='".$id."'";     
                                      

                                      $result= $this->db->query($query);

                                         $userSavedBenefits = $result->row();

                                        //echo "<pre>"; print_r($userSavedBenefits); exit;

                                         return $userSavedBenefits;




              }

                /*=============== FUNCTION FOR UPDATE USER  BENEFITS =============*/


                public function updateUserBenefits($id,$newBenefits)
                {


                    $updateBenefits = $this->db->where('id',$id)
                                               ->update('tbl_user_reciept_benefits',$newBenefits);

                                              return  $updateBenefits;

                }  


              /*========== FUNCTION FOR DELETE USER BENEFTS ============*/

              public function delBeneftsInfo($id,$data)
              {


                 $userBenefts = $this->db->where('id',$id)
                             ->update('tbl_user_reciept_benefits',$data);
                             
                             return $userBenefts;


              }

              /*============ FUNCTION FOR SAVE USER ADDRESS ========*/

               public function saveUserAddress($addressArray,$user_id)
               {

                  $address = $this->db->insert('tbl_user_address',$addressArray);

                   if($address)
                   {
                      $progress = array(
                             
                             'address' => 1
                           );

                        $this->db->where('u_id',$user_id)
                                 ->update('tbl_user_profile_progress_info',$progress);
                    }

                  return $address;

                 }



                 /*==============FUCTION FOR GET USER SAVED ADDRESSES ==============*/

                  public function getUserSavedAddress($user_id)
                  {
                    
                    $feedAdd = $this->db->select('tbl_user_address.*,tbl_mst_countries.country_name,tbl_mst_month.month_name')
                        ->from('tbl_user_address')
                        ->join('tbl_mst_countries','tbl_user_address.country=tbl_mst_countries.id')
                        ->join('tbl_mst_month','tbl_user_address.resident_f_m=tbl_mst_month.id')
                        ->where('tbl_user_address.user_id',$user_id)
                        ->where('tbl_user_address.flag',0)
                        ->get()->result();


                       // echo "<pre>";  print_r($feedAdd); exit;


                         return $feedAdd;



                  }



                  /*============= FUNCTION FOR GET SAVED ADDRESS FOR EDIT ===========*/

                  public function getUserAddressesInfo($id)
                  {


                     $userAddressInfo = $this->db->select('*')
                                                 ->from('tbl_user_address')
                                                 ->where('id',$id)
                                                 ->get()->row();


                                                 return $userAddressInfo;


                  }



                  /*============= FUCTION FOR UPDATE USER ADDRESS ============*/

                  public function updateUserAddress($addressArray,$id)
                  {

                    $udateAddress = $this->db->where('id',$id)
                                             ->update('tbl_user_address',$addressArray);

                                             return $udateAddress;

                  }


                  /*=============== FUNCTION FOR DELETE ADDRESS ===========*/


                  public function delAddressInfo($id,$data)
                  {


                      $userAddress = $this->db->where('id',$id)
                             ->update('tbl_user_address',$data);
                             
                             return $userAddress;

                  }


                  //function for fetching countries
              function fetchContries(){
             $result=$this->db->select('*')
                                ->from('countries')
                                ->get()->result();
                                return $result;

              }


              // function for  getStates
             function getStates($country_id)
             {
               
               $states = $this->db->select('*')
                                 ->from('states')
                                 ->where('country_id',$country_id)
                                 ->get()->result();

                                 return $states;

             }


             // function for getCities

             function getCities($state_id)
             {
                $cities = $this->db->select('*')
                                    ->from('cities')
                                    ->where('state_id',$state_id)
                                    ->get()->result();

                                    return $cities;

             }


     // need to upload on server

             //function for saving referral images
              function insertrefferalImages($array){

               $result = $this->db->insert('tbl_referral_images',$array);
              return $result;

              }

  // function for save guest info
   
   public function saveGuestInfo($guestInfoArray)
   {

     $guestInfo = $this->db->insert('tbl_guest',$guestInfoArray);

       $guestId = $this->db->insert_id();

       return $guestId;
  

   }

   // function for save guest referrral  homeless info 

    public function insertHomelessInfo($homelessInfo)
    {
       
       $homelessInfo = $this->db->insert('tbl_guest_homeless_referral',$homelessInfo);

       return $homelessInfo;
    }



  
    }
  ?>