    <?php
    class Client_model extends CI_Model{

    //function for adding client..

        function add_client($info){

         //echo "<pre>";print_r($info);exit;
            $result=$this->db->insert('tbl_client',$info);
            return $this->db->insert_id();

        }
    //function for checking login

        function check_user_login($emailaddress, $password){

            $result=$this->db->select('*')
            ->from('tbl_client')
            ->where('clnt_email',$emailaddress)
            ->where('clnt_password',$password)
            ->where('flag','1')

            ->get()->row();
            return $result;



        }

        //function for inserting client online data.
        function insert_online_data($insert_client_online_data){
            $result=$this->db->insert('tbl_client_profile',$insert_client_online_data);
            return $result;


        }

        //function for activating client
        function activateClientLogin($data,$token){
            $result=$this->db->where('id',$token)->update('tbl_client',$data);
            return $result;

        }

        //function for checking email
        function check_client_forgot_password($emailaddress){
         $result=$this->db->select('*')
         ->from('tbl_client')
         ->where('clnt_email',$emailaddress)
         ->get()->row();
         return $result;

     }

        //function for updating keystring

     function update_keystring($emailaddress,$randomkeystring){
        date_default_timezone_set('UTC');
              //date_default_timezone_set("Asia/Kolkata");
        $newkeystring = array(
            'keystring' => $randomkeystring,
            'isused' => 0,
            'updatedtimestamp' => date("Y-m-d H:i:s")
            );
        $result=$this->db->where('clnt_email',$emailaddress)->update('tbl_client',$newkeystring);
        return $result;


    }

       //function for checking random key
    function check_randomkey_string($randomkeystring){
        $result=$this->db->select('*')
        ->from('tbl_client')
        ->where('keystring',$randomkeystring)
        ->get()->row();
        return $result;

    } 

       //function for reseting password
    function update_password($emailaddress,$npassword) {
        $newpassword = array(
            'clnt_password' => md5($npassword),
            'isused' => 1
            );
        $this->db->where('clnt_email', $emailaddress);
        $this->db->update('tbl_client', $newpassword);
            //print_r($this->db->last_query()); exit;
        return TRUE;

    }

    //function for checking email
    function checkEmailId($emailid){

        $result=$this->db->select('*')
        ->from('tbl_client')
        ->where('clnt_email',$emailid)
        ->get()->row();
        return $result;
    }

    //funtion for check online form data
    function checkOnlineFormData($id){
        //echo $id;exit;
        $result=$this->db->select('*')
        ->from('tbl_client_profile')
        ->where('client_id',$id)
        ->get()->row();
                     //print_r($this->db->last_query());exit;
        return $result;

    }

    //function to check email

    function checkEmailExist($email){
        $result=$this->db->select('*')
        ->from('tbl_client')
        ->where('clnt_email',$email)
        ->get()->row();
        //echo '<pre>';print_r($this->db->last_query());exit;
        return $result;
    }

    //function for checking online profile data
    function checkProfile($id){

        $result=$this->db->select('tbl_client_profile.*,tbl_mst_countries.id')
        ->from('tbl_client_profile')
        ->join('tbl_mst_countries','tbl_mst_countries.id=tbl_client_profile.clnt_country')
        ->where('client_id',$id)
        ->get()->row();
        return $result;

    }

    //function for updating online data
    function update_online_data($insert_client_online_data,$id){
    // echo "<pre>";print_r($insert_client_online_data);exit;
    // echo $id;exit;
        $result=$this->db->where('client_id',$id)->update('tbl_client_profile',$insert_client_online_data);
        return $result;
    }


    //function for getting all packages
    function getAllPackages(){

       /* $result=$this->db->select('tbl_package.*,tbl_package_image.package_image')
        ->from('tbl_package')
        ->join('tbl_package_image','tbl_package_image.package_id=tbl_package.id')
        ->where('tbl_package.validity_date >=',date('Y-m-d H:i:s'))
        ->get()->result();
        return $result;*/

        $result=$this->db->select('tbl_package.*,(select SUM(job_adds) from tbl_job_adds  where tbl_job_adds.package_id = tbl_package.id) as totalJobAdds,(select service from tbl_service_package where tbl_service_package.package_id=tbl_package.id) as totalServices,(select package_image from tbl_package_image where tbl_package_image.package_id=tbl_package.id) as package_pic')
        ->from('tbl_package')
        //->join('tbl_job_adds','tbl_job_adds.admin_id=tbl_package.package_title')
        ->where('tbl_package.validity_date >=',date('Y-m-d H:i:s'))
        ->get()->result();
     //   print_r($this->db->last_query());exit;
        return $result;


    }



function getAllPurchasedPackages($id){


        $result=$this->db->select('tbl_package.*,(select SUM(job_adds) from tbl_job_adds  where tbl_job_adds.package_id = tbl_package.id) as totalJobAdds,(select service from tbl_service_package where tbl_service_package.package_id=tbl_package.id) as totalServices,(select package_image from tbl_package_image where tbl_package_image.package_id=tbl_package.id) as package_pic')
        ->from('tbl_package')
        //->join('tbl_job_adds','tbl_job_adds.admin_id=tbl_package.package_title')
        ->where('tbl_package.id',$id)
        ->where('tbl_package.validity_date >=',date('Y-m-d H:i:s'))
        ->get()->result();
       // print_r($this->db->last_query());exit;
        return $result;


    }


    //function for getting package details
    function getPackageDetail($id){

        /*$result=$this->db->select('*')
        ->from('tbl_package')
        //->join('tbl_mst_package','tbl_mst_package.id=tbl_package.package_title')
        ->where('tbl_package.id',$id)
        ->where('tbl_package.validity_date >=',date('Y-m-d H:i:s'))
        ->get()->row();
        return $result;*/

        $result=$this->db->select('tbl_package.*,(select SUM(job_adds) from tbl_job_adds  where tbl_job_adds.package_id = tbl_package.id) as totalJobAdds,(select service from tbl_service_package where tbl_service_package.package_id=tbl_package.id) as totalServices,(select package_image from tbl_package_image where tbl_package_image.package_id=tbl_package.id) as package_pic')
        ->from('tbl_package')
        //->join('tbl_job_adds','tbl_job_adds.admin_id=tbl_package.package_title')
        ->where('tbl_package.validity_date >=',date('Y-m-d H:i:s'))
        ->where('tbl_package.id',$id)
        ->get()->row();
       // print_r($this->db->last_query());exit;
        return $result;



    }

    //function for getting packages count
    function getPackageCount(){
        $result=$this->db->select('count(id) as packagescount')
        ->from('tbl_package')
        ->get()->row();
        return $result;
    }


    // function for get services to respective package from tbl_service_package

    function getServicePackage($id)
    {

       $services = $this->db->select('*')
                            ->from('tbl_service_package')
                            ->where('package_id')
                            ->get()->row();


       return  $services;


    }




    // function for check package services

    public function checkPackageSevices($id,$role)
    {
        //->join('tbl_mst_status','tbl_job_create.job_status=tbl_mst_status.status_id')

        $check = $this->db->select('tbl_job_adds.job_adds,tbl_job_adds.type')
        ->from('tbl_job_adds')
        ->join('tbl_packages_purchased','tbl_job_adds.package_id=tbl_packages_purchased.package_id','inner')
        ->join('tbl_package','tbl_packages_purchased.package_id=tbl_package.id')
        ->where('tbl_packages_purchased.client_id',$id)
        ->where('tbl_packages_purchased.role',$role)
        ->where('tbl_package.validity_date >=',date('Y-m-d H:i:s'))
        ->get()->row();


    //print_r($this->db->last_query()); exit;  

        return $check; 


    }

  
    // count subscribed packages
  public function countSubscribePackages($id,$role)
  {

          $check = $this->db->select('tbl_job_adds.job_adds,tbl_job_adds.type')
                            ->from('tbl_job_adds')
                            ->join('tbl_packages_purchased','tbl_job_adds.package_id=tbl_packages_purchased.package_id','inner')
                            ->join('tbl_package','tbl_packages_purchased.package_id=tbl_package.id')
                            ->where('tbl_packages_purchased.client_id',$id)
                            ->where('tbl_packages_purchased.role',$role)
                            ->where('tbl_package.validity_date >=',date('Y-m-d H:i:s'))
                            ->get()->result();


    //print_r($this->db->last_query()); exit;  

        return $check; 

  }   


    // function for check total post jobs 

    public function  checkTotalCreateJobs($id,$role)
    {


        if($role=='mainclient')
        {

            $countPostJobs = $this->db->select('*')
            ->from('tbl_job_create')
            ->where('client_id',$id)
            ->where('role',$role)
            ->where('job_publish',1)
            ->get()->result();
               
            //   print_r($this->db->last_query()); exit;  
            return $countPostJobs;

        }else{

          $countPostJobs = $this->db->select('*')
          ->from('tbl_job_create')
          ->where('subClient_id',$id)
          ->where('role',$role)
          ->where('job_publish',1)
          ->get()->result();
     //  print_r($this->db->last_query()); exit;  
          return $countPostJobs;


      }

    }






    //function for inserting job....
    function insertJobCreate($create_job){

      //  echo "<pre>"; print_r($create_job); exit;

        $result=$this->db->insert('tbl_job_create',$create_job);

        
           $lastId = $this->db->insert_id(); 
           //exit;
        
       // echo $lastId; exit;

        return $lastId;

    }

    //function for getting all jobs..
    function getAllJobs($where,$id,$role){
        //echo $id;
        $result=$this->db->select('tbl_job_create.*,tbl_mst_status.status_name')
        ->from('tbl_job_create')
        ->join('tbl_mst_status','tbl_job_create.job_status=tbl_mst_status.status_id')
        //->join('tbl_subClient_create','tbl_subClient_create.id=tbl_job_create.parent_client')
        ->where($where)
        ->where('tbl_job_create.role',$role)
        ->get()->result();
        //echo '<pre>';print_r($this->db->last_query());exit;
       // echo "<pre>"; print_r($result); exit;
        return $result;

    }

    //function for fetching suboffices jobs
    function getSubOfficesJobs($subclient_id){
        //echo $subclient_id;exit;
        $result=$this->db->select('tbl_job_create.*,tbl_mst_status.status_name')
        ->from('tbl_job_create')
        ->join('tbl_mst_status','tbl_job_create.job_status=tbl_mst_status.status_id')
        //->join('tbl_client','tbl_job_create.subClient_id=tbl_client.id')
        ->where('tbl_job_create.subClient_id',$subclient_id)
        ->get()->result();
       //echo '<pre>';print_r($this->db->last_query());exit;
        return $result;


    }


    //function for getting all countries
    function getAllCountries(){
        $result=$this->db->select('*')
        ->from('tbl_mst_countries')
        ->get()->result();
        return $result;
    }

    //function for inserting sub client details..
    function insertSubClientDetails($create_sub_client){
        //echo '<pre>';print_r($create_sub_client);exit;
        $result=$this->db->insert('tbl_subClient_create',$create_sub_client);
        return $result;


    }

    //function for fetching all clients
    function getAllClients($id){
        $result=$this->db->select('tbl_subClient_create.*,tbl_client.clnt_first_name,tbl_client.clnt_last_name,tbl_mst_source.source_name,tbl_mst_industry.industry_name')
        ->from('tbl_subClient_create')
        ->join('tbl_client','tbl_client.id=tbl_subClient_create.client_id')
        ->join('tbl_mst_source','tbl_subClient_create.source=tbl_mst_source.s_id')
        ->join('tbl_mst_industry','tbl_subClient_create.industry=tbl_mst_industry.ind_id')
        ->where('tbl_subClient_create.client_id',$id)
        ->get()->result();
                    //echo '<pre>';print_r($this->db->last_query());exit;
        return $result;
    }

    //function for getting sub client data by id
    function getSubClientDataById($subClientId){
        $result=$this->db->select('tbl_subClient_create.*,tbl_mst_source.s_id,tbl_mst_industry.ind_id')
        ->from('tbl_subClient_create')
        ->join('tbl_mst_industry','tbl_subClient_create.industry=tbl_mst_industry.ind_id')
        ->join('tbl_mst_source','tbl_subClient_create.source=tbl_mst_source.s_id')
        ->where('id',$subClientId)
        ->get()->row();
        return $result;

    }

    //function for updating sub client details
    function updateSubClientDetailsById($create_sub_client,$subClient_id){

        $result=$this->db->where('id',$subClient_id)->update('tbl_subClient_create',$create_sub_client);
        return $result;


    }

    //function for deactivating client account
    function deactivateAccount($updateArr,$subClientId){

        $result=$this->db->where('id',$subClientId)->update('tbl_subClient_create',$updateArr);
        return $result;

    }

    //function for activating client account
    function activateAccount($updateArr,$subClientId){
        $result=$this->db->where('id',$subClientId)->update('tbl_subClient_create',$updateArr);
        return $result;


    }

    //function for getting client information.
    function getSubclientInformation($subClientId){
        $result=$this->db->select('tbl_subClient_create.*,tbl_mst_industry.industry_name,tbl_mst_source.source_name')
        ->from('tbl_subClient_create')
        ->join('tbl_mst_industry','tbl_subClient_create.industry=tbl_mst_industry.ind_id')
        ->join('tbl_mst_source','tbl_subClient_create.source=tbl_mst_source.s_id')
                     //->join('tbl_job_create','tbl_job_create.parent_client=tbl_subClient_create.id')
        ->where('id',$subClientId)
        ->get()->row();
        return $result;


    }

    //function for deactivating job
    function deactiveJob($updateArr,$id){

        $result=$this->db->where('j_id',$id)->update('tbl_job_create',$updateArr);
        return $result;

    }

    //function for activating job
    function activeJob($updateArr,$id){
        $result=$this->db->where('j_id',$id)->update('tbl_job_create',$updateArr);
        return $result;


    }

    //function for getting job data

    function getJobDataById($id){

        $result=$this->db->select('tbl_job_create.*,tbl_mst_countries.id,tbl_mst_status.status_id,tbl_mst_experience.ex_id,tbl_subClient_create.subclient_name,tbl_mst_currency.currency_id')
        ->from('tbl_job_create')
        ->join('tbl_mst_countries','tbl_job_create.nationality=tbl_mst_countries.id')
        ->join('tbl_mst_currency','tbl_mst_currency.currency_id=tbl_job_create.currency')
        ->join('tbl_mst_status','tbl_job_create.job_status=tbl_mst_status.status_id')
        ->join('tbl_mst_experience','tbl_job_create.experience=tbl_mst_experience.ex_id')
       
                     //->join('tbl_mst_skill','tbl_job_create.skills=tbl_mst_skill.id')
        ->join('tbl_subClient_create','tbl_subClient_create.id=tbl_job_create.parent_client','left')
        ->where('tbl_job_create.j_id',$id)
        ->get()->row();
                     //print_r($this->db->last_query());exit;
        return $result;


    }

    //function for checking job data..
    function checkJobData($job_id){

        $result=$this->db->select('*')
        ->from('tbl_job_create')
        ->where('j_id',$job_id)
        ->get()->row();
        return $result;
    }

    //function for updating job.
    function updateJobCreate($update_job,$job_id){

        $result=$this->db->where('j_id',$job_id)->update('tbl_job_create',$update_job);
        return $result;
        
    }


    //function for inserting login credential
    function insertSubClientCredentials($createLogin){
        $result=$this->db->insert('tbl_client',$createLogin);
        return $result;


    }

    //function for checking client
    function checkClientData($email){

       $result=$this->db->select('*')
                        ->from('tbl_client')
                        ->where('clnt_email',$email)
                        ->get()->row();


                      //  echo "<pre>"; print_r($result); exit;
        return $result;


    }

    //function for fetching all Industries
    function getAllIndustries(){
        $result=$this->db->select('*')
        ->from('tbl_mst_industry')
        ->get()->result();
        return $result;

    }

    //function for fetching all status
    function getAllStatus(){
        $result=$this->db->select('*')
        ->from('tbl_mst_status')
        ->get()->result();
        return $result;


    }

    //function for counting total sub clients
    function totalSubClients($clientId){
        $result=$this->db->select('count(id) as id')
        ->from('tbl_subClient_create')
        ->where('client_id',$clientId)
        ->get()->row();
        return $result;

    }

    //function for count total jobs
    function totalJobs($where){
        
        $result=$this->db->select('count(j_id) as j_id')
        ->from('tbl_job_create')
        ->where($where)
        ->get()->row();
        return $result;


    }

    //function for inserting company logo
    function insertCompanyLogo($upload_logo){

        $result=$this->db->insert('tbl_company_logo',$upload_logo);
        return $result;

    }

    //function for fetching job of sub client..
    function getSubClientJobs($subClientId){

        $result=$this->db->select('tbl_job_create.*,tbl_mst_status.status_name')
        ->from('tbl_job_create')
        ->join('tbl_mst_status','tbl_mst_status.status_id=tbl_job_create.job_status')
        ->where('subClient_id',$subClientId)

        ->get()->result();
                     //echo '<pre>';print_r($this->db->last_query());exit;
        return $result;

    }

    //function for fetching all sub clients
    function getAllSubClients($ClientId){
        $result=$this->db->select('*')
        ->from('tbl_subClient_create')
        ->where('client_id',$ClientId)
        ->get()->result();
        return $result;

    }

    //function for fetching subclients
    function getSubClientsName($search,$client_id){
    //echo $search;exit;
        $result=$this->db->select('sub_ofc_loc,id')
        ->from('tbl_subClient_create')
        ->like('subclient_name',$search)
        ->where('client_id',$client_id)
        ->get()->result();
                     //print_r($this->db->last_query());exit;
        return $result;

    }

    //function for fetching sub client data
    function getSubClientData($ClientId){
        $result=$this->db->select('*')
        ->from('tbl_subClient_create')
        ->where('client_id',$ClientId)
        ->get()->result();
                     //print_r($this->db->last_query());exit;
        return $result;


    }

    //function for fetching all sub offices credentials
    function fetchAllSubOfficesCredentials($clientId){
        $result=$this->db->select('*')
        ->from('tbl_client')
        ->where('client_id',$clientId)
        ->get()->result();
        return $result;


    }

    //function for inserting client feedback
    function clientFeedback($create_feedback){
        $result=$this->db->insert('tbl_client_feedback',$create_feedback);
        return $result;


    }

    //function for viewing all sources
    function getAllSources(){
        $result=$this->db->select('*')
        ->from('tbl_mst_source')
        ->get()->result();
        return $result;

    }

    //function for getting all experiences
    function getExperience(){

        $result=$this->db->select('*')
        ->from('tbl_mst_experience')
        ->get()->result();
        return $result;

    }

    //function for disabling client credential
    function disableClientCredential($updateArr,$id){
        $result=$this->db->where('id',$id)->update('tbl_client',$updateArr);
        return $result;


    }

    //function for activating client credential
    function activateClientCredential($updateArr,$id){

        $result=$this->db->where('id',$id)->update('tbl_client',$updateArr);
        return $result;
        
    }

    //function for fetching email of subclient
    function fetchClientEmail($subClientId){
        $result=$this->db->select('*')
        ->from('tbl_client')
        ->get()->row();
        return $result;


    }

    //function for counting offices
    function countOffices($start_date,$end_date,$id){
        $result=$this->db->select('DATE_FORMAT(created_date, "%Y") as year,DATE_FORMAT(created_date, "%m") as month,count(*) as clientid')
            //$result=$this->db->select('count(*) as userid')
        ->from('tbl_subClient_create')
        ->where('created_date >=',$start_date)
        ->where('created_date <=',$end_date)
        ->where('client_id',$id)
        
        ->group_by('DATE_FORMAT(created_date, "%Y%m")')
        ->get()->result();
          // echo "<pre>";print_r($this->db->last_query());
        return $result;


    }

    //function for counting jobs
    function countJobs($start_date,$end_date,$id){
        $result=$this->db->select('DATE_FORMAT(created_date, "%Y") as year,DATE_FORMAT(created_date, "%m") as month,count(*) as jobid')
            //$result=$this->db->select('count(*) as userid')
        ->from('tbl_job_create')
        ->where('created_date >=',$start_date)
        ->where('created_date <=',$end_date)
        ->where('client_id',$id)

        ->group_by('DATE_FORMAT(created_date, "%Y%m")')
        ->get()->result();
          // echo "<pre>";print_r($this->db->last_query());
        return $result;


    }

    //function for fetching subclient id
    function getSubClientId($id){
        $result=$this->db->select('*')
        ->from('tbl_client')
        ->where('client_id',$id)
        ->get()->result();
    //echo '<pre>';print_r($this->db->last_query());exit;
        return $result;

    }

    //function for fetching credential list
    function get_credential_list($clientId){
        $sql="select * from tbl_client where client_id='".$clientId."'";
        $query=$this->db->query($sql);
        //echo "<pre>";print_r($query);exit;
        if($query->result_id->num_rows > 0){
           return $query->result();
       }else{
          return false;
      }

    }

    //function for filtering credential list 
    function getCredentialfilteredList($requestData,$clientId){
       $sql = "select * from tbl_client where client_id='".$clientId."'";
     //$sql.=" WHERE `tbl_client`.`client_id` = '".$clientId."'";
            if (!empty($requestData['search']['value'])) {   // if there is a search parameter, $requestData['search']['value'] contains search parameter
            $sql.=" AND client_id LIKE '" . $requestData['search']['value'] . "%' ";
            $sql.=" OR clnt_email LIKE '" . $requestData['search']['value'] . "%' ";
    //            $sql.=" OR first_name LIKE '" . $requestData['search']['value'] . "%' ";
    //            $sql.=" OR last_name LIKE '" . $requestData['search']['value'] . "%' ";
                //$sql.=" OR (link LIKE '" . $requestData['search']['value'] . "%' )";
                //$sql.=" OR (logo LIKE '" . $requestData['search']['value'] . "%' )";
        }
        $query = $this->db->query($sql);
        if ($query->result_id->num_rows > 0) {
            return $query->result();
        } else {
            return FALSE;
        }



    }
    //function for fetching credential list
    function getCredentialList($requestData,$clientId){

        $columns = array(
            0 => 'id',
            1 => 'clnt_email'
            //2 => 'link',
            //3 => 'logo'
            );
        $sql = "select * from tbl_client where client_id='".$clientId."'";
        //$sql.=" WHERE `user_mobile_numbers`.`isPrimary` = '1'";
            if (!empty($requestData['search']['value'])) {   // if there is a search parameter, $requestData['search']['value'] contains search parameter
            $sql.=" AND (client_id LIKE '" . $requestData['search']['value'] . "%') ";
            $sql.=" OR (clnt_email LIKE '" . $requestData['search']['value'] . "%' )";

    //            $sql.=" OR first_name LIKE '" . $requestData['search']['value'] . "%' ";
    //            $sql.=" OR    last_name LIKE '" . $requestData['search']['value'] . "%' ";
                //$sql.=" OR (link LIKE '" . $requestData['search']['value'] . "%' )";
                //$sql.=" OR (logo LIKE '" . $requestData['search']['value'] . "%' )";
        }
        $sql.=" ORDER BY " . $columns[$requestData['order'][0]['column']] . "   " . $requestData['order'][0]['dir'] . "  LIMIT " . $requestData['start'] . " ," . $requestData['length'] . "   ";
        $query = $this->db->query($sql);
        if ($query->result_id->num_rows > 0) {
            return $query->result();
        } else {
            return FALSE;
        }



    }

    //function for client list
    function get_client_list($clientId){

        $sql="SELECT `tbl_subClient_create`.*, `tbl_client`.`clnt_first_name`, `tbl_client`.`clnt_last_name`, `tbl_mst_source`.`source_name`, `tbl_mst_industry`.`industry_name`
        FROM `tbl_subClient_create`
        JOIN `tbl_client` ON `tbl_client`.`id`=`tbl_subClient_create`.`client_id`
        JOIN `tbl_mst_source` ON `tbl_subClient_create`.`source`=`tbl_mst_source`.`s_id`
        JOIN `tbl_mst_industry` ON `tbl_subClient_create`.`industry`=`tbl_mst_industry`.`ind_id`
        WHERE `tbl_subClient_create`.`client_id` = '".$clientId."'";
        $query=$this->db->query($sql);
        //echo "<pre>";print_r($query);exit;
        if($query->result_id->num_rows > 0){
           return $query->result();
       }else{
          return false;
      }


    }

    //function for filtered client list
    function getClientfilteredList($requestData,$clientId){


        $sql = "SELECT `tbl_subClient_create`.*, `tbl_client`.`clnt_first_name`, `tbl_client`.`clnt_last_name`, `tbl_mst_source`.`source_name`, `tbl_mst_industry`.`industry_name`
        FROM `tbl_subClient_create`
        JOIN `tbl_client` ON `tbl_client`.`id`=`tbl_subClient_create`.`client_id`
        JOIN `tbl_mst_source` ON `tbl_subClient_create`.`source`=`tbl_mst_source`.`s_id`
        JOIN `tbl_mst_industry` ON `tbl_subClient_create`.`industry`=`tbl_mst_industry`.`ind_id` where `tbl_subClient_create`.`client_id`='".$clientId."'";
     //$sql.=" WHERE `tbl_client`.`client_id` = '".$clientId."'";
            if (!empty($requestData['search']['value'])) {   // if there is a search parameter, $requestData['search']['value'] contains search parameter
            $sql.=" AND subclient_name LIKE '" . $requestData['search']['value'] . "%' ";
            $sql.=" OR clnt_first_name LIKE '" . $requestData['search']['value'] . "%' ";
            $sql.=" OR contact_number LIKE '" . $requestData['search']['value'] . "%' ";
    //            $sql.=" OR first_name LIKE '" . $requestData['search']['value'] . "%' ";
    //            $sql.=" OR last_name LIKE '" . $requestData['search']['value'] . "%' ";
                //$sql.=" OR (link LIKE '" . $requestData['search']['value'] . "%' )";
                //$sql.=" OR (logo LIKE '" . $requestData['search']['value'] . "%' )";
        }

        $query = $this->db->query($sql);
        if ($query->result_id->num_rows > 0) {
            return $query->result();
        } else {
            return FALSE;
        }

    }

    //function for client list
    function getClientList($requestData,$clientId){

        $columns = array(
            0 => 'client_id',
            1 => 'clnt_first_name',
            2 => 'subclient_name',
            3 => 'contact_number'
            );
        $sql = "SELECT `tbl_subClient_create`.*, `tbl_client`.`clnt_first_name`, `tbl_client`.`clnt_last_name`, `tbl_mst_source`.`source_name`, `tbl_mst_industry`.`industry_name`
        FROM `tbl_subClient_create`
        JOIN `tbl_client` ON `tbl_client`.`id`=`tbl_subClient_create`.`client_id`
        JOIN `tbl_mst_source` ON `tbl_subClient_create`.`source`=`tbl_mst_source`.`s_id`
        JOIN `tbl_mst_industry` ON `tbl_subClient_create`.`industry`=`tbl_mst_industry`.`ind_id` where `tbl_subClient_create`.`client_id`='".$clientId."'";

            if (!empty($requestData['search']['value'])) {   // if there is a search parameter, $requestData['search']['value'] contains search parameter
            //$sql.=" AND (client_id LIKE '" . $requestData['search']['value'] . "%') ";
            $sql.=" OR (clnt_first_name LIKE '" . $requestData['search']['value'] . "%' )";
            $sql.=" OR (subclient_name LIKE '" . $requestData['search']['value'] . "%' )";
            $sql.=" OR (contact_number LIKE '" . $requestData['search']['value'] . "%' )";

    //            $sql.=" OR first_name LIKE '" . $requestData['search']['value'] . "%' ";
    //            $sql.=" OR    last_name LIKE '" . $requestData['search']['value'] . "%' ";
                //$sql.=" OR (link LIKE '" . $requestData['search']['value'] . "%' )";
                //$sql.=" OR (logo LIKE '" . $requestData['search']['value'] . "%' )";
        }
        $sql.=" ORDER BY " . $columns[$requestData['order'][0]['column']] . "   " . $requestData['order'][0]['dir'] . "  LIMIT " . $requestData['start'] . " ," . $requestData['length'] . "   ";
        $query = $this->db->query($sql);
        if ($query->result_id->num_rows > 0) {
            return $query->result();
        } else {
            return FALSE;
        }

    }

    // function for packages list for datatable 

    function getPackagesList($requestData, $client_id,$role)
    {

    $date = date('Y-m-d H:i:s');

      $columns = array(
        0 => 'client_id',
        1 => 'package_title',
        2 => 'amount',
            //3 => 'created_date'
        );
//'tbl_package.validity_date >=',date('Y-m-d H:i:s')


      $sql = "select *, tp.package_title,tp.validity_date,tblPerchased.created_date,tblPerchased.amount,job_adds from  tbl_packages_purchased tblPerchased  inner join tbl_package tp on tp.id=tblPerchased.package_id  inner join tbl_job_adds tblja on tp.id=tblja.package_id where tblPerchased.client_id='".$client_id."'  AND   tp.validity_date >= '".$date."' AND tblPerchased.role='".$role."' ";

   //  echo $sql; exit;

           if (!empty($requestData['search']['value'])) {   // if there is a search parameter, $requestData['search']['value'] contains search parameter
           $sql.=" AND package_title LIKE '" . $requestData['search']['value'] . "%' ";
           $sql.=" OR amount LIKE '" . $requestData['search']['value'] . "%' ";
               // $sql.=" OR created_date LIKE '" . $requestData['search']['value'] . "%' ";
    //            $sql.=" OR first_name LIKE '" . $requestData['search']['value'] . "%' ";
    //            $sql.=" OR last_name LIKE '" . $requestData['search']['value'] . "%' ";
                //$sql.=" OR (link LIKE '" . $requestData['search']['value'] . "%' )";
                //$sql.=" OR (logo LIKE '" . $requestData['search']['value'] . "%' )";
       }

    //$sql.=" ORDER BY " . $columns[$requestData['order'][0]['column']] . "   " . $requestData['order'][0]['dir'] . "  LIMIT " . $requestData['start'] . " ," . $requestData['length'] . "   ";
    //echo $sql; exit;
       $query = $this->db->query($sql);
       if ($query->result_id->num_rows > 0) {
        return $query->result();
    } else {
        return FALSE;
    }

    }





       // function subscribed packages detail

    public function getSubsPackages($client_id,$role)
    {

      $query = "select *, tp.package_title,tp.validity_date,tblPerchased.created_date,tblPerchased.amount  from  tbl_packages_purchased tblPerchased  inner join tbl_package tp on tp.id=tblPerchased.package_id  where tblPerchased.client_id='".$client_id."'  AND  tblPerchased.role='".$role."' ";

          //      echo  $query; exit;

      $result= $this->db->query($query);

      $subscribedData = $result->result();

       //  echo "<pre>"; print_r($searchedJobs); exit();

      return $subscribedData;


    }





    // get package list for server site data table subscribed packages


    function get_package_list($client_id,$role)
    {


      $sql = "select *, tp.package_title,tp.validity_date,tblPerchased.created_date,tblPerchased.amount  from  tbl_packages_purchased tblPerchased  inner join tbl_package tp on tp.id=tblPerchased.package_id  where tblPerchased.client_id='".$client_id."'  AND  tblPerchased.role='".$role."' ";

      $query=$this->db->query($sql);
        //echo "<pre>";print_r($query);exit;
      if($query->result_id->num_rows > 0){
       return $query->result();
    }else{
      return false;
    }



    }

    // get subscribed packages total list

    function getSubscribedPackagesfilteredList($requestData, $client_id,$role)
    {




        $sql = "select *, tp.package_title,tp.validity_date,tblPerchased.created_date,tblPerchased.amount  from  tbl_packages_purchased tblPerchased  inner join tbl_package tp on tp.id=tblPerchased.package_id  where tblPerchased.client_id='".$client_id."'  AND  tblPerchased.role='".$role."' ";



           if (!empty($requestData['search']['value'])) {   // if there is a search parameter, $requestData['search']['value'] contains search parameter
           $sql.=" AND package_title LIKE '" . $requestData['search']['value'] . "%' ";
           $sql.=" OR amount LIKE '" . $requestData['search']['value'] . "%' ";
               //  $sql.=" OR created_date LIKE '" . $requestData['search']['value'] . "%' ";
    //            $sql.=" OR first_name LIKE '" . $requestData['search']['value'] . "%' ";
    //            $sql.=" OR last_name LIKE '" . $requestData['search']['value'] . "%' ";
                //$sql.=" OR (link LIKE '" . $requestData['search']['value'] . "%' )";
                //$sql.=" OR (logo LIKE '" . $requestData['search']['value'] . "%' )";
       }
       $query = $this->db->query($sql);
       if ($query->result_id->num_rows > 0) {
        return $query->result();
    } else {
        return FALSE;
    }
    }






    //function for mainjob list
    function get_mainjobs_list($where){
    //echo $where;exit;
        $sql="SELECT `tbl_job_create`.*, `tbl_mst_status`.`status_name`,`tbl_subClient_create`.`subclient_name`,`tbl_mst_currency`.`currency_name` 
        FROM `tbl_job_create`
        LEFT JOIN `tbl_subClient_create` ON `tbl_subClient_create`.`id`=`tbl_job_create`.`parent_client`
        JOIN `tbl_mst_status` ON `tbl_job_create`.`job_status`=`tbl_mst_status`.`status_id`  JOIN `tbl_mst_currency` on `tbl_mst_currency`.`currency_id`=`tbl_job_create`.`currency`";
    //JOIN `tbl_subClient_create`
        $sql.=$where;
    //print_r($sql);exit;
        $query=$this->db->query($sql);
        //echo "<pre>";print_r($query);exit;
        if($query->result_id->num_rows > 0){
           return $query->result();
       }else{
          return false;
      }


    }

   //function for get published jobs at client dashboard
    function get_publshdjobs_list($where){
    //echo $where;exit;
        $sql="SELECT `tbl_job_create`.*, `tbl_mst_status`.`status_name`,`tbl_subClient_create`.`subclient_name`,`tbl_mst_currency`.`currency_name` 
        FROM `tbl_job_create`
        LEFT JOIN `tbl_subClient_create` ON `tbl_subClient_create`.`id`=`tbl_job_create`.`parent_client`
        JOIN `tbl_mst_status` ON `tbl_job_create`.`job_status`=`tbl_mst_status`.`status_id`  JOIN `tbl_mst_currency` on `tbl_mst_currency`.`currency_id`=`tbl_job_create`.`currency`";
    //JOIN `tbl_subClient_create`
        $sql.=$where;
    //print_r($sql);exit;
        $query=$this->db->query($sql);
        //echo "<pre>";print_r($query);exit;
        if($query->result_id->num_rows > 0){
           return $query->result();
       }else{
          return false;
      }


    }


    // for front view
    public function get_appldJobs_list($role,$id)
    {

      //$query " ";



        if($role=='mainclient')
          {
           
 $query="select taj.job_id,taj.user_id,jc.job_title,jc.contact_name,jc.recruiter,jc.positions,jc.role,tbls.status_name,jc.created_date,jc.open_date,jc.max_salary,jc.min_salary, tblc.currency_code,(select count(taj1.job_id) from tbl_user_applied_job taj1 where taj1.job_id=jc.j_id) as counts from tbl_user_applied_job taj left join tbl_job_create jc on taj.job_id=jc.j_id inner join tbl_mst_status tbls on jc.job_status=tbls.status_id inner join tbl_mst_currency tblc on jc.currency=tblc.currency_id where jc.client_id='".$id."' and jc.role='mainclient' ";



          }else{

                
                $query="select taj.job_id,taj.user_id,jc.job_title,jc.contact_name,jc.recruiter,jc.positions,jc.role,tbls.status_name,jc.created_date,jc.open_date,jc.max_salary,jc.min_salary, tblc.currency_code,(select count(taj1.job_id) from tbl_user_applied_job taj1 where taj1.job_id=jc.j_id) as counts from tbl_user_applied_job taj left join tbl_job_create jc on taj.job_id=jc.j_id inner join tbl_mst_status tbls on jc.job_status=tbls.status_id inner join tbl_mst_currency tblc on jc.currency=tblc.currency_id where jc.subClient_id='".$id."' and jc.role='subclient' ";

          }

        //  echo $query;exit;

                 $result=$this->db->query($query);
                  
                  //echo "<pre>"; print_r($result); exit;

                 if($result->result_id->num_rows > 0)
                 {

                   //echo 'here';exit;
                    return $result->result();
                 }else{
                    //echo 'there';exit;
                    return false;
                 }

    }

  // inner view

    public function innerget_appldJobs_list($role,$id)
    {

      //$query " ";



        if($role=='mainclient')
          {
           
 $query="select taj.job_id,taj.user_id,jc.job_title,jc.contact_name,jc.recruiter,jc.positions,jc.role,tbls.status_name,jc.created_date,jc.open_date,jc.max_salary,jc.min_salary, tblc.currency_code,(select count(taj1.job_id) from tbl_user_applied_job taj1 where taj1.job_id=jc.j_id) as counts from tbl_user_applied_job taj left join tbl_job_create jc on taj.job_id=jc.j_id inner join tbl_mst_status tbls on jc.job_status=tbls.status_id inner join tbl_mst_currency tblc on jc.currency=tblc.currency_id where jc.client_id='".$id."' and jc.role='mainclient' ";



          }else{

                
                $query="select taj.job_id,taj.user_id,jc.job_title,jc.contact_name,jc.recruiter,jc.positions,jc.role,tbls.status_name,jc.created_date,jc.open_date,jc.max_salary,jc.min_salary, tblc.currency_code,(select count(taj1.job_id) from tbl_user_applied_job taj1 where taj1.job_id=jc.j_id) as counts from tbl_user_applied_job taj left join tbl_job_create jc on taj.job_id=jc.j_id inner join tbl_mst_status tbls on jc.job_status=tbls.status_id inner join tbl_mst_currency tblc on jc.currency=tblc.currency_id where jc.subClient_id='".$id."' and jc.role='subclient' ";

          }

        //  echo $query;exit;

                 $result=$this->db->query($query);
                  
                  //echo "<pre>"; print_r($result); exit;

                 if($result->result_id->num_rows > 0)
                 {

                   //echo 'here';exit;
                    return $result->result();
                 }else{
                    //echo 'there';exit;
                    return false;
                 }

    }





    //function for main job filtered list
    function getmainjobsfilteredList($requestData,$where){

        $sql = "SELECT `tbl_job_create`.*, `tbl_mst_status`.`status_name`,`tbl_subClient_create`.`subclient_name`,`tbl_mst_currency`.`currency_name` 
        FROM `tbl_job_create`
        LEFT JOIN `tbl_subClient_create` ON `tbl_subClient_create`.`id`=`tbl_job_create`.`parent_client`
        JOIN `tbl_mst_status` ON `tbl_job_create`.`job_status`=`tbl_mst_status`.`status_id`  JOIN `tbl_mst_currency` on `tbl_mst_currency`.`currency_id`=`tbl_job_create`.`currency`";

        $sql.=$where;
     //$sql.=" WHERE `tbl_client`.`client_id` = '".$clientId."'";
            if (!empty($requestData['search']['value'])) {   // if there is a search parameter, $requestData['search']['value'] contains search parameter
            $sql.=" AND job_title LIKE '" . $requestData['search']['value'] . "%' ";
            $sql.=" OR job_status LIKE '" . $requestData['search']['value'] . "%' ";
            $sql.=" OR recruiter LIKE '" . $requestData['search']['value'] . "%' ";
    //            $sql.=" OR first_name LIKE '" . $requestData['search']['value'] . "%' ";
    //            $sql.=" OR last_name LIKE '" . $requestData['search']['value'] . "%' ";
                //$sql.=" OR (link LIKE '" . $requestData['search']['value'] . "%' )";
                //$sql.=" OR (logo LIKE '" . $requestData['search']['value'] . "%' )";
        }
        $query = $this->db->query($sql);
        if ($query->result_id->num_rows > 0) {
            return $query->result();
        } else {
            return FALSE;
        }


    }

     //function for main job filtered list
    function getpublshdjobsfilteredList($requestData,$where){

        $sql = "SELECT `tbl_job_create`.*, `tbl_mst_status`.`status_name`,`tbl_subClient_create`.`subclient_name`,`tbl_mst_currency`.`currency_name` 
        FROM `tbl_job_create`
        LEFT JOIN `tbl_subClient_create` ON `tbl_subClient_create`.`id`=`tbl_job_create`.`parent_client`
        JOIN `tbl_mst_status` ON `tbl_job_create`.`job_status`=`tbl_mst_status`.`status_id`  JOIN `tbl_mst_currency` on `tbl_mst_currency`.`currency_id`=`tbl_job_create`.`currency`";


        



        $sql.=$where;
     //$sql.=" WHERE `tbl_client`.`client_id` = '".$clientId."'";
            if (!empty($requestData['search']['value'])) {   // if there is a search parameter, $requestData['search']['value'] contains search parameter
            $sql.=" AND job_title LIKE '" . $requestData['search']['value'] . "%' ";
            $sql.=" OR job_status LIKE '" . $requestData['search']['value'] . "%' ";
            $sql.=" OR recruiter LIKE '" . $requestData['search']['value'] . "%' ";
    //            $sql.=" OR first_name LIKE '" . $requestData['search']['value'] . "%' ";
    //            $sql.=" OR last_name LIKE '" . $requestData['search']['value'] . "%' ";
                //$sql.=" OR (link LIKE '" . $requestData['search']['value'] . "%' )";
                //$sql.=" OR (logo LIKE '" . $requestData['search']['value'] . "%' )";
        }
        $query = $this->db->query($sql);
        if ($query->result_id->num_rows > 0) {
            return $query->result();
        } else {
            return FALSE;
        }


    }
    // function for get apllied filtered jobs
    
    public function getAppliedjobsfilteredList($requestData,$where,$id,$role)
    {

             if($role=='mainclient')
          {
           
             $sql="select taj.job_id,taj.user_id,jc.job_title,jc.contact_name,jc.recruiter,jc.positions,jc.role,tbls.status_name,jc.created_date,jc.open_date,jc.max_salary,jc.min_salary,cities.name as city,countries.name as country, tblc.currency_code,tblu.first_name,tblu.last_name,(select count(taj1.job_id) from tbl_user_applied_job taj1 where taj1.job_id=jc.j_id) as counts from tbl_user_applied_job
 taj left join tbl_job_create jc on taj.job_id=jc.j_id inner join tbl_mst_status tbls on jc.job_status=tbls.status_id  inner join cities on jc.city=cities.id inner join countries on jc.nationality=countries.id inner join tbl_user tblu on taj.user_id=tblu.id inner join tbl_mst_currency tblc on jc.currency=tblc.currency_id where jc.client_id='".$id."' and jc.role='mainclient' ";

//   echo $sql; exit;



          }else{

                
                $sql="select taj.job_id,taj.user_id,jc.job_title,jc.contact_name,jc.recruiter,jc.positions,jc.role,tbls.status_name,jc.created_date,jc.open_date,jc.max_salary,jc.min_salary,cities.name as city,countries.name as country, tblc.currency_code,tblu.first_name,tblu.last_name,(select count(taj1.job_id) from tbl_user_applied_job taj1 where taj1.job_id=jc.j_id) as counts from tbl_user_applied_job
 taj left join tbl_job_create jc on taj.job_id=jc.j_id inner join tbl_mst_status tbls on jc.job_status=tbls.status_id inner join cities on jc.city=cities.id inner join countries on jc.nationality=countries.id inner join tbl_user tblu on taj.user_id=tblu.id inner join tbl_mst_currency tblc on jc.currency=tblc.currency_id where jc.subClient_id='".$id."' and jc.role='subclient' ";

          }
       //   echo $sql; exit;
 
             if (!empty($requestData['search']['value'])) {   // if there is a search parameter, $requestData['search']['value'] contains search parameter
            $sql.=" AND job_title LIKE '" . $requestData['search']['value'] . "%' ";
            $sql.=" OR job_status LIKE '" . $requestData['search']['value'] . "%' ";
            $sql.=" OR recruiter LIKE '" . $requestData['search']['value'] . "%' ";
    //            $sql.=" OR first_name LIKE '" . $requestData['search']['value'] . "%' ";
    //            $sql.=" OR last_name LIKE '" . $requestData['search']['value'] . "%' ";
                //$sql.=" OR (link LIKE '" . $requestData['search']['value'] . "%' )";
                //$sql.=" OR (logo LIKE '" . $requestData['search']['value'] . "%' )";
        }
       
        $query = $this->db->query($sql);
        if ($query->result_id->num_rows > 0) {
            return $query->result();
        } else {
            return FALSE;
        }

    }
   
   // for inner view

     public function innergetAppliedjobsfilteredList($requestData,$where,$id,$role)
    {

             if($role=='mainclient')
          {
           
             $sql="select taj.job_id,taj.user_id,jc.job_title,jc.contact_name,jc.recruiter,jc.positions,jc.role,tbls.status_name,jc.created_date,jc.open_date,jc.max_salary,jc.min_salary,cities.name as city,countries.name as country, tblc.currency_code,tblu.first_name,tblu.last_name,(select count(taj1.job_id) from tbl_user_applied_job taj1 where taj1.job_id=jc.j_id) as counts from tbl_user_applied_job
 taj left join tbl_job_create jc on taj.job_id=jc.j_id inner join tbl_mst_status tbls on jc.job_status=tbls.status_id  inner join cities on jc.city=cities.id inner join countries on jc.nationality=countries.id inner join tbl_user tblu on taj.user_id=tblu.id inner join tbl_mst_currency tblc on jc.currency=tblc.currency_id where jc.client_id='".$id."' and jc.role='mainclient' ";
       

       echo $sql; exit;


          }else{

                
                $sql="select taj.job_id,taj.user_id,jc.job_title,jc.contact_name,jc.recruiter,jc.positions,jc.role,tbls.status_name,jc.created_date,jc.open_date,jc.max_salary,jc.min_salary,cities.name as city,countries.name as country, tblc.currency_code,tblu.first_name,tblu.last_name,(select count(taj1.job_id) from tbl_user_applied_job taj1 where taj1.job_id=jc.j_id) as counts from tbl_user_applied_job
 taj left join tbl_job_create jc on taj.job_id=jc.j_id inner join tbl_mst_status tbls on jc.job_status=tbls.status_id inner join cities on jc.city=cities.id inner join countries on jc.nationality=countries.id inner join tbl_user tblu on taj.user_id=tblu.id inner join tbl_mst_currency tblc on jc.currency=tblc.currency_id where jc.subClient_id='".$id."' and jc.role='subclient' ";

          }
          echo $sql;exit;
 
             if (!empty($requestData['search']['value'])) {   // if there is a search parameter, $requestData['search']['value'] contains search parameter
            $sql.=" AND job_title LIKE '" . $requestData['search']['value'] . "%' ";
            $sql.=" OR job_status LIKE '" . $requestData['search']['value'] . "%' ";
            $sql.=" OR recruiter LIKE '" . $requestData['search']['value'] . "%' ";
    //            $sql.=" OR first_name LIKE '" . $requestData['search']['value'] . "%' ";
    //            $sql.=" OR last_name LIKE '" . $requestData['search']['value'] . "%' ";
                //$sql.=" OR (link LIKE '" . $requestData['search']['value'] . "%' )";
                //$sql.=" OR (logo LIKE '" . $requestData['search']['value'] . "%' )";
        }
       
        $query = $this->db->query($sql);
        if ($query->result_id->num_rows > 0) {
            return $query->result();
        } else {
            return FALSE;
        }

    }





    //function for fetching main job list
    function getmainjobsList($requestData,$where){

        $columns = array(
            0 => 'j_id',
            1 => 'job_title',
            2 => 'open_date',
            3 => 'job_status',
            4 =>'subclient_name',
            5 =>'recruiter'
            );
        $sql = "SELECT `tbl_job_create`.*, `tbl_mst_status`.`status_name`,`tbl_subClient_create`.`subclient_name`,`tbl_mst_currency`.`currency_name` 
        FROM `tbl_job_create`
        LEFT JOIN `tbl_subClient_create` ON `tbl_subClient_create`.`id`=`tbl_job_create`.`parent_client`
        JOIN `tbl_mst_status` ON `tbl_job_create`.`job_status`=`tbl_mst_status`.`status_id`  JOIN `tbl_mst_currency` on `tbl_mst_currency`.`currency_id`=`tbl_job_create`.`currency`";
        $sql.=$where;

            if (!empty($requestData['search']['value'])) {   // if there is a search parameter, $requestData['search']['value'] contains search parameter
            //$sql.=" AND (client_id LIKE '" . $requestData['search']['value'] . "%') ";
            $sql.=" OR (job_title LIKE '" . $requestData['search']['value'] . "%' )";
            $sql.=" OR (job_status LIKE '" . $requestData['search']['value'] . "%' )";
            $sql.=" OR (recruiter LIKE '" . $requestData['search']['value'] . "%' )";

    //            $sql.=" OR first_name LIKE '" . $requestData['search']['value'] . "%' ";
    //            $sql.=" OR    last_name LIKE '" . $requestData['search']['value'] . "%' ";
                //$sql.=" OR (link LIKE '" . $requestData['search']['value'] . "%' )";
                //$sql.=" OR (logo LIKE '" . $requestData['search']['value'] . "%' )";
        }
        $sql.=" ORDER BY " . $columns[$requestData['order'][0]['column']] . "   " . $requestData['order'][0]['dir'] . "  LIMIT " . $requestData['start'] . " ," . $requestData['length'] . "   ";
    //echo $sql;exit;
        $query = $this->db->query($sql);
        if ($query->result_id->num_rows > 0) {
            return $query->result();
        } else {
            return FALSE;
        }



    }

    
    //function for fetching pblished job list
    function getpblishedjobsList($requestData,$where){

        $columns = array(
            0 => 'j_id',
            1 => 'job_title',
            2 => 'open_date',
            3 => 'job_status',
            4 =>'subclient_name',
            5 =>'recruiter'
            );
        $sql = "SELECT `tbl_job_create`.*, `tbl_mst_status`.`status_name`,`tbl_subClient_create`.`subclient_name`,`tbl_mst_currency`.`currency_name` 
        FROM `tbl_job_create`
        LEFT JOIN `tbl_subClient_create` ON `tbl_subClient_create`.`id`=`tbl_job_create`.`parent_client`
        JOIN `tbl_mst_status` ON `tbl_job_create`.`job_status`=`tbl_mst_status`.`status_id`  JOIN `tbl_mst_currency` on `tbl_mst_currency`.`currency_id`=`tbl_job_create`.`currency`";
        $sql.=$where;

            if (!empty($requestData['search']['value'])) {   // if there is a search parameter, $requestData['search']['value'] contains search parameter
            //$sql.=" AND (client_id LIKE '" . $requestData['search']['value'] . "%') ";
            $sql.=" OR (job_title LIKE '" . $requestData['search']['value'] . "%' )";
            $sql.=" OR (job_status LIKE '" . $requestData['search']['value'] . "%' )";
            $sql.=" OR (recruiter LIKE '" . $requestData['search']['value'] . "%' )";

    //            $sql.=" OR first_name LIKE '" . $requestData['search']['value'] . "%' ";
    //            $sql.=" OR    last_name LIKE '" . $requestData['search']['value'] . "%' ";
                //$sql.=" OR (link LIKE '" . $requestData['search']['value'] . "%' )";
                //$sql.=" OR (logo LIKE '" . $requestData['search']['value'] . "%' )";
        }
        $sql.=" ORDER BY " . $columns[$requestData['order'][0]['column']] . "   " . $requestData['order'][0]['dir'] . "  LIMIT " . $requestData['start'] . " ," . $requestData['length'] . "   ";
    //echo $sql;exit;
        $query = $this->db->query($sql);
        if ($query->result_id->num_rows > 0) {
            return $query->result();
        } else {
            return FALSE;
        }



    }
 
 // function for get applied list

  public function  getAppliedjobsList($requestData,$where,$id,$role)
  {
     $columns = array(
            0 => 'j_id',
            1 => 'job_title',
            2 => 'open_date',
            3 => 'job_status',
            4 =>'subclient_name',
            5 =>'recruiter'
            );

       if($role=='mainclient')
          {
           
             $sql="select taj.job_id,taj.user_id,jc.job_title,jc.contact_name,jc.recruiter,jc.positions,jc.role,tbls.status_name,jc.created_date,jc.open_date,jc.max_salary,jc.min_salary, tblc.currency_code,(select count(taj1.job_id) from tbl_user_applied_job taj1 where taj1.job_id=jc.j_id) as counts from tbl_user_applied_job
 taj left join tbl_job_create jc on taj.job_id=jc.j_id inner join tbl_mst_status tbls on jc.job_status=tbls.status_id inner join tbl_mst_currency tblc on jc.currency=tblc.currency_id where jc.client_id='".$id."' and jc.role='mainclient' 
 group by taj.job_id";

              }else{
               $sql="select taj.job_id,taj.user_id,jc.job_title,jc.contact_name,jc.recruiter,jc.positions,jc.role,tbls.status_name,jc.created_date,jc.open_date,jc.max_salary,jc.min_salary, tblc.currency_code,(select count(taj1.job_id) from tbl_user_applied_job taj1 where taj1.job_id=jc.j_id) as counts from tbl_user_applied_job
 taj left join tbl_job_create jc on taj.job_id=jc.j_id inner join tbl_mst_status tbls on jc.job_status=tbls.status_id inner join tbl_mst_currency tblc on jc.currency=tblc.currency_id where jc.subClient_id='".$id."' and jc.role='subclient' 
 group by taj.job_id";

          }

         if (!empty($requestData['search']['value'])) {   // if there is a search parameter, $requestData['search']['value'] contains search parameter
            //$sql.=" AND (client_id LIKE '" . $requestData['search']['value'] . "%') ";
            $sql.=" OR (job_title LIKE '" . $requestData['search']['value'] . "%' )";
            $sql.=" OR (job_status LIKE '" . $requestData['search']['value'] . "%' )";
            $sql.=" OR (recruiter LIKE '" . $requestData['search']['value'] . "%' )";

    //            $sql.=" OR first_name LIKE '" . $requestData['search']['value'] . "%' ";
    //            $sql.=" OR    last_name LIKE '" . $requestData['search']['value'] . "%' ";
                //$sql.=" OR (link LIKE '" . $requestData['search']['value'] . "%' )";
                //$sql.=" OR (logo LIKE '" . $requestData['search']['value'] . "%' )";
        }
        $sql.=" ORDER BY " . $columns[$requestData['order'][0]['column']] . "   " . $requestData['order'][0]['dir'] . "  LIMIT " . $requestData['start'] . " ," . $requestData['length'] . "   ";
    // echo $sql;exit;
        $query = $this->db->query($sql);
        if ($query->result_id->num_rows > 0) {
            return $query->result();
        } else {
            return FALSE;
        }

  }




    //function for suboffices jobs
    function get_subOfficejobs_list($string){

        $sql="SELECT `tbl_job_create`.*, `tbl_mst_status`.`status_name`,`tbl_subClient_create`.`subclient_name`,`tbl_mst_job_type`.`job_type`,`tbl_mst_currency`.`currency_name`
        FROM `tbl_job_create`
        LEFT JOIN `tbl_subClient_create` ON `tbl_subClient_create`.`id`=`tbl_job_create`.`parent_client`
        JOIN `tbl_mst_status` ON `tbl_job_create`.`job_status`=`tbl_mst_status`.`status_id`
        JOIN  `tbl_mst_job_type` ON `tbl_mst_job_type`.`id`=`tbl_job_create`.`job_type`
        JOIN  `tbl_mst_currency` ON `tbl_mst_currency`.`currency_id`=`tbl_job_create`.`currency`
        WHERE `tbl_job_create`.`subClient_id` IN (".$string.")";
        $query=$this->db->query($sql);
        //echo "<pre>";print_r($query);exit;
        if($query->result_id->num_rows > 0){
           return $query->result();
       }else{
          return false;
      }


    }

    //function for filtering suboffices jobs
    function getsubOfficejobsfilteredList($requestData,$string){

        $sql = "SELECT `tbl_job_create`.*, `tbl_mst_status`.`status_name`,`tbl_subClient_create`.`subclient_name`,`tbl_mst_job_type`.`job_type`,`tbl_mst_currency`.`currency_name`
        FROM `tbl_job_create`
        LEFT JOIN `tbl_subClient_create` ON `tbl_subClient_create`.`id`=`tbl_job_create`.`parent_client`
        JOIN `tbl_mst_status` ON `tbl_job_create`.`job_status`=`tbl_mst_status`.`status_id`
        JOIN  `tbl_mst_job_type` ON `tbl_mst_job_type`.`id`=`tbl_job_create`.`job_type`
        JOIN  `tbl_mst_currency` ON `tbl_mst_currency`.`currency_id`=`tbl_job_create`.`currency`
        WHERE `tbl_job_create`.`subClient_id` IN (".$string.")";
     //$sql.=" WHERE `tbl_client`.`client_id` = '".$clientId."'";
            if (!empty($requestData['search']['value'])) {   // if there is a search parameter, $requestData['search']['value'] contains search parameter
            $sql.=" AND job_title LIKE '" . $requestData['search']['value'] . "%' ";
            $sql.=" OR job_status LIKE '" . $requestData['search']['value'] . "%' ";
            $sql.=" OR recruiter LIKE '" . $requestData['search']['value'] . "%' ";
    //            $sql.=" OR first_name LIKE '" . $requestData['search']['value'] . "%' ";
    //            $sql.=" OR last_name LIKE '" . $requestData['search']['value'] . "%' ";
                //$sql.=" OR (link LIKE '" . $requestData['search']['value'] . "%' )";
                //$sql.=" OR (logo LIKE '" . $requestData['search']['value'] . "%' )";
        }
        $query = $this->db->query($sql);
        if ($query->result_id->num_rows > 0) {
            return $query->result();
        } else {
            return FALSE;
        }



    }

    //function for suboffice list
    function getsubOfficejobsList($requestData,$string){
        //echo $string;exit;
        $columns = array(
            0 => 'j_id',
            1 => 'job_title',
            2 => 'open_date',
            3 => 'job_status',
            4 =>'parent_client',
            5 =>'recruiter'
            );
        $sql = "SELECT `tbl_job_create`.*, `tbl_mst_status`.`status_name`,`tbl_subClient_create`.`subclient_name`,`tbl_mst_job_type`.`job_type`,`tbl_mst_currency`.`currency_name`
        FROM `tbl_job_create`
        LEFT JOIN `tbl_subClient_create` ON `tbl_subClient_create`.`id`=`tbl_job_create`.`parent_client`
        JOIN `tbl_mst_status` ON `tbl_job_create`.`job_status`=`tbl_mst_status`.`status_id`
        JOIN  `tbl_mst_job_type` ON `tbl_mst_job_type`.`id`=`tbl_job_create`.`job_type`
        JOIN  `tbl_mst_currency` ON `tbl_mst_currency`.`currency_id`=`tbl_job_create`.`currency`
        WHERE `tbl_job_create`.`subClient_id` IN (".$string.")";
       //echo $sql;exit;
            if (!empty($requestData['search']['value'])) {   // if there is a search parameter, $requestData['search']['value'] contains search parameter
            //$sql.=" AND (client_id LIKE '" . $requestData['search']['value'] . "%') ";
            $sql.=" OR (job_title LIKE '" . $requestData['search']['value'] . "%' )";
            $sql.=" OR (job_status LIKE '" . $requestData['search']['value'] . "%' )";
            $sql.=" OR (recruiter LIKE '" . $requestData['search']['value'] . "%' )";

    //            $sql.=" OR first_name LIKE '" . $requestData['search']['value'] . "%' ";
    //            $sql.=" OR    last_name LIKE '" . $requestData['search']['value'] . "%' ";
                //$sql.=" OR (link LIKE '" . $requestData['search']['value'] . "%' )";
                //$sql.=" OR (logo LIKE '" . $requestData['search']['value'] . "%' )";
        }
        $sql.=" ORDER BY " . $columns[$requestData['order'][0]['column']] . "   " . $requestData['order'][0]['dir'] . "  LIMIT " . $requestData['start'] . " ," . $requestData['length'] . "   ";
        $query = $this->db->query($sql);
        if ($query->result_id->num_rows > 0) {
            return $query->result();
        } else {
            return FALSE;
        }


        
    }

    //function for checking old password
    function check_old_password($client_id, $password){

        $result=$this->db->select('*')
        ->from('tbl_client')
        ->where('id',$client_id)
        ->where('clnt_password',$password)
        ->get()->row();
        return $result;



    }

    //function for updating client password..

    function change_password($client_id, $updateArr){
        $result=$this->db->where('id',$client_id)->update('tbl_client',$updateArr);
        return $result;

    }

    //function for fetching sub client details
    function getSubClientDetails($id){
        $result=$this->db->select('tbl_subClient_create.*,tbl_client.clnt_email,tbl_client.clnt_password')
        ->from('tbl_subClient_create')
        ->join('tbl_client','tbl_client.subClient_id=tbl_subClient_create.id')
        ->where('tbl_subClient_create.id',$id)
        ->get()->row();
        return $result;

    }

    //functtion for fetching all job type
    function getJobType(){
        $result=$this->db->select('*')
        ->from('tbl_mst_job_type')
        ->get()->result();
        return $result;

    }

    //function for fetching skills
    function getSkills(){
        $result=$this->db->select('*')
        ->from('tbl_mst_skill')
        ->get()->result();
        $array = array();
        if(isset($result) && !empty($result)){
            foreach ($result as $key => $value) {
            # code...
                $array[$value->id] = $value->skill_name; 
            }
        }
        return $array;

    }

    //function for inserting profile data
    function insertProfileData($insertProfile){
        $result=$this->db->insert('tbl_company_profile',$insertProfile);
        return $result;

    }

    //function for saving company logo
    function saveCompanyLogo($insertArr){
        $result=$this->db->insert('tbl_company_logo',$insertArr);
        return $result;

    }

    //fucntion for fetching company logo
    function getCompanyLogo($client_id){
        $result=$this->db->select('*')
        ->from('tbl_company_logo')
        ->where('client_id',$client_id)
        ->get()->row();
        return $result;
    }

    //function for updating company logo
    function updateCompanyLogo($updateArr,$client_id){
        $result=$this->db->where('client_id',$client_id)->update('tbl_company_logo',$updateArr);
        return $result;
    }  
    //function for fetching company details
    function getCompanyDetails($client_id){
        $result=$this->db->select('*')
        ->from('tbl_company_profile')
        ->where('client_id',$client_id)
        ->get()->row();
        return $result;

    }

    //function for updating client profile data
    function updateProfileData($insertProfile,$client_id){
        $result=$this->db->where('client_id',$client_id)->update('tbl_company_profile',$insertProfile);
        return $result;

    }

    //function for inserting client information 
    function insertClientInformation($insertClientInfo){
        $result=$this->db->insert('tbl_client_social_and_contact_info',$insertClientInfo);
        return $result;

    }

    //function for fetching client information
    function fetchClientData($client_id){
        $result=$this->db->select('*')
        ->from('tbl_client_social_and_contact_info')
        ->get()->row();
        return $result;
    }

    //function for updating client info
    function updateClientInformation($insertClientInfo,$client_id){
        $result=$this->db->where('client_id',$client_id)->update('tbl_client_social_and_contact_info',$insertClientInfo);
        return $result;
    }

    //function for fetching banner details
    function getBannerDetails($client_id){
        $result=$this->db->select('*')
        ->from('tbl_banner')
        ->where('client_id',$client_id)
        ->get()->row();
        return $result;
    }

    //function for inserting banner data
    function insertBannerData($insert_arr){
        $result=$this->db->insert('tbl_banner',$insert_arr);
        return $result;
    }

    //function for updating banner data
    function updateBannerData($insert_arr,$client_id){
        $result=$this->db->where('client_id',$client_id)->update('tbl_banner',$insert_arr);
        return $result;

    }

    //function for fetching currencies
    function getCurrencies(){
        $result=$this->db->select('*')
        ->from('tbl_mst_currency')
        ->get()->result();
        return $result;


    }

    //function for fetching reguster client data
    function checkRegisterData($company_name){
        $result=$this->db->select('*')
        ->from('tbl_client')
        ->where('company_name',$company_name)
        ->get()->row();
        return $result;

    }




     // fuction for save txn detail of subscribe package

    function savePackageTxnDetail($txnDeatil)
    {

        $this->db->insert('tbl_transaction_detail',$txnDeatil);

        $last_insert_id = $this->db->insert_id();
        return $last_insert_id;




    }

    // function for get saved detail

    function getSavedtxnDetail($lastId)
    {

       $savedDetail = $this->db->select('*')
       ->from('tbl_transaction_detail')
       ->where('id',$lastId)
       ->get()->row();


       return  $savedDetail;
    }

     // function for get subscribed package detail

    function getSubscribedPackages($client_id,$role,$id)
    {


       if($role=='mainclient')
       {


           $packagesDetails = $this->db->select('*')
           ->from('tbl_transaction_detail')
           ->where('client_id',$client_id)
           ->where('txn_status','Y')
           ->where('role','mainclient')
           ->where('package_id',$id)
           ->get()->row();

           return $packagesDetails;
       }
       
       if($role=='subclient')
       {

         $packagesDetails = $this->db->select('*')
         ->from('tbl_transaction_detail')
         ->where('client_id',$client_id)
         ->where('txn_status','Y')
         ->where('role','subclient')
         ->where('package_id',$id)
         ->get()->row();

         return $packagesDetails;

     }




    }


    // function for get sub client id from sub client table

    function getSubClient($subClientId)
    {

       $subClientDetails = $this->db->select('*')
       ->from('tbl_subClient_create')
       ->where('id',$subClientId)
       ->get()->row();

       return $subClientDetails;

    }

     //function for showing services of packages

    public function get_service() {
      //$this->db->where('status', 1);
      $query = $this->db->get('tbl_service');
      $out = array();
      
      if ($query->num_rows() > 0) 
      {
          $result = $query->result();
          foreach ($result as $grp)$out[$grp->id] = $grp->service_name
            ;
    }
    return $out;
    }

    // function for save purchase detail

    public function savePurchaseDetail($purchasedRecord)
    {

       $saveDetails = $this->db->insert('tbl_packages_purchased',$purchasedRecord);

       return $saveDetails;

    }


// function get last saved job for preview

    public function getLastSavedJob($lastJobId)
    {


      /* $lastJob = $this->db->select('*')
                           ->from('tbl_job_create')
                           ->where('j_id',$lastJobId)
                           ->get()->row();


                           return $lastJob;*/
                
  $result=$this->db->select('tbl_job_create.*,tbl_mst_countries.id,tbl_mst_status.status_id,tbl_mst_experience.ex_id,tbl_subClient_create.subclient_name,tbl_mst_currency.currency_id,tbl_mst_countries.country_name,tbl_mst_status.status_name,tbl_mst_currency.currency_code,countries.name as countryName,states.name as statename,cities.name as cityName')
        ->from('tbl_job_create')
        ->join('tbl_mst_countries','tbl_job_create.nationality=tbl_mst_countries.id')
        ->join('tbl_mst_currency','tbl_mst_currency.currency_id=tbl_job_create.currency')
        ->join('tbl_mst_status','tbl_job_create.job_status=tbl_mst_status.status_id')
        ->join('tbl_mst_experience','tbl_job_create.experience=tbl_mst_experience.ex_id')
        ->join('countries','tbl_job_create.nationality=countries.id')
        ->join('states','tbl_job_create.state=states.id')
        ->join('cities','tbl_job_create.city=cities.id')
                     //->join('tbl_mst_skill','tbl_job_create.skills=tbl_mst_skill.id')
        ->join('tbl_subClient_create','tbl_subClient_create.id=tbl_job_create.parent_client','left')
        ->where('tbl_job_create.j_id',$lastJobId)
        ->get()->row();
        //   print_r($this->db->last_query()); exit;
/*
     $query = "SELECT tbl_job_create.*, tbl_mst_countries.id, tbl_mst_status.status_id, tbl_mst_experience.ex_id, tbl_mst_job_type.id as jobtype_id, tbl_subClient_create.subclient_name, tbl_mst_currency.currency_id,tbl_mst_countries.country_name,tbl_mst_status.status_name FROM tbl_job_create JOIN tbl_mst_countries ON tbl_job_create.nationality=tbl_mst_countries.id JOIN tbl_mst_currency ON tbl_mst_currency.currency_id=tbl_job_create.currency JOIN tbl_mst_status ON tbl_job_create.job_status=tbl_mst_status.status_id JOIN tbl_mst_experience ON tbl_job_create.experience=tbl_mst_experience.ex_id JOIN tbl_mst_job_type ON tbl_job_create.job_type=tbl_mst_job_type.id LEFT JOIN tbl_subClient_create ON tbl_subClient_create.id=tbl_job_create.parent_client WHERE tbl_job_create.j_id = '".$lastJobId."'";

        $result= $this->db->query($query);
          
          return $result->row();*/

 //print_r($this->db->last_query()); exit;


        return $result;

    }

    // get parent client Id 

     function getParentClientForCreateJob($id)
     {
         
          $logo = $this->db->select('*')
                           ->from('tbl_subClient_create')
                           ->where('id',$id)
                           ->get()->row();


                           return $logo;

     }
     // company logo for create job
        
 public function getComapanyLogoForCreateJob($parentId)
     {
        $parentData= $this->db->select('*')
                              ->from('tbl_company_logo')
                              ->where('client_id',$parentId)
                              ->get()->row();

                              return $parentData;



       }


       // function for get jobType name

       public function geJobTypeName($k)
       {
      
        /* $data = $this->db->select('job_type')
                          ->from('tbl_mst_job_type')
                          ->where_in('id',str_replace(" ' ",'',$k))
                          ->get()->result();*/

                       $query=  "SELECT `job_type` FROM `tbl_mst_job_type` WHERE `id` IN ($k)"; 
                    
                     $result = $this->db->query($query);

                     return $result->result();

    //print_r($this->db->last_query()); exit;
                        //  return $data;

       }

       // function for get saved hours

       function getHours($hrsArr)
       {

       $query = "SELECT `hours` FROM `tbl_mst_hours` WHERE `id` IN ($hrsArr)";
       $result = $this->db->query($query);

                     return $result->result();
       
       }

       // function for get saved shifts 
      
      function getShifts($shftsArr)
      {
         
         $query = "SELECT `shifts` FROM `tbl_mst_shifts` WHERE `id` IN ($shftsArr)";
       $result = $this->db->query($query);

                     return $result->result();

      }

      // function for get saved contract

      function getContract($contrsctArr)
      {
       
          $query = "SELECT `contract_type` FROM `tbl_mst_contract_type` WHERE `id` IN ($contrsctArr)";
       $result = $this->db->query($query);

                     return $result->result();

      } 



  // function for  get countries

       public function getCountries()
       {

          $countries = $this->db->select('*')
                                ->from('countries')
                                ->get()->result();

                                return $countries;


       }

        //function for fetching hours
              function fetchHours(){
                $result=$this->db->select('*')
                                 ->from('tbl_mst_hours')
                                 ->get()->result();
                                 return $result;

              }

              //function for fetching shifts
              function fetchShifts(){
                 $result=$this->db->select('*')
                                 ->from('tbl_mst_shifts')
                                 ->get()->result();
                                 return $result;


              }

              //function for fetching contract type
              function fetchContractType(){
               $result=$this->db->select('*')
                                 ->from('tbl_mst_contract_type')
                                 ->get()->result();
                                 return $result;


              }

              // function for  getStates
             function getStates($country_id)
             {
               
               $states = $this->db->select('*')
                                 ->from('states')
                                 ->where('country_id',$country_id)
                                 ->get()->result();

                                 return $states;

             }

             // function for getCities

             function getCities($state_id)
             {
                $cities = $this->db->select('*')
                                    ->from('cities')
                                    ->where('state_id',$state_id)
                                    ->get()->result();

                                    return $cities;

             }


          // function for publish job
          
          function publishJob($array,$job_id,$id,$role)
          {
                
                $publishJob = $this->db->where('j_id',$job_id)
                                       ->update('tbl_job_create',$array);

                                  //     print_r($this->db->last_query()); exit;

                                       return $publishJob;

          } 

          // function for get published jobs

          public function getPublishedJobs($id,$role)
          {
             
                 if($role=='mainclient')
                 {
                  //  echo 'if'; exit;

                     $publishedJobs = $this->db->select('*')
                                      ->from('tbl_job_create')
                                      ->where('client_id',$id)
                                      ->where('role',$role)
                                      ->where('job_publish',1)
                                      ->get()->result();
                            
                          //  print_r($this->db->last_query()); exit;
                                      return $publishedJobs;
                 }else{

                       // echo $role; exit;
                        $publishedJobs = $this->db->select('*')
                                      ->from('tbl_job_create')
                                      ->where('subClient_id',$id)
                                      ->where('role',$role)
                                      ->where('job_publish',1)
                                      ->get()->result();

                                      return $publishedJobs;

                 }

            

          }  

 // function for get users job applications
  
  public function getUsersJobApplications($id,$role)
    {
         
         //$jobApplications = $this->db->select

        if($role=='mainclient')
          {
           
             $query="select taj.job_id,taj.user_id,jc.job_title,jc.contact_name,jc.recruiter,jc.positions,jc.role, (select count(taj1.job_id) from tbl_user_applied_job taj1 where taj1.job_id=jc.j_id) as counts from tbl_user_applied_job taj left join tbl_job_create jc on taj.job_id=jc.j_id where jc.client_id='".$id."' and jc.role='mainclient' group by taj.job_id";



          }else{

                
                $query="select taj.job_id,taj.user_id,jc.job_title,jc.contact_name,jc.recruiter,jc.positions,jc.role, (select count(taj1.job_id) from tbl_user_applied_job taj1 where taj1.job_id=jc.j_id) as counts from tbl_user_applied_job taj left join tbl_job_create jc on taj.job_id=jc.j_id where jc.subClient_id='".$id."' and jc.role='subclient' group by taj.job_id";

          }

        //  echo $query; exit;
           $result=$this->db->query($query);

         $appliedJobs = $result->result();
            
            return $appliedJobs;
      
    }
 
 

    }

    ?>

