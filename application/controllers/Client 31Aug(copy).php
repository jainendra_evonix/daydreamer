  <?php 
  ob_start();
  defined('BASEPATH') OR exit('No direct script access allowed');

  class Client extends CI_Controller {

    /**
     * Index Page for this controller.
     *
     * Maps to the following URL
     *    http://example.com/index.php/welcome
     *  - or -
     *    http://example.com/index.php/welcome/index
     *  - or -
     * Since this controller is set as the default controller in
     * config/routes.php, it's displayed at http://example.com/
     *
     * So any other public methods not prefixed with an underscore will
     * map to /index.php/welcome/<method_name>
     * @see https://codeigniter.com/user_guide/general/urls.html
     */

    //function for calling constructor..
    public function __construct() {
      parent::__construct();
      $this->load->library('session');
      
      $this->load->library('form_validation');
      $this->load->model('Client_model');
      //$this->load->model('admin_model');
      $this->load->library('email');
      $this->load->helper('url');
      $this->load->helper('security');
      
      

    }

    //function for loading index page i.e Login page...
    public function index()
    {
      $this->load->view('client/all_css');
      $this->load->view('client/header');
      $this->load->view('client/index');
      $this->load->view('client/footer');

      $this->load->view('client/all_js');
      $this->load->view('client/pagewise_js/client_login_page.js');
    }

    //function for  registration ....

    public function register(){
      $data['allCountries']=$this->Client_model->getAllCountries();

      $this->load->view('client/all_css');
      $this->load->view('client/header');
      $this->load->view('client/registration',$data);
      $this->load->view('client/footer');

      $this->load->view('client/all_js');
      $this->load->view('client/pagewise_js/register_page.js');

    }

    //function for  login ....

    public function login(){
      $this->load->view('client/all_css');
      $this->load->view('client/header');
      $this->load->view('client/index');
      $this->load->view('client/footer');
      $this->load->view('client/all_js');
      $this->load->view('client/pagewise_js/client_login_page.js');
    } 

    //function for cover image_upload
    function image_upload(){

      $this->load->view('client/image_upload_ajax');
    }

    //function for saving cover image
    function cover_image_save(){

      $this->load->view('client/image_saveBG_ajax');
    }

  //   //function for loading dashboard
  //   function viewDashboard(){

  //     $logged_in_userdata = $this->session->userdata(base_url().'login_session');
  //     $this->checkSession($logged_in_userdata);
  //     $email=$logged_in_userdata['email_id'];
  //     $role=$logged_in_userdata['role'];
  //     $data['firstname']=$logged_in_userdata['first_name'];
  //     $data['lastname']=$logged_in_userdata['last_name'];
  //     if($role == "mainclient"){

  //      $clientId=$logged_in_userdata['client_id'];
  //      $where=array('client_id'=>'1','subClient_id'=>'0');

  //    }else{

  //     $clientId=$logged_in_userdata['sub_client_id'];
  //     $where=array('client_id'=>'0','subClient_id'=>'1');
  //   }
    
  //   $checkClient=$this->Client_model->checkClientData($email);
  //   $data['clnt_id']=$checkClient->client_id;

  //   $countSubClient=$this->Client_model->totalSubClients($clientId);
  //   $data['totalSubClients']=$countSubClient->id;

  //   $countJobs=$this->Client_model->totalJobs($where);
  //   $data['totalJobs']=$countJobs->j_id;
  //     //$this->checkSession($logged_in_userdata);

  //   $package=$this->Client_model->getPackageCount();
  //     //echo "<pre>";print_r($data['packageCount']);exit;
  //   $data['packageCount']=$package->packagescount;
  //     //echo $data['packageCount'];exit;
  //   $this->load->view('client/all_css');
  //   $this->load->view('client/dashboard_header',$data);
  //   $this->load->view('client/adminhome',$data);
  //   $this->load->view('client/footer');
  //   $this->load->view('client/all_js');
  //   $this->load->view('client/pagewise_js/clienthome_page.js');


  // }

    //function for loading package view
    function viewPackages(){
      $logged_in_userdata = $this->session->userdata(base_url().'client_login_session');
      $email=$logged_in_userdata['email_id'];
      $checkClient=$this->Client_model->checkClientData($email);
      $data['clnt_id']=$checkClient->client_id;
      $this->checkSession($logged_in_userdata);
      
      // $data['showService']=$this->Client_model->get_service();

      $data['packages']=$this->Client_model->getAllPackages();
      $data['showService']=$this->Client_model->get_service();
      
     // echo "<pre>";print_r($data['packages']);exit;

      $this->load->view('client/all_css');
      $this->load->view('client/dashboard_header',$data);
      $this->load->view('client/packages',$data);
      $this->load->view('client/footer');
      $this->load->view('client/all_js');
      $this->load->view('client/pagewise_js/packages.js');


    }

    //function for viewing subscribe package
    function viewPackage(){
     // echo '<pre>';print_r($this->session->userdata());exit;

      $client_data=$this->session->userdata(base_url().'client_login_session');
      $client_id=$client_data['client_id'];
      $role = $client_data['role'];



      $logged_in_userdata = $this->session->userdata(base_url().'client_login_session');
      $this->checkSession($logged_in_userdata);

      $id=$this->input->get('package_id');

      $data['getPackageDetails'] = $this->Client_model->getPackageDetail($id);

     
     
      $data['subscribedPackage'] = $this->Client_model->getSubscribedPackages($client_id,$role,$id);

      $data['showService']=$this->Client_model->get_service();
      


     //echo '<pre>';print_r($this->db->last_query());exit;
      $this->load->view('client/all_css');
      $this->load->view('client/dashboard_header');
      $this->load->view('client/viewDetails',$data);
      $this->load->view('client/footer');
      $this->load->view('client/all_js');
      //$this->load->view('client/pagewise_js/client_login_page.js');


    }

    // function for creating account of client...

    public function create_account(){

      $_POST = json_decode(file_get_contents('php://input'), true);
      //print_r($_POST); exit;

      if ($this->input->post()) {

        $this->form_validation->set_rules('firstname', 'FirstName', 'trim|required|trim|xss_clean|strip_tags');
        $this->form_validation->set_rules('lastname', 'LastName', 'trim|required|trim|xss_clean|strip_tags');
        $this->form_validation->set_rules('company_name', 'Company Name', 'trim|required|trim|xss_clean|strip_tags');
        $this->form_validation->set_rules('city', 'City', 'trim|required|trim|xss_clean|strip_tags');
        $this->form_validation->set_rules('country', 'Country', 'trim|required|trim|xss_clean|strip_tags');
        $this->form_validation->set_rules('telephone', 'Telephone', 'trim|required|trim|xss_clean|strip_tags|numeric');
        $this->form_validation->set_rules('email', 'Email', 'trim|required|trim|xss_clean|strip_tags');
        $this->form_validation->set_rules('validate_email', 'Email', 'trim|required|trim|xss_clean|strip_tags|valid_email');
        $this->form_validation->set_rules('validate_email', 'Email', 'trim|required|trim|xss_clean|strip_tags|valid_email');
        $this->form_validation->set_rules('password', 'Password', 'trim|required|trim|xss_clean|strip_tags');
        $this->form_validation->set_rules('validate_password', 'Password', 'trim|required|trim|xss_clean|strip_tags');
        $this->form_validation->set_rules('agree', 'Agreement', 'trim|required|trim|xss_clean|strip_tags');

        if ($this->form_validation->run() == TRUE) {

          $info['clnt_first_name']=$this->input->post('firstname');
          $info['clnt_last_name']=$this->input->post('lastname');
          $info['company_name']=$this->input->post('company_name');

          $info['clnt_city']=$this->input->post('city');
          $info['clnt_country']=$this->input->post('country');
          $info['clnt_telephone']=$this->input->post('telephone');
          $info['clnt_email']=$this->input->post('email');
          $info['role']='mainclient';
          $info['clnt_password']=md5($this->input->post('password'));
          //echo '<pre>';print_r($info);exit;
          $emailid=$this->input->post('email');

          $check_email=$this->Client_model->checkEmailId($emailid);


          $result=$this->Client_model->add_client($info);
          //echo "<pre>";print_r($result);exit;
          if($result){
           // $url=base_url()."client/activateLogin/".$result;
            $fname=$this->input->post('firstname');
            $lname=$this->input->post('lastname');
            $email=$this->input->post('email');
            $password=$this->input->post('password');
            $name=$fname.' '.$lname;

            $subject = "Thank you for registration";

            $message = "Dear ".$name."<br/>";
            $message .="Greetings from Day Dreamer!<br />";
            $message .="Thank you for registration.<br />";
            $message .="Below are your login details .<br />";
            $message .=" Email : ".$email." .<br />Password :".$password.".<br />";
            $message .="Your account will be activate  after DayDreamer approval.<br />";
            //$message .=".$url.";

            $this->sendMail($subject,$message,$email);
            $data = array('status' => '1');
            $this->session->set_flashdata('successmsg', '<div class="alert alert-success"><a type="button" class="close" data-dismiss="alert" aria-hidden="true">×</a>Registration has been done successfully and the confirmation mail has been sent to your email Id.</div>');

          }else{
           $data = array('status' => '0');
           $this->session->set_flashdata('errormsg', '<div class="alert alert-warning alert-dismissable"><a type="button" class="close" data-dismiss="alert" aria-hidden="true">×</a>Error while submitting.</div>');


         }

       }else{


         $data = array('status' => '2', 'error' => $this->form_validation->get_field_data(), 'postdata' => $this->input->post());
       }

     }else{
      $data = array('status' => '3');
      $this->session->set_flashdata('errormsg', '<div class="alert alert-warning alert-dismissable"><a type="button" class="close" data-dismiss="alert" aria-hidden="true">×</a>All fields are mandatory.</div>');

    }

    print_r(json_encode($data));

  }


  //function for client login...
  function check_login(){


    $_POST = json_decode(file_get_contents('php://input'), true);
    // print_r($_POST);exit;

    if($this->input->post()) {
      $this->form_validation->set_rules('email', 'Email', 'trim|required|trim|xss_clean|strip_tags');
      $this->form_validation->set_rules('password', 'Password', 'trim|required|trim|xss_clean|strip_tags');

      if ($this->form_validation->run() == TRUE) {
       $emailaddress = $this->input->post('email');
       $password = md5($this->input->post('password'));
       $response = $this->Client_model->check_user_login($emailaddress, $password);
   //echo "<pre>";print_r($response);exit;
       $role=$response->role;
       if(!empty($response)){

        if($role == 'mainclient'){
          //echo 'main';
          $session_array=array(
          //'id'=>$response->id,
            'client_id'=>$response->id,
            'first_name'=>$response->clnt_first_name,
            'last_name'=>$response->clnt_last_name,
            'company_name'=>$response->company_name,
            'email_id'=>$response->clnt_email,
            'role'=>$response->role,
            'loginsession' => true

            );
        }else if($role == 'subclient'){
               
               $subClientId = $response->subClient_id;
               
               $subClientDetail = $this->Client_model->getSubClient($subClientId);

       //echo 'sub';
               
          $session_array=array(
          //'id'=>$response->id,
            'client_id'=>$subClientDetail->id,
            'first_name'=>$response->clnt_first_name,
            'last_name'=>$response->clnt_last_name,
            'company_name'=>$subClientDetail->subclient_name,
            'email_id'=>$response->clnt_email,
            'role'=>$response->role,
            'loginsession' => true

            );


        }
      //echo "<pre>";print_r($session_array);exit;
        $this->session->set_userdata(base_url().'client_login_session',$session_array);
        //$client_data=$this->session->userdata(base_url().'login_session');
        
        

        $checkOnlineData=$this->Client_model->checkOnlineFormData($response->id);
        //echo "<pre>";print_r($checkOnlineData);exit;
        if(!empty($checkOnlineData)){

          $data = array('status' => '4');
        }else if($role == 'subclient'){
         $data = array('status' => '4');

       }
       else{
        $data = array('status' => '1');
        //redirect(base_url().'client/viewDashboard');
      }
    }else{

     $data = array('status' => '0');
     $this->session->set_flashdata('errormsg', '<div class="alert alert-warning alert-dismissable"><a type="button" class="close" data-dismiss="alert" aria-hidden="true">×</a>Please activate your account and then login.</div>');
   }

  }else{
    $data = array('status' => '2', 'error' => $this->form_validation->get_field_data(), 'postdata' => $this->input->post());

  }


  }else{

    $data = array('status' => '3');
    $this->session->set_flashdata('errormsg', '<div class="alert alert-warning alert-dismissable"><a type="button" class="close" data-dismiss="alert" aria-hidden="true">×</a>All fields are mandatory.</div>');


  }

  print_r(json_encode($data));

  }

  //function for client profile

  function client_profile(){

    $client_data=$this->session->userdata(base_url().'client_login_session');
    $email=$client_data['email_id'];
    $company_name=$client_data['company_name'];
    $checkClient=$this->Client_model->checkClientData($email);
    $data['clnt_id']=$checkClient->client_id;
    $id=$client_data['client_id'];
    //$this->checkSession($logged_in_userdata);
    $data['register_data']=$this->Client_model->checkRegisterData($company_name);
   // echo "<pre>";print_r($data['register_data']);exit;
    $data['online_data']=$this->Client_model->checkProfile($id);
    $data['allCountries']=$this->Client_model->getAllCountries();
    //echo "<pre>";print_r($check_online_data);exit;
    $this->load->view('client/all_css');
    $this->load->view('client/dashboard_header',$data);
    $this->load->view('client/client',$data);
    $this->load->view('client/footer');

    $this->load->view('client/all_js');
    $this->load->view('client/pagewise_js/client_profile_page.js');



  }

  //function for inserting online profile data
  function client_online_data(){
    $client_session_data=$this->session->userdata(base_url().'client_login_session');
    $id=$client_session_data['client_id'];
    //echo $client_id;exit;
    $_POST = json_decode(file_get_contents('php://input'), true);

    if($this->input->post()) {
      $this->form_validation->set_rules('company', 'Company', 'trim|required|trim|xss_clean|strip_tags');
      $this->form_validation->set_rules('address1', 'Address1', 'trim|required|trim|xss_clean|strip_tags');
      //$this->form_validation->set_rules('address2', 'Address2', 'trim|required|trim|xss_clean|strip_tags');
      //$this->form_validation->set_rules('address3', 'Address3', 'trim|required|trim|xss_clean|strip_tags');
      $this->form_validation->set_rules('city', 'City', 'trim|required|trim|xss_clean|strip_tags');
      $this->form_validation->set_rules('country', 'Country', 'trim|required|trim|xss_clean|strip_tags');
      $this->form_validation->set_rules('postcode', 'Postcode', 'trim|required|trim|xss_clean|strip_tags');
      $this->form_validation->set_rules('company_number', 'Company', 'trim|required|trim|xss_clean|strip_tags');
      $this->form_validation->set_rules('vat_number', 'Vat Number', 'trim|required|trim|xss_clean|strip_tags');
      $this->form_validation->set_rules('estimated_turnover', 'Estimated Turnover', 'trim|required|trim|xss_clean|strip_tags');
      $this->form_validation->set_rules('numberOfStaff', 'Number of Staff', 'trim|required|trim|xss_clean|strip_tags');
      $this->form_validation->set_rules('title', 'Title', 'trim|required|trim|xss_clean|strip_tags');
      $this->form_validation->set_rules('detailAddress1', 'Detail Address1', 'trim|required|trim|xss_clean|strip_tags');
      //$this->form_validation->set_rules('detailAddress2', 'Detail Address2', 'trim|required|trim|xss_clean|strip_tags');
      //$this->form_validation->set_rules('detailAddress3', 'Detail Address3', 'trim|required|trim|xss_clean|strip_tags');
      $this->form_validation->set_rules('detailCity', 'Detail City', 'trim|required|trim|xss_clean|strip_tags');
      $this->form_validation->set_rules('detailCountry', 'Detail Country', 'trim|required|trim|xss_clean|strip_tags');
      $this->form_validation->set_rules('detailPostcode', 'Postcode', 'trim|required|trim|xss_clean|strip_tags');
      $this->form_validation->set_rules('industry1', 'Industry 1', 'trim|required|trim|xss_clean|strip_tags');
      //$this->form_validation->set_rules('industry2', 'Industry 2', 'trim|required|trim|xss_clean|strip_tags');
      //$this->form_validation->set_rules('industry3', 'Industry 3', 'trim|required|trim|xss_clean|strip_tags');
      $this->form_validation->set_rules('csr_policy', 'CSR Policy', 'trim|required|trim|xss_clean|strip_tags');
      if ($this->form_validation->run() == TRUE) {

        $insert_client_online_data['client_id']=$id;
        $insert_client_online_data['cmpny_name']=$this->input->post('company');
        $insert_client_online_data['clnt_cmpny_add1']=$this->input->post('address1');
        $insert_client_online_data['clnt_cmpny_add2']=$this->input->post('address2');
        $insert_client_online_data['clnt_cmpny_add3']=$this->input->post('address3');
        $insert_client_online_data['clnt_cmpny_city']=$this->input->post('city');
        $insert_client_online_data['clnt_country']=$this->input->post('country');
        $insert_client_online_data['clnt_cmpny_postcode']=$this->input->post('postcode');
        $insert_client_online_data['clnt_cmpnyNo']=$this->input->post('company_number');
        $insert_client_online_data['clnt_cmpny_vatNo']=$this->input->post('vat_number');
        $insert_client_online_data['clnt_estd_trnovr']=$this->input->post('estimated_turnover');
        $insert_client_online_data['clnt_estd_staffNo']=$this->input->post('numberOfStaff');
        $insert_client_online_data['clnt_job_title']=$this->input->post('title');
        $insert_client_online_data['clnt_dt_add1']=$this->input->post('detailAddress1');
        $insert_client_online_data['clnt_dt_add2']=$this->input->post('detailAddress2');
        $insert_client_online_data['clnt_dt_add3']=$this->input->post('detailAddress3');
        $insert_client_online_data['clnt_dt_city']=$this->input->post('detailCity');
        $insert_client_online_data['clnt_dt_cntry']=$this->input->post('detailCountry');
        $insert_client_online_data['clnt_dt_postcode']=$this->input->post('detailPostcode');
        $insert_client_online_data['clnt_dt_indstry1']=$this->input->post('industry1');
        $insert_client_online_data['clnt_dt_indstry2']=$this->input->post('industry2');
        $insert_client_online_data['clnt_dt_indstry3']=$this->input->post('industry3');
        $insert_client_online_data['clnt_dt_apprentices']=$this->input->post('radio1');
        $insert_client_online_data['clnt_dt_csrpolicy']=$this->input->post('csr_policy');
        $insert_client_online_data['clnt_exoffenders']=$this->input->post('radio2');

  //echo "<pre>";print_r($insert_client_online_data);
        $check_online_data=$this->Client_model->checkOnlineFormData($id);   
        // echo "<pre>";print_r($check_online_data);exit;
        if($check_online_data){

          $result=$this->Client_model->update_online_data($insert_client_online_data,$id);
        }
        else{
          $result=$this->Client_model->insert_online_data($insert_client_online_data);
        }
        if($result){
          $data = array('status' => '1');
          $this->session->set_flashdata('successmsg', '<div class="alert alert-success"><a type="button" class="close" data-dismiss="alert" aria-hidden="true">×</a>Online form data has been saved successfully</div>');

        }else{
          $data = array('status' => '0');
          $this->session->set_flashdata('errormsg', '<div class="alert alert-warning alert-dismissable"><a type="button" class="close" data-dismiss="alert" aria-hidden="true">×</a>Error while submitting.</div>');

        }
      }

      else{
        $data = array('status' => '2', 'error' => $this->form_validation->get_field_data(), 'postdata' => $this->input->post());


      }

    }else{
     $data = array('status' => '3');
     $this->session->set_flashdata('errormsg', '<div class="alert alert-warning alert-dismissable"><a type="button" class="close" data-dismiss="alert" aria-hidden="true">×</a>All fields are mandatory.</div>');
   }
   print_r(json_encode($data));

  }

  //function to activate login
  function activateLogin($token){

   $actvntm = date("Y-m-d H:i:s");
   $data = array
   (
    'flag'=>1,
    'activation_token' => $token,
    'activation_time' => $actvntm,
    );
   $status = $this->Client_model->activateClientLogin($data,$token);
   if($status)
   {
    redirect(base_url().'client');
  }


  }

  //function for sending mail.
  function sendMail($subject,$message,$email){
    require 'vendor/autoload.php';
    $sendgrid = new SendGrid("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");
    $client_email = new SendGrid\Email();
    $client_email->addTo($email)
    //->addCc($ccemail)
    ->setFrom('support@evonixtech.com')
    ->setFromName("DayDreamer Admin")
    ->setSubject($subject)
    ->setHtml($message);
    $sendgrid->send($client_email);
  }
     //function for generating random string
  function generateRandomString($length = 16) {  
    $characters   = 'abcdefghijklmnopqrstuvwxyz0123456789';
    $randomString = '';
    for ($i = 0; $i < $length; $i++) {
      $randomString .= $characters[rand(0, strlen($characters) - 1)];
    }
    return $randomString;
  }

    //function for forget password
  function forgetPassword(){
    $_POST = json_decode(file_get_contents('php://input'), true);
    if ($this->input->post()) {
     $this->form_validation->set_rules('email', 'Email ID', 'trim|required|trim|xss_clean|strip_tags|valid_email');
     if ($this->form_validation->run() == TRUE) {
       $emailaddress = $this->input->post('email');
       $randomkeystring = $this->generateRandomString();
       $url = base_url();
       $response = $this->Client_model->check_client_forgot_password($emailaddress);
       if(!empty($response)){

         $updatekeystring = $this->Client_model->update_keystring($emailaddress,$randomkeystring);
         require 'vendor/autoload.php';

         $sendgrid = new SendGrid("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");
         $email = new SendGrid\Email();
         $email->addTo($emailaddress)
         ->addCc('sonal@evonix.co')
         ->setFrom('support@daydreamer.com')
         ->setFromName('Day Dreamer')
         ->setSubject("Here's the link to reset your password")
         ->setHtml("You recently requested a password reset.<br />To change your password<br /><a href='".$url."client/resetpassword/$randomkeystring'>Click here</a><br />The link will expire in 12 hours, so be sure to use it right away.<br /><br /> <br />Regards, <br />Support Team.");

         if($sendgrid->send($email)) {
          $data = array('status' => '1', 'message' => '<div class="alert alert-success">An email has been sent to '.$emailaddress.'. Please click on the link provided in the mail to reset your password.</div>');
        } 
        else {
          $data = array('status' => '4', 'message' => '<div class="alert alert-danger">Error in sending Email.</div>');
        }
      }else{
        $data = array('status' => '0', 'message' => '<div class="alert alert-danger"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>The email ID you have entered does not exist !.</div>');

      }
    }else{

      // $data = array('status' => '2', 'error' => $this->form_validation->get_field_data(), 'postdata' => $this->input->post());

    }
  }else{

    $data = array('status' => '3');
  }

  print_r(json_encode($data));
  }

  //function for resetting password
  function resetpassword($randomkeystring=''){
    $keystring['data'] = $this->Client_model->check_randomkey_string($randomkeystring);
    if(empty($keystring['data']))
    {
      $rediect_path = base_url().'admin';
      redirect($redirect_path, 'refresh');
    }
    $this->load->view('client/all_css');
    $this->load->view('client/header');
    $this->load->view('client/resetpassword',$keystring);
    $this->load->view('client/footer');
    $this->load->view('client/all_js');
    $this->load->view('client/pagewise_js/resetpassword.js');

  }

  //function for checking reset password
  function check_reset_password(){
    $_POST = json_decode(file_get_contents('php://input'), true);
  // print_r($_POST);exit;  
    if ($this->input->post()) {
      $this->form_validation->set_rules('npassword', 'New Password', 'trim|required|trim|xss_clean|strip_tags');
      $this->form_validation->set_rules('cpassword', 'Confirm Password', 'trim|required|trim|xss_clean|strip_tags|matches[npassword]');

      if ($this->form_validation->run() == TRUE) {
        $url=base_url();
        $emailaddress = $this->input->post('emailaddress');
        $npassword = $this->input->post('npassword');

        $updatepassword = $this->Client_model->update_password($emailaddress,$npassword);
        if ($updatepassword) {
          $data = array('status' => '1', 'message' => '<div class="alert alert-success">You have successfully changed your password. Please sign in with your new password.<br><a href="'.$url.'client" class="btn btn-twitter" style="padding: 2px 8px;">Sign In</a></div>');
        }
        else {
          //$data = array('status' => '0');
          $data = array('status' => '0', 'message' => '<div class="alert alert-danger"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>Oops... Something went wrong !.</div>');
        }
      }else{


      }

    }else{

      $data = array('status' => '3');
    }
    print_r(json_encode($data));
  }

  //function to check email.

  function check_email_exist(){
    //echo 'here';exit;
    $email=$this->input->post('email');
    //echo $email;exit;
    $result=$this->Client_model->checkEmailExist($email);
    //echo "<pre>";print_r($result);exit;
    if(!empty($result)){

      echo  'success';

    }else{

      echo 'error';
    }
  }

  //function for checking session
  function checkSession($logged_in_userdata){

   if (!empty($logged_in_userdata)) {
    return TRUE;
  } else {
    redirect('');
  }


  }

  //function for view job form
  function viewJobForm(){

    $logged_in_userdata=$this->session->userdata(base_url().'client_login_session');
    $this->checkSession($logged_in_userdata);
    $email=$logged_in_userdata['email_id'];
    $role=$logged_in_userdata['role'];
    $firstname=$logged_in_userdata['first_name'];
    $lastname=$logged_in_userdata['last_name'];
    $data['subClient_name']=$firstname.' '.$lastname;
    if($role == "mainclient"){

     $ClientId=$logged_in_userdata['client_id'];

   }else{

    $ClientId=$logged_in_userdata['client_id'];

  }

  $data['fetchSubClientData']=$this->Client_model->getSubClientData($ClientId);

  $checkClient=$this->Client_model->checkClientData($email);
  $data['clnt_id']=$checkClient->client_id;
    //$this->checkSession($logged_in_userdata);
  $data['allCountries']=$this->Client_model->getAllCountries();
  $data['allIndustries']=$this->Client_model->getAllIndustries();
  $data['allStatus']=$this->Client_model->getAllStatus();
  $data['allExperience']=$this->Client_model->getExperience();
  $data['allJobType']=$this->Client_model->getJobType();
  $data['allSkills']=$this->Client_model->getSkills();
  $data['allCurrency']=$this->Client_model->getCurrencies();
    //echo 'here';exit;
  $data['allSubCleints']=$this->Client_model->getAllSubClients($ClientId);
  $this->load->view('client/all_css');
  $this->load->view('client/dashboard_header',$data);
  $this->load->view('client/createjob',$data);
  $this->load->view('client/footer');

  $this->load->view('client/all_js');
  $this->load->view('client/pagewise_js/jobForm_page.js');


  }


  // public function fetchSubClient() 
  // { 
  //   $searchquery = $_GET["query"]; 
  //   if (trim($searchquery) != '') 
  //     { 
  //       $querystmt = "SELECT subclient_name, client_id from tbl_subClient_create where subclient_name like '%" . trim($searchquery) . "%'"; 

  //     } 
  //     $query = mysql_query($querystmt); 
  //     $json = array();

  //     if (mysqli_num_rows($query) > 0) {
  //      while ($row = mysqli_fetch_array($query)) 
  //       { 
  //         $json['suggestions'][] = array('value' => $row[0], 'data' => $row[1]); 
  //       } 
  //       print(json_encode($json)); 
  //       mysql_close(); 
  //     }
  //        else 
  //         { 
  //           $this->session->set_flashdata("errormessage", "Sub Office does not exist"); 
  //           redirect('client/viewJobForm'); 
  //   } 
  // }

  //function for fetching sub client name..
  public function fetchSubClient() 
  { 
    $client_session_data=$this->session->userdata(base_url().'client_login_session');
    $client_id=$client_session_data['client_id'];
    $company_name=$client_session_data['company_name'];
    $clientname=$company_name;
    $searchquery = $_GET["query"]; 

    $search=$searchquery;
    if ($search != '') 
    { 
        //echo 'here';exit;
     $result=$this->Client_model->getSubClientsName($search,$client_id);
     $json = array();
     
     foreach($result as $key){
       $subClientName=$key->subclient_name;

       $subClientId=$key->id;
       
       $json['suggestions'][] = array('value' => $subClientName,  'data' => $subClientId); 
       
     }
     
     $json['suggestions'][] = array('value' => $clientname,  'data' => $clientname);
     print(json_encode($json)); 

   } 


   else 
   { 
            //echo 'there';exit;
    $this->session->set_flashdata("errormessage", "Sub Office does not exist"); 
    redirect('client/viewJobForm'); 
  } 
  }



  //function for viewing all jobs
  function viewJobs(){

    $client_session_data=$this->session->userdata(base_url().'client_login_session');
    $this->checkSession($client_session_data);
    $email=$client_session_data['email_id'];

    $role=$client_session_data['role'];
    //echo $role;exit;
    $checkClient=$this->Client_model->checkClientData($email);
    $data['clnt_id']=$checkClient->client_id;
    if($role == 'mainclient'){
      $id=$client_session_data['client_id'];
      
    }else{

      $id=$client_session_data['client_id'];

    }


    if($role == 'subclient'){
     $where=array('client_id'=>'0','subClient_id'=>$id);

   }else{


    $where=array('subClient_id'=>'0','client_id'=>$id);
  }
    //$this->checkSession($client_session_data);

  $data['allJobs']=$this->Client_model->getAllJobs($where,$id,$role);
    //echo '<pre>';print_r($this->db->last_query());exit;
    //echo '<pre>';print_r($data['allJobs']);exit;
  $this->load->view('client/all_css');
  $this->load->view('client/dashboard_header',$data);
  $this->load->view('client/viewalljobs',$data);
  $this->load->view('client/footer');

  $this->load->view('client/all_js');
  $this->load->view('client/pagewise_js/viewJobs.js');

  }

  //function for viewing sub client form
  function viewSubClientForm(){
    //$this->checkSession($logged_in_userdata);
    $client_session_data=$this->session->userdata(base_url().'client_login_session');
    $ClientId=$client_session_data['client_id'];
    $clientFirstName=$client_session_data['first_name'];
    $clientLastName=$client_session_data['last_name'];
    $company_name=$client_session_data['company_name'];
    $data['parentName']=$company_name;
    $this->checkSession($client_session_data);
    $email=$client_session_data['email_id'];
    $checkClient=$this->Client_model->checkClientData($email);
    $data['clnt_id']=$checkClient->client_id;
    $data['allSubCleints']=$this->Client_model->getAllSubClients($ClientId);
    $data['allSources']=$this->Client_model->getAllSources();

    $data['allIndustries']=$this->Client_model->getAllIndustries();
    $data['allStatus']=$this->Client_model->getAllStatus();
    $this->load->view('client/all_css');
    $this->load->view('client/dashboard_header',$data);
    $this->load->view('client/createclient',$data);
    $this->load->view('client/footer');

    $this->load->view('client/all_js');
    $this->load->view('client/pagewise_js/createSubClient_form.js');

  }


  //function for creating job opening

  function create_job_opening(){
    //echo 'here';exit;
   // echo 'here';
    //print_r($subClientId);exit;

   
     //echo "<pre>"; print_r($_POST); exit;


    $client_session_data=$this->session->userdata(base_url().'client_login_session');

    $role=$client_session_data['role'];
    if($role == 'mainclient'){

      $id=$client_session_data['client_id'];
     //echo $id;exit;
    }else{
    //echo 'here';exit;
      $id=$client_session_data['client_id'];
      
    }
    
    $_POST = json_decode(file_get_contents('php://input'), true);

    // echo '<pre>';print_r($_POST);exit;
        

       $checkPackageSevices = $this->Client_model->checkPackageSevices($id,$role);

        //echo "<pre>";  print_r($checkPackageSevices); exit;

         if(empty($checkPackageSevices))
         {
              
              $this->session->set_flashdata('successmsg','Please subscribe package first to post job ');

             // redirect(base_url().'client/viewPackages');

              $data=array('status'=>'5');
             
         }else{

                  //echo 123; exit();
               //  echo "<pre>"; print_r($checkPackageSevices); exit;

              $allowedJobsads = $checkPackageSevices->job_adds;

              // echo $allowedJobsads; exit;

              $jobServiceType = $checkPackageSevices->type;
         

              $countCreateJobs = $this->Client_model->checkTotalCreateJobs($id,$role);
       
             $createJobCount = count($countCreateJobs);

           //   echo $createJobCount.'posts jobs'."    ".$allowedJobsads;  
                 // exit;
              //$newJBC  =  $createJobCount;

              // echo $newJBC; exit;

              if($allowedJobsads   >  $createJobCount )
                {
                    // echo 'Can post job'; exit;


                     
                 if ($this->input->post()) {
        //echo '<pre>';print_r($_POST);exit;
      $this->form_validation->set_rules('job_title', 'Job Title', 'trim|required|trim|xss_clean|strip_tags');
      $this->form_validation->set_rules('contact_name', 'Contact Name', 'trim|required|trim|xss_clean|strip_tags');

      $this->form_validation->set_select('job_status', 'Status', 'trim|required|trim|xss_clean|strip_tags');
      
      $this->form_validation->set_rules('state', 'State', 'trim|required|trim|xss_clean|strip_tags');
      $this->form_validation->set_select('experience', 'Experience', 'trim|required|trim|xss_clean|strip_tags');

      $this->form_validation->set_rules('positions', 'Position', 'trim|required|trim|xss_clean|strip_tags');
      // $this->form_validation->set_rules('open_date', 'Open Date', 'trim|required|trim|xss_clean|strip_tags');
      $this->form_validation->set_select('job_type', 'Job Type', 'trim|required|trim|xss_clean|strip_tags');
      $this->form_validation->set_select('qualifications', 'Qualifications', 'trim|required|trim|xss_clean|strip_tags');
      
      $this->form_validation->set_select('working_days', 'Working Days', 'trim|required|trim|xss_clean|strip_tags');
    $this->form_validation->set_select('company_profile', 'Company Profile', 'trim|required|trim|xss_clean|strip_tags');

      $this->form_validation->set_rules('city', 'City', 'trim|required|trim|xss_clean|strip_tags');
      $this->form_validation->set_select('nationality', 'Country', 'trim|required|trim|xss_clean|strip_tags');

      $this->form_validation->set_rules('job_desc', 'Job Description', 'trim|required|trim|xss_clean|strip_tags');
      $this->form_validation->set_rules('min', 'Minimum Salary', 'trim|required|trim|xss_clean|strip_tags');
      $this->form_validation->set_rules('max', 'Maximum Salary', 'trim|required|trim|xss_clean|strip_tags');
      $this->form_validation->set_rules('currency', 'Currency', 'trim|required|trim|xss_clean|strip_tags');
  //$this->form_validation->set_rules('attached_file', 'Job Summary', 'trim|required|trim|xss_clean|strip_tags');
       


      if ($this->form_validation->run() == TRUE) {
         // echo 'here';exit;
        //echo '<pre>';print_r($_POST);exit;
        if($role =='subclient'){
             //echo 'here';
          $create_job['subClient_id']=$id;
          

        }else{
                //echo 'there';
          $create_job['client_id']=$id;

        }
        
        $create_job['role'] = $role;
        $create_job['job_title']=$this->input->post('job_title');
        $create_job['contact_name']=$this->input->post('contact_name');
        //$create_job['recruiter']=$this->input->post('recruiter');
        //$create_job['target_date']=date('Y-m-d',strtotime($this->input->post('target_date')));
  //echo $create_job['target_date'];exit;
        $create_job['job_status']=$this->input->post('job_status');
        //$create_job['industry']=$this->input->post('industry');
        $create_job['state']=$this->input->post('state');
        $create_job['experience']=$this->input->post('experience');
          if($role == 'mainclient'){

           $create_job['parent_client']=$this->input->post('parent_client');
            }
         else{
              $create_job['parent_client']=$client_session_data['client_id'];
          }
       
       //$create_job['manager']=$this->input->post('manager');

       $create_job['positions']=$this->input->post('positions');
       $create_job['open_date']=date('Y-m-d',strtotime($this->input->post('open_date')));

       $create_job['job_type']=$this->input->post('job_type');
       $create_job['qualifications']=$this->input->post('qualifications');

       $create_job['working_days']=$this->input->post('working_days');
       $create_job['company_profile']=$this->input->post('company_profile');
        //$skill_array=$this->input->post('skills');
        //$create_job['skills']=implode(',',$skill_array);
       $create_job['city']=$this->input->post('city');

       $create_job['nationality']=$this->input->post('nationality');
       $create_job['min_salary']=$this->input->post('min');
       $create_job['max_salary']=$this->input->post('max');
       $create_job['currency']=$this->input->post('currency');
       //$create_job['job_nature']=$this->input->post('job_nature');
       $create_job['job_desc']=$this->input->post('job_desc');
       $create_job['job_summary']=$this->input->post('filename');
       $create_job['job_approve']=1;
       $create_job['job_publish'] = $this->input->post('published');

        //$create_job['subClient_id']=$this->input->post('sub_client_id');
        //$create_job['j_id']=$this->input->post('job_id');
       $job_id=$this->input->post('job_id');
        //echo '<pre>';print_r($create_job);exit;

           
         

    
    
       $checkJobData=$this->Client_model->checkJobData($job_id);

       if(!empty($checkJobData)){

           //echo 'here';exit;

        $update_job['job_title']=$this->input->post('job_title');
        $update_job['contact_name']=$this->input->post('contact_name');
        //$update_job['recruiter']=$this->input->post('recruiter');

        $update_job['job_status']=$this->input->post('job_status');
        //$update_job['industry']=$this->input->post('industry');
        $update_job['state']=$this->input->post('state');
        $update_job['experience']=$this->input->post('experience');
          //$update_job['parent_client']=$this->input->post('parent_client');
        //$update_job['manager']=$this->input->post('manager');

        $update_job['positions']=$this->input->post('positions');
        $update_job['open_date']=date('Y-m-d',strtotime($this->input->post('open_date')));

        $update_job['job_type']=$this->input->post('job_type');
        $update_job['qualifications']=$this->input->post('qualifications');
        //$update_job['gender']=$this->input->post('radio2');
        $update_job['working_days']=$this->input->post('working_days');
        $update_job['company_profile']=$this->input->post('company_profile');
          //$update_job['company_profile']=$this->input->post('company_profile');
          //$update_job['skills']=implode(',',$this->input->post('skills'));
          //print_r($update_job['skills']);exit;
          //$update_job['skills']=implode(',',$skill_array);


        $update_job['city']=$this->input->post('city');

        $update_job['nationality']=$this->input->post('nationality');
        $update_job['min_salary']=$this->input->post('min');
        $update_job['max_salary']=$this->input->post('max');
        $update_job['currency']=$this->input->post('currency');
        //$update_job['job_nature']=$this->input->post('job_nature');
        $update_job['job_desc']=$this->input->post('job_desc');
        $job_summary=$this->input->post('filename');

        if(isset($job_summary) && !empty($job_summary)){

          $update_job['job_summary']=$this->input->post('filename');
        }else{

          $update_job['job_summary']= $checkJobData->job_summary;
        }
        //echo '<pre>';print_r($update_job);exit;
        //$create_job['subClient_id']=$this->input->post('sub_client_id');
        //$create_job['j_id']=$this->input->post('job_id');
        $job_id=$this->input->post('job_id');

        $result=$this->Client_model->updateJobCreate($update_job,$job_id);
         //echo '<pre>';print_r($this->db->last_query());exit;
      }else{

       $result=$this->Client_model->insertJobCreate($create_job);

     }

     if($result){

      $data = array('status' => '1');
      $this->session->set_flashdata('successmsg', '<div class="alert alert-success"><a type="button" class="close" data-dismiss="alert" aria-hidden="true">×</a>Your request has been completed successfully.</div>');
    }else{

      $data = array('status' => '0');
      $this->session->set_flashdata('errormsg', '<div class="alert alert-warning alert-dismissable"><a type="button" class="close" data-dismiss="alert" aria-hidden="true">×</a>Error while submitting.</div>');
    }

  }else{
    $data = array('status' => '2', 'error' => $this->form_validation->get_field_data(), 'postdata' => $this->input->post());
  }


  }else{

    $data=array('status'=>'3');

  }
               

               

             }
             else{
                      
                   //  echo 'else'; exit;
                 
                 $this->session->set_flashdata('successmsg','You have already created allowed jobs');
   //              redirect(base_url().'client/viewPackages');


                  $data=array('status'=>'4');

                }


   }


    print_r(json_encode($data));

  }




  //function for file uploading....
  function upload_doc(){
    $client_session_data=$this->session->userdata(base_url().'client_login_session');
    $role=$client_session_data['role'];
    if($role == "mainclient"){

     $id=$client_session_data['client_id'];

   }else{

    $id=$client_session_data['client_id'];
  }
    //$id=$client_session_data['client_id'];

  $filename=$_FILES['file']['name'];
  $target = 'client_uploads/';
  $file = $target.$id.'-'.$filename;

  if(move_uploaded_file($_FILES['file']['tmp_name'], $file))
  {

    print_r(json_encode(array('filename'=>$filename)));

  }else{


    echo 'not uploded';
  }

  }

  //function for uploading client documents
  function upload_clientdoc(){
    $client_session_data=$this->session->userdata(base_url().'client_login_session');
    $id=$client_session_data['client_id'];

    $filename=$_FILES['file']['name'];
    $target = 'client_uploads/subClientDoc/';
    $file = $target.$id.'-'.$filename;
    //echo $file;exit;
    if(move_uploaded_file($_FILES['file']['tmp_name'], $file))
    {

      print_r(json_encode(array('filename'=>$filename)));

    }else{


      echo 'not uploaded';
    }


  }


  //function for uploading client company logo..
  function upload_company_logo(){

    $client_session_data=$this->session->userdata(base_url().'client_login_session');
    $id=$client_session_data['client_id'];

    $filename=$_FILES['file']['name'];
    $target = 'client_uploads/client_logo/';
    $file = $target.$id.'-'.$filename;
    //echo $file;exit;
    if(move_uploaded_file($_FILES['file']['tmp_name'], $file))
    {

      print_r(json_encode(array('filename'=>$filename)));

    }else{


      echo 'not uploaded';
    }




  }

  //function for Logo
  function clientLogo(){

    $client_session_data=$this->session->userdata(base_url().'client_login_session');
    $id=$client_session_data['client_id'];
    $_POST = json_decode(file_get_contents('php://input'), true);

    if ($this->input->post()) {

      $this->form_validation->set_rules('filename', 'Company Logo', 'trim|required|trim|xss_clean|strip_tags');

      if ($this->form_validation->run() == TRUE) {
  //echo 'here';exit;
        $upload_logo['client_id']=$id;
        $upload_logo['company_logo']=$this->input->post('filename');

        $result=$this->Client_model->insertCompanyLogo($upload_logo);

        if($result){

          $data=array('status'=>'1');
          $this->session->set_flashdata('successmsg', '<div class="alert alert-success"><a type="button" class="close" data-dismiss="alert" aria-hidden="true">×</a>Your Logo has been uploaded successfully.</div>');
        }else{

          $data=array('status'=>'0');
          $this->session->set_flashdata('errormsg', '<div class="alert alert-warning alert-dismissable"><a type="button" class="close" data-dismiss="alert" aria-hidden="true">×</a>Error while submitting.</div>');
        }

      }else{

       $data = array('status' => '2');
     }

   }else{

    $data=array('status'=>'3', 'error' => 'Please Upload your Company Logo');

  }
  print_r(json_encode($data));
  }

  //function for creating sub client....
  function create_sub_client(){
   // echo 'here';exit;
    $client_session_data=$this->session->userdata(base_url().'client_login_session');
    $id=$client_session_data['client_id'];
    //echo $id;exit;
    $_POST = json_decode(file_get_contents('php://input'), true);

    if ($this->input->post()) {
      $this->form_validation->set_rules('subclient_name', 'Client Name', 'trim|required|trim|xss_clean|strip_tags');
      $this->form_validation->set_rules('contact_number', 'Contact Number', 'trim|required|trim|xss_clean|strip_tags');
      //$this->form_validation->set_rules('manager', 'Account Manager', 'trim|required|trim|xss_clean|strip_tags');
      $this->form_validation->set_rules('industry', 'Industry', 'trim|required|trim|xss_clean|strip_tags');
      $this->form_validation->set_rules('about', 'About', 'trim|required|trim|xss_clean|strip_tags');
      $this->form_validation->set_rules('source', 'Source', 'trim|required|trim|xss_clean|strip_tags');
      $this->form_validation->set_rules('parent_client', 'Parent Client', 'trim|required|trim|xss_clean|strip_tags');
      $this->form_validation->set_rules('website', 'Website', 'trim|required|trim|xss_clean|strip_tags');
      $this->form_validation->set_rules('billing_street', 'Billing Street', 'trim|required|trim|xss_clean|strip_tags');
      $this->form_validation->set_rules('billing_city', 'Billing City', 'trim|required|trim|xss_clean|strip_tags');
      $this->form_validation->set_rules('billing_state', 'Billing State', 'trim|required|trim|xss_clean|strip_tags');
      $this->form_validation->set_rules('billing_code', 'Billing Code', 'trim|required|trim|xss_clean|strip_tags');
      $this->form_validation->set_rules('billing_country', 'Billing Country', 'trim|required|trim|xss_clean|strip_tags');
      $this->form_validation->set_rules('shipping_street', 'Shipping Street', 'trim|required|trim|xss_clean|strip_tags');

      $this->form_validation->set_rules('shipping_city', 'Shipping City', 'trim|required|trim|xss_clean|strip_tags');
      $this->form_validation->set_rules('shipping_state', 'Shipping State', 'trim|required|trim|xss_clean|strip_tags');
      $this->form_validation->set_rules('shipping_code', 'Shipping Code', 'trim|required|trim|xss_clean|strip_tags');
      $this->form_validation->set_rules('shipping_country', 'Shipping Country', 'trim|required|trim|xss_clean|strip_tags');
    //$this->form_validation->set_rules('attached_file', 'Attachments', 'trim|required|trim|xss_clean|strip_tags');


      if ($this->form_validation->run() == TRUE) {

        $create_sub_client['client_id']=$id;
        $create_sub_client['subclient_name']=$this->input->post('subclient_name');
        $create_sub_client['contact_number']=$this->input->post('contact_number');
        //$create_sub_client['manager']=$this->input->post('manager');
        $create_sub_client['industry']=$this->input->post('industry');
        $create_sub_client['about']=$this->input->post('about');
        $create_sub_client['source']=$this->input->post('source');
        $create_sub_client['parent_client']=$this->input->post('parent_client');
        $create_sub_client['website']=$this->input->post('website');
        $create_sub_client['billing_street']=$this->input->post('billing_street');
        $create_sub_client['billing_city']=$this->input->post('billing_city');
        $create_sub_client['billing_state']=$this->input->post('billing_state');
        $create_sub_client['billing_code']=$this->input->post('billing_code');
        $create_sub_client['billing_country']=$this->input->post('billing_country');
        $create_sub_client['shipping_street']=$this->input->post('shipping_street');
        $create_sub_client['shipping_city']=$this->input->post('shipping_city');
        $create_sub_client['shipping_state']=$this->input->post('shipping_state');
        $create_sub_client['shipping_code']=$this->input->post('shipping_code');
        $create_sub_client['shipping_country']=$this->input->post('shipping_country');
        $create_sub_client['attached_file']=$this->input->post('filename');
        $subClient_id=$this->input->post('subclient_id');
       //echo '<pre>';print_r($create_sub_client);exit;
        $checkSubClientData=$this->Client_model->getSubClientDataById($subClient_id);
    //echo '<pre>';print_r($checkSubClientData);exit;
        if($checkSubClientData){

         $result=$this->Client_model->updateSubClientDetailsById($create_sub_client,$subClient_id);
       }else{


         $result=$this->Client_model->insertSubClientDetails($create_sub_client);

       }
       if($result){
         $data = array('status' => '1');
         $this->session->set_flashdata('successmsg', '<div class="alert alert-success"><a type="button" class="close" data-dismiss="alert" aria-hidden="true">×</a>Your request has been completed successfully.</div>');

       }else{
        $data = array('status' => '0');
        $this->session->set_flashdata('errormsg', '<div class="alert alert-warning alert-dismissable"><a type="button" class="close" data-dismiss="alert" aria-hidden="true">×</a>Error while submitting.</div>');
      }

    }else{

     $data = array('status' => '2', 'error' => $this->form_validation->get_field_data(), 'postdata' => $this->input->post());
   }
  }else{

    $data=array('status'=>'3');

  }
  print_r(json_encode($data));
  }

  //function for viewing all clients
  function viewAllClients(){

    $client_session_data=$this->session->userdata(base_url().'client_login_session');
    $this->checkSession($client_session_data);
    $email=$client_session_data['email_id'];
    $role=$client_session_data['role'];
    $checkClient=$this->Client_model->checkClientData($email);
    $data['clnt_id']=$checkClient->client_id;
    
    if($role == 'mainclient'){

      $id=$client_session_data['client_id'];   
    }else{

      $id=$client_session_data['client_id']; 
    }
    //$this->checkSession($logged_in_userdata);

    $data['allClients']=$this->Client_model->getAllClients($id);

    $this->load->view('client/all_css');
    $this->load->view('client/dashboard_header',$data);
    $this->load->view('client/viewClients',$data);
    $this->load->view('client/footer');

    $this->load->view('client/all_js');
    $this->load->view('client/pagewise_js/viewClients.js');
  }

  //function for editing sub client details
  function editSubClientDetails(){
    $client_session_data=$this->session->userdata(base_url().'client_login_session');
    $this->checkSession($client_session_data);
    $email=$client_session_data['email_id'];
    $firstname=$client_session_data['first_name'];
    $lastname=$client_session_data['last_name'];
    $data['parentName']=$firstname.' '.$lastname;
    $checkClient=$this->Client_model->checkClientData($email);
    $data['clnt_id']=$checkClient->client_id;
    $subClientId=$this->input->get('id');
    $data['getSubClientData']=$this->Client_model->getSubClientDataById($subClientId);
    //echo '<pre>';print_r($data['getSubClientData']);exit;
    $data['allSources']=$this->Client_model->getAllSources();

    $data['allIndustries']=$this->Client_model->getAllIndustries();
  //echo '<pre>';print_r($getSubClientData);exit;
   // $this->checkSession($logged_in_userdata);
    $data['allCountries']=$this->Client_model->getAllCountries();
    //echo 'here';exit;
    $this->load->view('client/all_css');
    $this->load->view('client/dashboard_header',$data);
    $this->load->view('client/createclient',$data);
    $this->load->view('client/footer');

    $this->load->view('client/all_js');
    $this->load->view('client/pagewise_js/createSubClient_form.js');
  }

  //function for deactivating sub client
  function deactivateSubClientDetails(){
    $subClientId=$this->input->get('id');
    $updateArr=array('deactivate'=>'1');
    $result=$this->Client_model->deactivateAccount($updateArr,$subClientId);
    if($result){


     $this->session->set_flashdata('successmsg', '<div class="alert alert-success"><a type="button" class="close" data-dismiss="alert" aria-hidden="true">×</a>Client has been deactivated successfully.</div>');
     redirect(base_url().'client/viewAllClients');

   }else{

    $this->session->set_flashdata('errormsg', '<div class="alert alert-warning alert-dismissable"><a type="button" class="close" data-dismiss="alert" aria-hidden="true">×</a>Error while submitting.</div>'); 
  }
  }

  //function for activating subclient.
  function activateSubClientDetails(){
    $subClientId=$this->input->get('id');
    $updateArr=array('deactivate'=>'0');
    $result=$this->Client_model->activateAccount($updateArr,$subClientId);
    if($result){
      $this->session->set_flashdata('successmsg', '<div class="alert alert-success"><a type="button" class="close" data-dismiss="alert" aria-hidden="true">×</a>Client has been activated successfully.</div>');
      redirect(base_url().'client/viewAllClients');
    }else{

     $this->session->set_flashdata('errormsg', '<div class="alert alert-warning alert-dismissable"><a type="button" class="close" data-dismiss="alert" aria-hidden="true">×</a>Error while submitting.</div>');  
   }

  }

  //function for viewing client information
  function clientInfo(){
    $client_session_data=$this->session->userdata(base_url().'client_login_session');
    $this->checkSession($client_session_data);
    $email=$client_session_data['email_id'];
    $checkClient=$this->Client_model->checkClientData($email);
    $data['clnt_id']=$checkClient->client_id;
    //$this->checkSession($logged_in_userdata);
    $subClientId=$this->input->get('id');
    $data['getClientInfo']=$this->Client_model->getSubclientInformation($subClientId);
    //echo '<pre>';print_r($data['getClientInfo']);exit;
    $data['getJobs']=$this->Client_model->getSubClientJobs($subClientId);
    //echo '<pre>';print_r($data['getJobs']);exit;

    $this->load->view('client/all_css');
    $this->load->view('client/dashboard_header',$data);
    $this->load->view('client/client-info',$data);
    $this->load->view('client/footer');

    $this->load->view('client/all_js');

  }

  //function for inserting subClient job
  function insertSubclientJobOpening(){

    $client_session_data=$this->session->userdata(base_url().'client_login_session');
    $this->checkSession($client_session_data);
    $email=$client_session_data['email_id'];
    $checkClient=$this->Client_model->checkClientData($email);
    $data['clnt_id']=$checkClient->client_id;
    //$this->checkSession($logged_in_userdata);
    $data['subClientId']=$this->input->get('id');
    $data['allCountries']=$this->Client_model->getAllCountries();
    $data['allIndustries']=$this->Client_model->getAllIndustries();
    $data['allStatus']=$this->Client_model->getAllStatus();
    $data['allExperience']=$this->Client_model->getExperience();
    $data['allJobType']=$this->Client_model->getJobType();
  $data['allCurrency']=$this->Client_model->getCurrencies();
    $this->load->view('client/all_css');
    $this->load->view('client/dashboard_header',$data);
    $this->load->view('client/createjob',$data);
    $this->load->view('client/footer');

    $this->load->view('client/all_js');
    $this->load->view('client/pagewise_js/jobForm_page.js');
  }


  //function for deactivate job
  function deactiveJob(){
  //echo 'here';exit;
    $id=$this->input->get('id');
  //echo $id;exit;
    $updateArr=array('job_approve'=>'1');
    $result=$this->Client_model->deactiveJob($updateArr,$id);
    if($result){
      $this->session->set_flashdata('successmsg', '<div class="alert alert-success"><a type="button" class="close" data-dismiss="alert" aria-hidden="true">×</a>Job has been deactivated successfully.</div>');
      redirect(base_url().'client/viewJobs');
    }else{

     $this->session->set_flashdata('errormsg', '<div class="alert alert-warning alert-dismissable"><a type="button" class="close" data-dismiss="alert" aria-hidden="true">×</a>Error while submitting.</div>'); 
   }
  }

  //function for activate job
  function activeJob(){

    $id=$this->input->get('id');
    $updateArr=array('job_approve'=>'0');
    $result=$this->Client_model->activeJob($updateArr,$id);
    if($result){
      $this->session->set_flashdata('successmsg', '<div class="alert alert-success"><a type="button" class="close" data-dismiss="alert" aria-hidden="true">×</a>Job has been activated successfully.</div>');
      redirect(base_url().'client/viewJobs');
    } else{

      $this->session->set_flashdata('errormsg', '<div class="alert alert-warning alert-dismissable"><a type="button" class="close" data-dismiss="alert" aria-hidden="true">×</a>Error while submitting.</div>');

    }
  }

  //function for updating a job
  function updateJob(){
   $client_session_data=$this->session->userdata(base_url().'client_login_session');
   $this->checkSession($client_session_data);
   $email=$client_session_data['email_id'];
    //$checkClient=$this->Client_model->checkClientData($email);
    //$data['clnt_id']=$checkClient->client_id;
   $id=$this->input->get('id');
   $data['getJobData']=$this->Client_model->getJobDataById($id);
   //echo '<pre>';print_r($data['getJobData']);exit;
   $client=$data['getJobData']->subclient_name;
   //echo $client;exit;
   if($client!=''){

    $data['officeName']=$client;
  }else{

    $data['officeName']=$data['getJobData']->parent_client;
  }
    //print_r($this->db->last_query());exit;
    //echo '<pre>';print_r($data['getJobData']);exit;
    //print_r($data['getJobData']);exit;
    //$skills = explode(',', $data['getJobData']->skills);
    //$allskills = $this->Client_model->getSkills();
    //echo "<pre>";print_r($allskils);exit;
    //$array_skills = array();
    //if(isset($skills) && !empty($skills)){
     // foreach ($skills as $key => $value) {
        //print_r($allskills[$value]);exit;
        # code...
       // $array_skills[] = array('id' => $value ,'text' => $allskills[$value]);

      //}
    //}
    //print_r($array_skills);exit;
   // $data['skills']=json_encode($array_skills);
   // print_r($data['skills']);exit;
  $data['allCountries']=$this->Client_model->getAllCountries();
  $data['allIndustries']=$this->Client_model->getAllIndustries();
  $data['allStatus']=$this->Client_model->getAllStatus();
  $data['allExperience']=$this->Client_model->getExperience();
  $data['allJobType']=$this->Client_model->getJobType();
  $data['allSkills']=$this->Client_model->getSkills();
  $data['allCurrency']=$this->Client_model->getCurrencies();
    //echo '<pre>';print_r($data['allStatus']);exit;
  $this->load->view('client/all_css');
  $this->load->view('client/dashboard_header',$data);
  $this->load->view('client/createjob',$data);
  $this->load->view('client/footer');

  $this->load->view('client/all_js');
  $this->load->view('client/pagewise_js/jobForm_page.js');
  }


  //function for loading subclient Login credential view
  function createLoginCredentials(){
    $data['subClientId']=$this->input->get('id'); 
    $id=$this->input->get('id');
    $client_session_data=$this->session->userdata(base_url().'client_login_session');
    $this->checkSession($client_session_data);

    $email=$client_session_data['email_id'];
    $checkClient=$this->Client_model->checkClientData($email);
    $data['clnt_id']=$checkClient->client_id;
    $fetchSubClientData=$this->Client_model->getSubClientDetails($id);


    error_reporting(0);
    $data['client_name']=$fetchSubClientData->subclient_name;
    
    //$data['email_id']=$fetchSubClientData->clnt_email;
    //$data['password']=md5($fetchSubClientData->clnt_password);
    //echo $data['clnt_id'];exit;
    $this->load->view('client/all_css');
    $this->load->view('client/dashboard_header',$data);
    $this->load->view('client/createSubClientCredentialView',$data);
    $this->load->view('client/footer');

    $this->load->view('client/all_js');
    $this->load->view('client/pagewise_js/createCredential_page.js');


  }


  //funtion for creating login credential
  function createSubClientCredentials(){
   require 'vendor/autoload.php';
   $sendgrid = new SendGrid("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");
   $client_email = new SendGrid\Email();

   $client_session_data=$this->session->userdata(base_url().'client_login_session');
   $this->checkSession($client_session_data);
   $id=$client_session_data['client_id'];
   //echo $id;exit;
   $firstName=$client_session_data['first_name'];
   $lastName=$client_session_data['last_name'];
   
   $emailId=$client_session_data['email_id'];
   $_POST = json_decode(file_get_contents('php://input'), true);
  // print_r($_POST);exit;
   if ($this->input->post()) {
    $this->form_validation->set_rules('first_name', 'First Name', 'trim|required|trim|xss_clean|strip_tags');
    //$this->form_validation->set_rules('last_name', 'Last Name', 'trim|required|trim|xss_clean|strip_tags');
    $this->form_validation->set_rules('email', 'Email', 'trim|required|trim|xss_clean|strip_tags');
    $this->form_validation->set_rules('password', 'Password', 'trim|required|trim|xss_clean|strip_tags');
    if ($this->form_validation->run() == TRUE) {

      $createLogin['client_id']=$id;
      $createLogin['subClient_id']=$this->input->post('subclient_id');
      $createLogin['clnt_first_name']=$this->input->post('first_name');
      //$createLogin['clnt_last_name']=$this->input->post('last_name');
      
      $createLogin['clnt_email']=$this->input->post('email');


      
      $createLogin['clnt_password']=md5($this->input->post('password'));
      $createLogin['flag']='1';
      //$createLogin['login_created']='1';
      $createLogin['role']='subclient';

     //echo '<pre>';print_r($createLogin);exit;
      $result=$this->Client_model->insertSubClientCredentials($createLogin);

      if($result){


        $fname=$this->input->post('first_name');
        //$lname=$this->input->post('last_name');
        $officeemail=$emailId;
        $suboffice_email=$this->input->post('email');
        $password=$this->input->post('password');
        $name=$fname;
        $officeName=$firstName.' '.$lastName;
        $subject = "The Login Credential for ".$name." is:";

        $message = "Dear ".$officeName."<br/>";

        $message .="The Login Credentials which you have created for ".$name."  is.<br />";
        $message .=" Email : ".$suboffice_email." .<br />Password :".$password.".<br />";
        //$headers = "CC: ".$suboffice_email."" . "\r\n";
        //$this->addTo($suboffice_email);
        $client_ccemail=$this->input->post('email');
        $mail=$this->sendMail($subject,$message,$officeemail,$client_ccemail);
        
        
        $data = array('status' => '1');
        $this->session->set_flashdata('successmsg', '<div class="alert alert-success"><a type="button" class="close" data-dismiss="alert" aria-hidden="true">×</a>Sub Client Login has been created successfully.</div>');
        //redirect(base_url().'client/createLoginCredentials');

      }else{



        $data = array('status' => '0');
        $this->session->set_flashdata('errormsg', '<div class="alert alert-warning alert-dismissable"><a type="button" class="close" data-dismiss="alert" aria-hidden="true">×</a>Error while submitting.</div>');
        //redirect(base_url().'client/createLoginCredentials');
      }
      
    }else{

      $data = array('status' => '2', 'error' => $this->form_validation->get_field_data(), 'postdata' => $this->input->post());

    }
  }else{

    $data=array('status'=>'3');
  }

  print_r(json_encode($data));

  }

  //function for viewing feedback form
  function viewFeedbackForm(){

    $client_data=$this->session->userdata(base_url().'client_login_session');
    $this->checkSession($client_data);
    $email=$client_data['email_id'];
   // $checkClient=$this->Client_model->checkClientData($email);
    //$data['clnt_id']=$checkClient->client_id;
    //$id=$client_data['client_id'];


    $this->load->view('client/all_css');
    $this->load->view('client/dashboard_header');
    $this->load->view('client/clientfeedback');
    $this->load->view('client/footer');

    $this->load->view('client/all_js');
    $this->load->view('client/pagewise_js/clientfeedback_page.js');


  }

  //function for viewing all credentials of subclients
  function viewAllCredentials(){

    $client_data=$this->session->userdata(base_url().'client_login_session');
    $this->checkSession($client_data);
    $clientId=$client_data['client_id'];
    $email=$client_data['email_id'];
    $checkClient=$this->Client_model->checkClientData($email);
    $data['clnt_id']=$checkClient->client_id;
    $data['fetchSubOfficesCredentials']=$this->Client_model->fetchAllSubOfficesCredentials($clientId);
    $this->load->view('client/all_css');
    $this->load->view('client/dashboard_header',$data);
    $this->load->view('client/viewCredentials',$data);
    $this->load->view('client/footer');
    
    $this->load->view('client/all_js');
    $this->load->view('client/pagewise_js/viewCredentials.js');
  }

  //function for inserting client feedback
  function insertClientFeedback(){
    $client_session_data=$this->session->userdata(base_url().'client_login_session');
    $id=$client_session_data['client_id'];
    $_POST = json_decode(file_get_contents('php://input'), true);
  //echo '<pre>';print_r($_POST);exit;
    if ($this->input->post()) {
      $this->form_validation->set_rules('member', 'Member', 'trim|required|trim|xss_clean|strip_tags');
      $this->form_validation->set_rules('location', 'Location', 'trim|required|trim|xss_clean|strip_tags');
      $this->form_validation->set_rules('staff_list', 'Staff List', 'trim|required|trim|xss_clean|strip_tags');
      $this->form_validation->set_rules('comments', 'Comments', 'trim|required|trim|xss_clean|strip_tags');
      $this->form_validation->set_rules('name', 'Name', 'trim|required|trim|xss_clean|strip_tags');
      $this->form_validation->set_rules('position', 'Position', 'trim|required|trim|xss_clean|strip_tags');
      $this->form_validation->set_rules('email', 'Email', 'trim|required|trim|xss_clean|strip_tags');
      if ($this->form_validation->run() == TRUE) {
        $create_feedback['client_id']=$id;

        $create_feedback['member']=$this->input->post('member');
        $create_feedback['location']=$this->input->post('location');
        $create_feedback['start_date']=date('Y-m-d',strtotime($this->input->post('start_date')));
        $create_feedback['end_date']=date('Y-m-d',strtotime($this->input->post('end_date')));
        $create_feedback['punctuality']=$this->input->post('punctuality');
        $create_feedback['apperance']=$this->input->post('apperance');
        $create_feedback['skill']=$this->input->post('skill');
        $create_feedback['performance']=$this->input->post('performance');

        $create_feedback['staff_list']=$this->input->post('staff_list');
        $create_feedback['comment']=$this->input->post('comments');
        $create_feedback['name']=$this->input->post('name');
        $create_feedback['position']=$this->input->post('position');
        $create_feedback['email']=$this->input->post('email');

  //echo '<pre>';print_r($create_feedback);exit;
        $result=$this->Client_model->clientFeedback($create_feedback);

        if($result){
          $data=array('status'=>'1');
          $this->session->set_flashdata('successmsg', '<div class="alert alert-success"><a type="button" class="close" data-dismiss="alert" aria-hidden="true">×</a>Your Feedback has been submitted successfully.</div>');

        }else{

          $data=array('status'=>'0');
          $this->session->set_flashdata('errormsg', '<div class="alert alert-warning"><a type="button" class="close" data-dismiss="alert" aria-hidden="true">×</a>Your Feedback has not been submitted successfully.</div>');
        }

      }else{

        $data = array('status' => '2', 'error' => $this->form_validation->get_field_data(), 'postdata' => $this->input->post());

      }

    }else{

      $data=array('status'=>'3');
    }

    print_r(json_encode($data));
  }

  //function for disabling client
  function disableCredential(){
    $id=$this->input->get('id');
    $fetchEmail=$this->Client_model->fetchClientEmail($id);
    $email=$fetchEmail->clnt_email;
    $firstname=$fetchEmail->clnt_first_name;
    $lastname=$fetchEmail->clnt_last_name;
    $name=$firstname.' '.$lastname;
    $subject = "The Login Credential has been deactivated:";

    $message = "Dear ".$name."<br/>";

    $message .="Your Login Credentials has been deactivated.<br />";
    $message .="Kindly contact to Main Office to activate your account.<br />";
            //$message .=" Email : ".$suboffice_email." .<br />Password :".$password.".<br />";


    $this->sendMail($subject,$message,$email);
    $updateArr=array('deactivate'=>'1');
    
    $updateArr=array(
     'is_delete'=>'1',
     'flag'=>'0'
     );
    $result=$this->Client_model->disableClientCredential($updateArr,$id);
    if($result){

      $this->session->set_flashdata('successmsg', '<div class="alert alert-success"><a type="button" class="close" data-dismiss="alert" aria-hidden="true">×</a>Login Credentials has been deactivated  successfully.</div>');
      redirect(base_url().'client/viewAllCredentials');
    }else{

      $this->session->set_flashdata('errormsg', '<div class="alert alert-warning"><a type="button" class="close" data-dismiss="alert" aria-hidden="true">×</a>Login Credentials has not been deactivated  successfully.</div>');

    }

  }

  //function for activating subclient login credential
  function activateCredential(){
    $id=$this->input->get('id');
    $fetchEmail=$this->Client_model->fetchClientEmail($id);
    $email=$fetchEmail->clnt_email;
    $firstname=$fetchEmail->clnt_first_name;
    $lastname=$fetchEmail->clnt_last_name;
    $name=$firstname.' '.$lastname;
    $subject = "The Login Credential has been activated:";

    $message = "Dear ".$name."<br/>";

    $message .="Your Login Credentials has been activated.<br />";
    $message .="Kindly use the previous credentials to Login.<br />";
            //$message .=" Email : ".$suboffice_email." .<br />Password :".$password.".<br />";


    $this->sendMail($subject,$message,$email);
    $updateArr=array(
     'is_delete'=>'0',
     'flag'=>'1'
     );
    $result=$this->Client_model->activateClientCredential($updateArr,$id);
    if($result){

      $this->session->set_flashdata('successmsg', '<div class="alert alert-success"><a type="button" class="close" data-dismiss="alert" aria-hidden="true">×</a>Login Credentials has been activated  successfully.</div>');
      redirect(base_url().'client/viewAllCredentials');
    }else{

      $this->session->set_flashdata('errormsg', '<div class="alert alert-warning"><a type="button" class="close" data-dismiss="alert" aria-hidden="true">×</a>Login Credentials has not been activated  successfully.</div>');

    }
  }

  //function for viewing analytics
  function viewDashboard(){

    $logged_in_userdata = $this->session->userdata(base_url().'client_login_session');
    $this->checkSession($logged_in_userdata);
    $email=$logged_in_userdata['email_id'];
    $role=$logged_in_userdata['role'];
    $id=$logged_in_userdata['client_id'];
    $data['firstname']=$logged_in_userdata['first_name'];
    $data['lastname']=$logged_in_userdata['last_name'];

     $data['checkPackageSevices'] = $this->Client_model->checkPackageSevices($id,$role);

    if($role == "mainclient"){

     $clientId=$logged_in_userdata['client_id'];
     $where=array('client_id'=>$id,'subClient_id'=>'0','role'=>$role);

   }else{

    $clientId=$logged_in_userdata['client_id'];
    $where=array('client_id'=>'0','subClient_id'=>$id,'role'=>$role);
  }

  $checkClient=$this->Client_model->checkClientData($email);
  $data['clnt_id']=$checkClient->client_id;

  $countSubClient=$this->Client_model->totalSubClients($clientId);
  $data['totalSubClients']=$countSubClient->id;

  $countJobs=$this->Client_model->totalJobs($where);

  $data['totalJobs']=$countJobs->j_id;
      //$this->checkSession($logged_in_userdata);

  $package=$this->Client_model->getPackageCount();
      //echo "<pre>";print_r($data['packageCount']);exit;
  $data['packageCount']=$package->packagescount;

  $date1 = $this->input->post('start_date');
  if ($date1 != '') {
    $start_date = str_replace('/', '-', $date1);
  } else {

    $start_date = date('Y-m-d', strtotime('Jan 01'));
  }
  $date2 = $this->input->post('end_date');
  if ($date2 != '') {
    $end_date = str_replace('/', '-', $date2);
  } else {
    $end_date = date('Y-m-d', strtotime('Dec 31'));
  }
  $client_data=$this->session->userdata(base_url().'client_login_session');
  $this->checkSession($client_data);
  $email=$client_data['email_id'];

  $checkClient=$this->Client_model->checkClientData($email);
  $data['clnt_id']=$checkClient->client_id;
    //echo $data['clnt_id'];exit;
  error_reporting(0);
  $id=$client_data['client_id'];

  $countOffices=$this->Client_model->countOffices($start_date,$end_date,$id);
  $countJobs=$this->Client_model->countJobs($start_date,$end_date,$id);

  $temp = array();
  foreach ($countOffices as $key => $value) {
   $temp[$value->month] = $value;
  }

  $temp1=array();
  foreach ($countJobs as $key => $value) {
    $temp1[$value->month] = $value;
  }

  $month_array = array();
  for ($m = 1; $m <= 12; $m++) {
    $month = date('F', mktime(0, 0, 0, $m, 1, date('Y')));
    if ($m < 10) {
      $month_array['0' . $m] = $month;
    } else {
      $month_array[$m] = $month;
    }
  }

  $data['month_array']=$month_array;
  $data['client_array']=$temp;
  $data['job_array']=$temp1;

  $this->load->view('client/all_css');
  $this->load->view('client/dashboard_header',$data);
  $this->load->view('client/analytics',$data);
  $this->load->view('client/footer');
  $this->load->view('client/all_js');
  //$this->load->view('client/pagewise_js/client_login_page.js');

  }

  //function for viewing sub offices jobs
  function viewSubOfficesJobs(){

    $client_session_data=$this->session->userdata(base_url().'client_login_session');
    $this->checkSession($client_session_data);
    $email=$client_session_data['email_id'];
    $role=$client_session_data['role'];
    
    // $checkClient=$this->Client_model->checkClientData($email);
    // $data['clnt_id']=$checkClient->client_id;
    if($role == 'mainclient'){

      $id=$client_session_data['client_id'];
    //$where=array('client_id',$id);
    }else{
      $id=$client_session_data['subClient_id'];
    //$where=array('subClient_id',$id);
    }
    
    $fetchClientId=$this->Client_model->getSubClientId($id);
    //$subclient_id=$fetchClientId->subClient_id;
    //$data['fetchSubOfficesJobs']=$this->Client_model->getSubOfficesJobs($subclient_id);

    $this->load->view('client/all_css');
    $this->load->view('client/dashboard_header');
    $this->load->view('client/viewSubOfficesJobs',$data);
    $this->load->view('client/footer');
    $this->load->view('client/all_js');
    $this->load->view('client/pagewise_js/viewSubOfficesJobs.js');

  }

  //function for credential data
  function credentialData(){
    //echo 'here';
    $client_data=$this->session->userdata(base_url().'login_session');
    $clientId=$client_data['client_id'];
    $requestData = $_REQUEST;
    $totalData = $this->Client_model->get_credential_list($clientId);
    $totalFiltered = $totalData = count($totalData);
    $totalFiltered = $this->Client_model->getCredentialfilteredList($requestData,$clientId);
    $totalFiltered = count($totalFiltered);

    $data = array();
    $userlist = $this->Client_model->getCredentialList($requestData,$clientId);
    if (!empty($userlist)) {
      $count=1;
              foreach ($userlist as $user) { // preparing an array
                $nestedData = array();
                
                  //$view = "<img height='100' width='100' src='" . base_url() . "uploads/" . $user->logo . "'>";

                if($user->is_delete=='0'){

                  $view1="<a onclick='return confirmClientDisable();' href='".base_url()."client/disableCredential?id=".$user->id."' ><i class='fa fa-check-square-o' aria-hidden='true'></i></a>";
                }else{

                 $view1="<a onclick='return confirmClientActivate();' href='".base_url()."client/activateCredential?id=".$user->id."' ><i class='fa fa-minus-circle' aria-hidden='true'></i></a>";
               }
               $nestedData[] = $count++;
               $nestedData[] = $user->clnt_email;
                  //$nestedData[] = $user->link;
  //                $nestedData[] = $user->logo;
                  //$nestedData[] = $view;
               $nestedData[] = $view1;
               $data[] = $nestedData;
             }
           }

           $json_data = array(
              "draw" => intval($requestData['draw']), // for every request/draw by clientside , they send a number as a parameter, when they recieve a response/data they first check the draw number, so we are sending same number in draw. 
              "recordsTotal" => intval($totalData), // total number of records
              "recordsFiltered" => intval($totalFiltered), // total number of records after searching, if there is no searching then totalFiltered = totalData
              "data" => $data   // total data array
              );
           echo json_encode($json_data);


         }


  //function for client data
         function clientsData() {

          $client_data = $this->session->userdata(base_url() . 'client_login_session');
          $role = $client_data['role'];
  //$clientId=$client_data['client_id'];
          if ($role == 'mainclient') {

            $clientId = $client_data['client_id'];
          } else {

            $clientId = $client_data['client_id'];
          }

          $requestData = $_REQUEST;
          $totalData = $this->Client_model->get_client_list($clientId);
          $totalFiltered = $totalData = count($totalData);
          $totalFiltered = $this->Client_model->getClientfilteredList($requestData, $clientId);
          $totalFiltered = count($totalFiltered);

          $data = array();
          $userlist = $this->Client_model->getClientList($requestData, $clientId);

          if (!empty($userlist)) {

            $count=1;
              foreach ($userlist as $user) { // preparing an array
                $nestedData = array();

                $view = '<a href="' . base_url() . 'client/createLoginCredentials?id= ' . $user->id . '" class="btn btn-info btn-sm"><strong>Create Credential</strong></a>';
                $active = $user->deactivate;
                  //echo $active;
                $view1 = '<ul class="list-unstyled list-inline" style="padding-left: 0px;"><li><a href="' . base_url() . 'client/clientInfo?id= ' . $user->id . '" title="client Information"><i class="fa fa-eye" aria-hidden="true"></i></a></li><li><a href="' . base_url() . 'client/editSubClientDetails?id=' . $user->id . '" title="Update Information"><i class="fa fa-pencil" aria-hidden="true"></i></a>';
                if ($active == "0") {

                  $view1.= '<li onclick="return confirmDeactivate();"><a href="' . base_url() . 'client/deactivateSubClientDetails?id=' . $user->id . '" title="Activate Office"><i class="fa fa-check-square-o" aria-hidden="true"></i></a></li>';

                } else {

                  $view1.='<li onclick="return confirmActivate();"><a href="' . base_url() . 'client/activateSubClientDetails?id=' . $user->id . '" title="Deactivate Office"><i class="fa fa-minus-circle" aria-hidden="true"></i></a></li>';
                }
                $view1.='</ul>';

                $view2='<a href="' . base_url() . 'client/clientInfo?id= ' . $user->id . '">'.$user->subclient_name.'</a>';
                $nestedData[] = $count++;
                $nestedData[] = $view2;
                $nestedData[] = $user->contact_number;
                $nestedData[] = $user->clnt_first_name;
                $nestedData[] = $view;
                $nestedData[] = $view1;
                $data[] = $nestedData;
              }
            }

            $json_data = array(
              "draw" => intval($requestData['draw']), // for every request/draw by clientside , they send a number as a parameter, when they recieve a response/data they first check the draw number, so we are sending same number in draw.
              "recordsTotal" => intval($totalData), // total number of records
              "recordsFiltered" => intval($totalFiltered), // total number of records after searching, if there is no searching then totalFiltered = totalData
              "data" => $data   // total data array
              );
            echo json_encode($json_data);
          }



      // function for show subscribed packages detail 

        public function  subscribedPackagesData()
        {
          
          $client_data = $this->session->userdata(base_url() . 'client_login_session');
          $role = $client_data['role'];
          $client_id = $client_data['client_id'];

          


           $requestData = $_REQUEST;

            

            $totalData = $this->Client_model->get_package_list($client_id,$role);
          $totalFiltered = $totalData = count($totalData);
          $totalFiltered = $this->Client_model->getSubscribedPackagesfilteredList($requestData, $client_id,$role);
          $totalFiltered = count($totalFiltered);
              
          $subscribedPackages = $this->Client_model->getPackagesList($requestData, $client_id,$role);




            // /$query = "select * from tbl_"




            if (!empty($subscribedPackages)) {
            
            $count=1;
              foreach ($subscribedPackages as $packages) { // preparing an array

                $nestedData[] = $count++;
                
                $nestedData[] = $packages->package_title;
                $nestedData[] = $packages->amount;
                $nestedData[] = date('jS M Y', strtotime($packages->created_date));
                $nestedData[] = date('jS M Y', strtotime($packages->validity_date));
               
                $data[] = $nestedData;
              }
            }

            $json_data = array(
              "draw" => intval($requestData['draw']), // for every request/draw by clientside , they send a number as a parameter, when they recieve a response/data they first check the draw number, so we are sending same number in draw.
              "recordsTotal" => intval($totalData), // total number of records
              "recordsFiltered" => intval($totalFiltered), // total number of records after searching, if there is no searching then totalFiltered = totalData
              "data" => $data   // total data array
              );



          //  echo "<pre>"; print_r($json_data); exit;
            echo json_encode($json_data);



        }


      //function for viewing main jobs
          function mainJobsData(){
            $client_data=$this->session->userdata(base_url().'client_login_session');

            $role=$client_data['role'];
            $client=$client_data['company_name'];
            //echo $client;exit;
            if($role == 'mainclient'){

              $clientId=$client_data['client_id'];

            }
            else
            {
              $clientId=$client_data['client_id'];
            }

            if($role == 'subclient'){
             //$where=array('client_id'=>'0','subClient_id'=>$clientId);
             $where='where tbl_job_create.client_id="0" AND tbl_job_create.subClient_id="'.$clientId.'" AND tbl_job_create.role="'.$role.'" ';
             //print_r($where);exit;
           }else{


               //$where=array('subClient_id'=>'0','client_id'=>$clientId);
             $where='where tbl_job_create.subClient_id="0" AND tbl_job_create.client_id="'.$clientId.'" AND tbl_job_create.role="'.$role.'"';
               //print_r($where);exit;
           }

           $requestData = $_REQUEST;
           $totalData = $this->Client_model->get_mainjobs_list($where);
           $totalFiltered = $totalData = count($totalData);
           $totalFiltered = $this->Client_model->getmainjobsfilteredList($requestData,$where);
           $totalFiltered = count($totalFiltered);

           $data = array();
           $userlist = $this->Client_model->getmainjobsList($requestData,$where);
           if (!empty($userlist)) {
            $count=1;
              foreach ($userlist as $user) { // preparing an array
                $nestedData = array();
                $deactivate=$user->job_approve;
                  //echo $deactivate;
                $view = '<ul class="list-unstyled list-inline" style="padding-left: 0px;">';
                if($deactivate == '0'){


                  $view.='<li onclick="return confirmDeactivate();"><a href="'.base_url().'client/deactiveJob?id= '.$user->j_id.'" title="Activate Jobs"><i class="fa fa-check-square-o" aria-hidden="true"></i></a></li>';
                }else{

                 $view.='<li onclick="return confirmActivate();"><a href="'.base_url().'client/activeJob?id= '.$user->j_id.'" title="Deactivate Jobs"><i class="fa fa-minus-circle" aria-hidden="true"></i></a></li>';

               }

               $view.='<li><a href="'.base_url().'client/updateJob?id='.$user->j_id.'" title="Update Job Information"><i class="fa fa-pencil" aria-hidden="true"></i></a></li>';
               $view.='</ul>';

               $view1='<a href="'.base_url().'client/updateJob?id='.$user->j_id.'">'.$user->job_title.'</a>';

     //             if($user->is_delete=='0'){
               
     //  $view1="<a onclick='return confirmClientDisable();' href='".base_url()."client/disableCredential?id=".$user->id."' ><i class='fa fa-check-square-o' aria-hidden='true'></i></a>";
     //            }else{
               
     // $view1="<a onclick='return confirmClientActivate();' href='".base_url()."client/activateCredential?id=".$user->id."' ><i class='fa fa-minus-circle' aria-hidden='true'></i></a>";
     //            }
               if($user->subclient_name!=''){

                $client_name=$user->subclient_name;
              }else{

                $client_name=$client;
              }
              $nestedData[] = $count++;
              $nestedData[] = $view1;
              $nestedData[] = $user->recruiter;
              $nestedData[] = $user->status_name;
              $nestedData[] = $user->min_salary.'-'.$user->max_salary;
              $nestedData[] = $user->currency_name;
              $nestedData[] = $client_name;
              $nestedData[] = date('jS M Y',strtotime($user->created_date));
              $nestedData[] = date('jS M Y',strtotime($user->open_date));
              
              $nestedData[] = $user->job_type;
              
              
              $nestedData[] = $view;
              $data[] = $nestedData;
            }
          }

          $json_data = array(
              "draw" => intval($requestData['draw']), // for every request/draw by clientside , they send a number as a parameter, when they recieve a response/data they first check the draw number, so we are sending same number in draw. 
              "recordsTotal" => intval($totalData), // total number of records
              "recordsFiltered" => intval($totalFiltered), // total number of records after searching, if there is no searching then totalFiltered = totalData
              "data" => $data   // total data array
              );
          echo json_encode($json_data);



        }

      //function for subOffices jobs.
        function subOfficesJobsData(){
         $client_data=$this->session->userdata(base_url().'client_login_session');
         $client=$client_data['company_name'];
           //echo $client;exit;
         $role=$client_data['role'];
         if($role == 'mainclient'){

          $id=$client_data['client_id'];

        }
        else
        {
          $id=$client_data['subClient_id'];
        }



        $fetchClientId=$this->Client_model->getSubClientId($id);
          //echo '<pre>';print_r($fetchClientId);exit;
        foreach($fetchClientId as $key){

          $subclient_id[]=$key->subClient_id;
           //echo $subclient_id;
        }
        $string=implode(',', $subclient_id);
          //echo $string;exit;
        $requestData = $_REQUEST;
        $totalData = $this->Client_model->get_subOfficejobs_list($string);
        $totalFiltered = $totalData = count($totalData);
        $totalFiltered = $this->Client_model->getsubOfficejobsfilteredList($requestData,$string);
        $totalFiltered = count($totalFiltered);

        $data = array();
        $userlist = $this->Client_model->getsubOfficejobsList($requestData,$string);

        if (!empty($userlist)) {
         $count=1;
              foreach ($userlist as $user) { // preparing an array
                $nestedData = array();
                $deactivate=$user->job_approve;
                  //echo $deactivate;
                $view = '<ul class="list-unstyled list-inline" style="padding-left: 0px;">';
                if($deactivate == '0'){


                  $view.='<li onclick="return confirmDeactivate();"><a href="'.base_url().'client/deactiveJob?id= '.$user->j_id.'" title="Activate Jobs"><i class="fa fa-check-square-o" aria-hidden="true"></i></a></li>';
                }else{

                 $view.='<li onclick="return confirmActivate();"><a href="'.base_url().'client/activeJob?id= '.$user->j_id.'" title="Deactivate Jobs"><i class="fa fa-minus-circle" aria-hidden="true"></i></a></li>';

               }

               $view.='<li><a href="'.base_url().'client/updateJob?id='.$user->j_id.'" title="Update Job Information"><i class="fa fa-pencil" aria-hidden="true"></i></a></li>';
               $view.='</ul>';
               $view1='<a href="'.base_url().'client/updateJob?id='.$user->j_id.'">'.$user->job_title.'</a>';

     //             if($user->is_delete=='0'){
               
     //  $view1="<a onclick='return confirmClientDisable();' href='".base_url()."client/disableCredential?id=".$user->id."' ><i class='fa fa-check-square-o' aria-hidden='true'></i></a>";
     //            }else{
               
     // $view1="<a onclick='return confirmClientActivate();' href='".base_url()."client/activateCredential?id=".$user->id."' ><i class='fa fa-minus-circle' aria-hidden='true'></i></a>";
     //            }
               if($user->subclient_name == null){

                $client_name=$client;
              }else{

                $client_name=$user->subclient_name;
              }
              $nestedData[] = $count++;
              $nestedData[] = $view1;

              $nestedData[] = $user->recruiter;
              $nestedData[] = $user->status_name;
              $nestedData[] = $user->min_salary.'-'.$user->max_salary;
              $nestedData[] = $user->currency_name;
              $nestedData[] = $user->subclient_name;
              $nestedData[] = date('jS M Y',strtotime($user->created_date));
              $nestedData[] = date('jS M Y',strtotime($user->open_date));
              $nestedData[] = $user->job_type;
              
              
              
              $nestedData[] = $view;
              $data[] = $nestedData;
            }
          }
  //exit;
          $json_data = array(
              "draw" => intval($requestData['draw']), // for every request/draw by clientside , they send a number as a parameter, when they recieve a response/data they first check the draw number, so we are sending same number in draw. 
              "recordsTotal" => intval($totalData), // total number of records
              "recordsFiltered" => intval($totalFiltered), // total number of records after searching, if there is no searching then totalFiltered = totalData
              "data" => $data   // total data array
              );

          echo json_encode($json_data);

        }

      //function for change password
        function changePassword(){

          $client_session_data=$this->session->userdata(base_url().'client_login_session');
          $this->checkSession($client_session_data);
          $this->load->view('client/all_css');
          $this->load->view('client/dashboard_header');
          $this->load->view('client/changepassword');
          $this->load->view('client/footer');
          $this->load->view('client/all_js');
          $this->load->view('client/pagewise_js/changepassword.js');

        }   

    //function for changing client password
        function changeClientPassword(){
          $_POST = json_decode(file_get_contents('php://input'), true);
  //echo '<pre>';print_r($_POST);exit;
          if ($this->input->post()) {
            $this->form_validation->set_rules('oldpassword', 'Old Password', 'trim|required|trim|xss_clean|strip_tags');
            $this->form_validation->set_rules('password', 'Password', 'trim|required|trim|xss_clean|strip_tags');
            if ($this->form_validation->run() == TRUE) {
    //echo 'here';exit;
              $password = md5($this->input->post('oldpassword'));
  //echo $password;exit;
              $client_data=$this->session->userdata(base_url().'login_session');
              $client_id=$client_data['client_id'];
              $success = $this->Client_model->check_old_password($client_id, $password);
  //echo '<pre>';print_r($success);exit;
              if(!empty($success)){
    //echo 'here';exit;
                $pass = md5($this->input->post('password'));
                $updateArr=array(
                  'clnt_password'=>$pass


                  );
  //echo '<pre>';print_r($updateArr);exit;
                $response = $this->Client_model->change_password($client_id, $updateArr);
  //echo '<pre>';print_r($this->db->last_query());exit;
                if(!empty($response)){
    //echo 'here';exit;
                  $data = array('status' => '1');
                  $this->session->set_flashdata('successmsg', '<div class="alert alert-success"><a type="button" class="close" data-dismiss="alert" aria-hidden="true">×</a>Password has been changed successfully.</div>');
                }else{
    //echo 'there';exit;
                 $data = array('status' => '0');
                 $this->session->set_flashdata('errormsg', '<div class="alert alert-warning alert-dismissable"><a type="button" class="close" data-dismiss="alert" aria-hidden="true">×</a>Password has not been changed successfully.</div>');
               }


             }else{
    //echo 'there';exit;
              $data = array('status' => '0');
              $this->session->set_flashdata('errormsg', '<div class="alert alert-warning alert-dismissable"><a type="button" class="close" data-dismiss="alert" aria-hidden="true">×</a>Incorrect old password.</div>');
            }
          }else{

            $data = array('status' => '2', 'error' => $this->form_validation->get_field_data(), 'postdata' => $this->input->post()); 
          }


        }else{
          $data = array('status' => '3');
          $this->session->set_flashdata('errormsg', '<div class="alert alert-warning alert-dismissable"><a type="button" class="close" data-dismiss="alert" aria-hidden="true">×</a>All fields are mandatory.</div>');


        }
        print_r(json_encode($data));




      }  

      //function for viewing company online form
      function companyProfile(){
        $client_session_data=$this->session->userdata(base_url().'client_login_session');

        $this->checkSession($client_session_data);
        $client_id=$client_session_data['client_id'];
        $fetchCompanyLogo=$this->Client_model->getCompanyLogo($client_id);
        $data['fetchCompanyData']=$this->Client_model->getCompanyDetails($client_id);
        $data['fetchCompanyinfo']=$this->Client_model->fetchClientData($client_id);
        $data['fetchBannerPic']=$this->Client_model->getBannerDetails($client_id);
        error_reporting(0);
        if(!empty($fetchCompanyLogo)){
         $data['company_logo']=$fetchCompanyLogo->company_logo;
       }else{

         $data['company_logo']='Your-logo-here2.png';  
       }
       $this->load->view('client/all_css');
       $this->load->view('client/dashboard_header');
       $this->load->view('client/company_profile',$data);
       $this->load->view('client/footer');
       $this->load->view('client/all_js');
       $this->load->view('client/pagewise_js/company_profile.js');


     }

      //function for inserting company profile deatils
     function insertClientProfile(){
      $client_data=$this->session->userdata(base_url().'client_login_session');
      $client_id=$client_data['client_id'];
      $_POST = json_decode(file_get_contents('php://input'), true);
      //print_r($_POST);
      if ($this->input->post()) {
       $this->form_validation->set_rules('company_name', 'Company Profile', 'trim|required|trim|xss_clean|strip_tags');
       $this->form_validation->set_rules('company_overview', 'Company Overview', 'trim|required|trim|xss_clean|strip_tags');
       $this->form_validation->set_rules('founded', 'Founded', 'trim|required|trim|xss_clean|strip_tags');
       $this->form_validation->set_rules('headquarters', 'Head Quarters', 'trim|required|trim|xss_clean|strip_tags');
       $this->form_validation->set_rules('staff', 'Total Staff', 'trim|required|trim|xss_clean|strip_tags');
       $this->form_validation->set_rules('web_address', 'Web Address', 'trim|required|trim|xss_clean|strip_tags');
       if ($this->form_validation->run() == TRUE) {

         $insertProfile['company_name']=$this->input->post('company_name');
         $insertProfile['client_id']=$client_id;
         $insertProfile['company_overview']=$this->input->post('company_overview');
         $insertProfile['founded']=$this->input->post('founded');
         $insertProfile['headquarters']=$this->input->post('headquarters');
         $insertProfile['staff']=$this->input->post('staff');
         $insertProfile['web_address']=$this->input->post('web_address');
         //$insertProfile['rating']=$this->input->post('ratings');
         $insertProfile['banner_pic']=$this->input->post('filename');

       //echo '<pre>';print_r($insertProfile);exit;
         $checkProfileData=$this->Client_model->getCompanyDetails($client_id);

         if(!empty($checkProfileData)){

           $result=$this->Client_model->updateProfileData($insertProfile,$client_id);
         }else{
           $result=$this->Client_model->insertProfileData($insertProfile);
         }

         if($result){
          $data=array('status'=>'1');
          $this->session->set_flashdata('successmsg', '<div class="alert alert-success"><a type="button" class="close" data-dismiss="alert" aria-hidden="true">×</a>Company Profile data has been saved successfully.</div>');

        }else{

          $data=array('status'=>'0');
          $this->session->set_flashdata('errormsg', '<div class="alert alert-warning"><a type="button" class="close" data-dismiss="alert" aria-hidden="true">×</a>Company Profile data has not been saved successfully.</div>');
        }
      }else{

        $data = array('status' => '2', 'error' => $this->form_validation->get_field_data(), 'postdata' => $this->input->post()); 
      }

    }else{

     $data = array('status' => '3');
     $this->session->set_flashdata('errormsg', '<div class="alert alert-warning alert-dismissable"><a type="button" class="close" data-dismiss="alert" aria-hidden="true">×</a>All fields are mandatory.</div>');
   }

   print_r(json_encode($data));

  }

      //function for uploading banner image
  function uploadBanner(){

    $client_session_data=$this->session->userdata(base_url().'client_login_session');
    $role=$client_session_data['role'];
    if($role == "mainclient"){

     $id=$client_session_data['client_id'];

   }else{

    $id=$client_session_data['client_id'];
  }
    //$id=$client_session_data['client_id'];

  $filename=$_FILES['file']['name'];
  $target = 'banner_uploads/';
  $file = $target.$filename;

  if(move_uploaded_file($_FILES['file']['tmp_name'], $file))
  {

    print_r(json_encode(array('filename'=>$filename)));

  }else{


    echo 'not uploded';
  }


  }

  //function for storing company logo
  function sbmtCompanyLogo(){
    define('UPLOAD_DIR', 'client_uploads/client_logo/');
    $client_session_data=$this->session->userdata(base_url().'client_login_session');
    $client_id=$client_session_data['client_id'];
    $_POST = json_decode(file_get_contents('php://input'), true);
     //print_r($_POST);exit;     
    if ($this->input->post()) {

      $image_name = $_POST['userfile'];
      $img = $_POST['imgdata'];

         //  echo $image_name." ".$img; exit;
      $img = str_replace('data:image/png;base64,', '', $img);
      $img = str_replace(' ', '+', $img);
      $decoded = base64_decode($img);
      $uid=$client_id;
      $file = UPLOAD_DIR .$image_name;
      $file_name=$image_name;
            //echo $file_name;exit;
      $success = file_put_contents($file, $decoded);

      if($success)
      {


        $checkCompanyLogo=$this->Client_model->getCompanyLogo($client_id);
        if(!empty($checkCompanyLogo)){
          $updateArr=array('company_logo'=>$file_name);
          $upload = $this->Client_model->updateCompanyLogo($updateArr,$client_id);
        }else{
         $insertArr=array('company_logo' => $file_name,'client_id'=>$client_id);
         $upload = $this->Client_model->saveCompanyLogo($insertArr);
       }
           //echo '<pre>';print_r($this->db->last_query());exit;

       if($upload)
       {

         $data = array('status' =>'1');
         $this->session->set_flashdata('successmsg', '<div class="alert alert-success"><a type="button" class="close" data-dismiss="alert" aria-hidden="true">×</a>Company Logo has been uploaded successfully.</div>');
       }
     }

   }else{

     $data = array('status' => '2');
     $this->session->set_flashdata('errormsg', '<div class="alert alert-warning alert-dismissable"><a type="button" class="close" data-dismiss="alert" aria-hidden="true">×</a>Please select Company Logo properly.</div>');


   }

   print_r(json_encode($data));


  }

  //function for inserting client information
  function inserClientInfo(){
   $client_data=$this->session->userdata(base_url().'client_login_session');
   $client_id=$client_data['client_id'];
   $_POST = json_decode(file_get_contents('php://input'), true);
      //print_r($_POST);
   if ($this->input->post()) {
    $this->form_validation->set_rules('mobile_number', 'Mobile Number', 'trim|required|trim|xss_clean|strip_tags');
    $this->form_validation->set_rules('address', 'Address', 'trim|required|trim|xss_clean|strip_tags');
    $this->form_validation->set_rules('email', 'Email', 'trim|required|trim|xss_clean|strip_tags');
    $this->form_validation->set_rules('website', 'Website', 'trim|required|trim|xss_clean|strip_tags');
    $this->form_validation->set_rules('fb', 'Facebook Profile Link', 'trim|required|trim|xss_clean|strip_tags');
    $this->form_validation->set_rules('twitter', 'Twitter Profile Link', 'trim|required|trim|xss_clean|strip_tags');
    $this->form_validation->set_rules('linkedin', 'Linkedin Profile Link', 'trim|required|trim|xss_clean|strip_tags');
    $this->form_validation->set_rules('instagram', 'Instagram Profile Link', 'trim|required|trim|xss_clean|strip_tags');
    $this->form_validation->set_rules('googleplus', 'Google Plus Profile', 'trim|required|trim|xss_clean|strip_tags');
    $this->form_validation->set_rules('pinterest', 'Pinterest Profile Link', 'trim|required|trim|xss_clean|strip_tags');
    if ($this->form_validation->run() == TRUE) {
     $insertClientInfo['client_id']=$client_id;
     $insertClientInfo['mobile_number']=$this->input->post('mobile_number');
     $insertClientInfo['address']=$this->input->post('address');
     $insertClientInfo['email']=$this->input->post('email');
     $insertClientInfo['website']=$this->input->post('website');
     $insertClientInfo['fb']=$this->input->post('fb');
     $insertClientInfo['twitter']=$this->input->post('twitter');
     $insertClientInfo['linkedin']=$this->input->post('linkedin');
     $insertClientInfo['instagram']=$this->input->post('instagram');
     $insertClientInfo['googleplus']=$this->input->post('googleplus');
     $insertClientInfo['pinterest']=$this->input->post('pinterest');

     $checkClientInfo=$this->Client_model->fetchClientData($client_id);

     if(!empty($checkClientInfo)){
      $result=$this->Client_model->updateClientInformation($insertClientInfo,$client_id);
  //echo '<pre>';print_r($this->db->last_query());exit;
    }else{
      $result=$this->Client_model->insertClientInformation($insertClientInfo);

    }



    if($result){
      $data=array('status'=>'1');
      $this->session->set_flashdata('successmsg', '<div class="alert alert-success"><a type="button" class="close" data-dismiss="alert" aria-hidden="true">×</a>Company Contact and Social data has been saved successfully.</div>');

    }else{

      $data=array('status'=>'0');
      $this->session->set_flashdata('errormsg', '<div class="alert alert-warning"><a type="button" class="close" data-dismiss="alert" aria-hidden="true">×</a>Company Contact and Social data has not been saved successfully.</div>');
    }

  }else{

    $data = array('status' => '2', 'error' => $this->form_validation->get_field_data(), 'postdata' => $this->input->post());   
  }

  }else{
    $data = array('status' => '3');
    $this->session->set_flashdata('errormsg', '<div class="alert alert-warning alert-dismissable"><a type="button" class="close" data-dismiss="alert" aria-hidden="true">×</a>All fields are mandatory.</div>');


  }
  print_r(json_encode($data));

  }

   //function for inserting banner pic
  function inserBannerPic(){
   $client_id=$client_data['client_id'];
   $_POST = json_decode(file_get_contents('php://input'), true);
     // print_r($_POST);exit;
   if ($this->input->post()) {
    $insert_arr['banner_pic']=$this->input->post('filename');
    $insert_arr['client_id']=$client_id;
    $checkBannerData=$this->Client_model->getBannerDetails($client_id);

    if(!empty($checkBannerData)){
      $result=$this->Client_model->updateBannerData($insert_arr,$client_id);

    }else{
     
     $result=$this->Client_model->insertBannerData($insert_arr);
   }

   if($result){
     $data=array('status'=>'1');
     $this->session->set_flashdata('successmsg', '<div class="alert alert-success"><a type="button" class="close" data-dismiss="alert" aria-hidden="true">×</a>Banner Photo has been saved successfully.</div>');
   }else{
    $data=array('status'=>'0');
    $this->session->set_flashdata('errormsg', '<div class="alert alert-warning"><a type="button" class="close" data-dismiss="alert" aria-hidden="true">×</a>Banner Photo has not been saved successfully.</div>');

  }

  }else{

    $data = array('status' => '2', 'error' => $this->form_validation->get_field_data(), 'postdata' => $this->input->post());
  }
  print_r(json_encode($data));

  }

   //function for getting Worldpay response
  function reponsePayments(){
   if(isset($_POST)){
    echo 'here';
    print_r($_POST);
  }else{
   echo 'there';
   print_r($_GET);
  }


  }

  // function for success payment


  function success()
  {

    $client_data=$this->session->userdata(base_url().'client_login_session');
    $client_id=$client_data['client_id'];
    $role = $client_data['role'];



    if(!empty($_GET))
    {

       // echo "<pre>"; print_r($_GET); 

        $txnDeatil = array(

           'client_id' => $client_id,
           'txn_no' => $_GET['transId'] ,
           'package_id' => $_GET['cartId'],
           'txn_status' => $_GET['transStatus'],
           'amount'   =>$this->session->userdata('package_amt'),
           'role'     => $role
           );

      //  echo "<pre>"; print_r($txnDeatil);

        $lastId = $this->Client_model->savePackageTxnDetail($txnDeatil);

        if($lastId)
        {

            //$savedTxnDetails = $this->Client_model->getSavedtxnDetail($lastId);
             
             $purchasedRecord = array(

                      'client_id' => $client_id,
                      'package_id' => $_GET['cartId'],
                      'txn_tbl_id' => $lastId,
                      'txn_no'  => $_GET['transId'] ,
                      'amount'     => $this->session->userdata('package_amt'),
                      'role' =>   $role 
                       );

          $this->Client_model->savePurchaseDetail($purchasedRecord);
         
           // redirect(base_url().'client/viewPackages');


       $this->load->view('client/all_css');
       $this->load->view('client/dashboard_header');
       $this->load->view('client/paymentSuccess');
       $this->load->view('client/footer');
       $this->load->view('client/all_js');
       //$this->load->view('client/pagewise_js/company_profile.js');

        }


    }else{

      redirect(base_url().'client/viewPackages');
    }

    // echo "success"; exit;

  }



  function error()
  {

      $client_data=$this->session->userdata(base_url().'client_login_session');
      $client_id=$client_data['client_id'];
      $role = $client_data['role'];

     if(!empty($_GET)) 
     {
       //echo "<pre>"; print_r($_GET); 

      $txnDeatil = array(
           'client_id' =>$client_id,
           'txn_no' => $_GET['transId'] ,
           'package_id' => $_GET['cartId'],
           'txn_status' => $_GET['transStatus'],
           'amount'   =>$this->session->userdata('package_amt'),
           'role'     =>$role
           );

        // echo "<pre>"; print_r($txnDeatil);


         $lastId = $this->Client_model->savePackageTxnDetail($txnDeatil);


         if($lastId)
         {

             $savedTxnDetails = $this->Client_model->getSavedtxnDetail($lastId);
             
           //  redirect(base_url().'client/viewPackages');


             $this->load->view('client/all_css');
             $this->load->view('client/dashboard_header');
             $this->load->view('client/paymentError');
             $this->load->view('client/footer');
             $this->load->view('client/all_js');

   
         }

     }

     else{

      redirect(base_url().'client/viewPackages');
     }

    //echo "error"; exit;
  }


  // function for view subscribed packages
   
   public function  subscribedPackages()
   {

     
      $client_data=$this->session->userdata(base_url().'client_login_session');
      $client_id=$client_data['client_id'];
      $role = $client_data['role'];

             $this->load->view('client/all_css');
             $this->load->view('client/dashboard_header');
             $this->load->view('client/purchasedPackages');
             $this->load->view('client/footer');
             $this->load->view('client/all_js');
             $this->load->view('client/pagewise_js/purchasedPackages.js');
   }


  // function for Preview post jobs

   public function previewJob()
   {

    echo 123; exit;      


   }



  //function for logout..
  function logout(){

   $this->session->unset_userdata('loginsession');
   redirect(base_url().'client');
  }




  }
  ?>
