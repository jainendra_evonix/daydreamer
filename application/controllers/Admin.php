<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Admin extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */

	//function for calling constructor..
	public function __construct() {
    parent::__construct();
    $this->load->library('session');
    $this->load->library('form_validation');
    $this->load->model('Admin_model');
		//$this->load->model('admin_model');
    $this->load->library('email');
    $this->load->helper('url');
    $this->load->helper('security');

  }

	//function for loading index page i.e Login page...
  public function index()
  {
  	//echo "here";exit;
    $this->load->view('admin/all_css');
    $this->load->view('admin/header');
    $this->load->view('admin/index');
    $this->load->view('admin/footer');

    $this->load->view('admin/all_js');
    $this->load->view('admin/pagewise_js/login.js');
  }

  //function for checking session
  function checkSession($logged_in_userdata){

   if (!empty($logged_in_userdata)) {
    return TRUE;
  } else {
    redirect('');
  }


}


  //function for creating account
function registration(){
  $this->load->view('admin/all_css');
  $this->load->view('admin/header');
  $this->load->view('admin/register');
  $this->load->view('admin/footer');

  $this->load->view('admin/all_js');
  $this->load->view('admin/pagewise_js/registeradmin_page.js');


}

//   //function for loading dashboard
// function viewDashboard(){


//     //echo "<pre>";print_r($data['package_data']);exit;
//   $adminSessionData=$this->session->userdata(base_url().'login_session');
//   $this->checkSession($adminSessionData);
//   $data['firstname']=$adminSessionData['first_name'];
//   $data['lastname']=$adminSessionData['last_name'];
//   $totalPackages=$this->Admin_model->getPackages();
//   $data['countPackage']=$totalPackages->count;
//   $totalOffices=$this->Admin_model->getAllOffices();
//   $data['countOffice']=$totalOffices->offices;

//   $totalCandidates=$this->Admin_model->getAllCandidates();
//   $data['countCandidates']=$totalCandidates->candidates;

//   $totalJobs=$this->Admin_model->getAllJobs();
//   $data['countJobs']=$totalJobs->jobs;

//   //$data['fetchProfilePic']=$this->Admin_model->getProfilePicture();

//   $this->load->view('admin/all_css');
//   $this->load->view('admin/dashboard_header');
//   $this->load->view('admin/dashboard',$data);
//   $this->load->view('admin/footer');
//   $this->load->view('admin/all_js');
//   $this->load->view('admin/pagewise_js/dashboard.js');


// }

  //function for creating and viewing a package
function createPackage(){
  
  $adminSessionData=$this->session->userdata(base_url().'login_session');
  $this->checkSession($adminSessionData);
  $admin_id=$adminSessionData['admin_id'];
  //$date=date('Y-m-d');

  $data['package_data']=$this->Admin_model->fetchPackageData($admin_id);
  $data['package_details']=$this->Admin_model->getPackageDetails();
  $data['showService']=$this->Admin_model->get_service();
  $this->load->view('admin/all_css');
  $this->load->view('admin/dashboard_header');
  $this->load->view('admin/package',$data);
  $this->load->view('admin/footer');
  $this->load->view('admin/all_js');
  $this->load->view('admin/pagewise_js/package.js');

}

function addNewPackage(){
$admin_data=$this->session->userdata(base_url().'login_session');
$this->checkSession($admin_data);
$admin_id=$admin_data['admin_id'];
$id=$this->input->get('id'); 
$data['package_id']=$id;
$data['fetchJobAdv']=$this->Admin_model->getJobAdv($admin_id);
$data['fetchService']=$this->Admin_model->getService($admin_id);
$data['fetchPackageData']=$this->Admin_model->getPackageData($id);
//echo '<pre>';print_r($data['fetchPackageData']);exit;
error_reporting(0);
$fetchedservice=$data['fetchPackageData']->service;
$data['service']=json_decode($fetchedservice);
//echo '<pre>';print_r($data['service']);exit;
$data['fetchYears']=$this->Admin_model->getYears();
$data['fetchDays']=$this->Admin_model->getDays();
$this->load->view('admin/all_css');
$this->load->view('admin/dashboard_header');
$this->load->view('admin/newPackage',$data);
$this->load->view('admin/footer');
$this->load->view('admin/all_js');
$this->load->view('admin/pagewise_js/newPackage.js');    
    
    
}


//function for inserting job advertisement
function insertJobAdv(){
$admin_data=$this->session->userdata(base_url().'login_session');
$admin_id=$admin_data['admin_id'];
$job_adds=$this->input->post('job_adds');
$type=$this->input->post('type');
$duration=$this->input->post('duration');

$insertArr=array('admin_id'=>$admin_id,'job_adds'=>$job_adds,'type'=>$type,'duration'=>$duration);
$result=$this->Admin_model->insertJobAdvertisement($insertArr);

if($result){

  echo 'success';
}else{

  echo 'error';
}

} 


//function for saving service name
function saveServiceName(){
$admin_data=$this->session->userdata(base_url().'login_session');
$admin_id=$admin_data['admin_id'];
$service=$this->input->post('service_name');
$package_id=$this->input->post('package_id');
$insertArr=array('admin_id'=>$admin_id,'service_name'=>$service,'package_id'=>$package_id);
//echo '<pre>';print_r($insertArr);exit;
$result=$this->Admin_model->insertService($insertArr);
if($result){

  echo 'success';
}else{

  echo 'error';
}
}

//function for deleting service
function deleteService(){
$id=$this->input->get('id');
$package_id=$this->input->get('package_id');
$result=$this->Admin_model->deleteServiceById($id);
if($result && $package_id!=''){
$this->session->set_flashdata('successmsg', '<div class="alert alert-success alert-dismissable"><a type="button" class="close" data-dismiss="alert" aria-hidden="true">×</a>Service has been deleted successfully.</div>');
redirect(base_url().'admin/addNewPackage?id='.$package_id.'');
}else{

$this->session->set_flashdata('successmsg', '<div class="alert alert-success alert-dismissable"><a type="button" class="close" data-dismiss="alert" aria-hidden="true">×</a>Service has been deleted successfully.</div>');
redirect(base_url().'admin/addNewPackage');
}
}

//function for inserting new package
function insertNewPackage(){
$adminSessionData=$this->session->userdata(base_url().'login_session');
$this->checkSession($adminSessionData);
$admin_id=$adminSessionData['admin_id'];
$_POST = json_decode(file_get_contents('php://input'), true);
//echo '<pre>';print_r($_POST);
$this->form_validation->set_rules('package_title', 'Package Title', 'trim|required|trim|xss_clean|strip_tags');
$this->form_validation->set_rules('reference_number', 'Reference Number', 'trim|required|trim|xss_clean|strip_tags');
$this->form_validation->set_rules('description', 'Package Description', 'trim|required|trim|xss_clean|strip_tags');
$this->form_validation->set_rules('currency', 'Currency', 'trim|required|trim|xss_clean|strip_tags');
$this->form_validation->set_rules('amount', 'Amount', 'trim|required|trim|xss_clean|strip_tags');
$this->form_validation->set_rules('offer_currency', 'Offer Currency', 'trim|required|trim|xss_clean|strip_tags');
$this->form_validation->set_rules('offer_amount', 'Offer Amount', 'trim|required|trim|xss_clean|strip_tags');
$this->form_validation->set_rules('radpub', 'Publish', 'trim|required|trim|xss_clean|strip_tags');



if ($this->form_validation->run() == TRUE) {
$insert_package['admin_id']=$admin_id;
$id=$this->input->post('package_id');
//echo $id;exit;
$insert_package['package_title']=$this->input->post('package_title');
$insert_package['reference_number']=$this->input->post('reference_number');
$insert_package['package_description']=$this->input->post('description');
//$image=$this->input->post('filename');
$insert_package['price_currency']=$this->input->post('currency');
$insert_package['price_amount']=$this->input->post('amount');
$insert_package['offer_currency']=$this->input->post('offer_currency');
$insert_package['offer_amount']=$this->input->post('offer_amount');
$insert_package['publish']=$this->input->post('radpub');
$insert_package['validity_date']=date('y-m-d',strtotime($this->input->post('validity_date')));
$insert_package['facebook']=$this->input->post('radio1');
$insert_package['twitter']=$this->input->post('radio2');
$insert_package['linkedin']=$this->input->post('radio3');
$insert_package['package_end_date']=$this->input->post('radio');


$service=json_encode($this->input->post('test'));

//echo '<pre>';print_r($insert_package);exit;
$chechPackageData=$this->Admin_model->checkPackageData($id);

if(isset($service) && $service!=''){

$insert_package['service']=$service;
}else{

$insert_package['service']=$chechPackageData->service;
}

//echo '<pre>';print_r($chechPackageData);exit;
if(!empty($chechPackageData)){
$result=$this->Admin_model->updatePackageData($insert_package,$id);

}else{

$result=$this->Admin_model->insertPackageData($insert_package);

}

if($result){
$data = array('status' => '1');
$this->session->set_flashdata('successmsg', '<div class="alert alert-success"><a type="button" class="close" data-dismiss="alert" aria-hidden="true">×</a>Your request has been completed successfully.</div>');


}else{
$data = array('status' => '0');
$this->session->set_flashdata('errormsg', '<div class="alert alert-warning alert-dismissable"><a type="button" class="close" data-dismiss="alert" aria-hidden="true">×</a>Error while submitting.</div>');

}
}else{

 $data = array('status' => '2', 'error' => $this->form_validation->get_field_data(), 'postdata' => $this->input->post());  
}
print_r(json_encode($data));

}

//function for uploading image
function upload_image(){
  $filename=$_FILES['file']['name'];
  $target = 'package_images/';
  $file = $target.'-'.$filename;

  if(move_uploaded_file($_FILES['file']['tmp_name'], $file))
  {

    print_r(json_encode(array('filename'=>$filename)));

  }else{


    echo 'not uploded';
  }


}



//   //function for creating account of admin
//   function create_account(){
//     $_POST = json_decode(file_get_contents('php://input'), true);
//   //echo "<pre>";print_r($_POST);exit;
//     if ($this->input->post()) {

//       $this->form_validation->set_rules('firstname', 'FirstName', 'trim|required|trim|xss_clean|strip_tags');
//       $this->form_validation->set_rules('lastname', 'LastName', 'trim|required|trim|xss_clean|strip_tags');
//       $this->form_validation->set_rules('city', 'City', 'trim|required|trim|xss_clean|strip_tags');
//       $this->form_validation->set_rules('country', 'Country', 'trim|required|trim|xss_clean|strip_tags');
//       $this->form_validation->set_rules('telephone', 'Telephone', 'trim|required|trim|xss_clean|strip_tags|numeric');
//       $this->form_validation->set_rules('email', 'Email', 'trim|required|trim|xss_clean|strip_tags');
//       $this->form_validation->set_rules('validate_email', 'Email', 'trim|required|trim|xss_clean|strip_tags|valid_email');
//       $this->form_validation->set_rules('validate_email', 'Email', 'trim|required|trim|xss_clean|strip_tags|valid_email');
//       $this->form_validation->set_rules('password', 'Password', 'trim|required|trim|xss_clean|strip_tags');
//       $this->form_validation->set_rules('validate_password', 'Password', 'trim|required|trim|xss_clean|strip_tags');
//       $this->form_validation->set_rules('agree', 'Agreement', 'trim|required|trim|xss_clean|strip_tags');

//       if ($this->form_validation->run() == TRUE) {

//         $info['admin_firstName']=$this->input->post('firstname');
//         $info['admin_lastName']=$this->input->post('lastname');
//         $info['admin_city']=$this->input->post('city');
//         $info['admin_country']=$this->input->post('country');
//         $info['admin_telephone']=$this->input->post('telephone');
//         $info['admin_email']=$this->input->post('email');

//         $info['admin_password']=md5($this->input->post('password'));
//         //echo "<http://localhost/daydreamer/adminDashboardpre>";print_r($info);exit;
//         $emailid=$this->input->post('email');
//         // $check_email=$this->Admin_model->checkEmailId($emailid);


//         $result=$this->Admin_model->add_admin($info);
//         //echo "<pre>";print_r($this->db->last_query());exit;
//         if($result){
//           $url=base_url()."admin/activateLogin/".$result;
//           $fname=$this->input->post('firstname');
//           $lname=$this->input->post('lastname');
//           $email=$this->input->post('email');
//           $password=$this->input->post('password');
//           $name=$fname.' '.$lname;

//           $subject = "Thank you for registration";

//           $message = "Dear ".$name."<br/>";
//           $message .="Greetings from Day Dreamer!<br />";
//           $message .="Thank you for registration.<br />";
//           $message .="Below are your login details .<br />";
//           $message .=" Email : ".$email." .<br />Password :".$password.".<br />";
//           $message .="To activate your account, Please click the below link.<br />";
//           $message .=".$url.";

//           $this->sendMail($subject,$message,$email);
//           $data = array('status' => '1');
//           $this->session->set_flashdata('successmsg', '<div class="alert alert-success"><a type="button" class="close" data-dismiss="alert" aria-hidden="true">×</a>Registration has been done successfully.</div>');

//         }else{
//          $data = array('status' => '0');
//          $this->session->set_flashdata('errormsg', '<div class="alert alert-warning alert-dismissable"><a type="button" class="close" data-dismiss="alert" aria-hidden="true">×</a>Error while submitting.</div>');


//        }

//      }else{


//        $data = array('status' => '2', 'error' => $this->form_validation->get_field_data(), 'postdata' => $this->input->post());
//      }

//    }else{
//     $data = array('status' => '3');
//     $this->session->set_flashdata('errormsg', '<div class="alert alert-warning alert-dismissable"><a type="button" class="close" data-dismiss="alert" aria-hidden="true">×</a>All fields are mandatory.</div>');

//   }

//   print_r(json_encode($data));

// }

  //function for login activation

function activateLogin($token){

 $actvntm = date("Y-m-d H:i:s");
 $data = array
 (
  'flag'=>1,
  'activation_token' => $token,
  'activation_time' => $actvntm,
  );
 $status = $this->Admin_model->activateAdminLogin($data,$token);
 if($status)
 {
  redirect(base_url().'admin');
}


}

  //function for login
public function check_login(){
 $_POST = json_decode(file_get_contents('php://input'), true);
   //echo "<pre>";print_r($_POST);exit;
 if($this->input->post()) {
  $this->form_validation->set_rules('email', 'Email', 'trim|required|trim|xss_clean|strip_tags');
  $this->form_validation->set_rules('password', 'Password', 'trim|required|trim|xss_clean|strip_tags');

  if ($this->form_validation->run() == TRUE) {
   $emailaddress = $this->input->post('email');
   $password = md5($this->input->post('password'));
   $response = $this->Admin_model->check_admin_login($emailaddress, $password);
 //echo "<pre>";print_r($response);exit;
   if(!empty($response)){

    foreach($response as $key){
      $session_array=array(
        'admin_id'=>$key->id,
        'first_name'=>$key->admin_firstName,
        'last_name'=>$key->admin_lastName,
        'email_id'=>$key->admin_email,
        'profile_picture'=>$key->profile_pic,
        'loginsession' => true

        );
    //echo "<pre>";print_r($session_array);exit;
      $this->session->set_userdata(base_url().'login_session',$session_array);
    }
      //echo "<pre>";print_r($result);exit;
     //  $checkOnlineData=$this->Client_model->checkOnlineFormData($response->id);
     //  //echo "<pre>";print_r($checkOnlineData);exit;
     //  if(!empty($checkOnlineData)){

     //    $data = array('status' => '4');
     //  }
     //  else{
     //  $data = array('status' => '1');
     //  //redirect(base_url().'client/viewDashboard');

     // }
    $data = array('status' => '1');
  }else{

   $data = array('status' => '0');
   $this->session->set_flashdata('errormsg', '<div class="alert alert-warning alert-dismissable"><a type="button" class="close" data-dismiss="alert" aria-hidden="true">×</a>Wrong username or password combination !.</div>');
 }

}else{
  $data = array('status' => '2', 'error' => $this->form_validation->get_field_data(), 'postdata' => $this->input->post());

}


}else{

  $data = array('status' => '3');
  $this->session->set_flashdata('errormsg', '<div class="alert alert-warning alert-dismissable"><a type="button" class="close" data-dismiss="alert" aria-hidden="true">×</a>All fields are mandatory.</div>');


}

print_r(json_encode($data));


}

  //function for sending mail.
function sendMail($subject,$message,$email){
  require 'vendor/autoload.php';
  $sendgrid = new SendGrid("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");
  $client_email = new SendGrid\Email();
  $client_email->addTo($email)
  ->setFrom('support@evonixtech.com')
  ->setFromName("DayDreamer Admin")
  ->setSubject($subject)
  ->setHtml($message);
  $sendgrid->send($client_email);
}


//function for adding package
function insertPackage(){
  //echo '1237';exit;
  $client_data=$this->session->userdata(base_url().'login_session');
  $admin_id=$client_data['admin_id'];
//echo $client_id;exit;
  $_POST = json_decode(file_get_contents('php://input'), true);
//echo "<pre>";print_r($_POST);exit;
  if ($this->input->post()) {
    $this->form_validation->set_rules('package_title', 'Package Title', 'trim|required|trim|xss_clean|strip_tags');
    $this->form_validation->set_rules('package_description', 'Package Description', 'trim|required|trim|xss_clean|strip_tags');
    $this->form_validation->set_rules('amount', 'Amount', 'trim|required|trim|xss_clean|strip_tags');
    $this->form_validation->set_rules('validity', 'Validity Date', 'trim|required|trim|xss_clean|strip_tags');

    if ($this->form_validation->run() == TRUE) {
  //echo 'here';exit;

      $insert_package['admin_id']=$admin_id;
      $insert_package['package_title'] = $this->input->post('package_title');
      $insert_package['package_description'] = $this->input->post('package_description');
      $insert_package['amount']=$this->input->post('amount');

      $insert_package['validity_date']=date('Y-m-d',strtotime($this->input->post('validity')));
      $insert_package['job_adds']=$this->input->post('total_jobs');
      $insert_package['premium_jobs']=$this->input->post('premium_jobs');

//echo "<pre>";print_r($insert_package);exit;
      $addPackage=$this->Admin_model->addClientPackage($insert_package);
      if($addPackage){

        $data=array('status'=>'1');
        $this->session->set_flashdata('successmsg', '<div class="alert alert-success alert-dismissable"><a type="button" class="close" data-dismiss="alert" aria-hidden="true">×</a>Package has been added successfully</div>');
      }else{

       $data=array('status'=>'0');

     }

   }else{


   }
 }else{

   $data = array('status' => '3');
   $this->session->set_flashdata('errormsg', '<div class="alert alert-warning alert-dismissable"><a type="button" class="close" data-dismiss="alert" aria-hidden="true">×</a>All fields are mandatory</div>');
 }
 print_r(json_encode($data));
}

//function for deleting package
function deletePackage(){
  $id=$this->input->get('id');
//echo $id;exit;
  $result=$this->Admin_model->deletePackageById($id);
  if($result){
    $this->session->set_flashdata('successmsg', '<div class="alert alert-success alert-dismissable"><a type="button" class="close" data-dismiss="alert" aria-hidden="true">×</a>Package has been deleted successfully</div>');

    redirect(base_url().'admin/createPackage');
  }else{

    $this->session->set_flashdata('errormsg', '<div class="alert alert-warning alert-dismissable"><a type="button" class="close" data-dismiss="alert" aria-hidden="true">×</a>Package has not been deleted successfully</div>');
    redirect(base_url().'admin/createPackage');
  }
}

//function for fetching package data
function get_package(){

  $_POST = json_decode(file_get_contents('php://input'), true);
  if ($this->input->post('id')) {
    $id = $this->input->post('id');
    $package = $this->Admin_model->getpackage($id);
    $val_date=date('Y-m-d',strtotime($package->validity_date));
            //echo '<pre>';print_r($package);exit;
    $package = (array) $package;
    $package['validity_date'] = $val_date;
    $data['package'] = $package;
            //echo '<pre>';print_r($data['package']);exit;
  }
  print_r(json_encode($data));
}

//function for updating package details..
function update_package($id){
	//echo $id;exit;
  $_POST = json_decode(file_get_contents('php://input'), true);
  //echo "<pre>";print_r($_POST);exit;
  if ($this->input->post()) {
   $this->form_validation->set_rules('package_title', 'Package Title', 'trim|required|trim|xss_clean|strip_tags');
   $this->form_validation->set_rules('package_description', 'Package Description', 'trim|required|trim|xss_clean|strip_tags');
   $this->form_validation->set_rules('amount', 'Amount', 'trim|required|trim|xss_clean|strip_tags');
   $this->form_validation->set_rules('validity', 'Validity Date', 'trim|required|trim|xss_clean|strip_tags');
    

   $update_package=array(

    'package_title'=>$this->input->post('package_title'),
    'package_description'=>$this->input->post('package_description'),
    'amount'=>$this->input->post('amount'),
    //'validity_date'=>date('Y-m-d',$this->input->post('validity'))
    'validity_date'=>date('Y-m-d',strtotime($this->input->post('validity'))),
    'job_adds' =>$this->input->post('update_jobs'),
    'premium_jobs' =>$this->input->post('premiumJobs')
    );
//echo "<pre>";print_r($update_package);exit;

   $result=$this->Admin_model->updatePackage($update_package,$id);
 //echo "<pre>";print_r($this->db->last_query());exit;

   if($result){
    $data=array('status'=>'1');
    $this->session->set_flashdata('successmsg', '<div class="alert alert-success alert-dismissable"><a type="button" class="close" data-dismiss="alert" aria-hidden="true">×</a>Package has been updated successfully</div>');
  }else{

    $data=array('status'=>'2');
  }

}
print_r(json_encode($data));
}

 //function to check email.

function check_email_exist(){

  $email=$this->input->post('email');
  $result=$this->Admin_model->checkEmailExist($email);
  // echo "<pre>";print_r($result);
  if(!empty($result)){

    echo "success";
  }else{

    echo "error";
  }
}
//function for resetting password
function resetpassword($randomkeystring=''){
  $keystring['data'] = $this->Admin_model->check_randomkey_string($randomkeystring);
  if(empty($keystring['data']))
  {
    $rediect_path = base_url().'adminlogin';
    redirect($redirect_path, 'refresh');
  }
  $this->load->view('admin/all_css');
  $this->load->view('admin/header');
  $this->load->view('admin/resetpassword',$keystring);
  $this->load->view('admin/footer');
  $this->load->view('admin/all_js');
  $this->load->view('admin/pagewise_js/resetpassword.js');

}

 //function for generating random string
function generateRandomString($length = 16) {  
  $characters   = 'abcdefghijklmnopqrstuvwxyz0123456789';
  $randomString = '';
  for ($i = 0; $i < $length; $i++) {
    $randomString .= $characters[rand(0, strlen($characters) - 1)];
  }
  return $randomString;
}



//function for forget password
function forgetPassword(){
  $_POST = json_decode(file_get_contents('php://input'), true);
  if ($this->input->post()) {
    $this->form_validation->set_rules('email', 'Email ID', 'trim|required|trim|xss_clean|strip_tags|valid_email');
    if ($this->form_validation->run() == TRUE) {
      $emailaddress = $this->input->post('email');
      $randomkeystring = $this->generateRandomString();
      $url = base_url();
      $response = $this->Admin_model->check_admin_forgot_password($emailaddress);
      //echo '<pre>';print_r($response);exit;
      if(!empty($response)){
       $updatekeystring = $this->Admin_model->update_keystring($emailaddress,$randomkeystring);
      //echo '<pre>';print_r($this->db->last_query());exit;
       require 'vendor/autoload.php';
       $sendgrid = new SendGrid("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");
       $email = new SendGrid\Email();
       $email->addTo($emailaddress)
       ->addCc('sonal@evonix.co')
       ->setFrom('support@daydreamer.com')
       ->setFromName('Day Dreamer')
       ->setSubject("Here's the link to reset your password")
       ->setHtml("You recently requested a password reset.<br />To change your password<br /><a href='".$url."admin/resetpassword/$randomkeystring'>Click here</a><br />The link will expire in 12 hours, so be sure to use it right away.<br /><br /> <br />Regards, <br />Support Team.");
       if($sendgrid->send($email)) {
        $data = array('status' => '1', 'message' => '<div class="alert alert-success">An email has been sent to '.$emailaddress.'. Please click on the link provided in the mail to reset your password.</div>');
      } 
      else {
        $data = array('status' => '4', 'message' => '<div class="alert alert-danger">Error in sending Email.</div>');
      }
    // $data = array('status' => '2', 'error' => $this->form_validation->get_field_data(), 'postdata' => $this->input->post());
    }else{

      $data = array('status' => '0', 'message' => '<div class="alert alert-danger"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>The email ID you have entered does not exist !.</div>');
    }
  }else{

  }
}else{

  $data = array('status' => '3');
}
print_r(json_encode($data));
}

//function for checking reset password
function check_reset_password(){
  $_POST = json_decode(file_get_contents('php://input'), true);
// print_r($_POST);exit;  
  if ($this->input->post()) {
    $this->form_validation->set_rules('npassword', 'New Password', 'trim|required|trim|xss_clean|strip_tags');
    $this->form_validation->set_rules('cpassword', 'Confirm Password', 'trim|required|trim|xss_clean|strip_tags|matches[npassword]');

    if ($this->form_validation->run() == TRUE) {
      $url=base_url();
      $emailaddress = $this->input->post('emailaddress');
      $npassword = $this->input->post('npassword');

      $updatepassword = $this->Admin_model->update_password($emailaddress,$npassword);
      //echo '<pre>';print_r($updatepassword);exit;
      if ($updatepassword) {
        $data = array('status' => '1', 'message' => '<div class="alert alert-success">You have successfully changed your password. Please sign in with your new password.<br><a href="'.$url.'admin" class="btn btn-twitter" style="padding: 2px 8px;">Sign In</a></div>');
      }
      else {
        //$data = array('status' => '0');
        $data = array('status' => '0', 'message' => '<div class="alert alert-danger"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>Oops... Something went wrong !.</div>');
      }
    }else{


    }

  }else{

    $data = array('status' => '3');
  }
  print_r(json_encode($data));
}


//function for creating new office
function createOffice(){
  $adminSessionData=$this->session->userdata(base_url().'login_session');
  $this->checkSession($adminSessionData);
  $data['allIndustries']=$this->Admin_model->getIndustries();
  $data['allCountries']=$this->Admin_model->getCountries();
  $this->load->view('admin/all_css');
  $this->load->view('admin/dashboard_header');
  $this->load->view('admin/createclient',$data);
  $this->load->view('admin/footer');
  $this->load->view('admin/all_js');
  $this->load->view('admin/pagewise_js/createOffice.js');



}

//function for viewing all offices
function viewAllOffices(){
  //$id=$this->input->post('id');
 // echo $id;
  $adminSessionData=$this->session->userdata(base_url().'login_session');
  $this->checkSession($adminSessionData);
  $data['allOffices']=$this->Admin_model->getOffices();
  

  
  $this->load->view('admin/all_css');
  $this->load->view('admin/dashboard_header');
  $this->load->view('admin/viewallclients',$data);
  $this->load->view('admin/footer');
  $this->load->view('admin/all_js');
  $this->load->view('admin/pagewise_js/viewOffices.js');


}

//function for viewing all candidates
function viewAllCandidates(){
  $adminSessionData=$this->session->userdata(base_url().'login_session');
  $this->checkSession($adminSessionData);

 $candidate_first_name=$this->input->post('candidate_first_name') ? $this->input->post('candidate_first_name')  :'';
  
  $city=$this->input->post('city') ? $this->input->post('city') : '';
  $candidate_last_name=$this->input->post('candidate_last_name') ? $this->input->post('candidate_last_name')  :'';
  $email=$this->input->post('email') ? $this->input->post('email') : '';
  $requestData = $_REQUEST;
   if($this->input->post()){
    $array = array(
      'first_name'=>$candidate_first_name,
      'city'=>$city,
      'candidate_last_name'=>$candidate_last_name,
      'email'=>$email
      );
    $this->session->set_userdata('advance_filter',$array);
    $data['allCandidates']=$this->Admin_model->getCandidatesList($requestData,$candidate_first_name,$candidate_last_name,$email,$city);
   }else
     $data['allCandidates']=$this->Admin_model->getCandidates();
  $this->load->view('admin/all_css');
  $this->load->view('admin/dashboard_header');
  $this->load->view('admin/viewallcandidates',$data);
  $this->load->view('admin/footer');
  $this->load->view('admin/all_js');
  $this->load->view('admin/pagewise_js/viewCandidates.js');


}

//function for viewing all jobs
function viewJobs(){
  $adminSessionData=$this->session->userdata(base_url().'login_session');
  $this->checkSession($adminSessionData);
  $job_title=$this->input->post('job_title') ? $this->input->post('job_title')  :'';
  $min_salary=$this->input->post('min_salary') ? $this->input->post('min_salary')  :'';
  $max_salary=$this->input->post('max_salary') ? $this->input->post('max_salary')  :'';
  $job_type=$this->input->post('job_type') ? $this->input->post('job_type')  :'';
  $client_name=$this->input->post('client_name') ? $this->input->post('client_name')  :'';
  $data['jobType']=$this->Admin_model->getJobType();
  $requestData = $_REQUEST;
  if(!empty($_POST)){
  $array = array(
      'job_title'=>$job_title,
      'min_salary'=>$min_salary,
      'max_salary' =>$max_salary,
      'job_type'=>$job_type,
      'client_name'=>$client_name
      );
  $this->session->set_userdata('job_filter',$array);
  $data['allJobs']=$this->Admin_model->getMainJobfilteredList($requestData,$job_title,$min_salary,$max_salary,$job_type,$client_name);  
  }else{

    $data['allJobs']=$this->Admin_model->fetchJobs();
    
  }
  $this->load->view('admin/all_css');
  $this->load->view('admin/dashboard_header');
  $this->load->view('admin/viewjobs',$data);
  $this->load->view('admin/footer');
  $this->load->view('admin/all_js');
  $this->load->view('admin/pagewise_js/viewJobs.js');

  
}

//function for viewing suboffices jobs
function viewSubOfficesJobs(){
  $adminSessionData=$this->session->userdata(base_url().'login_session');
  $this->checkSession($adminSessionData);
  if(!empty($_POST)){

    $adminSessionData=$this->session->userdata(base_url().'login_session');
    $this->checkSession($adminSessionData);
    $job_title=$this->input->post('job_title');
    $closing_date=str_replace('/', '-', $this->input->post('closing_date'));
  // echo $closing_date;exit;
    $industry=$this->input->post('industry');
    $salary=$this->input->post('salary');

  //$adminSessionData=$this->session->userdata(base_url().'login_session');
  //$this->checkSession($adminSessionData);
    $this->load->model('Client_model');
    $data['allCountries']=$this->Admin_model->getCountries();
    $data['allIndustries']=$this->Client_model->getAllIndustries();
    $data['allJobs']=$this->Admin_model->getSubOfficesJobs($job_title,$closing_date,$salary,$industry);
  }else{

    $data['allJobs']=$this->Admin_model->fetchSubOfficesJobs();
    //echo '<pre>';print_r($data['allJobs']);exit;
  }
  //echo '<pre>';print_r($data['allJobs']);exit;
  $this->load->view('admin/all_css');
  $this->load->view('admin/dashboard_header');
  $this->load->view('admin/viewSubOfficesjobs',$data);
  $this->load->view('admin/footer');
  $this->load->view('admin/all_js');
  $this->load->view('admin/pagewise_js/viewSubOfficesJobs.js');


}

//function for disapproving a job
function disApproveJob(){
  $id=$this->input->get('id');
  $updateArr=array(
   'job_approve'=>'1'

   );
  $result=$this->Admin_model->disapproveClientJob($updateArr,$id);
  if($result){
    $this->session->set_flashdata('successmsg', '<div class="alert alert-success alert-dismissable"><a type="button" class="close" data-dismiss="alert" aria-hidden="true">×</a>Job has been disapproved successfully</div>');
    redirect(base_url().'admin/viewJobs');
  }else{
    $this->session->set_flashdata('errormsg', '<div class="alert alert-warning alert-dismissable"><a type="button" class="close" data-dismiss="alert" aria-hidden="true">×</a>Job has not been disapproved successfully</div>');

  }
}

//function for approving a job
function approveJob(){
  $id=$this->input->get('id');
  $updateArr=array(
   'job_approve'=>'0'

   );
  $result=$this->Admin_model->approveClientJob($updateArr,$id);
  if($result){
    $this->session->set_flashdata('successmsg', '<div class="alert alert-success alert-dismissable"><a type="button" class="close" data-dismiss="alert" aria-hidden="true">×</a>Job has been approved successfully</div>');
    redirect(base_url().'admin/viewJobs');
  }else{
    $this->session->set_flashdata('errormsg', '<div class="alert alert-warning alert-dismissable"><a type="button" class="close" data-dismiss="alert" aria-hidden="true">×</a>Job has not been approved successfully</div>');

  }



}

//function for viewing job description
function viewJobDescription(){
  $adminSessionData=$this->session->userdata(base_url().'login_session');
  $this->checkSession($adminSessionData);
  $id=$this->input->get('id');
  $data['job_description']=$this->Admin_model->getJobDescription($id);
  //echo '<pre>';print_r($data['job_description']);exit;
  $client=$data['job_description']->subclient_name;
 // echo $client;exit;
  if($client!=''){
   $data['client_name']=$data['job_description']->subclient_name;

 }else{

   $data['client_name']=$data['job_description']->parent_client;
 }
  //echo '<pre>';print_r($data['job_description']);exit;
 $this->load->view('admin/all_css');
 $this->load->view('admin/dashboard_header');
 $this->load->view('admin/job_info',$data);
 $this->load->view('admin/footer');
 $this->load->view('admin/all_js');
 $this->load->view('admin/pagewise_js/dashboard.js');

}

//function for disabling user
function disableUser(){
  $id=$this->input->get('id');

  $updateArr=array('deactivate'=>'1','flag'=>'0');
  $result=$this->Admin_model->disableUserByAdmin($updateArr,$id);
//echo '<pre>';print_r($result);exit;
  if($result){
    $this->session->set_flashdata('successmsg', '<div class="alert alert-success alert-dismissable"><a type="button" class="close" data-dismiss="alert" aria-hidden="true">×</a>User has been deactivated successfully</div>');
    redirect(base_url().'admin/viewAllCandidates');
  }else{
    $this->session->set_flashdata('errormsg', '<div class="alert alert-warning alert-dismissable"><a type="button" class="close" data-dismiss="alert" aria-hidden="true">×</a>User has not been deactivated successfully</div>');
    redirect(base_url().'admin/viewAllCandidates');
  }
}

//function for enabling user
function EnableUser(){
  $id=$this->input->get('id');
  $updateArr=array('deactivate'=>'0','flag'=>'1');
  $result=$this->Admin_model->enableUserByAdmin($updateArr,$id);
  if($result){
    $this->session->set_flashdata('successmsg', '<div class="alert alert-success alert-dismissable"><a type="button" class="close" data-dismiss="alert" aria-hidden="true">×</a>User has been activated successfully</div>');
    redirect(base_url().'admin/viewAllCandidates');
  }else{
    $this->session->set_flashdata('errormsg', '<div class="alert alert-warning alert-dismissable"><a type="button" class="close" data-dismiss="alert" aria-hidden="true">×</a>User has not been activated successfully</div>');
    redirect(base_url().'admin/viewAllCandidates');

  }
}

//function for disabling office
function disableClient(){
  $id=$this->input->get('id');
  $updateArr=array('deactivate'=>'1','flag'=>'0');
  $result=$this->Admin_model->enableOfficeByAdmin($updateArr,$id);
  $fetchEmail=$this->Admin_model->fetchClientEmail($id);
  $email=$fetchEmail->clnt_email;
  $firstname=$fetchEmail->clnt_first_name;
  $lastname=$fetchEmail->clnt_last_name;
  $name=$firstname.' '.$lastname;
  if($result){

    $subject = "Account has been deactivated";

    $message = "Dear ".$name."<br/>";
    $message .="Your Daydreamer account has been deactivated.<br />";
    $message .="Please contact to Daydreamer for activating your account.<br />";
          // $message .="Below are your login details .<br />";
          // $message .=" Email : ".$email." .<br />Password :".$password.".<br />";
          // // $message .="Your account will be activate  after DayDreamer approval.<br />";
          // //$message .=".$url.";

    $this->sendMail($subject,$message,$email);
    $this->session->set_flashdata('successmsg', '<div class="alert alert-success alert-dismissable"><a type="button" class="close" data-dismiss="alert" aria-hidden="true">×</a>Office has been deactivated successfully</div>');
    redirect(base_url().'admin/viewAllOffices');

  }else{
    $this->session->set_flashdata('errormsg', '<div class="alert alert-warning alert-dismissable"><a type="button" class="close" data-dismiss="alert" aria-hidden="true">×</a>Office has not been deactivated successfully</div>');
    redirect(base_url().'admin/viewAllOffices');

  }
}

//function for enabling office
function enableClient(){
  $id=$this->input->get('id');
  $updateArr=array('deactivate'=>'0','flag'=>'1');
  $result=$this->Admin_model->disableOfficeByAdmin($updateArr,$id);
  $fetchEmail=$this->Admin_model->fetchClientEmail($id);
  $email=$fetchEmail->clnt_email;
  $firstname=$fetchEmail->clnt_first_name;
  $lastname=$fetchEmail->clnt_last_name;
  $name=$firstname.' '.$lastname;
  if($result){
    $subject = "Account has been activated";

    $message = "Dear ".$name."<br/>";
    $message .="Your Daydreamer account has been activated.<br />";
    $message .="Please use your old credentials for Login.<br />";
          // $message .="Below are your login details .<br />";
          // $message .=" Email : ".$email." .<br />Password :".$password.".<br />";
          // // $message .="Your account will be activate  after DayDreamer approval.<br />";
          // //$message .=".$url.";

    $this->sendMail($subject,$message,$email);

    $this->session->set_flashdata('successmsg', '<div class="alert alert-success alert-dismissable"><a type="button" class="close" data-dismiss="alert" aria-hidden="true">×</a>Office has been activated successfully</div>');
    redirect(base_url().'admin/viewAllOffices');

  }else{
    $this->session->set_flashdata('errormsg', '<div class="alert alert-warning alert-dismissable"><a type="button" class="close" data-dismiss="alert" aria-hidden="true">×</a>Office has not been activated successfully</div>');
    redirect(base_url().'admin/viewAllOffices');

  }

}

//function for create a new office
function createClient(){
  $adminSessionData=$this->session->userdata(base_url().'login_session');
  $this->checkSession($adminSessionData);
  $_POST = json_decode(file_get_contents('php://input'), true);
//print_r($_POST);exit;
  if ($this->input->post()) {
    $this->form_validation->set_rules('first_name', 'First Name', 'trim|required|trim|xss_clean|strip_tags');
    $this->form_validation->set_rules('last_name', 'Last Name', 'trim|required|trim|xss_clean|strip_tags');
    $this->form_validation->set_rules('company_name', 'Company Name', 'trim|required|trim|xss_clean|strip_tags');
    $this->form_validation->set_rules('telephone', 'Telephone', 'trim|required|trim|xss_clean|strip_tags');
    $this->form_validation->set_rules('email', 'Email', 'trim|required|trim|xss_clean|strip_tags');
    $this->form_validation->set_rules('password', 'Password', 'trim|required|trim|xss_clean|strip_tags');
    $this->form_validation->set_rules('city', 'City', 'trim|required|trim|xss_clean|strip_tags');
    $this->form_validation->set_rules('country', 'Country', 'trim|required|trim|xss_clean|strip_tags');
    if ($this->form_validation->run() == TRUE) {
      $clientCreate['clnt_first_name']=$this->input->post('first_name');
      $clientCreate['clnt_last_name']=$this->input->post('last_name');
      $clientCreate['company_name']=$this->input->post('company_name');
      $clientCreate['clnt_telephone']=$this->input->post('telephone');
      $clientCreate['clnt_email']=$this->input->post('email');
      $clientCreate['clnt_password']=md5($this->input->post('password'));
      $clientCreate['clnt_city']=$this->input->post('city');
      $clientCreate['clnt_country']=$this->input->post('country');
      $clientCreate['flag']='1';
      $clientCreate['role']='mainclient';
      $client_id=$this->input->post('client_id');
//echo $client_id;exit;
      $checkClientData=$this->Admin_model->checkingClientData($client_id);

      if(!empty($checkClientData)){
        $result=$this->Admin_model->updateClientInfo($clientCreate,$client_id);

      }else{
        $result=$this->Admin_model->addClient($clientCreate);
        $fname=$this->input->post('firstname');
        $lname=$this->input->post('lastname');
        $email=$this->input->post('email');
        $password=$this->input->post('password');
        $name=$fname.' '.$lname;
        $subject = "DayDreamer Client Confirmation";

        $message = "Dear ".$name."<br/>";
        $message .="You are now a Daydreamer Client.<br />";
          //$message .="Thank you for registration.<br />";
        $message .="Below are your login details .<br />";
        $message .=" Email : ".$email." .<br />Password :".$password.".<br />";
          // $message .="Your account will be activate  after DayDreamer approval.<br />";
          //$message .=".$url.";

        $this->sendMail($subject,$message,$email);
      }

      if($result){


        $data = array('status' => '1');
        $this->session->set_flashdata('successmsg', '<div class="alert alert-success"><a type="button" class="close" data-dismiss="alert" aria-hidden="true">×</a>Your request has been completed successfully.</div>');

      }else{

       $data = array('status' => '0');
       $this->session->set_flashdata('errormsg', '<div class="alert alert-warning alert-dismissable"><a type="button" class="close" data-dismiss="alert" aria-hidden="true">×</a>Error while submitting.</div>');
     }

   }else{

    $data = array('status' => '2', 'error' => $this->form_validation->get_field_data(), 'postdata' => $this->input->post());  
  }

}else{

 $data = array('status' => '3');
 $this->session->set_flashdata('errormsg', '<div class="alert alert-warning alert-dismissable"><a type="button" class="close" data-dismiss="alert" aria-hidden="true">×</a>All fields are mandatory.</div>');
}

print_r(json_encode($data));

}

//function for updating client information
function editClientDetails(){
  $adminSessionData=$this->session->userdata(base_url().'login_session');
  $this->checkSession($adminSessionData);
  $id=$this->input->get('id');
  $data['fetchClientDetails']=$this->Admin_model->getClientDetails($id);
//echo '<pre>';print_r($data['fetchClientDetails']);exit;
  $data['allCountries']=$this->Admin_model->getCountries();

  $this->load->view('admin/all_css');
  $this->load->view('admin/dashboard_header');
  $this->load->view('admin/createclient',$data);
  $this->load->view('admin/footer');
  $this->load->view('admin/all_js');
  $this->load->view('admin/pagewise_js/createOffice.js');
}

//function for fetching all sub offices
function getSubOffices(){
  $id=$this->input->post('id');
//echo $id;exit;
  $result=$this->Admin_model->getSubOffices($id);
  if(!empty($result))
  {
    foreach($result as $key){

      echo ' '.'<li>'.$key->subclient_name.',</li>';
    }
  }else{

    echo '<li>No SubOffice Found</li>';
  }
}

//function for viewing analytics
function viewDashboard(){
  $adminSessionData=$this->session->userdata(base_url().'login_session');
  $this->checkSession($adminSessionData);
  $data['firstname']=$adminSessionData['first_name'];
  $data['lastname']=$adminSessionData['last_name'];
  $totalPackages=$this->Admin_model->getPackages();
  $data['countPackage']=$totalPackages->count;
  $totalOffices=$this->Admin_model->getAllOffices();
  $data['countOffice']=$totalOffices->offices;

  $totalCandidates=$this->Admin_model->getAllCandidates();
  $data['countCandidates']=$totalCandidates->candidates;

  $totalJobs=$this->Admin_model->getAllJobs();
  $data['countJobs']=$totalJobs->jobs;

  $date1 = $this->input->post('start_date');
  if ($date1 != '') {
    $start_date = str_replace('/', '-', $date1);
  } else {

    $start_date = date('Y-m-d', strtotime('Jan 01'));
  }
  $date2 = $this->input->post('end_date');
  if ($date2 != '') {
    $end_date = str_replace('/', '-', $date2);
  } else {
    $end_date = date('Y-m-d', strtotime('Dec 31'));
  }

  $totalOffices=$this->Admin_model->getAllOffices();
  $clientCount=$totalOffices->offices;

  $totalCandidates=$this->Admin_model->getAllCandidates();
  $userCount=$totalCandidates->candidates;

  $countCandidates=$this->Admin_model->countAllCandidates($start_date,$end_date);
  $countOffices=$this->Admin_model->countAllOffices($start_date,$end_date);
  $temp=array();

  foreach($countCandidates as $key => $value){
    $temp[$value->month]=$value;
  }

  $temp1=array();
  foreach ($countOffices as $key => $value) {
    $temp1[$value->month]=$value;
  }

  $month_array = array();
  for ($m = 1; $m <= 12; $m++) {
    $month = date('F', mktime(0, 0, 0, $m, 1, date('Y')));
    if ($m < 10) {
      $month_array['0' . $m] = $month;
    } else {
      $month_array[$m] = $month;
    }
  }


  if ($start_date != '' && $end_date != '') {


    $usersWholeCount = $this->Admin_model->countWholeUser($start_date, $end_date);
    $users_id = $usersWholeCount->user_id;


    $clientsWholeCount = $this->Admin_model->countWholeClients($start_date,$end_date);
    $client_id = $clientsWholeCount->client_id;




    $user_array = array('y' => $users_id, 'indexLabel' => 'Users');

    $merchant_array = array('y' => $client_id, 'indexLabel' => 'Clients');

    $analytics_array = array();

    array_push($analytics_array, $user_array);
    array_push($analytics_array, $merchant_array);
  } else {
    $userDefault_array = array('y' => $userCount, 'indexLabel' => 'Users');
    $clientDefault_array = array('y' => $clientCount, 'indexLabel' => 'Clients');
    $analytics_array = array();

    array_push($analytics_array, $userDefault_array);
    array_push($analytics_array, $merchantDefault_array);
  }

  $data['pie_data'] = $analytics_array;
  $data['month_array']=$month_array;
  $data['user_array']= $temp;
  $data['client_array']=$temp1;

  $this->load->view('admin/all_css');
  $this->load->view('admin/dashboard_header');
  $this->load->view('admin/analytics',$data);
  $this->load->view('admin/footer');
  $this->load->view('admin/all_js');
  //$this->load->view('admin/pagewise_js/createOffice.js');


}

//function for viewing profile
function viewProfile(){
  $adminSessionData=$this->session->userdata(base_url().'login_session');
  $this->checkSession($adminSessionData);
  $data['adminDetails']=$this->Admin_model->fetchAdminDetails();
  $data['fetchProfilePic']=$this->Admin_model->getProfilePicture();
  //echo '<pre>';print_r($data['adminDetails']);exit;
  $this->load->view('admin/all_css');
  $this->load->view('admin/dashboard_header');
  $this->load->view('admin/profile',$data);
  $this->load->view('admin/footer');
  $this->load->view('admin/all_js');
  $this->load->view('admin/pagewise_js/adminprofile.js');
  //$this->load->view('admin/pagewise_js/profile_pic.js');


}

//function for editing admin profile
function editProfile(){
  $adminSessionData=$this->session->userdata(base_url().'login_session');
  $this->checkSession($adminSessionData);
  $admin_id=$adminSessionData['admin_id'];
  $_POST = json_decode(file_get_contents('php://input'), true);
//echo '<pre>';print_r($_POST);
  if ($this->input->post()) {
    $this->form_validation->set_rules('first_name', 'First Name', 'trim|required|trim|xss_clean|strip_tags');
    $this->form_validation->set_rules('last_name', 'Last Name', 'trim|required|trim|xss_clean|strip_tags');
    $this->form_validation->set_rules('telephone', 'Telephone', 'trim|required|trim|xss_clean|strip_tags');
    $this->form_validation->set_rules('email', 'Email', 'trim|required|trim|xss_clean|strip_tags');
    
    $this->form_validation->set_rules('address', 'Address', 'trim|required|trim|xss_clean|strip_tags');
    $this->form_validation->set_rules('fb', 'Facebook Profile Link', 'trim|required|trim|xss_clean|strip_tags');
    $this->form_validation->set_rules('linkedin', 'Linkedin Profile Link', 'trim|required|trim|xss_clean|strip_tags');
    $this->form_validation->set_rules('twitter', 'Twitter Profile Link', 'trim|required|trim|xss_clean|strip_tags');
    $this->form_validation->set_rules('pinterest', 'Pinterest Profile Link', 'trim|required|trim|xss_clean|strip_tags');


    if ($this->form_validation->run() == TRUE) {
      $edit_profile['admin_firstName']=$this->input->post('first_name');
      $edit_profile['admin_lastName']=$this->input->post('last_name');
      $edit_profile['admin_telephone']=$this->input->post('telephone');
      $edit_profile['admin_email']=$this->input->post('email');
      
      $edit_profile['address']=$this->input->post('address');
      $edit_profile['fb']=$this->input->post('fb');
      $edit_profile['linkedin']=$this->input->post('linkedin');
      $edit_profile['twitter']=$this->input->post('twitter');
      $edit_profile['pinterest']=$this->input->post('pinterest');
      //echo '<pre>';print_r($edit_profile);exit;
      $id=$admin_id;
      $result=$this->Admin_model->editAdminProfile($edit_profile,$id);
      if($result){

       $data = array('status' => '1');
       $this->session->set_flashdata('successmsg', '<div class="alert alert-success"><a type="button" class="close" data-dismiss="alert" aria-hidden="true">×</a>Profile has been updated successfully.</div>');

     }else{

       $data = array('status' => '0');
       $this->session->set_flashdata('errormsg', '<div class="alert alert-warning alert-dismissable"><a type="button" class="close" data-dismiss="alert" aria-hidden="true">×</a>Error while submitting.</div>');
     }

   }else{

    $data = array('status' => '2', 'error' => $this->form_validation->get_field_data(), 'postdata' => $this->input->post());  

  }


}else{
  $data = array('status' => '3');
  $this->session->set_flashdata('errormsg', '<div class="alert alert-warning alert-dismissable"><a type="button" class="close" data-dismiss="alert" aria-hidden="true">×</a>All fields are mandatory.</div>');


}

print_r(json_encode($data));

}

//function for change profile picture
function changeProfilePicture(){
  $adminSessionData=$this->session->userdata(base_url().'login_session');
  $this->checkSession($adminSessionData);
  $data['fetchProfilePic']=$this->Admin_model->getProfilePicture();
  $data['adminDetails']=$this->Admin_model->fetchAdminDetails();
  $this->load->view('admin/all_css');
  $this->load->view('admin/dashboard_header');
  $this->load->view('admin/changeProfilePic',$data);
  $this->load->view('admin/footer');
  $this->load->view('admin/all_js');
  $this->load->view('admin/pagewise_js/picture.js');




}

//function for submitting profile pic
function sbmtProfilePic(){
  define('UPLOAD_DIR', 'client_uploads/profile_pic/');
  $_POST = json_decode(file_get_contents('php://input'), true);
   //print_r($_POST);exit;     
  if ($this->input->post()) {

    $image_name = $_POST['userfile'];
    $img = $_POST['imgdata'];

       //  echo $image_name." ".$img; exit;
    $img = str_replace('data:image/png;base64,', '', $img);
    $img = str_replace(' ', '+', $img);
    $decoded = base64_decode($img);
    $uid='1';
    $file = UPLOAD_DIR .$uid.'_'.$image_name;
    $file_name=$uid.'_'.$image_name;
          //echo $file_name;exit;
    $success = file_put_contents($file, $decoded);

    if($success)
    {

      $updateArr=array('profile_pic' => $file_name);

      $upload = $this->Admin_model->saveProfilePic($updateArr,$uid);
         //echo '<pre>';print_r($this->db->last_query());exit;

      if($upload)
      {

       $data = array('status' =>'1');
       $this->session->set_flashdata('successmsg', '<div class="alert alert-success"><a type="button" class="close" data-dismiss="alert" aria-hidden="true">×</a>Profile picture has been uploaded successfully.</div>');
     }
   }

 }else{

   $data = array('status' => '2');
   $this->session->set_flashdata('errormsg', '<div class="alert alert-warning alert-dismissable"><a type="button" class="close" data-dismiss="alert" aria-hidden="true">×</a>Please select profile picture properly.</div>');


 }

 print_r(json_encode($data));


}

//function for viewing change password view
function changePassword(){
  $adminSessionData=$this->session->userdata(base_url().'login_session');
  $this->checkSession($adminSessionData);
  $this->load->view('admin/all_css');
  $this->load->view('admin/dashboard_header');
  $this->load->view('admin/changepassword');
  $this->load->view('admin/footer');
  $this->load->view('admin/all_js');
  $this->load->view('admin/pagewise_js/changepassword.js');
  
}


//function for changing password
function changeAdminPassword(){

  $_POST = json_decode(file_get_contents('php://input'), true);
//echo '<pre>';print_r($_POST);exit;
  if ($this->input->post()) {
    $this->form_validation->set_rules('oldpassword', 'Old Password', 'trim|required|trim|xss_clean|strip_tags');
    $this->form_validation->set_rules('password', 'Password', 'trim|required|trim|xss_clean|strip_tags');
    if ($this->form_validation->run() == TRUE) {
  //echo 'here';exit;
      $password = md5($this->input->post('oldpassword'));
//echo $password;exit;
      $admin_id='1';
      $success = $this->Admin_model->check_old_password($admin_id, $password);
//echo '<pre>';print_r($success);exit;
      if(!empty($success)){
  //echo 'here';exit;
        $pass = md5($this->input->post('password'));
        $updateArr=array(
          'admin_password'=>$pass


          );
//echo '<pre>';print_r($updateArr);exit;
        $response = $this->Admin_model->change_password($admin_id, $updateArr);
//echo '<pre>';print_r($this->db->last_query());exit;
        if(!empty($response)){
  //echo 'here';exit;
          $data = array('status' => '1');
          $this->session->set_flashdata('successmsg', '<div class="alert alert-success"><a type="button" class="close" data-dismiss="alert" aria-hidden="true">×</a>Password has been changed successfully.</div>');
        }else{
  //echo 'there';exit;
         $data = array('status' => '0');
         $this->session->set_flashdata('errormsg', '<div class="alert alert-warning alert-dismissable"><a type="button" class="close" data-dismiss="alert" aria-hidden="true">×</a>Password has not been changed successfully.</div>');
       }


     }else{
  //echo 'there';exit;
      $data = array('status' => '0');
      $this->session->set_flashdata('errormsg', '<div class="alert alert-warning alert-dismissable"><a type="button" class="close" data-dismiss="alert" aria-hidden="true">×</a>Incorrect old password.</div>');
    }
  }else{

    $data = array('status' => '2', 'error' => $this->form_validation->get_field_data(), 'postdata' => $this->input->post()); 
  }


}else{
  $data = array('status' => '3');
  $this->session->set_flashdata('errormsg', '<div class="alert alert-warning alert-dismissable"><a type="button" class="close" data-dismiss="alert" aria-hidden="true">×</a>All fields are mandatory.</div>');


}
print_r(json_encode($data));

}

//function for viewing update Job Form
function updateJobs(){
  $adminSessionData=$this->session->userdata(base_url().'login_session');
  $this->checkSession($adminSessionData);
  $id=$this->input->get('id');
  $this->load->model('Client_model');
  $data['allCountries']=$this->Client_model->getAllCountries();
  $data['allIndustries']=$this->Client_model->getAllIndustries();
  $data['allStatus']=$this->Client_model->getAllStatus();
  $data['allExperience']=$this->Client_model->getExperience();
  $data['allJobType']=$this->Client_model->getJobType();
  $data['getJobInfo']=$this->Admin_model->fetchJobData($id);
  $data['currencies']=$this->Admin_model->fetchCurrencies();
  //echo '<pre>';print_r($data['getJobInfo']);exit;
  $client=$data['getJobInfo']->subclient_name;
  //echo $client;exit;
  if($client!=""){

   $data['client_name']=$client;
 }else{

  $data['client_name']=$data['getJobInfo']->parent_client;
}
  //echo '<pre>';print_r($data['getJobInfo']);exit;
$this->load->view('admin/all_css');
$this->load->view('admin/dashboard_header');
$this->load->view('admin/updateJobForm',$data);
$this->load->view('admin/footer');
$this->load->view('admin/all_js');
$this->load->view('admin/pagewise_js/updateJobForm.js');



}

//function for updating information of a job
function update_job_opening(){

  $_POST = json_decode(file_get_contents('php://input'), true);
  $job_id=$this->input->post('job_id');

  $this->load->model('Client_model');
  $checkJobData=$this->Client_model->checkJobData($job_id);
// echo '<pre>';print_r($checkJobData);exit;
  $this->form_validation->set_rules('job_title', 'Job Title', 'trim|required|trim|xss_clean|strip_tags');
  $this->form_validation->set_rules('contact_name', 'Contact Name', 'trim|required|trim|xss_clean|strip_tags');
  $this->form_validation->set_rules('recruiter', 'Recruiter', 'trim|required|trim|xss_clean|strip_tags');


  $this->form_validation->set_select('job_status', 'Status', 'trim|required|trim|xss_clean|strip_tags');
//   //$this->form_validation->set_rules('industry', 'Industry', 'trim|required|trim|xss_clean|strip_tags');
  $this->form_validation->set_rules('state', 'State', 'trim|required|trim|xss_clean|strip_tags');
  $this->form_validation->set_select('experience', 'Experience', 'trim|required|trim|xss_clean|strip_tags');
   //$this->form_validation->set_rules('parent_client', 'Client Name', 'trim|required|trim|xss_clean|strip_tags');
  $this->form_validation->set_rules('manager', 'Account Manager', 'trim|required|trim|xss_clean|strip_tags');
  $this->form_validation->set_rules('positions', 'Position', 'trim|required|trim|xss_clean|strip_tags');
  $this->form_validation->set_rules('open_date', 'Open Date', 'trim|required|trim|xss_clean|strip_tags');
  $this->form_validation->set_select('job_type', 'Job Type', 'trim|required|trim|xss_clean|strip_tags');
  $this->form_validation->set_select('qualifications', 'Qualifications', 'trim|required|trim|xss_clean|strip_tags');
  $this->form_validation->set_select('gender', 'Gender', 'trim|required|trim|xss_clean|strip_tags');
  $this->form_validation->set_select('working_days', 'Working Days', 'trim|required|trim|xss_clean|strip_tags');
  $this->form_validation->set_select('company_profile', 'Company Profile', 'trim|required|trim|xss_clean|strip_tags');
//$this->form_validation->set_select('skills', 'Skills', 'trim|required|trim|xss_clean|strip_tags');
  $this->form_validation->set_rules('city', 'City', 'trim|required|trim|xss_clean|strip_tags');
  $this->form_validation->set_select('nationality', 'Country', 'trim|required|trim|xss_clean|strip_tags');
  $this->form_validation->set_select('min', 'Minimum Salary', 'trim|required|trim|xss_clean|strip_tags');
  $this->form_validation->set_rules('max', 'Maximum Salary', 'trim|required|trim|xss_clean|strip_tags');
  $this->form_validation->set_rules('currency', 'Currency', 'trim|required|trim|xss_clean|strip_tags');
  $this->form_validation->set_rules('job_desc', 'Job Description', 'trim|required|trim|xss_clean|strip_tags');

  if ($this->form_validation->run() == TRUE) {

    $update_job['job_title']=$this->input->post('job_title');
    $update_job['contact_name']=$this->input->post('contact_name');
    $update_job['recruiter']=$this->input->post('recruiter');

    $update_job['job_status']=$this->input->post('job_status');
    
    $update_job['state']=$this->input->post('state');
    $update_job['experience']=$this->input->post('experience');
    $parent_client=$this->input->post('parent_client');

    if(isset($parent_client) && !empty($parent_client)){
      //echo 'here';
      $update_job['parent_client']=$this->input->post('parent_client');

    }else{
       // echo 'there';
      $update_job['parent_client']=$checkJobData->parent_client;
    }
    //echo $update_job['parent_client'];exit;
    
    $update_job['manager']=$this->input->post('manager');

    $update_job['positions']=$this->input->post('positions');
    $update_job['open_date']=date('Y-m-d',strtotime($this->input->post('open_date')));

    $update_job['job_type']=$this->input->post('job_type');
    $update_job['qualifications']=$this->input->post('qualifications');
    
    $update_job['working_days']=$this->input->post('working_days');
    $update_job['company_profile']=$this->input->post('company_profile');
        //$update_job['company_profile']=$this->input->post('company_profile');
       // $update_job['skills']=implode(',',$this->input->post('skills'));
        //print_r($update_job['skills']);exit;
        //$update_job['skills']=implode(',',$skill_array);


    $update_job['city']=$this->input->post('city');

    $update_job['nationality']=$this->input->post('nationality');
    $update_job['min_salary']=$this->input->post('min');
    $update_job['max_salary']=$this->input->post('max');
    $update_job['currency']=$this->input->post('currency');
    $update_job['job_desc']=$this->input->post('job_desc');
    $job_summary=$this->input->post('filename');

        //echo $job_summary;exit;
    if(isset($job_summary) && !empty($job_summary)){

      $update_job['job_summary']=$this->input->post('filename');
          //echo $update_job['job_summary'];exit;
    }else{

      $update_job['job_summary']= $checkJobData->job_summary;
          //echo $update_job['job_summary'];exit;
    }
      //echo '<pre>';print_r($update_job);exit;
      //$create_job['subClient_id']=$this->input->post('sub_client_id');
      //$create_job['j_id']=$this->input->post('job_id');
    $job_id=$this->input->post('job_id');

    $result=$this->Admin_model->updateJobCreate($update_job,$job_id);

    if($result){

      $data = array('status' => '1');
      $this->session->set_flashdata('successmsg', '<div class="alert alert-success"><a type="button" class="close" data-dismiss="alert" aria-hidden="true">×</a>Your request has been completed successfully.</div>');
    }else{

      $data = array('status' => '0');
      $this->session->set_flashdata('errormsg', '<div class="alert alert-warning alert-dismissable"><a type="button" class="close" data-dismiss="alert" aria-hidden="true">×</a>Error while submitting.</div>');
    }
  }else{

   $data = array('status' => '2', 'error' => $this->form_validation->get_field_data(), 'postdata' => $this->input->post()); 
 }
 print_r(json_encode($data));

}

//function for file uploading....
function upload_doc(){
  //$client_session_data=$this->session->userdata(base_url().'login_session');

  //$id=$client_session_data['client_id'];

  $filename=$_FILES['file']['name'];
  $target = 'admin_uploads/';
  $file = $target.'-'.$filename;

  if(move_uploaded_file($_FILES['file']['tmp_name'], $file))
  {

    print_r(json_encode(array('filename'=>$filename)));

  }else{


    echo 'not uploded';
  }

}

//function for viewing blog form
function viewBlogForm(){
  $adminSessionData=$this->session->userdata(base_url().'login_session');
  $this->checkSession($adminSessionData);
  $this->load->view('admin/all_css');
  $this->load->view('admin/dashboard_header');
  $this->load->view('admin/blogForm');
  $this->load->view('admin/footer');
  $this->load->view('admin/all_js');
  $this->load->view('admin/pagewise_js/blogForm.js');
}

//function for fetching all Clients
function fetchAllClient(){


  $searchquery = $_GET["query"]; 

  $search=$searchquery;
  if ($search != '') 
  { 
      //echo 'here';exit;
   $result=$this->Admin_model->getClientsName();
   $json = array();
   
   foreach($result as $key){
     $subClientName=$key->subclient_name;

     $subClientId=$key->id;
     
     $json['suggestions'][] = array('value' => $subClientName,  'data' => $subClientId); 
     
   }
   $fetchMainClient=$this->Admin_model->getMainClientsName();
   foreach($fetchMainClient as $temp){
    $clientName=$temp->clnt_first_name;

    $json['suggestions'][] = array('value' => $clientName,  'data' => $clientName);
  }

  print(json_encode($json)); 

} 


else 
{ 
          //echo 'there';exit;
  $this->session->set_flashdata("errormessage", "Sub Office does not exist"); 
  redirect('client/viewJobForm'); 
} 

}

//function for insert a blog
function insertBlog(){
  $adminSessionData=$this->session->userdata(base_url().'login_session');
  $admin_id=$adminSessionData['admin_id'];
//echo $admin_id;exit;
  $_POST = json_decode(file_get_contents('php://input'), true);
  //print_r($_POST);exit;
  $this->form_validation->set_rules('name', 'Admin Name', 'trim|required|trim|xss_clean|strip_tags');
  $this->form_validation->set_rules('blog_title', 'Admin Name', 'trim|required|trim|xss_clean|strip_tags');
  $this->form_validation->set_rules('blog_desc', 'Admin Name', 'trim|required|trim|xss_clean|strip_tags');
//$this->form_validation->set_rules('name', 'Admin Name', 'trim|required|trim|xss_clean|strip_tags');

  if ($this->form_validation->run() == TRUE) {
   $insert_blog['admin_name']=$this->input->post('name');
   $insert_blog['blog_title']=$this->input->post('blog_title');
   $insert_blog['blog_desc']=$this->input->post('blog_desc');

   $insert_blog['admin_id']=$admin_id;
   //echo '<pre>';print_r($insert_blog);exit;
   $image=$this->input->post('filename');
   $video=$this->input->post('video');

   $id=$this->input->post('admin_id');
//echo $id;exit;
   $checkBlogData=$this->Admin_model->fetchBlogData($id);
//echo '<pre>';print_r($checkBlogData);exit;
   if(isset($image) && $image!=''){

    $insert_blog['blog_image']=$image;
  }else{

    $insert_blog['blog_image']=$checkBlogData->blog_image;
  }

  if(isset($video) && $video!=''){

    $insert_blog['blog_video']=$video;
  }else{

    $insert_blog['blog_video']=$checkBlogData->blog_video;
  }
  //echo '<pre>';print_r($insert_blog);exit;

  if(!empty($checkBlogData)){

    $result=$this->Admin_model->updateBlogDetails($insert_blog,$id);
  }else{
    $result=$this->Admin_model->insertBlogDetails($insert_blog);

  }

  if($result){
    $data = array('status' => '1');
    $this->session->set_flashdata('successmsg', '<div class="alert alert-success"><a type="button" class="close" data-dismiss="alert" aria-hidden="true">×</a>Your request has been completed successfully.</div>');

  }else{
    $data = array('status' => '0');
    $this->session->set_flashdata('errormsg', '<div class="alert alert-warning alert-dismissable"><a type="button" class="close" data-dismiss="alert" aria-hidden="true">×</a>Error while submitting.</div>');
  }
}else{
  $data = array('status' => '2', 'error' => $this->form_validation->get_field_data(), 'postdata' => $this->input->post()); 

}


print_r(json_encode($data));
}



//function for uploading blog video
function upload_blogVideo(){
  $filename=$_FILES['file']['name'];
  //echo $filename;exit;
  $target = 'blog_uploads_video/';
  $file = $target.$filename;

  if(move_uploaded_file($_FILES['file']['tmp_name'], $file))
  {

    print_r(json_encode(array('filename'=>$filename)));

  }else{


    echo 'not uploded';
  }


}


//function for uploading a blog
function upload_blog(){
  $filename=$_FILES['file']['name'];
  $target = 'blog_uploads/';
  $file = $target.$filename;

  if(move_uploaded_file($_FILES['file']['tmp_name'], $file))
  {

    print_r(json_encode(array('filename'=>$filename)));

  }else{


    echo 'not uploded';
  }

}

//function for viewing all blogs
function viewAllBlogs(){
  $adminSessionData=$this->session->userdata(base_url().'login_session');
  $this->checkSession($adminSessionData);
  $this->load->view('admin/all_css');
  $this->load->view('admin/dashboard_header');
  $this->load->view('admin/viewBlogs');
  $this->load->view('admin/footer');
  $this->load->view('admin/all_js');
  $this->load->view('admin/pagewise_js/viewBlogs.js');

}

//function for fetching blog data
function blogData(){
 //echo 'here';
  $admin_data=$this->session->userdata(base_url().'login_session');
  
  $requestData = $_REQUEST;
  $totalData = $this->Admin_model->get_blog_list();
  $totalFiltered = $totalData = count($totalData);
  $totalFiltered = $this->Admin_model->getBlogfilteredList($requestData);
  $totalFiltered = count($totalFiltered);

  $data = array();
  $userlist = $this->Admin_model->getBlogList($requestData);
  //echo '<pre>';print_r($userlist);exit;
  if (!empty($userlist)) {
    $count=1;
    foreach ($userlist as $user) {
             // preparing an array
      $nestedData = array();

                //$view = "<img height='100' width='100' src='" . base_url() . "uploads/" . $user->logo . "'>";
      $path=base_url().'blog_uploads/'.$user->blog_image;
      $video_path=base_url().'blog_uploads_video/'.$user->blog_video;

      $view="<img src=".$path.">";

      $view1='<ul class="list-unstyled list-inline" style="padding-left: 0px;"><li><a href="' . base_url() . 'admin/updateBlog?id= ' . $user->id . '" title="Update Blog Information"><i class="fa fa-pencil" aria-hidden="true"></i></a></li><li><a onclick="return confirmMsg();" href="' . base_url() . 'admin/deleteBlog?id=' . $user->id . '" title="Delete Blog Information"><i class="fa fa-trash" aria-hidden="true"></i></a>';
   //$view1.="<a onclick='return confirmClientDisable();' href='".base_url()."client/disableCredential?id=".$user->id."' ><i class='fa fa-trash' aria-hidden='true'></i></a>";
      $view2="<video width='200' controls><source src=".$video_path." type='video/mp4'></video>";
      $nestedData[] = $count++;
      $nestedData[] = $user->admin_name;
      $nestedData[] = $user->blog_title;
      $nestedData[] = $user->blog_desc;
      $nestedData[] = $view;
      $nestedData[] = $view2;
      $nestedData[] = date('jS M Y',strtotime($user->created_date));
      $nestedData[] = $view1;
      $data[] = $nestedData;
    }
  }

  $json_data = array(
            "draw" => intval($requestData['draw']), // for every request/draw by clientside , they send a number as a parameter, when they recieve a response/data they first check the draw number, so we are sending same number in draw. 
            "recordsTotal" => intval($totalData), // total number of records
            "recordsFiltered" => intval($totalFiltered), // total number of records after searching, if there is no searching then totalFiltered = totalData
            "data" => $data   // total data array
            );
  echo json_encode($json_data);




}

        //function for viewing all main jobs
function mainJobsData(){


  $userdata =$this->session->userdata('job_filter');
  $job_title=$userdata['job_title'];
  
  $min_salary=$userdata['min_salary'];
  $max_salary=$userdata['max_salary'];
  $job_type=$userdata['job_type'];
  $client_name=$userdata['client_name'];
 // echo 'here';exit;
  $admin_data=$this->session->userdata(base_url().'login_session');
  
  $requestData = $_REQUEST;
  

  $totalData = $this->Admin_model->get_mainJob_list();
  $totalFiltered = $totalData = count($totalData);
  $totalFiltered = $this->Admin_model->getMainJobfilteredList($requestData,$job_title,$min_salary,$max_salary,$job_type,$client_name);
  $totalFiltered = count($totalFiltered);

  $data = array();
  $userlist = $this->Admin_model->getMainJobList($requestData,$job_title,$min_salary,$max_salary,$job_type,$client_name);
  //echo '<pre>';print_r($this->db->last_query());exit;
  $this->session->unset_userdata('job_filter');
  if (!empty($userlist)) {
    $count=1;
    foreach ($userlist as $user) {
             // preparing an array
      $nestedData = array();

      $client=$user->subclient_name;
                            //echo $client;exit;
      if($client!=''){

        $client_name=$client;
      }else{

        $client_name=$user->parent_client;
      }
      $view='<a href="'.base_url().'admin/viewJobDescription?id='.$user->j_id.'">'.$user->job_title.'</a>';
      $view1='<ul class="list-unstyled list-inline" style="padding-left: 0px;">';

      if($user->job_approve =='0'){
        $view1.='<li onclick="return confirmJobDisapprove();"><a href="'.base_url().'.admin/disApproveJob?id= '.$user->j_id.'" title="approve job"><i class="fa fa-thumbs-o-up fa-2x" aria-hidden="true"></i></a></li>'; 
      } else {
        $view1.='<li onclick="return confirmApprove();"><a href="'.base_url().'admin/approveJob?id='.$user->j_id.'"title="disapprove job"><i class="fa fa-thumbs-down fa-2x" aria-hidden="true"></i></a></li>'; 
      }
      // $view1.=' <li><a href="'.base_url().'admin/viewJobDescription?id='.$user->j_id.'"><i class="fa fa-eye" aria-hidden="true"></i></a></li>';

      $view1.='<li><a href="'.base_url().'admin/updateJobs?id='.$user->j_id.'"><i class="fa fa-edit" aria-hidden="true"></i></a></li>';
      $view1.='</ul>';
      $nestedData[] = $count++;
      $nestedData[] = $view;
      $nestedData[] = $user->recruiter;
      $nestedData[] = $user->status_name;
      $nestedData[] = $user->min_salary.'-'.$user->max_salary;
      $nestedData[] = $user->currency_code;
      $nestedData[] = $client_name;
      $nestedData[] = date('jS M Y',strtotime($user->created_date));
      $nestedData[] = date('jS M Y',strtotime($user->open_date));
      $nestedData[] = $user->job_type;
      //$nestedData[] = $view;
      $nestedData[] = $view1;
      $data[] = $nestedData;
    }
  }

  $json_data = array(
            "draw" => intval($requestData['draw']), // for every request/draw by clientside , they send a number as a parameter, when they recieve a response/data they first check the draw number, so we are sending same number in draw. 
            "recordsTotal" => intval($totalData), // total number of records
            "recordsFiltered" => intval($totalFiltered), // total number of records after searching, if there is no searching then totalFiltered = totalData
            "data" => $data   // total data array
            );
  echo json_encode($json_data);


}

//function for fetching all clients
function fetchClients(){
$searchquery = $_GET["query"]; 

  $search=$searchquery;
  if ($search != '') 
  { 
      //echo 'here';exit;
   $result=$this->Admin_model->getAllSubClients();
   $json = array();
   
   foreach($result as $key){
     $subClientName=$key->subclient_name;

     $subClientId=$key->id;
     
     $json['suggestions'][] = array('value' => $subClientName,  'data' => $subClientId); 
     
   }
   $clientResult=$this->Admin_model->getAllClients();
   foreach($clientResult as $temp){
    $clientName=$temp->company_name;
    $json['suggestions'][] = array('value' => $clientName,  'data' => $clientName);
   }
   


   print(json_encode($json)); 

 } 


 else 
 { 
          //echo 'there';exit;
  $this->session->set_flashdata("errormessage", "Sub Office does not exist"); 
  redirect('client/viewJobForm'); 
} 


}

//function for viewing all offices at server side
function viewOffices(){
 $admin_data=$this->session->userdata(base_url().'login_session');

 $requestData = $_REQUEST;
 $totalData = $this->Admin_model->getAllOfficesList();
 $totalFiltered = $totalData = count($totalData);
 $totalFiltered = $this->Admin_model->getOfficesfilteredList($requestData);
 //echo '<pre>';print_r($totalFiltered);exit;
 $totalFiltered = count($totalFiltered);

 $data = array();
 $userlist = $this->Admin_model->getOfficeList($requestData);
  //echo '<pre>';print_r($userlist);exit;
 if (!empty($userlist)) {
  $count=1;
  foreach ($userlist as $user) {
             // preparing an array
    $nestedData = array();

    $view='<span data-toggle="collapse" onclick="return myFunction('.$user->id.');" data-target="#'.$user->id.'" style="cursor: pointer;">'.$user->company_name.'</span>   <i class="fa fa-angle-down" aria-hidden="true"></i>
    <div id="'.$user->id.'" class="collapse">    
    <h5><strong>Sub clients:</strong></h5>
    <ul class="list-inline list-unstyled" id="showSubOffices">




    </ul>


    </div>';


    $view1='<ul class="list-unstyled list-inline" style="padding-left: 0px;">';
    if($user->deactivate =='0'){

     $view1.='<li onclick="return deactivateClient();"><a href="'.base_url().'admin/disableClient?id='.$user->id.'" title="activate client" class="btn btn-success btn-xs" style="min-width: 100px; margin-right: 5px;"><i class="fa fa-check-square-o" aria-hidden="true"></i> Activated</a></li>';

   }else{

    $view1.=' <li onclick="return activateClient();"><a href="'.base_url().'admin/enableClient?id='.$user->id.'" title="deactivate client" class="btn btn-danger btn-xs" style="min-width: 100px; margin-right: 5px;"><i class="fa fa-minus-circle" aria-hidden="true"></i> Deactive</a></li>';
  }

  $view1.='<li><a href="'.base_url().'admin/editClientDetails?id='.$user->id.'" title="Update Information" class="btn btn-primary btn-xs"><i class="fa fa-pencil" aria-hidden="true"></i> Edit</a>
  </li>';

  $view1.='</ul>';

  $nestedData[] = $count++;
  //$nestedData[] = $user->clnt_first_name;
  $nestedData[] = $view;
  $nestedData[] = $user->clnt_first_name.' '.$user->clnt_last_name;
  $nestedData[] = $view1;
  $data[] = $nestedData;
}
}

$json_data = array(
            "draw" => intval($requestData['draw']), // for every request/draw by clientside , they send a number as a parameter, when they recieve a response/data they first check the draw number, so we are sending same number in draw. 
            "recordsTotal" => intval($totalData), // total number of records
            "recordsFiltered" => intval($totalFiltered), // total number of records after searching, if there is no searching then totalFiltered = totalData
            "data" => $data   // total data array
            );
echo json_encode($json_data);


}

//function for viewing all candidates
function viewCandidates(){
  $userdata =$this->session->userdata('advance_filter');
  $candidate_first_name=$userdata['first_name'];
  
  $city=$userdata['city'];
  $candidate_last_name=$userdata['candidate_last_name'];
  $email=$userdata['email'];
  
 
 $admin_data=$this->session->userdata(base_url().'login_session');

 $requestData = $_REQUEST;
 $totalData = $this->Admin_model->getAllCandidatesList();
 $totalFiltered = $totalData = count($totalData);
 $totalFiltered = $this->Admin_model->getCandidatesfilteredList($requestData,$candidate_first_name,$candidate_last_name,$email,$city);
 //echo '<pre>';print_r($totalFiltered);exit;
 $totalFiltered = count($totalFiltered);

 $data = array();
 $userlist = $this->Admin_model->getCandidatesList($requestData,$candidate_first_name,$candidate_last_name,$email,$city);
  //echo '<pre>';print_r($userlist);exit;
 if (!empty($userlist)) {
  $count=1;
  foreach ($userlist as $user) {
             // preparing an array
    $nestedData = array();
if($user->profile == '1' && $user->education =='1' && $user->pre_employment=='1' && $user->employment=='1' && $user->resume=='1' && $user->references=='1' && $user->additional_info=='1' && $user->miscellaneous=='1' && $user->social_media=='1'){

$status='<div class="progress"><div class="progress-bar progress-bar-success progress-bar-striped" role="progressbar" aria-valuenow="40" aria-valuemin="0" aria-valuemax="100" style="width:100%">100% Complete</div></div>';

}else if($user->profile == '1' && $user->education =='1' && $user->pre_employment=='1' && $user->employment=='1' && $user->resume=='1' && $user->references=='1' && $user->additional_info=='1' && $user->miscellaneous=='0' && $user->social_media=='0'){


 $status='<div class="progress"><div class="progress-bar progress-bar-warning progress-bar-striped" role="progressbar" aria-valuenow="60" aria-valuemin="0" aria-valuemax="100" style="width:85%">85% Complete</div></div>'; 


}else if($user->profile == '1' && $user->education =='1' && $user->pre_employment=='1' && $user->employment=='0' && $user->resume=='0' && $user->references=='0' && $user->additional_info=='0' && $user->miscellaneous=='0' && $user->social_media=='0'){

$status=' <div class="progress"><div class="progress-bar progress-bar-danger progress-bar-striped" role="progressbar" aria-valuenow="70" aria-valuemin="0" aria-valuemax="100" style="width:50%">20% Complete</div></div>';

}else{

$status='<div class="progress"><div class="progress-bar progress-bar-danger progress-bar-striped" role="progressbar" aria-valuenow="70" aria-valuemin="0" aria-valuemax="100" style="width:100%">0% Complete</div>';
}
    
$deactivate=$user->deactivate;
$view='<a href="'.base_url().'admin/viewCandidateProfile?id='.$user->u_id.'">'.$user->first_name.' '.$user->last_name.'</a>';

$view1='<ul class="list-unstyled list-inline" style="padding-left: 0px;">';
if($deactivate == '0')
{
 $view1.='<li onclick="return deactivateUser();"><a href="'.base_url().'admin/disableUser?id='.$user->u_id.'"><i class="fa fa-check-square-o" aria-hidden="true"></i></a></li>';

}else{

$view1.='<li onclick="return activateUser();"><a href="'.base_url().'admin/EnableUser?id='.$user->u_id.'"><i class="fa fa-minus-circle" aria-hidden="true"></i></a></li>';
}
$view1.='<li><a href="'.base_url().'admin/candidateAppliedJobs?id='.$user->u_id.'"><i class="fa fa-paper-plane-o" aria-hidden="true" title="Applied Jobs"></i>
<a></li>';
$view1.='<li><a href="'.base_url().'admin/createTimesheets?id='.$user->u_id.'"><i class="fa fa-clock-o" aria-hidden="true" title="Create Timesheet"></i>
<a></li>';
 $view1.='</ul>';

  

  $nestedData[] = $count++;
  $nestedData[] = $view;
  //$nestedData[] = $user->first_name.' '.$user->last_name;
  $nestedData[] = $user->user_city;
  $nestedData[] = $user->user_email;
  $nestedData[] = $status;
  $nestedData[] = $view1;
  
  $data[] = $nestedData;
}
}

$json_data = array(
            "draw" => intval($requestData['draw']), // for every request/draw by clientside , they send a number as a parameter, when they recieve a response/data they first check the draw number, so we are sending same number in draw. 
            "recordsTotal" => intval($totalData), // total number of records
            "recordsFiltered" => intval($totalFiltered), // total number of records after searching, if there is no searching then totalFiltered = totalData
            "data" => $data   // total data array
            );
echo json_encode($json_data);



}

//function for viewing candidate profile
function viewCandidateProfile(){
  $user_id=$this->input->get('id');
  $data['candidate_id']=$user_id;
  $adminSessionData=$this->session->userdata(base_url().'login_session');
  $this->checkSession($adminSessionData);
  $data['educationInfo']=$this->Admin_model->getCandidateEducationInfo($user_id);
  $data['personalInfo']=$this->Admin_model->getCandidatePersonalInfo($user_id);
  $data['employmentInfo']=$this->Admin_model->getCandidateEmploymentInfo($user_id);
  $data['referenceInfo']=$this->Admin_model->getCandidateReferenceInfo($user_id);
  $data['days']=$this->Admin_model->getDays();
  $data['years']=$this->Admin_model->getYears();
  $data['references']=$this->Admin_model->getReferences();
  $data['allCountries']=$this->Admin_model->getCountries();
  $data['achieved']=$this->Admin_model->getAchieved();
  $data['qualifications']=$this->Admin_model->getQualifications();
  $data['grade']=$this->Admin_model->getGrades();
  $data['rel_status']=$this->Admin_model->getRelStatus();
  $data['ethnic_origin']=$this->Admin_model->getOrigin();
  $this->load->model('user_model');
  $data['userProfileData'] = $this->user_model->getUserProfileData($user_id);
  $data['userProfilePic']=$this->Admin_model->getUserProfilePicture($user_id);
  $data['userEducationDetails'] = $this->user_model->getUserEducationDetails($user_id);
  $data['PreEmploymentInfo']  = $this->user_model->getUserPreEmploymentInfo($user_id);
  $data['userSocialMediaLink'] = $this->user_model->getUserSavesLink($user_id);

  //echo '<pre>';print_r($this->db->last_query());exit;
  $data['relationshipStatus'] = $this->user_model->getRelationship();
  $userResumes = $this->user_model->getUserResumeInfo($user_id);
        
        $temp = array();

        foreach($userResumes->result() as $key => $val) {
          $temp[$val->profile_id][]=$val;

        }
 $data['resumesInfo']=$temp;
 $data['months'] = $this->user_model->getMonths();
 $data['years']  = $this->user_model->getYears();
 $data['qualification'] = $this->user_model->getQualificationTypes();
 $data['grade'] = $this->user_model->getGrade();
 $data['achieved'] = $this->user_model->getAchieved();
  $this->load->view('admin/all_css');
  $this->load->view('admin/dashboard_header');
  $this->load->view('admin/candidateProfile',$data);
  $this->load->view('admin/footer');
  $this->load->view('admin/all_js');
  $this->load->view('admin/pagewise_js/candidateProfile.js');

}
//function for viewing sub offices
function viewSubOffices(){
$admin_data=$this->session->userdata(base_url().'login_session');

 $requestData = $_REQUEST;
 $totalData = $this->Admin_model->getAllSubOfficesList();
 $totalFiltered = $totalData = count($totalData);
 $totalFiltered = $this->Admin_model->getSubOfficesfilteredList($requestData);
 //echo '<pre>';print_r($totalFiltered);exit;
 $totalFiltered = count($totalFiltered);

 $data = array();
 $userlist = $this->Admin_model->getSubOfficesList($requestData);
  // echo '<pre>';print_r($this->db->last_query());exit;
 if (!empty($userlist)) {
  $count=1;
  foreach ($userlist as $user) {
    if(is_numeric($user->parent_client)){
                                                 //echo 'if';exit;
      $client_name=$user->subclient_name;
 }else{
      // echo 'else';exit;
    $client_name=$user->parent_client;
}
$view='<a href="'.base_url().'admin/viewJobDescription?id='.$user->j_id.'">'.$user->job_title.'</a>';
$view1='<ul class="list-unstyled list-inline" style="padding-left: 0px;">';

      if($user->job_approve =='0'){
        $view1.='<li onclick="return confirmJobDisapprove();"><a href="'.base_url().'.admin/disApproveJob?id= '.$user->j_id.'" title="approve job"><i class="fa fa-thumbs-o-up fa-2x" aria-hidden="true"></i></a></li>'; 
      } else {
        $view1.='<li onclick="return confirmApprove();"><a href="'.base_url().'admin/approveJob?id='.$user->j_id.'"title="disapprove job"><i class="fa fa-thumbs-down fa-2x" aria-hidden="true"></i></a></li>'; 
      }
      $view1.=' <li><a href="'.base_url().'admin/viewJobDescription?id='.$user->j_id.'"><i class="fa fa-eye" aria-hidden="true"></i></a></li>';

      $view1.='<li><a href="'.base_url().'admin/updateJobs?id='.$user->j_id.'"><i class="fa fa-edit" aria-hidden="true"></i></a></li>';
      $view1.='<ul/>';
             // preparing an array
      $nestedData = array();
      $nestedData[] = $count++;
      $nestedData[] = $view;
      $nestedData[] = $user->recruiter;
      $nestedData[] = $user->status_name;
      $nestedData[] = $user->min_salary.'-'.$user->max_salary;
      $nestedData[] = $client_name;
      $nestedData[] = date('jS M Y',strtotime($user->created_date));
      $nestedData[] = date('jS M Y',strtotime($user->open_date));
      $nestedData[] = $user->job_type;
      $nestedData[] = $view1;
      $data[] = $nestedData;

}
}

$json_data = array(
            "draw" => intval($requestData['draw']), // for every request/draw by clientside , they send a number as a parameter, when they recieve a response/data they first check the draw number, so we are sending same number in draw. 
            "recordsTotal" => intval($totalData), // total number of records
            "recordsFiltered" => intval($totalFiltered), // total number of records after searching, if there is no searching then totalFiltered = totalData
            "data" => $data   // total data array
            );
echo json_encode($json_data);


}


//function for updating blog information
function updateBlog(){
  $id=$this->input->get('id');
  $data['fetchBlogInfo']=$this->Admin_model->getBlogInfo($id);

  $this->load->view('admin/all_css');
  $this->load->view('admin/dashboard_header');
  $this->load->view('admin/blogForm',$data);
  $this->load->view('admin/footer');
  $this->load->view('admin/all_js');
  $this->load->view('admin/pagewise_js/blogForm.js');

}

//function for deleting a blog
function deleteBlog(){
  $id=$this->input->get('id');
  $result=$this->Admin_model->deleteBlog($id);
  if($result){
    $this->session->set_flashdata('successmsg', '<div class="alert alert-success"><a type="button" class="close" data-dismiss="alert" aria-hidden="true">×</a>Your request has been completed successfully.</div>');
    redirect(base_url().'admin/viewAllBlogs');
  }else{
    $this->session->set_flashdata('errormsg', '<div class="alert alert-warning alert-dismissable"><a type="button" class="close" data-dismiss="alert" aria-hidden="true">×</a>Error while submitting.</div>');
    redirect(base_url().'admin/viewAllBlogs');
  }

}

//function for viewing email templates
function emailTemplates(){

  $adminSessionData=$this->session->userdata(base_url().'login_session');
  $this->checkSession($adminSessionData);
  $this->load->view('admin/all_css');
  $this->load->view('admin/dashboard_header');
  $this->load->view('admin/addTemplate');
  $this->load->view('admin/footer');
  $this->load->view('admin/all_js');
  $this->load->view('admin/pagewise_js/template.js');

}

//function for fetching particular package jobs
 function getJobs(){
  $id=$this->input->post('id');
  //echo $id;exit;
  $fetchJobs=$this->Admin_model->fetchParticularPackageJob($id);

  if(!empty($fetchJobs)){
    $totalJobs=$fetchJobs->package_jobs;
    echo $totalJobs;
  
  // $array=array(
  //  'total_jobs'=>$totalJobs,
  //  'premium_jobs'=>$premium_jobs
  //   );
  // print_r(json_encode($array));
  }else{

   echo '';
  }

 }

 //function for fetching premium jobs
 function getPremiumJobs(){
  $id=$this->input->post('id');
  $fetchJobs=$this->Admin_model->fetchParticularPackageJob($id);
  
  
  //echo $premium_jobs;exit;
  if(!empty($fetchJobs)){
    $premium_jobs=$fetchJobs->premium_jobs;
    echo $premium_jobs;
  
  // $array=array(
  //  'total_jobs'=>$totalJobs,
  //  'premium_jobs'=>$premium_jobs
  //   );
  // print_r(json_encode($array));
  }else{

   echo '';
  }

 }

 //function for viewing CSV Form
function viewCSVForm(){

  $this->load->view('admin/all_css');
  $this->load->view('admin/dashboard_header');
  $this->load->view('admin/viewCSVForm');
  $this->load->view('admin/footer');
  $this->load->view('admin/all_js');
  $this->load->view('admin/pagewise_js/viewCSVForm.js');

}

//function for importing CSV file
function importCSV(){

if(isset($_POST["import"]))
    {
        $filename=$_FILES["file"]["tmp_name"];
        if($_FILES["file"]["size"] > 0)
          {
            $file = fopen($filename, "r");
             while (($importdata = fgetcsv($file, 10000, ",")) !== FALSE)
             {
                    $data = array(
                        'id' => $importdata[0],
                        'first_name' =>$importdata[1],
                        'last_name' =>$importdata[2],
                        'user_city' =>$importdata[3],
                        'user_email' =>$importdata[4],
                        'user_password'=>md5($importdata[5]),
                        'flag'=>$importdata[6],
                        'created_date' => date('Y-m-d'),
                        );
                    //print_r($data);exit;
             $insert = $this->Admin_model->insertCSV($data);
             }                    
            fclose($file);
      $this->session->set_flashdata('message', 'Data imported successfully..');
        redirect('admin/viewCSVForm');
          }else{
      $this->session->set_flashdata('message', 'Something went wrong..');
      redirect('admin/viewCSVForm');
    }
    }


}


//function for viewing client CSV form
function viewClientCSVForm(){
  $this->load->view('admin/all_css');
  $this->load->view('admin/dashboard_header');
  $this->load->view('admin/viewClientCSVForm');
  $this->load->view('admin/footer');
  $this->load->view('admin/all_js');
  $this->load->view('admin/pagewise_js/viewClientCSVForm.js');

}

//function for importing client CSV file
function importClientCSV(){
if(isset($_POST["import"]))
    {
        $filename=$_FILES["file"]["tmp_name"];
        if($_FILES["file"]["size"] > 0)
          {
            $file = fopen($filename, "r");
             while (($importdata = fgetcsv($file, 10000, ",")) !== FALSE)
             {
                    $data = array(
                        'id' => $importdata[0],
                        'clnt_first_name' =>$importdata[1],
                        'clnt_last_name' =>$importdata[2],
                        'clnt_city' =>$importdata[3],
                        'clnt_country' =>$importdata[4],
                        'clnt_telephone' =>$importdata[5],
                        'clnt_email' =>$importdata[6],
                        'clnt_password'=>md5($importdata[7]),
                        'flag'=>$importdata[8],
                        'created_date' => date('Y-m-d'),
                        );
                    //print_r($data);exit;
             $insert = $this->Admin_model->insertClientCSV($data);
             }                    
            fclose($file);
      $this->session->set_flashdata('message', 'Data imported successfully..');
        redirect('admin/viewCSVForm');
          }else{
      $this->session->set_flashdata('message', 'Something went wrong..');
      redirect('admin/viewCSVForm');
    }
    }

}

//function for inserting email format
function insertEmailFormat(){
$adminSessionData=$this->session->userdata(base_url().'login_session');
$this->checkSession($adminSessionData);
$admin_id=$adminSessionData['admin_id'];
$template=$this->input->post('template_name');  
$subject=$this->input->post('subject');
$message=$this->input->post('message');

$insertArr=array(
 'admin_id'=>$admin_id,
 'template_name'=>$template,
 'subject'=>$subject,
 'message'=>$message

  );

$result=$this->Admin_model->insertEmailFormatData($insertArr);

if($result){
$this->session->set_flashdata('message', '<div class="alert alert-success"><a type="button" class="close" data-dismiss="alert" aria-hidden="true">×</a>Email Format has  been saved successfully.</div>');
redirect('admin/emailTemplates');
}else{
$this->session->set_flashdata('message', '<div class="alert alert-danger"><a type="button" class="close" data-dismiss="alert" aria-hidden="true">×</a>Email Format has not been saved successfully.</div>');
redirect('admin/emailTemplates');
}

}

//function for uploading interview map
function upload_interview_map(){
  $filename=$_FILES['file']['name'];
  //echo $filename;exit;
  $target = 'interview_maps/';
  $file = $target.$filename;

  if(move_uploaded_file($_FILES['file']['tmp_name'], $file))
  {

    print_r(json_encode(array('filename'=>$filename)));

  }else{


    echo 'not uploded';
  }

}

//function for viewing interview form
function viewInterviewForm(){
 $adminSessionData=$this->session->userdata(base_url().'login_session');
  $this->checkSession($adminSessionData);
  error_reporting(0);
  $data['id']=$this->input->get('id');
  $id=$this->input->get('id');
  $candidate_name=$this->Admin_model->getCandidateName($id);
  $data['candidateFirstName']=$candidate_name->first_name;
  $data['candidateLastName']=$candidate_name->last_name;
  //echo $data['id'];exit;
  
  $data['allClients']=$this->Admin_model->getAllClients();
  $this->load->view('admin/all_css');
  $this->load->view('admin/dashboard_header');
  $this->load->view('admin/interviewForm',$data);
  $this->load->view('admin/footer');
  $this->load->view('admin/all_js');
  $this->load->view('admin/pagewise_js/interview.js');


}
//function for viewing training form
function viewTrainingForm(){
  $adminSessionData=$this->session->userdata(base_url().'login_session');
  $this->checkSession($adminSessionData);
  $this->load->view('admin/all_css');
  $this->load->view('admin/dashboard_header');
  $this->load->view('admin/trainingForm');
  $this->load->view('admin/footer');
  $this->load->view('admin/all_js');
  $this->load->view('admin/pagewise_js/training.js');



}

//function for viewing Calender
function viewCalender(){
  $adminSessionData=$this->session->userdata(base_url().'login_session');
  $admin_id=$adminSessionData['admin_id'];
  $this->checkSession($adminSessionData);
  $type=$this->input->post('type');
  //echo $type;
  error_reporting(0);
  if($type =="events"){

    $data['allClients']=$this->Admin_model->getAllClients();
    $data['allCandidates']=$this->Admin_model->fetchCandidates();
    $events = array();
    $allEvents=$this->Admin_model->fetchAllEvents($admin_id);
    foreach($allEvents as $key){
     $e = array();
     //$title=$key->event_title;
     $e['id'] = $key->id; 
     $e['title'] = $key->event_title;
     $e['start'] = date('Y-m-d',strtotime($key->date));
     $e['end'] = date('Y-m-d',strtotime($key->end_date));
     $e['color']='#8175c7';
     $e['url']='viewEventInfo?id='.$key->id.'';
   //$allday = ($key->allDay == "true") ? true : false;
   //$e['allDay'] = $allday;
     array_push($events, $e);
   }

 }else if($type=="interview"){
   $events = array();
   $allInterviews=$this->Admin_model->fetchAllInterviews($admin_id);

   foreach($allInterviews as $key){
     $e = array();
     $e['id'] = $key->id; 
     $e['title'] = $key->first_name.' '.$key->last_name; 
     $e['start'] = date('Y-m-d',strtotime($key->interview_date));
   //$e['end'] = date('Y-m-d',strtotime($key->end_date));
     $e['color']='#5cb85d';
     $e['url']='viewInterviewInfo?id='.$key->id.'';
   //$allday = ($key->allDay == "true") ? true : false;
   //$e['allDay'] = $allday;
     array_push($events, $e);
   }

 }else if($type=="jobs"){
   $events = array();
   $allJobs=$this->Admin_model->get_mainJob_list();

   foreach($allJobs as $key){
    $e = array();
    $e['id'] = $key->j_id; 
    $e['title'] = $key->job_title; 
    $e['start'] = date('Y-m-d',strtotime($key->created_date));
    $e['end'] = date('Y-m-d',strtotime($key->open_date));
    $e['color']='#aa66cc';
    $e['url']='viewJobDescription?id='.$key->j_id.'';
   //$allday = ($key->allDay == "true") ? true : false;
   //$e['allDay'] = $allday;
    array_push($events, $e);


  }
}

else if($type=="all"){
 $data['allClients']=$this->Admin_model->getAllClients();
 $data['allCandidates']=$this->Admin_model->fetchCandidates();
 $events = array();
 $allEvents=$this->Admin_model->fetchAllEvents($admin_id);
 foreach($allEvents as $key){
   $e = array();
   $e['id'] = $key->id; 
   $e['title'] = $key->event_title; 
   $e['start'] = date('Y-m-d',strtotime($key->date));
   $e['end'] = date('Y-m-d',strtotime($key->end_date));
   $e['color']='#8175c7';

   $e['url']='viewEventInfo?id='.$key->id.'';
   //$allday = ($key->allDay == "true") ? true : false;
   //$e['allDay'] = $allday;
   array_push($events, $e);
   

 }
 
 $allInterviews=$this->Admin_model->fetchAllInterviews($admin_id);

 foreach($allInterviews as $key){
   $e = array();
   $e['id'] = $key->id; 
   $e['title'] = $key->first_name.' '.$key->last_name; 
   $e['start'] = date('Y-m-d',strtotime($key->interview_date));
   //$e['end'] = date('Y-m-d',strtotime($key->end_date));
   $e['color']='#5cb85d';
   $e['url']='viewInterviewInfo?id='.$key->id.'';
   //$allday = ($key->allDay == "true") ? true : false;
   //$e['allDay'] = $allday;
   array_push($events, $e);
 }



 $allJobs=$this->Admin_model->get_mainJob_list();
    //echo '<pre>';print_r($allJobs);exit;
 foreach($allJobs as $key){
  $e = array();
  $e['id'] = $key->j_id; 
  $e['title'] = $key->job_title; 
  $e['start'] = date('Y-m-d',strtotime($key->created_date));
  $e['end'] = date('Y-m-d',strtotime($key->open_date));
  $e['color']='#aa66cc';
  $e['url']='viewJobDescription?id='.$key->j_id.'';
   //$allday = ($key->allDay == "true") ? true : false;
   //$e['allDay'] = $allday;
  array_push($events, $e);


}



}else{
  $data['allClients']=$this->Admin_model->getAllClients();
  $data['allCandidates']=$this->Admin_model->fetchCandidates();
  $events = array();
  $allEvents=$this->Admin_model->fetchAllEvents($admin_id);
  foreach($allEvents as $key){
   $e = array();
   $e['id'] = $key->id; 
   $e['title'] = $key->event_title; 
   $e['start'] = date('Y-m-d',strtotime($key->date));
   $e['end'] = date('Y-m-d',strtotime($key->end_date));
   $e['color']='#8175c7';

   $e['url']='viewEventInfo?id='.$key->id.'';
   //$allday = ($key->allDay == "true") ? true : false;
   //$e['allDay'] = $allday;
   array_push($events, $e);
   

 }




 $allInterviews=$this->Admin_model->fetchAllInterviews($admin_id);

 foreach($allInterviews as $key){
   $e = array();
   $e['id'] = $key->id; 
   $e['title'] = $key->first_name.' '.$key->last_name; 
   $e['start'] = date('Y-m-d',strtotime($key->interview_date));
   //$e['end'] = date('Y-m-d',strtotime($key->end_date));
   $e['color']='#5cb85d';
   $e['url']='viewInterviewInfo?id='.$key->id.'';
   //$allday = ($key->allDay == "true") ? true : false;
   //$e['allDay'] = $allday;
   array_push($events, $e);
 }

 $allJobs=$this->Admin_model->get_mainJob_list();
    //echo '<pre>';print_r($allJobs);exit;
 foreach($allJobs as $key){
  $e = array();
  $e['id'] = $key->j_id; 
  $e['title'] = $key->job_title; 
  $e['start'] = date('Y-m-d',strtotime($key->created_date));
  $e['end'] = date('Y-m-d',strtotime($key->open_date));
  $e['color']='#aa66cc';
  $e['url']='viewJobDescription?id='.$key->j_id.'';
   //$allday = ($key->allDay == "true") ? true : false;
   //$e['allDay'] = $allday;
  array_push($events, $e);


}


}


$data['events']=$events;
  //echo '<pre>';print_r($data['events']);exit;
$this->load->view('admin/all_css');
$this->load->view('admin/dashboard_header');
$this->load->view('admin/calender',$data);
$this->load->view('admin/footer');
$this->load->view('admin/all_js',$data);
$this->load->view('admin/pagewise_js/calender.js');


}
//function for viewing data form
function viewDataForm(){
  $adminSessionData=$this->session->userdata(base_url().'login_session');
  $this->checkSession($adminSessionData);
  $this->load->view('admin/all_css');
  $this->load->view('admin/dashboard_header');
  $this->load->view('admin/allocatedData');
  $this->load->view('admin/footer');
  $this->load->view('admin/all_js');
  $this->load->view('admin/pagewise_js/allocatedData.js');


}




//function for creating an event
function createEvent(){
  $adminSessionData=$this->session->userdata(base_url().'login_session');
  $admin_id=$adminSessionData['admin_id'];
  $_POST = json_decode(file_get_contents('php://input'), true);
   //print_r($_POST);exit;

  $this->form_validation->set_rules('event_title', 'Event Title', 'trim|required|trim|xss_clean|strip_tags');
  $this->form_validation->set_rules('client_name', 'Client Name', 'trim|required|trim|xss_clean|strip_tags');
  $this->form_validation->set_rules('candidate_name', 'Candidate Name', 'trim|required|trim|xss_clean|strip_tags');
  $this->form_validation->set_rules('type', 'Type', 'trim|required|trim|xss_clean|strip_tags');
  $this->form_validation->set_rules('state', 'State', 'trim|required|trim|xss_clean|strip_tags');
  $this->form_validation->set_rules('desc', 'Description', 'trim|required|trim|xss_clean|strip_tags');


  if ($this->form_validation->run() == TRUE) {
   $insert_eventInfo['admin_id']=$admin_id;
   $insert_eventInfo['event_title']=$this->input->post('event_title');
   $insert_eventInfo['client_name']=$this->input->post('client_name');
   $insert_eventInfo['candidate_name']=$this->input->post('candidate_name');
   $insert_eventInfo['type']=$this->input->post('type');
   //$insert_eventInfo['public']=$this->input->post('public');
   $insert_eventInfo['date']=date('Y-m-d',strtotime($this->input->post('date')));
   $insert_eventInfo['end_date']=date('Y-m-d',strtotime($this->input->post('end_date')));
   $insert_eventInfo['time']=$this->input->post('time');
   $insert_eventInfo['hours']=$this->input->post('hours');
   $insert_eventInfo['minutes']=$this->input->post('minutes');
   $insert_eventInfo['meridien']=$this->input->post('meridium');
   $insert_eventInfo['organiser']=$this->input->post('organiser');
   $insert_eventInfo['contact']=$this->input->post('contact');
   $insert_eventInfo['contact_details']=$this->input->post('contact_details');
   $insert_eventInfo['event_details']=$this->input->post('event_details');
   $insert_eventInfo['event_url']=$this->input->post('event_url');
   //$insert_eventInfo['industry']=$this->input->post('industry');
   $insert_eventInfo['state']=$this->input->post('state');
   $insert_eventInfo['description']=$this->input->post('desc');
   $event_id=$this->input->post('event_id');
  // echo $event_id;exit;
   $checkEventData=$this->Admin_model->checkEventData($event_id);
   //echo '<pre>';print_r($checkEventData);exit;
   if(!empty($checkEventData)){
     $result=$this->Admin_model->updateEventInfo($insert_eventInfo,$event_id);
       //echo '<pre>';print_r($this->db->last_query());exit; 
   }else{
    
   $result=$this->Admin_model->insertEventInfo($insert_eventInfo);
   //echo '<pre>';print_r($this->db->last_query());exit;
   }
   //echo '<pre>';print_r($insert_eventInfo);exit;
   

   if($result){
    $data = array('status' => '1');
    $this->session->set_flashdata('successmsg', '<div class="alert alert-success"><a type="button" class="close" data-dismiss="alert" aria-hidden="true">×</a>Your request has been completed successfully.</div>');

  }else{
    $data = array('status' => '0');
    $this->session->set_flashdata('errormsg', '<div class="alert alert-warning alert-dismissable"><a type="button" class="close" data-dismiss="alert" aria-hidden="true">×</a>Error while submitting.</div>');
  }


}else{

 $data = array('status' => '2', 'error' => $this->form_validation->get_field_data(), 'postdata' => $this->input->post());   
}
print_r(json_encode($data));


}





//function for viewing event information
function viewEventInfo(){
$adminSessionData=$this->session->userdata(base_url().'login_session');
$this->checkSession($adminSessionData);
$event_id=$this->input->get('id');
$data['eventInfo']=$this->Admin_model->getEventInformation($event_id);
//echo '<pre>';print_r($data['eventInfo']);exit;
$this->load->view('admin/all_css');
$this->load->view('admin/dashboard_header');
$this->load->view('admin/eventInformation',$data);
$this->load->view('admin/footer');
$this->load->view('admin/all_js');
//$this->load->view('admin/pagewise_js/eventInfo.js');

}

//function for viewing event form
function viewEventForm(){
  $adminSessionData=$this->session->userdata(base_url().'login_session');
  $admin_id=$adminSessionData['admin_id'];
  $this->checkSession($adminSessionData);
  $data['allClients']=$this->Admin_model->getAllClients();
  $data['allCandidates']=$this->Admin_model->fetchCandidates();
  $this->load->view('admin/all_css');
  $this->load->view('admin/dashboard_header');
  $this->load->view('admin/eventForm',$data);
  $this->load->view('admin/footer');
  $this->load->view('admin/all_js');
  $this->load->view('admin/pagewise_js/event.js');


}


//function for viewing events
function viewEvents(){

  $adminSessionData=$this->session->userdata(base_url().'login_session');
  $this->checkSession($adminSessionData);
  $this->load->view('admin/all_css');
  $this->load->view('admin/dashboard_header');
  $this->load->view('admin/viewEvents');
  $this->load->view('admin/footer');
  $this->load->view('admin/all_js');
  $this->load->view('admin/pagewise_js/viewEvents.js');


}

//function for viewing event data
function eventData(){
$admin_data=$this->session->userdata(base_url().'login_session');
$admin_id=$admin_data['admin_id'];
  $requestData = $_REQUEST;
  $totalData = $this->Admin_model->getAllEvents($admin_id);
  $totalFiltered = $totalData = count($totalData);
  $totalFiltered = $this->Admin_model->getEventsfilteredList($requestData,$admin_id);
 //echo '<pre>';print_r($totalFiltered);exit;
  $totalFiltered = count($totalFiltered);

  $data = array();
  $userlist = $this->Admin_model->getEventsList($requestData,$admin_id);
  // echo '<pre>';print_r($this->db->last_query());exit;
  if (!empty($userlist)) {
    $count=1;
    foreach ($userlist as $key) {
      
      //$view='<a href="'.base_url().'admin/viewJobDescription?id='.$user->j_id.'">'.$user->job_title.'</a>';
      $view1='<ul class="list-unstyled list-inline" style="padding-left: 0px;">';

      
      //$view1.=' <li><a href="'.base_url().'admin/viewJobDescription?id='.$user->j_id.'"><i class="fa fa-eye" aria-hidden="true"></i></a></li>';

      $view1.='<li><a href="'.base_url().'admin/updateEvents?id='.$key->id.'"><i class="fa fa-edit" aria-hidden="true"></i></a></li>';
      $view1.='<li><a href="'.base_url().'admin/deleteEvents?id='.$key->id.'" onclick="return confirmMsg();"><i class="fa fa-trash" aria-hidden="true"></i></a></li>';
      $view1.='<ul/>';
             //preparing an array
      $nestedData = array();
      $nestedData[] = $count++;
      
      $nestedData[] = $key->event_title;
      $nestedData[] = $key->clnt_first_name.' '.$key->clnt_last_name;
      $nestedData[] = $key->first_name.' '.$key->last_name;
      $nestedData[] = $key->type;
      $nestedData[] = $key->state;
      $nestedData[] = $key->description;
      $nestedData[] = date('jS M Y',strtotime($key->created_date));
      $nestedData[] = $view1;
      $data[] = $nestedData;

    }
  }

  $json_data = array(
            "draw" => intval($requestData['draw']), // for every request/draw by clientside , they send a number as a parameter, when they recieve a response/data they first check the draw number, so we are sending same number in draw. 
            "recordsTotal" => intval($totalData), // total number of records
            "recordsFiltered" => intval($totalFiltered), // total number of records after searching, if there is no searching then totalFiltered = totalData
            "data" => $data   // total data array
            );
  echo json_encode($json_data);



}

//function for updating events
function updateEvents(){
  $adminSessionData=$this->session->userdata(base_url().'login_session');
  $this->checkSession($adminSessionData);
  $id=$this->input->get('id');
  //echo $id;exit;
  $data['eventData']=$this->Admin_model->fetchEventData($id);
  $data['allClients']=$this->Admin_model->getAllClients();
  $data['allCandidates']=$this->Admin_model->fetchCandidates();
  $this->load->view('admin/all_css');
  $this->load->view('admin/dashboard_header');
  $this->load->view('admin/eventForm',$data);
  $this->load->view('admin/footer');
  $this->load->view('admin/all_js');
  $this->load->view('admin/pagewise_js/event.js');

}

//function for deleting events
function deleteEvents(){
$id=$this->input->get('id');
$result=$this->Admin_model->deleteEventsId($id);
if($result){
$this->session->set_flashdata('successmsg', '<div class="alert alert-success alert-dismissable"><a type="button" class="close" data-dismiss="alert" aria-hidden="true">×</a>Event has been deleted successfully.</div>');
redirect(base_url().'admin/viewEvents');
}else{

$this->session->set_flashdata('errormsg', '<div class="alert alert-warning alert-dismissable"><a type="button" class="close" data-dismiss="alert" aria-hidden="true">×</a>Event has not been deleted successfully.</div>');
redirect(base_url().'admin/viewEvents');
}

}

//function for creating job add
function createJobAdd(){
$adminSessionData=$this->session->userdata(base_url().'login_session');
$this->checkSession($adminSessionData);
$admin_id=$adminSessionData['admin_id'];
$data['id']=$this->input->get('id');
$id=$this->input->get('id');
$data['fetchPackageData']=$this->Admin_model->getJobAdv($id);
$this->load->view('admin/all_css');
$this->load->view('admin/dashboard_header');
$this->load->view('admin/jobAdv',$data);
$this->load->view('admin/footer');
$this->load->view('admin/all_js');
$this->load->view('admin/pagewise_js/jobadv.js');

}

//function for inserting job adds
function insertJobAdds(){
$_POST = json_decode(file_get_contents('php://input'), true);
//echo '<pre>';print_r($_POST);
$adminSessionData=$this->session->userdata(base_url().'login_session');
$this->checkSession($adminSessionData);
$admin_id=$adminSessionData['admin_id'];

if($this->input->post()){
$this->form_validation->set_rules('job_adds', 'Job Adds', 'trim|required|trim|xss_clean|strip_tags');
$this->form_validation->set_rules('type', 'Type', 'trim|required|trim|xss_clean|strip_tags');
$this->form_validation->set_rules('duration', 'Duration', 'trim|required|trim|xss_clean|strip_tags');

 if ($this->form_validation->run() == TRUE) {
   $insertJobAdds['admin_id'] = $admin_id;
   $insertJobAdds['package_id'] = $this->input->post('package_id');
   $insertJobAdds['job_adds'] = $this->input->post('job_adds');
   $insertJobAdds['type']     =    $this->input->post('type');
   $insertJobAdds['duration'] = $this->input->post('duration');
   //echo '<pre>';print_r($insertJobAdds);exit;

   $result=$this->Admin_model->insertJobAdv($insertJobAdds);

   if($result){
    $data = array('status' => '1');
    $this->session->set_flashdata('successmsg', '<div class="alert alert-success alert-dismissable"><a type="button" class="close" data-dismiss="alert" aria-hidden="true">×</a>Your request has been completed successfully.</div>');
   }else{
   $data = array('status' => '0');
   $this->session->set_flashdata('errormsg', '<div class="alert alert-warning alert-dismissable"><a type="button" class="close" data-dismiss="alert" aria-hidden="true">×</a>Your request has not been completed successfully.</div>');

   }

 }else{

$data = array('status' => '2', 'error' => $this->form_validation->get_field_data(), 'postdata' => $this->input->post());

 }

}else{
$data = array('status' => '3');
$this->session->set_flashdata('errormsg', '<div class="alert alert-warning alert-dismissable"><a type="button" class="close" data-dismiss="alert" aria-hidden="true">×</a>All fields are mandatory.</div>');

}
print_r(json_encode($data));

}

//function for candidates applied jobs
function candidateAppliedJobs(){
$candidate_id=$this->input->get('id');
$data['candidate_id']=$candidate_id;
$this->load->view('admin/all_css');
$this->load->view('admin/dashboard_header');
$this->load->view('admin/viewCandidatedAppliedJobs',$data);
$this->load->view('admin/footer');
$this->load->view('admin/all_js');
$this->load->view('admin/pagewise_js/appliedJobs.js');
}

//function for viewing applied jobs
function viewCandidatesJobs(){
$id=$this->input->post('candidate_id');
//echo $id;exit;
  $requestData = $_REQUEST;
  $totalData = $this->Admin_model->getAllCandidatesApplyJobs($id);
  $totalFiltered = $totalData = count($totalData);
  $totalFiltered = $this->Admin_model->getCandidatesApplyJobsfilteredList($requestData,$id);
 //echo '<pre>';print_r($totalFiltered);exit;
  $totalFiltered = count($totalFiltered);

  $data = array();
  $userlist = $this->Admin_model->getCandidatesApplyJobsList($requestData,$id);
  // echo '<pre>';print_r($this->db->last_query());exit;
  if (!empty($userlist)) {
    $count=1;
    foreach ($userlist as $key) {
      //print_r($key);exit;
      
      //$view='<a href="'.base_url().'admin/viewJobDescription?id='.$user->user_id.'">'.$user->job_title.'</a>';
      $view1='<ul class="list-unstyled list-inline" style="padding-left: 0px;">';

      
      $view1.=' <li><a href="'.base_url().'admin/viewInterviewForm?id='.$key->user_id.'"><i class="fa fa-briefcase" aria-hidden="true" title="Schedule Interview"></i></a></li>';

      //$view1.='<li><a href="'.base_url().'admin/updateEvents?id='.$key->id.'"><i class="fa fa-edit" aria-hidden="true"></i></a></li>';
      //$view1.='<li><a href="'.base_url().'admin/deleteEvents?id='.$key->id.'" onclick="return confirmMsg();"><i class="fa fa-trash" aria-hidden="true"></i></a></li>';
      $view1.='<ul/>';
             //preparing an array
      $nestedData = array();
      $nestedData[] = $count++;
  
      //$nestedData[] = $key->first_name.' '.$key->last_name;
      $nestedData[] = $key->user_city;
      $nestedData[] = $key->user_email;
      $nestedData[] = $key->job_title;
      $nestedData[] = $key->min_salary.'-'.$key->max_salary;
      //$nestedData[] = date('jS M Y',strtotime($key->created_date));
      $nestedData[] = $view1;
      $data[] = $nestedData;

    }
  }

  $json_data = array(
            "draw" => intval($requestData['draw']), // for every request/draw by clientside , they send a number as a parameter, when they recieve a response/data they first check the draw number, so we are sending same number in draw. 
            "recordsTotal" => intval($totalData), // total number of records
            "recordsFiltered" => intval($totalFiltered), // total number of records after searching, if there is no searching then totalFiltered = totalData
            "data" => $data   // total data array
            );
  echo json_encode($json_data);
}


//function for viewing view and save CV form
function viewAndSaveCV(){
$adminSessionData=$this->session->userdata(base_url().'login_session');
$this->checkSession($adminSessionData);
$id=$this->input->get('id');
$data['cvInfo']=$this->Admin_model->getCVInfo($id);
//echo '<pre>';print_r($data['cvInfo']);exit; 
$data['package_id']=$id; 
$this->load->view('admin/all_css');
$this->load->view('admin/dashboard_header');
$this->load->view('admin/viewCVForm',$data);
$this->load->view('admin/footer');
$this->load->view('admin/all_js');
$this->load->view('admin/pagewise_js/viewCV.js');


}


//function for inserting view and save cv's
function insertViewAndSaveCV(){
$adminSessionData=$this->session->userdata(base_url().'login_session');
$admin_id=$adminSessionData['admin_id'];
$_POST = json_decode(file_get_contents('php://input'), true);  
$this->form_validation->set_rules('no_of_cv', 'Number of CV', 'trim|required|trim|xss_clean|strip_tags');
$this->form_validation->set_rules('no_of_views', 'Number of Views', 'trim|required|trim|xss_clean|strip_tags');
$this->form_validation->set_rules('no_of_days', 'Number of Days', 'trim|required|trim|xss_clean|strip_tags');
  

  if ($this->form_validation->run() == TRUE) {
   $insertCVInfo['admin_id']=$admin_id;
   $insertCVInfo['package_id']=$this->input->post('package_id');
   $insertCVInfo['no_of_cv']=$this->input->post('no_of_cv');
   $insertCVInfo['no_of_views']=$this->input->post('no_of_views');
   $insertCVInfo['no_of_days']=$this->input->post('no_of_days');
  
   //echo '<pre>';print_r($insertCVInfo);exit;
   $result=$this->Admin_model->insertCVInfo($insertCVInfo);

   if($result){
    $data = array('status' => '1');
    $this->session->set_flashdata('successmsg', '<div class="alert alert-success"><a type="button" class="close" data-dismiss="alert" aria-hidden="true">×</a>Your request has been completed successfully.</div>');

  }else{
    $data = array('status' => '0');
    $this->session->set_flashdata('errormsg', '<div class="alert alert-warning alert-dismissable"><a type="button" class="close" data-dismiss="alert" aria-hidden="true">×</a>Error while submitting.</div>');
  }


}else{

 $data = array('status' => '2', 'error' => $this->form_validation->get_field_data(), 'postdata' => $this->input->post());   
}
print_r(json_encode($data));

}

//function for deleting view and save cv's
function deleteViewCV(){
$package_id=$this->input->get('package_id');
$id=$this->input->get('id');
$result=$this->Admin_model->deleteViewAndSaveCV($id);
//echo '<pre>';print_r($this->db->last_query());exit;
if($result){

$this->session->set_flashdata('successmsg', '<div class="alert alert-success"><a type="button" class="close" data-dismiss="alert" aria-hidden="true">×</a>Your request has been completed successfully.</div>');
redirect(base_url().'admin/viewAndSaveCV?id='.$package_id.'');  
}else{

$this->session->set_flashdata('errormsg', '<div class="alert alert-warning"><a type="button" class="close" data-dismiss="alert" aria-hidden="true">×</a>Your request has not been completed successfully.</div>');
redirect(base_url().'admin/viewAndSaveCV?id='.$package_id.'');  
}
}

//function for deleting job add
function deleteJobAdd(){
 $package_id=$this->input->get('package_id');
 $id=$this->input->get('id');
$result=$this->Admin_model->delete_job_adds($id);
//echo '<pre>';print_r($this->db->last_query());exit;
if($result){

$this->session->set_flashdata('successmsg', '<div class="alert alert-success"><a type="button" class="close" data-dismiss="alert" aria-hidden="true">×</a>Your request has been completed successfully.</div>');
redirect(base_url().'admin/createJobAdd?id='.$package_id.'');  
}else{

$this->session->set_flashdata('errormsg', '<div class="alert alert-warning"><a type="button" class="close" data-dismiss="alert" aria-hidden="true">×</a>Your request has not been completed successfully.</div>');
redirect(base_url().'admin/createJobAdd?id='.$package_id.'');  
}

}

//function for create interview
function create_interview(){
$adminSessionData=$this->session->userdata(base_url().'login_session');
$admin_id=$adminSessionData['admin_id'];
$_POST = json_decode(file_get_contents('php://input'), true);
//print_r($_POST);exit;
  //$this->form_validation->set_rules('candidate_name', 'Candidate Name', 'trim|required|trim|xss_clean|strip_tags');
  $this->form_validation->set_rules('location', 'Location', 'trim|required|trim|xss_clean|strip_tags');
  $this->form_validation->set_rules('main_contact', 'Main Contact', 'trim|required|trim|xss_clean|strip_tags');
  $this->form_validation->set_rules('interview_address', 'Interview Address', 'trim|required|trim|xss_clean|strip_tags');
  $this->form_validation->set_rules('breakdown_details', 'Breakdown Details', 'trim|required|trim|xss_clean|strip_tags');
  $this->form_validation->set_rules('additional_notes', 'Additional Notes', 'trim|required|trim|xss_clean|strip_tags');
  $this->form_validation->set_rules('start_time', 'Start Time', 'trim|required|trim|xss_clean|strip_tags');
  $this->form_validation->set_rules('finish_time', 'Finish Time', 'trim|required|trim|xss_clean|strip_tags');
  $this->form_validation->set_rules('contact_details', 'Contact Details', 'trim|required|trim|xss_clean|strip_tags');
  $this->form_validation->set_rules('site_contact', 'Site Contact', 'trim|required|trim|xss_clean|strip_tags');
  //$this->form_validation->set_rules('site_contact', 'Site Contact', 'trim|required|trim|xss_clean|strip_tags');


  if ($this->form_validation->run() == TRUE) {
   $insert_interviewInfo['admin_id']=$admin_id;
   $insert_interviewInfo['candidate_id']=$this->input->post('candidate_id');
   $insert_interviewInfo['candidate_name']=$this->input->post('candidate_name');
   $insert_interviewInfo['location']=$this->input->post('location');
   $insert_interviewInfo['interview_date']=date('Y-m-d',strtotime($this->input->post('interview_date')));
   $insert_interviewInfo['main_contact']=$this->input->post('main_contact');
   $insert_interviewInfo['interview_address']=$this->input->post('interview_address');
   
   $insert_interviewInfo['breakdown_interview']=$this->input->post('breakdown_details');
   $insert_interviewInfo['additional_notes']=$this->input->post('additional_notes');
   $insert_interviewInfo['event_start_time']=$this->input->post('start_time');
   $insert_interviewInfo['event_finish_time']=$this->input->post('finish_time');
   $client_name=$this->input->post('client_name');
   $insert_interviewInfo['contact_details']=$this->input->post('contact_details');
   $insert_interviewInfo['site_contact']=$this->input->post('site_contact');
   $image=$this->input->post('filename');
   $interview_id=$this->input->post('interview_id');

 
   
   $checkInterviewData=$this->Admin_model->checkInterviewData($interview_id);
   if(isset($image) && $image!=''){

    $insert_interviewInfo['saved_map']=$image;
  }else{

    $insert_interviewInfo['saved_map']=$checkInterviewData->saved_map;
  }

   if(isset($client_name) && $client_name!=''){

    $insert_interviewInfo['client_name']=$client_name;
  }else{

    $insert_interviewInfo['client_name']=$checkInterviewData->client_name;
  }

   if($checkInterviewData!=''){
   
    $result=$this->Admin_model->updateinterviewInfo($insert_interviewInfo,$interview_id);
    //echo '<pre>';print_r($result);exit;
   }else{

    $result=$this->Admin_model->insertinterviewInfo($insert_interviewInfo);
   }
 
   

   if($result){
    $data = array('status' => '1');
    $this->session->set_flashdata('successmsg', '<div class="alert alert-success"><a type="button" class="close" data-dismiss="alert" aria-hidden="true">×</a>Your request has been completed successfully.</div>');

  }else{
    $data = array('status' => '0');
    $this->session->set_flashdata('errormsg', '<div class="alert alert-warning alert-dismissable"><a type="button" class="close" data-dismiss="alert" aria-hidden="true">×</a>Error while submitting.</div>');
  }


}else{

 $data = array('status' => '2', 'error' => $this->form_validation->get_field_data(), 'postdata' => $this->input->post());   
}
print_r(json_encode($data));

}

//function for viewing interview Calender
function viewInterviewCalender(){
$adminSessionData=$this->session->userdata(base_url().'login_session');
$this->checkSession($adminSessionData);
$admin_id=$adminSessionData['admin_id'];
$interviews = array();
$allInterviews=$this->Admin_model->fetchAllInterviews($admin_id);
foreach($allInterviews as $key){
   $e = array();
   $e['id'] = $key->id; 
   $e['title'] = $key->first_name.' '.$key->last_name; 
   $e['start'] = date('Y-m-d',strtotime($key->interview_date));
   //$e['end'] = date('Y-m-d',strtotime($key->end_date));
   $e['color']='#5cb85d';
   $e['url']='viewInterviewInfo?id='.$key->id.'';
   //$allday = ($key->allDay == "true") ? true : false;
   //$e['allDay'] = $allday;
   array_push($interviews, $e);
  }
  $data['interviews']=$interviews;
  //echo'<pre>';print_r($data['interviews']);exit;
$this->load->view('admin/all_css');
$this->load->view('admin/dashboard_header');
$this->load->view('admin/interviewCalender',$data);
$this->load->view('admin/footer');
$this->load->view('admin/all_js',$data);
$this->load->view('admin/pagewise_js/interviewCalender.js');

}

//function for viewing Interview Info
function viewInterviewInfo(){
$adminSessionData=$this->session->userdata(base_url().'login_session');
$this->checkSession($adminSessionData);
$interview_id=$this->input->get('id');
$data['interviewInfo']=$this->Admin_model->getInterviewInformation($interview_id);
//echo '<pre>';print_r($data['interviewInfo']);exit;
$this->load->view('admin/all_css');
$this->load->view('admin/dashboard_header');
$this->load->view('admin/interviewInformation',$data);
$this->load->view('admin/footer');
$this->load->view('admin/all_js');


}

public function fetchClient() 
{ 
 
  $searchquery = $_GET["query"]; 

  $search=$searchquery;
  if ($search != '') 
  { 
      //echo 'here';exit;
   $result=$this->Admin_model->getAllClients();
   $json = array();
   
   foreach($result as $key){
     $ClientName=$key->company_name;

     $ClientId=$key->id;
     
     $json['suggestions'][] = array('value' => $ClientName,  'data' => $ClientId); 
     
   }
   
   //$json['suggestions'][] = array('value' => $clientname,  'data' => $clientname);
   print(json_encode($json)); 

 } 


 else 
 { 
          //echo 'there';exit;
  $this->session->set_flashdata("errormessage", "Sub Office does not exist"); 
  redirect('client/viewJobForm'); 
} 
}

public function fetchCandidate(){

$searchquery = $_GET["query"]; 

  $search=$searchquery;
  if ($search != '') 
  { 
      //echo 'here';exit;

   $result=$this->Admin_model->fetchCandidates();
   $json = array();
   
   foreach($result as $key){
    //print_r($key);exit;
     $CandidateName=$key->first_name.' '.$key->last_name;

     $CandidateId=$key->id;
     
     $json['suggestions'][] = array('value' => $CandidateName,  'data' => $CandidateId); 
     
   }
   
   //$json['suggestions'][] = array('value' => $clientname,  'data' => $clientname);
   print(json_encode($json)); 

 } 


 else 
 { 
          //echo 'there';exit;
  $this->session->set_flashdata("errormessage", "Sub Office does not exist"); 
  redirect('client/viewJobForm'); 
} 


}

//function for creating service
function createService(){
$adminSessionData=$this->session->userdata(base_url().'login_session');
$this->checkSession($adminSessionData);
$admin_id=$adminSessionData['admin_id'];
$package_id=$this->input->get('id');
$data['package_id']=$package_id;
error_reporting(0);
$data['serviceData']=$this->Admin_model->getPackageServices($package_id);
$fetchedservice=$data['serviceData']->service;
$data['service']=json_decode($fetchedservice);
//print_r($data['service']);exit;
$data['fetchServiceData']=$this->Admin_model->getServiceData($package_id);
$this->load->view('admin/all_css');
$this->load->view('admin/dashboard_header');
$this->load->view('admin/service',$data);
$this->load->view('admin/footer');
$this->load->view('admin/all_js');
$this->load->view('admin/pagewise_js/service.js',$data);

}

//function for inserting service....
function insertService(){
$adminSessionData=$this->session->userdata(base_url().'login_session');
$this->checkSession($adminSessionData);
$admin_id=$adminSessionData['admin_id']; 
$_POST=json_decode(file_get_contents('php://input'), true);
//echo '<pre>';print_r($_POST);exit;
if($this->input->post()){
$insertService['admin_id'] = $admin_id;
$insertService['package_id'] = $this->input->post('package_id');
$insertService['service'] = json_encode($this->input->post('test'));
$package_id=$this->input->post('package_id');
$checkServiceData=$this->Admin_model->getPackageServices($package_id);
if($checkServiceData!=''){

$result=$this->Admin_model->updateServices($insertService,$package_id);
}else{
$result=$this->Admin_model->insertServices($insertService);  
}
//echo '<pre>';print_r($insertService);exit;

if($result){
$data = array('status' => '1');
$this->session->set_flashdata('successmsg', '<div class="alert alert-success alert-dismissable"><a type="button" class="close" data-dismiss="alert" aria-hidden="true">×</a>Your request has been completed successfully.</div>');
}else{
$data = array('status' => '0');
$this->session->set_flashdata('errormsg', '<div class="alert alert-warning alert-dismissable"><a type="button" class="close" data-dismiss="alert" aria-hidden="true">×</a>Your request has not been completed successfully.</div>');
}

}else{
$data = array('status' => '3');
$this->session->set_flashdata('errormsg', '<div class="alert alert-warning alert-dismissable"><a type="button" class="close" data-dismiss="alert" aria-hidden="true">×</a>All fields are mandatory.</div>');


}
print_r(json_encode($data));
}

//function for viewing all Interviews
function viewAllInterviews(){
  $adminSessionData=$this->session->userdata(base_url().'login_session');
  $this->checkSession($adminSessionData);
  $this->load->view('admin/all_css');
  $this->load->view('admin/dashboard_header');
  $this->load->view('admin/viewInterviews');
  $this->load->view('admin/footer');
  $this->load->view('admin/all_js');
  $this->load->view('admin/pagewise_js/viewInterviews.js');


}


//function for viewing interview data
function interviewData(){
$admin_data=$this->session->userdata(base_url().'login_session');
$admin_id=$admin_data['admin_id'];
  $requestData = $_REQUEST;
  $totalData = $this->Admin_model->getAllInterviews($admin_id);
  $totalFiltered = $totalData = count($totalData);
  $totalFiltered = $this->Admin_model->getInterviewsfilteredList($requestData,$admin_id);
 //echo '<pre>';print_r($totalFiltered);exit;
  $totalFiltered = count($totalFiltered);

  $data = array();
  $userlist = $this->Admin_model->getInterviewsList($requestData,$admin_id);
  // echo '<pre>';print_r($this->db->last_query());exit;
  if (!empty($userlist)) {
    $count=1;
    foreach ($userlist as $key) {
      
      //$view='<a href="'.base_url().'admin/viewJobDescription?id='.$user->j_id.'">'.$user->job_title.'</a>';
      $view1='<ul class="list-unstyled list-inline" style="padding-left: 0px;">';

      
      //$view1.=' <li><a href="'.base_url().'admin/viewJobDescription?id='.$user->j_id.'"><i class="fa fa-eye" aria-hidden="true"></i></a></li>';

      $view1.='<li><a href="'.base_url().'admin/updateInterviews?id='.$key->id.' && candidate_id='.$key->candidate_name.'"><i class="fa fa-edit" aria-hidden="true"></i></a></li>';
      $view1.='<li><a href="'.base_url().'admin/deleteInterviews?id='.$key->id.'" onclick="return deleteInterview();"><i class="fa fa-trash" aria-hidden="true"></i></a></li>';
      $view1.='<ul/>';
             //preparing an array
      $nestedData = array();
      $nestedData[] = $count++;
      
      //$nestedData[] = $key->event_title;
      
      $nestedData[] = $key->first_name.' '.$key->last_name;
      $nestedData[] = $key->clnt_first_name.' '.$key->clnt_last_name;
      $nestedData[] = date('jS M Y',strtotime($key->interview_date));
      $nestedData[] = $key->interview_address;
      $nestedData[] = $key->main_contact;
      $nestedData[] = $key->contact_details;
      $nestedData[] = date('jS M Y',strtotime($key->created_date));
      $nestedData[] = $view1;
      $data[] = $nestedData;

    }
  }

  $json_data = array(
            "draw" => intval($requestData['draw']), // for every request/draw by clientside , they send a number as a parameter, when they recieve a response/data they first check the draw number, so we are sending same number in draw. 
            "recordsTotal" => intval($totalData), // total number of records
            "recordsFiltered" => intval($totalFiltered), // total number of records after searching, if there is no searching then totalFiltered = totalData
            "data" => $data   // total data array
            );
  echo json_encode($json_data);




}


//function for deleting a interview
function deleteInterviews(){
$id=$this->input->get('id');
$result=$this->Admin_model->deleteInterview($id);
if($result){
$this->session->set_flashdata('successmsg', '<div class="alert alert-success"><a type="button" class="close" data-dismiss="alert" aria-hidden="true">×</a>Interview has been deleted successfully.</div>');
redirect(base_url().'admin/viewAllInterviews');  

}else{
$this->session->set_flashdata('errormsg', '<div class="alert alert-error"><a type="button" class="close" data-dismiss="alert" aria-hidden="true">×</a>Interview has not been completed successfully.</div>');
redirect(base_url().'admin/viewAllInterviews'); 

}
}

//function for updating interview
function updateInterviews(){
$interview_id=$this->input->get('id');
$id=$this->input->get('candidate_id');
$candidate_name=$this->Admin_model->getCandidateName($id);
$data['candidateFirstName']=$candidate_name->first_name;
$data['candidateLastName']=$candidate_name->last_name;
//$data['interview_id']=$id;
$data['id']=$id;
// $candidate_name=$this->Admin_model->getCandidateName($id);
// $data['candidateFirstName']=$candidate_name->first_name;
// $data['candidateLastName']=$candidate_name->last_name;
//echo $id;exit;
$data['interviewData']=$this->Admin_model->fetchInterviewDataById($interview_id);
//echo '<pre>';print_r($data['interviewData']);exit;
$this->load->view('admin/all_css');
$this->load->view('admin/dashboard_header');
$this->load->view('admin/interviewForm',$data);
$this->load->view('admin/footer');
$this->load->view('admin/all_js');
$this->load->view('admin/pagewise_js/interview.js');

}

//function for creating timesheets
function createTimesheets(){
  $id=$this->input->get('id');
  $data['candidate_id']=$id;
  $data['candidateData']=$this->Admin_model->fetchCandidateData($id);
  $data['currency']=$this->Admin_model->fetchCurrencies();
  $adminSessionData=$this->session->userdata(base_url().'login_session');
  $this->checkSession($adminSessionData);
  $admin_id= $adminSessionData['admin_id'];
  $this->load->view('admin/all_css');
  $this->load->view('admin/dashboard_header');
  $this->load->view('admin/createtimesheet',$data);
  $this->load->view('admin/footer');
  $this->load->view('admin/all_js');
  $this->load->view('admin/pagewise_js/timesheet.js',$data);

}



//function for viewing Timesheets
function viewTimesheets(){
  $adminSessionData=$this->session->userdata(base_url().'login_session');
  $this->checkSession($adminSessionData);
  $admin_id= $adminSessionData['admin_id'];
  $this->load->view('admin/all_css');
  $this->load->view('admin/dashboard_header');
  $this->load->view('admin/viewtimesheet');
  $this->load->view('admin/footer');
  $this->load->view('admin/all_js');
  $this->load->view('admin/pagewise_js/viewtimesheet.js');


}

//function for changing package pic.
function changePackagePic(){
  $id=$this->input->get('id');
  $data['fetchPackagePic']=$this->Admin_model->getPackagePic($id);
  //print_r($this->db->last_query());exit;
  $data['id']=$id;
  $adminSessionData=$this->session->userdata(base_url().'login_session');
  $this->checkSession($adminSessionData);
  $admin_id= $adminSessionData['admin_id'];
  $this->load->view('admin/all_css');
  $this->load->view('admin/dashboard_header');
  $this->load->view('admin/packagePic',$data);
  $this->load->view('admin/footer');
  $this->load->view('admin/all_js');
  $this->load->view('admin/pagewise_js/packagePic.js',$data);


}

//function for package pic
function submitPackagePic(){
$adminSessionData=$this->session->userdata(base_url().'login_session');
$admin_id=$adminSessionData['admin_id'];
define('UPLOAD_DIR', 'package_images/');
  $_POST = json_decode(file_get_contents('php://input'), true);
   //print_r($_POST);exit;     
  if ($this->input->post()) {

    $image_name = $_POST['userfile'];
    $img = $_POST['imgdata'];
    $package_id=$_POST['package_id'];
         //echo $package_id; exit;
    $img = str_replace('data:image/png;base64,', '', $img);
    $img = str_replace(' ', '+', $img);
    $decoded = base64_decode($img);
    //$uid='1';
    $file = UPLOAD_DIR .$admin_id.'_'.$image_name;
    $file_name=$admin_id.'_'.$image_name;
          //echo $file_name;exit;
    $success = file_put_contents($file, $decoded);
    //echo $success;exit;
    if($success)
    {
     
      $insertArr=array('package_image'=>$file_name,'admin_id'=>$admin_id,'package_id' =>$package_id);
      //print_r($insertArr);exit;
      $checkPackageImage=$this->Admin_model->fetchPackageImgData($package_id);
      if(empty($checkPackageImage)){
       $upload = $this->Admin_model->insertPackagePic($insertArr);
       //print_r($this->db->last_query());exit;
      }else{
      $updateArr=array('package_image' => $file_name); 

      $upload = $this->Admin_model->savePackagePic($updateArr,$package_id);
      //print_r($this->db->last_query());exit;
      }

      
         //echo '<pre>';print_r($this->db->last_query());exit;

      if($upload)
      {

       $data = array('status' =>'1');
       $this->session->set_flashdata('successmsg', '<div class="alert alert-success"><a type="button" class="close" data-dismiss="alert" aria-hidden="true">×</a>Package picture has been uploaded successfully.</div>');
     }
   }

 }else{

   $data = array('status' => '2');
   $this->session->set_flashdata('errormsg', '<div class="alert alert-warning alert-dismissable"><a type="button" class="close" data-dismiss="alert" aria-hidden="true">×</a>Please select Package picture properly.</div>');


 }

 print_r(json_encode($data));


}


//function for creating timesheet
function createTimesheet(){
$adminSessionData=$this->session->userdata(base_url().'login_session');
$this->checkSession($adminSessionData);
$admin_id= $adminSessionData['admin_id'];
$_POST=json_decode(file_get_contents('php://input'), true);
// echo '<pre>';print_r($_POST);
//$this->form_validation->set_rules('day', 'Day', 'trim|required|trim|xss_clean|strip_tags');
$this->form_validation->set_rules('break_duartion', 'Unpaid Break Duration', 'trim|required|trim|xss_clean|strip_tags');
$this->form_validation->set_rules('pay_rate', 'Pay Rate', 'trim|required|trim|xss_clean|strip_tags');
$this->form_validation->set_rules('currency', 'Currency', 'trim|required|trim|xss_clean|strip_tags');
$this->form_validation->set_rules('pay_type', 'Pay Type', 'trim|required|trim|xss_clean|strip_tags');  

  if ($this->form_validation->run() == TRUE) {
   $insertTimesheetInfo['admin_id']=$admin_id;
   $insertTimesheetInfo['admin_id']=$admin_id;
   $insertTimesheetInfo['candidate_id']=$this->input->post('candidate_id');
   $insertTimesheetInfo['client_name']=$this->input->post('client_name');
   $insertTimesheetInfo['candidate_name']=$this->input->post('cand_name');
   // $insertTimesheetInfo['day']=$this->input->post('day');
   $insertTimesheetInfo['type']=$this->input->post('type');
   $insertTimesheetInfo['location']=$this->input->post('location');
   $insertTimesheetInfo['date']=date('Y-m-d',strtotime($this->input->post('date')));
   
   $insertTimesheetInfo['hours']=$this->input->post('hours');
   $insertTimesheetInfo['minutes']=$this->input->post('minutes');
   $insertTimesheetInfo['meridium']=$this->input->post('meridium');
   $insertTimesheetInfo['finish_hours']=$this->input->post('finish_hours');
   $insertTimesheetInfo['finish_minutes']=$this->input->post('finish_minutes');
   $insertTimesheetInfo['finish_meridium']=$this->input->post('finish_meridium');
   $insertTimesheetInfo['break_duartion']=$this->input->post('break_duartion');
   $insertTimesheetInfo['pay_rate']=$this->input->post('pay_rate');
   $insertTimesheetInfo['currency']=$this->input->post('currency');
   $insertTimesheetInfo['pay_type']=$this->input->post('pay_type');
   $id=$this->input->post('timesheet_id');
   //echo $id;
   //echo '<pre>';print_r($insertTimesheetInfo);exit;
   $checkTimesheetData=$this->Admin_model->fetchTimesheetData($id);
   if(!empty($checkTimesheetData)){
   $updateTimesheetInfo['admin_id']=$admin_id;
 
   $updateTimesheetInfo['date']=date('Y-m-d',strtotime($this->input->post('date')));
   $updateTimesheetInfo['client_name']=$this->input->post('client_name');
   $updateTimesheetInfo['candidate_name']=$this->input->post('cand_name');
   // $updateTimesheetInfo['day']=$this->input->post('day');
   $updateTimesheetInfo['type']=$this->input->post('type');
   $updateTimesheetInfo['hours']=$this->input->post('hours');
   $updateTimesheetInfo['minutes']=$this->input->post('minutes');
   $updateTimesheetInfo['meridium']=$this->input->post('meridium');
   $updateTimesheetInfo['finish_hours']=$this->input->post('finish_hours');
   $updateTimesheetInfo['finish_minutes']=$this->input->post('finish_minutes');
   $updateTimesheetInfo['finish_meridium']=$this->input->post('finish_meridium');
   $updateTimesheetInfo['break_duartion']=$this->input->post('break_duartion');
   $updateTimesheetInfo['pay_rate']=$this->input->post('pay_rate');
   $updateTimesheetInfo['currency']=$this->input->post('currency');
   $updateTimesheetInfo['pay_type']=$this->input->post('pay_type');
   $id=$this->input->post('timesheet_id');
  // echo '<pre>';print_r($updateTimesheetInfo);exit;
   $result=$this->Admin_model->updateTimesheetInfo($updateTimesheetInfo,$id);

   }else{
    
    $result=$this->Admin_model->insertTimesheetInfo($insertTimesheetInfo);
   }
   
   //echo '<pre>';print_r($result);exit;

   if($result){
    $data = array('status' => '1');
    $this->session->set_flashdata('successmsg', '<div class="alert alert-success"><a type="button" class="close" data-dismiss="alert" aria-hidden="true">×</a>Your request has been completed successfully.</div>');

  }else{
    $data = array('status' => '0');
    $this->session->set_flashdata('errormsg', '<div class="alert alert-warning alert-dismissable"><a type="button" class="close" data-dismiss="alert" aria-hidden="true">×</a>Error while submitting.</div>');
  }


}else{

 $data = array('status' => '2', 'error' => $this->form_validation->get_field_data(), 'postdata' => $this->input->post());   
}
print_r(json_encode($data));
}


//function for managing timesheet
function manageTimesheets(){
  $adminSessionData=$this->session->userdata(base_url().'login_session');
  $this->checkSession($adminSessionData);
  $admin_id= $adminSessionData['admin_id'];
  $this->load->view('admin/all_css');
  $this->load->view('admin/dashboard_header');
  $this->load->view('admin/viewTableTimesheet');
  $this->load->view('admin/footer');
  $this->load->view('admin/all_js');
  $this->load->view('admin/pagewise_js/tableTimesheet.js');


}

//function for viewing timesheet data
function timesheetData(){
  $admin_data=$this->session->userdata(base_url().'login_session');
  $admin_id=$admin_data['admin_id'];
  $requestData = $_REQUEST;
  $totalData = $this->Admin_model->getAllTimesheets($admin_id);
  $totalFiltered = $totalData = count($totalData);
  $totalFiltered = $this->Admin_model->getTimesheetsfilteredList($requestData,$admin_id);
 //echo '<pre>';print_r($totalFiltered);exit;
  $totalFiltered = count($totalFiltered);

  $data = array();
  $userlist = $this->Admin_model->getTimesheetsList($requestData,$admin_id);
  // echo '<pre>';print_r($this->db->last_query());exit;
  if (!empty($userlist)) {
    $count=1;
    foreach ($userlist as $key) {
      
      //$view='<a href="'.base_url().'admin/viewJobDescription?id='.$user->j_id.'">'.$user->job_title.'</a>';
      $view1='<ul class="list-unstyled list-inline" style="padding-left: 0px;">';

      
      //$view1.=' <li><a href="'.base_url().'admin/viewJobDescription?id='.$user->j_id.'"><i class="fa fa-eye" aria-hidden="true"></i></a></li>';

      $view1.='<li><a href="'.base_url().'admin/updateTimesheet?id='.$key->id.'"><i class="fa fa-edit" aria-hidden="true"></i></a></li>';
      $view1.='<li><a href="'.base_url().'admin/deleteTimesheet?id='.$key->id.'" onclick="return deleteTimesheet();"><i class="fa fa-trash" aria-hidden="true"></i></a></li>';
      $view1.='<ul/>';
             //preparing an array
      $nestedData = array();
      $nestedData[] = $count++;
      
      //$nestedData[] = $key->event_title;
      $nestedData[] = $key->type;
      $nestedData[] = date('l', strtotime($key->date));
      $nestedData[] = date('jS M Y',strtotime($key->date));
      $nestedData[] = $key->hours.': '.$key->minutes.' '.$key->meridium;
      $nestedData[] = $key->finish_hours.' :'.$key->finish_minutes.' '.$key->finish_meridium;
      $nestedData[] = $key->break_duartion;
      $nestedData[] = $key->pay_rate;
      $nestedData[] = $key->currency;
      $nestedData[] = $key->pay_type;
      $nestedData[] = date('jS M Y',strtotime($key->created_date));
      $nestedData[] = $view1;
      $data[] = $nestedData;

    }
  }

  $json_data = array(
            "draw" => intval($requestData['draw']), // for every request/draw by clientside , they send a number as a parameter, when they recieve a response/data they first check the draw number, so we are sending same number in draw. 
            "recordsTotal" => intval($totalData), // total number of records
            "recordsFiltered" => intval($totalFiltered), // total number of records after searching, if there is no searching then totalFiltered = totalData
            "data" => $data   // total data array
            );
  echo json_encode($json_data);



}

//funcion for updating timesheet
function updateTimesheet(){
   $id=$this->input->get('id');
   //$data['candidate_id']=$id;
  //$data['id']=$id;
  $data['timesheetData']=$this->Admin_model->fetchTimesheetData($id);
  $data['currency']=$this->Admin_model->fetchCurrencies();
  $this->load->view('admin/all_css');
  $this->load->view('admin/dashboard_header');
  $this->load->view('admin/createtimesheet',$data);
  $this->load->view('admin/footer');
  $this->load->view('admin/all_js');
  $this->load->view('admin/pagewise_js/timesheet.js');

}

//function for deleting timesheet
function deleteTimesheet(){
$id=$this->input->get('id');
$result=$this->Admin_model->deleteTimesheet($id);
if($result){
$this->session->set_flashdata('successmsg', '<div class="alert alert-success"><a type="button" class="close" data-dismiss="alert" aria-hidden="true">×</a>Your request has been completed successfully.</div>');
redirect(base_url().'admin/manageTimesheets');
}else{
$this->session->set_flashdata('errormsg', '<div class="alert alert-warning"><a type="button" class="close" data-dismiss="alert" aria-hidden="true">×</a>Your request has not been completed successfully.</div>');
redirect(base_url().'admin/manageTimesheets');
}
}

//function for viewing interview Calender
function viewEventCalender(){
$adminSessionData=$this->session->userdata(base_url().'login_session');
$this->checkSession($adminSessionData);
$admin_id=$adminSessionData['admin_id'];
$data['allClients']=$this->Admin_model->getAllClients();
$data['allCandidates']=$this->Admin_model->fetchCandidates();
$interviews = array();
$allEvents=$this->Admin_model->fetchAllEvents($admin_id);

foreach($allEvents as $key){
   $e = array();
   $e['id'] = $key->id; 
   $e['title'] = $key->event_title; 
   $e['start'] = date('Y-m-d',strtotime($key->date));
   $e['end'] = date('Y-m-d',strtotime($key->end_date));
   $e['color']='#8175c7';
   $e['url']='viewEventInfo?id='.$key->id.'';
   //$allday = ($key->allDay == "true") ? true : false;
   //$e['allDay'] = $allday;
   array_push($interviews, $e);
  }
  $data['interviews']=$interviews;
  //echo'<pre>';print_r($data['interviews']);exit;
$this->load->view('admin/all_css');
$this->load->view('admin/dashboard_header');
$this->load->view('admin/interviewCalender',$data);
$this->load->view('admin/footer');
$this->load->view('admin/all_js',$data);
$this->load->view('admin/pagewise_js/interviewCalender.js');


}

//function for viewing job calender
function viewJobCalender(){
$adminSessionData=$this->session->userdata(base_url().'login_session');
$this->checkSession($adminSessionData);
$admin_id=$adminSessionData['admin_id'];

$interviews = array();
$allJobs=$this->Admin_model->get_mainJob_list();

foreach($allJobs as $key){
 
   $e = array();
   $e['id'] = $key->j_id; 
   $e['title'] = $key->job_title; 
   $e['start'] = date('Y-m-d',strtotime($key->created_date));
   $e['end'] = date('Y-m-d',strtotime($key->open_date));
   $e['color']='#aa66cc';
   $e['url']='viewJobDescription?id='.$key->j_id.'';
   //$allday = ($key->allDay == "true") ? true : false;
   //$e['allDay'] = $allday;
   array_push($interviews, $e);
  }
  $data['interviews']=$interviews;
  //echo'<pre>';print_r($data['interviews']);exit;
$this->load->view('admin/all_css');
$this->load->view('admin/dashboard_header');
$this->load->view('admin/interviewCalender',$data);
$this->load->view('admin/footer');
$this->load->view('admin/all_js',$data);
$this->load->view('admin/pagewise_js/interviewCalender.js');


}

//function for viewing group interview form
function createGroupInterview(){
  $adminSessionData=$this->session->userdata(base_url().'login_session');
  $this->checkSession($adminSessionData);
  $this->load->view('admin/all_css');
  $this->load->view('admin/dashboard_header');
  $this->load->view('admin/groupinterviewForm');
  $this->load->view('admin/footer');
  $this->load->view('admin/all_js');
  $this->load->view('admin/pagewise_js/groupinterview.js');


}
//function for candidate listing for group interview
function viewCandidatesForGroupInterview(){
  $userdata =$this->session->userdata('advance_filter');
  $candidate_first_name=$userdata['first_name'];
  
  $city=$userdata['city'];
  $candidate_last_name=$userdata['candidate_last_name'];
  $email=$userdata['email'];
  
  // echo '<pre>';print_r();exit;

  $employmentCount=$this->Admin_model->getEmploymentCount();
  $checkPreEmplyment=$employmentCount->empcount;

  $educationCount=$this->Admin_model->getEducationCount();
  $checkEducation=$educationCount->educationcount;

  $admin_data=$this->session->userdata(base_url().'login_session');

  $requestData = $_REQUEST;
 //print_r($requestData);exit;
  $totalData = $this->Admin_model->getAllCandidatesList();
  $totalFiltered = $totalData = count($totalData);
  $totalFiltered = $this->Admin_model->getCandidatesfilteredList($requestData,$candidate_first_name,$candidate_last_name,$email,$city);
 // echo '<pre>';print_r($totalFiltered);exit;
  $totalFiltered = count($totalFiltered);

  $data = array();
  $userlist = $this->Admin_model->getCandidatesList($requestData,$candidate_first_name,$candidate_last_name,$email,$city);
 //echo '<pre>';print_r($this->db->last_query());exit;
  $this->session->unset_userdata('advance_filter');


  //echo '<pre>';print_r($userlist);exit;
  if (!empty($userlist)) {
    $count=1;
    foreach ($userlist as $user) {
             // preparing an array
      $nestedData = array();

// if($user->profile == '1' && $user->education =='1' && $user->pre_employment=='1' && $user->employment=='1' && $user->resume=='1' && $user->references=='1' && $user->additional_info=='1' && $user->miscellaneous=='1' && $user->social_media=='1'){

// $status='<div class="progress"><div class="progress-bar progress-bar-success progress-bar-striped" role="progressbar" aria-valuenow="40" aria-valuemin="0" aria-valuemax="100" style="width:100%">100% Complete</div></div>';

// }else if($user->profile == '1' && $user->education =='1' && $user->pre_employment=='1' && $user->employment=='1' && $user->resume=='1' && $user->references=='1' && $user->additional_info=='1' && $user->miscellaneous=='0' && $user->social_media=='0'){


//  $status='<div class="progress"><div class="progress-bar progress-bar-warning progress-bar-striped" role="progressbar" aria-valuenow="60" aria-valuemin="0" aria-valuemax="100" style="width:85%">85% Complete</div></div>'; 


// }else if($user->profile == '1' && $user->education =='1' && $user->pre_employment=='1' && $user->employment=='0' && $user->resume=='0' && $user->references=='0' && $user->additional_info=='0' && $user->miscellaneous=='0' && $user->social_media=='0'){

// $status=' <div class="progress"><div class="progress-bar progress-bar-danger progress-bar-striped" role="progressbar" aria-valuenow="70" aria-valuemin="0" aria-valuemax="100" style="width:50%">20% Complete</div></div>';

// }else{

// $status='<div class="progress"><div class="progress-bar progress-bar-danger progress-bar-striped" role="progressbar" aria-valuenow="70" aria-valuemin="0" aria-valuemax="100" style="width:100%">0%</div>';
// }
      $progress = $user->profile+$user->education+$user->pre_employment+$user->employment+$user->resume+$user->references+$user->additional_info+$user->miscellaneous+$user->social_media;

      if($progress == 1){

       $progressPercentage = '<div class="progress"><div class="progress-bar progress-bar-danger progress-bar-striped" role="progressbar" aria-valuenow="60" aria-valuemin="0" aria-valuemax="100" style="width:12%">12% Complete</div></div>';
     }

     if($progress == 2 ){

       if($user->education==1 && $checkEducation >0)
       {
        $progressPercentage = '<div class="progress"><div class="progress-bar progress-bar-danger progress-bar-striped" role="progressbar" aria-valuenow="60" aria-valuemin="0" aria-valuemax="100" style="width:24%">24% Complete</div></div>';
      }
      else{

       $progressPercentage = '<div class="progress"><div class="progress-bar progress-bar-danger progress-bar-striped" role="progressbar" aria-valuenow="60" aria-valuemin="0" aria-valuemax="100" style="width:12%">12% Complete</div></div>';
     }


   }

   if($progress == 3){

    if ($user->pre_employment == 1 && $checkPreEmplyment > 0) {

      $progressPercentage ='<div class="progress"><div class="progress-bar progress-bar-warning progress-bar-striped" role="progressbar" aria-valuenow="60" aria-valuemin="0" aria-valuemax="100" style="width:36%">36% Complete</div></div>' ;
    }

    else{

      $progressPercentage = '<div class="progress"><div class="progress-bar progress-bar-warning progress-bar-striped" role="progressbar" aria-valuenow="60" aria-valuemin="0" aria-valuemax="100" style="width:24%">24% Complete</div></div>';
    }
  }

  if($progress == 4){


    if ($user->pre_employment == 1 && $checkPreEmplyment > 0 && $user->education==1 && $checkEducation >0 ) {

      $progressPercentage = '<div class="progress"><div class="progress-bar progress-bar-warning progress-bar-striped" role="progressbar" aria-valuenow="60" aria-valuemin="0" aria-valuemax="100" style="width:48%">48% Complete</div></div>';
    }


    else{

      $progressPercentage = '<div class="progress"><div class="progress-bar progress-bar-warning progress-bar-striped" role="progressbar" aria-valuenow="60" aria-valuemin="0" aria-valuemax="100" style="width:36%">36% Complete</div></div>';
    }


  }

  if($progress == 5){


    if ($user->pre_employment == 1 && $checkPreEmplyment > 0 && $user->education==1 && $checkEducation >0 ) {

      $progressPercentage = '<div class="progress"><div class="progress-bar progress-bar-warning progress-bar-striped" role="progressbar" aria-valuenow="60" aria-valuemin="0" aria-valuemax="100" style="width:60%">60% Complete</div></div>';
    }


    else{

      $progressPercentage = '<div class="progress"><div class="progress-bar progress-bar-warning progress-bar-striped" role="progressbar" aria-valuenow="60" aria-valuemin="0" aria-valuemax="100" style="width:48%">48% Complete</div></div>';
    }

  }
  if($progress == 6){


    if ($user->pre_employment == 1 && $checkPreEmplyment > 0 && $user->education==1 && $checkEducation >0 ) {

      $progressPercentage = '<div class="progress"><div class="progress-bar progress-bar-warning progress-bar-striped" role="progressbar" aria-valuenow="60" aria-valuemin="0" aria-valuemax="100" style="width:70%">70% Complete</div></div>';
    }


    else{

      $progressPercentage = '<div class="progress"><div class="progress-bar progress-bar-warning progress-bar-striped" role="progressbar" aria-valuenow="60" aria-valuemin="0" aria-valuemax="100" style="width:60%">60% Complete</div></div>';
    }

  }


  if($progress == 7){

   if ($user->pre_employment == 1 && $checkPreEmplyment > 0 && $user->education==1 && $checkEducation >0 ) {

    $progressPercentage = '<div class="progress"><div class="progress-bar progress-bar-warning progress-bar-striped" role="progressbar" aria-valuenow="60" aria-valuemin="0" aria-valuemax="100" style="width:80%">80% Complete</div></div>';
  }


  else{

    $progressPercentage = '<div class="progress"><div class="progress-bar progress-bar-warning progress-bar-striped" role="progressbar" aria-valuenow="60" aria-valuemin="0" aria-valuemax="100" style="width:70%">70% Complete</div></div>';
  }

}

if($progress == 8){


 if ($user->pre_employment == 1 && $checkPreEmplyment > 0 && $user->education==1 && $checkEducation >0) {

  $progressPercentage = '<div class="progress"><div class="progress-bar progress-bar-warning progress-bar-striped" role="progressbar" aria-valuenow="60" aria-valuemin="0" aria-valuemax="100" style="width:90%">90% Complete</div></div>';
}


else{

  $progressPercentage = '<div class="progress"><div class="progress-bar progress-bar-warning progress-bar-striped" role="progressbar" aria-valuenow="60" aria-valuemin="0" aria-valuemax="100" style="width:80%">80% Complete</div></div>';
}

}

if($progress == 9){


 if ($user->pre_employment == 1 && $checkPreEmplyment > 0 && $user->education==1 && $checkEducation >0) {

  $progressPercentage = '<div class="progress"><div class="progress-bar progress-bar-success progress-bar-striped" role="progressbar" aria-valuenow="60" aria-valuemin="0" aria-valuemax="100" style="width:100%">100% Complete</div></div>';
}

else{

  $progressPercentage = '<div class="progress"><div class="progress-bar progress-bar-success progress-bar-striped" role="progressbar" aria-valuenow="60" aria-valuemin="0" aria-valuemax="100" style="width:90%">90% Complete</div></div>';
}

}


$deactivate=$user->deactivate;
$view='<a href="'.base_url().'admin/viewCandidateProfile?id='.$user->u_id.'">'.$user->first_name.' '.$user->last_name.'</a>';
$view2='<input type="checkbox" name="cand_check" value='.$user->u_id.' id="cand_value">';
$view1='<ul class="list-unstyled list-inline" style="padding-left: 0px;">';
if($deactivate == '0')
{
 $view1.='<li onclick="return deactivateUser();"><a href="'.base_url().'admin/disableUser?id='.$user->u_id.'"><i class="fa fa-check-square-o" aria-hidden="true"></i></a></li>';

}else{

  $view1.='<li onclick="return activateUser();"><a href="'.base_url().'admin/EnableUser?id='.$user->u_id.'"><i class="fa fa-minus-circle" aria-hidden="true"></i></a></li>';
}

$view1.='<li><a href="'.base_url().'admin/candidateAppliedJobs?id='.$user->u_id.'"><i class="fa fa-paper-plane-o" aria-hidden="true" title="Applied Jobs"></i>
<a></li>';

$view1.='<li><a href="'.base_url().'admin/createTimesheets?id='.$user->u_id.'"><i class="fa fa-clock-o" aria-hidden="true" title="Create Timesheet"></i>
<a></li>';

$view1.='</ul>';


$nestedData[] = $view2; 
$nestedData[] = $count++;
$nestedData[] = $view;

$nestedData[] = $user->user_city;
$nestedData[] = $user->user_email;
$nestedData[] = $progressPercentage;
//$nestedData[] = $user->job_title;
$nestedData[] = $view1;
$data[] = $nestedData;
}
}

// echo "<pre>";print_r($_REQUEST);exit;
$json_data = array(
            "draw" => isset($requestData['draw']) ? intval($requestData['draw']) : '', // for every request/draw by clientside , they send a number as a parameter, when they recieve a response/data they first check the draw number, so we are sending same number in draw. 
            "recordsTotal" => intval($totalData), // total number of records
            "recordsFiltered" => intval($totalFiltered), // total number of records after searching, if there is no searching then totalFiltered = totalData
            "data" => $data   // total data array
            );
echo json_encode($json_data);



}


//function for viewing group interviews
function viewGroupInterview(){
  $adminSessionData=$this->session->userdata(base_url().'login_session');
  $this->checkSession($adminSessionData);
  $this->load->view('admin/all_css');
  $this->load->view('admin/dashboard_header');
  $this->load->view('admin/viewGroupInterview');
  $this->load->view('admin/footer');
  $this->load->view('admin/all_js');
  $this->load->view('admin/pagewise_js/viewgroupinterview.js');


}

//function for viewing group interview table
function groupInterviewData(){
  $admin_data=$this->session->userdata(base_url().'login_session');
  $admin_id=$admin_data['admin_id'];
  $requestData = $_REQUEST;
  $totalData = $this->Admin_model->getAllGroupInterviews($admin_id);
  $totalFiltered = $totalData = count($totalData);
  $totalFiltered = $this->Admin_model->getGroupInterviewsfilteredList($requestData,$admin_id);
 //echo '<pre>';print_r($totalFiltered);exit;
  $totalFiltered = count($totalFiltered);

  $data = array();
  $userlist = $this->Admin_model->getGroupInterviewsList($requestData,$admin_id);
  // echo '<pre>';print_r($this->db->last_query());exit;
  if (!empty($userlist)) {
    $count=1;
    foreach ($userlist as $key) {

      $view='<a href="'.base_url().'admin/viewGroupCandidates?id='.$key->id.'">'.$key->group_name.'</a>';
      
      //$view='<a href="'.base_url().'admin/viewJobDescription?id='.$user->j_id.'">'.$user->job_title.'</a>';
      $view1='<ul class="list-unstyled list-inline" style="padding-left: 0px;">';

      
      //$view1.=' <li><a href="'.base_url().'admin/viewJobDescription?id='.$user->j_id.'"><i class="fa fa-eye" aria-hidden="true"></i></a></li>';

       $view1.='<li><a href="'.base_url().'admin/updateGroup/'.$key->id.'"><i class="fa fa-edit" aria-hidden="true"></i></a></li>';
       $view1.='<li><a href="'.base_url().'admin/deleteGroup/'.$key->id.'" onclick="return deleteGroup();"><i class="fa fa-trash" aria-hidden="true"></i></a></li>';
       $view1.='<ul/>';
             //preparing an array
      $nestedData = array();
      $nestedData[] = $count++;
      
      //$nestedData[] = $key->event_title;
      $nestedData[] = $view;
      $nestedData[] = $key->job_title;
      $nestedData[] = $key->clnt_first_name.' '.$key->clnt_last_name;
      
      $nestedData[] = date('jS M Y',strtotime($key->created_date));
      $nestedData[] = $view1;
      $data[] = $nestedData;

    }
  }

  $json_data = array(
            "draw" => intval($requestData['draw']), // for every request/draw by clientside , they send a number as a parameter, when they recieve a response/data they first check the draw number, so we are sending same number in draw. 
            "recordsTotal" => intval($totalData), // total number of records
            "recordsFiltered" => intval($totalFiltered), // total number of records after searching, if there is no searching then totalFiltered = totalData
            "data" => $data   // total data array
            );
  echo json_encode($json_data);


}

//function for fetching group interview candidate data
function groupInterviewCandidateData(){
 $admin_data=$this->session->userdata(base_url().'login_session');
 $admin_id=$admin_data['admin_id'];
 $group_id=$this->input->post('group_id');
 $getCandidates=$this->Admin_model->fetchGroupCandidates($group_id);
 $candidates=$getCandidates->candidates;
 $requestData = $_REQUEST;
 $totalData = $this->Admin_model->getAllGroupInterviewCandidates($candidates);
 $totalFiltered = $totalData = count($totalData);
 $totalFiltered = $this->Admin_model->getGroupInterviewCandidatesfilteredList($requestData,$candidates);
 //echo '<pre>';print_r($totalFiltered);exit;
 $totalFiltered = count($totalFiltered);

 $data = array();
 $userlist = $this->Admin_model->getGroupInterviewCandidatesList($requestData,$candidates);
  // echo '<pre>';print_r($this->db->last_query());exit;
 if (!empty($userlist)) {
  $count=1;
  foreach ($userlist as $key) {

      //$view='<a href="'.base_url().'admin/viewGroupCandidates?id='.$key->id.'">'.$key->group_name.'</a>';

      //$view='<a href="'.base_url().'admin/viewJobDescription?id='.$user->j_id.'">'.$user->job_title.'</a>';
      //$view1='<ul class="list-unstyled list-inline" style="padding-left: 0px;">';


      //$view1.=' <li><a href="'.base_url().'admin/viewJobDescription?id='.$user->j_id.'"><i class="fa fa-eye" aria-hidden="true"></i></a></li>';

      // $view1.='<li><a href="'.base_url().'admin/updateTimesheet?id='.$key->id.'"><i class="fa fa-edit" aria-hidden="true"></i></a></li>';
      // $view1.='<li><a href="'.base_url().'admin/deleteTimesheet?id='.$key->id.'" onclick="return deleteTimesheet();"><i class="fa fa-trash" aria-hidden="true"></i></a></li>';
      // $view1.='<ul/>';


    //$candidates_name=$key->candidates;
                           // echo '<pre>'; print_r($key);exit;
   
   // echo "<pre>";print_r($candidateName);exit;
             //preparing an array
   $nestedData = array();
   $nestedData[] = $count++;

   $nestedData[] = $key->first_name.' '.$key->last_name;
   $nestedData[] = $key->user_city;
   $nestedData[] = $key->user_email;
   //$nestedData[] = $candidateCity;

   $data[] = $nestedData;
// echo "<pre>";print_r($data);exit;
 }
}

$json_data = array(
            "draw" => intval($requestData['draw']), // for every request/draw by clientside , they send a number as a parameter, when they recieve a response/data they first check the draw number, so we are sending same number in draw. 
            "recordsTotal" => intval($totalData), // total number of records
            "recordsFiltered" => intval($totalFiltered), // total number of records after searching, if there is no searching then totalFiltered = totalData
            "data" => $data   // total data array
            );
echo json_encode($json_data);




}


//function for viewing group candidates
function viewGroupCandidates(){
  $id=$this->input->get('id');
  $data['group_id']=$id;
  $data['showCandidates']=$this->Admin_model->get_candidates();
  //echo '<pre>';print_r($data['showCandidates']);exit;
  $this->load->view('admin/all_css');
  $this->load->view('admin/dashboard_header');
  $this->load->view('admin/viewGroupCandidates',$data);
  $this->load->view('admin/footer');
  $this->load->view('admin/all_js');
  $this->load->view('admin/pagewise_js/groupCandidates.js',$data);


}

//function for inserting group interview
function insertGroupInterview(){
  $admin_data=$this->session->userdata(base_url().'login_session');
  $admin_id=$admin_data['admin_id'];
  $_POST = json_decode(file_get_contents('php://input'), true);
//echo '<pre>';print_r($_POST);
  $this->form_validation->set_rules('group_name', 'Group Name', 'trim|required|trim|xss_clean|strip_tags');
  // $this->form_validation->set_rules('blog_title', 'Admin Name', 'trim|required|trim|xss_clean|strip_tags');
  // $this->form_validation->set_rules('blog_desc', 'Admin Name', 'trim|required|trim|xss_clean|strip_tags');
//$this->form_validation->set_rules('name', 'Admin Name', 'trim|required|trim|xss_clean|strip_tags');

  if ($this->form_validation->run() == TRUE) {

   $candidates=$this->input->post('test');
   $candidates = implode(',', $candidates);
   // echo "<pre>";print_r($candidates);exit;
   
   $insert_group_interview['admin_id']=$admin_id;
   $insert_group_interview['group_name']=$this->input->post('group_name');
   $insert_group_interview['job_title']=$this->input->post('job_title');
   $insert_group_interview['location']=$this->input->post('location');
   $insert_group_interview['interview_date']=date('Y-m-d',strtotime($this->input->post('date')));
   $insert_group_interview['candidates']=($candidates);
   $client_name=$this->input->post('client_name');
   $insert_group_interview['client_name']=$this->input->post('client_name');
   $id=$this->input->post('group_id');
   $checkGroupData=$this->Admin_model->fetchGroupData($id);
   //echo $group_id;exit;
   if($candidates!=''){
 
    $insert_group_interview['candidates']=$candidates;
   }else{

   $insert_group_interview['candidates']=$checkGroupData->candidates;
   }

   if($client_name!=''){
   $insert_group_interview['client_name']=$this->input->post('client_name');

   }else{
   $insert_group_interview['client_name']=$checkGroupData->client_name;

   }


    //echo '<pre>';print_r($insert_group_interview);exit;
   if(!empty($checkGroupData)){
  $result=$this->Admin_model->updateGroupInterviewDetails($insert_group_interview,$id);

   }else{
  $result=$this->Admin_model->insertGroupInterviewDetails($insert_group_interview);


   }

   //echo '<pre>';print_r($this->db->last_query()); 
 //exit;

   if($result){
    $data = array('status' => '1');
    $this->session->set_flashdata('successmsg', '<div class="alert alert-success"><a type="button" class="close" data-dismiss="alert" aria-hidden="true">×</a>Your request has been completed successfully.</div>');

  }else{
    $data = array('status' => '0');
    $this->session->set_flashdata('errormsg', '<div class="alert alert-warning alert-dismissable"><a type="button" class="close" data-dismiss="alert" aria-hidden="true">×</a>Error while submitting.</div>');
  }
}else{
  $data = array('status' => '2', 'error' => $this->form_validation->get_field_data(), 'postdata' => $this->input->post()); 

}


print_r(json_encode($data));
}

//function for deleting a group
function deleteGroup($id){
$result=$this->Admin_model->deleteGroupById($id);
if($result){
    $this->session->set_flashdata('successmsg', '<div class="alert alert-success"><a type="button" class="close" data-dismiss="alert" aria-hidden="true">×</a>Your request has been completed successfully.</div>');
    redirect(base_url().'admin/viewGroupInterview');
  }else{
    $this->session->set_flashdata('errormsg', '<div class="alert alert-warning"><a type="button" class="close" data-dismiss="alert" aria-hidden="true">×</a>Your request has not been completed successfully.</div>');
    redirect(base_url().'admin/viewGroupInterview');
  }

}

//function for updating group information 
function updateGroup($id){
  $adminSessionData=$this->session->userdata(base_url().'login_session');
  $this->checkSession($adminSessionData);
  $data['groupData']=$this->Admin_model->fetchGroupData($id);
  $this->load->view('admin/all_css');
  $this->load->view('admin/dashboard_header');
  $this->load->view('admin/groupinterviewForm',$data);
  $this->load->view('admin/footer');
  $this->load->view('admin/all_js');
  $this->load->view('admin/pagewise_js/groupinterview.js',$data);


}
//function for displaying homeless data
function viewHomeless(){
  $this->load->view('admin/all_css');
  $this->load->view('admin/dashboard_header');
  $this->load->view('admin/viewHomlessUsers');
  $this->load->view('admin/footer');
  $this->load->view('admin/all_js');
  $this->load->view('admin/pagewise_js/homelessUsers.js');


}

//function for displaying substance data
function viewSubstance(){
  $this->load->view('admin/all_css');
  $this->load->view('admin/dashboard_header');
  $this->load->view('admin/viewSubstanceUsers');
  $this->load->view('admin/footer');
  $this->load->view('admin/all_js');
  $this->load->view('admin/pagewise_js/substanceUsers.js');

}

//function for viewing homeless data
function homelessData(){
 $admin_data=$this->session->userdata(base_url().'login_session');
 $admin_id=$admin_data['admin_id'];


 
 $requestData = $_REQUEST;
 $totalData = $this->Admin_model->getAllHomelessCandidates();
 $totalFiltered = $totalData = count($totalData);
 $totalFiltered = $this->Admin_model->getHomelessCandidatesfilteredList($requestData);
 //echo '<pre>';print_r($totalFiltered);exit;
 $totalFiltered = count($totalFiltered);

 $data = array();
 $userlist = $this->Admin_model->getHomelessCandidatesList($requestData);
  // echo '<pre>';print_r($this->db->last_query());exit;
 if (!empty($userlist)) {
  $count=1;
  foreach ($userlist as $key) {

      //$view='<a href="'.base_url().'admin/viewGroupCandidates?id='.$key->id.'">'.$key->group_name.'</a>';

      //$view='<a href="'.base_url().'admin/viewJobDescription?id='.$user->j_id.'">'.$user->job_title.'</a>';
    $view1='<ul class="list-unstyled list-inline" style="padding-left: 0px;">';


      $view1.=' <li><a href="'.base_url().'admin/updateHomeless/'.$key->id.'"><i class="fa fa-edit" aria-hidden="true"></i></a></li>';

      // $view1.='<li><a href="'.base_url().'admin/updateTimesheet?id='.$key->id.'"><i class="fa fa-edit" aria-hidden="true"></i></a></li>';
       $view1.='<li><a href="'.base_url().'admin/deleteHomeless/'.$key->id.'" onclick="return deleteHomeless();"><i class="fa fa-trash" aria-hidden="true"></i></a></li>';
       $view1.='<ul/>';


    //$candidates_name=$key->candidates;
                           // echo '<pre>'; print_r($key);exit;
   
   // echo "<pre>";print_r($candidateName);exit;
             //preparing an array
   $nestedData = array();
   $nestedData[] = $count++;
   $nestedData[] = date('jS M Y',strtotime($key->start_date));
   $nestedData[] = date('jS M Y',strtotime($key->end_date));
   $nestedData[] = $key->first_name.' '.$key->last_name;
   $nestedData[] = $key->country_name;
   $nestedData[] = $key->city;
   $nestedData[] = $key->story;
   $nestedData[] = $key->help;
   $nestedData[] = $key->rate;
   $nestedData[] = $key->info;
   $nestedData[] = $view1;
   $data[] = $nestedData;
// echo "<pre>";print_r($data);exit;
 }
}

$json_data = array(
            "draw" => intval($requestData['draw']), // for every request/draw by clientside , they send a number as a parameter, when they recieve a response/data they first check the draw number, so we are sending same number in draw. 
            "recordsTotal" => intval($totalData), // total number of records
            "recordsFiltered" => intval($totalFiltered), // total number of records after searching, if there is no searching then totalFiltered = totalData
            "data" => $data   // total data array
            );
echo json_encode($json_data);



}

//function for viewing substance data
function substanceData(){
 $admin_data=$this->session->userdata(base_url().'login_session');
 $admin_id=$admin_data['admin_id'];


 
 $requestData = $_REQUEST;
 $totalData = $this->Admin_model->getAllSubstanceCandidates();
 $totalFiltered = $totalData = count($totalData);
 $totalFiltered = $this->Admin_model->getSubstanceCandidatesfilteredList($requestData);
 //echo '<pre>';print_r($totalFiltered);exit;
 $totalFiltered = count($totalFiltered);

 $data = array();
 $userlist = $this->Admin_model->getSubstanceCandidatesList($requestData);
  // echo '<pre>';print_r($this->db->last_query());exit;
 if (!empty($userlist)) {
  $count=1;
  foreach ($userlist as $key) {

      //$view='<a href="'.base_url().'admin/viewGroupCandidates?id='.$key->id.'">'.$key->group_name.'</a>';

      //$view='<a href="'.base_url().'admin/viewJobDescription?id='.$user->j_id.'">'.$user->job_title.'</a>';
    $view1='<ul class="list-unstyled list-inline" style="padding-left: 0px;">';


      $view1.=' <li><a href="'.base_url().'admin/updateSubstance/'.$key->id.'"><i class="fa fa-edit" aria-hidden="true"></i></a></li>';

      // $view1.='<li><a href="'.base_url().'admin/updateTimesheet?id='.$key->id.'"><i class="fa fa-edit" aria-hidden="true"></i></a></li>';
       $view1.='<li><a href="'.base_url().'admin/deleteSubstance/'.$key->id.'" onclick="return deleteSubstance();"><i class="fa fa-trash" aria-hidden="true"></i></a></li>';
       $view1.='<ul/>';


    //$candidates_name=$key->candidates;
                           // echo '<pre>'; print_r($key);exit;
   
   // echo "<pre>";print_r($candidateName);exit;
             //preparing an array
   $nestedData = array();
   $nestedData[] = $count++;
   
   $nestedData[] = $key->first_name.' '.$key->last_name;
   $nestedData[] = $key->substance_name;
   $nestedData[] = date('jS M Y',strtotime($key->start_date));
   $nestedData[] = date('jS M Y',strtotime($key->until_date));
   $nestedData[] = $key->frequency;
   $nestedData[] = $key->frequency_use_now;
   $nestedData[] = $key->reasons;
   $nestedData[] = $key->significant_info;
 
   $nestedData[] = $view1;
   $data[] = $nestedData;
// echo "<pre>";print_r($data);exit;
 }
}

$json_data = array(
            "draw" => intval($requestData['draw']), // for every request/draw by clientside , they send a number as a parameter, when they recieve a response/data they first check the draw number, so we are sending same number in draw. 
            "recordsTotal" => intval($totalData), // total number of records
            "recordsFiltered" => intval($totalFiltered), // total number of records after searching, if there is no searching then totalFiltered = totalData
            "data" => $data   // total data array
            );
echo json_encode($json_data);


}

//function for deleting homeless
function deleteHomeless($id){
// echo $id;exit;
  $result=$this->Admin_model->deleteHomeless($id);
  if($result){
    $this->session->set_flashdata('successmsg', '<div class="alert alert-success"><a type="button" class="close" data-dismiss="alert" aria-hidden="true">×</a>Your request has been completed successfully.</div>');
    redirect(base_url().'admin/viewHomeless');
  }else{
    $this->session->set_flashdata('errormsg', '<div class="alert alert-warning"><a type="button" class="close" data-dismiss="alert" aria-hidden="true">×</a>Your request has not been completed successfully.</div>');
    redirect(base_url().'admin/viewHomeless');
  }


}
//function for deleting substance
function deleteSubstance($id){
  $result=$this->Admin_model->deleteSubstance($id);
  if($result){
    $this->session->set_flashdata('successmsg', '<div class="alert alert-success"><a type="button" class="close" data-dismiss="alert" aria-hidden="true">×</a>Your request has been completed successfully.</div>');
    redirect(base_url().'admin/viewSubstance');
  }else{
    $this->session->set_flashdata('errormsg', '<div class="alert alert-warning"><a type="button" class="close" data-dismiss="alert" aria-hidden="true">×</a>Your request has not been completed successfully.</div>');
    redirect(base_url().'admin/viewSubstance');
  }

}


//function for dispalying updating homeless view.
function updateHomeless($id){
  $data['getHomelessData']=$this->Admin_model->fetchHomelessdata($id);
  $data['allCountries']=$this->Admin_model->getCountries();
  $this->load->view('admin/all_css');
  $this->load->view('admin/dashboard_header');
  $this->load->view('admin/updateHomlessUsers',$data);
  $this->load->view('admin/footer');
  $this->load->view('admin/all_js');
  $this->load->view('admin/pagewise_js/updatehomeless.js',$data);


}

//function for displaying updating substance view
function updateSubstance($id){
  $data['getSubstanceData']=$this->Admin_model->fetchSubstancedata($id);
  //echo '<pre>';print_r($data['getSubstanceData']);exit;
  $this->load->view('admin/all_css');
  $this->load->view('admin/dashboard_header');
  $this->load->view('admin/updateSubstanceUsers',$data);
  $this->load->view('admin/footer');
  $this->load->view('admin/all_js');
  $this->load->view('admin/pagewise_js/updateSubstance.js',$data);

}

//function for updating homeless data
function updateHomelessData(){
 $_POST = json_decode(file_get_contents('php://input'),true);
 //echo "<pre>"; print_r($_POST); exit();
  $user_id = $this->session->userdata('user_id');

  if(isset($user_id) && $user_id!=''){

    $user_id=$user_id;

//echo $user_id;exit;
    $role="user";
  }else{

    $user_id='';
    $role="guest";
  }
  if ($this->input->post()) {
    // $this->form_validation->set_rules('first_name', 'FirstName', 'trim|required|trim|xss_clean|strip_tags');
    // $this->form_validation->set_rules('last_name', 'LastName', 'trim|required|trim|xss_clean|strip_tags');
    // $this->form_validation->set_rules('telephone', 'Telephone', 'trim|required|trim|xss_clean|strip_tags');
    // $this->form_validation->set_rules('email', 'Email', 'trim|required|trim|xss_clean|strip_tags');
    $this->form_validation->set_rules('country', 'Country', 'trim|required|trim|xss_clean|strip_tags');
    $this->form_validation->set_rules('city', 'City', 'trim|required|trim|xss_clean|strip_tags');
    $this->form_validation->set_rules('story', 'Story', 'trim|required|trim|xss_clean|strip_tags');
    $this->form_validation->set_rules('help', 'Help', 'trim|required|trim|xss_clean|strip_tags');
    $this->form_validation->set_rules('info', 'Significant Information', 'trim|required|trim|xss_clean|strip_tags');
    if ($this->form_validation->run() == TRUE) {
      // $insertHomelessData['user_id']=$user_id;
      // $insertHomelessData['first_name']=$this->input->post('first_name');
      // $insertHomelessData['last_name']=$this->input->post('last_name');
      // $insertHomelessData['telephone']=$this->input->post('telephone');
      // $insertHomelessData['email']=$this->input->post('email');
      $insertHomelessData['start_date']=date('Y-m-d',strtotime($this->input->post('date')));
      $insertHomelessData['end_date']=date('Y-m-d',strtotime($this->input->post('end_date')));
      $insertHomelessData['country']=$this->input->post('country');
      $insertHomelessData['city']=$this->input->post('city');
      $insertHomelessData['story']=$this->input->post('story');
      $insertHomelessData['help']=$this->input->post('help');
      $rate=$this->input->post('rate');
      $insertHomelessData['info']=$this->input->post('info');
      $insertHomelessData['role']=$role;
      $id=$this->input->post('homeless_id');
//echo '<pre>';print_r($homeless_id);exit;


        $checkHomelessData=$this->Admin_model->fetchHomelessdata($id);
         if($rate!=''){

        $insertHomelessData['rate']=$rate;
      }else{

        $insertHomelessData['rate']=$checkHomelessData->rate;
      }
     


      if(!empty($checkHomelessData)){
        $result=$this->Admin_model->updateHomelessData($insertHomelessData,$id);

      }else{

        
      }

      if($result){

       $data = array('status' => '1');
       $this->session->set_flashdata('successmsg', '<div class="alert alert-success"><a type="button" class="close" data-dismiss="alert" aria-hidden="true">×</a>Your request has been completed successfully.</div>');

     }else{

       $data = array('status' => '0');
       $this->session->set_flashdata('errormsg', '<div class="alert alert-warning alert-dismissable"><a type="button" class="close" data-dismiss="alert" aria-hidden="true">×</a>Error while submitting.</div>');
     }
   }else{

    $data = array('status' => '2', 'error' => $this->form_validation->get_field_data(), 'postdata' => $this->input->post());  
  }
}else{

  $data = array('status' => '3');
  $this->session->set_flashdata('errormsg', '<div class="alert alert-warning alert-dismissable"><a type="button" class="close" data-dismiss="alert" aria-hidden="true">×</a>All fields are mandatory.</div>');
}

print_r(json_encode($data));


}

//function for updating substance data
function updateSubstanceData(){
$_POST = json_decode(file_get_contents('php://input'),true);
  $user_id = $this->session->userdata('user_id');

  if(isset($user_id) && $user_id!=''){

    $user_id=$user_id;

    $role="user";
  }else{

    $user_id='';
    $role="guest";
  }
  if ($this->input->post()) {
    
    $this->form_validation->set_rules('drug_name', 'Substance Name', 'trim|required|trim|xss_clean|strip_tags'); 
    //$this->form_validation->set_rules('frequency', 'Frequency of use', 'trim|required|trim|xss_clean|strip_tags'); 
    //$this->form_validation->set_rules('frequency_use_now', 'Frequency of use now', 'trim|required|trim|xss_clean|strip_tags');
    $this->form_validation->set_rules('reasons', 'Reasons for use', 'trim|required|trim|xss_clean|strip_tags');
    $this->form_validation->set_rules('significant_info', 'Any other significant information', 'trim|required|trim|xss_clean|strip_tags');  
    if ($this->form_validation->run() == TRUE) {
      
      
      $insertSubstanceData['substance_name']=$this->input->post('drug_name');
      $insertSubstanceData['start_date']=date('Y-m-d',strtotime($this->input->post('start_date')));
      $insertSubstanceData['until_date']=date('Y-m-d',strtotime($this->input->post('until_date')));
      $insertSubstanceData['frequency']=$this->input->post('optradio');
      
      $insertSubstanceData['frequency_use_now']=$this->input->post('optradio1');
      $insertSubstanceData['reasons']=$this->input->post('reasons');
      $insertSubstanceData['significant_info']=$this->input->post('significant_info');
      $insertSubstanceData['role']=$role;

   // echo '<pre>';print_r($insertSubstanceData);exit;
      
        $id=$this->input->post('substance_id');
        //echo $id;exit;
        $checkSubstanceData=$this->Admin_model->fetchSubstanceData($id);
        if($user_id!=''){
       $insertSubstanceData['user_id']=$user_id;

      }else{

       $insertSubstanceData['user_id']=$checkSubstanceData->user_id;

      }
     
      //echo '<pre>';print_r($insertSubstanceData);exit;
      if(!empty($checkSubstanceData)){
        $result=$this->Admin_model->updateSubstanceData($insertSubstanceData,$id);
      }else{

        
      }


      if($result){

       $data = array('status' => '1');
       $this->session->set_flashdata('successmsg', '<div class="alert alert-success"><a type="button" class="close" data-dismiss="alert" aria-hidden="true">×</a>Your request has been completed successfully.</div>');

     }else{

       $data = array('status' => '0');
       $this->session->set_flashdata('errormsg', '<div class="alert alert-warning alert-dismissable"><a type="button" class="close" data-dismiss="alert" aria-hidden="true">×</a>Error while submitting.</div>');
     }

   }else{

    $data = array('status' => '2', 'error' => $this->form_validation->get_field_data(), 'postdata' => $this->input->post());    
  }
}else{

  $data = array('status' => '3');
  $this->session->set_flashdata('errormsg', '<div class="alert alert-warning alert-dismissable"><a type="button" class="close" data-dismiss="alert" aria-hidden="true">×</a>All fields are mandatory.</div>');
}
print_r(json_encode($data));


}

//function for viewing organization
function vieworganization(){
 $this->load->view('admin/all_css');
 $this->load->view('admin/dashboard_header');
 $this->load->view('admin/viewOrganization');
 $this->load->view('admin/footer');
 $this->load->view('admin/all_js');
 $this->load->view('admin/pagewise_js/organization.js');

}

//function for viewing organization
function viewContacts(){
 $this->load->view('admin/all_css');
 $this->load->view('admin/dashboard_header');
 $this->load->view('admin/viewContacts');
 $this->load->view('admin/footer');
 $this->load->view('admin/all_js');
 $this->load->view('admin/pagewise_js/contacts.js');

}



//function for logout..
function logout(){
  $this->session->sess_destroy();
  redirect('admin/index');  

}

}
?>