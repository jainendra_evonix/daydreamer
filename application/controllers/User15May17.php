<?php
ob_start();
	defined('BASEPATH') OR exit('No direct script access allowed');

	class User extends CI_Controller {

			/**
		 * Index Page for this controller.
		 *
		 * Maps to the following URL
		 * 		http://example.com/index.php/welcome
		 *	- or -
		 * 		http://example.com/index.php/welcome/index
		 *	- or -
		 * Since this controller is set as the default controller in
		 * config/routes.php, it's displayed at http://example.com/
		 *
		 * So any other public methods not prefixed with an underscore will
		 * map to /index.php/welcome/<method_name>
		 * @see https://codeigniter.com/user_guide/general/urls.html
		 */

			public function __construct() {
	        parent::__construct();
	        $this->load->library('session');
	        $this->load->library('form_validation');
			$this->load->model('user_model');
			//$this->load->model('admin_model');
	        $this->load->library('email');
	        $this->load->helper('url');
	        $this->load->helper('security');
	        $this->load->helper('cookie');
	      
	    }



               
      public function index()
			{ 
                 
                // echo $this->session->userdata('user_id'); exit;

                  error_reporting(0);
                if($this->session->userdata('user_id'))
                {
                   
                   $user_id = $this->session->userdata('user_id');
                	$data['userProfileData'] = $this->user_model->getUserProfileData($user_id);
                }
	        //echo 123; exit;
				$this->load->view('frontEnd/all_css');
				if($data){

                	$this->load->view('header',$data);
                }else{
                	$this->load->view('header');
                }
				$this->load->view('frontEnd/index');

				$this->load->view('footer');
				$this->load->view('frontEnd/all_js');
			  $this->load->view('frontEnd/pagewisejs/index.js');

}

 /*==================== FUNCTION FOR LOAD VIEW BLOG ===============*/
	  public function blog()
	  {
	  //  echo 123; exit;
	   $this->load->view('user/all_css'); 
       $this->load->view('user/header');
       $this->load->view('user/blogs');
       $this->load->view('user/footer');



	  }



 /*================= FUNCTION FOR VIEW BLOG ===================*/
   
   public function viewBlog()
   {


       $this->load->view('user/all_css'); 
       $this->load->view('user/header');
       $this->load->view('user/singleblog');
       $this->load->view('user/footer');



   }






/*==========FUNCTION FOR LOAD VIEW SEARCHED JOBS=============*/

     public function searchedJob()
     {

      // $str = "(industry_name LIKE '%Financial Services%') OR (industry_name LIKE '%IT Services%') OR " ;
      // echo $str.'<br>';
      // echo rtrim($str, ' OR ');
      //exit;        
     // echo "<pre>"; print_r($_POST); exit;


                error_reporting(0);
                if($this->session->userdata('user_id'))
                {
                   
                  $user_id = $this->session->userdata('user_id');
                	$data1['userProfileData'] = $this->user_model->getUserProfileData($user_id);
                	//$data['userAppliedJobs']  = $this->user_model->getUserAppliedJobs($user_id);
                }
                 
                //echo "<pre>"; print_r($data); exit;
               
                 $keyword = $this->input->post('keyword');
                 $location = $this->input->post('location');

                // echo $keyword."  ".$location; exit;
                
                  if(!empty($keyword || $location)){

                $data['searchedJobs'] = $this->user_model->searchJobs($keyword,$location);
                $data['jobsByIndustry'] = $this->user_model->getJobsByIndustry();
                $data['industries']     = $this->user_model->getAllIndustries();
                $data['jobsByTitle']    = $this->user_model->getJobsByTitle();
                $data['jobsByCity']     = $this->user_model->getJobsByCity(); 
                $data['jobsByExperience'] = $this->user_model->getJobsByExperience(); 
                }
                else{

                	$data['searchedJobs'] = $this->user_model->searchJobs2();
                	$data['jobsByIndustry'] = $this->user_model->getJobsByIndustry();
                	$data['industries']     = $this->user_model->getAllIndustries();
                	$data['jobsByTitle']    = $this->user_model->getJobsByTitle();
                	$data['jobsByCity']     = $this->user_model->getJobsByCity();
                	$data['jobsByExperience'] = $this->user_model->getJobsByExperience();
                }
              // echo "data"."<pre>"; print_r($data); exit;

                $this->load->view('frontEnd/all_css');

                if(!empty($data1)){

                	$this->load->view('header',$data1);
                }else{
                	$this->load->view('header');
                }
				
				$this->load->view('frontEnd/job-listing',$data);

				$this->load->view('footer');
				$this->load->view('frontEnd/all_js');
				$this->load->view('frontEnd/pagewisejs/jobListing.js');



     }


     public function saveDeselect()
     {

       $deSelected = ($this->input->post('f'));

         $string = implode(',',$deSelected);

       $user_id = $this->session->userdata('user_id');
       
       $data = array('job_type' => $string);


       $saveDeselect = $this->user_model->userJobDeselect($user_id,$data);

     }


	public function login()
			{ 

	        //echo 123; exit;
				$this->load->view('user/all_css');
				$this->load->view('user/header');
				$this->load->view('user/login');

				$this->load->view('user/footer');
				$this->load->view('user/all_js');
				$this->load->view('user/pagewisejs/login.js');


			}

			/*========Function for user login=========== */

			public function checklogin() {

		   $_POST = json_decode(file_get_contents('php://input'), true);

	       // print_r($_POST); exit;
          
           error_reporting(0);
				if ($this->input->post()) {
					$this->form_validation->set_rules('emailaddress', 'Email ID', 'trim|required|trim|xss_clean|strip_tags|valid_email');
					$this->form_validation->set_rules('password', 'Password', 'trim|required|trim|xss_clean|strip_tags');
					$this->form_validation->set_rules('remember','Rememerme','');
                  

					

					if ($this->form_validation->run() == TRUE) {

						$emailaddress = $this->input->post('emailaddress');
						$password = $this->input->post('password');

                        $remember = $this->input->post('remember');

                         if($remember)
                         {
                            
                                   $this->rememberme->setCookie($this->input->post('remember'));
                         			 $cookie_user = $this->rememberme->verifyCookie();
										 if ($cookie_user) {
										 	// find user id of cookie_user stored in application database
										 	$user = User::findUser($cookie_user);
										 	// set session if necessary
										 	if (!$this->session->userdata('user_id')) {
										 		$this->session->set_userdata('user_id', $user);
										 	}
										 	$this->user = $user;
										 }
										 else if ($this->session->userdata('user_id')) {
										 	$this->user = $this->session->userdata('user_id');
										 }


                         }
                            
                       
				$response = $this->user_model->check_user_login($emailaddress, $password);

						if ($response) {
					//echo '<pre>';print_r($response);exit;
							/*$this->session->set_userdata(array(
								'user_id' => $response->id,
								'user_email' => $response->user_email,
								'first_name' => $response->first_name,
								'last_name'  => $response->last_name,
								'loginsession' => true));*/
							
                       
							//$this->session->set_userdata('login_session',$afterlogin);
							//print_r($this->session->all_userdata()); exit;
                     $user_id = $response->id;
                    //$user_profileData = $this->user_model->getUserProfileData($user_id);

                   // echo "<pre>"; print_r($user_profileData); exit;
                 $userLogedData = array(
								'user_id' => $response->id,
								'user_email' => $response->user_email,
								'f_name' => $response->first_name,
								'l_name'  => $response->last_name,
								/*'profilePic' => $user_profileData->user_profilePic,*/
								'user_login' => true);
                       
                     //  echo "<pre>"; print_r($userLogedData); exit; 
                         $logDetail = array(
                                      'user_id' =>$response->id,
                                      'role'  => 'user',
                                      'status' => 1,
                         	              );

                   $saveLog = $this->user_model->saveLoginLogOut($logDetail);

                       $this->session->set_userdata($userLogedData);
                         
                         $checkSteps = $this->user_model->getUserCompSteps($user_id);

                         $profile = $checkSteps->profile;
                          

                        //echo $profile; exit;

                          if($profile == 1)
                          {

                          	$data = array('status' => '4');
                          }else{

                          	$data = array('status' => '1');
                          }

							
						}
						else {
					//$data = array('status' => '0');
							$data = array('status' => '0', 'message' => '<div class="alert alert-danger"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>Wrong username or password combination !.</div>');
						}
					} else {
						$data = array('status' => '2', 'error' => $this->form_validation->get_field_data(), 'postdata' => $this->input->post());
					}
	        //print_r($this->form_validation->get_field_data()); exit;
	       //print_r($data);     		
				}
				else {
					$data = array('status' => '3');
				}
	    //print_r($data); exit;
				print_r(json_encode($data));
	    //echo json_encode($data);
			}

  
  /*===========FUNCTION FOR LOAD DASHBOARD VIEW ============*/

  public function dashboard()
	{
                
           if(!$this->session->userdata('user_id')){

	          	redirect(base_url().'user');
	          }

                $user_id = $this->session->userdata('user_id');
                $data['title'] = "Dashboard";
                $data['fname'] = $this->session->userdata('f_name');
                $data['lname'] = $this->session->userdata('l_name');
                $data['email'] = $this->session->userdata('user_email');
                $data['userProfileData'] = $this->user_model->getUserProfileData($user_id);
                $data['userEmploymentDetail'] = $this->user_model->userEmploymentInfo($user_id);
                $userResumes = $this->user_model->getUserResumeInfo($user_id);

                  $temp = array();

        foreach($userResumes->result() as $key => $val) {
          $temp[$val->profile_id][]=$val;

        }
             //echo "<pre>"; print_r($temp); exit;
        $data['resumesInfo']=$temp;


                $data['appliedJobs'] = $this->user_model->getAppliedUserJobs($user_id);
                $data['lastUpdate']  = $this->user_model->getLastUpdateuserProfile($user_id);
                $data['completeSteps'] = $this->user_model->getUserCompSteps($user_id);
                $data['checkEducation'] = $this->user_model->checkUserEducation($user_id);
                $data['checkPreEmplyment'] = $this->user_model->checkUserPreEmployment($user_id);
               // $this->session->set_userdata('imgurl',$userProfileData->user_profilePic);
                $this->load->view('frontEnd/all_css',$data);
				$this->load->view('header',$data);
				$this->load->view('user/userDashboard',$data);

				$this->load->view('footer');
				$this->load->view('frontEnd/all_js');
				$this->load->view('user/pagewisejs/dashboard.js');



		  }


  /*========= FUCTION FOR SAVE PROFILE PICTURE ===================*/

 public function sbmtProfilePic()
 {

  define('UPLOAD_DIR', 'uploads/profilePic/');
        $_POST = json_decode(file_get_contents('php://input'), true);
        
        if ($this->input->post()) {
          
          $image_name = $_POST['userfile'];
          $img = $_POST['imgdata'];
       
       //  echo $image_name." ".$img; exit;
          $img = str_replace('data:image/png;base64,', '', $img);
	     $img = str_replace(' ', '+', $img);
	     $decoded = base64_decode($img);
		 $uid=$this->session->userdata('user_id');
		 $file = UPLOAD_DIR .$uid.'_'.$image_name;
		 $file_name=$uid.'_'.$image_name;

	    $success = file_put_contents($file, $decoded);

	    if($success)
	    {

         $upload = $this->user_model->saveProfilePic($file_name,$uid);


         if($upload)
           {
            
               $data = array('status' =>'1');
               $this->session->set_flashdata('succmsg','Profile picture has been uploaded successfully');
           }
	    }

        }else{

         $data = array('status' => '2');
         $this->session->set_flashdata('errmsg','Please select profile picture properly');
  

        }

 print_r(json_encode($data));

 }



/*======== FUNCTION FOR MATCH PASSWORD TO CHANGE PASSWORD ==========*/

 public function matchPassword()
 {

   // $_POST = json_decode(file_get_contents('php://input'), true);

    $currntPswd = md5($this->input->post('currntPass'));
    $user_id = $this->session->userdata('user_id');
   // $encryptPass = md5($currntPswd);
   // echo $currntPswd; exit;

   $check  = $this->user_model->checkCurrentPswrd($currntPswd,$user_id);
  // echo "<pre>"; print_r($check);
   if(!empty($check)){

   	echo 1; exit;
   }


 }


   /*======== FUNCTION FOR UPDATE NEW PASSWORD ===========*/

 public function updateNewPassword()
 {
   $_POST = json_decode(file_get_contents('php://input'), true);

    $newPass = $this->input->post('newPswrd');
    $cpass = $this->input->post('cnfrmPass'); 
    $user_id = $this->session->userdata('user_id');
 
   //echo $newPass."asss".$cpass; exit;

    if($newPass == $cpass)
    {

      $updatePass = $this->user_model->updateUserPassword($user_id);

      if($updatePass)
      {
         $data = array('status' => 1);

          $this->session->set_flashdata('succmsg', 'Your password has been changed');


      }



    }else{

      
      $data = array('status'=>2);

    $this->session->set_flashdata('errmsg', 'Your password has not been changed');

    }
   
    print_r(json_encode($data));

 }



//function for upload cover_letter

public function uploadCoverLetter(){
                
           // echo 1234; exit;
 
    $user_id = $this->session->userdata('user_id');

$filename=$_FILES['file']['name'];

//echo $filename; exit;

          $target = 'uploads/coverLetters/';
                     $file = $target.$user_id.'-'.$filename;
                  if(isset($_POST)){


                     if(isset($_FILES['file']['tmp_name'])){

                       
                        if(move_uploaded_file($_FILES['file']['tmp_name'], $file))
                        {
                            
                           
                                
                        }else{


                            echo 'not uploded';
                        }
                     }
                        
                       }

}



//function for upload user_resume

public function uploadResume(){
                
           // echo 1234; exit;
 
    $user_id = $this->session->userdata('user_id');

$filename=$_FILES['file']['name'];

//echo $filename; exit;

          $target = 'uploads/userResumes/';
                     $file = $target.$user_id.'-'.$filename;
                  if(isset($_POST)){


                     if(isset($_FILES['file']['tmp_name'])){

                       
                        if(move_uploaded_file($_FILES['file']['tmp_name'], $file))
                        {
                            
                           
                                
                        }else{


                            echo 'not uploded';
                        }
                     }
                        
                       }

}

/*========== FUNCTION FOR SAVE USER RESUME INFO ========*/

public function userResumeInfo()
{

$_POST = json_decode(file_get_contents('php://input'), true);

 //echo "<pre>"; print_r($_POST); exit; 

if($this->input->post()){

  
     $this->form_validation->set_rules('covrLetter','CoverLetter','required');
     $this->form_validation->set_rules('resumeTitle','ResumeTitle','required');
     $this->form_validation->set_rules('resumeUser','UserResume','required');
     $this->form_validation->set_rules('profileName','profileName','required');
     
      $user_id = $this->session->userdata('user_id');
     if($this->form_validation->run() == TRUE)
     {

        $coverLetters = $this->input->post('covrLetter');
        $resumeTitle = $this->input->post('resumeTitle');
        $resumeUser =  $this->input->post('resumeUser');
        $profileName = $this->input->post('profileName');
        date_default_timezone_set('Asia/Kolkata');
                      $date = date('d-m-Y H:i');
        $resumeInfo = array(
                      
                         'user_id' => $user_id,
                          'resume_title' => $resumeTitle,
                          'cover_letter' => $user_id.'-'.$coverLetters,
                          'resume'  =>   $user_id.'-'.$resumeUser,
                          'profile' => $profileName

        	            );

      //  echo "<pre>"; print_r($resumeInfo); exit;

        $saveUserResume = $this->user_model->saveUserResume($user_id,$profileName);

        if($saveUserResume){

                     $steps = array(
                                       'u_id' => $user_id,
                                       'resume' => 1,
                                       'updated_date' => $date

                           	             );


                          $steps = $this->user_model->updateUserSteps($steps,$user_id);

	                  

	                     	 $data = array('status' => '1');
		                        	$this->session->set_flashdata('succmsg','Your information has been saved successfully');

         }

     }



  }else{

         $data = array('status' => '2', 'error' => $this->form_validation->get_field_data(), 'postdata' => $this->input->post());


  }

   
  print_r(json_encode($data));
}

/*=============FUNCTION FOR LOG OUT ==============*/

	public	function logout()
		{

      
       if($this->session->userdata('user_login')){
          
            $logDetail = array(
                                      'user_id' =>$this->session->userdata('user_id'),
                                      'role'  => 'user',
                                      'status' => 0,
                         	              );

                   $saveLog = $this->user_model->saveLoginLogOut($logDetail);
          
       	$this->session->unset_userdata('user_login');
        $this->session->unset_userdata('user_email');
        $this->session->unset_userdata('f_name');
        $this->session->unset_userdata('l_name');
        $this->session->unset_userdata('user_id');
        
      //  $this->session->sess_destroy();
		    
		    redirect(base_url().'user/index');
		}

     }

/*function for show signup msg*/

public function activaton()
{

                $this->load->view('user/all_css');
				$this->load->view('user/header');
				
                $this->load->view('user/activation');
				$this->load->view('user/footer');
				$this->load->view('user/all_js');

}

            /*============FUNCTION FOR SEND MAIL ===========*/

            public function sendTOmail($subject,$message,$username)
        {
        	
        	   require 'vendor/autoload.php';
		
				$sendgrid = new SendGrid("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");
				$email    = new SendGrid\Email();
		
				$email->addTo($username)
      				  ->addCc(cc_email)
                      ->setFrom(from_email)
                      ->setFromName("Day Dreamer | Job Portal")
                      ->setSubject($subject)
                      ->setHtml($message);
 
       		$sendgrid->send($email);


 
        }


			/*=========Function for user registration===========*/

			public function registration()
			{

				$this->load->view('user/all_css');
				$this->load->view('user/header');
				$this->load->view('user/registration');
				$this->load->view('user/footer');
				$this->load->view('user/all_js');
				$this->load->view('user/pagewisejs/registration.js');

			}

			/*===== Function for submit user info ====*/

			public function userData()
			{

				$_POST = json_decode(file_get_contents('php://input'),true);

				if($this->input->post()){

	         // echo "<pre>"; print_r($_POST);

					$this->form_validation->set_rules('fname','FirstName','trim|required|trim|xss_clean|strip_tags');
					

					$this->form_validation->set_rules('lname', 'LastName', 'trim|required|trim|xss_clean|strip_tags');
					
					$this->form_validation->set_rules('city', 'City', 'trim|required|trim|xss_clean|strip_tags|strip_tags');
					
					$this->form_validation->set_rules('emailid', 'Emailid', 'trim|required|trim|xss_clean|strip_tags|valid_email');
	                 
					$this->form_validation->set_rules('password', 'Password', 'trim|required|trim|xss_clean|strip_tags');
	               
					$this->form_validation->set_rules('cpassword', 'Cpassword', 'trim|required|trim|xss_clean|strip_tags');

					if($this->form_validation->run() == TRUE)
					{
						$randomkeystring = $this->generateRandomString();
	                    $email = $this->input->post('emailid');

	                   $chkEmail =  $this->user_model->checkEmail($email);
                          
                         // echo "<pre>"; print_r($chkEmail); exit;

	                   if($chkEmail == null)

	                   {
	                       
	                       $info = array(

							'first_name' => $this->input->post('fname'),
							'last_name'  =>  $this->input->post('lname'),
							'user_city'  =>  $this->input->post('city'),
							'user_email' => $this->input->post('emailid'),
							'user_password'=>md5($this->input->post('password')),
							'activation_token' => $randomkeystring,
							'created_date'=>date("Y-m-d H:i:s")


							);
	                 
	                    $response = $this->user_model->sigUp($info);



                       
                        $url=base_url()."user/activate/".$randomkeystring;
                        $fname = $this->input->post('fname');
                        $lname = $this->input->post('lname');
                        $password = $this->input->post('password');

                        $fullname = $fname." ".$lname;
                                 
                                 
                             
                    					$subject = "Thank you for registration";
                    					    
                    			   	    $message = "Dear ".$fullname."<br><br>";
                    				    $message .="Greetings from Day Dreamer!<br><br>";
                    				    $message .="Thank you for registration.<br><br>";
                    				    
                    				 
                    				    
                    				    $message .="Below are your login details .<br>";
                    				    $message .="<b> Email</b> : ".$email." .<br> <b> Password</b>  :".$password.".<br>";


                    				      
                                       $message .="<br />To activate your account, Please click the below link. <br>";
                    				   $message .="<b> <a href=".$url."> Click Here.</a> </b><br><br>";
                                          
                    				   
                    				   
                    				   $this->sendTOmail($subject,$message,$email);

                          
                       if ($response) {
			           	$data = array('status' => '1');
				       $this->session->set_flashdata('successmsg', 'Account activation link has been send on your email.Please activate your account.');
				    }

				else 
				{
				$data = array('status' => '0');
				$this->session->set_flashdata('errormsg', '<div class="alert alert-warning alert-dismissable"><a type="button" class="close" data-dismiss="alert" aria-hidden="true">×</a>Error while submitting.</div>');
        		}

        		 }else{
                      

                      $data = array('status' => '4');
			          $this->session->set_flashdata('errormsg', '<div class="alert alert-warning alert-dismissable"><a type="button" class="close" data-dismiss="alert" aria-hidden="true">×</a>The email ID you have entered is already exist !.</div>');


	                   }
						

	 



	                 }else{

							 $data = array('status' => '2', 'error' => $this->form_validation->get_field_data(), 'postdata' => $this->input->post());
							}

	}

	else{
           
           $data = array('status' => '3');
		$this->session->set_flashdata('errormsg', '<div class="alert alert-warning alert-dismissable"><a type="button" class="close" data-dismiss="alert" aria-hidden="true">×</a>All fields are mandatory.</div>');

	}
	print_r(json_encode($data));


	}


	/*============= FUNCTION FOR ACTIVATE SIGNUP ACCOUNT =============*/
     
     public function activate($token)
        
     {
     	
      $actvntm = date("Y-m-d H:i:s");

      $data = array(

            'flag'=>1,
           
            'activation_time'  => $actvntm,
            	);
     
    $status = $this->user_model->activateLogin($data,$token);

    if($status)
    {
            $this->session->set_flashdata('succmsg',"Your email id has been verified. Login with your credentials");

    redirect(base_url().'user');

    }


     }

/*====================FUNCTION FOR CHECK DUPLICATE EMAILID ===============*/

    public function checkEmail()
   {

     $email = $this->input->post('email');

    
     $checkEmail =   $this->user_model->findEmail($email);

    //echo "<pre>"; print_r($checkEmail); exit;
  if($checkEmail != null)
  {

   echo 0;


  }else{

  	echo 1;
  }

   }


/*========== FUNCTION FOR USER PROFILE DETAILS INPUT =========*/
  public function profile()

	 {

	          if(!$this->session->userdata('user_id')){

	          	redirect(base_url().'user');
	          }
	          

	          $user_id = $this->session->userdata('user_id');
              
              error_reporting(0);

	       //$status =  $this->user_model->getCompleteSteps($user_id);
            $data['title'] = "Personal info";  
            $data['months'] = $this->user_model->getMonths();
            $data['years']  = $this->user_model->getYears(); 
            $data['days']  = $this->user_model->getdays();
	        $data['userProfileData'] = $this->user_model->getUserProfileData($user_id);
	        $userProfileData = $this->user_model->getUserProfileData($user_id);
	     //  echo "<pre>";print_r($data['userProfileData']);exit;
	        $data['user_jobType']    = $this->user_model->getUserFeedJobType($user_id);
            $data['job_type'] = $this->user_model->getAllJobType();
	    	$data['countries'] = $this->user_model->getAllContries();
	    	$data['ethnicOrigin'] = $this->user_model->getAllEthnicOrigin();
	    	$data['relationshipStatus'] = $this->user_model->getRelationship();
	      //  echo "<pre>"; print_r($data); exit;
	    	$data['userProfileSteps'] = $this->user_model->getUserCompSteps($user_id);

		     $userProfileSteps = $this->user_model->getUserCompSteps($user_id);

		     $data1['profile'] = $userProfileSteps->profile;
		     $profile_steps = $userProfileSteps->profile; 
		     $data1['education']= $userProfileSteps->education;
		    
		     $data1['pre_employment']= $userProfileSteps->pre_employment;
		     $data1['employment']= $userProfileSteps->employment;
		     $data1['references']= $userProfileSteps->references;
		     $data1['additional_info']= $userProfileSteps->additional_info;
		     $data1['miscellaneous']= $userProfileSteps->miscellaneous;
		     $data1['social_media']= $userProfileSteps->social_media;
		     $data1['resume'] = $userProfileSteps->resume;
		     $data1['current']=1;



	    // echo "<pre>"; print_r($userProfileData); exit;
            $this->load->view('user/all_css',$data);
			$this->load->view('user/header',$data);
			$this->load->view('user/menu',$data1);
			$this->load->view('user/customer',$data);
		    $this->load->view('user/footer');
		    $this->load->view('user/all_js');
			$this->load->view('user/pagewisejs/customer.js');
              
            // print_r(json_encode($data)) ;
	 	}
 	

/*============== FUNCTION FOR KEY STRING==============*/

 public function generateRandomString($length = 16)
  {	
			$characters   = 'abcdefghijklmnopqrstuvwxyz0123456789';
			$randomString = '';
			for ($i = 0; $i < $length; $i++) {
				$randomString .= $characters[rand(0, strlen($characters) - 1)];
			}
			return $randomString;
	}




 	/*=============== FUNCTION FOR FORGOT PASSWORD =============*/
 	public function check_forgot_password()
 	{
 		$_POST = json_decode(file_get_contents('php://input'), true);
        

       //$_POST = json_decode(file_get_contents('php://input'), true);
        if ($this->input->post()) {
        	$this->form_validation->set_rules('email', 'Email ID', 'trim|required|trim|xss_clean|strip_tags|valid_email');
        	if ($this->form_validation->run() == TRUE) {
            $emailaddress = $this->input->post('email'); 
    	  		$randomkeystring = $this->generateRandomString();
    	  		$url = base_url().'user/';
            $response = $this->user_model->check_admin_forgot_password($emailaddress);
            if ($response) {
            	//echo '<pre>';print_r($response);exit;
				//$data = array('status' => '1');
				//$updatepassword = $this->admin_model->update_password($emailaddress,$randomkeystring);
				$updatekeystring = $this->user_model->update_keystring($emailaddress,$randomkeystring);
				
				//send mail
				require 'vendor/autoload.php';
				
				$sendgrid = new SendGrid("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");
				$email = new SendGrid\Email();
				
				$email->addTo($emailaddress)
                      ->setFrom('support@Daydreamer.com')
       				  ->setFromName('Day Dreamer')
       				  ->setSubject("Here's the link to reset your password")
       				  ->setHtml("You recently requested a password reset.<br />To change your password<br /><a href='".$url."resetpassword/$randomkeystring'>Click here</a><br />The link will expire in 12 hours, so be sure to use it right away.<br /><br /> <br />Regards, <br />Support Team.");
       
				if($sendgrid->send($email)) {
				$data = array('status' => '1', 'message' => '<div class="alert alert-success">An email has been sent to '.$emailaddress.'. Please click on the link provided in the mail to reset your password.</div>');
         	} 
         	else {
         		$data = array('status' => '4', 'message' => '<div class="alert alert-danger">Error in sending Email.</div>');
				}
				
				}
				else {
				$data = array('status' => '0', 'message' => '<div class="alert alert-danger"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>The email ID you have entered does not exist !.</div>');
        		}
        	} else {
            $data = array('status' => '2', 'error' => $this->form_validation->get_field_data(), 'postdata' => $this->input->post());
        }   		
    }
    else {
		$data = array('status' => '3');
   }
    print_r(json_encode($data));



 	 	}

/*============ FUNCTION FOR RESET PASSWORD ===========*/


 function resetpassword($randomkeystring='')
 {
  $keystring['data'] = $this->user_model->check_randomkey_string($randomkeystring);
  if(empty($keystring['data']))
  {
    $rediect_path = base_url().'user';
    redirect($redirect_path, 'refresh');
  }

  $this->load->view('user/all_css');
  $this->load->view('user/header');
  $this->load->view('user/resetpassword',$keystring);
  $this->load->view('user/footer');
  $this->load->view('user/all_js');
  $this->load->view('user/pagewisejs/resetpassword.js');

}


/*==========FUNCTION FOR CHECK RESET PASSWORD ===========*/


 public function check_reset_password() {
        $_POST = json_decode(file_get_contents('php://input'), true);
        //print_r($_POST); exit;
        if ($this->input->post()) {
        	$this->form_validation->set_rules('npassword', 'New Password', 'trim|required|trim|xss_clean|strip_tags');
        	$this->form_validation->set_rules('cpassword', 'Confirm Password', 'trim|required|trim|xss_clean|strip_tags|matches[npassword]');
        	if ($this->form_validation->run() == TRUE) {
        		$url = base_url();
            $emailaddress = $this->input->post('emailaddress');
            $npassword = $this->input->post('npassword');
            $updatepassword = $this->user_model->update_password($emailaddress,$npassword);
            //$response = $this->admin_model->check_user_login($emailaddress, $npassword);
            if ($updatepassword) {
				$data = array('status' => '1', 'message' => '<div class="alert alert-success">You have successfully changed your password. Please sign in with your new password.<br><a href="'.$url.'user" class="btn btn-twitter" style="padding: 2px 8px;">Sign In</a></div>');
				}
				else {
				//$data = array('status' => '0');
				$data = array('status' => '0', 'message' => '<div class="alert alert-danger"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>Oops... Something went wrong !.</div>');
        		}
        	} else {
            $data = array('status' => '2', 'error' => $this->form_validation->get_field_data(), 'postdata' => $this->input->post());
        }
       //print_r($data);     		
    }
    else {
		$data = array('status' => '3');
   }
    //print_r($data); exit;
    print_r(json_encode($data));
    //echo json_encode($data);
    }
      

/*================ PUBLIC FUNCTION FOR USER PERSONAL INFO ================ */


 public function personalInfo()
 {


  // echo 5432; exit; 

$_POST = json_decode(file_get_contents('php://input'),true);

   //  echo "<pre>"; print_r($_POST); exit;

				if($this->input->post()){

    
              
                     


					$this->form_validation->set_rules('gender','Gender','required');
                    $this->form_validation->set_rules('rstatus', 'Rstatus', 'required');
					$this->form_validation->set_rules('day','Day','required');
					$this->form_validation->set_rules('month','Month','required');
					$this->form_validation->set_rules('year','Year','required');
					$this->form_validation->set_rules('nationality', 'Nationality', 'required');
	                
					$this->form_validation->set_rules('ethnicorigin', 'Ethnicorigin', 'required');
	              
					$this->form_validation->set_rules('disability', 'Disability', 'required');
                     
					$this->form_validation->set_rules('disabilityDesc', 'DisabilityDesc', '');
                    
					

                   $this->form_validation->set_rules('permittedtowork','Permittedtowork','required');
                   $this->form_validation->set_rules('desiredJobtype','DesiredJobType','required');
                    

					$this->form_validation->set_select('jobtype[]', 'jobtype', 'required');
                   
					if($this->form_validation->run() == TRUE)
                    {
                      

                       $user_id = $this->session->userdata('user_id');

                       
                       $day =   $this->input->post('day');
                       $month = $this->input->post('month');
                       $year =  $this->input->post('year');
                     
                      $dob = $day.$month.$year;

                   

                       $gender           = $this->input->post('gender');
                       $rstatus          = $this->input->post('rstatus');
                      
                       $nationality      = $this->input->post('nationality');
                       $disability       = $this->input->post('disability');
                       $ethnicorigin     = $this->input->post('ethnicorigin');
                       $disabilityDesc   = $this->input->post('disabilityDesc');
                       $permittedtowork  = $this->input->post('permittedtowork');
                       $desiredJobtype   = $this->input->post('desiredJobtype');
                       

                     date_default_timezone_set('Asia/Kolkata');
                      $date = date('d-m-Y H:i'); 
                       $infoArray = array(
                              'user_id'  => $user_id,
                              'user_gender' => $gender,
                              'user_rel_status' => $rstatus , 
                              'user_dob'          => $dob,
                              'user_day'         =>$day,
                              'user_month'       =>$month,
                              'user_year'   => $year,
                              'user_nationality'    =>  $nationality,
                              'user_ethinic_origin'  =>  $ethnicorigin,
                              'user_disabilities'    => $disability ,
                              'user_disabilities_type' => $disabilityDesc,
                              'user_permission'  =>  $permittedtowork,
                              'user_desiredjobType' => $desiredJobtype

                               );
                       $userProfileStatus = $this->user_model->checkUserProfileStatus($user_id);
                       
                       if(!empty($userProfileStatus))
                       {
                         
                              $infoArray = array(
                              'user_id'  => $user_id,
                              'user_gender' => $gender,
                              'user_rel_status' => $rstatus , 
                              'user_dob'          => $dob,
                              'user_day'  => $day,
                              'user_month' => $month,
                              'user_year'  => $year,
                              'user_nationality'    =>  $nationality,
                              'user_ethinic_origin'  =>  $ethnicorigin,
                              'user_disabilities'    => $disability ,
                              'user_disabilities_type' => $disabilityDesc,
                              'user_permission'  =>  $permittedtowork,
                              'user_desiredjobType' => $desiredJobtype,
                              'updated_date' =>$date

                               );

                       

	                       $response = $this->user_model->updateUserPinfo($infoArray,$user_id);
                         
                          $p_id = $response->id;
                         
                           if($p_id)
                           {
                                     
    
	                        $updateInfo = $this->user_model->updateUserJobType($user_id,$p_id,$date);
                           }
	                        
	                          $steps = array(

	                                        'u_id' => $user_id,
	                                        'profile' => 1,
	                                        'updated_date' => $date,
	                                         );
	                          //echo "<pre>";  print_r($steps); exit;
	                            $completeSteps =  $this->user_model->updateUserSteps($steps,$user_id);
	                           
	                        	$data = array('status' => '1');
	                        	$this->session->set_flashdata('succmsg','Your personal information has been updated Successfully');
	                        
	                      


                        
                       }
                      else
                      {
                      	$lastId = $this->user_model->saveUserPersonalInfo($infoArray);



                      	foreach ($_POST['jobtype'] as $selected)
                        {
                          $data1 = array(
                                           'user_id' => $user_id,
                                           'pro_tbl_id'    => $lastId,
                                           'job_type' => $selected
                                         );
                            
                           // echo "<pre>"; print_r($data1); exit;

                        $saveJT = $this->user_model->saveUserJobType($data1);

                        }
                        if($saveJT){
                            
                          $steps = array(

                                        'u_id' => $user_id,
                                        'profile' => 1
                                         );
                            $completeSteps =  $this->user_model->saveCompleteSteps($steps);
                           
                        	$data = array('status' => '1');
                        	$this->session->set_flashdata('succmsg','Your personal information has been saved Successfully');
                        }
                      
                      }

                      
                    }else{
                              //echo "not valid";
                      $data = array('status' => '2', 'error' => $this->form_validation->get_field_data(), 'postdata' => $this->input->post());

                    }

                 }else{

                      
                      $data = array('status' => '3');
		$this->session->set_flashdata('errormsg', '<div class="alert alert-warning alert-dismissable"><a type="button" class="close" data-dismiss="alert" aria-hidden="true">×</a>All fields are mandatory.</div>');


                 }

               //  print_r($data); exit;
 	print_r(json_encode($data));
}


	/*===================  FUNCTION USER EDUCATION INFO =====================*/

	public function education()
	{
              

         if(!$this->session->userdata('user_id')){

	          	redirect(base_url().'user');
	          }
           error_reporting(0);
           $user_id = $this->session->userdata('user_id');

           $data['months'] = $this->user_model->getMonths();
           $data['years']  = $this->user_model->getYears();
           $data['qualification'] = $this->user_model->getQualificationTypes();
           $data['grade'] = $this->user_model->getGrade();
           $data['achieved'] = $this->user_model->getAchieved();
           $data['userEducationDetails'] = $this->user_model->getUserEducationDetails($user_id);
           $data['userEducation'] = $this->user_model->getEdu($user_id);
           $data['userProfileData'] = $this->user_model->getUserProfileData($user_id);
           
      
        // echo "<pre>";    print_r($relationshipStatus); exit;
              
        $userProfileSteps = $this->user_model->getUserCompSteps($user_id);
         $data['title'] = "Education";
	     $data1['profile'] = $userProfileSteps->profile;
	    // echo $profile; exit;
	     $data1['education']= $userProfileSteps->education;
	    
	     $data1['pre_employment']= $userProfileSteps->pre_employment;
	     $data1['employment']= $userProfileSteps->employment;
	     $data1['references']= $userProfileSteps->references;
	     $data1['additional_info']= $userProfileSteps->additional_info;
	     $data1['miscellaneous']= $userProfileSteps->miscellaneous;
	     $data1['social_media']= $userProfileSteps->social_media;
	     $data1['resume'] = $userProfileSteps->resume;
         $data1['current']=2;

           
				$this->load->view('user/all_css',$data);
				$this->load->view('user/header',$data);
				$this->load->view('user/menu',$data1);
				$this->load->view('user/education',$data);
				$this->load->view('user/footer');
				$this->load->view('user/all_js');
				$this->load->view('user/pagewisejs/education.js');
				

	}

/*=================== FUNCTION FOR USER EDUCATION INFO ===============*/

public function educationInfo()
{

//$_POST = json_decode(file_get_contents('php://input'),true);

$_POST = json_decode(file_get_contents('php://input'),true);
    

   // echo "<pre>"; print_r($_POST); exit;

    if($this->input->post())
    {
      
           

           $this->form_validation->set_rules('instName','InstitutionName','required');
           $this->form_validation->set_rules('courseStudy','CourseStudy','required');
           $this->form_validation->set_rules('qualificationType','QualificationType','required');
           $this->form_validation->set_select('fromYear','fromYear','required');
           //$this->form_validation->set_radio('studying','Studying','required');
           $this->form_validation->set_select('fromMonth','FromMonth','required');
           $this->form_validation->set_select('toYear','ToYear','required');
           $this->form_validation->set_select('toMonth','ToMonth','required');
           $this->form_validation->set_select('courseStudy','CourseStudy','required');
           $this->form_validation->set_select('grade','Grade','required');
           $this->form_validation->set_select('achieved','Achieved','required');
           $this->form_validation->set_rules('currStatus','CurrentStatus','');
          
            if($this->form_validation->run() == TRUE){

                //echo  "formValidate"."123"; exit; 
                    
                    $user_id = $this->session->userdata('user_id');
                    $instName = $this->input->post('instName');
                    $fromMonth = $this->input->post('fromMonth');
                    $fromYear = $this->input->post('fromYear');
                    $toMonth = $this->input->post('toMonth');
                    $toYear = $this->input->post('toYear');
                    $course = $this->input->post('courseStudy');
                    $grade = $this->input->post('grade');
                    $achieved = $this->input->post('achieved');
                    $studying = $this->input->post('currStatus');
                    $qualificationType = $this->input->post('qualificationType');

                     date_default_timezone_set('Asia/Kolkata');
                      $date = date('d-m-Y H:i'); 
                       
                      // $educationStatus = $this->user_model->checkEducationStatus($user_id);
                      
                    //  print_r($educationStatus); exit;

                     $educationArray = array(

                                    'user_id' => $user_id,
                                    'institute_name' => $instName,
                                    'from_year' =>  $fromYear,
                                    'from_month' => $fromMonth,
                                    'to_year' =>    $toYear,
                                    'to_month' =>   $toMonth,
                                    'studying' => $studying,
                                    'course' =>     $course,
                                    'qualification_type' =>$qualificationType,
                                    'grade' =>    $grade,
                                    'achieved' => $achieved

                     	                 );

                       //echo "<pre>"; print_r($educationArray); 

                    /* if(!empty($educationStatus))
                     {
                        echo 1; exit;        
                       $updateEducationInfo = $this->user_model->saveUserEducationInfo($educationArray);  
                       
                      
                          $steps = array(
	                     	 	                 'u_id' => $user_id,
	                                             'education' => 1,
	                                             'updated_date' =>$date
	                                            );


	                          $steps = $this->user_model->updateUserSteps($steps,$user_id);

	                  

	                     	 $data = array('status' => '1');
		                        	$this->session->set_flashdata('succmsg','Your Information Saved Successfully');

                          

                     }*/
                    

              //echo 2; exit;

                     

	                     $saveEducationInfo = $this->user_model->saveUserEducationInfo($educationArray);

	                     if($saveEducationInfo)
	                     { 

	                     	 $steps = array(
	                     	 	                 'u_id' => $user_id,
	                                             'education' => 1,
	                                             'updated_date' =>$date
	                                            );


	                          $steps = $this->user_model->updateUserSteps($steps,$user_id);

	                  

	                     	 $data = array('status' => '1');
		                        	$this->session->set_flashdata('succmsg','Your information has been saved successfully');
	                     }

                        

  

            }else{


             $data = array('status' => '2', 'error' => $this->form_validation->get_field_data(), 'postdata' => $this->input->post());

            }

          }else{

                      
               $data = array('status' => '3');
		       $this->session->set_flashdata('errormsg', '<div class="alert alert-warning alert-dismissable"><a type="button" class="close" data-dismiss="alert" aria-hidden="true">×</a>All fields are mandatory.</div>');
          }
 

            print_r(json_encode($data));


    }


/*================= FUNCTION FOR LOAD VIEW FOR PRE EMPLOYMENT ==================*/


  public function preEmployment()
  {


     if(!$this->session->userdata('user_id')){


     	redirect(base_url().'user');
     }
          
          error_reporting(0);
           $user_id = $this->session->userdata('user_id');
         $data['title'] = "Pre-employment";
         $data['months'] = $this->user_model->getMonths();
         $data['years']  = $this->user_model->getYears(); 
         $data['PreEmploymentInfo'] = $this->user_model->getUserPreEmploymentInfo($user_id);
         $data['userOrganisation']   = $this->user_model->getUserOrganisation($user_id);
          $data['userProfileData'] = $this->user_model->getUserProfileData($user_id);

       //  echo "<pre>"; print_r($data); exit;

          $data['organisation'] = $this->user_model->getOrganisations();

          $userProfileSteps = $this->user_model->getUserCompSteps($user_id);

	     $data1['profile'] = $userProfileSteps->profile;
	    // echo $profile; exit;
	     $data1['education']= $userProfileSteps->education;
	    
	     $data1['pre_employment']= $userProfileSteps->pre_employment;
	     $data1['employment']= $userProfileSteps->employment;
	     $data1['references']= $userProfileSteps->references;
	     $data1['additional_info']= $userProfileSteps->additional_info;
	     $data1['miscellaneous']= $userProfileSteps->miscellaneous;
	     $data1['social_media']= $userProfileSteps->social_media;
	     $data['resume'] = $userProfileSteps->resume;
         $data1['current']=3;

            $this->load->view('user/all_css',$data);
			$this->load->view('user/header',$data);
			$this->load->view('user/menu',$data1);
			$this->load->view('user/pre-employment',$data);
		    $this->load->view('user/footer');
		    $this->load->view('user/all_js');
		    $this->load->view('user/pagewisejs/preEmployment.js');
			//$this->load->view('user/pagewisejs/customer.js');

  }


 /*====================== FUNCTION FOR GET USER EDUCATION INFO FOR EDIT ===============*/
    
    public function getUserEducationInfo()
    {
    
    $_POST = json_decode(file_get_contents('php://input'),true);
    $id = $this->input->post('id');

    //echo $id; exit;
 
 $data['userEducationDetail'] = $this->user_model->userEducationInfo($id);

  

  
// echo "<pre>"; print_r($data['userEductionDetail']); exit;
     
      print_r(json_encode($data));



    }
/*=================== FUNCTION FOR UPDATE USER EDUCATION INFO ==================*/


  public function updateEducationInfo()
  {

     $_POST = json_decode(file_get_contents('php://input'),true);
       // echo "<pre>"; print_r($_POST); exit;


     if($this->input->post())
    {

    
           $this->form_validation->set_rules('einstName','InstitutionName','required');
           $this->form_validation->set_rules('hiddenId','hidden','required');
           $this->form_validation->set_rules('ecourseStudy','CourseStudy','required');
           $this->form_validation->set_rules('equalificationType','QualificationType','required');
           $this->form_validation->set_select('efromYear','fromYear','required');

           $this->form_validation->set_select('efromMonth','FromMonth','required');
           $this->form_validation->set_select('etoYear','ToYear','required');
           $this->form_validation->set_select('etoMonth','ToMonth','required');
           $this->form_validation->set_select('estudying','Estudying','required');
           $this->form_validation->set_select('ecourseStudy','CourseStudy','required');
           $this->form_validation->set_select('egrade','Grade','required');
           $this->form_validation->set_select('eachieved','Achieved','required');
          
            if($this->form_validation->run() == TRUE){

                    $id= $this->input->post('hiddenId');
                    $user_id = $this->session->userdata('user_id');
                    $instName = $this->input->post('einstName');
                    $fromMonth = $this->input->post('efromMonth');
                    $fromYear = $this->input->post('efromYear');
                    $toMonth = $this->input->post('etoMonth');
                    $toYear = $this->input->post('etoYear');
                    $estudying= $this->input->post('estudying');
                    $course = $this->input->post('ecourseStudy');
                    $grade = $this->input->post('egrade');
                    $achieved = $this->input->post('eachieved');
                    $qualificationType = $this->input->post('equalificationType');

                     date_default_timezone_set('Asia/Kolkata');
                      $date = date('d-m-Y H:i'); 
                       
                       
                      
                     // print_r($educationStatus); exit;

                     $educationArray = array(

                                    'user_id' => $user_id,
                                    'institute_name' => $instName,
                                    'from_year' =>  $fromYear,
                                    'from_month' => $fromMonth,
                                    'to_year' =>    $toYear,
                                    'to_month' =>   $toMonth,
                                    'studying' =>   $estudying,
                                    'course' =>     $course,
                                    'qualification_type' =>$qualificationType,
                                    'grade' =>    $grade,
                                    'achieved' => $achieved,
                                    'updated_date'=>$date

                     	                 );
                      // echo $id;
                       //echo "<pre>"; print_r($educationArray); exit;

                   
                                
                       $updateEducationInfo = $this->user_model->updateUserEducationInfo($educationArray,$id);  
                       
                      
                          $steps = array(
	                     	 	                 'u_id' => $user_id,
	                                             'education' => 1,
	                                             'updated_date' =>$date
	                                            );


	                          $steps = $this->user_model->updateUserSteps($steps,$user_id);

	                  

	                     	 $data = array('status' => '1');
		                        	$this->session->set_flashdata('succmsg','Your information has been updated successfully');
               
                
               }else{
   
                 $data = array('status' => '3');
		       $this->session->set_flashdata('errormsg', '<div class="alert alert-warning alert-dismissable"><a type="button" class="close" data-dismiss="alert" aria-hidden="true">×</a>All fields are mandatory.</div>');

           }



    }



        print_r(json_encode($data));

  }
 

 /*==================== FUNCTION FOR DELETE USER EDUCATION ================*/

  public function deleteUserEducationInfo()
  {
    
    $_POST = json_decode(file_get_contents('php://input'),true);

    $id = $this->input->post('id'); 

     // echo $id; exit;

      $deleteEdu = $this->user_model->deleteUserEducation($id);

       $data = array('status' => '1');
		          $this->session->set_flashdata('succmsg','Your information has been deleted successfully');

		           print_r(json_encode($data));

  }

  /*==================== FUNCTION FOR DELETE USER RESUME ================*/

  public function deleteUserResume()
  {
    
    $_POST = json_decode(file_get_contents('php://input'),true);

    $id = $this->input->post('id'); 

     // echo $id; exit;

      $deleteEdu = $this->user_model->deleteResume($id);

       $data = array('status' => '1');
		          $this->session->set_flashdata('succmsg','Your information has been deleted successfully');

		           print_r(json_encode($data));

  }
  
   public function deleteUserResumeDash()
    {
    
    $_POST = json_decode(file_get_contents('php://input'),true);

    $id = $this->input->post('id'); 

     // echo $id; exit;

      $deleteEdu = $this->user_model->deleteResume($id);

       $data = array('status' => '1');
		          $this->session->set_flashdata('succmsg','Your information has been deleted successfully');

		           print_r(json_encode($data));

   }

  /*============FUNCTION FOR DELETE USER PRE-EMPLOYMENT==========*/

      public function deleteUserPreEmployment()
      {

        $_POST = json_decode(file_get_contents('php://input'),true);

       $id = $this->input->post('id'); 

       $delpreEmloyment = $this->user_model->delUserPreEmploymentInfo($id);

       if($delpreEmloyment)
       {

        $data = array('status' => '1');
		          $this->session->set_flashdata('succmsg','Your information has been deleted successfully');

		           

       }
           print_r(json_encode($data));

      }
  


  /*====================FUNCTION FOR SAVE USER PRE-EMPLOYMENT DETAIL ===============*/

    public function preEmploymentInfo()
    {

	   $_POST = json_decode(file_get_contents('php://input'),true);

	   $id = $this->input->post('id'); 
      $user_id = $this->input->post('user_id');
	 // echo "<pre>"; print_r($_POST); exit;




	  /* foreach ($_POST['organisations'] as $selected) {

                            
                       $orgArray = array(
                                  
                                  'user_id' => $user_id,
                                  'organisation' => $selected,
                                    );
                             echo "<pre>"; print_r($orgArray); 
                         //  $saveSelected = $this->user_model->saveSelectedOrg($orgArray);
                            }

                            exit;*/

		    if($this->input->post())
		    {
                 $this->form_validation->set_rules('emplrNme','EmployerName','required');
                 $this->form_validation->set_rules('fromMonth','FromMonth','required');
                 $this->form_validation->set_rules('fromYear','FromYear','required');
                 $this->form_validation->set_rules('toMonth','ToMonth','required');
                 $this->form_validation->set_rules('toYear','ToYear','required');
                 $this->form_validation->set_rules('designation','Designation','required');
                 $this->form_validation->set_rules('jobprofile','JobProfile','required');
                 $this->form_validation->set_rules('empreference','Empreference','required');

			     $this->form_validation->set_rules('cEmp','cEmpployee','required');
                 $this->form_validation->set_rules('salary','Salary','required');
                 $this->form_validation->set_rules('dIncome','DIncome','required');
                 $this->form_validation->set_rules('crentact','CurrentActivity','required');
                 $this->form_validation->set_rules('mytext','mytext','');
                 $this->form_validation->set_rules('charitywrk','CharityWork','required');
                 $this->form_validation->set_rules('assCharity','AssistingCharity','required');
                 $this->form_validation->set_select('organisations[]','Organisations','required');
                
                 date_default_timezone_set('Asia/Kolkata');
                      $date = date('d-m-Y H:i'); 

	             if($this->form_validation->run() == TRUE)
	             
	            {
	            	 //echo "valid"; exit;

	                $user_id = $this->session->userdata('user_id');
                    $emplrNme = $this->input->post('emplrNme');
                    $fromMonth = $this->input->post('fromMonth');
                    $fromYear = $this->input->post('fromYear');
                    $toMonth = $this->input->post('toMonth');
                    $toYear = $this->input->post('toYear');
                    $designation = $this->input->post('designation');
                    $jobprofile = $this->input->post('jobprofile');
                    $empreference = $this->input->post('empreference');

	            	$cEmp = $this->input->post('cEmp');
	            	$salary = $this->input->post('salary');
	            	$dIncome = $this->input->post('dIncome');
	            	$crentact = $this->input->post('crentact');
	            	$addiInstrctn = $this->input->post('mytext');
	            	$charitywrk = $this->input->post('charitywrk');
	            	$assCharity = $this->input->post('assCharity');
	            	$organisations = $this->input->post('organisations');
	            	$resume = $user_id.$this->input->post('pdf');
	            	
                      // $check = $this->user_model->getUserPreEmploymentInfo($user_id);                                        
                      
                      

                             $preEmploymentArray = array(
                                         'user_id' => $user_id,
                                         'employer_name' => $emplrNme,
                                         'designation' => $designation,
                                         'job_profile'  => $jobprofile,
                                        
                                         'employer_reference' => $empreference,
                                         'from_month' => $fromMonth,
                                         'from_year' => $fromYear,
                                         'to_month'  => $toMonth ,
                                         'to_year'  => $toYear ,
                                        'current_emplymnt_status' => $cEmp,
                                        'present_income' => $salary,
                                        'desired_income' => $dIncome,
                                        'currently_actively_applying_status' => $crentact,
                                        'currently_actively_applying_desc' => $addiInstrctn,
                                        'undertaken_voluntary_charity_work' => $charitywrk,
                                        'interested_in_volunteering_assisting_charities_status' => $assCharity,
                                         );

                   // echo "<pre>"; print_r($preEmploymentArray); exit;

                   $userPreEmploymentInfo = $this->user_model->saveUserPreEmploymentInfo($preEmploymentArray);
               
                  $last_id = $userPreEmploymentInfo;
                   if($userPreEmploymentInfo)
                   {

                       
                          
                        
                       foreach ($_POST['organisations'] as $selected) {

                            
                       $orgArray = array(
                                  
                                  'u_id' => $user_id,
                                  'organisation' => $selected,
                                  'pre_id' => $last_id
                                    );
                           
                           $saveSelected = $this->user_model->saveSelectedOrg($orgArray);
                            }

                           $steps = array(
                                       'u_id' => $user_id,
                                       'pre_employment' => 1,
                                       'updated_date' => $date

                           	             );


                          $steps = $this->user_model->updateUserSteps($steps,$user_id);

	                  

	                     	 $data = array('status' => '1');
		                        	$this->session->set_flashdata('succmsg','Your information has been saved successfully');

                   }

                    
	             }
	             else
	             {
                  
                    $data = array('status' => '3');
		       $this->session->set_flashdata('errormsg', 'All fields are mandatory.');

	             }



		     }
                
		     print_r(json_encode($data));
    }

    /*====================FUNCTION FOR USER EMPLOYMENT VIEW ===============*/


    public function employment()
    {

          // echo 123; exit;
           if(!$this->session->userdata('user_id'))
           {
           	redirect(base_url().'user');
           }
           error_reporting(0);
           $user_id = $this->session->userdata('user_id');
           $data['title'] = "Employment";
           $data['userEmploymentDetail'] = $this->user_model->userEmploymentInfo($user_id);  
           $data['months'] = $this->user_model->getMonths();
           $data['years']  = $this->user_model->getYears();
           $data['training'] = $this->user_model->getTrainings();
           $userProfileSteps = $this->user_model->getUserCompSteps($user_id);
           $data['userProfileData'] = $this->user_model->getUserProfileData($user_id);

	     $data1['profile'] = $userProfileSteps->profile;
	    // echo $profile; exit;
	     $data1['education']= $userProfileSteps->education;
	    
	     $data1['pre_employment']= $userProfileSteps->pre_employment;
	     $data1['employment']= $userProfileSteps->employment;
	     $data1['references']= $userProfileSteps->references;
	     $data1['additional_info']= $userProfileSteps->additional_info;
	     $data1['miscellaneous']= $userProfileSteps->miscellaneous;
	     $data1['social_media']= $userProfileSteps->social_media;
	     $data1['resume'] = $userProfileSteps->resume;
         $data1['current']=4;
            $this->load->view('user/all_css',$data);
			$this->load->view('user/header',$data);
			$this->load->view('user/menu',$data1);
			$this->load->view('user/employment',$data);
		    $this->load->view('user/footer');
		    $this->load->view('user/all_js');
		   $this->load->view('user/pagewisejs/employment.js');

    }




    /*==================== FUNCTION FOR SAVE USER EMPLOYMENT DEATAIL ================*/

    public function employmentInfo()
    {
     
     $_POST = json_decode(file_get_contents('php://input'),true);
         
        //print_r($_POST); exit;

		    if($this->input->post())
		    {
 
            $this->form_validation->set_rules('position','Position','required');
            $this->form_validation->set_rules('cmpnyName','CmpnyName','required');
            $this->form_validation->set_select('yearFrom','yearFrom','required');
            $this->form_validation->set_select('monthFrom','monthFrom','required');
            $this->form_validation->set_select('dateTo','dateTo','required');
            $this->form_validation->set_select('toMonth','toMonth','required');
            $this->form_validation->set_rules('userCity','userCity','required');
            $this->form_validation->set_rules('userCountry','userCountry','required');
            $this->form_validation->set_rules('role','role','required');
            $this->form_validation->set_rules('respn','respn','required');
            $this->form_validation->set_select('training','training','required');

                  
            if($this->form_validation->run() == TRUE)
             {
                  $user_id = $this->session->userdata('user_id');

                 $position = $this->input->post('position');
                 $cmpnyName = $this->input->post('cmpnyName');
                 $yearFrom = $this->input->post('yearFrom');
                 $monthFrom = $this->input->post('monthFrom');
                 $toYear = $this->input->post('dateTo');
                 $toMonth = $this->input->post('toMonth');
                 $userCity = $this->input->post('userCity');
    
                 $userCountry = $this->input->post('userCountry');
                 $role = $this->input->post('role');
                 $respn = $this->input->post('respn');
                 $training = $this->input->post('training');
                 date_default_timezone_set('Asia/Kolkata');
                      $date = date('d-m-Y H:i');

                 $employmentArray = array(
                               
                               'user_id' => $user_id,
                               'position_held' =>$position,
                               'company_name' => $cmpnyName,
                               'from_year'   =>  $yearFrom,
                               'from_month'  =>  $monthFrom,
                               'to_year'      => $toYear,
                               'to_month'       => $toMonth,
                               'location_city' => $userCity,
                               'location_country' => $userCountry,
                               'purpose_of_role' =>$role,
                               'rspnsibilities_achvmnts' => $respn,
                               'internal_training' => $training
                                );
                          
               $check = $this->user_model->checkUserEmployment($user_id);
               if(empty($check)){


                 $saveUserEmntInfo = $this->user_model->saveUserEmploymentInfo($employmentArray);
                 $steps = array(
                                       'u_id' => $user_id,
                                       'employment' => 1,
                                       'updated_date' => $date

                           	             );


                          $steps = $this->user_model->updateUserSteps($steps,$user_id);

	                  

	                     	 $data = array('status' => '1');
		                        	$this->session->set_flashdata('succmsg','Your information has been saved successfully');
      

                 }else{

                   
                     $updateUserEmployment = $this->user_model->updateUserEmployment($user_id,$employmentArray);

                   $data = array('status' => '2');
		                        	$this->session->set_flashdata('succmsg','Your information has been updated successfully');

                 }





            }
            
             }else{
            

               $data = array('status' => '3');
		       $this->session->set_flashdata('errormsg', '<div class="alert alert-warning alert-dismissable"><a type="button" class="close" data-dismiss="alert" aria-hidden="true">×</a>All fields are mandatory.</div>');

		         }

            print_r(json_encode($data));

    }

/*================= FUNCTION FOR GET USER SAVED PREEMPLOYMENT DETAIL ===========*/


public function getUserPreEmployment()
{
$_POST = json_decode(file_get_contents('php://input'),true);
  $id = $this->input->post('id');
  $user_id = $this->session->userdata('user_id');
  $data['userPreEmployementInfo'] = $this->user_model->userPreEmplymentInfo($id);
  $data['userOrganisation']   = $this->user_model->getUserOrganisation($id);

    
  //  print_r($data); 



    print_r(json_encode($data));


}

 /*=============== FUNCTION FOR UPDATE USER PRE-EMPLOYMENT =========*/

 public function replaceUserPreEmploymentInfo(){

   $_POST = json_decode(file_get_contents('php://input'),true);



       //  echo "<pre>"; print_r($_POST); exit();


	    if($this->input->post())
	    {

               
               $this->form_validation->set_rules('eemplrNme','EmployerName','required');
               $this->form_validation->set_rules('efromMonth','FromMonth','required');
               $this->form_validation->set_rules('efromYear','FromYear','required');
               $this->form_validation->set_rules('etoMonth','toMonth','required');
               $this->form_validation->set_rules('etoYear','toYear','required');
               $this->form_validation->set_rules('edesignation','EmployerName','required');
               $this->form_validation->set_rules('ejobprofile','jobProfile','required');
               $this->form_validation->set_rules('ecEmp','Cemp','required');
               $this->form_validation->set_rules('eempreference','Reference','required');
               $this->form_validation->set_rules('esalary','Salary','required');
               $this->form_validation->set_rules('edIncome','dincome','required');
               $this->form_validation->set_rules('ecrentact','act','required');
               $this->form_validation->set_rules('emytext','crrAppLoc','');
               $this->form_validation->set_rules('echaritywrk','Charity','required');
               $this->form_validation->set_rules('eassCharity','AssCharity','required');
               $this->form_validation->set_select('eorganisations','organisation','required');
              
               
               if($this->form_validation->run() == TRUE)
              
              {
                   
                   date_default_timezone_set('Asia/Kolkata');
                      $date = date('d-m-Y H:i');
                    $id = $this->input->post('eid');
                    $user_id =  $this->session->userdata('user_id');
                    $eemplrNme = $this->input->post('eemplrNme');
                    $efromMonth = $this->input->post('efromMonth');
                    $efromYear = $this->input->post('efromYear');
                    $etoMonth = $this->input->post('etoMonth');
                    $etoYear = $this->input->post('etoYear');
                    $edesignation = $this->input->post('edesignation');
                    $ejobprofile = $this->input->post('ejobprofile');
                   
                    $ecEmp = $this->input->post('ecEmp');
                    $eempreference = $this->input->post('eempreference');
                    $esalary = $this->input->post('esalary');
                    $edIncome = $this->input->post('edIncome');
                    $ecrentact = $this->input->post('ecrentact');
                    $crrAppLoc = $this->input->post('emytext');
                    $echaritywrk = $this->input->post('echaritywrk');
                    $eassCharity = $this->input->post('eassCharity');
                    $eorganisations = $this->input->post('eorganisations'); 


                    $editUserPreEmployementArr = array(
                                            
                                            'user_id' =>  $user_id,
                                            'employer_name' => $eemplrNme,
                                            'designation' => $edesignation,
                                            'job_profile' => $ejobprofile,
                                            
                                            'employer_reference' => $eempreference,
                                            'from_month' => $efromMonth, 
                                            'from_year' => $efromYear,
                                            'to_month' => $etoMonth,
                                            'to_year' => $etoYear,
                                            'current_emplymnt_status' => $ecEmp,
                                            'present_income' =>$esalary,
                                            'desired_income' => $edIncome,
                                            'currently_actively_applying_status' => $ecrentact,
                                            'currently_actively_applying_desc' => $crrAppLoc,
                                            'undertaken_voluntary_charity_work' => $echaritywrk,
                                            'interested_in_volunteering_assisting_charities_status' =>$eassCharity,
                                            'updated_date' => $date
                                              );
                       
                      // echo "<pre>"; print_r($editUserPreEmployementArr); exit;

                   $updatePreEmplymntInfo = $this->user_model->updateUserPreEmploymentInfo($editUserPreEmployementArr,$id);

                   if($updatePreEmplymntInfo)
                   {

                         $saveSelected = $this->user_model->updateSelectedOrg($user_id,$id);

                         $data = array('status' => '1');
		                        	$this->session->set_flashdata('succmsg','Your information has been updated successfully');
                           
                            }






                   }
                 


              

              


	    }else{


              	 $data = array('status' => '3');
			       $this->session->set_flashdata('errormsg', '<div class="alert alert-warning alert-dismissable"><a type="button" class="close" data-dismiss="alert" aria-hidden="true">×</a>All fields are mandatory.</div>');
              }
 
                print_r(json_encode($data));
      

  }
    /*===============FUNCTION FOR LOAD VIEW  reference ==============*/
     public function reference()
     {


        //echo 123; exit;
         if(!$this->session->userdata('user_id'))
           {
           	redirect(base_url().'user');
           }
           error_reporting(0);
             $user_id = $this->session->userdata('user_id');
             $data['countries'] = $this->user_model->getAllContries();
             $data['userRefences'] = $this->user_model->getUserRefeneces();
             $data['savedUserReferences'] = $this->user_model->getSavedUserReferences($user_id);
             $userProfileSteps = $this->user_model->getUserCompSteps($user_id);
             $data['userProfileData'] = $this->user_model->getUserProfileData($user_id);

	     $data1['profile'] = $userProfileSteps->profile;
	    // echo $profile; exit;
	     $data1['education']= $userProfileSteps->education;
	     $data['title'] = "References";
	    
	     $data1['pre_employment']= $userProfileSteps->pre_employment;
	     $data1['employment']= $userProfileSteps->employment;
	     $data1['references']= $userProfileSteps->references;
	     $data1['additional_info']= $userProfileSteps->additional_info;
	     $data1['miscellaneous']= $userProfileSteps->miscellaneous;
	     $data1['social_media']= $userProfileSteps->social_media;
	     $data1['resume'] = $userProfileSteps->resume;
          $data1['current']=6;
            $this->load->view('user/all_css',$data);
			$this->load->view('user/header',$data);
			$this->load->view('user/menu',$data1);
			$this->load->view('user/references',$data);
		    $this->load->view('user/footer');
		    $this->load->view('user/all_js');
		    $this->load->view('user/pagewisejs/references.js');



     }


     /*================== FUNCTION FOR SAVE USER REFRENCES INFO ==============*/

     public function referenceInfo()
     {

     $_POST = json_decode(file_get_contents('php://input'),true);
      
       //echo "<pre>"; print_r($_POST); exit;


         if($this->input->post())
         {

           
           $this->form_validation->set_select('userRefence','UserRefence','required');
           $this->form_validation->set_rules('bDetail','BusinessDetail','required');
           $this->form_validation->set_rules('addLine1','Address1','required');
           $this->form_validation->set_rules('addLine2','Address2','required');
           $this->form_validation->set_rules('addLine3','Address3','required');
           $this->form_validation->set_rules('city','City','required');
           $this->form_validation->set_select('country','Country','required');
           $this->form_validation->set_rules('postcode','Postcode','required');
           $this->form_validation->set_rules('email','Email','required');
           $this->form_validation->set_rules('telephone','Telephone','required');
           $this->form_validation->set_rules('fax','Fax');
            
	           if($this->form_validation->run()==TRUE)
	           {
                 // echo "valid"; exit;
                 $user_id = $this->session->userdata('user_id');
                 $userRefence = $this->input->post('userRefence');
                 $bDetail = $this->input->post('bDetail');
                 $Address1 = $this->input->post('addLine1');
                 $Address2 = $this->input->post('addLine2');
                 $Address3 = $this->input->post('addLine3');
                 $city = $this->input->post('city');
                 $country = $this->input->post('country');
                 $postcode = $this->input->post('postcode');
                 $email = $this->input->post('email');
                 $telephone = $this->input->post('telephone');
                 $fax = $this->input->post('fax');
                  
                   date_default_timezone_set('Asia/Kolkata');
                      $date = date('d-m-Y H:i');
                     
                     $referenceArray=array(

                                   'user_id'=>$user_id,
                                   'type_of_reference' => $userRefence,
                                   'business_details_main_contact' => $bDetail,
                                   'address1' => $Address1,
                                   'address2' => $Address2,
                                   'address3' => $Address3,
                                   'city' =>    $city,
                                   'country' => $country,
                                   'post_code' => $postcode,
                                   'email' =>   $email,
                                   'telephone' => $telephone,
                                   'fax'  => $fax
                                     );
                           
                         //  echo "<pre>"; print_r($referenceArray); exit;

                        $check = $this->user_model->checkUserReferences($user_id);

                        if(!empty($check)){


                           $updateUserReferences =    $this->user_model->updateUserReferences($referenceArray,$user_id);


                        if($updateUserReferences)
                        {


                         $steps = array(
                                       'u_id' => $user_id,
                                       'references' => 1,
                                       'updated_date' => $date

                           	             );


                          $steps = $this->user_model->updateUserSteps($steps,$user_id);

	                  

	                     	 $data = array('status' => '1');
		                        	$this->session->set_flashdata('succmsg','Your information has been updated successfully');




                        }


                        }else{

                        	 $userReferences =    $this->user_model->saveUserReferences($referenceArray);


                        if($userReferences)
                        {


                         $steps = array(
                                       'u_id' => $user_id,
                                       'references' => 1,
                                       'updated_date' => $date

                           	             );


                          $steps = $this->user_model->updateUserSteps($steps,$user_id);

	                  

	                     	 $data = array('status' => '1');
		                        	$this->session->set_flashdata('succmsg','Your information has been saved successfully');




                        }

                        }

                       


	           }
	         

	         }else{
	            

	               $data = array('status' => '3');
			       $this->session->set_flashdata('errormsg', '<div class="alert alert-warning alert-dismissable"><a type="button" class="close" data-dismiss="alert" aria-hidden="true">×</a>All fields are mandatory.</div>');

			   }
              
               print_r(json_encode($data));

        }



/*=================== FUNCTION FOR SAVE USER ADDITIONAL INFORMATION ============*/


  public function additionalInfo(){

     if(!$this->session->userdata('user_id'))
           {
           	redirect(base_url().'user');
           }
             


         $user_id = $this->session->userdata('user_id');
         $userProfileSteps = $this->user_model->getUserCompSteps($user_id);
         $data['savedUserAdditionalInfo'] = $this->user_model->getSavedUserAdditionalInfo($user_id); 
         $data['title'] = "Additional-Information";
	     $data1['profile'] = $userProfileSteps->profile;
	     $data['userProfileData'] = $this->user_model->getUserProfileData($user_id);
	    // echo $profile; exit;
	     $data1['education']= $userProfileSteps->education;
	    
	     $data1['pre_employment']= $userProfileSteps->pre_employment;
	     $data1['employment']= $userProfileSteps->employment;
	     $data1['references']= $userProfileSteps->references;
	     $data1['additional_info']= $userProfileSteps->additional_info;
	     $data1['miscellaneous']= $userProfileSteps->miscellaneous;
	     $data1['social_media']= $userProfileSteps->social_media;
	     $data1['resume'] = $userProfileSteps->resume;
         $data1['current']=7;
            $this->load->view('user/all_css',$data);
			$this->load->view('user/header',$data);
			$this->load->view('user/menu',$data1);
			$this->load->view('user/additional-information',$data);
		    $this->load->view('user/footer');
		    $this->load->view('user/all_js');
		    $this->load->view('user/pagewisejs/additionalInfo.js');
  




  }

/*================= FUNCTION FOR SAVE USER ADDITIONAL INFORMATION ===============*/

public function userAdditionalInfo()
{

  $_POST = json_decode(file_get_contents('php://input'),true);
      
       //echo "<pre>"; print_r($_POST); exit;

       if($this->input->post())
         {
          

           $this->form_validation->set_rules('favSub','FavSub','required');
           $this->form_validation->set_rules('exp','Exp','required');
           $this->form_validation->set_rules('newSub','NewSub','required');
           $this->form_validation->set_rules('dreamJob','DreamJob','required');
           $this->form_validation->set_rules('significant','Significant','required');
           $this->form_validation->set_rules('achieve','Achieve','required');
           $this->form_validation->set_rules('regret','Regret','required');
           $this->form_validation->set_rules('live','Live','required');
           
           if($this->form_validation->run()==TRUE)

           {
            $user_id = $this->session->userdata('user_id');
            $favSub = $this->input->post('favSub');
            $exp = $this->input->post('exp');
            $newSub = $this->input->post('newSub');
            $dreamJob = $this->input->post('dreamJob');
            $significant = $this->input->post('significant');
            $achieve = $this->input->post('achieve');
            $regret = $this->input->post('regret');
            $live = $this->input->post('live');
            date_default_timezone_set('Asia/Kolkata');
                      $date = date('d-m-Y H:i');

            $userAddArray = array(


                           'user_id' => $user_id ,
                           'favourite_subject_at_school' => $favSub,
                           'stating_explain' => $exp,
                           'lrn_new_sub_wht_and_why'=> $newSub,
                           'dream_job_why' => $dreamJob,
                           'long_term_and_aspirations' => $significant,
                           'taken_to_achieve'=> $achieve,
                           'regret_in_life' => $regret,
                           'spend_and_regrets' => $live
                          );
                      
                   
                   $check = $this->user_model->getSavedUserAdditionalInfo($user_id);

                   if(empty($check))
                   {
                   // echo 1; exit;
                    $userAdditionalInfo = $this->user_model->saveuserAdditionalInfo($userAddArray);
                  
                  

                       if($userAdditionalInfo)
                        {


                         $steps = array(
                                       'u_id' => $user_id,
                                       'additional_info' => 1,
                                       'updated_date' => $date

                           	             );


                          $steps = $this->user_model->updateUserSteps($steps,$user_id);

	                  

	                     	 $data = array('status' => '1');
		                        	$this->session->set_flashdata('succmsg','Your information has been saved successfully');




                        }



                   }
                   else{
                          //echo 2;

                          //echo "<pre>"; print_r($userAddArray); exit;
                 $userAdditionalInfo = $this->user_model->updateUserAdditionalInfo($userAddArray,$user_id);
                  
                  

                       if($userAdditionalInfo)
                        {


                         $steps = array(
                                       'u_id' => $user_id,
                                       'additional_info' => 1,
                                       'updated_date' => $date

                           	             );


                          $steps = $this->user_model->updateUserSteps($steps,$user_id);

	                  

	                     	 $data = array('status' => '1');
		                        	$this->session->set_flashdata('succmsg','Your information has been updated successfully');




                        }

                  
                        }

                    }  






          }else{
	            

	               $data = array('status' => '3');
			       $this->session->set_flashdata('errormsg', '<div class="alert alert-warning alert-dismissable"><a type="button" class="close" data-dismiss="alert" aria-hidden="true">×</a>All fields are mandatory.</div>');

			   }
              
               print_r(json_encode($data));



}

/*================ FUNCTION FOR LOAD VIEW miscellaneous =================== */

  public function miscellaneous()
  {
    
     if(!$this->session->userdata('user_id'))
           {
           	redirect(base_url().'user');
           }
              
              error_reporting(0);
       
             $user_id = $this->session->userdata('user_id');
             $userProfileSteps = $this->user_model->getUserCompSteps($user_id);
             $data['userMiscellaneousInfo'] = $this->user_model->getSaveduserMiscellaneous($user_id);
             $data['userProfileData'] = $this->user_model->getUserProfileData($user_id);

	     $data1['profile'] = $userProfileSteps->profile;
	    // echo $profile; exit;
	     $data1['education']= $userProfileSteps->education;
	     $data['title'] = 'Miscellaneous';
	     $data1['pre_employment']= $userProfileSteps->pre_employment;
	     $data1['employment']= $userProfileSteps->employment;
	     $data1['references']= $userProfileSteps->references;
	     $data1['additional_info']= $userProfileSteps->additional_info;
	     $data1['miscellaneous']= $userProfileSteps->miscellaneous;
	     $data1['social_media']= $userProfileSteps->social_media;
	     $data1['resume'] = $userProfileSteps->resume;
         $data1['current']=8;

            $this->load->view('user/all_css',$data);
			$this->load->view('user/header',$data);
			$this->load->view('user/menu',$data1);
			$this->load->view('user/miscellaneous',$data);
		    $this->load->view('user/footer');
		    $this->load->view('user/all_js');
		    $this->load->view('user/pagewisejs/miscellaneous.js');
  



  }



 /*================== FUNCTION FOR SAVE USER MISCLAANEOUS INFORMMATION =================*/

 public function miscellaneousInfo()
 {
   
   $_POST = json_decode(file_get_contents('php://input'),true);

 //echo "<pre>"; print_r($_POST); exit;

	    if($this->input->post())
	    {

	       $this->form_validation->set_rules('lifeStyle','LifeStyle','required');
	       $this->form_validation->set_rules('health','Health','required');
	       $this->form_validation->set_rules('fitness','Fitness','required');
	       $this->form_validation->set_radio('optradio','lifeStyleRate','required');
	       $this->form_validation->set_radio('optradio1','healthRate','required');
	       $this->form_validation->set_radio('optradio2','finessRate','required');
	       

				       if($this->form_validation->run()==TRUE)
				       {

                        
                        $user_id = $this->session->userdata('user_id');

                        $lifeStyle = $this->input->post('lifeStyle');
                        $health = $this->input->post('health');
                        $fitness = $this->input->post('fitness');
                        $lyfstylrate = $this->input->post('optradio');
                        $healthRate = $this->input->post('optradio1');
                        $fitnessRate = $this->input->post('optradio2');
                        date_default_timezone_set('Asia/Kolkata');
                      $date = date('d-m-Y H:i');
                         
                         $miscellaneousArray = array(
                                        
                                        'user_id' => $user_id,
                                        'life_style' => $lifeStyle,
                                        'lyf_style_rating' => $lyfstylrate,
                                         'health'=>  $health,
                                         'health_rating' =>$healthRate,
                                         'fitness' => $fitness,
                                         'fitness_rating' => $fitnessRate

                         	                 );

                          $userMiscellaneousInfo = $this->user_model->getSaveduserMiscellaneous($user_id);

                          if(empty($userMiscellaneousInfo))
                            {
                               $miscellaneous = $this->user_model->saveMiscellaneousInfo($miscellaneousArray);


	                               if($miscellaneous)
	                               {

                                        

                         $steps = array(

                                       'u_id' => $user_id,
                                       'miscellaneous' => 1,
                                       'updated_date' => $date

                           	             );


                          $steps = $this->user_model->updateUserSteps($steps,$user_id);

	                  

	                     	 $data = array('status' => '1');
		                        	$this->session->set_flashdata('succmsg','Your information has been saved successfully');



	                               }



                            }else{

                                $updateMiscellaneous = $this->user_model->updateUserMiscellaneous($miscellaneousArray,$user_id);

                                 if($updateMiscellaneous)
	                               {

                                        

                         $steps = array(

                                       'u_id' => $user_id,
                                       'miscellaneous' => 1,
                                       'updated_date' => $date

                           	             );


                          $steps = $this->user_model->updateUserSteps($steps,$user_id);

	                  

	                     	 $data = array('status' => '1');
		                        	$this->session->set_flashdata('succmsg','Your information has been updated successfully');



	                               }

                            }



				       }
				 





	     }else{
	            

	               $data = array('status' => '3');
			       $this->session->set_flashdata('errormsg', '<div class="alert alert-warning alert-dismissable"><a type="button" class="close" data-dismiss="alert" aria-hidden="true">×</a>All fields are mandatory.</div>');

			   }
              
               print_r(json_encode($data));
	    




 }




 /*=================== FUNCTION FOR LOAD VIEW SOCIAL MEDIA SECTION =============*/


 public function socialMedia()
 {

    if(!$this->session->userdata('user_id'))
           {
           	redirect(base_url().'user');
           }
         
         error_reporting(0);

         $user_id = $this->session->userdata('user_id');
         $userProfileSteps = $this->user_model->getUserCompSteps($user_id);

         $data['userSocialMediaLink'] = $this->user_model->getUserSavesLink($user_id);
         $data['title'] = 'Social-Media';   
	     $data1['profile'] = $userProfileSteps->profile;
	     $data['userProfileData'] = $this->user_model->getUserProfileData($user_id);
	    // echo $profile; exit;
	     $data1['education']= $userProfileSteps->education;
	    
	     $data1['pre_employment']= $userProfileSteps->pre_employment;
	     $data1['employment']= $userProfileSteps->employment;
	     $data1['references']= $userProfileSteps->references;
	     $data1['additional_info']= $userProfileSteps->additional_info;
	     $data1['miscellaneous']= $userProfileSteps->miscellaneous;
	     $data1['social_media']= $userProfileSteps->social_media;
	     $data1['resume'] = $userProfileSteps->resume;
         $data1['current']=9;
            $this->load->view('user/all_css',$data);
			$this->load->view('user/header',$data);
			$this->load->view('user/menu',$data1);
			$this->load->view('user/social-media',$data);
		    $this->load->view('user/footer');
		    $this->load->view('user/all_js');
		    $this->load->view('user/pagewisejs/socialMedia.js');
 
 

 }

/*================ FUNCTION FOR LOAD VIEW UPLOAD RESUME==========*/

 public function resume()
 {


if(!$this->session->userdata('user_id'))
           {
           	redirect(base_url().'user');
           }
         
         error_reporting(0);

         $user_id = $this->session->userdata('user_id');
         $userProfileSteps = $this->user_model->getUserCompSteps($user_id);

         $data['userSocialMediaLink'] = $this->user_model->getUserSavesLink($user_id);
         $data['title'] = 'Resume';   
	     $data1['profile'] = $userProfileSteps->profile;
	     $data['userProfileData'] = $this->user_model->getUserProfileData($user_id);
	    // echo $profile; exit;
	     $userResumes = $this->user_model->getUserResumeInfo($user_id);
        
        $temp = array();

        foreach($userResumes->result() as $key => $val) {
          $temp[$val->profile_id][]=$val;

        }
             //echo "<pre>"; print_r($temp); exit;
        $data['resumesInfo']=$temp;


	     $data1['education']= $userProfileSteps->education;
	    
	     $data1['pre_employment']= $userProfileSteps->pre_employment;
	     $data1['employment']= $userProfileSteps->employment;
	     $data1['references']= $userProfileSteps->references;
	     $data1['additional_info']= $userProfileSteps->additional_info;
	     $data1['miscellaneous']= $userProfileSteps->miscellaneous;
	     $data1['social_media']= $userProfileSteps->social_media;
	     $data1['resume'] = $userProfileSteps->resume;
         $data1['current']=5;
            $this->load->view('user/all_css',$data);
			$this->load->view('user/header',$data);
			$this->load->view('user/menu',$data1);
			$this->load->view('user/resume',$data);
		    $this->load->view('user/footer');
		    $this->load->view('user/all_js');
		    $this->load->view('user/pagewisejs/resume.js');



 }






 /*============= FUNCTION FOR SAVE USER SOCIAL MEDIA INFO===============*/

  public function  userSocialMediaInfo()
  {

     $_POST = json_decode(file_get_contents('php://input'),true);

     //echo "<pre>"; print_r($_POST); exit;

     if($this->input->post())
     {

         $this->form_validation->set_rules('fb','facebook','');
         $this->form_validation->set_rules('youtube','YouTube','');
         $this->form_validation->set_rules('twitter','Twitter','');
         $this->form_validation->set_rules('linkedIn','LinkedInn','');
         $this->form_validation->set_rules('instagram','Instagram','');
        
            

              
              $user_id = $this->session->userdata('user_id'); 
              $fb = $this->input->post('fb');
              $youtube = $this->input->post('youtube');
              $twitter = $this->input->post('twitter');
              $linkedIn = $this->input->post('linkedIn');
              $instagram = $this->input->post('instagram');
                 date_default_timezone_set('Asia/Kolkata');
                      $date = date('d-m-Y H:i');
           $socialmediaArray = array(

                            'user_id' => $user_id,
                            'facebook' => $fb,
                            'youtube' => $youtube,
                            'twitter' => $twitter,
                            'linkedIn' => $linkedIn,
                            'instagram' => $instagram,

                            );

                 
                 $check = $this->user_model->getUserSavesLink($user_id);
                 if(!empty($check))
                 {


                    $updateUserLinks = $this->user_model->updateUserSocialMedia($user_id,$socialmediaArray);
                     if($updateUserLinks){

                             $steps = array(

                                       'u_id' => $user_id,
                                       'social_media' => 1,
                                       'updated_date' => $date

                           	             );


                          $steps = $this->user_model->updateUserSteps($steps,$user_id);

	                  

	                     	 $data = array('status' => '1');
		                        	$this->session->set_flashdata('succmsg','Your information has been updated successfully');



	                               }

                 }else{


                 $saveUserSocial = $this->user_model->saveUserSocialMedia($socialmediaArray);

                 if($saveUserSocial){

                             $steps = array(

                                       'u_id' => $user_id,
                                       'social_media' => 1,
                                       'updated_date' => $date

                           	             );


                          $steps = $this->user_model->updateUserSteps($steps,$user_id);

	                  

	                     	 $data = array('status' => '1');
		                        	$this->session->set_flashdata('succmsg','Your  information has been saved successfully');



	                               }
                                }





            


     }else{
	            

	               $data = array('status' => '3');
			       $this->session->set_flashdata('errormsg', '<div class="alert alert-warning alert-dismissable"><a type="button" class="close" data-dismiss="alert" aria-hidden="true">×</a>All fields are mandatory.</div>');

			   }
            

            print_r(json_encode($data));


  }


/*============== FUNCTION FOR APPLY JOB BY USER ===============*/

	public function applyJob($j_id)
	{


     //echo $j_id; exit;
	// $_POST = json_decode(file_get_contents('php://input'),true);

             error_reporting(0);
                if($this->session->userdata('user_id'))
                {
                   
                   $user_id = $this->session->userdata('user_id');
                	$data1['userProfileData'] = $this->user_model->getUserProfileData($user_id);
                }
	

      // echo $this->session->userdata('user_id'); exit();
	    //echo $jobId; exit;

	  $data['jobDescription'] = $this->user_model->getJobDescription($j_id);

         $this->load->view('frontEnd/all_css');

                if(!empty($data1)){

                	$this->load->view('header',$data1);
                }else{
                	$this->load->view('header');
                }
				
				$this->load->view('user/job-description',$data);

				$this->load->view('footer');
				$this->load->view('frontEnd/all_js');
			 	$this->load->view('user/pagewisejs/jobdescription.js');


	}


	/*============ FUNCTION FOR SAVE APPLY JOB BY USER ===============*/
     
     public function userApplyJob()
     {
     //	$_POST = json_decode(file_get_contents('php://input'),true);

     //  echo $j_id.'cvffd'.$user_id; exit;
       
      $user_id = $this->session->userdata('user_id');
      $jobId =   $this->input->post('job_id');
      $resumeId = $this->input->post('resumeID');


     //  print_r($_POST); exit;
     $jobs = array(
             'job_id'  => $jobId,
             'user_id' => $user_id,
             'resume_id' => $resumeId
     	 );
   

    $saveUserApplyJob = $this->user_model->saveApplyJobsByUser($jobs);

    if($saveUserApplyJob){


    	$this->session->set_flashdata('succmsg','You have apply job successfully');

    	redirect(base_url().'user/searchedJob');
    } 

     }


     /*=============== FUNCTION FOR GET PROFILE INFO WHILE USER APPLYING JOB ============*/


     public function getUserSavedResumes()
     {


     $_POST = json_decode(file_get_contents('php://input'),true);

     // echo "<pre>"; print_r($_POST); exit;
     
     $user_id = $this->input->post('user_id');
     $job_id = $this->input->post('job_id');

                $resumesInfo =    $this->user_model->getUserResumeInfo($user_id);
               // $data['userProfileData'] = $this->user_model->getUserProfileData($user_id);

               foreach ($resumesInfo as $key ) {
               	
/*
               	$array = array(
                 'resume_id' =>$key->id,
                 'user-id' => $key->user_id,
                 'resume' => $key->resume_title

               		);
*/

              echo "<input type='radio' value='".$key->id."' name='resumeID'  checked >"."<input type='hidden' value='".$job_id."' name='job_id' >".' '.$key->resume_title;  		 
               //	print_r($array);
               }
    

                //print_r(json_encode($array));

     }


     /*==================== FUNCTION FOR CUSTOM JOB SEARCH BASED ON CHECK/SELECT ============*/
          
             public function oppeningsByIndusrty (){

          	// $_POST = json_decode(file_get_contents('php://input'),true);

            // echo "<pre>"; print_r($_POST); exit;

            // $searchedKeyword = $this->input->post('type');

            // echo  $searchedKeyword; exit;


             $data['searched'] = $this->user_model->getOppenningByIndustry();
                   
   
 
             $searchedJobs = $this->load->view('user/showJobs',$data,true);
            // $this->load->view('frontEnd/pagewisejs/jobListing.js');
             
              $jobs = array('jobs' => $searchedJobs);

            echo json_encode($jobs);



            /*//  echo "<pre>"; print_r($searched); exit;

              foreach ($searched as $jobs) {

               //  echo $jobs->job_title;
              //   echo "<br>";
               //  echo $jobs->cmpny_name;

              	
              	echo "<div class='col-md-6 col-sm-12'><ul class='list-unstyled'><li><div class='row'><div class='col-md-12 col-sm-6 col-xs-12'>
                    <div class='freelancer-wrap row-fluid clearfix'>
                        <div class='col-md-3 text-center'>
                            <div class='post-media'>
                                <img class='img-responsive' src='http://127.0.0.1/daydreamer/client_uploads/client_logo/$jobs->company_logo' alt=''>
                            </div>
                        </div>

                        <div class='col-md-9'>
                            <div class='rating'>
                                <i class='fa fa-star'></i>
                                <i class='fa fa-star'></i>
                                <i class='fa fa-star'></i>
                                <i class='fa fa-star'></i>
                                <i class='fa fa-star'></i>
                            </div>
                            <h4><a href='javascript:void()'> $jobs->job_title</a></h4>
                            <h5>$jobs->cmpny_name</h5>
                            <p class='skillsreq'>PHP, MySQL, Linux, Wordpress</p>
                            <ul class='list-inline'>
                                <li><small>£35,000 per year</small></li>
                                <li><small>$jobs->experience</small></li>
                            </ul>
                            <p>$jobs->job_desc</p>
                        </div>
                      
                      </div>
                </div></ul></div>";
              }*/



          }


          /*================== FUNCTION FOR LOAD VIEW REFINE SEARCH ==============*/


          public function refineSearch()
          {


              $data['jobsSectors'] = $this->user_model->getAllJobType();


              //echo "<pre>"; print_r($jobsSectors); exit;

             $this->load->view('user/all_css');
             $this->load->view('user/header');
             $this->load->view('frontEnd/refineSearch',$data);
             $this->load->view('user/footer');
		         $this->load->view('user/all_js');
		         $this->load->view('frontEnd/pagewisejs/refineSearch.js');



          }

          /*==========FUNCTION FOR ADVANCED SEARCH KEYWORDS==============*/


          public function advancedSearch()
          {

           // echo 123; exit;
               

           // echo "<pre>"; print_r($_POST); exit;


             error_reporting(0);
                if($this->session->userdata('user_id'))
                {
                   
                    $user_id = $this->session->userdata('user_id');
                    $data1['userProfileData'] = $this->user_model->getUserProfileData($user_id);
                  //$data['userAppliedJobs']  = $this->user_model->getUserAppliedJobs($user_id);
                }

            $search1  = $this->input->post('submit1');
            $Keywords = $this->input->post('Keywords');
            $KeywordsContains = $this->input->post('KeywordsContains');
            $jobtitle = $this->input->post('jobtitle');
            $locations = $this->input->post('locations');
            $jt = $this->input->post('jt');
            
             if(($search1))
             {

              
              // echo "<pre>"; print_r($_POST); exit();
            $search1  = $this->input->post('submit1');
            $Keywords = $this->input->post('Keywords');
            $KeywordsContains = $this->input->post('KeywordsContains');
            $jobtitle = $this->input->post('jobtitle');
            $jt = $this->input->post('jt');

              $data['searchedJobs'] = $this->user_model->getJobsByAdSearch1($Keywords,$KeywordsContains,$jobtitle,$jt,$locations);
              $data['jobsByIndustry'] = $this->user_model->getJobsByIndustry();
              $data['industries']     = $this->user_model->getAllIndustries();
              $data['jobsByTitle']    = $this->user_model->getJobsByTitle();
              $data['jobsByCity']     = $this->user_model->getJobsByCity(); 
              $data['jobsByExperience'] = $this->user_model->getJobsByExperience();


 
             $this->load->view('frontEnd/all_css');

                if(!empty($data1)){

                  $this->load->view('header',$data1);
                }else{

                  $this->load->view('header');
                }
        
        $this->load->view('frontEnd/job-listing',$data);

        $this->load->view('footer');
        $this->load->view('frontEnd/all_js');
        $this->load->view('frontEnd/pagewisejs/jobListing.js');



             }else{

           //  echo "<pre>"; print_r($_POST); exit();

             /* job type and salary */

              $salaryType = $this->input->post('salaryType');
              $salaryRangeFrom = $this->input->post('salaryRangeFrom');
              $salaryRangeTo = $this->input->post('salaryRangeTo');
              $hourlyRangeFrom = $this->input->post('hourlyRangeFrom');
              $hourlyRangeTo  = $this->input->post('hourlyRangeTo');

              /*  sectors */

               foreach($_POST['jobsSector'] as $key) {

                     $jobsSectorArr = array(
                         
                         'jobsSector' => $key

                         );

                  // echo "<pre>";  print_r($jobsSectorArr); 

               } //exit;

  $data['searchedJobs'] = $this->user_model->getJobsByAdSearch2($salaryType,$salaryRangeFrom,$salaryRangeTo,$hourlyRangeFrom,$hourlyRangeTo);
              $data['jobsByIndustry'] = $this->user_model->getJobsByIndustry();
              $data['industries']     = $this->user_model->getAllIndustries();
              $data['jobsByTitle']    = $this->user_model->getJobsByTitle();
              $data['jobsByCity']     = $this->user_model->getJobsByCity(); 
              $data['jobsByExperience'] = $this->user_model->getJobsByExperience();


 
             $this->load->view('frontEnd/all_css');

                if(!empty($data1)){

                  $this->load->view('header',$data1);
                }else{

                  $this->load->view('header');
                }
        
        $this->load->view('frontEnd/job-listing',$data);

        $this->load->view('footer');
        $this->load->view('frontEnd/all_js');
        $this->load->view('frontEnd/pagewisejs/jobListing.js');

             }



          }

          /*========== FUNCTION FOR SAVE JOB BY USER  ============*/

           public function saveJobByUser()
           {

              $job_id = $this->input->post('job_id');
              $user_id = $this->session->userdata('user_id');


             //echo $user_id."  ".$job_id; exit;

              $saveJob = $this->user_model->saveUserJob($user_id,$job_id);

              if($saveJob)
              {
                // $this->session->set_flashdata('succmsg','Job has been saved successfully');

                $status = $this->user_model->getSavedJobs($user_id,$job_id);

               // echo "<pre>"; print_r($status); exit;

               // $status1 = array('status1' => $status);

               echo json_encode($status);

                  

              }
               
               

           }
 
}

?>