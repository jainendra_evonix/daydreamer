<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', 'demochec_daydreamer_wp');

/** MySQL database username */
define('DB_USER', 'demochec_wordp');

/** MySQL database password */
define('DB_PASSWORD', 'Word@123');

/** MySQL hostname */
define('DB_HOST', 'democheck.in');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8mb4');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         '$yn[x];v%wg<];eE@9u}9S~^W}V/.5i)lnL/y(eP9>C!UJ7=/]`0@k]v~>Q,)L:9');
define('SECURE_AUTH_KEY',  '9np6ma8L,i1=6Pzwx;YYKC([)xzBxvko.08=.X]~#X{?rL8}_FcPw_{?FLeSj3{W');
define('LOGGED_IN_KEY',    'OsO<:h$A&J_>7E{JAR]dxPAkR9HF[tfrtqTM=Tw/`~3O]F&!L#Q?4C5Z8}(YGc+>');
define('NONCE_KEY',        '`,h28[f{s6U}UE@kUIKV@aLnvw3G)=3^6;>Y&D=J/_;v2Hs]6f@UhZ-F1w7of$+B');
define('AUTH_SALT',        'DPzKg)^s;b.ddu7_ 0;~9FHPfHu$U.3qWkYk/xY/}g6[zECoMN9,DM-hh#2(nX7$');
define('SECURE_AUTH_SALT', 'Fy4zJ%U]4+7/QcGbm&c*NDS+Q;<YL1/G-4>;S&ksUi3-h`:*[qY5VG.w#%f]H;jb');
define('LOGGED_IN_SALT',   '(}c6!dz;w^CVsd,?;;sM@~U^eyuO`K,ELA<{TNe4*gVJx!Is68b8m)(Kpv1&79P<');
define('NONCE_SALT',       'j*Q{cVe2u/?iL@+&$!Ia!SC&VEbF:>QoS(~XA0JlEb1rxwx5h&ccSW@8*fL.IL>v');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define('WP_DEBUG', false);

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
